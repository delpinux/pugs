# IMPORTANT NOTICE

This directory contains third party libraries that are mandatory to a minimal
build of `pugs`

These libraries are **NOT** part of `pugs`, they are only distributed to ease
`pugs`' building!

**THIS LIBRARIES ARE SUBJECT TO THEIR OWN LICENSE AND DO NOT FOLLOW `PUGS`
LICENCE**
