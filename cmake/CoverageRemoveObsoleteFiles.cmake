# ------------------------------------------------------------------------------
# Remove obsolete generated files that could last after a failing test-suite run
# These are generally trailing .gcda files
# ------------------------------------------------------------------------------

file(GLOB_RECURSE GCDA_FILE_LIST  "${PUGS_BINARY_DIR}/*.gcda")
if(GCDA_FILE_LIST)
  file(REMOVE ${GCDA_FILE_LIST})
endif()
