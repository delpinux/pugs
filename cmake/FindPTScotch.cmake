# Looking for PTScotch

find_package(PkgConfig)
pkg_check_modules(PC_PTSCOTCH QUIET PTSCOTCH)

find_path(PTSCOTCH_INCLUDE_DIR
  NAMES ptscotch.h
  PATH_SUFFIXES  "include" "include/scotch"
  HINTS "$ENV{PTSCOTCH_INCDIR}")

if(EXISTS "${PTSCOTCH_INCLUDE_DIR}/ptscotch.h")
  message(STATUS "Found ptscotch.h in ${PTSCOTCH_INCLUDE_DIR}")
  find_library(LIB_PTSCOTCH ptscotch $ENV{PTSCOTCH_LIBDIR})
  if("${LIB_PTSCOTCH}" STREQUAL "LIB_PTSCOTCH-NOTFOUND")
    message(WARNING "** Could not find ptscotch library.\n** Is PTSCOTCH_LIBDIR correctly set (Actual: \"$ENV{PTSCOTCH_LIBDIR}\")?")
  endif()
  set(PTSCOTCH_LIBRARIES ${LIB_PTSCOTCH})
  message(STATUS "Found ptscotch/scotch libraries ${PTSCOTCH_LIBRARIES}")
else()
  message(WARNING "** Could not find ptscotch.h.\n** Is PTSCOTCH_INCDIR correctly set (Actual: \"$ENV{PTSCOTCH_INCDIR}\")?")
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PTScotch
  FOUND_VAR
    PTSCOTCH_FOUND
  REQUIRED_VARS
    PTSCOTCH_LIBRARIES
    PTSCOTCH_INCLUDE_DIR)
