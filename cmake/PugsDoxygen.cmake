# ---------------------- Doxygen ----------------------
# check for Doxygen and eventually configure it

# define the option
option(BUILD_DOXYGEN_DOC "Build Doxygen documentation" ON)

# this is very much inspired from https://vicrucann.github.io/tutorials/quick-cmake-doxygen/

# check if Doxygen is installed
find_package(Doxygen)
if(DOXYGEN_FOUND)
  # set input and output files
  set(DOXYGEN_IN "${PUGS_SOURCE_DIR}/doc/Doxyfile.in")
  set(DOXYGEN_OUT "${PUGS_BINARY_DIR}/Doxyfile")

  # request to configure the file
  configure_file(${DOXYGEN_IN} ${DOXYGEN_OUT} @ONLY)
  message(STATUS "Configuring Doxygen")

  add_custom_target(doxygen
    COMMAND ${DOXYGEN_EXECUTABLE} ${DOXYGEN_OUT}
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Generating API documentation with Doxygen"
    VERBATIM)

endif()
