# Looking for Slurm

find_package(PkgConfig)
pkg_check_modules(PC_SLURM QUIET SLURM)

find_path(SLURM_INCLUDE_DIR
  NAMES
    "slurm/slurm.h"
    "slurm/spank.h"
  PATHS
    ENV SLURM
    ENV SLURM_PATH
    ENV SLURM_ROOT
    ENV CPATH
    ENV C_INCLUDE_PATH
    ENV CPLUS_INCLUDE_PATH
  PATH_SUFFIXES
    "include"
  DOC
  "Path to the SLURM include directory")

find_library(SLURM_LIBRARY
  NAMES
    slurm
  PATHS
    ENV SLURM
    ENV SLURM_PATH
    ENV SLURM_ROOT
    ENV LD_LIBRARY_PATH
    ENV LIBRARY_PATH
    ENV PATH
  PATH_SUFFIXES
    "lib"
    "lib64"
  DOC
    "Path to the SLURM shared library")

 if(EXISTS "${SLURM_INCLUDE_DIR}/slurm/slurm.h")
   message(STATUS "Found slurm.h in ${SLURM_INCLUDE_DIR}")
   if("${SLURM_LIBRARY}" STREQUAL "SLURM_LIBDIR-NOTFOUND")
     message(WARNING "Could not find slurm library.")
   endif()
   message(STATUS "Found slurm library ${SLURM_LIBRARY}")
 else()
   message(WARNING "Could not find slurm include dir.")
 endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Slurm
  FOUND_VAR
    SLURM_FOUND
  REQUIRED_VARS
    SLURM_LIBRARY
    SLURM_INCLUDE_DIR)
