# -------------------- Documentation ----------------

# check for Emacs since the documentation is writen in org-mode
find_program(EMACS emacs)

# check for LaTeX
find_package(LATEX COMPONENTS PDFLATEX)

# check for pygmentize
find_program(PYGMENTIZE pygmentize)

# check for gnuplot
find_package(Gnuplot)

# check for gmsh
find_program(GMSH NAMES gmsh)

add_custom_target(userdoc)
add_custom_target(doc DEPENDS userdoc)

if (EMACS AND GNUPLOT_FOUND AND GMSH)

  add_custom_target(pugsdoc-dir
    COMMAND ${CMAKE_COMMAND} -E make_directory "${PUGS_BINARY_DIR}/doc"
  )

  set(ORG_GENERATOR_FILES
    "${PUGS_SOURCE_DIR}/doc/lisp/build-doc-config.el"
    "${PUGS_SOURCE_DIR}/doc/lisp/share/pugs.el"
    "${PUGS_SOURCE_DIR}/doc/lisp/share/ob-pugs.el"
    "${PUGS_SOURCE_DIR}/doc/lisp/share/ob-pugs-error.el"
  )

  add_custom_command(
    COMMENT "Check Emacs packages for documentation building"
    OUTPUT "${PUGS_SOURCE_DIR}/doc/lisp/elpa"
    COMMAND
    ${CMAKE_COMMAND} -E env
    PUGS=${PUGS_BINARY_DIR}/pugs
    PUGS_CHECKPOINT=${PUGS_BINARY_DIR}/pugs_checkpoint
    HOME=${PUGS_SOURCE_DIR}/doc/lisp
    PUGS_SOURCE_DIR=${PUGS_SOURCE_DIR}
    PUGS_BINARY_DIR=${PUGS_BINARY_DIR}
    ${EMACS} -Q --script ${PUGS_SOURCE_DIR}/doc/lisp/build-doc-config.el
    DEPENDS ${ORG_GENERATOR_FILES}
  )

  add_custom_target(pugsdoc-download-elpa DEPENDS "${PUGS_SOURCE_DIR}/doc/lisp/elpa")

  add_custom_command(
    OUTPUT "${PUGS_BINARY_DIR}/doc/userdoc.html"
    COMMAND
    ${CMAKE_COMMAND} -E env
    PUGS=${PUGS_BINARY_DIR}/pugs
    PUGS_CHECKPOINT=${PUGS_BINARY_DIR}/pugs_checkpoint
    HOME=${PUGS_SOURCE_DIR}/doc/lisp
    PUGS_SOURCE_DIR=${PUGS_SOURCE_DIR}
    PUGS_BINARY_DIR=${PUGS_BINARY_DIR}
    ${EMACS} -Q --script ${PUGS_SOURCE_DIR}/doc/lisp/userdoc-html.el
    DEPENDS
    "${PUGS_SOURCE_DIR}/doc/userdoc.org"
    "${PUGS_SOURCE_DIR}/doc/lisp/userdoc-html.el"
    pugs
    pugs_checkpoint
    pugsdoc-dir
    pugsdoc-download-elpa
    ${ORG_GENERATOR_FILES}
    WORKING_DIRECTORY ${PUGS_BINARY_DIR}/doc
    COMMENT "Building user documentation in doc/userdoc.html"
    VERBATIM)

  add_custom_target(userdoc-html DEPENDS pugsdoc-dir "${PUGS_BINARY_DIR}/doc/userdoc.html")

  add_dependencies(userdoc userdoc-html)

  if (LATEX_PDFLATEX_FOUND AND PYGMENTIZE)

    add_custom_command(
      OUTPUT "${PUGS_BINARY_DIR}/doc/userdoc.pdf"
      COMMAND
      ${CMAKE_COMMAND} -E env
      PUGS=${PUGS_BINARY_DIR}/pugs
      PUGS_CHECKPOINT=${PUGS_BINARY_DIR}/pugs_checkpoint
      HOME=${PUGS_SOURCE_DIR}/doc/lisp
      PUGS_SOURCE_DIR=${PUGS_SOURCE_DIR}
      PUGS_BINARY_DIR=${PUGS_BINARY_DIR}
      ${EMACS} -Q --script ${PUGS_SOURCE_DIR}/doc/lisp/userdoc-pdf.el
      DEPENDS
      "${PUGS_SOURCE_DIR}/doc/userdoc.org"
      "${PUGS_SOURCE_DIR}/doc/lisp/userdoc-pdf.el"
      "${PUGS_SOURCE_DIR}/tools/pgs-pygments.sh"
      "${PUGS_SOURCE_DIR}/tools/pgs-pygments.py"
      pugs
      pugs_checkpoint
      pugsdoc-dir
      pugsdoc-download-elpa
      ${ORG_GENERATOR_FILES}
      WORKING_DIRECTORY ${PUGS_BINARY_DIR}/doc
      COMMENT "Building user documentation in doc/userdoc.pdf"
      VERBATIM)

    configure_file("${PUGS_SOURCE_DIR}/doc/build-userdoc-pdf.sh.in"
      "${PUGS_BINARY_DIR}/doc/build-userdoc-pdf.sh"
      FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
      @ONLY)

    add_custom_target(userdoc-pdf DEPENDS pugsdoc-dir "${PUGS_BINARY_DIR}/doc/userdoc.pdf" "${PUGS_BINARY_DIR}/doc/build-userdoc-pdf.sh")

    add_dependencies(userdoc userdoc-pdf)
    add_dependencies(userdoc-html userdoc-pdf)

  else()
    if (NOT LATEX_PDFLATEX_FOUND)
      add_custom_target(userdoc-missing-latex
	COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --no-newline "Cannot build pdf documentation: "
	COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --red --bold "pdflatex missing")
      add_dependencies(userdoc userdoc-missing-latex)
    endif()

    if (NOT PIGMENTIZE_FOUND)
      add_custom_target(userdoc-missing-pygmentize
	COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --no-newline "Cannot build pdf documentation: "
	COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --red --bold "pygmentize missing")
      add_dependencies(userdoc userdoc-missing-pygmentize)
    endif()

  endif()

else()
  if (NOT EMACS)
    add_custom_target(userdoc-missing-emacs
      COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --no-newline "Cannot build documentation: "
      COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --red --bold "emacs missing")
    add_dependencies(userdoc userdoc-missing-emacs)
  endif()

  if (NOT GNUPLOT_FOUND)
    add_custom_target(userdoc-missing-gnuplot
      COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --no-newline "Cannot build documentation: "
      COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --red --bold "gnuplot missing")
    add_dependencies(userdoc userdoc-missing-gnuplot)
  endif()

  if (NOT GMSH)
    add_custom_target(userdoc-missing-gmsh
      COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --no-newline "Cannot build documentation: "
      COMMAND ${CMAKE_COMMAND} -E cmake_echo_color --red --bold "gmsh missing")
    add_dependencies(userdoc userdoc-missing-gmsh)
  endif()
endif()

add_dependencies(doc userdoc)
