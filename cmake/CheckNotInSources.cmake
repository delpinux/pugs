# -----------------------------------------------------------------
#            Checks we are not building in source-tree
# -----------------------------------------------------------------

# Explicitely authorize ${CMAKE_SOURCE_DIR}/build since some people
if (${CMAKE_BINARY_DIR} MATCHES "^${CMAKE_SOURCE_DIR}")
  if (NOT ${CMAKE_BINARY_DIR} MATCHES "^${CMAKE_SOURCE_DIR}/build")
    message("")
    message("##############################################")
    message("     In-source building is not allowed!       ")
    message("##############################################")
    message("")
    message(" Run cmake outside from source directory ")
    message(" or from ${CMAKE_SOURCE_DIR}/build")
    message("")
    message("----------------------------------------------")
    message(" warning: remaining generated files!")
    message("   ${CMAKE_SOURCE_DIR}/CMakeCache.txt")
    message("   ${CMAKE_SOURCE_DIR}/CMakeFiles")
    message("----------------------------------------------")
    message("")
    message(" Please remove remaining generated files ")
    message(" and run cmake from an appropriate location")
    message("")

    message(FATAL_ERROR  "\n** CMake aborted **\n")
  endif()
endif()
