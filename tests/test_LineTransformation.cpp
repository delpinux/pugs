#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/LineTransformation.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("LineTransformation", "[geometry]")
{
  SECTION("1D")
  {
    using R1 = TinyVector<1>;

    const R1 a{0.3};
    const R1 b{2.7};

    const LineTransformation<1> t(a, b);

    REQUIRE(t(R1{-1})[0] == Catch::Approx(0.3));
    REQUIRE(t(R1{0})[0] == Catch::Approx(1.5));
    REQUIRE(t(R1{1})[0] == Catch::Approx(2.7));

    REQUIRE(t.jacobianDeterminant() == Catch::Approx(1.2));

    auto p = [](const R1& X) {
      const double x = X[0];
      return 2 * x * x + 3 * x + 2;
    };

    QuadratureFormula<1> qf = QuadratureManager::instance().getLineFormula(GaussQuadratureDescriptor(2));

    double sum = 0;
    for (size_t i = 0; i < qf.numberOfPoints(); ++i) {
      sum += qf.weight(i) * t.jacobianDeterminant() * p(t(qf.point(i)));
    }

    auto P = [](const R1& X) {
      const double x = X[0];
      return 2. / 3 * x * x * x + 3. / 2 * x * x + 2 * x;
    };

    REQUIRE(sum == Catch::Approx(P(b) - P(a)));
  }

  SECTION("2D")
  {
    using R1 = TinyVector<1>;
    using R2 = TinyVector<2>;

    const R2 a{0.3, 1.2};
    const R2 b{2.7, 0.7};

    const LineTransformation<2> t(a, b);

    REQUIRE(t(R1{-1})[0] == Catch::Approx(0.3));
    REQUIRE(t(R1{-1})[1] == Catch::Approx(1.2));
    REQUIRE(t(R1{0})[0] == Catch::Approx(1.5));
    REQUIRE(t(R1{0})[1] == Catch::Approx(0.95));
    REQUIRE(t(R1{1})[0] == Catch::Approx(2.7));
    REQUIRE(t(R1{1})[1] == Catch::Approx(0.7));

    REQUIRE(t.velocityNorm() == Catch::Approx(l2Norm(0.5 * (b - a))));

    auto p = [](const R2& X) {
      const double x = X[0];
      const double y = X[1];
      return 2 * x * x + 3 * x - 3 * y * y + y + 2;
    };

    QuadratureFormula<1> qf = QuadratureManager::instance().getLineFormula(GaussQuadratureDescriptor(2));

    double sum = 0;
    for (size_t i = 0; i < qf.numberOfPoints(); ++i) {
      sum += qf.weight(i) * t.velocityNorm() * p(t(qf.point(i)));
    }

    REQUIRE(sum == Catch::Approx(24.8585155630822));
  }

  SECTION("3D")
  {
    using R1 = TinyVector<1>;
    using R3 = TinyVector<3>;

    const R3 a{0.3, 1.2, -1};
    const R3 b{2.7, 0.7, 0.3};

    const LineTransformation<3> t(a, b);

    REQUIRE(t(R1{-1})[0] == Catch::Approx(0.3));
    REQUIRE(t(R1{-1})[1] == Catch::Approx(1.2));
    REQUIRE(t(R1{-1})[2] == Catch::Approx(-1.));
    REQUIRE(t(R1{0})[0] == Catch::Approx(1.5));
    REQUIRE(t(R1{0})[1] == Catch::Approx(0.95));
    REQUIRE(t(R1{0})[2] == Catch::Approx(-0.35));
    REQUIRE(t(R1{1})[0] == Catch::Approx(2.7));
    REQUIRE(t(R1{1})[1] == Catch::Approx(0.7));
    REQUIRE(t(R1{1})[2] == Catch::Approx(0.3));

    REQUIRE(t.velocityNorm() == Catch::Approx(l2Norm(0.5 * (b - a))));

    auto p = [](const R3& X) {
      const double x = X[0];
      const double y = X[1];
      const double z = X[2];
      return 2 * x * x + 3 * x - 3 * y * y + y + 2 * z * z - 0.5 * z + 2;
    };

    QuadratureFormula<1> qf = QuadratureManager::instance().getLineFormula(GaussQuadratureDescriptor(2));

    double sum = 0;
    for (size_t i = 0; i < qf.numberOfPoints(); ++i) {
      sum += qf.weight(i) * t.velocityNorm() * p(t(qf.point(i)));
    }

    REQUIRE(sum == Catch::Approx(30.08440406681767));
  }
}
