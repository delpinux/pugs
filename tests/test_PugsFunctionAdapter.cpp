#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeFunctionEvaluationExpressionBuilder.hpp>
#include <language/ast/ASTNodeFunctionExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/PugsFunctionAdapter.hpp>
#include <language/utils/SymbolTable.hpp>

#include <pegtl/string_input.hpp>

// clazy:excludeall=non-pod-global-static

namespace tests_adapter
{
template <typename T>
class TestBinary;
template <typename OutputType, typename... InputType>
class TestBinary<OutputType(InputType...)> : public PugsFunctionAdapter<OutputType(InputType...)>
{
  using Adapter = PugsFunctionAdapter<OutputType(InputType...)>;

 public:
  template <typename ArgT>
  static auto
  one_arg(const FunctionSymbolId& function_symbol_id, const ArgT& x)
  {
    auto& expression    = Adapter::getFunctionExpression(function_symbol_id);
    auto convert_result = Adapter::getResultConverter(expression.m_data_type);

    auto context_list = Adapter::getContextList(expression);

    auto& execution_policy = context_list[0];

    Adapter::convertArgs(execution_policy.currentContext(), x);
    auto result = expression.execute(execution_policy);

    return convert_result(std::move(result));
  }

  template <typename Arg1T, typename Arg2T>
  static auto
  two_args(const FunctionSymbolId& function_symbol_id, const Arg1T& x, const Arg2T& y)
  {
    auto& expression    = Adapter::getFunctionExpression(function_symbol_id);
    auto convert_result = Adapter::getResultConverter(expression.m_data_type);

    auto context_list = Adapter::getContextList(expression);

    auto& execution_policy = context_list[0];

    Adapter::convertArgs(execution_policy.currentContext(), x, y);
    auto result = expression.execute(execution_policy);

    return convert_result(std::move(result));
  }
};
}   // namespace tests_adapter

TEST_CASE("PugsFunctionAdapter", "[language]")
{
  SECTION("Valid calls")
  {
    std::string_view data = R"(
let Rtimes2: R -> R, x -> 2*x;
let BandB: B*B -> B, (a,b) -> a and b;
let NplusN: N*N -> N, (x,y) -> x+y;
let ZplusZ: Z*Z -> Z, (x,y) -> x+y;
let RplusR: R*R -> R, (x,y) -> x+y;
let RRtoR2: R*R -> R^2, (x,y) -> [x+y, x-y];
let R3times2: R^3 -> R^3, x -> 2*x;
let R33times2: R^3x3 -> R^3x3, x -> 2*x;
let BtoR1: B -> R^1, b -> not b;
let BtoR11: B -> R^1x1, b -> not b;
let NtoR1: N -> R^1, n -> n*n;
let NtoR11: N -> R^1x1, n -> n*n;
let ZtoR1: Z -> R^1, z -> -z;
let ZtoR11: Z -> R^1x1, z -> -z;
let RtoR1: R -> R^1, x -> x*x;
let RtoR11: R -> R^1x1, x -> x*x;
let R3toR3zero: R^3 -> R^3, x -> 0;
let R33toR33zero: R^3x3 -> R^3x3, x -> 0;
)";
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};

    auto ast = ASTBuilder::build(input);

    ASTExecutionStack::create();

    ASTModulesImporter{*ast};
    ASTNodeTypeCleaner<language::import_instruction>{*ast};

    ASTSymbolTableBuilder{*ast};
    ASTNodeDataTypeBuilder{*ast};

    ASTNodeTypeCleaner<language::var_declaration>{*ast};
    ASTNodeTypeCleaner<language::fct_declaration>{*ast};
    ASTNodeExpressionBuilder{*ast};

    std::shared_ptr<SymbolTable> symbol_table = ast->m_symbol_table;

    // ensure that variables are declared at this point
    TAO_PEGTL_NAMESPACE::position position{data.size(), 1, 1, "fixture"};

    {
      auto [i_symbol, found] = symbol_table->find("Rtimes2", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const double x = 2;
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      double result = tests_adapter::TestBinary<double(double)>::one_arg(function_symbol_id, x);

      REQUIRE(result == (2 * x));
    }

    {
      auto [i_symbol, found] = symbol_table->find("BandB", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const bool a = true;
      const bool b = false;
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      double result = tests_adapter::TestBinary<bool(bool, bool)>::two_args(function_symbol_id, a, b);

      REQUIRE(result == (a and b));
    }

    {
      auto [i_symbol, found] = symbol_table->find("NplusN", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const uint64_t x = 2;
      const uint64_t y = 3;
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      double result = tests_adapter::TestBinary<uint64_t(uint64_t, uint64_t)>::two_args(function_symbol_id, x, y);

      REQUIRE(result == (x + y));
    }

    {
      auto [i_symbol, found] = symbol_table->find("ZplusZ", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const int64_t x = 2;
      const int64_t y = 3;
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      double result = tests_adapter::TestBinary<int64_t(int64_t, int64_t)>::two_args(function_symbol_id, x, y);

      REQUIRE(result == (x + y));
    }

    {
      auto [i_symbol, found] = symbol_table->find("RplusR", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const double x = 2;
      const double y = 3;
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      double result = tests_adapter::TestBinary<double(double, double)>::two_args(function_symbol_id, x, y);

      REQUIRE(result == (x + y));
    }

    {
      auto [i_symbol, found] = symbol_table->find("RRtoR2", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const double x = 2;
      const double y = 3;

      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      TinyVector<2> result =
        tests_adapter::TestBinary<TinyVector<2>(double, double)>::two_args(function_symbol_id, x, y);

      REQUIRE(result == TinyVector<2>{x + y, x - y});
    }

    {
      auto [i_symbol, found] = symbol_table->find("R3times2", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const TinyVector<3> x{2, 3, 4};

      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      TinyVector<3> result = tests_adapter::TestBinary<TinyVector<3>(TinyVector<3>)>::one_arg(function_symbol_id, x);

      REQUIRE(result == 2 * x);
    }

    {
      auto [i_symbol, found] = symbol_table->find("R33times2", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const TinyMatrix<3> x{2, 3, 4, 1, 6, 5, 9, 7, 8};

      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      TinyMatrix<3> result = tests_adapter::TestBinary<TinyMatrix<3>(TinyMatrix<3>)>::one_arg(function_symbol_id, x);

      REQUIRE(result == 2 * x);
    }

    {
      auto [i_symbol, found] = symbol_table->find("BtoR1", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      {
        const bool b = true;

        FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
        TinyVector<1> result = tests_adapter::TestBinary<TinyVector<1>(bool)>::one_arg(function_symbol_id, b);

        REQUIRE(result == TinyVector<1>{not b});
      }

      {
        const bool b = false;

        FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
        TinyVector<1> result = tests_adapter::TestBinary<TinyVector<1>(bool)>::one_arg(function_symbol_id, b);

        REQUIRE(result == TinyVector<1>{not b});
      }
    }

    {
      auto [i_symbol, found] = symbol_table->find("BtoR11", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      {
        const bool b = true;

        FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
        TinyMatrix<1> result = tests_adapter::TestBinary<TinyMatrix<1>(bool)>::one_arg(function_symbol_id, b);

        REQUIRE(result == TinyMatrix<1>{not b});
      }

      {
        const bool b = false;

        FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
        TinyMatrix<1> result = tests_adapter::TestBinary<TinyMatrix<1>(bool)>::one_arg(function_symbol_id, b);

        REQUIRE(result == TinyMatrix<1>{not b});
      }
    }

    {
      auto [i_symbol, found] = symbol_table->find("NtoR1", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const uint64_t n = 4;

      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      TinyVector<1> result = tests_adapter::TestBinary<TinyVector<1>(uint64_t)>::one_arg(function_symbol_id, n);

      REQUIRE(result == TinyVector<1>{n * n});
    }

    {
      auto [i_symbol, found] = symbol_table->find("NtoR11", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const uint64_t n = 4;

      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      TinyMatrix<1> result = tests_adapter::TestBinary<TinyMatrix<1>(uint64_t)>::one_arg(function_symbol_id, n);

      REQUIRE(result == TinyMatrix<1>{n * n});
    }

    {
      auto [i_symbol, found] = symbol_table->find("ZtoR1", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const int64_t z = 3;

      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      TinyVector<1> result = tests_adapter::TestBinary<TinyVector<1>(int64_t)>::one_arg(function_symbol_id, z);

      REQUIRE(result == TinyVector<1>{-z});
    }

    {
      auto [i_symbol, found] = symbol_table->find("ZtoR11", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const int64_t z = 3;

      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      TinyMatrix<1> result = tests_adapter::TestBinary<TinyMatrix<1>(int64_t)>::one_arg(function_symbol_id, z);

      REQUIRE(result == TinyMatrix<1>{-z});
    }

    {
      auto [i_symbol, found] = symbol_table->find("RtoR1", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const double x = 3.3;

      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      TinyVector<1> result = tests_adapter::TestBinary<TinyVector<1>(double)>::one_arg(function_symbol_id, x);

      REQUIRE(result == TinyVector<1>{x * x});
    }

    {
      auto [i_symbol, found] = symbol_table->find("RtoR11", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const double x = 3.3;

      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      TinyMatrix<1> result = tests_adapter::TestBinary<TinyMatrix<1>(double)>::one_arg(function_symbol_id, x);

      REQUIRE(result == TinyMatrix<1>{x * x});
    }

    {
      auto [i_symbol, found] = symbol_table->find("R3toR3zero", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const TinyVector<3> x{1, 1, 1};

      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      TinyVector<3> result = tests_adapter::TestBinary<TinyVector<3>(TinyVector<3>)>::one_arg(function_symbol_id, x);

      REQUIRE(result == TinyVector<3>{0, 0, 0});
    }

    {
      auto [i_symbol, found] = symbol_table->find("R33toR33zero", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const TinyMatrix<3> x{1, 0, 0, 0, 1, 0, 0, 0, 1};

      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);
      TinyMatrix<3> result = tests_adapter::TestBinary<TinyMatrix<3>(TinyMatrix<3>)>::one_arg(function_symbol_id, x);

      REQUIRE(result == TinyMatrix<3>{0, 0, 0, 0, 0, 0, 0, 0, 0});
    }

    ASTExecutionStack::destroy();
    ast->m_symbol_table->clearValues();
  }

  SECTION("Errors calls")
  {
    std::string_view data = R"(
let R1toR1: R^1 -> R^1, x -> x;
let R3toR3: R^3 -> R^3, x -> 0;
let RRRtoR3: R*R*R -> R^3, (x,y,z) -> [x,y,z];
let R3toR2: R^3 -> R^2, x -> [x[0],x[1]+x[2]];
let RtoNS: R -> N*string, x -> (1, "foo");
let RtoR: R -> R, x -> 2*x;
let R33toR22: R^3x3 -> R^2x2, x -> [[x[0,0], x[0,1]+x[0,2]], [x[2,0]*x[1,1], x[2,1]+x[2,2]]];
)";
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};

    auto ast = ASTBuilder::build(input);

    ASTExecutionStack::create();

    ASTModulesImporter{*ast};
    ASTNodeTypeCleaner<language::import_instruction>{*ast};

    ASTSymbolTableBuilder{*ast};
    ASTNodeDataTypeBuilder{*ast};

    ASTNodeTypeCleaner<language::var_declaration>{*ast};
    ASTNodeTypeCleaner<language::fct_declaration>{*ast};
    ASTNodeExpressionBuilder{*ast};

    std::shared_ptr<SymbolTable> symbol_table = ast->m_symbol_table;

    // ensure that variables are declared at this point
    TAO_PEGTL_NAMESPACE::position position{data.size(), 1, 1, "fixture"};

    {
      auto [i_symbol, found] = symbol_table->find("R1toR1", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const TinyVector<1> x{2};
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

      REQUIRE_THROWS_WITH(tests_adapter::TestBinary<double(TinyVector<1>)>::one_arg(function_symbol_id, x),
                          "error: invalid function type\n"
                          "note: expecting R^1 -> R\n"
                          "note: provided function R1toR1: R^1 -> R^1");
    }

    {
      auto [i_symbol, found] = symbol_table->find("R3toR3", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const TinyVector<3> x{2, 1, 3};
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

      REQUIRE_THROWS_WITH(tests_adapter::TestBinary<TinyVector<2>(TinyVector<3>)>::one_arg(function_symbol_id, x),
                          "error: invalid function type\n"
                          "note: expecting R^3 -> R^2\n"
                          "note: provided function R3toR3: R^3 -> R^3");
    }

    {
      auto [i_symbol, found] = symbol_table->find("RRRtoR3", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const double x = 1;
      const double y = 2;
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

      REQUIRE_THROWS_WITH(tests_adapter::TestBinary<TinyVector<3>(double, double)>::two_args(function_symbol_id, x, y),
                          "error: invalid function type\n"
                          "note: expecting R*R -> R^3\n"
                          "note: provided function RRRtoR3: R*R*R -> R^3");
    }

    {
      auto [i_symbol, found] = symbol_table->find("R3toR2", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const double x = 1;
      const double y = 2;
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

      REQUIRE_THROWS_WITH(tests_adapter::TestBinary<TinyVector<3>(double, double)>::two_args(function_symbol_id, x, y),
                          "error: invalid function type\n"
                          "note: expecting R*R -> R^3\n"
                          "note: provided function R3toR2: R^3 -> R^2");
    }

    {
      auto [i_symbol, found] = symbol_table->find("RtoNS", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const double x = 1;
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

      REQUIRE_THROWS_WITH(tests_adapter::TestBinary<TinyVector<2>(double)>::one_arg(function_symbol_id, x),
                          "error: invalid function type\n"
                          "note: expecting R -> R^2\n"
                          "note: provided function RtoNS: R -> N*string");
    }

    {
      auto [i_symbol, found] = symbol_table->find("RtoR", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const double x = 1;
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

      REQUIRE_THROWS_WITH(tests_adapter::TestBinary<TinyVector<3>(double)>::one_arg(function_symbol_id, x),
                          "error: invalid function type\n"
                          "note: expecting R -> R^3\n"
                          "note: provided function RtoR: R -> R");
    }

    {
      auto [i_symbol, found] = symbol_table->find("R33toR22", position);
      REQUIRE(found);
      REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

      const double x = 1;
      FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

      REQUIRE_THROWS_WITH(tests_adapter::TestBinary<double(double)>::one_arg(function_symbol_id, x),
                          "error: invalid function type\n"
                          "note: expecting R -> R\n"
                          "note: provided function R33toR22: R^3x3 -> R^2x2");
    }

    ASTExecutionStack::destroy();
    ast->m_symbol_table->clearValues();
  }
}
