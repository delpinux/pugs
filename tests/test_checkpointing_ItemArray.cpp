#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Messenger.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshVariant.hpp>
#include <utils/checkpointing/ReadItemArray.hpp>
#include <utils/checkpointing/WriteItemArray.hpp>

#include <MeshDataBaseForTests.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_ItemArray", "[utils/checkpointing]")
{
  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});
    HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

    SECTION("ItemArray")
    {
      HighFive::Group checkpoint_group = file.createGroup("checkpoint_group");
      HighFive::Group useless_group;

      auto mesh_2d = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>();

      CellArray<double> item_array{mesh_2d->connectivity(), 3};
      item_array.fill(0);

      for (CellId cell_id = 0; cell_id < mesh_2d->numberOfCells(); ++cell_id) {
        for (size_t i = 0; i < 3; ++i) {
          item_array[cell_id][i] = (1. * std::rand()) / (1. / 3 * RAND_MAX);
        }
      }

      checkpointing::write(checkpoint_group, "item_array", item_array);

      file.flush();

      auto is_same = [](const auto& a, const auto& b) {
        bool same = true;
        for (size_t i = 0; i < a.numberOfRows(); ++i) {
          for (size_t j = 0; j < a.numberOfColumns(); ++j) {
            if (a[i][j] != b[i][j]) {
              same = false;
            }
          }
        }
        return parallel::allReduceAnd(same);
      };

      CellArray<double> read_item_array =
        checkpointing::readItemArray<double, ItemType::cell>(checkpoint_group, "item_array", mesh_2d->connectivity());
      REQUIRE(is_same(item_array.tableView(), read_item_array.tableView()));
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }
}
