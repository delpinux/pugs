#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Messenger.hpp>

#include <analysis/GaussLegendreQuadratureDescriptor.hpp>
#include <analysis/GaussLobattoQuadratureDescriptor.hpp>
#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/IQuadratureDescriptor.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <utils/checkpointing/ReadIQuadratureDescriptor.hpp>
#include <utils/checkpointing/WriteIQuadratureDescriptor.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_IQuadratureDescriptor", "[utils/checkpointing]")
{
  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});
    HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

    SECTION("IQuadratureDescriptor")
    {
      HighFive::Group symbol_table_group = file.createGroup("symbol_table");
      HighFive::Group useless_group;

      auto p_gauss_quadrature_descriptor = std::make_shared<GaussQuadratureDescriptor>(3);
      checkpointing::writeIQuadratureDescriptor("gauss",
                                                EmbeddedData{std::make_shared<DataHandler<const IQuadratureDescriptor>>(
                                                  p_gauss_quadrature_descriptor)},
                                                file, useless_group, symbol_table_group);

      auto p_gauss_legendre_quadrature_descriptor = std::make_shared<GaussLegendreQuadratureDescriptor>(7);
      checkpointing::writeIQuadratureDescriptor("gauss_legendre",
                                                EmbeddedData{std::make_shared<DataHandler<const IQuadratureDescriptor>>(
                                                  p_gauss_legendre_quadrature_descriptor)},
                                                file, useless_group, symbol_table_group);

      auto p_gauss_lobatto_quadrature_descriptor = std::make_shared<GaussLobattoQuadratureDescriptor>(2);
      checkpointing::writeIQuadratureDescriptor("gauss_lobatto",
                                                EmbeddedData{std::make_shared<DataHandler<const IQuadratureDescriptor>>(
                                                  p_gauss_lobatto_quadrature_descriptor)},
                                                file, useless_group, symbol_table_group);

      file.flush();

      EmbeddedData read_gauss_quadrature_descriptor =
        checkpointing::readIQuadratureDescriptor("gauss", symbol_table_group);

      EmbeddedData read_gauss_legendre_quadrature_descriptor =
        checkpointing::readIQuadratureDescriptor("gauss_legendre", symbol_table_group);

      EmbeddedData read_gauss_lobatto_quadrature_descriptor =
        checkpointing::readIQuadratureDescriptor("gauss_lobatto", symbol_table_group);

      auto get_value = [](const EmbeddedData& embedded_data) -> const IQuadratureDescriptor& {
        return *dynamic_cast<const DataHandler<const IQuadratureDescriptor>&>(embedded_data.get()).data_ptr();
      };

      REQUIRE_NOTHROW(get_value(read_gauss_quadrature_descriptor));
      REQUIRE_NOTHROW(get_value(read_gauss_legendre_quadrature_descriptor));
      REQUIRE_NOTHROW(get_value(read_gauss_lobatto_quadrature_descriptor));

      REQUIRE_NOTHROW(dynamic_cast<const GaussQuadratureDescriptor&>(get_value(read_gauss_quadrature_descriptor)));
      REQUIRE_NOTHROW(
        dynamic_cast<const GaussLegendreQuadratureDescriptor&>(get_value(read_gauss_legendre_quadrature_descriptor)));
      REQUIRE_NOTHROW(
        dynamic_cast<const GaussLobattoQuadratureDescriptor&>(get_value(read_gauss_lobatto_quadrature_descriptor)));

      REQUIRE(get_value(read_gauss_quadrature_descriptor).type() == QuadratureType::Gauss);
      REQUIRE(get_value(read_gauss_quadrature_descriptor).degree() == 3);
      REQUIRE(get_value(read_gauss_legendre_quadrature_descriptor).type() == QuadratureType::GaussLegendre);
      REQUIRE(get_value(read_gauss_legendre_quadrature_descriptor).degree() == 7);
      REQUIRE(get_value(read_gauss_lobatto_quadrature_descriptor).type() == QuadratureType::GaussLobatto);
      REQUIRE(get_value(read_gauss_lobatto_quadrature_descriptor).degree() == 2);
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }
}
