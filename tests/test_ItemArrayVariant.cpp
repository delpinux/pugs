#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemArrayVariant.hpp>
#include <mesh/Mesh.hpp>
#include <utils/Messenger.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ItemArrayVariant", "[mesh]")
{
  std::shared_ptr mesh_v = MeshDataBaseForTests::get().hybrid2DMesh();
  std::shared_ptr mesh   = mesh_v->get<Mesh<2>>();

  const Connectivity<2>& connectivity = *mesh->shared_connectivity();

  using R1 = TinyVector<1>;
  using R2 = TinyVector<2>;
  using R3 = TinyVector<3>;

  using R1x1 = TinyMatrix<1>;
  using R2x2 = TinyMatrix<2>;
  using R3x3 = TinyMatrix<3>;

  SECTION("NodeArray<double>")
  {
    NodeArray<double> node_array{connectivity, 2};
    ItemArrayVariant v(node_array);
    REQUIRE_NOTHROW(v.get<NodeArray<const double>>());
    REQUIRE_THROWS_WITH(v.get<NodeArray<int64_t>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<NodeArray<uint64_t>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<NodeArray<const R1>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<NodeArray<const R2>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<NodeArray<const R3>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<NodeArray<const R1x1>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<NodeArray<const R2x2>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<NodeArray<const R3x3>>(), "error: invalid ItemArray type");

    REQUIRE_THROWS_WITH(v.get<EdgeArray<const double>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<FaceArray<const double>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<CellArray<const double>>(), "error: invalid ItemArray type");
  }

  SECTION("EdgeArray<R3>")
  {
    EdgeArray<R3> node_array{connectivity, 3};
    ItemArrayVariant v(node_array);
    REQUIRE_THROWS_WITH(v.get<EdgeArray<int64_t>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<EdgeArray<uint64_t>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<EdgeArray<double>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<EdgeArray<const R1>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<EdgeArray<const R2>>(), "error: invalid ItemArray type");
    REQUIRE_NOTHROW(v.get<EdgeArray<const R3>>());
    REQUIRE_THROWS_WITH(v.get<EdgeArray<const R1x1>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<EdgeArray<const R2x2>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<EdgeArray<const R3x3>>(), "error: invalid ItemArray type");

    REQUIRE_THROWS_WITH(v.get<NodeArray<const R3>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<FaceArray<const R3>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<CellArray<const R3>>(), "error: invalid ItemArray type");
  }

  SECTION("FaceArray<R3x3>")
  {
    FaceArray<R3x3> node_array{connectivity, 2};
    ItemArrayVariant v(node_array);
    REQUIRE_THROWS_WITH(v.get<FaceArray<int64_t>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<FaceArray<uint64_t>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<FaceArray<double>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<FaceArray<const R1>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<FaceArray<const R2>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<FaceArray<const R3>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<FaceArray<const R1x1>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<FaceArray<const R2x2>>(), "error: invalid ItemArray type");
    REQUIRE_NOTHROW(v.get<FaceArray<const R3x3>>());

    REQUIRE_THROWS_WITH(v.get<NodeArray<const R3x3>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<EdgeArray<const R3x3>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<CellArray<const R3x3>>(), "error: invalid ItemArray type");
  }

  SECTION("CellArray<int64_t>")
  {
    CellArray<int64_t> node_array{connectivity, 2};
    ItemArrayVariant v(node_array);
    REQUIRE_NOTHROW(v.get<CellArray<const int64_t>>());
    REQUIRE_THROWS_WITH(v.get<CellArray<const uint64_t>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<CellArray<const double>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<CellArray<const R1>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<CellArray<const R2>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<CellArray<const R3>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<CellArray<const R1x1>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<CellArray<const R2x2>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<CellArray<const R3x3>>(), "error: invalid ItemArray type");

    REQUIRE_THROWS_WITH(v.get<NodeArray<const int64_t>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<EdgeArray<const int64_t>>(), "error: invalid ItemArray type");
    REQUIRE_THROWS_WITH(v.get<FaceArray<const int64_t>>(), "error: invalid ItemArray type");
  }
}
