#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshEdgeBoundary.hpp>
#include <mesh/NamedBoundaryDescriptor.hpp>
#include <mesh/NumberedBoundaryDescriptor.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("MeshEdgeBoundary", "[mesh]")
{
  auto is_same = [](const auto& a, const auto& b) -> bool {
    if (a.size() > 0 and b.size() > 0) {
      return (a.size() == b.size()) and (&(a[0]) == &(b[0]));
    } else {
      return (a.size() == b.size());
    }
  };

  auto get_edge_list_from_tag = [](const size_t tag, const auto& connectivity) -> Array<const EdgeId> {
    for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::edge>(); ++i) {
      const auto& ref_edge_list = connectivity.template refItemList<ItemType::edge>(i);
      const RefId ref_id        = ref_edge_list.refId();
      if (ref_id.tagNumber() == tag) {
        return ref_edge_list.list();
      }
    }
    return {};
  };

  auto get_edge_list_from_name = [](const std::string& name, const auto& connectivity) -> Array<const EdgeId> {
    for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::edge>(); ++i) {
      const auto& ref_edge_list = connectivity.template refItemList<ItemType::edge>(i);
      const RefId ref_id        = ref_edge_list.refId();
      if (ref_id.tagName() == name) {
        return ref_edge_list.list();
      }
    }
    return {};
  };

  SECTION("1D")
  {
    static constexpr size_t Dimension = 1;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    SECTION("cartesian 1d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().cartesian1DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {0, 1};

        for (auto tag : tag_set) {
          NumberedBoundaryDescriptor numbered_boundary_descriptor(tag);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, numbered_boundary_descriptor);

          auto edge_list = get_edge_list_from_tag(tag, connectivity);
          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }

      {
        const std::set<std::string> name_set = {"XMIN", "XMAX"};

        for (const auto& name : name_set) {
          NamedBoundaryDescriptor named_boundary_descriptor(name);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, named_boundary_descriptor);

          auto edge_list = get_edge_list_from_name(name, connectivity);

          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }
    }

    SECTION("unordered 1d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().unordered1DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {1, 2};

        for (auto tag : tag_set) {
          NumberedBoundaryDescriptor numbered_boundary_descriptor(tag);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, numbered_boundary_descriptor);

          auto edge_list = get_edge_list_from_tag(tag, connectivity);
          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }

      {
        const std::set<std::string> name_set = {"XMIN", "XMAX"};

        for (const auto& name : name_set) {
          NamedBoundaryDescriptor named_boundary_descriptor(name);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, named_boundary_descriptor);

          auto edge_list = get_edge_list_from_name(name, connectivity);
          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }
    }
  }

  SECTION("2D")
  {
    static constexpr size_t Dimension = 2;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    SECTION("cartesian 2d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().cartesian2DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {0, 1, 2, 3};

        for (auto tag : tag_set) {
          NumberedBoundaryDescriptor numbered_boundary_descriptor(tag);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, numbered_boundary_descriptor);

          auto edge_list = get_edge_list_from_tag(tag, connectivity);
          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }

      {
        const std::set<std::string> name_set = {"XMIN", "XMAX", "YMIN", "YMAX"};

        for (const auto& name : name_set) {
          NamedBoundaryDescriptor numbered_boundary_descriptor(name);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, numbered_boundary_descriptor);

          auto edge_list = get_edge_list_from_name(name, connectivity);
          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }
    }

    SECTION("hybrid 2d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid2DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {1, 2, 3, 4};

        for (auto tag : tag_set) {
          NumberedBoundaryDescriptor numbered_boundary_descriptor(tag);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, numbered_boundary_descriptor);

          auto edge_list = get_edge_list_from_tag(tag, connectivity);
          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }

      {
        const std::set<std::string> name_set = {"XMIN", "YMIN", "XMAX", "YMIN"};

        for (const auto& name : name_set) {
          NamedBoundaryDescriptor numbered_boundary_descriptor(name);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, numbered_boundary_descriptor);

          auto edge_list = get_edge_list_from_name(name, connectivity);
          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }
    }
  }

  SECTION("3D")
  {
    static constexpr size_t Dimension = 3;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    SECTION("cartesian 3d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().cartesian3DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {0, 1, 2, 3, 4, 5};

        for (auto tag : tag_set) {
          NumberedBoundaryDescriptor numbered_boundary_descriptor(tag);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, numbered_boundary_descriptor);

          auto edge_list = get_edge_list_from_tag(tag, connectivity);
          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }

      {
        const std::set<std::string> name_set = {"XMIN", "XMAX", "YMIN", "YMAX", "ZMIN", "ZMAX"};

        for (const auto& name : name_set) {
          NamedBoundaryDescriptor numbered_boundary_descriptor(name);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, numbered_boundary_descriptor);

          auto edge_list = get_edge_list_from_name(name, connectivity);
          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }
    }

    SECTION("hybrid 3d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid3DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {22, 23, 24, 25, 26, 27};

        for (auto tag : tag_set) {
          NumberedBoundaryDescriptor numbered_boundary_descriptor(tag);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, numbered_boundary_descriptor);

          auto edge_list = get_edge_list_from_tag(tag, connectivity);
          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }

      {
        const std::set<std::string> name_set = {"XMIN", "XMAX", "YMIN", "YMAX", "ZMIN", "ZMAX"};

        for (const auto& name : name_set) {
          NamedBoundaryDescriptor numbered_boundary_descriptor(name);
          const auto& edge_boundary = getMeshEdgeBoundary(mesh, numbered_boundary_descriptor);

          auto edge_list = get_edge_list_from_name(name, connectivity);
          REQUIRE(is_same(edge_boundary.edgeList(), edge_list));
        }
      }
    }
  }

  SECTION("errors")
  {
    SECTION("cannot find boundary")
    {
      static constexpr size_t Dimension = 3;

      using MeshType = Mesh<Dimension>;

      std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid3DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      NamedBoundaryDescriptor named_boundary_descriptor("invalid_boundary");

      REQUIRE_THROWS_WITH(getMeshEdgeBoundary(mesh, named_boundary_descriptor),
                          "error: cannot find edge list with name \"invalid_boundary\"");
    }

    SECTION("surface is inside")
    {
      SECTION("1D")
      {
        static constexpr size_t Dimension = 1;

        using MeshType = Mesh<Dimension>;

        std::shared_ptr p_mesh = MeshDataBaseForTests::get().unordered1DMesh();
        const MeshType& mesh   = *p_mesh->get<MeshType>();

        NamedBoundaryDescriptor named_boundary_descriptor("INTERFACE");

        REQUIRE_THROWS_WITH(getMeshEdgeBoundary(mesh, named_boundary_descriptor),
                            "error: invalid boundary \"INTERFACE\": inner edges cannot be used to define mesh "
                            "boundaries");
      }

      SECTION("2D")
      {
        static constexpr size_t Dimension = 2;

        using MeshType = Mesh<Dimension>;

        std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid2DMesh();
        const MeshType& mesh   = *p_mesh->get<MeshType>();

        NamedBoundaryDescriptor named_boundary_descriptor("INTERFACE");

        REQUIRE_THROWS_WITH(getMeshEdgeBoundary(mesh, named_boundary_descriptor),
                            "error: invalid boundary \"INTERFACE\": inner edges cannot be used to define mesh "
                            "boundaries");
      }

      SECTION("3D")
      {
        static constexpr size_t Dimension = 3;

        using MeshType = Mesh<Dimension>;

        std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid3DMesh();
        const MeshType& mesh   = *p_mesh->get<MeshType>();

        NamedBoundaryDescriptor named_boundary_descriptor("INTERFACE1");

        REQUIRE_THROWS_WITH(getMeshEdgeBoundary(mesh, named_boundary_descriptor),
                            "error: invalid boundary \"INTERFACE1\": inner edges cannot be used to define mesh "
                            "boundaries");
      }
    }
  }
}
