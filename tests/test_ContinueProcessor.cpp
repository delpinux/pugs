#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/node_processor/ContinueProcessor.hpp>

#include <rang.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ContinueProcessor", "[language]")
{
  rang::setControlMode(rang::control::Off);

  ExecutionPolicy exec_policy;

  REQUIRE(exec_policy.exec() == true);

  ContinueProcessor continue_processor;
  continue_processor.execute(exec_policy);

  REQUIRE(exec_policy.exec() == false);
  REQUIRE(exec_policy.jumpType() == ExecutionPolicy::JumpType::continue_jump);
}
