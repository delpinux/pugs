#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshNodeInterface.hpp>
#include <mesh/NamedInterfaceDescriptor.hpp>
#include <mesh/NumberedInterfaceDescriptor.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("MeshNodeInterface", "[mesh]")
{
  auto is_same = [](const auto& a, const auto& b) -> bool {
    if (a.size() > 0 and b.size() > 0) {
      return (a.size() == b.size()) and (&(a[0]) == &(b[0]));
    } else {
      return (a.size() == b.size());
    }
  };

  auto get_node_list_from_tag = [](const size_t tag, const auto& connectivity) -> Array<const NodeId> {
    for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::node>(); ++i) {
      const auto& ref_node_list = connectivity.template refItemList<ItemType::node>(i);
      const RefId ref_id        = ref_node_list.refId();
      if (ref_id.tagNumber() == tag) {
        return ref_node_list.list();
      }
    }
    return {};
  };

  auto get_node_list_from_name = [](const std::string& name, const auto& connectivity) -> Array<const NodeId> {
    for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::node>(); ++i) {
      const auto& ref_node_list = connectivity.template refItemList<ItemType::node>(i);
      const RefId ref_id        = ref_node_list.refId();
      if (ref_id.tagName() == name) {
        return ref_node_list.list();
      }
    }
    return {};
  };

  SECTION("1D")
  {
    static constexpr size_t Dimension = 1;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    SECTION("unordered 1d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().unordered1DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {3};

        for (auto tag : tag_set) {
          NumberedInterfaceDescriptor numbered_interface_descriptor(tag);
          const auto& node_interface = getMeshNodeInterface(mesh, numbered_interface_descriptor);

          auto node_list = get_node_list_from_tag(tag, connectivity);
          REQUIRE(is_same(node_interface.nodeList(), node_list));
        }
      }

      {
        const std::set<std::string> name_set = {"INTERFACE"};

        for (const auto& name : name_set) {
          NamedInterfaceDescriptor named_interface_descriptor(name);
          const auto& node_interface = getMeshNodeInterface(mesh, named_interface_descriptor);

          auto node_list = get_node_list_from_name(name, connectivity);
          REQUIRE(is_same(node_interface.nodeList(), node_list));
        }
      }
    }
  }

  SECTION("2D")
  {
    static constexpr size_t Dimension = 2;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    SECTION("hybrid 2d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid2DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {5};

        for (auto tag : tag_set) {
          NumberedInterfaceDescriptor numbered_interface_descriptor(tag);
          const auto& node_interface = getMeshNodeInterface(mesh, numbered_interface_descriptor);

          auto node_list = get_node_list_from_tag(tag, connectivity);
          REQUIRE(is_same(node_interface.nodeList(), node_list));
        }
      }

      {
        const std::set<std::string> name_set = {"INTERFACE"};

        for (const auto& name : name_set) {
          NamedInterfaceDescriptor numbered_interface_descriptor(name);
          const auto& node_interface = getMeshNodeInterface(mesh, numbered_interface_descriptor);

          auto node_list = get_node_list_from_name(name, connectivity);
          REQUIRE(is_same(node_interface.nodeList(), node_list));
        }
      }
    }
  }

  SECTION("3D")
  {
    static constexpr size_t Dimension = 3;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    SECTION("hybrid 3d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid3DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {55, 56};

        for (auto tag : tag_set) {
          NumberedInterfaceDescriptor numbered_interface_descriptor(tag);
          const auto& node_interface = getMeshNodeInterface(mesh, numbered_interface_descriptor);

          auto node_list = get_node_list_from_tag(tag, connectivity);
          REQUIRE(is_same(node_interface.nodeList(), node_list));
        }
      }

      {
        const std::set<std::string> name_set = {"INTERFACE1", "INTERFACE2"};

        for (const auto& name : name_set) {
          NamedInterfaceDescriptor numbered_interface_descriptor(name);
          const auto& node_interface = getMeshNodeInterface(mesh, numbered_interface_descriptor);

          auto node_list = get_node_list_from_name(name, connectivity);
          REQUIRE(is_same(node_interface.nodeList(), node_list));
        }
      }
    }
  }

  SECTION("errors")
  {
    SECTION("cannot find interface")
    {
      static constexpr size_t Dimension = 3;

      using MeshType = Mesh<Dimension>;

      std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid3DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      NamedInterfaceDescriptor named_interface_descriptor("invalid_interface");

      REQUIRE_THROWS_WITH(getMeshNodeInterface(mesh, named_interface_descriptor),
                          "error: cannot find node list with name \"invalid_interface\"");
    }

    SECTION("surface is inside")
    {
      SECTION("1D")
      {
        static constexpr size_t Dimension = 1;

        using MeshType = Mesh<Dimension>;

        std::shared_ptr p_mesh = MeshDataBaseForTests::get().unordered1DMesh();
        const MeshType& mesh   = *p_mesh->get<MeshType>();

        NamedInterfaceDescriptor named_interface_descriptor("XMIN");

        REQUIRE_THROWS_WITH(getMeshNodeInterface(mesh, named_interface_descriptor),
                            "error: invalid interface \"XMIN(1)\": boundary nodes cannot be used to define mesh "
                            "interfaces");
      }

      SECTION("2D")
      {
        static constexpr size_t Dimension = 2;

        using MeshType = Mesh<Dimension>;

        std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid2DMesh();
        const MeshType& mesh   = *p_mesh->get<MeshType>();

        NamedInterfaceDescriptor named_interface_descriptor("XMIN");

        REQUIRE_THROWS_WITH(getMeshNodeInterface(mesh, named_interface_descriptor),
                            "error: invalid interface \"XMIN(1)\": boundary nodes cannot be used to define mesh "
                            "interfaces");
      }

      SECTION("3D")
      {
        static constexpr size_t Dimension = 3;

        using MeshType = Mesh<Dimension>;

        std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid3DMesh();
        const MeshType& mesh   = *p_mesh->get<MeshType>();

        NamedInterfaceDescriptor named_interface_descriptor("ZMAX");

        REQUIRE_THROWS_WITH(getMeshNodeInterface(mesh, named_interface_descriptor),
                            Catch::Matchers::StartsWith("error: invalid interface \"ZMAX(24)\": boundary"));
      }
    }
  }
}
