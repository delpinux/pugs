#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/EmbeddedDiscreteFunctionUtils.hpp>
#include <scheme/DiscreteFunctionP0.hpp>
#include <scheme/DiscreteFunctionP0Vector.hpp>

#include <MeshDataBaseForTests.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("EmbeddedDiscreteFunctionUtils", "[language]")
{
  using R1 = TinyVector<1, double>;
  using R2 = TinyVector<2, double>;
  using R3 = TinyVector<3, double>;

  using R1x1 = TinyMatrix<1, 1, double>;
  using R2x2 = TinyMatrix<2, 2, double>;
  using R3x3 = TinyMatrix<3, 3, double>;

  SECTION("operand type name")
  {
    SECTION("basic types")
    {
      REQUIRE(EmbeddedDiscreteFunctionUtils::getOperandTypeName(double{1}) == "R");
      REQUIRE(EmbeddedDiscreteFunctionUtils::getOperandTypeName(std::make_shared<double>(1)) == "R");
    }

    SECTION("discrete P0 function")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d = named_mesh.mesh()->get<Mesh<1>>();

          REQUIRE(EmbeddedDiscreteFunctionUtils::getOperandTypeName(DiscreteFunctionP0<double>{mesh_1d}) == "Vh(P0:R)");

          REQUIRE(EmbeddedDiscreteFunctionUtils::getOperandTypeName(DiscreteFunctionP0<R1>{mesh_1d}) == "Vh(P0:R^1)");
          REQUIRE(EmbeddedDiscreteFunctionUtils::getOperandTypeName(DiscreteFunctionP0<R2>{mesh_1d}) == "Vh(P0:R^2)");
          REQUIRE(EmbeddedDiscreteFunctionUtils::getOperandTypeName(DiscreteFunctionP0<R3>{mesh_1d}) == "Vh(P0:R^3)");

          REQUIRE(EmbeddedDiscreteFunctionUtils::getOperandTypeName(DiscreteFunctionP0<R1x1>{mesh_1d}) ==
                  "Vh(P0:R^1x1)");
          REQUIRE(EmbeddedDiscreteFunctionUtils::getOperandTypeName(DiscreteFunctionP0<R2x2>{mesh_1d}) ==
                  "Vh(P0:R^2x2)");
          REQUIRE(EmbeddedDiscreteFunctionUtils::getOperandTypeName(DiscreteFunctionP0<R3x3>{mesh_1d}) ==
                  "Vh(P0:R^3x3)");
        }
      }
    }

    SECTION("discrete P0Vector function")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d = named_mesh.mesh()->get<Mesh<1>>();

          REQUIRE(EmbeddedDiscreteFunctionUtils::getOperandTypeName(DiscreteFunctionP0Vector<double>{mesh_1d, 2}) ==
                  "Vh(P0Vector:R)");
        }
      }
    }
  }

#ifndef NDEBUG
  SECTION("errors")
  {
    REQUIRE_THROWS_WITH(EmbeddedDiscreteFunctionUtils::getOperandTypeName(std::shared_ptr<double>()),
                        "dangling shared_ptr");
  }

#endif   // NDEBUG
}
