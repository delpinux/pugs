#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeAffectationExpressionBuilder.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/node_processor/ForProcessor.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_FOR_PROCESSOR_RESULT(data, variable_name, expected_value)       \
  {                                                                           \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                \
    auto ast = ASTBuilder::build(input);                                      \
                                                                              \
    ASTExecutionStack::create();                                              \
                                                                              \
    ASTModulesImporter{*ast};                                                 \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                   \
                                                                              \
    ASTSymbolTableBuilder{*ast};                                              \
    ASTNodeDataTypeBuilder{*ast};                                             \
                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                      \
                                                                              \
    ASTNodeExpressionBuilder{*ast};                                           \
    ExecutionPolicy exec_policy;                                              \
    ast->execute(exec_policy);                                                \
                                                                              \
    auto symbol_table = ast->m_symbol_table;                                  \
                                                                              \
    using namespace TAO_PEGTL_NAMESPACE;                                      \
    position use_position{10000, 1000, 10, "fixture"};                        \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);   \
                                                                              \
    auto attributes = symbol->attributes();                                   \
    auto value      = std::get<decltype(expected_value)>(attributes.value()); \
                                                                              \
    ASTExecutionStack::destroy();                                             \
                                                                              \
    REQUIRE(value == expected_value);                                         \
    ast->m_symbol_table->clearValues();                                       \
  }

#define CHECK_FOR_PROCESSOR_THROWS_WITH(data, error_message)          \
  {                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};        \
                                                                      \
    ASTExecutionStack::create();                                      \
                                                                      \
    auto ast = ASTBuilder::build(input);                              \
                                                                      \
    ASTModulesImporter{*ast};                                         \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};           \
                                                                      \
    ASTSymbolTableBuilder{*ast};                                      \
                                                                      \
    ASTExecutionStack::destroy();                                     \
                                                                      \
    REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error_message); \
    ast->m_symbol_table->clearValues();                               \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ForProcessor", "[language]")
{
  SECTION("simple for")
  {
    std::string_view data = R"(
let i:N, i = 0;
for(let l:N, l=0; l<10; ++l) {
  i += l;
}
)";
    CHECK_FOR_PROCESSOR_RESULT(data, "i", 45ul);
  }

  SECTION("for with break")
  {
    std::string_view data = R"(
let i:N, i = 0;
for(let l:N, l=0; l<10; ++l) {
  i += l;
  if (i > 30) break;
}
)";
    CHECK_FOR_PROCESSOR_RESULT(data, "i", 36ul);
  }

  SECTION("for with continue")
  {
    std::string_view data = R"(
let i:N, i = 0;
for(let l:N, l=0; l<10; ++l) {
  if (l<3) continue;
  i += l;
}
)";
    CHECK_FOR_PROCESSOR_RESULT(data, "i", 42ul);
  }

  SECTION("errors")
  {
    SECTION("bad test type")
    {
      std::string_view data = R"(
for(let l:N, l=0; l; ++l) {
}
)";

      CHECK_FOR_PROCESSOR_THROWS_WITH(data, "invalid implicit conversion: N -> B");
    }
  }

  SECTION("expression type")
  {
    ASTNode node;
    REQUIRE(ForProcessor{node}.type() == INodeProcessor::Type::for_processor);
  }
}
