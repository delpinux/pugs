#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <scheme/DiscreteFunctionType.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("DiscreteFunctionType", "[scheme]")
{
  SECTION("name")
  {
    REQUIRE(name(DiscreteFunctionType::P0) == "P0");
    REQUIRE(name(DiscreteFunctionType::P0Vector) == "P0Vector");
  }
}
