#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_predicate.hpp>

#include <utils/checkpointing/Resume.hpp>
#include <utils/pugs_config.hpp>

#ifdef PUGS_HAS_HDF5

#include <MeshDataBaseForTests.hpp>
#include <dev/ParallelChecker.hpp>
#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/modules/MathModule.hpp>
#include <language/utils/ASTCheckpointsInfo.hpp>
#include <language/utils/CheckpointResumeRepository.hpp>
#include <utils/ExecutionStatManager.hpp>
#include <utils/checkpointing/SetResumeFrom.hpp>

class ASTCheckpointsInfoTester
{
 private:
  ASTCheckpointsInfo m_ast_checkpoint_info;

 public:
  ASTCheckpointsInfoTester(const ASTNode& root_node) : m_ast_checkpoint_info(root_node) {}
  ~ASTCheckpointsInfoTester() = default;
};

#define RUN_AST(data)                                          \
  {                                                            \
    ExecutionStatManager::create();                            \
    ParallelChecker::create();                                 \
    CheckpointResumeRepository::create();                      \
                                                               \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"}; \
    auto ast = ASTBuilder::build(input);                       \
                                                               \
    ASTModulesImporter{*ast};                                  \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};    \
                                                               \
    ASTSymbolTableBuilder{*ast};                               \
    ASTNodeDataTypeBuilder{*ast};                              \
                                                               \
    ASTNodeDeclarationToAffectationConverter{*ast};            \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};       \
    ASTNodeTypeCleaner<language::fct_declaration>{*ast};       \
                                                               \
    ASTNodeExpressionBuilder{*ast};                            \
    ExecutionPolicy exec_policy;                               \
    ASTExecutionStack::create();                               \
    ASTCheckpointsInfoTester ast_cp_info_tester{*ast};         \
    ast->execute(exec_policy);                                 \
    ASTExecutionStack::destroy();                              \
    ast->m_symbol_table->clearValues();                        \
                                                               \
    CheckpointResumeRepository::destroy();                     \
    ParallelChecker::destroy();                                \
    ExecutionStatManager::destroy();                           \
    ast->m_symbol_table->clearValues();                        \
  }

#endif   // PUGS_HAS_HDF5

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_Resume", "[utils/checkpointing]")
{
#ifdef PUGS_HAS_HDF5

  SECTION("general")
  {
    auto frobeniusNorm = [](const auto& A) {
      using A_T = std::decay_t<decltype(A)>;
      static_assert(is_tiny_matrix_v<A_T>);
      return std::sqrt(trace(transpose(A) * A));
    };

    using R1 = TinyVector<1>;
    using R2 = TinyVector<2>;
    using R3 = TinyVector<3>;

    using R11 = TinyMatrix<1>;
    using R22 = TinyMatrix<2>;
    using R33 = TinyMatrix<3>;

    MeshDataBaseForTests::destroy();
    GlobalVariableManager::instance().setMeshId(0);
    GlobalVariableManager::instance().setConnectivityId(0);

    ResumingManager::destroy();
    ResumingManager::create();

    std::string tmp_dirname;
    {
      {
        if (parallel::rank() == 0) {
          tmp_dirname = [&]() -> std::string {
            std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
            return std::string{mkdtemp(&temp_filename[0])};
          }();
        }
        parallel::broadcast(tmp_dirname, 0);
      }
      std::filesystem::path path = tmp_dirname;
      const std::string filename = path / "checkpoint.h5";

      ResumingManager::getInstance().setFilename(filename);
    }

    const std::string filename = ResumingManager::getInstance().filename();

    std::string data = R"(
import math;
import mesh;
import scheme;

let f:R*R^3 -> R^3, (a, v) -> a * v;

let alpha:R, alpha = 3.2;
let u1:R^1, u1 = [0.3];
let u2:R^2, u2 = [0.3, 1.2];
let u3:R^3, u3 = [1, 2, 3];

let A1:R^1x1, A1 = [[0.7]];
let A2:R^2x2, A2 = [[1.4, 2.1], [0.6, 3]];
let A3:R^3x3, A3 = [[1.1, 2.2, 3.3], [0.1, 0.2, 0.3], [1.6, 1.2, 1.4]];

let m2d:mesh, m2d = cartesianMesh(0, [1,1], (10,10));

let b_tuple:(B), b_tuple = (true, false, true);
let n_tuple:(N), n_tuple = (1, 2, 3, 4);
let z_tuple:(Z), z_tuple = (1, -2, 3, -4);
let r_tuple:(R), r_tuple = (1.2, -2.4, 3.1, -4.3);
let s_tuple:(string), s_tuple = ("foo", "bar");

let r1_tuple:(R^1), r1_tuple = ([1], [2]);
let r2_tuple:(R^2), r2_tuple = ([1.2, 3], [2.3, 4], [3.2, 1.4]);
let r3_tuple:(R^3), r3_tuple = ([1.2, 0.2, 3], [2.3, -1, 4], [3.2, 2.1, 1.4]);

let r11_tuple:(R^1x1), r11_tuple = ([[1.3]], [[2.4]]);
let r22_tuple:(R^2x2), r22_tuple = ([[1.2, 3], [2.3, 4]], [[3.2, 1.4], [1.3, 5.2]]);
let r33_tuple:(R^3x3), r33_tuple = ([[1.2, 0.2, 3], [2.3, -1, 4], [3.2, 2.1, 1.4]]);

let m1d:mesh, m1d = cartesianMesh([0], [1], 10);

let m3d:mesh, m3d = cartesianMesh(0, [1,1,1], (6,6,6));

let b:B, b = false;
let z:Z, z = -2;
let s:string, s = "foobar";

for(let i:N, i=0; i<3; ++i) {
  checkpoint();

  s = "foobar_"+i;
  z += 1;
  b = not b;

  let g:R -> R^3, x -> [x, 1.2*x, 3];
  u3 = f(alpha,u3);

  checkpoint();
}
)";

    const size_t initial_mesh_id         = GlobalVariableManager::instance().getMeshId();
    const size_t initial_connectivity_id = GlobalVariableManager::instance().getConnectivityId();

    RUN_AST(data);

    GlobalVariableManager::instance().setMeshId(initial_mesh_id);
    GlobalVariableManager::instance().setConnectivityId(initial_connectivity_id);

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");

      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 5);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_5");

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 2);

      HighFive::Group symbol_table1 = symbol_table0.getGroup("symbol table");
      REQUIRE(symbol_table1.getAttribute("alpha").read<double>() == 3.2);
      REQUIRE(l2Norm(symbol_table1.getAttribute("u1").read<R1>() - R1{0.3}) == Catch::Approx(0).margin(1E-12));
      REQUIRE(l2Norm(symbol_table1.getAttribute("u2").read<R2>() - R2{0.3, 1.2}) == Catch::Approx(0).margin(1E-12));
      REQUIRE(l2Norm(symbol_table1.getAttribute("u3").read<R3>() - R3{32.768, 65.536, 98.304}) ==
              Catch::Approx(0).margin(1E-12));
      REQUIRE(symbol_table1.getAttribute("b").read<bool>() == true);
      REQUIRE(symbol_table1.getAttribute("z").read<int64_t>() == 1);
      REQUIRE(symbol_table1.getAttribute("s").read<std::string>() == "foobar_2");
      REQUIRE(symbol_table1.getAttribute("A1").read<R11>()(0, 0) == Catch::Approx(0.7).margin(1E-12));
      REQUIRE(frobeniusNorm(symbol_table1.getAttribute("A2").read<R22>() - R22{1.4, 2.1, 0.6, 3}) ==
              Catch::Approx(0).margin(1E-12));
      REQUIRE(frobeniusNorm(symbol_table1.getAttribute("A3").read<R33>() -
                            R33{1.1, 2.2, 3.3, 0.1, 0.2, 0.3, 1.6, 1.2, 1.4}) == Catch::Approx(0).margin(1E-12));
      REQUIRE(symbol_table1.getAttribute("b_tuple").read<std::vector<bool>>() == std::vector<bool>{true, false, true});
      REQUIRE(symbol_table1.getAttribute("n_tuple").read<std::vector<uint64_t>>() == std::vector<uint64_t>{1, 2, 3, 4});
      REQUIRE(symbol_table1.getAttribute("z_tuple").read<std::vector<int64_t>>() == std::vector<int64_t>{1, -2, 3, -4});
      REQUIRE(symbol_table1.getAttribute("r_tuple").read<std::vector<double>>() ==
              std::vector<double>{1.2, -2.4, 3.1, -4.3});
      REQUIRE(symbol_table1.getAttribute("s_tuple").read<std::vector<std::string>>() ==
              std::vector<std::string>{"foo", "bar"});

      REQUIRE(symbol_table1.getAttribute("r1_tuple").read<std::vector<R1>>() == std::vector{R1{1}, R1{2}});
      REQUIRE(symbol_table1.getAttribute("r2_tuple").read<std::vector<R2>>() ==
              std::vector{R2{1.2, 3}, R2{2.3, 4}, R2{3.2, 1.4}});
      REQUIRE(symbol_table1.getAttribute("r3_tuple").read<std::vector<R3>>() ==
              std::vector{R3{1.2, 0.2, 3}, R3{2.3, -1, 4}, R3{3.2, 2.1, 1.4}});

      REQUIRE(symbol_table1.getAttribute("r11_tuple").read<std::vector<R11>>() == std::vector{R11{1.3}, R11{2.4}});
      REQUIRE(symbol_table1.getAttribute("r22_tuple").read<std::vector<R22>>() ==
              std::vector{R22{1.2, 3, 2.3, 4}, R22{3.2, 1.4, 1.3, 5.2}});
      REQUIRE(symbol_table1.getAttribute("r33_tuple").read<std::vector<R33>>() ==
              std::vector{R33{1.2, 0.2, 3, 2.3, -1, 4, 3.2, 2.1, 1.4}});

      HighFive::Group embedded1 = symbol_table1.getGroup("embedded");

      HighFive::Group m1d = embedded1.getGroup("m1d");
      REQUIRE(m1d.getAttribute("type").read<std::string>() == "mesh");
      REQUIRE(m1d.getAttribute("id").read<uint64_t>() == initial_mesh_id + 3);

      HighFive::Group m2d = embedded1.getGroup("m2d");
      REQUIRE(m2d.getAttribute("type").read<std::string>() == "mesh");
      REQUIRE(m2d.getAttribute("id").read<uint64_t>() == initial_mesh_id + 1);

      HighFive::Group m3d = embedded1.getGroup("m3d");
      REQUIRE(m3d.getAttribute("type").read<std::string>() == "mesh");
      REQUIRE(m3d.getAttribute("id").read<uint64_t>() == initial_mesh_id + 5);

      HighFive::Group singleton        = checkpoint.getGroup("singleton");
      HighFive::Group global_variables = singleton.getGroup("global_variables");
      REQUIRE(global_variables.getAttribute("connectivity_id").read<uint64_t>() ==
              initial_connectivity_id + 3 * (1 + (parallel::size() > 1)));
      REQUIRE(global_variables.getAttribute("mesh_id").read<uint64_t>() == initial_mesh_id + 6);
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 1);

      HighFive::Group connectivity = checkpoint.getGroup("connectivity");
      HighFive::Group connectivity0 =
        connectivity.getGroup(std::to_string(initial_connectivity_id + (parallel::size() > 1)));
      REQUIRE(connectivity0.getAttribute("dimension").read<uint64_t>() == 2);
      REQUIRE(connectivity0.getAttribute("id").read<uint64_t>() == initial_connectivity_id + (parallel::size() > 1));
      REQUIRE(connectivity0.getAttribute("type").read<std::string>() == "unstructured");

      HighFive::Group connectivity1 =
        connectivity.getGroup(std::to_string(initial_connectivity_id + (1 + 2 * (parallel::size() > 1))));
      REQUIRE(connectivity1.getAttribute("dimension").read<uint64_t>() == 1);
      REQUIRE(connectivity1.getAttribute("id").read<uint64_t>() ==
              initial_connectivity_id + (1 + 2 * (parallel::size() > 1)));
      REQUIRE(connectivity1.getAttribute("type").read<std::string>() == "unstructured");

      HighFive::Group connectivity2 =
        connectivity.getGroup(std::to_string(initial_connectivity_id + (2 + 3 * (parallel::size() > 1))));
      REQUIRE(connectivity2.getAttribute("dimension").read<uint64_t>() == 3);
      REQUIRE(connectivity2.getAttribute("id").read<uint64_t>() ==
              initial_connectivity_id + (2 + 3 * (parallel::size() > 1)));
      REQUIRE(connectivity2.getAttribute("type").read<std::string>() == "unstructured");

      HighFive::Group mesh  = checkpoint.getGroup("mesh");
      HighFive::Group mesh0 = mesh.getGroup(std::to_string(initial_mesh_id + 1));
      REQUIRE(mesh0.getAttribute("connectivity").read<uint64_t>() == initial_connectivity_id + (parallel::size() > 1));
      REQUIRE(mesh0.getAttribute("dimension").read<uint64_t>() == 2);
      REQUIRE(mesh0.getAttribute("id").read<uint64_t>() == initial_mesh_id + 1);
      REQUIRE(mesh0.getAttribute("type").read<std::string>() == "polygonal");

      HighFive::Group mesh1 = mesh.getGroup(std::to_string(initial_mesh_id + 3));
      REQUIRE(mesh1.getAttribute("connectivity").read<uint64_t>() ==
              initial_connectivity_id + (1 + 2 * (parallel::size() > 1)));
      REQUIRE(mesh1.getAttribute("dimension").read<uint64_t>() == 1);
      REQUIRE(mesh1.getAttribute("id").read<uint64_t>() == initial_mesh_id + 3);
      REQUIRE(mesh1.getAttribute("type").read<std::string>() == "polygonal");

      HighFive::Group mesh2 = mesh.getGroup(std::to_string(initial_mesh_id + 5));
      REQUIRE(mesh2.getAttribute("connectivity").read<uint64_t>() ==
              initial_connectivity_id + (2 + 3 * (parallel::size() > 1)));
      REQUIRE(mesh2.getAttribute("dimension").read<uint64_t>() == 3);
      REQUIRE(mesh2.getAttribute("id").read<uint64_t>() == initial_mesh_id + 5);
      REQUIRE(mesh2.getAttribute("type").read<std::string>() == "polygonal");

      HighFive::Group functions = checkpoint.getGroup("functions");
      HighFive::Group f         = functions.getGroup("f");
      REQUIRE(f.getAttribute("id").read<uint64_t>() == 0);
      REQUIRE(f.getAttribute("symbol_table_id").read<uint64_t>() == 1);
      HighFive::Group g = functions.getGroup("g");
      REQUIRE(g.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(g.getAttribute("symbol_table_id").read<uint64_t>() == 0);
    }

    parallel::barrier();

    setResumeFrom(filename, 3);

    parallel::barrier();

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");

      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 3);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_3");

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 1);

      HighFive::Group symbol_table1 = symbol_table0.getGroup("symbol table");
      REQUIRE(symbol_table1.getAttribute("alpha").read<double>() == 3.2);
      REQUIRE(l2Norm(symbol_table1.getAttribute("u1").read<R1>() - R1{0.3}) == Catch::Approx(0).margin(1E-12));
      REQUIRE(l2Norm(symbol_table1.getAttribute("u2").read<R2>() - R2{0.3, 1.2}) == Catch::Approx(0).margin(1E-12));
      REQUIRE(l2Norm(symbol_table1.getAttribute("u3").read<R3>() - R3{10.24, 20.48, 30.72}) ==
              Catch::Approx(0).margin(1E-12));
      REQUIRE(symbol_table1.getAttribute("b").read<bool>() == false);
      REQUIRE(symbol_table1.getAttribute("z").read<int64_t>() == 0);
      REQUIRE(symbol_table1.getAttribute("s").read<std::string>() == "foobar_1");
      REQUIRE(symbol_table1.getAttribute("A1").read<R11>()(0, 0) == Catch::Approx(0.7).margin(1E-12));
      REQUIRE(frobeniusNorm(symbol_table1.getAttribute("A2").read<R22>() - R22{1.4, 2.1, 0.6, 3}) ==
              Catch::Approx(0).margin(1E-12));
      REQUIRE(frobeniusNorm(symbol_table1.getAttribute("A3").read<R33>() -
                            R33{1.1, 2.2, 3.3, 0.1, 0.2, 0.3, 1.6, 1.2, 1.4}) == Catch::Approx(0).margin(1E-12));

      REQUIRE(symbol_table1.getAttribute("b_tuple").read<std::vector<bool>>() == std::vector<bool>{true, false, true});
      REQUIRE(symbol_table1.getAttribute("n_tuple").read<std::vector<uint64_t>>() == std::vector<uint64_t>{1, 2, 3, 4});
      REQUIRE(symbol_table1.getAttribute("z_tuple").read<std::vector<int64_t>>() == std::vector<int64_t>{1, -2, 3, -4});
      REQUIRE(symbol_table1.getAttribute("r_tuple").read<std::vector<double>>() ==
              std::vector<double>{1.2, -2.4, 3.1, -4.3});
      REQUIRE(symbol_table1.getAttribute("s_tuple").read<std::vector<std::string>>() ==
              std::vector<std::string>{"foo", "bar"});

      REQUIRE(symbol_table1.getAttribute("r1_tuple").read<std::vector<R1>>() == std::vector{R1{1}, R1{2}});
      REQUIRE(symbol_table1.getAttribute("r2_tuple").read<std::vector<R2>>() ==
              std::vector{R2{1.2, 3}, R2{2.3, 4}, R2{3.2, 1.4}});
      REQUIRE(symbol_table1.getAttribute("r3_tuple").read<std::vector<R3>>() ==
              std::vector{R3{1.2, 0.2, 3}, R3{2.3, -1, 4}, R3{3.2, 2.1, 1.4}});

      REQUIRE(symbol_table1.getAttribute("r11_tuple").read<std::vector<R11>>() == std::vector{R11{1.3}, R11{2.4}});
      REQUIRE(symbol_table1.getAttribute("r22_tuple").read<std::vector<R22>>() ==
              std::vector{R22{1.2, 3, 2.3, 4}, R22{3.2, 1.4, 1.3, 5.2}});
      REQUIRE(symbol_table1.getAttribute("r33_tuple").read<std::vector<R33>>() ==
              std::vector{R33{1.2, 0.2, 3, 2.3, -1, 4, 3.2, 2.1, 1.4}});

      HighFive::Group embedded1 = symbol_table1.getGroup("embedded");

      HighFive::Group m1d = embedded1.getGroup("m1d");
      REQUIRE(m1d.getAttribute("type").read<std::string>() == "mesh");
      REQUIRE(m1d.getAttribute("id").read<uint64_t>() == initial_mesh_id + 3);

      HighFive::Group m2d = embedded1.getGroup("m2d");
      REQUIRE(m2d.getAttribute("type").read<std::string>() == "mesh");
      REQUIRE(m2d.getAttribute("id").read<uint64_t>() == initial_mesh_id + 1);

      HighFive::Group m3d = embedded1.getGroup("m3d");
      REQUIRE(m3d.getAttribute("type").read<std::string>() == "mesh");
      REQUIRE(m3d.getAttribute("id").read<uint64_t>() == initial_mesh_id + 5);

      HighFive::Group singleton        = checkpoint.getGroup("singleton");
      HighFive::Group global_variables = singleton.getGroup("global_variables");
      REQUIRE(global_variables.getAttribute("connectivity_id").read<uint64_t>() ==
              initial_connectivity_id + 3 * (1 + (parallel::size() > 1)));
      REQUIRE(global_variables.getAttribute("mesh_id").read<uint64_t>() == initial_mesh_id + 6);
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 1);

      HighFive::Group connectivity = checkpoint.getGroup("connectivity");
      HighFive::Group connectivity0 =
        connectivity.getGroup(std::to_string(initial_connectivity_id + (parallel::size() > 1)));
      REQUIRE(connectivity0.getAttribute("dimension").read<uint64_t>() == 2);
      REQUIRE(connectivity0.getAttribute("id").read<uint64_t>() == initial_connectivity_id + (parallel::size() > 1));
      REQUIRE(connectivity0.getAttribute("type").read<std::string>() == "unstructured");

      HighFive::Group connectivity1 =
        connectivity.getGroup(std::to_string(initial_connectivity_id + (1 + 2 * (parallel::size() > 1))));
      REQUIRE(connectivity1.getAttribute("dimension").read<uint64_t>() == 1);
      REQUIRE(connectivity1.getAttribute("id").read<uint64_t>() ==
              initial_connectivity_id + (1 + 2 * (parallel::size() > 1)));
      REQUIRE(connectivity1.getAttribute("type").read<std::string>() == "unstructured");

      HighFive::Group connectivity2 =
        connectivity.getGroup(std::to_string(initial_connectivity_id + (2 + 3 * (parallel::size() > 1))));
      REQUIRE(connectivity2.getAttribute("dimension").read<uint64_t>() == 3);
      REQUIRE(connectivity2.getAttribute("id").read<uint64_t>() ==
              initial_connectivity_id + (2 + 3 * (parallel::size() > 1)));
      REQUIRE(connectivity2.getAttribute("type").read<std::string>() == "unstructured");

      HighFive::Group mesh  = checkpoint.getGroup("mesh");
      HighFive::Group mesh0 = mesh.getGroup(std::to_string(initial_mesh_id + 1));
      REQUIRE(mesh0.getAttribute("connectivity").read<uint64_t>() == initial_connectivity_id + (parallel::size() > 1));
      REQUIRE(mesh0.getAttribute("dimension").read<uint64_t>() == 2);
      REQUIRE(mesh0.getAttribute("id").read<uint64_t>() == initial_mesh_id + 1);
      REQUIRE(mesh0.getAttribute("type").read<std::string>() == "polygonal");

      HighFive::Group mesh1 = mesh.getGroup(std::to_string(initial_mesh_id + 3));
      REQUIRE(mesh1.getAttribute("connectivity").read<uint64_t>() ==
              initial_connectivity_id + (1 + 2 * (parallel::size() > 1)));
      REQUIRE(mesh1.getAttribute("dimension").read<uint64_t>() == 1);
      REQUIRE(mesh1.getAttribute("id").read<uint64_t>() == initial_mesh_id + 3);
      REQUIRE(mesh1.getAttribute("type").read<std::string>() == "polygonal");

      HighFive::Group mesh2 = mesh.getGroup(std::to_string(initial_mesh_id + 5));
      REQUIRE(mesh2.getAttribute("connectivity").read<uint64_t>() ==
              initial_connectivity_id + (2 + 3 * (parallel::size() > 1)));
      REQUIRE(mesh2.getAttribute("dimension").read<uint64_t>() == 3);
      REQUIRE(mesh2.getAttribute("id").read<uint64_t>() == initial_mesh_id + 5);
      REQUIRE(mesh2.getAttribute("type").read<std::string>() == "polygonal");

      HighFive::Group functions = checkpoint.getGroup("functions");
      HighFive::Group f         = functions.getGroup("f");
      REQUIRE(f.getAttribute("id").read<uint64_t>() == 0);
      REQUIRE(f.getAttribute("symbol_table_id").read<uint64_t>() == 1);
      HighFive::Group g = functions.getGroup("g");
      REQUIRE(g.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(g.getAttribute("symbol_table_id").read<uint64_t>() == 0);
    }

    ResumingManager::destroy();
    ResumingManager::create();
    ResumingManager::getInstance().setFilename(filename);
    ResumingManager::getInstance().setIsResuming(true);
    GlobalVariableManager::instance().setMeshId(initial_mesh_id);
    GlobalVariableManager::instance().setConnectivityId(initial_connectivity_id);

    RUN_AST(data);

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");

      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 5);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_5");

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 2);

      HighFive::Group symbol_table1 = symbol_table0.getGroup("symbol table");
      REQUIRE(symbol_table1.getAttribute("alpha").read<double>() == 3.2);
      REQUIRE(l2Norm(symbol_table1.getAttribute("u1").read<R1>() - R1{0.3}) == Catch::Approx(0).margin(1E-12));
      REQUIRE(l2Norm(symbol_table1.getAttribute("u2").read<R2>() - R2{0.3, 1.2}) == Catch::Approx(0).margin(1E-12));
      REQUIRE(l2Norm(symbol_table1.getAttribute("u3").read<R3>() - R3{32.768, 65.536, 98.304}) ==
              Catch::Approx(0).margin(1E-12));
      REQUIRE(symbol_table1.getAttribute("b").read<bool>() == true);
      REQUIRE(symbol_table1.getAttribute("z").read<int64_t>() == 1);
      REQUIRE(symbol_table1.getAttribute("s").read<std::string>() == "foobar_2");
      REQUIRE(symbol_table1.getAttribute("A1").read<R11>()(0, 0) == Catch::Approx(0.7).margin(1E-12));
      REQUIRE(frobeniusNorm(symbol_table1.getAttribute("A2").read<R22>() - R22{1.4, 2.1, 0.6, 3}) ==
              Catch::Approx(0).margin(1E-12));
      REQUIRE(frobeniusNorm(symbol_table1.getAttribute("A3").read<R33>() -
                            R33{1.1, 2.2, 3.3, 0.1, 0.2, 0.3, 1.6, 1.2, 1.4}) == Catch::Approx(0).margin(1E-12));

      REQUIRE(symbol_table1.getAttribute("b_tuple").read<std::vector<bool>>() == std::vector<bool>{true, false, true});
      REQUIRE(symbol_table1.getAttribute("n_tuple").read<std::vector<uint64_t>>() == std::vector<uint64_t>{1, 2, 3, 4});
      REQUIRE(symbol_table1.getAttribute("z_tuple").read<std::vector<int64_t>>() == std::vector<int64_t>{1, -2, 3, -4});
      REQUIRE(symbol_table1.getAttribute("r_tuple").read<std::vector<double>>() ==
              std::vector<double>{1.2, -2.4, 3.1, -4.3});
      REQUIRE(symbol_table1.getAttribute("s_tuple").read<std::vector<std::string>>() ==
              std::vector<std::string>{"foo", "bar"});

      REQUIRE(symbol_table1.getAttribute("r1_tuple").read<std::vector<R1>>() == std::vector{R1{1}, R1{2}});
      REQUIRE(symbol_table1.getAttribute("r2_tuple").read<std::vector<R2>>() ==
              std::vector{R2{1.2, 3}, R2{2.3, 4}, R2{3.2, 1.4}});
      REQUIRE(symbol_table1.getAttribute("r3_tuple").read<std::vector<R3>>() ==
              std::vector{R3{1.2, 0.2, 3}, R3{2.3, -1, 4}, R3{3.2, 2.1, 1.4}});

      REQUIRE(symbol_table1.getAttribute("r11_tuple").read<std::vector<R11>>() == std::vector{R11{1.3}, R11{2.4}});
      REQUIRE(symbol_table1.getAttribute("r22_tuple").read<std::vector<R22>>() ==
              std::vector{R22{1.2, 3, 2.3, 4}, R22{3.2, 1.4, 1.3, 5.2}});
      REQUIRE(symbol_table1.getAttribute("r33_tuple").read<std::vector<R33>>() ==
              std::vector{R33{1.2, 0.2, 3, 2.3, -1, 4, 3.2, 2.1, 1.4}});

      HighFive::Group embedded1 = symbol_table1.getGroup("embedded");

      HighFive::Group m1d = embedded1.getGroup("m1d");
      REQUIRE(m1d.getAttribute("type").read<std::string>() == "mesh");
      REQUIRE(m1d.getAttribute("id").read<uint64_t>() == initial_mesh_id + 3);

      HighFive::Group m2d = embedded1.getGroup("m2d");
      REQUIRE(m2d.getAttribute("type").read<std::string>() == "mesh");
      REQUIRE(m2d.getAttribute("id").read<uint64_t>() == initial_mesh_id + 1);

      HighFive::Group m3d = embedded1.getGroup("m3d");
      REQUIRE(m3d.getAttribute("type").read<std::string>() == "mesh");
      REQUIRE(m3d.getAttribute("id").read<uint64_t>() == initial_mesh_id + 5);

      HighFive::Group singleton        = checkpoint.getGroup("singleton");
      HighFive::Group global_variables = singleton.getGroup("global_variables");
      REQUIRE(global_variables.getAttribute("connectivity_id").read<uint64_t>() ==
              initial_connectivity_id + 3 * (1 + (parallel::size() > 1)));
      REQUIRE(global_variables.getAttribute("mesh_id").read<uint64_t>() == initial_mesh_id + 6);
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 2);

      HighFive::Group connectivity = checkpoint.getGroup("connectivity");
      HighFive::Group connectivity0 =
        connectivity.getGroup(std::to_string(initial_connectivity_id + (parallel::size() > 1)));
      REQUIRE(connectivity0.getAttribute("dimension").read<uint64_t>() == 2);
      REQUIRE(connectivity0.getAttribute("id").read<uint64_t>() == initial_connectivity_id + (parallel::size() > 1));
      REQUIRE(connectivity0.getAttribute("type").read<std::string>() == "unstructured");

      HighFive::Group connectivity1 =
        connectivity.getGroup(std::to_string(initial_connectivity_id + (1 + 2 * (parallel::size() > 1))));
      REQUIRE(connectivity1.getAttribute("dimension").read<uint64_t>() == 1);
      REQUIRE(connectivity1.getAttribute("id").read<uint64_t>() ==
              initial_connectivity_id + (1 + 2 * (parallel::size() > 1)));
      REQUIRE(connectivity1.getAttribute("type").read<std::string>() == "unstructured");

      HighFive::Group connectivity2 =
        connectivity.getGroup(std::to_string(initial_connectivity_id + (2 + 3 * (parallel::size() > 1))));
      REQUIRE(connectivity2.getAttribute("dimension").read<uint64_t>() == 3);
      REQUIRE(connectivity2.getAttribute("id").read<uint64_t>() ==
              initial_connectivity_id + (2 + 3 * (parallel::size() > 1)));
      REQUIRE(connectivity2.getAttribute("type").read<std::string>() == "unstructured");

      HighFive::Group mesh  = checkpoint.getGroup("mesh");
      HighFive::Group mesh0 = mesh.getGroup(std::to_string(initial_mesh_id + 1));
      REQUIRE(mesh0.getAttribute("connectivity").read<uint64_t>() == initial_connectivity_id + (parallel::size() > 1));
      REQUIRE(mesh0.getAttribute("dimension").read<uint64_t>() == 2);
      REQUIRE(mesh0.getAttribute("id").read<uint64_t>() == initial_mesh_id + 1);
      REQUIRE(mesh0.getAttribute("type").read<std::string>() == "polygonal");

      HighFive::Group mesh1 = mesh.getGroup(std::to_string(initial_mesh_id + 3));
      REQUIRE(mesh1.getAttribute("connectivity").read<uint64_t>() ==
              initial_connectivity_id + (1 + 2 * (parallel::size() > 1)));
      REQUIRE(mesh1.getAttribute("dimension").read<uint64_t>() == 1);
      REQUIRE(mesh1.getAttribute("id").read<uint64_t>() == initial_mesh_id + 3);
      REQUIRE(mesh1.getAttribute("type").read<std::string>() == "polygonal");

      HighFive::Group mesh2 = mesh.getGroup(std::to_string(initial_mesh_id + 5));
      REQUIRE(mesh2.getAttribute("connectivity").read<uint64_t>() ==
              initial_connectivity_id + (2 + 3 * (parallel::size() > 1)));
      REQUIRE(mesh2.getAttribute("dimension").read<uint64_t>() == 3);
      REQUIRE(mesh2.getAttribute("id").read<uint64_t>() == initial_mesh_id + 5);
      REQUIRE(mesh2.getAttribute("type").read<std::string>() == "polygonal");

      HighFive::Group functions = checkpoint.getGroup("functions");
      HighFive::Group f         = functions.getGroup("f");
      REQUIRE(f.getAttribute("id").read<uint64_t>() == 0);
      REQUIRE(f.getAttribute("symbol_table_id").read<uint64_t>() == 1);
      HighFive::Group g = functions.getGroup("g");
      REQUIRE(g.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(g.getAttribute("symbol_table_id").read<uint64_t>() == 0);
    }

    parallel::barrier();
    if (parallel::rank() == 0) {
      std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
    }

    // Revert to default value
    ResumingManager::getInstance().setFilename("checkpoint.h5");
    MeshDataBaseForTests::create();
  }

  SECTION("simple if")
  {
    ResumingManager::destroy();
    ResumingManager::create();

    std::string tmp_dirname;
    {
      {
        if (parallel::rank() == 0) {
          tmp_dirname = [&]() -> std::string {
            std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
            return std::string{mkdtemp(&temp_filename[0])};
          }();
        }
        parallel::broadcast(tmp_dirname, 0);
      }
      std::filesystem::path path = tmp_dirname;
      const std::string filename = path / "checkpoint.h5";

      ResumingManager::getInstance().setFilename(filename);
    }

    const std::string filename = ResumingManager::getInstance().filename();

    std::string data = R"(
let i:N, i = 3;

if (true) {
  checkpoint();
}
i = 7;
checkpoint();
)";

    RUN_AST(data);

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");

      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_1");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 1);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 7);
    }

    parallel::barrier();

    setResumeFrom(filename, 0);

    parallel::barrier();

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");
      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 0);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 0);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_0");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 1);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 3);
    }

    ResumingManager::destroy();
    ResumingManager::create();
    ResumingManager::getInstance().setFilename(filename);
    ResumingManager::getInstance().setIsResuming(true);

    RUN_AST(data);

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");
      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_1");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 2);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 7);
    }

    parallel::barrier();
    if (parallel::rank() == 0) {
      std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
    }

    // Revert to default value
    ResumingManager::getInstance().setFilename("checkpoint.h5");
  }

  SECTION("simple else")
  {
    ResumingManager::destroy();
    ResumingManager::create();

    std::string tmp_dirname;
    {
      {
        if (parallel::rank() == 0) {
          tmp_dirname = [&]() -> std::string {
            std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
            return std::string{mkdtemp(&temp_filename[0])};
          }();
        }
        parallel::broadcast(tmp_dirname, 0);
      }
      std::filesystem::path path = tmp_dirname;
      const std::string filename = path / "checkpoint.h5";

      ResumingManager::getInstance().setFilename(filename);
    }

    const std::string filename = ResumingManager::getInstance().filename();

    std::string data = R"(
let i:N, i = 3;

if (false) {
} else {
  checkpoint();
}
i = 7;
checkpoint();
)";

    RUN_AST(data);

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");

      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_1");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 1);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 7);
    }

    parallel::barrier();

    setResumeFrom(filename, 0);

    parallel::barrier();

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");
      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 0);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 0);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_0");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 1);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 3);
    }

    ResumingManager::destroy();
    ResumingManager::create();
    ResumingManager::getInstance().setFilename(filename);
    ResumingManager::getInstance().setIsResuming(true);

    RUN_AST(data);

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");
      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_1");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 2);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 7);
    }

    parallel::barrier();
    if (parallel::rank() == 0) {
      std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
    }

    // Revert to default value
    ResumingManager::getInstance().setFilename("checkpoint.h5");
  }

  SECTION("simple do while")
  {
    ResumingManager::destroy();
    ResumingManager::create();

    std::string tmp_dirname;
    {
      {
        if (parallel::rank() == 0) {
          tmp_dirname = [&]() -> std::string {
            std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
            return std::string{mkdtemp(&temp_filename[0])};
          }();
        }
        parallel::broadcast(tmp_dirname, 0);
      }
      std::filesystem::path path = tmp_dirname;
      const std::string filename = path / "checkpoint.h5";

      ResumingManager::getInstance().setFilename(filename);
    }

    const std::string filename = ResumingManager::getInstance().filename();

    std::string data = R"(
let i:N, i = 3;

do {
  checkpoint();
} while(false);

i = 7;
checkpoint();
)";

    RUN_AST(data);

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");

      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_1");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 1);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 7);
    }

    parallel::barrier();

    setResumeFrom(filename, 0);

    parallel::barrier();

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");
      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 0);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 0);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_0");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 1);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      HighFive::Group symbol_table1 = symbol_table0.getGroup("symbol table");
      REQUIRE(symbol_table1.getAttribute("i").read<uint64_t>() == 3);
    }

    ResumingManager::destroy();
    ResumingManager::create();
    ResumingManager::getInstance().setFilename(filename);
    ResumingManager::getInstance().setIsResuming(true);

    RUN_AST(data);

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");
      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_1");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 2);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 7);
    }

    parallel::barrier();
    if (parallel::rank() == 0) {
      std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
    }

    // Revert to default value
    ResumingManager::getInstance().setFilename("checkpoint.h5");
  }

  SECTION("simple while")
  {
    ResumingManager::destroy();
    ResumingManager::create();

    std::string tmp_dirname;
    {
      {
        if (parallel::rank() == 0) {
          tmp_dirname = [&]() -> std::string {
            std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
            return std::string{mkdtemp(&temp_filename[0])};
          }();
        }
        parallel::broadcast(tmp_dirname, 0);
      }
      std::filesystem::path path = tmp_dirname;
      const std::string filename = path / "checkpoint.h5";

      ResumingManager::getInstance().setFilename(filename);
    }

    const std::string filename = ResumingManager::getInstance().filename();

    std::string data = R"(
let i:N, i = 3;

while (true) {
  checkpoint();
  break;
};

i = 7;
checkpoint();
)";

    RUN_AST(data);

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");

      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_1");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 1);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 7);
    }

    parallel::barrier();

    setResumeFrom(filename, 0);

    parallel::barrier();

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");
      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 0);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 0);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_0");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 1);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      HighFive::Group symbol_table1 = symbol_table0.getGroup("symbol table");
      HighFive::Group symbol_table2 = symbol_table1.getGroup("symbol table");
      REQUIRE(symbol_table2.getAttribute("i").read<uint64_t>() == 3);
    }

    ResumingManager::destroy();
    ResumingManager::create();
    ResumingManager::getInstance().setFilename(filename);
    ResumingManager::getInstance().setIsResuming(true);

    RUN_AST(data);

    {   // Check checkpoint file

      HighFive::File file(ResumingManager::getInstance().filename(), HighFive::File::ReadOnly);

      HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");
      REQUIRE(checkpoint.getAttribute("id").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("checkpoint_number").read<uint64_t>() == 1);
      REQUIRE(checkpoint.getAttribute("name").read<std::string>() == "checkpoint_1");

      HighFive::Group singleton      = checkpoint.getGroup("singleton");
      HighFive::Group execution_info = singleton.getGroup("execution_info");
      REQUIRE(execution_info.getAttribute("run_number").read<uint64_t>() == 2);

      HighFive::Group symbol_table0 = checkpoint.getGroup("symbol table");
      REQUIRE(symbol_table0.getAttribute("i").read<uint64_t>() == 7);
    }

    parallel::barrier();
    if (parallel::rank() == 0) {
      std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
    }

    // Revert to default value
    ResumingManager::getInstance().setFilename("checkpoint.h5");
  }
#else    // PUGS_HAS_HDF5
  REQUIRE_THROWS_WITH(resume(), "error: checkpoint/resume mechanism requires HDF5");
#endif   // PUGS_HAS_HDF5
}
