#include <catch2/catch_test_macros.hpp>

#include <utils/Messenger.hpp>
#include <utils/Partitioner.hpp>

#include <set>

// clazy:excludeall=non-pod-global-static

TEST_CASE("Partitioner", "[utils]")
{
  SECTION("one graph split to all")
  {
    std::vector<int> entries_vector;
    std::vector<int> neighbors_vector;

    entries_vector.push_back(neighbors_vector.size());

    if (parallel::rank() == 0) {
      neighbors_vector.push_back(1);
      neighbors_vector.push_back(2);
      neighbors_vector.push_back(4);
      entries_vector.push_back(neighbors_vector.size());

      neighbors_vector.push_back(0);
      neighbors_vector.push_back(3);
      neighbors_vector.push_back(5);
      entries_vector.push_back(neighbors_vector.size());

      neighbors_vector.push_back(0);
      neighbors_vector.push_back(2);
      neighbors_vector.push_back(5);
      entries_vector.push_back(neighbors_vector.size());

      neighbors_vector.push_back(0);
      neighbors_vector.push_back(2);
      neighbors_vector.push_back(5);
      entries_vector.push_back(neighbors_vector.size());

      neighbors_vector.push_back(3);
      neighbors_vector.push_back(5);
      neighbors_vector.push_back(7);
      entries_vector.push_back(neighbors_vector.size());

      neighbors_vector.push_back(3);
      neighbors_vector.push_back(5);
      neighbors_vector.push_back(6);
      entries_vector.push_back(neighbors_vector.size());

      neighbors_vector.push_back(5);
      neighbors_vector.push_back(6);
      neighbors_vector.push_back(7);
      entries_vector.push_back(neighbors_vector.size());

      neighbors_vector.push_back(3);
      neighbors_vector.push_back(5);
      neighbors_vector.push_back(7);
      entries_vector.push_back(neighbors_vector.size());

      neighbors_vector.push_back(4);
      neighbors_vector.push_back(6);
      neighbors_vector.push_back(7);
      entries_vector.push_back(neighbors_vector.size());
    }

    Array<int> entries   = convert_to_array(entries_vector);
    Array<int> neighbors = convert_to_array(neighbors_vector);

    CRSGraph graph{entries, neighbors};

    SECTION("ParMETIS")
    {
      PartitionerOptions partitioner_options;
      partitioner_options.library() = PartitionerLibrary::parmetis;
      Partitioner partitioner{partitioner_options};

      Array<int> partitioned = partitioner.partition(graph);

      REQUIRE((partitioned.size() + 1) == entries.size());

#ifdef PUGS_HAS_PARMETIS
      if (parallel::rank() == 0) {
        std::set<int> assigned_ranks;
        for (size_t i = 0; i < partitioned.size(); ++i) {
          assigned_ranks.insert(partitioned[i]);
        }

        REQUIRE(assigned_ranks.size() == parallel::size());
      }
#else    // PUGS_HAS_PARMETIS
      REQUIRE(min(partitioned) == 0);
      REQUIRE(max(partitioned) == 0);
#endif   // PUGS_HAS_PARMETIS
    }

    SECTION("PTScotch")
    {
      PartitionerOptions partitioner_options;
      partitioner_options.library() = PartitionerLibrary::ptscotch;
      Partitioner partitioner{partitioner_options};

      Array<int> partitioned = partitioner.partition(graph);

      REQUIRE((partitioned.size() + 1) == entries.size());

#ifdef PUGS_HAS_PTSCOTCH
      if (parallel::rank() == 0) {
        std::set<int> assigned_ranks;
        for (size_t i = 0; i < partitioned.size(); ++i) {
          assigned_ranks.insert(partitioned[i]);
        }

        REQUIRE(assigned_ranks.size() == parallel::size());
      }
#else    // PUGS_HAS_PTSCOTCH
      REQUIRE(min(partitioned) == 0);
      REQUIRE(max(partitioned) == 0);
#endif   // PUGS_HAS_PTSCOTCH
    }
  }
}
