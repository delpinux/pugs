#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ParseError.hpp>

#include <pegtl/string_input.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTSymbolTableBuilder", "[language]")
{
  using namespace TAO_PEGTL_NAMESPACE;

  SECTION("Build symbols")
  {
    std::string_view data = R"(
{
 let m:N, m = 2;
 let n:R, n = m/3.;
}
let n:N, n = 2;
)";

    string_input input{data, "test.pgs"};
    auto ast = ASTBuilder::build(input);

    ASTSymbolTableBuilder{*ast};
    ast->m_symbol_table->clearValues();
  }

  SECTION("Populate symbol table")
  {
    std::string_view data = R"(
let b:B;
let n:N;
let z:Z;
let x:R;
let (c0,c1,c2,c3):R*Z*N*B;
let f: R*Z*B->R, (x,n,z) -> x+n;
)";

    string_input input{data, "test.pgs"};
    auto ast = ASTBuilder::build(input);

    ASTSymbolTableBuilder{*ast};

    auto root_st = ast->m_symbol_table;

    std::stringstream st_output;
    st_output << '\n' << *root_st;

    std::stringstream expected_output;
    expected_output << '\n'
                    << "-- Symbol table state -- parent : " << static_cast<SymbolTable*>(nullptr) << '\n'
                    << " b: undefined:--\n"
                    << " n: undefined:--\n"
                    << " z: undefined:--\n"
                    << " x: undefined:--\n"
                    << " c0: undefined:--\n"
                    << " c1: undefined:--\n"
                    << " c2: undefined:--\n"
                    << " c3: undefined:--\n"
                    << " f: undefined:0\n"
                    << "------------------------\n";

    REQUIRE(st_output.str() == expected_output.str());
    ast->m_symbol_table->clearValues();
  }

  SECTION("errors")
  {
    SECTION("Undeclared symbol")
    {
      std::string_view data = R"(
let n:N, n = a;
)";

      string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);

      REQUIRE_THROWS_WITH(ASTSymbolTableBuilder{*ast}, "undefined symbol 'a'");
      ast->m_symbol_table->clearValues();
    }

    SECTION("Re-declared symbol")
    {
      std::string_view data = R"(
let n:N, n = 0;
let n:N, n = 1;
)";

      string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);

      REQUIRE_THROWS_WITH(ASTSymbolTableBuilder{*ast}, "symbol 'n' was already defined at line 2");
      ast->m_symbol_table->clearValues();
    }

    SECTION("Re-declared symbol (nested scope)")
    {
      std::string_view data = R"(
let n:N, n = 0;
{
  let n:N, n = 1;
}
)";

      string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);

      REQUIRE_THROWS_WITH(ASTSymbolTableBuilder{*ast}, "symbol 'n' was already defined at line 2");
      ast->m_symbol_table->clearValues();
    }

    SECTION("Re-declared symbol (function)")
    {
      std::string_view data = R"(
let f:N;
let f : R -> R, x -> 1;
)";

      string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);

      REQUIRE_THROWS_WITH(ASTSymbolTableBuilder{*ast}, "symbol 'f' was already defined at line 2");
      ast->m_symbol_table->clearValues();
    }

    SECTION("Re-declared symbol (builtin function)")
    {
      std::string_view data = R"(
import math;
let cos:N;
)";

      string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      ASTModulesImporter{*ast};

      REQUIRE_THROWS_WITH(ASTSymbolTableBuilder{*ast}, "symbol 'cos' already denotes a builtin function!");
      ast->m_symbol_table->clearValues();
    }

    SECTION("Re-declared symbol (builtin function) 2")
    {
      std::string_view data = R"(
import math;
let cos: R -> R, x->2*x;
)";

      string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      ASTModulesImporter{*ast};

      REQUIRE_THROWS_WITH(ASTSymbolTableBuilder{*ast}, "symbol 'cos' already denotes a builtin function!");
      ast->m_symbol_table->clearValues();
    }

    SECTION("Re-declared parameter (function)")
    {
      std::string_view data = R"(
let f : R*R*N -> R,
       (x,y,x) -> 1;
)";

      string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);

      REQUIRE_THROWS_WITH(ASTSymbolTableBuilder{*ast}, "symbol 'x' was already defined at line 3");
      ast->m_symbol_table->clearValues();
    }
  }
}
