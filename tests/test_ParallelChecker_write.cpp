#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <dev/ParallelChecker.hpp>

#include <MeshDataBaseForTests.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

#ifdef PUGS_HAS_HDF5

#include <ParallelCheckerTester.hpp>

TEST_CASE("ParallelChecker_write", "[dev]")
{
  {
    ParallelCheckerTester pc_tester;
    if (pc_tester.isCreated()) {
      REQUIRE_NOTHROW(ParallelChecker::destroy());
    }
  }

  REQUIRE_NOTHROW(ParallelChecker::create());
  REQUIRE_NOTHROW(ParallelChecker::instance().setMode(ParallelChecker::Mode::write));

  auto get_pc_options = []() -> std::tuple<std::string, ParallelChecker::Mode, size_t> {
    ParallelCheckerTester pc_tester;
    return std::make_tuple(pc_tester.getFilename(), pc_tester.getMode(), pc_tester.getTag());
  };

  auto set_pc_options = [](const std::tuple<std::string, ParallelChecker::Mode, size_t>& options) {
    auto [filename, mode, tag] = options;
    ParallelCheckerTester pc_tester;
    pc_tester.setFilename(filename);
    pc_tester.setMode(mode);
    pc_tester.setTag(tag);
  };

  SECTION("set config at init")
  {
    auto [filename, mode, tag] = get_pc_options();

    REQUIRE(ParallelChecker::instance().filename() == "parallel_checker.h5");
    REQUIRE(ParallelChecker::instance().mode() == mode);
    REQUIRE(tag == 0);

    ParallelChecker::instance().setFilename("foo.h5");
    ParallelChecker::instance().setMode(ParallelChecker::Mode::automatic);

    REQUIRE(ParallelChecker::instance().filename() == "foo.h5");
    REQUIRE(ParallelChecker::instance().mode() == ParallelChecker::Mode::automatic);
  }

  std::string tmp_dirname;

  {
    if (parallel::rank() == 0) {
      tmp_dirname = [&]() -> std::string {
        std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_test_write_h5_XXXXXX";
        return std::string{mkdtemp(&temp_filename[0])};
      }();
    }
    parallel::broadcast(tmp_dirname, 0);

    std::filesystem::path path = tmp_dirname;
    REQUIRE_NOTHROW(ParallelChecker::instance().setFilename(path / "parallel_check.h5"));
  }

  SECTION("set values")
  {
    set_pc_options(std::make_tuple(std::string{"foo.h5"}, ParallelChecker::Mode::write, 37));

    auto pc_options = get_pc_options();

    auto [filename, mode, tag] = pc_options;

    REQUIRE(ParallelChecker::instance().filename() == filename);
    REQUIRE(ParallelChecker::instance().filename() == "foo.h5");
    REQUIRE(ParallelChecker::instance().mode() == ParallelChecker::Mode::write);
    REQUIRE(ParallelChecker::instance().mode() == mode);
    REQUIRE(tag == 37);
  }

  SECTION("is writing")
  {
    ParallelCheckerTester pc_tester;

    pc_tester.setMode(ParallelChecker::Mode::write);
    REQUIRE(ParallelChecker::instance().isWriting());

    pc_tester.setMode(ParallelChecker::Mode::read);
    REQUIRE(not ParallelChecker::instance().isWriting());

    pc_tester.setMode(ParallelChecker::Mode::automatic);
    REQUIRE(ParallelChecker::instance().isWriting() == (parallel::size() == 1));
  }

  SECTION("check ItemValue/ItemArray attributes")
  {
    auto check = []<typename ItemValueT>(const ItemValueT& item_value, const std::string& var_name,
                                         const SourceLocation& source_location, const size_t tag) {
      ItemType item_type = ItemValueT::item_t;
      using DataType     = typename ItemValueT::data_type;

      HighFive::File file(ParallelChecker::instance().filename(), HighFive::File::ReadOnly);
      HighFive::Group group_var0 = file.getGroup("values/" + std::to_string(tag));
      REQUIRE(group_var0.getNumberObjects() == 2);

      REQUIRE(group_var0.exist(std::string{itemName(item_type)} + "_numbers"));
      REQUIRE(group_var0.exist("data/" + var_name));

      REQUIRE(group_var0.getNumberAttributes() == 7);
      REQUIRE(group_var0.hasAttribute("filename"));
      REQUIRE(group_var0.hasAttribute("function"));
      REQUIRE(group_var0.hasAttribute("line"));
      REQUIRE(group_var0.hasAttribute("dimension"));
      REQUIRE(group_var0.hasAttribute("data_type"));
      REQUIRE(group_var0.hasAttribute("item_type"));
      REQUIRE(group_var0.hasAttribute("name"));

      REQUIRE(group_var0.getAttribute("filename").read<std::string>() == source_location.filename());
      REQUIRE(group_var0.getAttribute("function").read<std::string>() == source_location.function());
      REQUIRE(group_var0.getAttribute("line").read<size_t>() == source_location.line());
      REQUIRE(group_var0.getAttribute("dimension").read<size_t>() == item_value.connectivity_ptr()->dimension());
      REQUIRE(group_var0.getAttribute("data_type").read<std::string>() == demangle<DataType>());
      REQUIRE(group_var0.getAttribute("item_type").read<std::string>() == itemName(item_type));
      REQUIRE(group_var0.getAttribute("name").read<std::string>() == var_name);
    };

    // ItemValues
    {   // 1d
      auto mesh = MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>();

      const Connectivity<1>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        CellValue<double> var{connectivity};
        var.fill(1);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        DiscreteFunctionP0<double> var{mesh};
        var.fill(1);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var.cellValues(), var_name, source_location, tag);
      }
      {
        NodeValue<TinyVector<2>> var{connectivity};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(ItemValueVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
    }

    {   // 2d
      auto mesh = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>();

      const Connectivity<2>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        FaceValue<TinyMatrix<3, 2>> var{connectivity};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        FaceValue<TinyVector<1>> var{connectivity};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(ItemValueVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
    }

    {   // 3d
      auto mesh = MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>();

      const Connectivity<3>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        EdgeValue<TinyMatrix<2, 2>> var{connectivity};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        NodeValue<TinyVector<3>> var{connectivity};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(ItemValueVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        DiscreteFunctionP0<TinyVector<3>> var{mesh};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(DiscreteFunctionVariant{var}, var_name, source_location);
        check(var.cellValues(), var_name, source_location, tag);
      }
    }

    // ItemArrays
    {   // 1d
      auto mesh = MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>();

      const Connectivity<1>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        CellArray<double> var{connectivity, 2};
        var.fill(1);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        DiscreteFunctionP0Vector<double> var{mesh, 2};
        var.fill(1);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var.cellArrays(), var_name, source_location, tag);
      }
      {
        NodeArray<TinyVector<2>> var{connectivity, 1};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(ItemArrayVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
    }

    {   // 2d
      auto mesh = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>();

      const Connectivity<2>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        FaceArray<TinyMatrix<3, 2>> var{connectivity, 2};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        FaceArray<TinyVector<1>> var{connectivity, 3};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(ItemArrayVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
    }

    {   // 3d
      auto mesh = MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>();

      const Connectivity<3>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        EdgeArray<TinyMatrix<2, 2>> var{connectivity, 2};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        NodeArray<TinyVector<3>> var{connectivity, 3};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(ItemArrayVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        DiscreteFunctionP0Vector<double> var{mesh, 3};
        var.fill(0);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(DiscreteFunctionVariant{var}, var_name, source_location);
        check(var.cellArrays(), var_name, source_location, tag);
      }
    }
  }

  SECTION("check SubItemValuePerItem/SubItemArrayPerItem attributes")
  {
    auto check = []<typename SubItemValuePerItemT>(const SubItemValuePerItemT& item_value, const std::string& var_name,
                                                   const SourceLocation& source_location, const size_t tag) {
      ItemType item_type     = SubItemValuePerItemT::item_type;
      ItemType sub_item_type = SubItemValuePerItemT::sub_item_type;
      using DataType         = typename SubItemValuePerItemT::data_type;

      HighFive::File file(ParallelChecker::instance().filename(), HighFive::File::ReadOnly);
      HighFive::Group group_var0 = file.getGroup("values/" + std::to_string(tag));
      REQUIRE(group_var0.getNumberObjects() == 5);

      REQUIRE(group_var0.exist(std::string{itemName(item_type)} + "_numbers"));
      REQUIRE(group_var0.exist(std::string{itemName(sub_item_type)} + "_numbers"));
      REQUIRE(group_var0.exist("sub_item_index"));
      REQUIRE(group_var0.exist("rows_map"));
      REQUIRE(group_var0.exist("data/" + var_name));

      REQUIRE(group_var0.getNumberAttributes() == 8);
      REQUIRE(group_var0.hasAttribute("filename"));
      REQUIRE(group_var0.hasAttribute("function"));
      REQUIRE(group_var0.hasAttribute("line"));
      REQUIRE(group_var0.hasAttribute("dimension"));
      REQUIRE(group_var0.hasAttribute("data_type"));
      REQUIRE(group_var0.hasAttribute("item_type"));
      REQUIRE(group_var0.hasAttribute("subitem_type"));
      REQUIRE(group_var0.hasAttribute("name"));

      REQUIRE(group_var0.getAttribute("filename").read<std::string>() == source_location.filename());
      REQUIRE(group_var0.getAttribute("function").read<std::string>() == source_location.function());
      REQUIRE(group_var0.getAttribute("line").read<size_t>() == source_location.line());
      REQUIRE(group_var0.getAttribute("dimension").read<size_t>() == item_value.connectivity_ptr()->dimension());
      REQUIRE(group_var0.getAttribute("data_type").read<std::string>() == demangle<DataType>());
      REQUIRE(group_var0.getAttribute("item_type").read<std::string>() == itemName(item_type));
      REQUIRE(group_var0.getAttribute("subitem_type").read<std::string>() == itemName(sub_item_type));
      REQUIRE(group_var0.getAttribute("name").read<std::string>() == var_name);
    };

    // ItemValues
    {   // 1d
      auto mesh = MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>();

      const Connectivity<1>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        CellValuePerNode<double> var{connectivity};
        var.fill(1);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        NodeValuePerCell<TinyVector<2>> var{connectivity};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(SubItemValuePerItemVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
    }

    {   // 2d
      auto mesh = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>();

      const Connectivity<2>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        FaceValuePerCell<TinyMatrix<3, 2>> var{connectivity};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        FaceValuePerNode<TinyVector<1>> var{connectivity};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(SubItemValuePerItemVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
    }

    {   // 3d
      auto mesh = MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>();

      const Connectivity<3>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        EdgeValuePerFace<TinyMatrix<2, 2>> var{connectivity};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        NodeValuePerCell<TinyVector<3>> var{connectivity};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(SubItemValuePerItemVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
    }

    // ItemArrays
    {   // 1d
      auto mesh = MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>();

      const Connectivity<1>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        CellArrayPerNode<double> var{connectivity, 2};
        var.fill(1);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        NodeArrayPerCell<TinyVector<2>> var{connectivity, 1};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(SubItemArrayPerItemVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
    }

    {   // 2d
      auto mesh = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>();

      const Connectivity<2>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        FaceArrayPerNode<TinyMatrix<3, 2>> var{connectivity, 2};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        FaceArrayPerCell<TinyVector<1>> var{connectivity, 3};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(SubItemArrayPerItemVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
    }

    {   // 3d
      auto mesh = MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>();

      const Connectivity<3>& connectivity = mesh->connectivity();

      ParallelCheckerTester pc_tester;
      {
        EdgeArrayPerFace<TinyMatrix<2, 2>> var{connectivity, 2};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(var, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
      {
        NodeArrayPerEdge<TinyVector<3>> var{connectivity, 3};
        var.fill(zero);

        const SourceLocation source_location;
        const size_t tag           = pc_tester.getTag();
        const std::string var_name = "var_" + std::to_string(tag);

        parallel_check(SubItemArrayPerItemVariant{var}, var_name, source_location);
        check(var, var_name, source_location, tag);
      }
    }
  }

  SECTION("invalid set config at after first write")
  {
    auto [filename, mode, tag] = get_pc_options();

    REQUIRE(ParallelChecker::instance().filename() == filename);
    REQUIRE(ParallelChecker::instance().mode() == mode);
    REQUIRE(tag == 0);

    set_pc_options(std::make_tuple(filename, mode, 2ul));

    REQUIRE_THROWS_WITH(ParallelChecker::instance().setFilename("foo.h5"),
                        "unexpected error: Cannot modify parallel checker file if it was already used");
    REQUIRE_THROWS_WITH(ParallelChecker::instance().setMode(ParallelChecker::Mode::automatic),
                        "unexpected error: Cannot modify parallel checker mode if it was already used");
  }

#ifndef NDEBUG
  SECTION("bad creation/destruction/access")
  {
    REQUIRE_THROWS_WITH(ParallelChecker::create(), "ParallelChecker has already been created");
    REQUIRE_NOTHROW(ParallelChecker::destroy());

    REQUIRE_THROWS_WITH(ParallelChecker::destroy(), "ParallelChecker has already been destroyed");
    REQUIRE_THROWS_WITH(ParallelChecker::instance(), "ParallelChecker was not created");

    REQUIRE_NOTHROW(ParallelChecker::create());
  }
#endif

  std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  REQUIRE_NOTHROW(ParallelChecker::destroy());
}

#else   // PUGS_HAS_HDF5

TEST_CASE("ParallelChecker_write", "[dev]")
{
  REQUIRE_NOTHROW(ParallelChecker::create());
  REQUIRE_NOTHROW(ParallelChecker::instance().setMode(ParallelChecker::Mode::read));
  REQUIRE_NOTHROW(not ParallelChecker::instance().isWriting());
  REQUIRE_NOTHROW(ParallelChecker::instance().setMode(ParallelChecker::Mode::write));
  REQUIRE_NOTHROW(ParallelChecker::instance().isWriting());
  REQUIRE_NOTHROW(ParallelChecker::instance().setMode(ParallelChecker::Mode::automatic));
  REQUIRE_NOTHROW(ParallelChecker::instance().isWriting() == (parallel::size() == 1));

  auto mesh = MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>();

  const Connectivity<1>& connectivity = mesh->connectivity();

  NodeValue<double> nv{connectivity};
  REQUIRE_THROWS_WITH(parallel_check(nv, "test"), "error: parallel checker cannot be used without HDF5 support");

  REQUIRE_THROWS_WITH(parallel_check(ItemValueVariant{nv}, "test"),
                      "error: parallel checker cannot be used without HDF5 support");

  NodeArray<double> na{connectivity, 2};
  REQUIRE_THROWS_WITH(parallel_check(na, "test"), "error: parallel checker cannot be used without HDF5 support");

  REQUIRE_THROWS_WITH(parallel_check(ItemArrayVariant{na}, "test"),
                      "error: parallel checker cannot be used without HDF5 support");

  NodeValuePerCell<double> nvpc{connectivity};
  REQUIRE_THROWS_WITH(parallel_check(nvpc, "test"), "error: parallel checker cannot be used without HDF5 support");

  REQUIRE_THROWS_WITH(parallel_check(SubItemValuePerItemVariant{nvpc}, "test"),
                      "error: parallel checker cannot be used without HDF5 support");

  NodeArrayPerCell<double> napc{connectivity, 2};
  REQUIRE_THROWS_WITH(parallel_check(napc, "test"), "error: parallel checker cannot be used without HDF5 support");

  REQUIRE_THROWS_WITH(parallel_check(SubItemArrayPerItemVariant{napc}, "test"),
                      "error: parallel checker cannot be used without HDF5 support");

  REQUIRE_NOTHROW(ParallelChecker::destroy());
}

#endif   // PUGS_HAS_HDF5
