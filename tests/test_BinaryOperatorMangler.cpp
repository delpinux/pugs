#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/BinaryOperatorMangler.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("BinaryOperatorMangler", "[language]")
{
  SECTION("binary operators")
  {
    const ASTNodeDataType R = ASTNodeDataType::build<ASTNodeDataType::double_t>();
    const ASTNodeDataType Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

    REQUIRE(binaryOperatorMangler<language::multiply_op>(R, Z) == "R * Z");
    REQUIRE(binaryOperatorMangler<language::divide_op>(R, Z) == "R / Z");
    REQUIRE(binaryOperatorMangler<language::plus_op>(R, Z) == "R + Z");
    REQUIRE(binaryOperatorMangler<language::minus_op>(R, Z) == "R - Z");
    REQUIRE(binaryOperatorMangler<language::or_op>(R, Z) == "R or Z");
    REQUIRE(binaryOperatorMangler<language::and_op>(R, Z) == "R and Z");
    REQUIRE(binaryOperatorMangler<language::xor_op>(R, Z) == "R xor Z");
    REQUIRE(binaryOperatorMangler<language::greater_op>(R, Z) == "R > Z");
    REQUIRE(binaryOperatorMangler<language::greater_or_eq_op>(R, Z) == "R >= Z");
    REQUIRE(binaryOperatorMangler<language::lesser_op>(R, Z) == "R < Z");
    REQUIRE(binaryOperatorMangler<language::lesser_or_eq_op>(R, Z) == "R <= Z");
    REQUIRE(binaryOperatorMangler<language::eqeq_op>(R, Z) == "R == Z");
    REQUIRE(binaryOperatorMangler<language::not_eq_op>(R, Z) == "R != Z");
    REQUIRE(binaryOperatorMangler<language::shift_left_op>(R, Z) == "R << Z");
    REQUIRE(binaryOperatorMangler<language::shift_right_op>(R, Z) == "R >> Z");
  }
}
