#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/CRSGraph.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("CRSGraph", "[utils]")
{
  Array<int> entries{5};
  Array<int> neighbors{9};

  entries[0]   = 0;
  neighbors[0] = 0;
  neighbors[1] = 1;

  entries[1]   = 2;
  neighbors[2] = 1;
  neighbors[3] = 3;

  entries[2]   = 4;
  neighbors[4] = 2;
  neighbors[5] = 1;
  neighbors[6] = 3;

  entries[3]   = 7;
  neighbors[7] = 0;
  neighbors[8] = 1;

  entries[4] = 9;

  CRSGraph graph(entries, neighbors);

  REQUIRE(graph.numberOfNodes() == 4);

  REQUIRE(entries.size() == graph.entries().size());
  REQUIRE(&entries[0] == &graph.entries()[0]);
  REQUIRE(neighbors.size() == graph.neighbors().size());
  REQUIRE(&neighbors[0] == &graph.neighbors()[0]);
}
