#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <test_BinaryExpressionProcessor_utils.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("BinaryExpressionProcessor equality", "[language]")
{
  SECTION("==")
  {
    SECTION("lhs is B")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true == true;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false == true;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true == false;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false == false;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let b:B, b = true == n;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = true == n;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = false == n;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 0; let b:B, b = false == n;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true == 3;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true == 1;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false == 0;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false == 1;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false == 1.;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true == 1.;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false == 0.;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true == -1.7;)", "b", false);
    }

    SECTION("lhs is N")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = n == true;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 0; let b:B, b = n == true;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = n == false;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 0; let b:B, b = n == false;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let m:N, m = 2; let b:B, b = n == m;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let m:N, m = 2; let b:B, b = n == m;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let b:B, b = n == 4;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let b:B, b = n == 5;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = n == 2.3;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = n == 1.;)", "b", true);
    }

    SECTION("lhs is Z")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 1 == true;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 1 == false;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 0 == true;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 0 == false;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let b:B, b = -3 == n;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 3; let b:B, b = 3 == n;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 1 == 2;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 11 == 11;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 3 == 2.5;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 3 == 3.;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 3 == 3.5;)", "b", false);
    }

    SECTION("lhs is R")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = -1.2 == true;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 1. == true;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 0.1 == false;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 0. == false;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let b:B, b = -1.2 == n;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let b:B, b = 2. == n;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = -1.2 == 1;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = -2. == -2;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = -1.2 == 2.3;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = -1.2 == -1.2;)", "b", true);
    }
  }

  SECTION("!=")
  {
    SECTION("lhs is B")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true != true;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false != true;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true != false;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false != false;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let b:B, b = true != n;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = true != n;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = false != n;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 0; let b:B, b = false != n;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true != 3;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true != 1;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false != 0;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false != 1;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false != 1.;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true != 1.;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false != 0.;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true != -1.7;)", "b", true);
    }

    SECTION("lhs is N")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = n != true;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 0; let b:B, b = n != true;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = n != false;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 0; let b:B, b = n != false;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let m:N, m = 2; let b:B, b = n != m;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let m:N, m = 2; let b:B, b = n != m;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let b:B, b = n != 4;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let b:B, b = n != 5;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = n != 2.3;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let b:B, b = n != 1.;)", "b", false);
    }

    SECTION("lhs is Z")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 1 != true;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 1 != false;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 0 != true;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 0 != false;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let b:B, b = -3 != n;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 3; let b:B, b = 3 != n;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 1 != 2;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 11 != 11;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 3 != 2.5;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 3 != 3.;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 3 != 3.5;)", "b", true);
    }

    SECTION("lhs is R")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = -1.2 != true;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 1. != true;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 0.1 != false;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = 0. != false;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let b:B, b = -1.2 != n;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let b:B, b = 2. != n;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = -1.2 != 1;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = -2. != -2;)", "b", false);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = -1.2 != 2.3;)", "b", true);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = -1.2 != -1.2;)", "b", false);
    }
  }
}
