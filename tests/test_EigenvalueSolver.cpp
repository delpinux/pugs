#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <algebra/CRSMatrix.hpp>
#include <algebra/CRSMatrixDescriptor.hpp>
#include <algebra/EigenvalueSolver.hpp>
#include <algebra/SmallMatrix.hpp>

#include <utils/pugs_config.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("EigenvalueSolver", "[algebra]")
{
  SECTION("symmetric system")
  {
    SECTION("SLEPc")
    {
      EigenvalueSolverOptions options;
      options.library() = ESLibrary::slepsc;

      SECTION("Sparse Matrices")
      {
        Array<int> non_zeros(3);
        non_zeros.fill(3);
        CRSMatrixDescriptor<double> S{3, 3, non_zeros};

        S(0, 0) = 3;
        S(0, 1) = 2;
        S(0, 2) = 4;

        S(1, 0) = 2;
        S(1, 1) = 0;
        S(1, 2) = 2;

        S(2, 0) = 4;
        S(2, 1) = 2;
        S(2, 2) = 3;

        CRSMatrix A{S.getCRSMatrix()};

        SECTION("eigenvalues")
        {
          SmallArray<double> eigenvalues;

#ifdef PUGS_HAS_SLEPC
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues);
          REQUIRE(eigenvalues[0] == Catch::Approx(-1));
          REQUIRE(eigenvalues[1] == Catch::Approx(-1));
          REQUIRE(eigenvalues[2] == Catch::Approx(8));
#else    //  PUGS_HAS_SLEPC
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues),
                              "error: SLEPSc is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_SLEPC
        }

        SECTION("eigenvalues and eigenvectors")
        {
          SmallArray<double> eigenvalue_list;
          std::vector<SmallVector<double>> eigenvector_list;

#ifdef PUGS_HAS_SLEPC
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> P{3};
          SmallMatrix<double> PT{3};
          SmallMatrix<double> D{3};
          D = zero;

          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              P(i, j)  = eigenvector_list[j][i];
              PT(i, j) = eigenvector_list[i][j];
            }
            D(i, i) = eigenvalue_list[i];
          }

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - S(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_SLEPC
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list),
                              "error: SLEPSc is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_SLEPC
        }

        SECTION("eigenvalues and transition matrix")
        {
          SmallArray<double> eigenvalue_list;
          SmallMatrix<double> P{};

#ifdef PUGS_HAS_SLEPC
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> D{3};
          D = zero;
          for (size_t i = 0; i < 3; ++i) {
            D(i, i) = eigenvalue_list[i];
          }
          SmallMatrix PT = transpose(P);

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - S(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_SLEPC
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P),
                              "error: SLEPSc is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_SLEPC
        }
      }

      SECTION("SmallMatrix")
      {
        SmallMatrix<double> A{3, 3};
        A.fill(0);
        A(0, 0) = 3;
        A(0, 1) = 2;
        A(0, 2) = 4;

        A(1, 0) = 2;
        A(1, 1) = 0;
        A(1, 2) = 2;

        A(2, 0) = 4;
        A(2, 1) = 2;
        A(2, 2) = 3;

        SECTION("eigenvalues")
        {
          SmallArray<double> eigenvalues;

#ifdef PUGS_HAS_SLEPC
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues);
          REQUIRE(eigenvalues[0] == Catch::Approx(-1));
          REQUIRE(eigenvalues[1] == Catch::Approx(-1));
          REQUIRE(eigenvalues[2] == Catch::Approx(8));
#else    //  PUGS_HAS_SLEPC
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues),
                              "error: SLEPSc is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_SLEPC
        }

        SECTION("eigenvalues and eigenvectors")
        {
          SmallArray<double> eigenvalue_list;
          std::vector<SmallVector<double>> eigenvector_list;

#ifdef PUGS_HAS_SLEPC
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> P{3};
          SmallMatrix<double> PT{3};
          SmallMatrix<double> D{3};
          D = zero;

          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              P(i, j)  = eigenvector_list[j][i];
              PT(i, j) = eigenvector_list[i][j];
            }
            D(i, i) = eigenvalue_list[i];
          }

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - A(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_SLEPC
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list),
                              "error: SLEPSc is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_SLEPC
        }

        SECTION("eigenvalues and transition matrix")
        {
          SmallArray<double> eigenvalue_list;
          SmallMatrix<double> P{};

#ifdef PUGS_HAS_SLEPC
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> D{3};
          D = zero;
          for (size_t i = 0; i < 3; ++i) {
            D(i, i) = eigenvalue_list[i];
          }
          SmallMatrix PT = transpose(P);

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - A(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_SLEPC
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P),
                              "error: SLEPSc is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_SLEPC
        }
      }

      SECTION("TinyMatrix")
      {
        TinyMatrix<3> A = zero;

        A(0, 0) = 3;
        A(0, 1) = 2;
        A(0, 2) = 4;

        A(1, 0) = 2;
        A(1, 1) = 0;
        A(1, 2) = 2;

        A(2, 0) = 4;
        A(2, 1) = 2;
        A(2, 2) = 3;

        SECTION("eigenvalues")
        {
          SmallArray<double> eigenvalues;

#ifdef PUGS_HAS_SLEPC
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues);
          REQUIRE(eigenvalues[0] == Catch::Approx(-1));
          REQUIRE(eigenvalues[1] == Catch::Approx(-1));
          REQUIRE(eigenvalues[2] == Catch::Approx(8));
#else    //  PUGS_HAS_SLEPC
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues),
                              "error: SLEPSc is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_SLEPC
        }

        SECTION("eigenvalues and eigenvectors")
        {
          SmallArray<double> eigenvalue_list;
          std::vector<SmallVector<double>> eigenvector_list;

#ifdef PUGS_HAS_SLEPC
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> P{3};
          SmallMatrix<double> PT{3};
          SmallMatrix<double> D{3};
          D = zero;

          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              P(i, j)  = eigenvector_list[j][i];
              PT(i, j) = eigenvector_list[i][j];
            }
            D(i, i) = eigenvalue_list[i];
          }

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - A(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_SLEPC
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list),
                              "error: SLEPSc is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_SLEPC
        }

        SECTION("eigenvalues and transition matrix")
        {
          SmallArray<double> eigenvalue_list;
          SmallMatrix<double> P{};

#ifdef PUGS_HAS_SLEPC
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> D{3};
          D = zero;
          for (size_t i = 0; i < 3; ++i) {
            D(i, i) = eigenvalue_list[i];
          }
          SmallMatrix PT = transpose(P);

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - A(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_SLEPC
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P),
                              "error: SLEPSc is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_SLEPC
        }
      }
    }

    SECTION("Eigen3")
    {
      EigenvalueSolverOptions options;
      options.library() = ESLibrary::eigen3;

      SECTION("Sparse Matrices")
      {
        Array<int> non_zeros(3);
        non_zeros.fill(3);
        CRSMatrixDescriptor<double> S{3, 3, non_zeros};

        S(0, 0) = 3;
        S(0, 1) = 2;
        S(0, 2) = 4;

        S(1, 0) = 2;
        S(1, 1) = 0;
        S(1, 2) = 2;

        S(2, 0) = 4;
        S(2, 1) = 2;
        S(2, 2) = 3;

        CRSMatrix A{S.getCRSMatrix()};

        SECTION("eigenvalues")
        {
          SmallArray<double> eigenvalues;

#ifdef PUGS_HAS_EIGEN3
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues);
          REQUIRE(eigenvalues[0] == Catch::Approx(-1));
          REQUIRE(eigenvalues[1] == Catch::Approx(-1));
          REQUIRE(eigenvalues[2] == Catch::Approx(8));
#else    //  PUGS_HAS_EIGEN3
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues),
                              "error: Eigen3 is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_EIGEN3
        }

        SECTION("eigenvalues and eigenvectors")
        {
          SmallArray<double> eigenvalue_list;
          std::vector<SmallVector<double>> eigenvector_list;

#ifdef PUGS_HAS_EIGEN3
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> P{3};
          SmallMatrix<double> PT{3};
          SmallMatrix<double> D{3};
          D = zero;

          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              P(i, j)  = eigenvector_list[j][i];
              PT(i, j) = eigenvector_list[i][j];
            }
            D(i, i) = eigenvalue_list[i];
          }

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - S(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_EIGEN3
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list),
                              "error: Eigen3 is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_EIGEN3
        }

        SECTION("eigenvalues and transition matrix")
        {
          SmallArray<double> eigenvalue_list;
          SmallMatrix<double> P{};

#ifdef PUGS_HAS_EIGEN3
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> D{3};
          D = zero;
          for (size_t i = 0; i < 3; ++i) {
            D(i, i) = eigenvalue_list[i];
          }
          SmallMatrix PT = transpose(P);

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - S(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_EIGEN3
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P),
                              "error: Eigen3 is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_EIGEN3
        }
      }

      SECTION("SmallMatrix")
      {
        SmallMatrix<double> A{3, 3};
        A.fill(0);
        A(0, 0) = 3;
        A(0, 1) = 2;
        A(0, 2) = 4;

        A(1, 0) = 2;
        A(1, 1) = 0;
        A(1, 2) = 2;

        A(2, 0) = 4;
        A(2, 1) = 2;
        A(2, 2) = 3;

        SECTION("eigenvalues")
        {
          SmallArray<double> eigenvalues;

#ifdef PUGS_HAS_EIGEN3
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues);
          REQUIRE(eigenvalues[0] == Catch::Approx(-1));
          REQUIRE(eigenvalues[1] == Catch::Approx(-1));
          REQUIRE(eigenvalues[2] == Catch::Approx(8));
#else    //  PUGS_HAS_EIGEN3
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues),
                              "error: Eigen3 is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_EIGEN3
        }

        SECTION("eigenvalues and eigenvectors")
        {
          SmallArray<double> eigenvalue_list;
          std::vector<SmallVector<double>> eigenvector_list;

#ifdef PUGS_HAS_EIGEN3
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> P{3};
          SmallMatrix<double> PT{3};
          SmallMatrix<double> D{3};
          D = zero;

          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              P(i, j)  = eigenvector_list[j][i];
              PT(i, j) = eigenvector_list[i][j];
            }
            D(i, i) = eigenvalue_list[i];
          }

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - A(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_EIGEN3
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list),
                              "error: Eigen3 is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_EIGEN3
        }

        SECTION("eigenvalues and transition matrix")
        {
          SmallArray<double> eigenvalue_list;
          SmallMatrix<double> P{};

#ifdef PUGS_HAS_EIGEN3
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> D{3};
          D = zero;
          for (size_t i = 0; i < 3; ++i) {
            D(i, i) = eigenvalue_list[i];
          }
          SmallMatrix PT = transpose(P);

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - A(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_EIGEN3
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P),
                              "error: Eigen3 is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_EIGEN3
        }
      }

      SECTION("TinyMatrix")
      {
        TinyMatrix<3> A = zero;

        A(0, 0) = 3;
        A(0, 1) = 2;
        A(0, 2) = 4;

        A(1, 0) = 2;
        A(1, 1) = 0;
        A(1, 2) = 2;

        A(2, 0) = 4;
        A(2, 1) = 2;
        A(2, 2) = 3;

        SECTION("eigenvalues")
        {
          SmallArray<double> eigenvalues;

#ifdef PUGS_HAS_EIGEN3
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues);
          REQUIRE(eigenvalues[0] == Catch::Approx(-1));
          REQUIRE(eigenvalues[1] == Catch::Approx(-1));
          REQUIRE(eigenvalues[2] == Catch::Approx(8));
#else    //  PUGS_HAS_EIGEN3
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalues),
                              "error: Eigen3 is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_EIGEN3
        }

        SECTION("eigenvalues and eigenvectors")
        {
          SmallArray<double> eigenvalue_list;
          std::vector<SmallVector<double>> eigenvector_list;

#ifdef PUGS_HAS_EIGEN3
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> P{3};
          SmallMatrix<double> PT{3};
          SmallMatrix<double> D{3};
          D = zero;

          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              P(i, j)  = eigenvector_list[j][i];
              PT(i, j) = eigenvector_list[i][j];
            }
            D(i, i) = eigenvalue_list[i];
          }

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - A(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_EIGEN3
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, eigenvector_list),
                              "error: Eigen3 is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_EIGEN3
        }

        SECTION("eigenvalues and transition matrix")
        {
          SmallArray<double> eigenvalue_list;
          SmallMatrix<double> P{};

#ifdef PUGS_HAS_EIGEN3
          EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P);

          REQUIRE(eigenvalue_list[0] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[1] == Catch::Approx(-1));
          REQUIRE(eigenvalue_list[2] == Catch::Approx(8));

          SmallMatrix<double> D{3};
          D = zero;
          for (size_t i = 0; i < 3; ++i) {
            D(i, i) = eigenvalue_list[i];
          }
          SmallMatrix PT = transpose(P);

          SmallMatrix PDPT = P * D * PT;
          for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 3; ++j) {
              REQUIRE(PDPT(i, j) - A(i, j) == Catch::Approx(0).margin(1E-13));
            }
          }
#else    //  PUGS_HAS_EIGEN3
          REQUIRE_THROWS_WITH(EigenvalueSolver{options}.computeForSymmetricMatrix(A, eigenvalue_list, P),
                              "error: Eigen3 is not linked to pugs. Cannot use it!");
#endif   // PUGS_HAS_EIGEN3
        }
      }
    }
  }
}
