#ifndef FIXTURES_FOR_BUILTIN_T_HPP
#define FIXTURES_FOR_BUILTIN_T_HPP

#include <language/utils/ASTNodeDataTypeTraits.hpp>

#include <memory>
#include <stdexcept>

template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const double>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("builtin_t");
inline const auto builtin_data_type = ast_node_data_type_from<std::shared_ptr<const double>>;

inline std::shared_ptr<const double>
operator-(const std::shared_ptr<const double>& p_a)
{
  return std::make_shared<double>(-*p_a);
}

inline std::shared_ptr<const double>
operator+(const std::shared_ptr<const double>& p_a, const std::shared_ptr<const double>& p_b)
{
  return std::make_shared<double>(*p_a + *p_b);
}

inline std::shared_ptr<const double>
operator+(std::shared_ptr<const double> p_a, double b)
{
  return std::make_shared<double>(*p_a + b);
}

inline std::shared_ptr<const double>
operator+(double a, const std::shared_ptr<const double>& p_b)
{
  return std::make_shared<double>(a + *p_b);
}

inline std::shared_ptr<const double>
operator/(const std::shared_ptr<const double>&, const std::shared_ptr<const double>&)
{
  throw std::runtime_error("runtime error both");
}

inline std::shared_ptr<const double>
operator/(const std::shared_ptr<const double>&, double)
{
  throw std::runtime_error("runtime error lhs");
}

inline std::shared_ptr<const double>
operator/(double, const std::shared_ptr<const double>&)
{
  throw std::runtime_error("runtime error rhs");
}

inline std::shared_ptr<const double>
operator<<(const std::shared_ptr<const double>& p_a, const std::shared_ptr<const double>& p_b)
{
  return std::make_shared<double>(static_cast<int>(*p_a) << static_cast<int>(*p_b));
}

inline std::shared_ptr<const double>
operator>>(const std::shared_ptr<const double>& p_a, const std::shared_ptr<const double>& p_b)
{
  return std::make_shared<double>(static_cast<int>(*p_a) >> static_cast<int>(*p_b));
}

#endif   // FIXTURES_FOR_BUILTIN_T_HPP
