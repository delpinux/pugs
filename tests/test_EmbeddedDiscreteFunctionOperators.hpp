#ifndef TEST_EMBEDDED_DISCRETE_FUNCTION_OPERATORS_HPP
#define TEST_EMBEDDED_DISCRETE_FUNCTION_OPERATORS_HPP

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>

#include <language/utils/EmbeddedDiscreteFunctionOperators.hpp>
#include <scheme/DiscreteFunctionP0.hpp>
#include <scheme/DiscreteFunctionP0Vector.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>

// clazy:excludeall=non-pod-global-static

#define CHECK_EMBEDDED_VH2_TO_VH(P_U, OPERATOR, P_V, U_TYPE, V_TYPE, U_OP_V_TYPE) \
  {                                                                               \
    std::shared_ptr p_u_op_v = P_U OPERATOR P_V;                                  \
                                                                                  \
    REQUIRE(p_u_op_v.use_count() > 0);                                            \
                                                                                  \
    auto u_op_v = p_u_op_v->get<U_OP_V_TYPE>().cellValues();                      \
                                                                                  \
    auto u_values = P_U->get<U_TYPE>().cellValues();                              \
    auto v_values = P_V->get<V_TYPE>().cellValues();                              \
    bool is_same  = true;                                                         \
    for (CellId cell_id = 0; cell_id < u_values.numberOfItems(); ++cell_id) {     \
      if (u_op_v[cell_id] != (u_values[cell_id] OPERATOR v_values[cell_id])) {    \
        is_same = false;                                                          \
        break;                                                                    \
      }                                                                           \
    }                                                                             \
                                                                                  \
    REQUIRE(is_same);                                                             \
  }

#define CHECK_EMBEDDED_VECTOR_VH2_TO_VH(P_U, OPERATOR, P_V, TYPE)                                \
  {                                                                                              \
    std::shared_ptr p_u_op_v = P_U OPERATOR P_V;                                                 \
                                                                                                 \
    REQUIRE(p_u_op_v.use_count() > 0);                                                           \
                                                                                                 \
    auto u_op_v_arrays = p_u_op_v->get<TYPE>().cellArrays();                                     \
                                                                                                 \
    auto u_arrays = P_U->get<TYPE>().cellArrays();                                               \
    auto v_arrays = P_V->get<TYPE>().cellArrays();                                               \
                                                                                                 \
    REQUIRE(u_arrays.sizeOfArrays() > 0);                                                        \
    REQUIRE(u_arrays.sizeOfArrays() == v_arrays.sizeOfArrays());                                 \
    REQUIRE(u_arrays.sizeOfArrays() == u_op_v_arrays.sizeOfArrays());                            \
                                                                                                 \
    bool is_same = true;                                                                         \
    for (CellId cell_id = 0; cell_id < u_arrays.numberOfItems(); ++cell_id) {                    \
      for (size_t i = 0; i < u_arrays.sizeOfArrays(); ++i) {                                     \
        if (u_op_v_arrays[cell_id][i] != (u_arrays[cell_id][i] OPERATOR v_arrays[cell_id][i])) { \
          is_same = false;                                                                       \
          break;                                                                                 \
        }                                                                                        \
      }                                                                                          \
    }                                                                                            \
                                                                                                 \
    REQUIRE(is_same);                                                                            \
  }

#define CHECK_EMBEDDED_VHxX_TO_VH(P_U, OPERATOR, V, U_TYPE, U_OP_V_TYPE)      \
  {                                                                           \
    std::shared_ptr p_u_op_v = P_U OPERATOR V;                                \
                                                                              \
    REQUIRE(p_u_op_v.use_count() > 0);                                        \
                                                                              \
    auto u_op_v = p_u_op_v->get<U_OP_V_TYPE>().cellValues();                  \
                                                                              \
    auto u_values = P_U->get<U_TYPE>().cellValues();                          \
    bool is_same  = true;                                                     \
    for (CellId cell_id = 0; cell_id < u_values.numberOfItems(); ++cell_id) { \
      if (u_op_v[cell_id] != (u_values[cell_id] OPERATOR V)) {                \
        is_same = false;                                                      \
        break;                                                                \
      }                                                                       \
    }                                                                         \
                                                                              \
    REQUIRE(is_same);                                                         \
  }

#define CHECK_EMBEDDED_XxVH_TO_VH(U, OPERATOR, P_V, V_TYPE, U_OP_V_TYPE)      \
  {                                                                           \
    std::shared_ptr p_u_op_v = U OPERATOR P_V;                                \
                                                                              \
    REQUIRE(p_u_op_v.use_count() > 0);                                        \
                                                                              \
    auto u_op_v = p_u_op_v->get<U_OP_V_TYPE>().cellValues();                  \
                                                                              \
    auto v_values = P_V->get<V_TYPE>().cellValues();                          \
    bool is_same  = true;                                                     \
    for (CellId cell_id = 0; cell_id < v_values.numberOfItems(); ++cell_id) { \
      if (u_op_v[cell_id] != (U OPERATOR v_values[cell_id])) {                \
        is_same = false;                                                      \
        break;                                                                \
      }                                                                       \
    }                                                                         \
                                                                              \
    REQUIRE(is_same);                                                         \
  }

#define CHECK_EMBEDDED_VECTOR_XxVH_TO_VH(U, OPERATOR, P_V, TYPE)                                 \
  {                                                                                              \
    std::shared_ptr p_u_op_v = U OPERATOR P_V;                                                   \
                                                                                                 \
    REQUIRE(p_u_op_v.use_count() > 0);                                                           \
                                                                                                 \
    auto u_op_v = p_u_op_v->get<TYPE>().cellArrays();                                            \
                                                                                                 \
    auto v_arrays = P_V->get<TYPE>().cellArrays();                                               \
                                                                                                 \
    REQUIRE(v_arrays.numberOfItems() == u_op_v.numberOfItems());                                 \
    REQUIRE(v_arrays.sizeOfArrays() == u_op_v.sizeOfArrays());                                   \
                                                                                                 \
    bool is_same = true;                                                                         \
    for (CellId cell_id = 0; cell_id < v_arrays.numberOfItems(); ++cell_id) {                    \
      for (size_t i = 0; i < v_arrays.sizeOfArrays(); ++i) {                                     \
        if (u_op_v[cell_id][i] != (U OPERATOR v_arrays[cell_id][i])) {                           \
          is_same = false;                                                                       \
          std::clog << u_op_v[cell_id][i] << " !=" << (U OPERATOR v_arrays[cell_id][i]) << '\n'; \
        }                                                                                        \
      }                                                                                          \
    }                                                                                            \
                                                                                                 \
    REQUIRE(is_same);                                                                            \
  }

#define CHECK_EMBEDDED_VH_TO_VH(OPERATOR, P_U, U_TYPE)                        \
  {                                                                           \
    std::shared_ptr p_op_u = OPERATOR P_U;                                    \
                                                                              \
    REQUIRE(p_op_u.use_count() > 0);                                          \
                                                                              \
    auto op_u = p_op_u->get<U_TYPE>().cellValues();                           \
                                                                              \
    auto u_values = P_U->get<U_TYPE>().cellValues();                          \
    bool is_same  = true;                                                     \
    for (CellId cell_id = 0; cell_id < u_values.numberOfItems(); ++cell_id) { \
      if (op_u[cell_id] != (OPERATOR u_values[cell_id])) {                    \
        is_same = false;                                                      \
        break;                                                                \
      }                                                                       \
    }                                                                         \
                                                                              \
    REQUIRE(is_same);                                                         \
  }

#define CHECK_EMBEDDED_VECTOR_VH_TO_VH(OPERATOR, P_U, TYPE)                   \
  {                                                                           \
    std::shared_ptr p_op_u = OPERATOR P_U;                                    \
                                                                              \
    REQUIRE(p_op_u.use_count() > 0);                                          \
                                                                              \
    auto op_u_arrays = p_op_u->get<TYPE>().cellArrays();                      \
                                                                              \
    auto u_arrays = P_U->get<TYPE>().cellArrays();                            \
                                                                              \
    REQUIRE(u_arrays.sizeOfArrays() == op_u_arrays.sizeOfArrays());           \
    REQUIRE(u_arrays.numberOfItems() == op_u_arrays.numberOfItems());         \
                                                                              \
    bool is_same = true;                                                      \
    for (CellId cell_id = 0; cell_id < u_arrays.numberOfItems(); ++cell_id) { \
      for (size_t i = 0; i < u_arrays.sizeOfArrays(); ++i) {                  \
        if (op_u_arrays[cell_id][i] != (OPERATOR u_arrays[cell_id][i])) {     \
          is_same = false;                                                    \
          break;                                                              \
        }                                                                     \
      }                                                                       \
    }                                                                         \
                                                                              \
    REQUIRE(is_same);                                                         \
  }

#endif   // TEST_EMBEDDED_DISCRETE_FUNCTION_OPERATORS_HPP
