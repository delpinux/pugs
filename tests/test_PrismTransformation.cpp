#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/PrismTransformation.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("PrismTransformation", "[geometry]")
{
  using R3 = TinyVector<3>;

  const R3 a_hat{0, 0, -1};
  const R3 b_hat{1, 0, -1};
  const R3 c_hat{0, 1, -1};
  const R3 d_hat{0, 0, +1};
  const R3 e_hat{1, 0, +1};
  const R3 f_hat{0, 1, +1};

  const R3 m_hat{1. / 3, 1. / 3, 0};

  const R3 a{1, 2, 0};
  const R3 b{3, 1, 3};
  const R3 c{2, 5, 2};
  const R3 d{0, 3, 1};
  const R3 e{1, 2, 5};
  const R3 f{3, 1, 7};

  const PrismTransformation t(a, b, c, d, e, f);

  SECTION("points")
  {
    REQUIRE(l2Norm(t(a_hat) - a) == Catch::Approx(0));
    REQUIRE(l2Norm(t(b_hat) - b) == Catch::Approx(0));
    REQUIRE(l2Norm(t(c_hat) - c) == Catch::Approx(0));
    REQUIRE(l2Norm(t(d_hat) - d) == Catch::Approx(0));
    REQUIRE(l2Norm(t(e_hat) - e) == Catch::Approx(0));
    REQUIRE(l2Norm(t(f_hat) - f) == Catch::Approx(0));

    R3 m = (1. / 6) * (a + b + c + d + e + f);

    REQUIRE(l2Norm(t(m_hat) - m) == Catch::Approx(0).margin(1E-14));
  }

  SECTION("Jacobian determinant")
  {
    SECTION("at points")
    {
      auto detJ = [](const R3 X) {
        const double& x = X[0];
        const double& y = X[1];
        const double& z = X[2];

        return ((2 * y + 0.5 * x + 0.5) * (z + 2) - (y - 0.5 * x - 0.5) * (2 * z + 4)) +
               (1.5 - 0.5 * z) * ((2 * y + 0.5 * x + 0.5) * (0.5 - 2.5 * z) - (0.5 - 2.5 * y) * (2 * z + 4)) +
               (0.5 * z + 3.5) * ((0.5 - 2.5 * y) * (z + 2) - (y - 0.5 * x - 0.5) * (0.5 - 2.5 * z));
      };

      REQUIRE(t.jacobianDeterminant(a_hat) == Catch::Approx(detJ(a_hat)));
      REQUIRE(t.jacobianDeterminant(b_hat) == Catch::Approx(detJ(b_hat)));
      REQUIRE(t.jacobianDeterminant(c_hat) == Catch::Approx(detJ(c_hat)));
      REQUIRE(t.jacobianDeterminant(d_hat) == Catch::Approx(detJ(d_hat)));
      REQUIRE(t.jacobianDeterminant(e_hat) == Catch::Approx(detJ(e_hat)));
      REQUIRE(t.jacobianDeterminant(f_hat) == Catch::Approx(detJ(f_hat)));

      REQUIRE(t.jacobianDeterminant(m_hat) == Catch::Approx(detJ(m_hat)));
    }

    SECTION("volume calculation")
    {
      // due to the z component of the jacobian determinant, degree 3
      // polynomials must be exactly integrated
      const QuadratureFormula<3>& gauss = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(3));

      double volume = 0;
      for (size_t i = 0; i < gauss.numberOfPoints(); ++i) {
        volume += gauss.weight(i) * t.jacobianDeterminant(gauss.point(i));
      }

      // 11/2 is actually the volume of the prism
      REQUIRE(volume == Catch::Approx(11. / 2));
    }

    SECTION("exact polynomial integration")
    {
      auto p = [](const R3& X) {
        const double x = X[0];
        const double y = X[1];
        const double z = X[2];

        return 3 * x * x + 2 * y * y + 3 * z * z + 4 * x + 3 * y + 2 * z + 1;
      };

      // 5 is the minimum quadrature rule to integrate the polynomial on the prism
      const QuadratureFormula<3>& gauss = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(5));

      double integral = 0;
      for (size_t i = 0; i < gauss.numberOfPoints(); ++i) {
        integral += gauss.weight(i) * t.jacobianDeterminant(gauss.point(i)) * p(t(gauss.point(i)));
      }

      REQUIRE(integral == Catch::Approx(30377. / 90));
    }
  }
}
