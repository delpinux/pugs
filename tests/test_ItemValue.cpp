#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemValue.hpp>
#include <mesh/Mesh.hpp>
#include <utils/Messenger.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ItemValue", "[mesh]")
{
  SECTION("default constructors")
  {
    REQUIRE_NOTHROW(NodeValue<int>{});
    REQUIRE_NOTHROW(EdgeValue<int>{});
    REQUIRE_NOTHROW(FaceValue<int>{});
    REQUIRE_NOTHROW(CellValue<int>{});

    REQUIRE(not NodeValue<int>{}.isBuilt());
    REQUIRE(not EdgeValue<int>{}.isBuilt());
    REQUIRE(not FaceValue<int>{}.isBuilt());
    REQUIRE(not CellValue<int>{}.isBuilt());
  }

  SECTION("1D")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_1d_v = named_mesh.mesh();
        auto mesh_1d   = mesh_1d_v->get<Mesh<1>>();

        const Connectivity<1>& connectivity = mesh_1d->connectivity();

        REQUIRE_NOTHROW(NodeValue<int>{connectivity});
        REQUIRE_NOTHROW(EdgeValue<int>{connectivity});
        REQUIRE_NOTHROW(FaceValue<int>{connectivity});
        REQUIRE_NOTHROW(CellValue<int>{connectivity});

        REQUIRE(NodeValue<int>{connectivity}.isBuilt());
        REQUIRE(EdgeValue<int>{connectivity}.isBuilt());
        REQUIRE(FaceValue<int>{connectivity}.isBuilt());
        REQUIRE(CellValue<int>{connectivity}.isBuilt());

        NodeValue<int> node_value{connectivity};
        EdgeValue<int> edge_value{connectivity};
        FaceValue<int> face_value{connectivity};
        CellValue<int> cell_value{connectivity};

        REQUIRE(edge_value.numberOfItems() == node_value.numberOfItems());
        REQUIRE(face_value.numberOfItems() == node_value.numberOfItems());
        REQUIRE(cell_value.numberOfItems() + 1 == node_value.numberOfItems());
      }
    }
  }

  SECTION("2D")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_2d_v = named_mesh.mesh();
        auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

        const Connectivity<2>& connectivity = mesh_2d->connectivity();

        REQUIRE_NOTHROW(NodeValue<int>{connectivity});
        REQUIRE_NOTHROW(EdgeValue<int>{connectivity});
        REQUIRE_NOTHROW(FaceValue<int>{connectivity});
        REQUIRE_NOTHROW(CellValue<int>{connectivity});

        REQUIRE(NodeValue<int>{connectivity}.isBuilt());
        REQUIRE(EdgeValue<int>{connectivity}.isBuilt());
        REQUIRE(FaceValue<int>{connectivity}.isBuilt());
        REQUIRE(CellValue<int>{connectivity}.isBuilt());

        EdgeValue<int> edge_value{connectivity};
        FaceValue<int> face_value{connectivity};

        REQUIRE(edge_value.numberOfItems() == face_value.numberOfItems());
      }
    }
  }

  SECTION("3D")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_3d_v = named_mesh.mesh();
        auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

        const Connectivity<3>& connectivity = mesh_3d->connectivity();

        REQUIRE_NOTHROW(NodeValue<int>{connectivity});
        REQUIRE_NOTHROW(EdgeValue<int>{connectivity});
        REQUIRE_NOTHROW(FaceValue<int>{connectivity});
        REQUIRE_NOTHROW(CellValue<int>{connectivity});

        REQUIRE(NodeValue<int>{connectivity}.isBuilt());
        REQUIRE(EdgeValue<int>{connectivity}.isBuilt());
        REQUIRE(FaceValue<int>{connectivity}.isBuilt());
        REQUIRE(CellValue<int>{connectivity}.isBuilt());
      }
    }
  }

  SECTION("set values from array")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_3d_v = named_mesh.mesh();
        auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

        const Connectivity<3>& connectivity = mesh_3d->connectivity();

        Array<size_t> values{connectivity.numberOfCells()};
        for (size_t i = 0; i < values.size(); ++i) {
          values[i] = i;
        }

        CellValue<size_t> cell_value{connectivity, values};

        {
          bool is_same = true;
          for (CellId i_cell = 0; i_cell < mesh_3d->numberOfCells(); ++i_cell) {
            is_same &= (cell_value[i_cell] == i_cell);
          }
          REQUIRE(is_same);
          REQUIRE(&(cell_value[CellId{0}]) == &(values[0]));
        }
      }
    }
  }

  SECTION("copy")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_3d_v = named_mesh.mesh();
        auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

        const Connectivity<3>& connectivity = mesh_3d->connectivity();

        CellValue<int> cell_value{connectivity};
        cell_value.fill(parallel::rank());

        CellValue<const int> const_cell_value;
        const_cell_value = copy(cell_value);

        cell_value.fill(0);

        {
          bool is_same = true;
          for (CellId i_cell = 0; i_cell < mesh_3d->numberOfCells(); ++i_cell) {
            is_same &= (cell_value[i_cell] == 0);
          }
          REQUIRE(is_same);
        }

        {
          bool is_same = true;
          for (CellId i_cell = 0; i_cell < mesh_3d->numberOfCells(); ++i_cell) {
            is_same &= (const_cell_value[i_cell] == static_cast<std::int64_t>(parallel::rank()));
          }
          REQUIRE(is_same);
        }
      }
    }
  }

  SECTION("WeakItemValue")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_2d_v = named_mesh.mesh();
        auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

        const Connectivity<2>& connectivity = mesh_2d->connectivity();

        WeakFaceValue<int> weak_face_value{connectivity};
        for (FaceId face_id = 0; face_id < mesh_2d->numberOfFaces(); ++face_id) {
          weak_face_value[face_id] = 2 * face_id + 1;
        }

        FaceValue<const int> face_value{weak_face_value};

        REQUIRE(face_value.connectivity_ptr() == weak_face_value.connectivity_ptr());

        FaceValue<int> copied_face_value = copy(weak_face_value);
        REQUIRE(copied_face_value.connectivity_ptr() == weak_face_value.connectivity_ptr());

        weak_face_value.fill(0);
        for (FaceId face_id = 0; face_id < mesh_2d->numberOfFaces(); ++face_id) {
          REQUIRE(face_value[face_id] == static_cast<int>(0));
        }

        {
          bool is_same = true;
          for (FaceId face_id = 0; face_id < mesh_2d->numberOfFaces(); ++face_id) {
            is_same &= (copied_face_value[face_id] == static_cast<int>(2 * face_id + 1));
          }
          REQUIRE(is_same);
        }
      }
    }
  }

  SECTION("output")
  {
    auto mesh_v = MeshDataBaseForTests::get().unordered1DMesh();
    auto mesh   = mesh_v->get<Mesh<1>>();

    Array<int> array{mesh->numberOfCells()};
    for (size_t i = 0; i < array.size(); ++i) {
      array[i] = 2 * i + 1;
    }

    CellValue<int> cell_value{mesh->connectivity(), array};

    std::ostringstream array_ost;
    array_ost << array;
    std::ostringstream cell_value_ost;
    cell_value_ost << cell_value;
    REQUIRE(array_ost.str() == cell_value_ost.str());
  }

#ifndef NDEBUG
  SECTION("error")
  {
    SECTION("checking for build ItemValue")
    {
      CellValue<int> cell_value;
      REQUIRE_THROWS_WITH(cell_value[CellId{0}], "ItemValue is not built");

      FaceValue<int> face_value;
      REQUIRE_THROWS_WITH(face_value[FaceId{0}], "ItemValue is not built");

      EdgeValue<int> edge_value;
      REQUIRE_THROWS_WITH(edge_value[EdgeId{0}], "ItemValue is not built");

      NodeValue<int> node_value;
      REQUIRE_THROWS_WITH(node_value[NodeId{0}], "ItemValue is not built");
    }

    SECTION("checking for bounds violation")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          CellValue<int> cell_value{connectivity};
          CellId invalid_cell_id = connectivity.numberOfCells();
          REQUIRE_THROWS_WITH(cell_value[invalid_cell_id], "invalid item_id");

          FaceValue<int> face_value{connectivity};
          FaceId invalid_face_id = connectivity.numberOfFaces();
          REQUIRE_THROWS_WITH(face_value[invalid_face_id], "invalid item_id");

          EdgeValue<int> edge_value{connectivity};
          EdgeId invalid_edge_id = connectivity.numberOfEdges();
          REQUIRE_THROWS_WITH(edge_value[invalid_edge_id], "invalid item_id");

          NodeValue<int> node_value{connectivity};
          NodeId invalid_node_id = connectivity.numberOfNodes();
          REQUIRE_THROWS_WITH(node_value[invalid_node_id], "invalid item_id");
        }
      }
    }

    SECTION("set values from invalid array size")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          Array<size_t> values{3 + connectivity.numberOfCells()};
          REQUIRE_THROWS_WITH(CellValue<size_t>(connectivity, values), "invalid values size");
        }
      }
    }

    SECTION("invalid copy_to")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity_2d = mesh_2d->connectivity();

          std::array mesh_3d_list = MeshDataBaseForTests::get().all3DMeshes();

          for (const auto& named_mesh_3d : mesh_3d_list) {
            SECTION(named_mesh_3d.name())
            {
              auto mesh_3d_v = named_mesh_3d.mesh();
              auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

              const Connectivity<3>& connectivity_3d = mesh_3d->connectivity();

              CellValue<int> cell_2d_value{connectivity_2d};
              CellValue<int> cell_3d_value{connectivity_3d};
              REQUIRE_THROWS_WITH(copy_to(cell_2d_value, cell_3d_value), "different connectivities");
            }
          }
        }
      }
    }
  }
#endif   // NDEBUG
}
