#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/EmbedderTable.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("BuiltinFunctionEmbedderTable", "[language]")
{
  rang::setControlMode(rang::control::Off);

  EmbedderTable<IBuiltinFunctionEmbedder> table;

  REQUIRE(table.size() == 0);

  std::shared_ptr<IBuiltinFunctionEmbedder> embedded_sin =
    std::make_shared<BuiltinFunctionEmbedder<double(double)>>([](double x) -> double { return std::sin(x); });
  table.add(embedded_sin);

  REQUIRE(table.size() == 1);

  std::shared_ptr<IBuiltinFunctionEmbedder> embedded_greater =
    std::make_shared<BuiltinFunctionEmbedder<bool(int64_t, int64_t)>>(
      [](int64_t i, int64_t j) -> bool { return i > j; });
  table.add(embedded_greater);

  REQUIRE(table.size() == 2);

  REQUIRE(table[0] == embedded_sin);
  REQUIRE(table[1] == embedded_greater);

  const auto& const_table = table;

  REQUIRE(const_table.size() == 2);

  REQUIRE(const_table[0] == embedded_sin);
  REQUIRE(const_table[1] == embedded_greater);

#ifndef NDEBUG
  REQUIRE_THROWS_AS(table[2], AssertError);
  REQUIRE_THROWS_AS(const_table[2], AssertError);
#endif   // NDEBUG
}
