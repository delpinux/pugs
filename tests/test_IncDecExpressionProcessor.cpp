#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_INC_DEC_RESULT(data, variable_name, expected_value)             \
  {                                                                           \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                \
    auto ast = ASTBuilder::build(input);                                      \
                                                                              \
    ASTExecutionStack::create();                                              \
                                                                              \
    ASTModulesImporter{*ast};                                                 \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                   \
                                                                              \
    ASTSymbolTableBuilder{*ast};                                              \
    ASTNodeDataTypeBuilder{*ast};                                             \
                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                      \
                                                                              \
    ASTNodeExpressionBuilder{*ast};                                           \
    ExecutionPolicy exec_policy;                                              \
    ast->execute(exec_policy);                                                \
                                                                              \
    auto symbol_table = ast->m_symbol_table;                                  \
                                                                              \
    using namespace TAO_PEGTL_NAMESPACE;                                      \
    position use_position{10000, 1000, 10, "fixture"};                        \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);   \
                                                                              \
    auto attributes = symbol->attributes();                                   \
    auto value      = std::get<decltype(expected_value)>(attributes.value()); \
                                                                              \
    ASTExecutionStack::destroy();                                             \
                                                                              \
    REQUIRE(value == expected_value);                                         \
    ast->m_symbol_table->clearValues();                                       \
  }

#define CHECK_INCDEC_EXPRESSION_THROWS_WITH(data, error_message)      \
  {                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};        \
    auto ast = ASTBuilder::build(input);                              \
                                                                      \
    ASTExecutionStack::create();                                      \
                                                                      \
    ASTModulesImporter{*ast};                                         \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};           \
                                                                      \
    ASTSymbolTableBuilder{*ast};                                      \
                                                                      \
    REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error_message); \
                                                                      \
    ASTExecutionStack::destroy();                                     \
    ast->m_symbol_table->clearValues();                               \
  }

#define CHECK_INCDEC_THROWS_WITH(data, error_message)              \
  {                                                                \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};     \
    auto ast = ASTBuilder::build(input);                           \
                                                                   \
    ASTExecutionStack::create();                                   \
                                                                   \
    ASTSymbolTableBuilder{*ast};                                   \
    ASTNodeDataTypeBuilder{*ast};                                  \
                                                                   \
    ASTNodeDeclarationToAffectationConverter{*ast};                \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};           \
                                                                   \
    ASTNodeExpressionBuilder{*ast};                                \
    ExecutionPolicy exec_policy;                                   \
                                                                   \
    REQUIRE_THROWS_WITH(ast->execute(exec_policy), error_message); \
                                                                   \
    ASTExecutionStack::destroy();                                  \
    ast->m_symbol_table->clearValues();                            \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("IncDecExpressionProcessor", "[language]")
{
  SECTION("pre ++")
  {
    SECTION("N")
    {
      CHECK_INC_DEC_RESULT(R"(let n:N, n = 2; ++n;)", "n", 3ul);
      CHECK_INC_DEC_RESULT(R"(let n:N, n = 2; let m:N, m = ++n;)", "m", 3ul);
    }

    SECTION("Z")
    {
      CHECK_INC_DEC_RESULT(R"(let z:Z, z = 2; ++z;)", "z", 3l);
      CHECK_INC_DEC_RESULT(R"(let z:Z, z = 2; let p:Z, p = ++z;)", "p", 3l);
    }
  }

  SECTION("pre --")
  {
    SECTION("N")
    {
      CHECK_INC_DEC_RESULT(R"(let n:N, n = 2; --n;)", "n", 1ul);
      CHECK_INC_DEC_RESULT(R"(let n:N, n = 2; let m:N, m = --n;)", "m", 1ul);
    }

    SECTION("Z")
    {
      CHECK_INC_DEC_RESULT(R"(let z:Z, z = 2; --z;)", "z", 1l);
      CHECK_INC_DEC_RESULT(R"(let z:Z, z = 2; let p:Z, p = --z;)", "p", 1l);
    }
  }

  SECTION("post ++")
  {
    SECTION("N")
    {
      CHECK_INC_DEC_RESULT(R"(let n:N, n = 2; n++;)", "n", 3ul);
      CHECK_INC_DEC_RESULT(R"(let n:N, n = 2; let m:N, m = n++;)", "m", 2ul);
    }

    SECTION("Z")
    {
      CHECK_INC_DEC_RESULT(R"(let z:Z, z = 2; z++;)", "z", 3l);
      CHECK_INC_DEC_RESULT(R"(let z:Z, z = 2; let p:Z, p = z++;)", "p", 2l);
    }
  }

  SECTION("post --")
  {
    SECTION("N")
    {
      CHECK_INC_DEC_RESULT(R"(let n:N, n = 2; n--;)", "n", 1ul);
      CHECK_INC_DEC_RESULT(R"(let n:N, n = 2; let m:N, m = n--;)", "m", 2ul);
    }

    SECTION("Z")
    {
      CHECK_INC_DEC_RESULT(R"(let z:Z, z = 2; z--;)", "z", 1l);
      CHECK_INC_DEC_RESULT(R"(let z:Z, z = 2; let p:Z, p = z--;)", "p", 2l);
    }
  }

  SECTION("errors")
  {
    SECTION("negative pre -- operator for N")
    {
      CHECK_INCDEC_THROWS_WITH(R"(let n:N, n=0;--n;)", "decrement would produce negative value");
    }

    SECTION("negative post -- operator for N")
    {
      CHECK_INCDEC_THROWS_WITH(R"(let n:N, n=0;n--;)", "decrement would produce negative value");
    }

    SECTION("undefined pre -- operator")
    {
      auto error_message = [](std::string type_name) {
        return std::string{R"(undefined increment/decrement operator
note: unexpected operand type )"} +
               type_name;
      };

      CHECK_INCDEC_EXPRESSION_THROWS_WITH(R"(--"foo";)", error_message("string"));
    }

    SECTION("undefined pre ++ operator")
    {
      auto error_message = [](std::string type_name) {
        return std::string{R"(undefined increment/decrement operator
note: unexpected operand type )"} +
               type_name;
      };

      CHECK_INCDEC_EXPRESSION_THROWS_WITH(R"(++true;)", error_message("B"));
    }

    SECTION("undefined pre ++ operator")
    {
      auto error_message = [](std::string type_name) {
        return std::string{R"(undefined increment/decrement operator
note: unexpected operand type )"} +
               type_name;
      };

      CHECK_INCDEC_EXPRESSION_THROWS_WITH(R"(++1.2;)", error_message("R"));
    }

    SECTION("undefined post -- operator")
    {
      auto error_message = [](std::string type_name) {
        return std::string{R"(undefined increment/decrement operator
note: unexpected operand type )"} +
               type_name;
      };

      CHECK_INCDEC_EXPRESSION_THROWS_WITH(R"(true--;)", error_message("B"));
    }

    SECTION("undefined post ++ operator")
    {
      auto error_message = [](std::string type_name) {
        return std::string{R"(undefined increment/decrement operator
note: unexpected operand type )"} +
               type_name;
      };

      CHECK_INCDEC_EXPRESSION_THROWS_WITH(R"("bar"++;)", error_message("string"));
    }
  }
}
