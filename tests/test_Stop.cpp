#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/ExecutionStatManager.hpp>
#include <utils/Stop.hpp>

#include <chrono>
#include <filesystem>
#include <fstream>

// clazy:excludeall=non-pod-global-static

TEST_CASE("Stop", "[utils]")
{
  ExecutionStatManager::create();
  auto stop_file = std::filesystem::current_path() / "stop";
  std::filesystem::remove(stop_file);

  REQUIRE(not std::filesystem::exists(stop_file));
  REQUIRE(not stop());

  std::ofstream{stop_file.c_str()}.put('a');
  REQUIRE(std::filesystem::exists(stop_file));

  std::filesystem::file_time_type ftime = std::filesystem::last_write_time(stop_file);

  using namespace std::chrono_literals;
  std::filesystem::last_write_time(stop_file, ftime - 1h);

  REQUIRE(not stop());

  std::filesystem::last_write_time(stop_file, ftime);
  REQUIRE(stop());

  ExecutionStatManager::destroy();
}
