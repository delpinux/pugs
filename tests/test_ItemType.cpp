#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <mesh/ItemType.hpp>
#include <utils/PugsMacros.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ItemType", "[connectivity]")
{
  ItemType node_type = ItemType::node;
  ItemType edge_type = ItemType::edge;
  ItemType face_type = ItemType::face;
  ItemType cell_type = ItemType::cell;

  SECTION("checking for item type differences")
  {
    REQUIRE(((node_type != edge_type) and (node_type != face_type) and (node_type != cell_type) and
             (edge_type != face_type) and (edge_type != cell_type) and (face_type != cell_type)));
  }

  SECTION("checking for item type names")
  {
    REQUIRE(itemName(node_type) == "node");
    REQUIRE(itemName(edge_type) == "edge");
    REQUIRE(itemName(face_type) == "face");
    REQUIRE(itemName(cell_type) == "cell");
  }

  SECTION("checking for item ids in 1d")
  {
    REQUIRE(ItemTypeId<1>::itemTId(cell_type) == 0);
    REQUIRE(ItemTypeId<1>::itemTId(face_type) == 1);
    REQUIRE(ItemTypeId<1>::itemTId(edge_type) == 1);
    REQUIRE(ItemTypeId<1>::itemTId(node_type) == 1);

    REQUIRE(ItemTypeId<1>::dimension(cell_type) == 1);
    REQUIRE(ItemTypeId<1>::dimension(face_type) == 0);
    REQUIRE(ItemTypeId<1>::dimension(edge_type) == 0);
    REQUIRE(ItemTypeId<1>::dimension(node_type) == 0);
  }

  SECTION("checking for item ids in 2d")
  {
    REQUIRE(ItemTypeId<2>::itemTId(cell_type) == 0);
    REQUIRE(ItemTypeId<2>::itemTId(face_type) == 1);
    REQUIRE(ItemTypeId<2>::itemTId(edge_type) == 1);
    REQUIRE(ItemTypeId<2>::itemTId(node_type) == 2);

    REQUIRE(ItemTypeId<2>::dimension(cell_type) == 2);
    REQUIRE(ItemTypeId<2>::dimension(face_type) == 1);
    REQUIRE(ItemTypeId<2>::dimension(edge_type) == 1);
    REQUIRE(ItemTypeId<2>::dimension(node_type) == 0);
  }

  SECTION("checking for item ids in 3d")
  {
    REQUIRE(ItemTypeId<3>::itemTId(cell_type) == 0);
    REQUIRE(ItemTypeId<3>::itemTId(face_type) == 1);
    REQUIRE(ItemTypeId<3>::itemTId(edge_type) == 2);
    REQUIRE(ItemTypeId<3>::itemTId(node_type) == 3);

    REQUIRE(ItemTypeId<3>::dimension(cell_type) == 3);
    REQUIRE(ItemTypeId<3>::dimension(face_type) == 2);
    REQUIRE(ItemTypeId<3>::dimension(edge_type) == 1);
    REQUIRE(ItemTypeId<3>::dimension(node_type) == 0);
  }
}
