#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <language/modules/SocketModule.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <utils/Socket.hpp>

#include <thread>

// clazy:excludeall=non-pod-global-static

TEST_CASE("SocketModule", "[language]")
{
  SocketModule socket_module;
  const auto& name_builtin_function = socket_module.getNameBuiltinFunctionMap();

  REQUIRE(name_builtin_function.size() == 25);

  auto create_socket_server = [&name_builtin_function] {
    auto i_function = name_builtin_function.find("createSocketServer:N");
    REQUIRE(i_function != name_builtin_function.end());

    uint64_t arg            = 0;
    DataVariant arg_variant = arg;

    IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
    DataVariant result_variant                  = function_embedder.apply({arg_variant});

    REQUIRE_NOTHROW(dynamic_cast<const DataHandler<const Socket>&>(std::get<EmbeddedData>(result_variant).get()));

    return dynamic_cast<const DataHandler<const Socket>&>(std::get<EmbeddedData>(result_variant).get()).data_ptr();
  };

  auto accept_socket_client = [&name_builtin_function](const std::shared_ptr<const Socket>& server) {
    auto i_function = name_builtin_function.find("acceptSocketClient:socket");
    REQUIRE(i_function != name_builtin_function.end());

    DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(server));

    IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
    DataVariant result_variant                  = function_embedder.apply({arg_variant});

    REQUIRE_NOTHROW(dynamic_cast<const DataHandler<const Socket>&>(std::get<EmbeddedData>(result_variant).get()));

    return dynamic_cast<const DataHandler<const Socket>&>(std::get<EmbeddedData>(result_variant).get()).data_ptr();
  };

  auto connect_socket_server = [&name_builtin_function](const std::string& hostname, int64_t port) {
    auto i_function = name_builtin_function.find("connectSocketServer:string*N");
    if (i_function == name_builtin_function.end()) {
      FAIL_CHECK("Cannot connect to server. This should NEVER happen");
    }

    DataVariant hostname_variant = hostname;
    DataVariant port_variant     = port;

    IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
    DataVariant result_variant                  = function_embedder.apply({hostname_variant, port_variant});

    return dynamic_cast<const DataHandler<const Socket>&>(std::get<EmbeddedData>(result_variant).get()).data_ptr();
  };

  SECTION("read")
  {
    SECTION("read_B")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        bool b = true;
        write(*self_server, b);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("read_B:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      REQUIRE(std::get<bool>(result_variant) == true);

      t.join();
    }

    SECTION("read_N")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        uint64_t n = 12;
        write(*self_server, n);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("read_N:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      REQUIRE(std::get<uint64_t>(result_variant) == 12);

      t.join();
    }

    SECTION("read_Z")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        int64_t k = -3;
        write(*self_server, k);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("read_Z:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      REQUIRE(std::get<int64_t>(result_variant) == -3);

      t.join();
    }

    SECTION("read_R")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        double x = -3.141;
        write(*self_server, x);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("read_R:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      REQUIRE(std::get<double>(result_variant) == -3.141);

      t.join();
    }

    SECTION("read_R1")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyVector<1> x{1.414};
        write(*self_server, x);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("read_R1:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      REQUIRE(std::get<TinyVector<1>>(result_variant) == TinyVector<1>{1.414});

      t.join();
    }

    SECTION("read_R2")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyVector<2> x{1.414, -3.7};
        write(*self_server, x);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("read_R2:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      REQUIRE(std::get<TinyVector<2>>(result_variant) == TinyVector<2>{1.414, -3.7});

      t.join();
    }

    SECTION("read_R3")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyVector<3> x{1.414, -3.7, 5.19};
        write(*self_server, x);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("read_R3:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      REQUIRE(std::get<TinyVector<3>>(result_variant) == TinyVector<3>{1.414, -3.7, 5.19});

      t.join();
    }

    SECTION("read_R1x1")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyMatrix<1> x{1.414};
        write(*self_server, x);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("read_R1x1:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      REQUIRE(std::get<TinyMatrix<1>>(result_variant) == TinyMatrix<1>{1.414});

      t.join();
    }

    SECTION("read_R2x2")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyMatrix<2> x{1.414, -3.7, 2.4, -6};
        write(*self_server, x);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("read_R2x2:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      REQUIRE(std::get<TinyMatrix<2>>(result_variant) == TinyMatrix<2>{1.414, -3.7, 2.4, -6});

      t.join();
    }

    SECTION("read_R3x3")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyMatrix<3> x{1.414, -3.7, 5.19, 2, 0, -2.6, 1.2, -6, 9.3};
        write(*self_server, x);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("read_R3x3:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      REQUIRE(std::get<TinyMatrix<3>>(result_variant) == TinyMatrix<3>{1.414, -3.7, 5.19, 2, 0, -2.6, 1.2, -6, 9.3});

      t.join();
    }

    SECTION("read_string")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        std::string s = "foobar";
        write(*self_server, s.size());
        write(*self_server, s);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("read_string:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      REQUIRE(std::get<std::string>(result_variant) == std::string{"foobar"});

      t.join();
    }
  }

  SECTION("write")
  {
    auto get_result = [&name_builtin_function](std::shared_ptr<const Socket> p_client) {
      auto i_function = name_builtin_function.find("read_B:socket");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant arg_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});
      return std::get<bool>(result_variant);
    };

    SECTION("B")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        bool b;
        read(*self_server, b);
        const bool result = (b == true);
        write(*self_server, result);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("write:socket*B");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant socket_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));
      DataVariant value_variant  = true;

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({socket_variant, value_variant});

      REQUIRE(get_result(p_client));

      t.join();
    }

    SECTION("N")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        uint64_t n;
        read(*self_server, n);
        const bool result = (n == 17);
        write(*self_server, result);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("write:socket*N");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant socket_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));
      DataVariant value_variant  = uint64_t{17};

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({socket_variant, value_variant});

      REQUIRE(get_result(p_client));

      t.join();
    }

    SECTION("Z")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        int64_t k;
        read(*self_server, k);
        const bool result = (k == -13);
        write(*self_server, result);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("write:socket*Z");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant socket_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));
      DataVariant value_variant  = int64_t{-13};

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({socket_variant, value_variant});

      REQUIRE(get_result(p_client));

      t.join();
    }

    SECTION("R")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        double x;
        read(*self_server, x);
        const bool result = (x == -3.1415);
        write(*self_server, result);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("write:socket*R");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant socket_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));
      DataVariant value_variant  = double{-3.1415};

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({socket_variant, value_variant});

      REQUIRE(get_result(p_client));

      t.join();
    }

    SECTION("R^1")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyVector<1> x;
        read(*self_server, x);
        const bool result = (x == TinyVector<1>{-3.1415});
        write(*self_server, result);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("write:socket*R^1");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant socket_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));
      DataVariant value_variant  = TinyVector<1>{-3.1415};

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({socket_variant, value_variant});

      REQUIRE(get_result(p_client));

      t.join();
    }

    SECTION("R^2")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyVector<2> x;
        read(*self_server, x);
        const bool result = (x == TinyVector<2>{-3.1415, 1.414});
        write(*self_server, result);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("write:socket*R^2");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant socket_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));
      DataVariant value_variant  = TinyVector<2>{-3.1415, 1.414};

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({socket_variant, value_variant});

      REQUIRE(get_result(p_client));

      t.join();
    }

    SECTION("R^3")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyVector<3> x;
        read(*self_server, x);
        const bool result = (x == TinyVector<3>{-3.1415, 1.414, 1.3});
        write(*self_server, result);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("write:socket*R^3");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant socket_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));
      DataVariant value_variant  = TinyVector<3>{-3.1415, 1.414, 1.3};

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({socket_variant, value_variant});

      REQUIRE(get_result(p_client));

      t.join();
    }

    SECTION("R^1x1")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyMatrix<1> x;
        read(*self_server, x);
        const bool result = (x == TinyMatrix<1>{-3.1415});
        write(*self_server, result);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("write:socket*R^1x1");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant socket_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));
      DataVariant value_variant  = TinyMatrix<1>{-3.1415};

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({socket_variant, value_variant});

      REQUIRE(get_result(p_client));

      t.join();
    }

    SECTION("R^2x2")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyMatrix<2> x;
        read(*self_server, x);
        const bool result = (x == TinyMatrix<2>{3, -3.1415, -1.2, 5.6});
        write(*self_server, result);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("write:socket*R^2x2");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant socket_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));
      DataVariant value_variant  = TinyMatrix<2>{3, -3.1415, -1.2, 5.6};

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({socket_variant, value_variant});

      REQUIRE(get_result(p_client));

      t.join();
    }

    SECTION("R^3x3")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        TinyMatrix<3> x;
        read(*self_server, x);
        const bool result = (x == TinyMatrix<3>{3, -3.1415, -1.2, 5.6, -4, 1.7, -5.2, 3.4, 2});
        write(*self_server, result);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("write:socket*R^3x3");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant socket_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));
      DataVariant value_variant  = TinyMatrix<3>{3, -3.1415, -1.2, 5.6, -4, 1.7, -5.2, 3.4, 2};

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({socket_variant, value_variant});

      REQUIRE(get_result(p_client));

      t.join();
    }

    SECTION("string")
    {
      std::shared_ptr p_server = create_socket_server();

      auto self_client = [&connect_socket_server](int port) {
        std::shared_ptr self_server = connect_socket_server("localhost", port);

        size_t size;
        read(*self_server, size);
        std::string s;
        s.resize(size);
        read(*self_server, s);
        const bool result = (s == "foobar");
        write(*self_server, result);
      };
      std::thread t(self_client, p_server->portNumber());

      std::shared_ptr p_client = accept_socket_client(p_server);

      auto i_function = name_builtin_function.find("write:socket*string");
      REQUIRE(i_function != name_builtin_function.end());

      DataVariant socket_variant = EmbeddedData(std::make_shared<DataHandler<const Socket>>(p_client));
      DataVariant value_variant  = std::string{"foobar"};

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({socket_variant, value_variant});

      REQUIRE(get_result(p_client));

      t.join();
    }
  }
}
