#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <algebra/CRSMatrix.hpp>
#include <algebra/CRSMatrixDescriptor.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("CRSMatrix", "[algebra]")
{
  SECTION("matrix size")
  {
    Array<int> non_zeros{6};
    non_zeros.fill(3);

    CRSMatrixDescriptor<int> S{6, 5, non_zeros};
    S(0, 2) = 3;
    S(1, 2) = 11;
    S(1, 1) = 1;
    S(3, 0) = 5;
    S(3, 1) = -3;
    S(4, 1) = 1;
    S(2, 2) = 4;
    S(4, 3) = 2;
    S(4, 4) = -2;
    S(5, 2) = 1;

    CRSMatrix<int> A{S.getCRSMatrix()};

    REQUIRE(A.numberOfRows() == S.numberOfRows());
    REQUIRE(A.numberOfColumns() == S.numberOfColumns());
  }

  SECTION("square matrix vector product (simple)")
  {
    Array<int> non_zeros{5};
    non_zeros.fill(3);
    CRSMatrixDescriptor<int> S{5, 5, non_zeros};
    S(0, 2) = 3;
    S(1, 2) = 11;
    S(1, 1) = 1;
    S(3, 0) = 5;
    S(3, 1) = -3;
    S(4, 1) = 1;
    S(2, 2) = 4;
    S(4, 3) = 2;
    S(4, 4) = -2;

    CRSMatrix<int> A{S.getCRSMatrix()};

    Vector<int> x(A.numberOfColumns());
    x    = zero;
    x[0] = 1;

    Vector<int> y = A * x;
    REQUIRE(y[0] == 0);
    REQUIRE(y[1] == 0);
    REQUIRE(y[2] == 0);
    REQUIRE(y[3] == 5);
    REQUIRE(y[4] == 0);

    x    = zero;
    x[1] = 2;

    y = A * x;
    REQUIRE(y[0] == 0);
    REQUIRE(y[1] == 2);
    REQUIRE(y[2] == 0);
    REQUIRE(y[3] == -6);
    REQUIRE(y[4] == 2);

    x    = zero;
    x[2] = -1;

    y = A * x;
    REQUIRE(y[0] == -3);
    REQUIRE(y[1] == -11);
    REQUIRE(y[2] == -4);
    REQUIRE(y[3] == 0);
    REQUIRE(y[4] == 0);

    x    = zero;
    x[3] = 3;

    y = A * x;
    REQUIRE(y[0] == 0);
    REQUIRE(y[1] == 0);
    REQUIRE(y[2] == 0);
    REQUIRE(y[3] == 0);
    REQUIRE(y[4] == 6);

    x    = zero;
    x[4] = 1;

    y = A * x;
    REQUIRE(y[0] == 0);
    REQUIRE(y[1] == 0);
    REQUIRE(y[2] == 0);
    REQUIRE(y[3] == 0);
    REQUIRE(y[4] == -2);
  }

  SECTION("rectangular matrix vector product (simple)")
  {
    Array<int> non_zeros{3};
    non_zeros.fill(3);
    CRSMatrixDescriptor<int> S{3, 5, non_zeros};
    S(0, 2) = 3;
    S(0, 4) = 2;
    S(1, 2) = 11;
    S(1, 1) = 2;
    S(1, 0) = 1;
    S(2, 3) = 6;
    S(2, 2) = 4;

    CRSMatrix<int> A{S.getCRSMatrix()};

    Vector<int> x(A.numberOfColumns());
    x[0] = 1;
    x[1] = 3;
    x[2] = 2;
    x[3] = -3;
    x[4] = 5;

    Vector<int> y = A * x;
    REQUIRE(y[0] == 16);
    REQUIRE(y[1] == 29);
    REQUIRE(y[2] == -10);
  }

  SECTION("matrix vector product (complete)")
  {
    Array<int> non_zeros{4};
    non_zeros.fill(4);
    CRSMatrixDescriptor<int> S{4, 4, non_zeros};
    S(0, 0) = 1;
    S(0, 1) = 2;
    S(0, 2) = 3;
    S(0, 3) = 4;
    S(1, 0) = 5;
    S(1, 1) = 6;
    S(1, 2) = 7;
    S(1, 3) = 8;
    S(2, 0) = 9;
    S(2, 1) = 10;
    S(2, 2) = 11;
    S(2, 3) = 12;
    S(3, 0) = 13;
    S(3, 1) = 14;
    S(3, 2) = 15;
    S(3, 3) = 16;

    CRSMatrix<int> A{S.getCRSMatrix()};

    Vector<int> x(A.numberOfColumns());
    x[0] = 1;
    x[1] = 2;
    x[2] = 3;
    x[3] = 4;

    Vector<int> y = A * x;
    REQUIRE(y[0] == 30);
    REQUIRE(y[1] == 70);
    REQUIRE(y[2] == 110);
    REQUIRE(y[3] == 150);
  }

  SECTION("square matrices sum")
  {
    Array<int> non_zeros_DA{4};
    non_zeros_DA.fill(2);
    CRSMatrixDescriptor<int> DA{4, 4, non_zeros_DA};
    DA(0, 0) = 1;
    DA(0, 1) = 2;
    DA(1, 0) = 5;
    DA(1, 1) = 6;
    DA(2, 0) = 9;
    DA(2, 3) = 12;
    DA(3, 0) = 13;
    DA(3, 3) = 16;

    CRSMatrix<int> A{DA.getCRSMatrix()};

    Array<int> non_zeros_DB{4};
    non_zeros_DB.fill(2);
    CRSMatrixDescriptor<int> DB{4, 4, non_zeros_DB};
    DB(0, 0) = 1;
    DB(0, 2) = 2;
    DB(1, 0) = 5;
    DB(1, 2) = 6;
    DB(2, 0) = 9;
    DB(2, 2) = 12;
    DB(3, 0) = 13;
    DB(3, 3) = 16;

    CRSMatrix<int> B{DB.getCRSMatrix()};

    std::ostringstream ost;
    ost << A + B;

    std::string ref = R"(0| 0:2 1:2 2:2
1| 0:10 1:6 2:6
2| 0:18 2:12 3:12
3| 0:26 3:32
)";

    REQUIRE(ost.str() == ref);
  }

  SECTION("square matrices difference")
  {
    Array<int> non_zeros_DA{4};
    non_zeros_DA.fill(2);
    CRSMatrixDescriptor<int> DA{4, 4, non_zeros_DA};
    DA(0, 0) = 1;
    DA(0, 1) = 2;
    DA(1, 0) = 5;
    DA(1, 1) = 6;
    DA(2, 0) = 9;
    DA(2, 3) = 12;
    DA(3, 0) = 13;
    DA(3, 3) = 16;

    CRSMatrix<int> A{DA.getCRSMatrix()};

    Array<int> non_zeros_DB{4};
    non_zeros_DB.fill(2);
    CRSMatrixDescriptor<int> DB{4, 4, non_zeros_DB};
    DB(0, 0) = -1;
    DB(0, 2) = 3;
    DB(1, 0) = 7;
    DB(1, 2) = 3;
    DB(2, 0) = 7;
    DB(2, 2) = 11;
    DB(3, 0) = 3;
    DB(3, 3) = 8;

    CRSMatrix<int> B{DB.getCRSMatrix()};

    std::ostringstream ost;
    ost << A - B;

    std::string ref = R"(0| 0:2 1:2 2:-3
1| 0:-2 1:6 2:-3
2| 0:2 2:-11 3:12
3| 0:10 3:8
)";

    REQUIRE(ost.str() == ref);
  }

  SECTION("rectangle matrices sum")
  {
    Array<int> non_zeros_DA{4};
    non_zeros_DA.fill(2);
    CRSMatrixDescriptor<int> DA{4, 5, non_zeros_DA};
    DA(0, 0) = 1;
    DA(0, 1) = 2;
    DA(0, 4) = 2;
    DA(1, 0) = 5;
    DA(1, 1) = 6;
    DA(2, 0) = 9;
    DA(2, 3) = 12;
    DA(3, 0) = 13;
    DA(3, 3) = 16;
    DA(3, 4) = 16;

    CRSMatrix<int> A{DA.getCRSMatrix()};

    Array<int> non_zeros_DB{4};
    non_zeros_DB.fill(2);
    CRSMatrixDescriptor<int> DB{4, 5, non_zeros_DB};
    DB(0, 0) = 1;
    DB(0, 2) = 2;
    DB(1, 0) = 5;
    DB(1, 2) = 6;
    DB(1, 4) = 3;
    DB(2, 0) = 9;
    DB(2, 2) = 12;
    DB(3, 0) = 13;
    DB(3, 3) = 16;

    CRSMatrix<int> B{DB.getCRSMatrix()};

    std::ostringstream ost;
    ost << A + B;

    std::string ref = R"(0| 0:2 1:2 2:2 4:2
1| 0:10 1:6 2:6 4:3
2| 0:18 2:12 3:12
3| 0:26 3:32 4:16
)";

    REQUIRE(ost.str() == ref);
  }

  SECTION("rectangle matrices difference")
  {
    Array<int> non_zeros_DA{4};
    non_zeros_DA.fill(2);
    CRSMatrixDescriptor<int> DA{4, 3, non_zeros_DA};
    DA(0, 0) = 1;
    DA(0, 1) = 2;
    DA(1, 0) = 5;
    DA(1, 1) = 6;
    DA(2, 0) = 9;
    DA(2, 2) = 12;
    DA(3, 0) = 13;
    DA(3, 2) = 16;

    CRSMatrix<int> A{DA.getCRSMatrix()};

    Array<int> non_zeros_DB{4};
    non_zeros_DB.fill(2);
    CRSMatrixDescriptor<int> DB{4, 3, non_zeros_DB};
    DB(0, 0) = -1;
    DB(0, 2) = 3;
    DB(1, 0) = 7;
    DB(1, 2) = 3;
    DB(2, 0) = 7;
    DB(2, 2) = 11;
    DB(3, 0) = 3;
    DB(3, 1) = 8;

    CRSMatrix<int> B{DB.getCRSMatrix()};

    std::ostringstream ost;
    ost << A - B;

    std::string ref = R"(0| 0:2 1:2 2:-3
1| 0:-2 1:6 2:-3
2| 0:2 2:1
3| 0:10 1:-8 2:16
)";

    REQUIRE(ost.str() == ref);
  }

  SECTION("check values")
  {
    Array<int> non_zeros{4};
    non_zeros.fill(3);
    non_zeros[2] = 4;
    CRSMatrixDescriptor<int> S{4, 4, non_zeros};
    S(3, 0) = 13;
    S(0, 0) = 1;
    S(0, 1) = 2;
    S(1, 1) = 6;
    S(1, 2) = 7;
    S(2, 2) = 11;
    S(3, 2) = 15;
    S(2, 0) = 9;
    S(3, 3) = 16;
    S(2, 3) = 12;
    S(0, 3) = 4;
    S(1, 0) = 5;
    S(2, 1) = 10;

    CRSMatrix<int> A{S.getCRSMatrix()};

    auto values = A.values();
    REQUIRE(values.size() == 13);
    REQUIRE(values[0] == 1);
    REQUIRE(values[1] == 2);
    REQUIRE(values[2] == 4);
    REQUIRE(values[3] == 5);
    REQUIRE(values[4] == 6);
    REQUIRE(values[5] == 7);
    REQUIRE(values[6] == 9);
    REQUIRE(values[7] == 10);
    REQUIRE(values[8] == 11);
    REQUIRE(values[9] == 12);
    REQUIRE(values[10] == 13);
    REQUIRE(values[11] == 15);
    REQUIRE(values[12] == 16);

    auto row_map = A.rowMap();
    REQUIRE(row_map.size() == 5);
    REQUIRE(row_map[0] == 0);
    REQUIRE(row_map[1] == 3);
    REQUIRE(row_map[2] == 6);
    REQUIRE(row_map[3] == 10);
    REQUIRE(row_map[4] == 13);

    auto column_indices = A.columnIndices();
    REQUIRE(column_indices.size() == 13);
    REQUIRE(column_indices[0] == 0);
    REQUIRE(column_indices[1] == 1);
    REQUIRE(column_indices[2] == 3);
    REQUIRE(column_indices[3] == 0);
    REQUIRE(column_indices[4] == 1);
    REQUIRE(column_indices[5] == 2);
    REQUIRE(column_indices[6] == 0);
    REQUIRE(column_indices[7] == 1);
    REQUIRE(column_indices[8] == 2);
    REQUIRE(column_indices[9] == 3);
    REQUIRE(column_indices[10] == 0);
    REQUIRE(column_indices[11] == 2);
    REQUIRE(column_indices[12] == 3);

    std::ostringstream crs_descriptor_os;
    std::ostringstream crs_os;

    crs_descriptor_os << S;
    crs_os << A;

    REQUIRE(crs_descriptor_os.str() == crs_os.str());
  }

#ifndef NDEBUG
  SECTION("runtime incompatible matrix/vector product")
  {
    Array<int> non_zeros(2);
    non_zeros.fill(0);
    CRSMatrixDescriptor<int> S{2, 4, non_zeros};
    CRSMatrix<int> A{S.getCRSMatrix()};
    Vector<int> x{2};
    REQUIRE_THROWS_WITH(A * x, "cannot compute product: incompatible matrix and vector sizes");
  }

  SECTION("runtime incompatible matrices sum")
  {
    Array<int> A_non_zeros(2);
    A_non_zeros.fill(0);
    CRSMatrixDescriptor<int> DA{2, 4, A_non_zeros};
    CRSMatrix<int> A{DA.getCRSMatrix()};

    Array<int> B_non_zeros(3);
    B_non_zeros.fill(0);
    CRSMatrixDescriptor<int> DB{3, 4, B_non_zeros};
    CRSMatrix<int> B{DB.getCRSMatrix()};

    REQUIRE_THROWS_WITH(A + B, "cannot apply inner binary operator on matrices of different sizes");
  }

  SECTION("runtime incompatible matrices difference")
  {
    Array<int> A_non_zeros(2);
    A_non_zeros.fill(0);
    CRSMatrixDescriptor<int> DA{2, 4, A_non_zeros};
    CRSMatrix<int> A{DA.getCRSMatrix()};

    Array<int> B_non_zeros(2);
    B_non_zeros.fill(0);
    CRSMatrixDescriptor<int> DB{2, 3, B_non_zeros};
    CRSMatrix<int> B{DB.getCRSMatrix()};

    REQUIRE_THROWS_WITH(A - B, "cannot apply inner binary operator on matrices of different sizes");
  }

#endif   // NDEBUG
}
