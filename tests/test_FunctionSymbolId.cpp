#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/FunctionSymbolId.hpp>
#include <language/utils/SymbolTable.hpp>
#include <utils/PugsAssert.hpp>

#include <sstream>

// clazy:excludeall=non-pod-global-static

TEST_CASE("FunctionSymbolId", "[language]")
{
  std::shared_ptr<SymbolTable> s = std::make_shared<SymbolTable>();
  const FunctionSymbolId f{2, s};
  REQUIRE(f.id() == 2);
  REQUIRE(&f.symbolTable() == &(*s));

  {
    FunctionSymbolId g{f};
    REQUIRE(g.id() == 2);
  }

  {
    FunctionSymbolId h{4, s};
    FunctionSymbolId g{std::move(h)};

    REQUIRE(g.id() == 4);
  }

  {
    FunctionSymbolId g;
    g = f;
    REQUIRE(g.id() == 2);
  }

  {
    FunctionSymbolId g;
    FunctionSymbolId h{4, s};
    g = std::move(h);
    REQUIRE(g.id() == 4);
  }

  {
    std::ostringstream sout;
    sout << f;

    REQUIRE(sout.str() == "2");
  }

#ifndef NDEBUG
  {
    std::shared_ptr<SymbolTable> unset_s;
    REQUIRE_THROWS_AS((FunctionSymbolId{0, unset_s}.symbolTable()), AssertError);
  }
#endif
}
