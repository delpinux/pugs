#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <test_BinaryExpressionProcessor_utils.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("BinaryExpressionProcessor logic", "[language]")
{
  SECTION("and")
  {
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true and true;)", "b", true);
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false and true;)", "b", false);
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true and false;)", "b", false);
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false and false;)", "b", false);

    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = (2<3) and ((3.2-1) != 2);)", "b", true);
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = (1>4) and true;)", "b", false);
  }

  SECTION("or")
  {
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true or true;)", "b", true);
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false or true;)", "b", true);
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true or false;)", "b", true);
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false or false;)", "b", false);

    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = (2<3) and ((3.2-1) != 2);)", "b", true);
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = (1>4) or true;)", "b", true);
  }

  SECTION("xor")
  {
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true xor true;)", "b", false);
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false xor true;)", "b", true);
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = true xor false;)", "b", true);
    CHECK_BINARY_EXPRESSION_RESULT(R"(let b:B, b = false xor false;)", "b", false);
  }

  SECTION("errors")
  {
    SECTION("bad implicit conversion")
    {
      SECTION("and")
      {
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types N and B)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(let n:N, n=1; n and true;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types B and N)";
          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(let n:N, n=2; false and n;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types Z and B)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(1 and true;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types B and Z)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(false and 2;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types R and B)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(1.1 and true;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types B and R)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(false and 2e-2;)", error_message);
        }
      }

      SECTION("or")
      {
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types N and B)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(let n:N, n=1; n or true;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types B and N)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(let n:N, n=2; false or n;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types Z and B)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(1 or true;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types B and Z)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(false or 2;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types R and B)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(1.1 or true;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types B and R)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(false or 2e-2;)", error_message);
        }
      }

      SECTION("xor")
      {
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types N and B)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(let n:N, n=1; n xor true;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types B and N)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(let n:N, n=2; false xor n;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types Z and B)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(1 xor true;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types B and Z)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(false xor 2;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types R and B)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(1.1 xor true;)", error_message);
        }
        {
          const std::string error_message = R"(undefined binary operator
note: incompatible operand types B and R)";

          CHECK_BINARY_EXPRESSION_THROWS_WITH(R"(false xor 2e-2;)", error_message);
        }
      }
    }
  }
}
