#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTPrinter.hpp>

#include <pegtl/string_input.hpp>

#define CHECK_AST(data, expected_output)                                                       \
  {                                                                                            \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);             \
    static_assert(std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view>);  \
                                                                                               \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                 \
    auto ast = ASTBuilder::build(input);                                                       \
                                                                                               \
    ASTModulesImporter{*ast};                                                                  \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                    \
                                                                                               \
    ASTSymbolTableBuilder{*ast};                                                               \
    ASTNodeDataTypeBuilder{*ast};                                                              \
                                                                                               \
    ASTNodeDeclarationToAffectationConverter{*ast};                                            \
                                                                                               \
    std::stringstream ast_output;                                                              \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::none}}; \
                                                                                               \
    REQUIRE(ast_output.str() == expected_output);                                              \
    ast->m_symbol_table->clearValues();                                                        \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeDeclarationToAffectationConverter", "[language]")
{
  SECTION("nothing to convert")
  {
    std::string_view data = R"(
let z:R;
)";

    std::string_view result = R"(
(root)
 `-(language::var_declaration)
     +-(language::name:z)
     `-(language::R_set)
)";

    CHECK_AST(data, result);
  }

  SECTION("simple constructor")
  {
    std::string_view data = R"(
let z :R, z = 0;
)";

    std::string_view result = R"(
(root)
 `-(language::eq_op)
     +-(language::name:z)
     `-(language::integer:0)
)";

    CHECK_AST(data, result);
  }

  SECTION("complex constructors")
  {
    std::string_view data = R"(
let k:N, k = 0;
for(let i:N, i=0; i<2; ++i) {
  let j:N, j = 2*i+k;
  k = 2*j-1;
}
)";

    std::string_view result = R"(
(root)
 +-(language::eq_op)
 |   +-(language::name:k)
 |   `-(language::integer:0)
 `-(language::for_statement)
     +-(language::eq_op)
     |   +-(language::name:i)
     |   `-(language::integer:0)
     +-(language::lesser_op)
     |   +-(language::name:i)
     |   `-(language::integer:2)
     +-(language::unary_plusplus)
     |   `-(language::name:i)
     `-(language::for_statement_block)
         +-(language::eq_op)
         |   +-(language::name:j)
         |   `-(language::plus_op)
         |       +-(language::multiply_op)
         |       |   +-(language::integer:2)
         |       |   `-(language::name:i)
         |       `-(language::name:k)
         `-(language::eq_op)
             +-(language::name:k)
             `-(language::minus_op)
                 +-(language::multiply_op)
                 |   +-(language::integer:2)
                 |   `-(language::name:j)
                 `-(language::integer:1)
)";

    CHECK_AST(data, result);
  }
}
