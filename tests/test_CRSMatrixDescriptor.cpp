#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/PugsAssert.hpp>

#include <algebra/CRSMatrixDescriptor.hpp>

// Instantiate to ensure full coverage is performed
template class CRSMatrixDescriptor<int>;

// clazy:excludeall=non-pod-global-static

TEST_CASE("CRSMatrixDescriptor", "[algebra]")
{
  SECTION("sizes")
  {
    SECTION("rectangle")
    {
      const size_t nb_lines   = 2;
      const size_t nb_columns = 5;

      Array<int> non_zeros{nb_lines};
      non_zeros.fill(2);
      CRSMatrixDescriptor<int> S(nb_lines, nb_columns, non_zeros);

      REQUIRE(S.numberOfRows() == 2);
      REQUIRE(S.numberOfColumns() == 5);
    }

    SECTION("square")
    {
      const size_t nb_lines = 3;

      Array<int> non_zeros{nb_lines};
      non_zeros.fill(2);
      CRSMatrixDescriptor<int> S(nb_lines, non_zeros);

      REQUIRE(S.numberOfRows() == 3);
      REQUIRE(S.numberOfColumns() == 3);
    }
  }

  SECTION("has overflow / not filled")
  {
    const size_t nb_lines   = 2;
    const size_t nb_columns = 5;

    Array<int> non_zeros{nb_lines};
    non_zeros.fill(2);
    CRSMatrixDescriptor<int> S(nb_lines, nb_columns, non_zeros);

    S(1, 3) = 5;
    S(1, 1) += 3;
    S(0, 1) = 4;
    S(1, 0) = 2;
    S(1, 2) = 7;

    REQUIRE(S.numberOfRows() == 2);
    REQUIRE(S.numberOfColumns() == 5);

    REQUIRE(S.isFilled() == false);
    REQUIRE(S.hasOverflow() == true);

    std::ostringstream os;
    os << S;

    std::string_view output =
      R"(0| 1:4
1| 0:2 1:3 2:7 3:5
)";
    REQUIRE(os.str() == output);
  }

  SECTION("no overflow / not filled")
  {
    const size_t nb_lines   = 2;
    const size_t nb_columns = 5;

    Array<int> non_zeros{nb_lines};
    non_zeros.fill(2);
    CRSMatrixDescriptor<int> S(nb_lines, nb_columns, non_zeros);

    S(1, 3) = 5;
    S(1, 1) += 3;
    S(0, 1) = 4;

    REQUIRE(S.numberOfRows() == 2);
    REQUIRE(S.numberOfColumns() == 5);

    REQUIRE(S.isFilled() == false);
    REQUIRE(S.hasOverflow() == false);

    std::ostringstream os;
    os << S;

    std::string_view output =
      R"(0| 1:4
1| 1:3 3:5
)";
    REQUIRE(os.str() == output);
  }

  SECTION("no overflow / filled")
  {
    const size_t nb_lines   = 2;
    const size_t nb_columns = 5;

    Array<int> non_zeros{nb_lines};
    non_zeros[0] = 1;
    non_zeros[1] = 5;
    CRSMatrixDescriptor<int> S(nb_lines, nb_columns, non_zeros);

    S(1, 3) = 2;
    S(1, 3) += 3;
    S(1, 1) += 3;
    S(0, 1) = 4;
    S(1, 2) = 2;
    S(1, 0) = 1;
    S(1, 4) = 13;

    REQUIRE(S.numberOfRows() == 2);
    REQUIRE(S.numberOfColumns() == 5);

    REQUIRE(S.isFilled() == true);
    REQUIRE(S.hasOverflow() == false);

    std::ostringstream os;
    os << S;

    std::string_view output =
      R"(0| 1:4
1| 0:1 1:3 2:2 3:5 4:13
)";
    REQUIRE(os.str() == output);
  }

  SECTION("has overflow / filled")
  {
    const size_t nb_lines   = 2;
    const size_t nb_columns = 5;

    Array<int> non_zeros{nb_lines};
    non_zeros.fill(2);
    CRSMatrixDescriptor<int> S(nb_lines, nb_columns, non_zeros);

    S(1, 3) = 3;
    S(1, 0) = -2;
    S(1, 1) += 1;
    S(1, 1) += 1;
    S(1, 1) = 3;
    S(0, 1) = 4;
    S(0, 0) = 1;
    S(0, 3) = 5;
    S(0, 2) = 1;

    REQUIRE(S.numberOfRows() == 2);
    REQUIRE(S.numberOfColumns() == 5);

    REQUIRE(S.isFilled() == true);
    REQUIRE(S.hasOverflow() == true);

    std::ostringstream os;
    os << S;

    std::string_view output =
      R"(0| 0:1 1:4 2:1 3:5
1| 0:-2 1:3 3:3
)";
    REQUIRE(os.str() == output);
  }

#ifndef NDEBUG
  SECTION("incompatible non zero vector size and number of columns")
  {
    Array<int> non_zeros(3);
    non_zeros.fill(1);
    REQUIRE_THROWS_AS((CRSMatrixDescriptor<int>{2, 4, non_zeros}), AssertError);
  }

  SECTION("bad row number")
  {
    Array<int> non_zeros(2);
    non_zeros.fill(1);
    CRSMatrixDescriptor<int> S{2, 4, non_zeros};
    REQUIRE_THROWS_AS(S(2, 3) = 1, AssertError);
  }

  SECTION("bad column number")
  {
    Array<int> non_zeros(2);
    non_zeros.fill(2);
    CRSMatrixDescriptor<int> S{2, 4, non_zeros};
    REQUIRE_THROWS_AS(S(0, 5) = 1, AssertError);
  }

  SECTION("negative number of non-zeros")
  {
    Array<int> non_zeros(2);
    non_zeros[0] = 1;
    non_zeros[1] = -2;
    REQUIRE_THROWS_AS((CRSMatrixDescriptor<int>{2, 4, non_zeros}), AssertError);
  }

#endif   // NDEBUG
}
