// -*- coding: utf-8 -*-
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <language/utils/SymbolTable.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_OUTPUT(data, expected_output, format)                                           \
  {                                                                                           \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);            \
    static_assert(std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view>); \
                                                                                              \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                \
    auto ast = ASTBuilder::build(input);                                                      \
                                                                                              \
    std::stringstream ast_output;                                                             \
    ast_output << '\n' << ASTPrinter{*ast, format};                                           \
                                                                                              \
    REQUIRE(ast_output.str() == expected_output);                                             \
    ast->m_symbol_table->clearValues();                                                       \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTPrinter", "[language]")
{
  rang::setControlMode(rang::control::Off);

  SECTION("raw output")
  {
    std::string_view data = R"(
let n:N, n = 2 + 3;
)";

    std::string_view result = R"(
(root:undefined)
 `-(language::var_declaration:undefined)
     +-(language::name:n:undefined)
     +-(language::N_set:undefined)
     +-(language::name:n:undefined)
     `-(language::plus_op:undefined)
         +-(language::integer:2:undefined)
         `-(language::integer:3:undefined)
)";
    CHECK_OUTPUT(data, result, ASTPrinter::Format::raw);
  }

  SECTION("pretty output")
  {
    std::string_view data = R"(
let n:N, n = 2 + 3;
)";

    std::string_view result = R"(
(root:undefined)
 └──(language::var_declaration:undefined)
     ├──(language::name:n:undefined)
     ├──(language::N_set:undefined)
     ├──(language::name:n:undefined)
     └──(language::plus_op:undefined)
         ├──(language::integer:2:undefined)
         └──(language::integer:3:undefined)
)";
    CHECK_OUTPUT(data, result, ASTPrinter::Format::pretty);
  }

  SECTION("escaped sequences")
  {
    std::string_view data = R"(
let s:string, s = "a string";
)";

    std::string_view result = R"(
(root:undefined)
 `-(language::var_declaration:undefined)
     +-(language::name:s:undefined)
     +-(language::string_type:undefined)
     +-(language::name:s:undefined)
     `-(language::literal:"a string":undefined)
)";
    CHECK_OUTPUT(data, result, ASTPrinter::Format::raw);
  }
}
