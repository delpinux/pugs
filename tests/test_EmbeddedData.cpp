#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>

#include <sstream>

// clazy:excludeall=non-pod-global-static

TEST_CASE("EmbeddedData", "[language]")
{
  REQUIRE_NOTHROW(EmbeddedData{});
  EmbeddedData e;
  {
    EmbeddedData f{std::make_shared<DataHandler<double>>(std::make_shared<double>(1.3))};
    REQUIRE_NOTHROW(e = std::move(f));
  }

  REQUIRE(*dynamic_cast<const DataHandler<double>&>(e.get()).data_ptr() == 1.3);

  {
    EmbeddedData f{std::make_shared<DataHandler<double>>(std::make_shared<double>(2.3))};
    REQUIRE_NOTHROW(e = f);
  }

  REQUIRE(*dynamic_cast<const DataHandler<double>&>(e.get()).data_ptr() == 2.3);

  std::ostringstream fout;
  fout << e;
  REQUIRE(fout.str() == "embedded_data");
}
