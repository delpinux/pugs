#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeFunctionEvaluationExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#define CHECK_AST(data, expected_output)                                                            \
  {                                                                                                 \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);                  \
    static_assert((std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view>) or    \
                  (std::is_same_v<std::decay_t<decltype(expected_output)>, std::string>));          \
                                                                                                    \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                      \
    auto ast = ASTBuilder::build(input);                                                            \
                                                                                                    \
    ASTModulesImporter{*ast};                                                                       \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                         \
                                                                                                    \
    ASTSymbolTableBuilder{*ast};                                                                    \
    ASTNodeDataTypeBuilder{*ast};                                                                   \
                                                                                                    \
    ASTNodeTypeCleaner<language::fct_declaration>{*ast};                                            \
                                                                                                    \
    ASTNodeExpressionBuilder{*ast};                                                                 \
    std::stringstream ast_output;                                                                   \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::exec_type}}; \
                                                                                                    \
    REQUIRE(ast_output.str() == expected_output);                                                   \
    ast->m_symbol_table->clearValues();                                                             \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeFunctionEvaluationExpressionBuilder", "[language]")
{
  SECTION("C function evaluation")
  {
    std::string_view data = R"(
import math;
sin(3);
)";

    std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:BuiltinFunctionProcessor)
     +-(language::name:sin:FakeProcessor)
     `-(language::integer:3:ValueProcessor)
)";

    CHECK_AST(data, result);
  }

  SECTION("function evaluation")
  {
    std::string_view data = R"(
let sum : R*R -> R, (x,y) -> x+y;
sum(1,2);
)";

    std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:sum:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         `-(language::integer:2:ValueProcessor)
)";

    CHECK_AST(data, result);
  }
}
