#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/ParseError.hpp>

#include <string>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ParseError", "[language]")
{
  SECTION("single position")
  {
    const std::string source = R"(
a first line
a second line
)";
    TAO_PEGTL_NAMESPACE::internal::inputerator i(&source[0], 3, 1, 2);
    TAO_PEGTL_NAMESPACE::position p{i, source};
    ParseError parse_error("error message", p);
    REQUIRE(parse_error.positions() == std::vector{p});
    REQUIRE(parse_error.what() == std::string{"error message"});
  }

  SECTION("position list")
  {
    const std::string source = R"(
a first line
a second line
)";
    TAO_PEGTL_NAMESPACE::internal::inputerator i0(&source[0], 3, 1, 2);
    TAO_PEGTL_NAMESPACE::position p0{i0, source};
    TAO_PEGTL_NAMESPACE::internal::inputerator i1(&source[0], 4, 1, 3);
    TAO_PEGTL_NAMESPACE::position p1{i1, source};

    ParseError parse_error("error message", std::vector{p0, p1});
    REQUIRE(parse_error.positions() == std::vector{p0, p1});
    REQUIRE(parse_error.what() == std::string{"error message"});
  }
}
