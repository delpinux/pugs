#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <mesh/RefItemList.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("RefItemList", "[mesh]")
{
  SECTION("nodes")
  {
    const Array<NodeId> node_id_array = convert_to_array(std::vector<NodeId>{1, 3, 7, 2, 4, 11});
    const RefId ref_id{3, "my_reference"};

    RefItemList<ItemType::node> ref_node_list{ref_id, node_id_array, RefItemListBase::Type::boundary};
    REQUIRE(ref_node_list.refId() == ref_id);
    REQUIRE(ref_node_list.list().size() == node_id_array.size());
    REQUIRE(&(ref_node_list.list()[0]) == &(node_id_array[0]));
    REQUIRE(ref_node_list.type() == RefItemListBase::Type::boundary);

    {
      RefItemList copy_ref_node_list{ref_node_list};
      REQUIRE(copy_ref_node_list.refId() == ref_id);
      REQUIRE(copy_ref_node_list.list().size() == node_id_array.size());
      REQUIRE(&(copy_ref_node_list.list()[0]) == &(node_id_array[0]));
      REQUIRE(copy_ref_node_list.type() == RefItemListBase::Type::boundary);
    }

    {
      RefItemList<ItemType::node> affect_ref_node_list;
      affect_ref_node_list = ref_node_list;
      REQUIRE(affect_ref_node_list.refId() == ref_id);
      REQUIRE(affect_ref_node_list.list().size() == node_id_array.size());
      REQUIRE(&(affect_ref_node_list.list()[0]) == &(node_id_array[0]));
      REQUIRE(affect_ref_node_list.type() == RefItemListBase::Type::boundary);

      RefItemList<ItemType::node> move_ref_node_list;
      move_ref_node_list = std::move(affect_ref_node_list);
      REQUIRE(move_ref_node_list.refId() == ref_id);
      REQUIRE(move_ref_node_list.list().size() == node_id_array.size());
      REQUIRE(&(move_ref_node_list.list()[0]) == &(node_id_array[0]));
      REQUIRE(move_ref_node_list.type() == RefItemListBase::Type::boundary);
    }
  }

  SECTION("edges")
  {
    const Array<EdgeId> edge_id_array = convert_to_array(std::vector<EdgeId>{1, 3, 7, 2, 4, 11});
    const RefId ref_id{3, "my_reference"};

    RefItemList<ItemType::edge> ref_edge_list{ref_id, edge_id_array, RefItemListBase::Type::interface};
    REQUIRE(ref_edge_list.refId() == ref_id);
    REQUIRE(ref_edge_list.list().size() == edge_id_array.size());
    REQUIRE(&(ref_edge_list.list()[0]) == &(edge_id_array[0]));
    REQUIRE(ref_edge_list.type() == RefItemListBase::Type::interface);

    {
      RefItemList copy_ref_edge_list{ref_edge_list};
      REQUIRE(copy_ref_edge_list.refId() == ref_id);
      REQUIRE(copy_ref_edge_list.list().size() == edge_id_array.size());
      REQUIRE(&(copy_ref_edge_list.list()[0]) == &(edge_id_array[0]));
      REQUIRE(copy_ref_edge_list.type() == RefItemListBase::Type::interface);
    }

    {
      RefItemList<ItemType::edge> affect_ref_edge_list;
      affect_ref_edge_list = ref_edge_list;
      REQUIRE(affect_ref_edge_list.refId() == ref_id);
      REQUIRE(affect_ref_edge_list.list().size() == edge_id_array.size());
      REQUIRE(&(affect_ref_edge_list.list()[0]) == &(edge_id_array[0]));
      REQUIRE(affect_ref_edge_list.type() == RefItemListBase::Type::interface);

      RefItemList<ItemType::edge> move_ref_edge_list;
      move_ref_edge_list = std::move(affect_ref_edge_list);
      REQUIRE(move_ref_edge_list.refId() == ref_id);
      REQUIRE(move_ref_edge_list.list().size() == edge_id_array.size());
      REQUIRE(&(move_ref_edge_list.list()[0]) == &(edge_id_array[0]));
      REQUIRE(move_ref_edge_list.type() == RefItemListBase::Type::interface);
    }
  }

  SECTION("faces")
  {
    const Array<FaceId> face_id_array = convert_to_array(std::vector<FaceId>{1, 3, 7, 2, 4, 11});
    const RefId ref_id{3, "my_reference"};

    RefItemList<ItemType::face> ref_face_list{ref_id, face_id_array, RefItemListBase::Type::interface};
    REQUIRE(ref_face_list.refId() == ref_id);
    REQUIRE(ref_face_list.list().size() == face_id_array.size());
    REQUIRE(&(ref_face_list.list()[0]) == &(face_id_array[0]));
    REQUIRE(ref_face_list.type() == RefItemListBase::Type::interface);

    {
      RefItemList copy_ref_face_list{ref_face_list};
      REQUIRE(copy_ref_face_list.refId() == ref_id);
      REQUIRE(copy_ref_face_list.list().size() == face_id_array.size());
      REQUIRE(&(copy_ref_face_list.list()[0]) == &(face_id_array[0]));
      REQUIRE(copy_ref_face_list.type() == RefItemListBase::Type::interface);
    }

    {
      RefItemList<ItemType::face> affect_ref_face_list;
      affect_ref_face_list = ref_face_list;
      REQUIRE(affect_ref_face_list.refId() == ref_id);
      REQUIRE(affect_ref_face_list.list().size() == face_id_array.size());
      REQUIRE(&(affect_ref_face_list.list()[0]) == &(face_id_array[0]));
      REQUIRE(affect_ref_face_list.type() == RefItemListBase::Type::interface);

      RefItemList<ItemType::face> move_ref_face_list;
      move_ref_face_list = std::move(affect_ref_face_list);
      REQUIRE(move_ref_face_list.refId() == ref_id);
      REQUIRE(move_ref_face_list.list().size() == face_id_array.size());
      REQUIRE(&(move_ref_face_list.list()[0]) == &(face_id_array[0]));
      REQUIRE(move_ref_face_list.type() == RefItemListBase::Type::interface);
    }
  }

  SECTION("cells")
  {
    const Array<CellId> cell_id_array = convert_to_array(std::vector<CellId>{1, 3, 7, 2, 4, 11});
    const RefId ref_id{3, "my_reference"};

    RefItemList<ItemType::cell> ref_cell_list{ref_id, cell_id_array, RefItemListBase::Type::set};
    REQUIRE(ref_cell_list.refId() == ref_id);
    REQUIRE(ref_cell_list.list().size() == cell_id_array.size());
    REQUIRE(&(ref_cell_list.list()[0]) == &(cell_id_array[0]));
    REQUIRE(ref_cell_list.type() == RefItemListBase::Type::set);

    {
      RefItemList copy_ref_cell_list{ref_cell_list};
      REQUIRE(copy_ref_cell_list.refId() == ref_id);
      REQUIRE(copy_ref_cell_list.list().size() == cell_id_array.size());
      REQUIRE(&(copy_ref_cell_list.list()[0]) == &(cell_id_array[0]));
      REQUIRE(copy_ref_cell_list.type() == RefItemListBase::Type::set);
    }

    {
      RefItemList<ItemType::cell> affect_ref_cell_list;
      affect_ref_cell_list = ref_cell_list;
      REQUIRE(affect_ref_cell_list.refId() == ref_id);
      REQUIRE(affect_ref_cell_list.list().size() == cell_id_array.size());
      REQUIRE(&(affect_ref_cell_list.list()[0]) == &(cell_id_array[0]));
      REQUIRE(affect_ref_cell_list.type() == RefItemListBase::Type::set);

      RefItemList<ItemType::cell> move_ref_cell_list;
      move_ref_cell_list = std::move(affect_ref_cell_list);
      REQUIRE(move_ref_cell_list.refId() == ref_id);
      REQUIRE(move_ref_cell_list.list().size() == cell_id_array.size());
      REQUIRE(&(move_ref_cell_list.list()[0]) == &(cell_id_array[0]));
      REQUIRE(move_ref_cell_list.type() == RefItemListBase::Type::set);
    }
  }
}
