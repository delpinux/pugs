#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/DualConnectivityManager.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("DualConnectivityManager", "[mesh]")
{
  using MeshType         = Mesh<2>;
  using ConnectivityType = typename MeshType::Connectivity;

  std::shared_ptr mesh_v = MeshDataBaseForTests::get().hybrid2DMesh();
  std::shared_ptr mesh   = mesh_v->get<MeshType>();

  const ConnectivityType& connectivity = mesh->connectivity();

  SECTION("diamond dual connectivity access")
  {
    std::shared_ptr p_diamond_dual_connectivity =
      DualConnectivityManager::instance().getDiamondDualConnectivity(connectivity);

    std::shared_ptr p_diamond_dual_connectivity_mapper =
      DualConnectivityManager::instance().getPrimalToDiamondDualConnectivityDataMapper(connectivity);

    const auto ref_counter        = p_diamond_dual_connectivity.use_count();
    const auto ref_counter_mapper = p_diamond_dual_connectivity_mapper.use_count();

    {
      std::shared_ptr p_diamond_dual_connectivity2 =
        DualConnectivityManager::instance().getDiamondDualConnectivity(connectivity);
      std::shared_ptr p_diamond_dual_connectivity_mapper2 =
        DualConnectivityManager::instance().getPrimalToDiamondDualConnectivityDataMapper(connectivity);

      REQUIRE(p_diamond_dual_connectivity == p_diamond_dual_connectivity2);
      REQUIRE(p_diamond_dual_connectivity.use_count() == ref_counter + 1);
      REQUIRE(p_diamond_dual_connectivity_mapper == p_diamond_dual_connectivity_mapper2);
      REQUIRE(p_diamond_dual_connectivity_mapper.use_count() == ref_counter_mapper + 1);
    }

    REQUIRE(p_diamond_dual_connectivity.use_count() == ref_counter);

    DualConnectivityManager::instance().deleteConnectivity(&connectivity);
    REQUIRE(p_diamond_dual_connectivity.use_count() == ref_counter - 1);

    // Can delete connectivity from the list again. This means that no
    // dual connectivity associated with it is managed.
    REQUIRE_NOTHROW(DualConnectivityManager::instance().deleteConnectivity(&connectivity));
    REQUIRE(p_diamond_dual_connectivity.use_count() == ref_counter - 1);

    // A new dual connectivity is built
    std::shared_ptr p_diamond_dual_connectivity_rebuilt =
      DualConnectivityManager::instance().getDiamondDualConnectivity(connectivity);
    REQUIRE(p_diamond_dual_connectivity != p_diamond_dual_connectivity_rebuilt);
    REQUIRE(p_diamond_dual_connectivity.get() != p_diamond_dual_connectivity_rebuilt.get());

    // Exactly two references to the dual connectivity. One here and
    // one in the manager.
    REQUIRE(p_diamond_dual_connectivity_rebuilt.use_count() == 2);
  }

  SECTION("median dual connectivity access")
  {
    std::shared_ptr p_median_dual_connectivity =
      DualConnectivityManager::instance().getMedianDualConnectivity(connectivity);

    std::shared_ptr p_median_dual_connectivity_mapper =
      DualConnectivityManager::instance().getPrimalToMedianDualConnectivityDataMapper(connectivity);

    const auto ref_counter        = p_median_dual_connectivity.use_count();
    const auto ref_counter_mapper = p_median_dual_connectivity_mapper.use_count();

    {
      std::shared_ptr p_median_dual_connectivity2 =
        DualConnectivityManager::instance().getMedianDualConnectivity(connectivity);

      std::shared_ptr p_median_dual_connectivity_mapper2 =
        DualConnectivityManager::instance().getPrimalToMedianDualConnectivityDataMapper(connectivity);

      REQUIRE(p_median_dual_connectivity == p_median_dual_connectivity2);
      REQUIRE(p_median_dual_connectivity.use_count() == ref_counter + 1);
      REQUIRE(p_median_dual_connectivity_mapper == p_median_dual_connectivity_mapper2);
      REQUIRE(p_median_dual_connectivity_mapper.use_count() == ref_counter_mapper + 1);
    }

    REQUIRE(p_median_dual_connectivity.use_count() == ref_counter);

    DualConnectivityManager::instance().deleteConnectivity(&connectivity);
    REQUIRE(p_median_dual_connectivity.use_count() == ref_counter - 1);

    // Can delete connectivity from the list again. This means that no
    // dual connectivity associated with it is managed.
    REQUIRE_NOTHROW(DualConnectivityManager::instance().deleteConnectivity(&connectivity));
    REQUIRE(p_median_dual_connectivity.use_count() == ref_counter - 1);

    // A new dual connectivity is built
    std::shared_ptr p_median_dual_connectivity_rebuilt =
      DualConnectivityManager::instance().getMedianDualConnectivity(connectivity);
    REQUIRE(p_median_dual_connectivity != p_median_dual_connectivity_rebuilt);
    REQUIRE(p_median_dual_connectivity.get() != p_median_dual_connectivity_rebuilt.get());

    // Exactly two references to the dual connectivity. One here and
    // one in the manager.
    REQUIRE(p_median_dual_connectivity_rebuilt.use_count() == 2);
  }

  SECTION("check multiple dual connectivities using/freeing")
  {
    std::shared_ptr p_median_dual_connectivity =
      DualConnectivityManager::instance().getMedianDualConnectivity(connectivity);
    std::shared_ptr p_diamond_dual_connectivity =
      DualConnectivityManager::instance().getDiamondDualConnectivity(connectivity);

    std::shared_ptr p_median_dual_connectivity_mapper =
      DualConnectivityManager::instance().getPrimalToMedianDualConnectivityDataMapper(connectivity);
    std::shared_ptr p_diamond_dual_connectivity_mapper =
      DualConnectivityManager::instance().getPrimalToDiamondDualConnectivityDataMapper(connectivity);

    const auto median_ref_counter  = p_median_dual_connectivity.use_count();
    const auto diamond_ref_counter = p_diamond_dual_connectivity.use_count();

    const auto median_mapper_ref_counter  = p_median_dual_connectivity_mapper.use_count();
    const auto diamond_mapper_ref_counter = p_diamond_dual_connectivity_mapper.use_count();

    REQUIRE(p_median_dual_connectivity != p_diamond_dual_connectivity);
    REQUIRE(size_t(p_median_dual_connectivity_mapper.get()) != size_t(p_diamond_dual_connectivity_mapper.get()));

    REQUIRE_NOTHROW(DualConnectivityManager::instance().deleteConnectivity(&connectivity));
    REQUIRE(p_median_dual_connectivity.use_count() == median_ref_counter - 1);
    REQUIRE(p_diamond_dual_connectivity.use_count() == diamond_ref_counter - 1);
    REQUIRE(p_median_dual_connectivity_mapper.use_count() == median_mapper_ref_counter - 1);
    REQUIRE(p_diamond_dual_connectivity_mapper.use_count() == diamond_mapper_ref_counter - 1);
  }
}
