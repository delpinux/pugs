#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_AFFECTATION_RESULT(data, variable_name, expected_value)         \
  {                                                                           \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                \
    auto ast = ASTBuilder::build(input);                                      \
                                                                              \
    ASTExecutionStack::create();                                              \
                                                                              \
    ASTModulesImporter{*ast};                                                 \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                   \
                                                                              \
    ASTSymbolTableBuilder{*ast};                                              \
    ASTNodeDataTypeBuilder{*ast};                                             \
                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                      \
                                                                              \
    ASTNodeExpressionBuilder{*ast};                                           \
    ExecutionPolicy exec_policy;                                              \
    ast->execute(exec_policy);                                                \
                                                                              \
    auto symbol_table = ast->m_symbol_table;                                  \
                                                                              \
    using namespace TAO_PEGTL_NAMESPACE;                                      \
    position use_position{10000, 1000, 1, "fixture"};                         \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);   \
                                                                              \
    auto attributes = symbol->attributes();                                   \
    auto value      = std::get<decltype(expected_value)>(attributes.value()); \
                                                                              \
    ASTExecutionStack::destroy();                                             \
                                                                              \
    REQUIRE(value == expected_value);                                         \
    ast->m_symbol_table->clearValues();                                       \
  }

#define CHECK_AFFECTATION_THROW_WITH(data, error_message)          \
  {                                                                \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};     \
    auto ast = ASTBuilder::build(input);                           \
                                                                   \
    ASTExecutionStack::create();                                   \
                                                                   \
    ASTModulesImporter{*ast};                                      \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};        \
                                                                   \
    ASTSymbolTableBuilder{*ast};                                   \
    ASTNodeDataTypeBuilder{*ast};                                  \
                                                                   \
    ASTNodeDeclarationToAffectationConverter{*ast};                \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};           \
                                                                   \
    ASTNodeExpressionBuilder{*ast};                                \
    ExecutionPolicy exec_policy;                                   \
    REQUIRE_THROWS_WITH(ast->execute(exec_policy), error_message); \
                                                                   \
    ASTExecutionStack::destroy();                                  \
    ast->m_symbol_table->clearValues();                            \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ListAffectationProcessor", "[language]")
{
  auto stringify = [](auto&& value) {
    std::ostringstream os;
    os << std::boolalpha << value;
    return os.str();
  };

  SECTION("basic types list affectations")
  {
    SECTION("R*R^2*R^2x2*string")
    {
      CHECK_AFFECTATION_RESULT(R"(let (x,u,A,s): R*R^2*R^2x2*string, (x,u,A,s) = (1.2, [2,3], [[4,3],[2,1]], "foo");)",
                               "x", double{1.2});
      CHECK_AFFECTATION_RESULT(R"(let (x,u,A,s): R*R^2*R^2x2*string, (x,u,A,s) = (1.2, [2,3], [[4,3],[2,1]], "foo");)",
                               "u", (TinyVector<2>{2, 3}));
      CHECK_AFFECTATION_RESULT(R"(let (x,u,A,s): R*R^2*R^2x2*string, (x,u,A,s) = (1.2, [2,3], [[4,3],[2,1]], "foo");)",
                               "A", (TinyMatrix<2>{4, 3, 2, 1}));
      CHECK_AFFECTATION_RESULT(R"(let (x,u,A,s): R*R^2*R^2x2*string, (x,u,A,s) = (1.2, [2,3], [[4,3],[2,1]], "foo");)",
                               "s", std::string{"foo"});
    }

    SECTION("compound with string conversion")
    {
      CHECK_AFFECTATION_RESULT(R"(let z:R, z = 3; let (x,u,s):R*R^2*string, (x,u,s) = (1.2, [2,3], z);)", "s",
                               stringify(double{3}));
      CHECK_AFFECTATION_RESULT(R"(let v:R^1, v = 7; let  (x,u,s):R*R^2*string, (x,u,s) = (1.2, [2,3], v);)", "s",
                               stringify(TinyVector<1>{7}));
      CHECK_AFFECTATION_RESULT(R"(let v: R^2, v = [6,3]; let (x,u,s):R*R^2*string, (x,u,s) = (1.2, [2,3], v);)", "s",
                               stringify(TinyVector<2>{6, 3}));
      CHECK_AFFECTATION_RESULT(R"(let v:R^3, v = [1,2,3]; let (x,u,s):R*R^2*string, (x,u,s) = (1.2, [2,3], v);)", "s",
                               stringify(TinyVector<3>{1, 2, 3}));
    }

    SECTION("compound R^d from '0'")
    {
      CHECK_AFFECTATION_RESULT(R"(let (x,y,z):R^3*R^2*R^1, (x,y,z) = (0,0,0);)", "x", (TinyVector<3>{zero}));
      CHECK_AFFECTATION_RESULT(R"(let (x,y,z):R^3*R^2*R^1, (x,y,z) = (0,0,0);)", "y", (TinyVector<2>{zero}));
      CHECK_AFFECTATION_RESULT(R"(let (x,y,z):R^3*R^2*R^1, (x,y,z) = (0,0,0);)", "z", (TinyVector<1>{zero}));
    }

    SECTION("compound R^dxd from '0'")
    {
      CHECK_AFFECTATION_RESULT(R"(let (x,y,z):R^3x3*R^2x2*R^1x1, (x,y,z) = (0,0,0);)", "x", (TinyMatrix<3>{zero}));
      CHECK_AFFECTATION_RESULT(R"(let (x,y,z):R^3x3*R^2x2*R^1x1, (x,y,z) = (0,0,0);)", "y", (TinyMatrix<2>{zero}));
      CHECK_AFFECTATION_RESULT(R"(let (x,y,z):R^3x3*R^2x2*R^1x1, (x,y,z) = (0,0,0);)", "z", (TinyMatrix<1>{zero}));
    }
  }

  SECTION("lists with tuples affectation")
  {
    SECTION("with (B)")
    {
      SECTION("from value")
      {
        std::string_view data = R"(
let (b1,b2): (B)*(B), (b1,b2) = (false, true);
)";

        CHECK_AFFECTATION_RESULT(data, "b1", (std::vector<bool>{false}));
        CHECK_AFFECTATION_RESULT(data, "b2", (std::vector<bool>{true}));
      }

      SECTION("from list")
      {
        std::string_view data = R"(
let (b1,b2): (B)*B, (b1,b2) = ((true,false), true);
)";

        CHECK_AFFECTATION_RESULT(data, "b1", (std::vector<bool>{true, false}));
        CHECK_AFFECTATION_RESULT(data, "b2", true);
      }

      SECTION("from (B)")
      {
        std::string_view data = R"(
let b : (B), b = (true, false);
let (b1,b2) : (B)*B, (b1,b2) = (b, false);
)";
        CHECK_AFFECTATION_RESULT(data, "b1", (std::vector<bool>{true, false}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }
    }

    SECTION("(N)")
    {
      SECTION("from value")
      {
        std::string_view data = R"(
let n:N, n = 9;
let (n1,n2,n3): (N)*(N)*(N), (n1,n2,n3) = (false, 1, n);
)";

        CHECK_AFFECTATION_RESULT(data, "n1", (std::vector<uint64_t>{false}));
        CHECK_AFFECTATION_RESULT(data, "n2", (std::vector<uint64_t>{1}));
        CHECK_AFFECTATION_RESULT(data, "n3", (std::vector<uint64_t>{9}));
      }

      SECTION("from list")
      {
        std::string_view data = R"(
let (n,m) :(N)*N, (n,m) = ((1,2,3), 7);
)";
        CHECK_AFFECTATION_RESULT(data, "n", (std::vector<uint64_t>{1, 2, 3}));
        CHECK_AFFECTATION_RESULT(data, "m", 7ul);
      }

      SECTION("from (B)")
      {
        std::string_view data = R"(
let b :(B), b = (true,false,true);
let (n,m) :(N)*(N), (n,m) = (b, (false,true));
)";
        CHECK_AFFECTATION_RESULT(data, "n", (std::vector<uint64_t>{1, 0, 1}));
        CHECK_AFFECTATION_RESULT(data, "m", (std::vector<uint64_t>{0, 1}));
      }

      SECTION("from (N)")
      {
        std::string_view data = R"(
let l:(N), l = (1,2,3);
let (n,m):(N)*(N), (n,m) = (l,(2,3,1));
)";
        CHECK_AFFECTATION_RESULT(data, "n", (std::vector<uint64_t>{1, 2, 3}));
        CHECK_AFFECTATION_RESULT(data, "m", (std::vector<uint64_t>{2, 3, 1}));
      }

      SECTION("from (Z)")
      {
        std::string_view data = R"(
let z:(Z), z = (1,2,3);
let (n,x):(N)*R, (n,x) = (z,2.3);
)";
        CHECK_AFFECTATION_RESULT(data, "n", (std::vector<uint64_t>{1, 2, 3}));
        CHECK_AFFECTATION_RESULT(data, "x", double{2.3});
      }
    }

    SECTION("(Z)")
    {
      SECTION("from value")
      {
        std::string_view data = R"(
let n:N, n = 9;
let (z1,z2,z3): (Z)*(Z)*(Z), (z1,z2,z3) = (false, -1, n);
)";

        CHECK_AFFECTATION_RESULT(data, "z1", (std::vector<int64_t>{false}));
        CHECK_AFFECTATION_RESULT(data, "z2", (std::vector<int64_t>{-1}));
        CHECK_AFFECTATION_RESULT(data, "z3", (std::vector<int64_t>{9}));
      }

      SECTION("from list")
      {
        std::string_view data = R"(
let (z1,z2):(Z)*(Z), (z1,z2) = ((1,2,3),(2,1,3));
)";
        CHECK_AFFECTATION_RESULT(data, "z1", (std::vector<int64_t>{1, 2, 3}));
        CHECK_AFFECTATION_RESULT(data, "z2", (std::vector<int64_t>{2, 1, 3}));
      }

      SECTION("from (N)")
      {
        std::string_view data = R"(
let m : (N), m = (1,2,3);
let (z,n) : (Z)*N, (z,n) = (m,2);
)";
        CHECK_AFFECTATION_RESULT(data, "z", (std::vector<int64_t>{1, 2, 3}));
        CHECK_AFFECTATION_RESULT(data, "n", 2ul);
      }

      SECTION("from (Z)")
      {
        std::string_view data = R"(
let z0:(Z), z0 = (1,2,3);
let (z,a): (Z)*Z, (z,a) = (z0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "z", (std::vector<int64_t>{1, 2, 3}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }
    }

    SECTION("(R)")
    {
      SECTION("from value")
      {
        std::string_view data = R"(
let n:N, n = 9;
let (r1,r2,r3,r4): (R)*(R)*(R)*(R), (r1,r2,r3,r4) = (false, -1, n, 2.3);
)";

        CHECK_AFFECTATION_RESULT(data, "r1", (std::vector<double>{false}));
        CHECK_AFFECTATION_RESULT(data, "r2", (std::vector<double>{-1}));
        CHECK_AFFECTATION_RESULT(data, "r3", (std::vector<double>{9}));
        CHECK_AFFECTATION_RESULT(data, "r4", (std::vector<double>{2.3}));
      }

      SECTION("from list")
      {
        std::string_view data = R"(
let (r1,r2):(R)*(R), (r1,r2) = ((1.2,2,3.1),(2.4,1,1.3));
)";
        CHECK_AFFECTATION_RESULT(data, "r1", (std::vector<double>{1.2, 2, 3.1}));
        CHECK_AFFECTATION_RESULT(data, "r2", (std::vector<double>{2.4, 1, 1.3}));
      }

      SECTION("from (N)")
      {
        std::string_view data = R"(
let m : (N), m = (1,2,3);
let (r,n) : (R)*N, (r,n) = (m,2);
)";
        CHECK_AFFECTATION_RESULT(data, "r", (std::vector<double>{1, 2, 3}));
        CHECK_AFFECTATION_RESULT(data, "n", 2ul);
      }

      SECTION("from (Z)")
      {
        std::string_view data = R"(
let z0:(Z), z0 = (1,2,3);
let (r,a): (R)*Z, (r,a) = (z0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "r", (std::vector<double>{1, 2, 3}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }

      SECTION("from (R)")
      {
        std::string_view data = R"(
let r0:(R), r0 = (1.2,2,3);
let (r,a): (R)*Z, (r,a) = (r0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "r", (std::vector<double>{1.2, 2, 3}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }
    }

    SECTION("(R^1)")
    {
      using R1 = TinyVector<1>;

      SECTION("from value")
      {
        std::string_view data = R"(
let n:N, n = 9;
let (X1,X2,X3,X4,X5): (R^1)*(R^1)*(R^1)*(R^1)*(R^1), (X1,X2,X3,X4,X5) = (false, -1, n, 2.3, [7.2]);
)";

        CHECK_AFFECTATION_RESULT(data, "X1", (std::vector<R1>{R1(false)}));
        CHECK_AFFECTATION_RESULT(data, "X2", (std::vector<R1>{R1(-1)}));
        CHECK_AFFECTATION_RESULT(data, "X3", (std::vector<R1>{R1(9)}));
        CHECK_AFFECTATION_RESULT(data, "X4", (std::vector<R1>{R1(2.3)}));
        CHECK_AFFECTATION_RESULT(data, "X5", (std::vector<R1>{R1(7.2)}));
      }

      SECTION("from list")
      {
        std::string_view data = R"(
let (X1,X2):(R^1)*(R^1), (X1,X2) = (([1.2],2,3.1),([2.4],1, true,1.3));
)";
        CHECK_AFFECTATION_RESULT(data, "X1", (std::vector<R1>{R1(1.2), R1(2), R1(3.1)}));
        CHECK_AFFECTATION_RESULT(data, "X2", (std::vector<R1>{R1(2.4), R1(1), R1(true), R1(1.3)}));
      }

      SECTION("from (N)")
      {
        std::string_view data = R"(
let m : (N), m = (1,2,3);
let (X,n) : (R^1)*N, (X,n) = (m,2);
)";
        CHECK_AFFECTATION_RESULT(data, "X", (std::vector<R1>{R1(1), R1(2), R1(3)}));
        CHECK_AFFECTATION_RESULT(data, "n", 2ul);
      }

      SECTION("from (Z)")
      {
        std::string_view data = R"(
let z0:(Z), z0 = (1,2,3);
let (X,a): (R^1)*Z, (X,a) = (z0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "X", (std::vector<R1>{R1(1), R1(2), R1(3)}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }

      SECTION("from (R)")
      {
        std::string_view data = R"(
let r0:(R), r0 = (1.2,2,3);
let (X,a): (R^1)*Z, (X,a) = (r0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "X", (std::vector<R1>{R1(1.2), R1(2), R1(3)}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }

      SECTION("from (R^1)")
      {
        std::string_view data = R"(
let X0:(R^1), X0 = (1.2,2,3);
let (X,a): (R^1)*Z, (X,a) = (X0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "X", (std::vector<R1>{R1(1.2), R1(2), R1(3)}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }
    }

    SECTION("R^2")
    {
      using R2 = TinyVector<2>;

      SECTION("from value")
      {
        std::string_view data = R"(
let (X1,X2): (R^2)*(R^2), (X1,X2) = ([1,2], 0);
)";

        CHECK_AFFECTATION_RESULT(data, "X1", (std::vector<R2>{R2(1, 2)}));
        CHECK_AFFECTATION_RESULT(data, "X2", (std::vector<R2>{R2(0, 0)}));
      }

      SECTION("from list")
      {
        std::string_view data = R"(
let (X1,X2):(R^2)*(R^2), (X1,X2) = (([1.2, 2],0,[3,1]),([2.4,1],[true,1.3]));
)";
        CHECK_AFFECTATION_RESULT(data, "X1", (std::vector<R2>{R2(1.2, 2), R2(0, 0), R2(3, 1)}));
        CHECK_AFFECTATION_RESULT(data, "X2", (std::vector<R2>{R2(2.4, 1), R2(true, 1.3)}));
      }

      SECTION("from (R^2)")
      {
        std::string_view data = R"(
let X0:(R^2), X0 = ([1.2,2],[3,1],0);
let (X,a): (R^2)*Z, (X,a) = (X0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "X", (std::vector<R2>{R2(1.2, 2), R2(3, 1), R2(0, 0)}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }
    }

    SECTION("R^3")
    {
      using R3 = TinyVector<3>;

      SECTION("from value")
      {
        std::string_view data = R"(
let (X1,X2): (R^3)*(R^3), (X1,X2) = ([1,2,3], 0);
)";

        CHECK_AFFECTATION_RESULT(data, "X1", (std::vector<R3>{R3(1, 2, 3)}));
        CHECK_AFFECTATION_RESULT(data, "X2", (std::vector<R3>{R3(0, 0, 0)}));
      }

      SECTION("from list")
      {
        std::string_view data = R"(
let (X1,X2):(R^3)*(R^3), (X1,X2) = (([5, 1.2, 2],0,[3,6,1]),([2.4,1,1.3],[true,1.3,2]));
)";
        CHECK_AFFECTATION_RESULT(data, "X1", (std::vector<R3>{R3(5, 1.2, 2), R3(0, 0, 0), R3(3, 6, 1)}));
        CHECK_AFFECTATION_RESULT(data, "X2", (std::vector<R3>{R3(2.4, 1, 1.3), R3(true, 1.3, 2)}));
      }

      SECTION("from (R^3)")
      {
        std::string_view data = R"(
let X0:(R^3), X0 = ([1.2,-1.3,2],[3,1,5],0);
let (X,a): (R^3)*Z, (X,a) = (X0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "X", (std::vector<R3>{R3(1.2, -1.3, 2), R3(3, 1, 5), R3(0, 0, 0)}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }
    }

    SECTION("R^1x1")
    {
      using R1x1 = TinyMatrix<1>;

      SECTION("from value")
      {
        std::string_view data = R"(
let n:N, n = 9;
let (X1,X2,X3,X4,X5): (R^1x1)*(R^1x1)*(R^1x1)*(R^1x1)*(R^1x1), (X1,X2,X3,X4,X5) = (false, -1, n, 2.3, [[7.2]]);
)";

        CHECK_AFFECTATION_RESULT(data, "X1", (std::vector<R1x1>{R1x1(false)}));
        CHECK_AFFECTATION_RESULT(data, "X2", (std::vector<R1x1>{R1x1(-1)}));
        CHECK_AFFECTATION_RESULT(data, "X3", (std::vector<R1x1>{R1x1(9)}));
        CHECK_AFFECTATION_RESULT(data, "X4", (std::vector<R1x1>{R1x1(2.3)}));
        CHECK_AFFECTATION_RESULT(data, "X5", (std::vector<R1x1>{R1x1(7.2)}));
      }

      SECTION("from list")
      {
        std::string_view data = R"(
let (X1,X2):(R^1x1)*(R^1x1), (X1,X2) = (([[1.2]],2,3.1),([[2.4]],1, true,1.3));
)";
        CHECK_AFFECTATION_RESULT(data, "X1", (std::vector<R1x1>{R1x1(1.2), R1x1(2), R1x1(3.1)}));
        CHECK_AFFECTATION_RESULT(data, "X2", (std::vector<R1x1>{R1x1(2.4), R1x1(1), R1x1(true), R1x1(1.3)}));
      }

      SECTION("from (N)")
      {
        std::string_view data = R"(
let m : (N), m = (1,2,3);
let (X,n) : (R^1x1)*N, (X,n) = (m,2);
)";
        CHECK_AFFECTATION_RESULT(data, "X", (std::vector<R1x1>{R1x1(1), R1x1(2), R1x1(3)}));
        CHECK_AFFECTATION_RESULT(data, "n", 2ul);
      }

      SECTION("from (Z)")
      {
        std::string_view data = R"(
let z0:(Z), z0 = (1,2,3);
let (X,a): (R^1x1)*Z, (X,a) = (z0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "X", (std::vector<R1x1>{R1x1(1), R1x1(2), R1x1(3)}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }

      SECTION("from (R)")
      {
        std::string_view data = R"(
let r0:(R), r0 = (1.2,2,3);
let (X,a): (R^1x1)*Z, (X,a) = (r0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "X", (std::vector<R1x1>{R1x1(1.2), R1x1(2), R1x1(3)}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }

      SECTION("from (R^1x1)")
      {
        std::string_view data = R"(
let X0:(R^1x1), X0 = (1.2,2,3);
let (X,a): (R^1x1)*Z, (X,a) = (X0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "X", (std::vector<R1x1>{R1x1(1.2), R1x1(2), R1x1(3)}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }
    }

    SECTION("R^2x2")
    {
      using R2x2 = TinyMatrix<2>;

      SECTION("from value")
      {
        std::string_view data = R"(
let (X1,X2): (R^2x2)*(R^2x2), (X1,X2) = ([[1,2],[3,4]], 0);
)";

        CHECK_AFFECTATION_RESULT(data, "X1", (std::vector<R2x2>{R2x2(1, 2, 3, 4)}));
        CHECK_AFFECTATION_RESULT(data, "X2", (std::vector<R2x2>{R2x2(0, 0, 0, 0)}));
      }

      SECTION("from list")
      {
        std::string_view data = R"(
let (X1,X2):(R^2x2)*(R^2x2), (X1,X2) = (([[1.2, 2],[7,4]],0,[[3,1],[5,2]]),([[2.4,1],[1,3]],[[true,1.3],[2,2]]));
)";
        CHECK_AFFECTATION_RESULT(data, "X1",
                                 (std::vector<R2x2>{R2x2(1.2, 2, 7, 4), R2x2(0, 0, 0, 0), R2x2(3, 1, 5, 2)}));
        CHECK_AFFECTATION_RESULT(data, "X2", (std::vector<R2x2>{R2x2(2.4, 1, 1, 3), R2x2(true, 1.3, 2, 2)}));
      }

      SECTION("from (R^2x2)")
      {
        std::string_view data = R"(
let X0:(R^2x2), X0 = ([[1.2,2],[3,1]],[[3,1],[5,8]],0);
let (X,a): (R^2x2)*Z, (X,a) = (X0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "X",
                                 (std::vector<R2x2>{R2x2(1.2, 2, 3, 1), R2x2(3, 1, 5, 8), R2x2(0, 0, 0, 0)}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }
    }

    SECTION("R^3x3")
    {
      using R3x3 = TinyMatrix<3>;

      SECTION("from value")
      {
        std::string_view data = R"(
let (X1,X2): (R^3x3)*(R^3x3), (X1,X2) = ([[1,2,3],[4,5,6],[7,8,9]], 0);
)";

        CHECK_AFFECTATION_RESULT(data, "X1", (std::vector<R3x3>{R3x3(1, 2, 3, 4, 5, 6, 7, 8, 9)}));
        CHECK_AFFECTATION_RESULT(data, "X2", (std::vector<R3x3>{R3x3(0, 0, 0, 0, 0, 0, 0, 0, 0)}));
      }

      SECTION("from list")
      {
        std::string_view data = R"(
let (X1,X2):(R^3x3)*(R^3x3), (X1,X2) = (([[1.2, 2, 1],[3,1,9],[7,5,4]],0,[[3,0.3,1],[5,7,2],[2,1,8]]),([[3,2.4,1],[2,1,3],[2.2,1.8,3]],[[true,2,1.3],[2.1,4,2],[1.2,2.4,1.2]]));
)";
        CHECK_AFFECTATION_RESULT(data, "X1",
                                 (std::vector<R3x3>{R3x3(1.2, 2, 1, 3, 1, 9, 7, 5, 4), R3x3(0, 0, 0, 0, 0, 0, 0, 0, 0),
                                                    R3x3(3, 0.3, 1, 5, 7, 2, 2, 1, 8)}));
        CHECK_AFFECTATION_RESULT(data, "X2",
                                 (std::vector<R3x3>{R3x3(3, 2.4, 1, 2, 1, 3, 2.2, 1.8, 3),
                                                    R3x3(true, 2, 1.3, 2.1, 4, 2, 1.2, 2.4, 1.2)}));
      }

      SECTION("from (R^3x3)")
      {
        std::string_view data = R"(
let X0:(R^3x3), X0 = ([[1.2,7,2],[2,1,6],[1,3,1]],[[3,1,6],[2,7,1],[1,5,8]]);
let (X,a): (R^3x3)*Z, (X,a) = (X0,3);
)";
        CHECK_AFFECTATION_RESULT(data, "X",
                                 (std::vector<R3x3>{R3x3(1.2, 7, 2, 2, 1, 6, 1, 3, 1),
                                                    R3x3(3, 1, 6, 2, 7, 1, 1, 5, 8)}));
        CHECK_AFFECTATION_RESULT(data, "a", 3l);
      }
    }

    SECTION("with (string)")
    {
      SECTION("from value")
      {
        std::string_view data = R"(
let n: N, n = 3;
let (s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11)
   : (string)*(string)*(string)*(string)*(string)*(string)*(string)*(string)*(string)*(string)*(string),
   (s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11) = (false, n, 1, 1.3, [1], [1,2], [1,2,3], [[1]], [[1,2],[3,4]], [[1,2,3],[4,5,6],[7,8,9]], "foo");
)";

        CHECK_AFFECTATION_RESULT(data, "s1", (std::vector<std::string>{stringify(false)}));
        CHECK_AFFECTATION_RESULT(data, "s2", (std::vector<std::string>{stringify(3ul)}));
        CHECK_AFFECTATION_RESULT(data, "s3", (std::vector<std::string>{stringify(1l)}));
        CHECK_AFFECTATION_RESULT(data, "s4", (std::vector<std::string>{stringify(double{1.3})}));
        CHECK_AFFECTATION_RESULT(data, "s5", (std::vector<std::string>{stringify(TinyVector<1>{1})}));
        CHECK_AFFECTATION_RESULT(data, "s6", (std::vector<std::string>{stringify(TinyVector<2>{1, 2})}));
        CHECK_AFFECTATION_RESULT(data, "s7", (std::vector<std::string>{stringify(TinyVector<3>{1, 2, 3})}));
        CHECK_AFFECTATION_RESULT(data, "s8", (std::vector<std::string>{stringify(TinyMatrix<1>{1})}));
        CHECK_AFFECTATION_RESULT(data, "s9", (std::vector<std::string>{stringify(TinyMatrix<2>{1, 2, 3, 4})}));
        CHECK_AFFECTATION_RESULT(data, "s10",
                                 (std::vector<std::string>{stringify(TinyMatrix<3>{1, 2, 3, 4, 5, 6, 7, 8, 9})}));
        CHECK_AFFECTATION_RESULT(data, "s11", (std::vector<std::string>{stringify("foo")}));
      }

      SECTION("from list")
      {
        std::string_view data = R"(
let n: N, n = 3;
let (s1,s2): (string)*string,
    (s1,s2) = ((false, n, 1, 1.3, [1], [1,2], [1,2,3], [[1]], [[1,2],[3,4]], [[1,2,3],[4,5,6],[7,8,9]], "foo"), "bar");
)";

        CHECK_AFFECTATION_RESULT(data, "s1",
                                 (std::vector<std::string>{stringify(false), stringify(3ul), stringify(1l),
                                                           stringify(double{1.3}), stringify(TinyVector<1>{1}),
                                                           stringify(TinyVector<2>{1, 2}),
                                                           stringify(TinyVector<3>{1, 2, 3}),
                                                           stringify(TinyMatrix<1>{1}),
                                                           stringify(TinyMatrix<2>{1, 2, 3, 4}),
                                                           stringify(TinyMatrix<3>{1, 2, 3, 4, 5, 6, 7, 8, 9}),
                                                           stringify("foo")}));
        CHECK_AFFECTATION_RESULT(data, "s2", std::string("bar"));
      }

      SECTION("from (B)")
      {
        std::string_view data = R"(
let b : (B), b = (true, false);
let (s1,b2) : (string)*B, (s1,b2) = (b, false);
)";
        CHECK_AFFECTATION_RESULT(data, "s1", (std::vector<std::string>{stringify(true), stringify(false)}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }

      SECTION("from (N)")
      {
        std::string_view data = R"(
let n : (N), n = (1, 2);
let (s1,b2) : (string)*B, (s1,b2) = (n, false);
)";
        CHECK_AFFECTATION_RESULT(data, "s1", (std::vector<std::string>{stringify(1ul), stringify(2ul)}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }

      SECTION("from (Z)")
      {
        std::string_view data = R"(
let z : (Z), z = (1, -2);
let (s1,b2) : (string)*B, (s1,b2) = (z, false);
)";
        CHECK_AFFECTATION_RESULT(data, "s1", (std::vector<std::string>{stringify(1l), stringify(-2l)}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }

      SECTION("from (R)")
      {
        std::string_view data = R"(
let r : (R), r = (1, -2.3);
let (s1,b2) : (string)*B, (s1,b2) = (r, false);
)";
        CHECK_AFFECTATION_RESULT(data, "s1", (std::vector<std::string>{stringify(double{1}), stringify(double{-2.3})}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }

      SECTION("from (R^1)")
      {
        std::string_view data = R"(
let r : (R^1), r = (1, -2.3);
let (s1,b2) : (string)*B, (s1,b2) = (r, false);
)";
        CHECK_AFFECTATION_RESULT(data, "s1",
                                 (std::vector<std::string>{stringify(TinyVector<1>{1}),
                                                           stringify(TinyVector<1>{-2.3})}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }

      SECTION("from (R^2)")
      {
        std::string_view data = R"(
let r : (R^2), r = ([1,2],[-2.3,4]);
let (s1,b2) : (string)*B, (s1,b2) = (r, false);
)";
        CHECK_AFFECTATION_RESULT(data, "s1",
                                 (std::vector<std::string>{stringify(TinyVector<2>{1, 2}),
                                                           stringify(TinyVector<2>{-2.3, 4})}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }

      SECTION("from (R^3)")
      {
        std::string_view data = R"(
let r : (R^3), r = ([1,2,3],[-2.3,4,7]);
let (s1,b2) : (string)*B, (s1,b2) = (r, false);
)";
        CHECK_AFFECTATION_RESULT(data, "s1",
                                 (std::vector<std::string>{stringify(TinyVector<3>{1, 2, 3}),
                                                           stringify(TinyVector<3>{-2.3, 4, 7})}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }

      SECTION("from (R^1x1)")
      {
        std::string_view data = R"(
let r : (R^1x1), r = (1, -2.3);
let (s1,b2) : (string)*B, (s1,b2) = (r, false);
)";
        CHECK_AFFECTATION_RESULT(data, "s1",
                                 (std::vector<std::string>{stringify(TinyMatrix<1>{1}),
                                                           stringify(TinyMatrix<1>{-2.3})}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }

      SECTION("from (R^2x2)")
      {
        std::string_view data = R"(
let r : (R^2x2), r = ([[1,2],[3,4]], [[-2.3,4],[7,2]]);
let (s1,b2) : (string)*B, (s1,b2) = (r, false);
)";
        CHECK_AFFECTATION_RESULT(data, "s1",
                                 (std::vector<std::string>{stringify(TinyMatrix<2>{1, 2, 3, 4}),
                                                           stringify(TinyMatrix<2>{-2.3, 4, 7, 2})}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }

      SECTION("from (R^3)")
      {
        std::string_view data = R"(
let r : (R^3x3), r = ([[1,2,3],[4,5,6],[7,8,9]], [[-2.3,4,7],[6,2,1],[8,3,4]]);
let (s1,b2) : (string)*B, (s1,b2) = (r, false);
)";
        CHECK_AFFECTATION_RESULT(data, "s1",
                                 (std::vector<std::string>{stringify(TinyMatrix<3>{1, 2, 3, 4, 5, 6, 7, 8, 9}),
                                                           stringify(TinyMatrix<3>{-2.3, 4, 7, 6, 2, 1, 8, 3, 4})}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }

      SECTION("from (string)")
      {
        std::string_view data = R"(
let s : (string), s = ("foo", "bar", "foobar");
let (s1,b2) : (string)*B, (s1,b2) = (s, false);
)";
        CHECK_AFFECTATION_RESULT(data, "s1", (std::vector<std::string>{"foo", "bar", "foobar"}));
        CHECK_AFFECTATION_RESULT(data, "b2", false);
      }
    }
  }

  SECTION("list from tuple affectation")
  {
    SECTION("from (B)")
    {
      std::string_view data = R"(
let t:(B), t = (true, false, true, false, true, false, true);
let (b,n,z,r,r1,r11,s):B*N*Z*R*R^1*R^1x1*string, (b,n,z,r,r1,r11,s)=t;
)";

      CHECK_AFFECTATION_RESULT(data, "b", (true));
      CHECK_AFFECTATION_RESULT(data, "n", (0ul));
      CHECK_AFFECTATION_RESULT(data, "z", (1l));
      CHECK_AFFECTATION_RESULT(data, "r", (double{0}));
      CHECK_AFFECTATION_RESULT(data, "r1", (TinyVector<1>{1}));
      CHECK_AFFECTATION_RESULT(data, "r11", (TinyMatrix<1>{0}));
      CHECK_AFFECTATION_RESULT(data, "s", (std::string{stringify(true)}));
    }

    SECTION("from (N)")
    {
      std::string_view data = R"(
let t:(N), t = (1, 2, 3, 4, 5, 6);
let (n,z,r,r1,r11,s):N*Z*R*R^1*R^1x1*string, (n,z,r,r1,r11,s)=t;
)";

      CHECK_AFFECTATION_RESULT(data, "n", (1ul));
      CHECK_AFFECTATION_RESULT(data, "z", (2l));
      CHECK_AFFECTATION_RESULT(data, "r", (double{3}));
      CHECK_AFFECTATION_RESULT(data, "r1", (TinyVector<1>{4}));
      CHECK_AFFECTATION_RESULT(data, "r11", (TinyMatrix<1>{5}));
      CHECK_AFFECTATION_RESULT(data, "s", (std::string{stringify(6ul)}));
    }

    SECTION("from (Z)")
    {
      std::string_view data = R"(
let t:(Z), t = (1, -2, 3, -4, 5, 6);
let (n,z,r,r1,r11,s):N*Z*R*R^1*R^1x1*string, (n,z,r,r1,r11,s)=t;
)";

      CHECK_AFFECTATION_RESULT(data, "n", (1ul));
      CHECK_AFFECTATION_RESULT(data, "z", (-2l));
      CHECK_AFFECTATION_RESULT(data, "r", (double{3}));
      CHECK_AFFECTATION_RESULT(data, "r1", (TinyVector<1>{-4}));
      CHECK_AFFECTATION_RESULT(data, "r11", (TinyMatrix<1>{5}));
      CHECK_AFFECTATION_RESULT(data, "s", (std::string{stringify(6l)}));
    }

    SECTION("from (R)")
    {
      std::string_view data = R"(
let t:(R), t = (1.2, -2.3, 7.3, -4.2);
let (r,r1,r11,s):R*R^1*R^1x1*string, (r,r1,r11,s)=t;
)";

      CHECK_AFFECTATION_RESULT(data, "r", (double{1.2}));
      CHECK_AFFECTATION_RESULT(data, "r1", (TinyVector<1>{-2.3}));
      CHECK_AFFECTATION_RESULT(data, "r11", (TinyMatrix<1>{7.3}));
      CHECK_AFFECTATION_RESULT(data, "s", (std::string{stringify(-4.2)}));
    }

    SECTION("from (R^1)")
    {
      std::string_view data = R"(
let t:(R^1), t = (1.2, -[2.3]);
let (r1,s):R^1*string, (r1,s)=t;
)";

      CHECK_AFFECTATION_RESULT(data, "r1", (TinyVector<1>{1.2}));
      CHECK_AFFECTATION_RESULT(data, "s", (std::string{stringify(TinyVector<1>{-2.3})}));
    }

    SECTION("from (R^2)")
    {
      std::string_view data = R"(
let t:(R^2), t = ([1.2, 2], [2.3, 0]);
let (r2,s):R^2*string, (r2,s)=t;
)";

      CHECK_AFFECTATION_RESULT(data, "r2", (TinyVector<2>{1.2, 2}));
      CHECK_AFFECTATION_RESULT(data, "s", (std::string{stringify(TinyVector<2>{2.3, 0})}));
    }

    SECTION("from (R^3)")
    {
      std::string_view data = R"(
let t:(R^3), t = ([1.2, 2, -1], [2.3, 0, 2]);
let (r3,s):R^3*string, (r3,s)=t;
)";

      CHECK_AFFECTATION_RESULT(data, "r3", (TinyVector<3>{1.2, 2, -1}));
      CHECK_AFFECTATION_RESULT(data, "s", (std::string{stringify(TinyVector<3>{2.3, 0, 2})}));
    }

    SECTION("from (R^1x1)")
    {
      std::string_view data = R"(
let t:(R^1x1), t = (1.2, -[[2.3]]);
let (r11,s):R^1x1*string, (r11,s)=t;
)";

      CHECK_AFFECTATION_RESULT(data, "r11", (TinyMatrix<1>{1.2}));
      CHECK_AFFECTATION_RESULT(data, "s", (std::string{stringify(TinyMatrix<1>{-2.3})}));
    }

    SECTION("from (R^2x2)")
    {
      std::string_view data = R"(
let t:(R^2x2), t = ([[1.2, 2], [2.3, 0]], [[1.1, 2.2], [-1.3, 2]]);
let (r22,s):R^2x2*string, (r22,s)=t;
)";

      CHECK_AFFECTATION_RESULT(data, "r22", (TinyMatrix<2>{1.2, 2, 2.3, 0}));
      CHECK_AFFECTATION_RESULT(data, "s", (std::string{stringify(TinyMatrix<2>{1.1, 2.2, -1.3, 2})}));
    }

    SECTION("from (R^3x3)")
    {
      std::string_view data = R"(
let t:(R^3x3), t = ([[1.2, 2, -1], [2.3, 0, 2], [1, 4, 5]], 0);
let (r33,s):R^3x3*string, (r33,s)=t;
)";

      CHECK_AFFECTATION_RESULT(data, "r33", (TinyMatrix<3>{1.2, 2, -1, 2.3, 0, 2, 1, 4, 5}));
      CHECK_AFFECTATION_RESULT(data, "s", (std::string{stringify(TinyMatrix<3>{zero})}));
    }

    SECTION("from (string)")
    {
      std::string_view data = R"(
let t:(string), t = ("foo", "bar");
let (s1,s2):string*string, (s1,s2)=t;
)";

      CHECK_AFFECTATION_RESULT(data, "s1", (std::string{"foo"}));
      CHECK_AFFECTATION_RESULT(data, "s2", (std::string{"bar"}));
    }

    SECTION("errors")
    {
      SECTION("negative (Z) -> N")
      {
        std::string_view data = R"(
let t:(Z), t = (2, 3, -4, 5);
let (a,b,c,d):Z*N*N*R , (a,b,c,d)=t;
)";

        CHECK_AFFECTATION_THROW_WITH(data, "trying to affect negative value (-4)");
      }

      SECTION("tuple too small")
      {
        std::string_view data = R"(
let t:(Z), t = (2, 3, 4);
let (a,b,c,d):Z*N*N*R , (a,b,c,d)=t;
)";

        CHECK_AFFECTATION_THROW_WITH(data, "cannot affect a (Z) of size 3 to a Z*N*N*R");
      }

      SECTION("tuple too large")
      {
        std::string_view data = R"(
let t:(Z), t = (1, 2, 3, 4, 5);
let (a,b,c,d):Z*N*N*R , (a,b,c,d)=t;
)";

        CHECK_AFFECTATION_THROW_WITH(data, "cannot affect a (Z) of size 5 to a Z*N*N*R");
      }
    }
  }
}
