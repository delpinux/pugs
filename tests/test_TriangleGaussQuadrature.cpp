#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <analysis/TriangleGaussQuadrature.hpp>
#include <geometry/TriangleTransformation.hpp>
#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("TriangleGaussQuadrature", "[analysis]")
{
  auto integrate = [](auto f, auto quadrature_formula) {
    using R2 = TinyVector<2>;

    const R2 A{-1, -1};
    const R2 B{+1, -1};
    const R2 C{-1, +1};

    TriangleTransformation<2> t{A, B, C};

    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    auto value = weight_list[0] * f(t(point_list[0]));
    for (size_t i = 1; i < weight_list.size(); ++i) {
      value += weight_list[i] * f(t(point_list[i]));
    }

    return t.jacobianDeterminant() * value;
  };

  auto integrate_on_triangle = [](auto f, auto quadrature_formula, const TinyVector<2>& a, const TinyVector<2>& b,
                                  const TinyVector<2>& c) {
    TriangleTransformation<2> t(a, b, c);

    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    auto value = weight_list[0] * f(t(point_list[0]));
    for (size_t i = 1; i < weight_list.size(); ++i) {
      value += weight_list[i] * f(t(point_list[i]));
    }

    return t.jacobianDeterminant() * value;
  };

  auto get_order = [&integrate, &integrate_on_triangle](auto f, auto quadrature_formula, const double exact_value) {
    using R2               = TinyVector<2>;
    const double int_T_hat = integrate(f, quadrature_formula);
    const double int_refined   //
      = integrate_on_triangle(f, quadrature_formula, R2{-1, -1}, R2{0, -1}, R2{-1, 0}) +
        integrate_on_triangle(f, quadrature_formula, R2{0, -1}, R2{0, 0}, R2{-1, 0}) +
        integrate_on_triangle(f, quadrature_formula, R2{0, -1}, R2{1, -1}, R2{0, 0}) +
        integrate_on_triangle(f, quadrature_formula, R2{-1, 0}, R2{0, 0}, R2{-1, 1});

    return -std::log(std::abs(int_refined - exact_value) / std::abs(int_T_hat - exact_value)) / std::log(2);
  };

  auto p0 = [](const TinyVector<2>&) { return 2; };
  auto p1 = [](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return 2 * x + 3 * y + 1;
  };
  auto p2 = [&p1](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p1(X) * (2.5 * x - 3 * y + 3);
  };
  auto p3 = [&p2](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p2(X) * (-1.5 * x + 3 * y - 3);
  };
  auto p4 = [&p3](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p3(X) * (x + y + 1);
  };
  auto p5 = [&p4](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p4(X) * (-0.2 * x - 1.3 * y - 0.7);
  };
  auto p6 = [&p5](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p5(X) * (3 * x - 2 * y + 3);
  };
  auto p7 = [&p6](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p6(X) * (-2 * x + 4 * y - 7);
  };
  auto p8 = [&p7](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p7(X) * (0.2 * x - 0.3 * y - 0.3);
  };
  auto p9 = [&p8](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p8(X) * (0.9 * x + 0.6 * y - 0.4);
  };
  auto p10 = [&p9](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p9(X) * (-0.1 * x + 0.3 * y + 0.6);
  };
  auto p11 = [&p10](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p10(X) * (0.7 * x - 0.6 * y + 0.2);
  };
  auto p12 = [&p11](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p11(X) * (0.2 * x + 0.3 * y - 0.4);
  };
  auto p13 = [&p12](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p12(X) * (-0.9 * x + 0.4 * y + 0.3);
  };
  auto p14 = [&p13](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p13(X) * (0.7 * x - 0.8 * y - 0.9);
  };
  auto p15 = [&p14](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p14(X) * (-0.9 * x + 0.4 * y + 0.2);
  };
  auto p16 = [&p15](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p15(X) * (-1.3 * x - 1.2 * y - 0.9);
  };
  auto p17 = [&p16](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p16(X) * (0.7 * x + 0.6 * y - 0.2);
  };
  auto p18 = [&p17](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p17(X) * (-0.2 * x + 0.5 * y + 0.7);
  };
  auto p19 = [&p18](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p18(X) * (0.4 * x - 0.7 * y - 0.3);
  };
  auto p20 = [&p19](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p19(X) * (-0.1 * x + 0.8 * y - 0.7);
  };
  auto p21 = [&p20](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p20(X) * (0.5 * x - 0.2 * y + 0.3);
  };

  SECTION("degree 1")
  {
    const QuadratureFormula<2>& l1 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(1));

    REQUIRE(l1.numberOfPoints() == 1);

    REQUIRE(integrate(p0, l1) == Catch::Approx(4));
    REQUIRE(integrate(p1, l1) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l1) != Catch::Approx(-19. / 3));

    REQUIRE(get_order(p2, l1, -19. / 3) == Catch::Approx(2));
  }

  SECTION("degree 2")
  {
    const QuadratureFormula<2>& l2 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(2));

    REQUIRE(l2.numberOfPoints() == 3);

    REQUIRE(integrate(p0, l2) == Catch::Approx(4));
    REQUIRE(integrate(p1, l2) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l2) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l2) != Catch::Approx(146. / 5));

    // In a weird way, the formula achieves 4th order on this test
    REQUIRE(get_order(p3, l2, 146. / 5) == Catch::Approx(4));
  }

  SECTION("degree 3 and 4")
  {
    const QuadratureFormula<2>& l3 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(3));
    const QuadratureFormula<2>& l4 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(4));

    bool is_same = true;
    is_same &= (l3.numberOfPoints() == l4.numberOfPoints());
    for (size_t i = 0; i < l3.pointList().size(); ++i) {
      is_same &= (l3.point(i) == l3.point(i));
      is_same &= (l3.weight(i) == l3.weight(i));
    }

    REQUIRE(is_same);

    REQUIRE(l4.numberOfPoints() == 6);

    REQUIRE(integrate(p0, l4) == Catch::Approx(4));
    REQUIRE(integrate(p1, l4) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l4) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l4) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l4) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l4) != Catch::Approx(-17. / 10));

    // In a weird way, the formula achieves 6th order on this test
    REQUIRE(get_order(p5, l4, -17. / 10) == Catch::Approx(6));
  }

  SECTION("degree 5")
  {
    const QuadratureFormula<2>& l5 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(5));

    REQUIRE(l5.numberOfPoints() == 7);

    REQUIRE(integrate(p0, l5) == Catch::Approx(4));
    REQUIRE(integrate(p1, l5) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l5) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l5) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l5) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l5) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l5) != Catch::Approx(-197. / 175));

    REQUIRE(get_order(p6, l5, -197. / 175) == Catch::Approx(6));
  }

  SECTION("degree 6")
  {
    const QuadratureFormula<2>& l6 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(6));

    REQUIRE(l6.numberOfPoints() == 12);

    REQUIRE(integrate(p0, l6) == Catch::Approx(4));
    REQUIRE(integrate(p1, l6) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l6) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l6) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l6) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l6) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l6) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l6) != Catch::Approx(-4507. / 1575));

    REQUIRE(get_order(p7, l6, -4507. / 1575) == Catch::Approx(8));
  }

  SECTION("degree 7")
  {
    const QuadratureFormula<2>& l7 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(7));

    REQUIRE(l7.numberOfPoints() == 15);

    REQUIRE(integrate(p0, l7) == Catch::Approx(4));
    REQUIRE(integrate(p1, l7) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l7) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l7) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l7) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l7) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l7) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l7) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l7) != Catch::Approx(-23867. / 1575));

    REQUIRE(get_order(p8, l7, -23867. / 1575) == Catch::Approx(8));
  }

  SECTION("degree 8")
  {
    const QuadratureFormula<2>& l8 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(8));

    REQUIRE(l8.numberOfPoints() == 16);

    REQUIRE(integrate(p0, l8) == Catch::Approx(4));
    REQUIRE(integrate(p1, l8) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l8) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l8) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l8) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l8) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l8) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l8) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l8) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l8) != Catch::Approx(7782251. / 346500));

    // In a weird way, the formula achieves 10th order on this test
    REQUIRE(get_order(p9, l8, 7782251. / 346500) == Catch::Approx(10));
  }

  SECTION("degree 9")
  {
    const QuadratureFormula<2>& l9 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(9));

    REQUIRE(l9.numberOfPoints() == 19);

    REQUIRE(integrate(p0, l9) == Catch::Approx(4));
    REQUIRE(integrate(p1, l9) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l9) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l9) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l9) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l9) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l9) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l9) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l9) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l9) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l9) != Catch::Approx(126885809. / 14437500));

    REQUIRE(get_order(p10, l9, 126885809. / 14437500) == Catch::Approx(10));
  }

  SECTION("degree 10")
  {
    const QuadratureFormula<2>& l10 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(10));

    REQUIRE(l10.numberOfPoints() == 25);

    REQUIRE(integrate(p0, l10) == Catch::Approx(4));
    REQUIRE(integrate(p1, l10) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l10) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l10) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l10) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l10) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l10) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l10) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l10) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l10) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l10) == Catch::Approx(126885809. / 14437500));
    REQUIRE(integrate(p11, l10) != Catch::Approx(22133453663. / 11261250000));

    // In a weird way, the formula achieves 12th order on this test
    REQUIRE(get_order(p11, l10, 22133453663. / 11261250000) == Catch::Approx(12));
  }

  SECTION("degree 11")
  {
    const QuadratureFormula<2>& l11 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(11));

    REQUIRE(l11.numberOfPoints() == 28);

    REQUIRE(integrate(p0, l11) == Catch::Approx(4));
    REQUIRE(integrate(p1, l11) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l11) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l11) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l11) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l11) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l11) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l11) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l11) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l11) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l11) == Catch::Approx(126885809. / 14437500));
    REQUIRE(integrate(p11, l11) == Catch::Approx(22133453663. / 11261250000));
    REQUIRE(integrate(p12, l11) != Catch::Approx(-419453736959. / 262762500000));

    REQUIRE(get_order(p12, l11, -419453736959. / 262762500000) == Catch::Approx(12));
  }

  SECTION("degree 12")
  {
    const QuadratureFormula<2>& l12 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(12));

    REQUIRE(l12.numberOfPoints() == 33);

    REQUIRE(integrate(p0, l12) == Catch::Approx(4));
    REQUIRE(integrate(p1, l12) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l12) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l12) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l12) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l12) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l12) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l12) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l12) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l12) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l12) == Catch::Approx(126885809. / 14437500));
    REQUIRE(integrate(p11, l12) == Catch::Approx(22133453663. / 11261250000));
    REQUIRE(integrate(p12, l12) == Catch::Approx(-419453736959. / 262762500000));
    REQUIRE(integrate(p13, l12) != Catch::Approx(-3625092349117. / 7882875000000));

    // In a weird way, the formula achieves 14th order on this test
    REQUIRE(get_order(p13, l12, -3625092349117. / 7882875000000) == Catch::Approx(14));
  }

  SECTION("degree 13")
  {
    const QuadratureFormula<2>& l13 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(13));

    REQUIRE(l13.numberOfPoints() == 37);

    REQUIRE(integrate(p0, l13) == Catch::Approx(4));
    REQUIRE(integrate(p1, l13) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l13) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l13) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l13) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l13) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l13) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l13) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l13) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l13) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l13) == Catch::Approx(126885809. / 14437500));
    REQUIRE(integrate(p11, l13) == Catch::Approx(22133453663. / 11261250000));
    REQUIRE(integrate(p12, l13) == Catch::Approx(-419453736959. / 262762500000));
    REQUIRE(integrate(p13, l13) == Catch::Approx(-3625092349117. / 7882875000000));
    REQUIRE(integrate(p14, l13) != Catch::Approx(541632196607. / 1642265625000));

    REQUIRE(get_order(p14, l13, 541632196607. / 1642265625000) == Catch::Approx(14));
  }

  SECTION("degree 14")
  {
    const QuadratureFormula<2>& l14 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(14));

    REQUIRE(l14.numberOfPoints() == 42);

    REQUIRE(integrate(p0, l14) == Catch::Approx(4));
    REQUIRE(integrate(p1, l14) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l14) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l14) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l14) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l14) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l14) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l14) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l14) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l14) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l14) == Catch::Approx(126885809. / 14437500));
    REQUIRE(integrate(p11, l14) == Catch::Approx(22133453663. / 11261250000));
    REQUIRE(integrate(p12, l14) == Catch::Approx(-419453736959. / 262762500000));
    REQUIRE(integrate(p13, l14) == Catch::Approx(-3625092349117. / 7882875000000));
    REQUIRE(integrate(p14, l14) == Catch::Approx(541632196607. / 1642265625000));
    REQUIRE(integrate(p15, l14) != Catch::Approx(-287809833862769. / 3350221875000000));

    // In a weird way, the formula achieves 16th order on this test
    REQUIRE(get_order(p15, l14, -287809833862769. / 3350221875000000) == Catch::Approx(16));
  }

  SECTION("degree 15")
  {
    const QuadratureFormula<2>& l15 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(15));

    REQUIRE(l15.numberOfPoints() == 49);

    REQUIRE(integrate(p0, l15) == Catch::Approx(4));
    REQUIRE(integrate(p1, l15) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l15) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l15) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l15) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l15) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l15) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l15) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l15) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l15) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l15) == Catch::Approx(126885809. / 14437500));
    REQUIRE(integrate(p11, l15) == Catch::Approx(22133453663. / 11261250000));
    REQUIRE(integrate(p12, l15) == Catch::Approx(-419453736959. / 262762500000));
    REQUIRE(integrate(p13, l15) == Catch::Approx(-3625092349117. / 7882875000000));
    REQUIRE(integrate(p14, l15) == Catch::Approx(541632196607. / 1642265625000));
    REQUIRE(integrate(p15, l15) == Catch::Approx(-287809833862769. / 3350221875000000));
    REQUIRE(integrate(p16, l15) != Catch::Approx(3340311405172793. / 6700443750000000).epsilon(1E-10));

    REQUIRE(get_order(p16, l15, 3340311405172793. / 6700443750000000) == Catch::Approx(16));
  }

  SECTION("degree 16")
  {
    const QuadratureFormula<2>& l16 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(16));

    REQUIRE(l16.numberOfPoints() == 55);

    REQUIRE(integrate(p0, l16) == Catch::Approx(4));
    REQUIRE(integrate(p1, l16) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l16) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l16) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l16) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l16) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l16) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l16) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l16) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l16) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l16) == Catch::Approx(126885809. / 14437500));
    REQUIRE(integrate(p11, l16) == Catch::Approx(22133453663. / 11261250000));
    REQUIRE(integrate(p12, l16) == Catch::Approx(-419453736959. / 262762500000));
    REQUIRE(integrate(p13, l16) == Catch::Approx(-3625092349117. / 7882875000000));
    REQUIRE(integrate(p14, l16) == Catch::Approx(541632196607. / 1642265625000));
    REQUIRE(integrate(p15, l16) == Catch::Approx(-287809833862769. / 3350221875000000));
    REQUIRE(integrate(p16, l16) == Catch::Approx(3340311405172793. / 6700443750000000));
    REQUIRE(integrate(p17, l16) != Catch::Approx(-8386984372282772827. / 19096264687500000000.).epsilon(1E-10));

    // In a weird way, the formula achieves ~18th order on this test
    REQUIRE(get_order(p17, l16, -8386984372282772827. / 19096264687500000000.) == Catch::Approx(18).margin(1E-2));
  }

  SECTION("degree 17")
  {
    const QuadratureFormula<2>& l17 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(17));

    REQUIRE(l17.numberOfPoints() == 60);

    REQUIRE(integrate(p0, l17) == Catch::Approx(4));
    REQUIRE(integrate(p1, l17) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l17) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l17) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l17) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l17) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l17) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l17) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l17) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l17) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l17) == Catch::Approx(126885809. / 14437500));
    REQUIRE(integrate(p11, l17) == Catch::Approx(22133453663. / 11261250000));
    REQUIRE(integrate(p12, l17) == Catch::Approx(-419453736959. / 262762500000));
    REQUIRE(integrate(p13, l17) == Catch::Approx(-3625092349117. / 7882875000000));
    REQUIRE(integrate(p14, l17) == Catch::Approx(541632196607. / 1642265625000));
    REQUIRE(integrate(p15, l17) == Catch::Approx(-287809833862769. / 3350221875000000));
    REQUIRE(integrate(p16, l17) == Catch::Approx(3340311405172793. / 6700443750000000));
    REQUIRE(integrate(p17, l17) == Catch::Approx(-8386984372282772827. / 19096264687500000000.));
    REQUIRE(integrate(p18, l17) != Catch::Approx(-1599289493784003137. / 6820094531250000000.).epsilon(1E-10));

    REQUIRE(get_order(p18, l17, -1599289493784003137. / 6820094531250000000.) == Catch::Approx(18).margin(1E-2));
  }

  SECTION("degree 18")
  {
    const QuadratureFormula<2>& l18 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(18));

    REQUIRE(l18.numberOfPoints() == 67);

    REQUIRE(integrate(p0, l18) == Catch::Approx(4));
    REQUIRE(integrate(p1, l18) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l18) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l18) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l18) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l18) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l18) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l18) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l18) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l18) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l18) == Catch::Approx(126885809. / 14437500));
    REQUIRE(integrate(p11, l18) == Catch::Approx(22133453663. / 11261250000));
    REQUIRE(integrate(p12, l18) == Catch::Approx(-419453736959. / 262762500000));
    REQUIRE(integrate(p13, l18) == Catch::Approx(-3625092349117. / 7882875000000));
    REQUIRE(integrate(p14, l18) == Catch::Approx(541632196607. / 1642265625000));
    REQUIRE(integrate(p15, l18) == Catch::Approx(-287809833862769. / 3350221875000000));
    REQUIRE(integrate(p16, l18) == Catch::Approx(3340311405172793. / 6700443750000000));
    REQUIRE(integrate(p17, l18) == Catch::Approx(-8386984372282772827. / 19096264687500000000.));
    REQUIRE(integrate(p18, l18) == Catch::Approx(-1599289493784003137. / 6820094531250000000.));
    REQUIRE(integrate(p19, l18) != Catch::Approx(5280879341958226453. / 47740661718750000000.).epsilon(1E-10));

    // In a weird way, the formula achieves ~20th order on this test
    REQUIRE(get_order(p19, l18, 5280879341958226453. / 47740661718750000000.) == Catch::Approx(20).margin(0.1));
  }

  SECTION("degree 19")
  {
    const QuadratureFormula<2>& l19 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(19));

    REQUIRE(l19.numberOfPoints() == 73);

    REQUIRE(integrate(p0, l19) == Catch::Approx(4));
    REQUIRE(integrate(p1, l19) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l19) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l19) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l19) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l19) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l19) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l19) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l19) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l19) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l19) == Catch::Approx(126885809. / 14437500));
    REQUIRE(integrate(p11, l19) == Catch::Approx(22133453663. / 11261250000));
    REQUIRE(integrate(p12, l19) == Catch::Approx(-419453736959. / 262762500000));
    REQUIRE(integrate(p13, l19) == Catch::Approx(-3625092349117. / 7882875000000));
    REQUIRE(integrate(p14, l19) == Catch::Approx(541632196607. / 1642265625000));
    REQUIRE(integrate(p15, l19) == Catch::Approx(-287809833862769. / 3350221875000000));
    REQUIRE(integrate(p16, l19) == Catch::Approx(3340311405172793. / 6700443750000000));
    REQUIRE(integrate(p17, l19) == Catch::Approx(-8386984372282772827. / 19096264687500000000.));
    REQUIRE(integrate(p18, l19) == Catch::Approx(-1599289493784003137. / 6820094531250000000.));
    REQUIRE(integrate(p19, l19) == Catch::Approx(5280879341958226453. / 47740661718750000000.));
    REQUIRE(integrate(p20, l19) != Catch::Approx(1390615013183923183. / 85251181640625000000.).epsilon(1E-10));

    REQUIRE(get_order(p20, l19, 1390615013183923183. / 85251181640625000000.) == Catch::Approx(20).margin(0.1));
  }

  SECTION("degree 20")
  {
    const QuadratureFormula<2>& l20 = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(20));

    REQUIRE(l20.numberOfPoints() == 79);

    REQUIRE(integrate(p0, l20) == Catch::Approx(4));
    REQUIRE(integrate(p1, l20) == Catch::Approx(-4. / 3));
    REQUIRE(integrate(p2, l20) == Catch::Approx(-19. / 3));
    REQUIRE(integrate(p3, l20) == Catch::Approx(146. / 5));
    REQUIRE(integrate(p4, l20) == Catch::Approx(-25. / 6));
    REQUIRE(integrate(p5, l20) == Catch::Approx(-17. / 10));
    REQUIRE(integrate(p6, l20) == Catch::Approx(-197. / 175));
    REQUIRE(integrate(p7, l20) == Catch::Approx(-4507. / 1575));
    REQUIRE(integrate(p8, l20) == Catch::Approx(-23867. / 1575));
    REQUIRE(integrate(p9, l20) == Catch::Approx(7782251. / 346500));
    REQUIRE(integrate(p10, l20) == Catch::Approx(126885809. / 14437500));
    REQUIRE(integrate(p11, l20) == Catch::Approx(22133453663. / 11261250000));
    REQUIRE(integrate(p12, l20) == Catch::Approx(-419453736959. / 262762500000));
    REQUIRE(integrate(p13, l20) == Catch::Approx(-3625092349117. / 7882875000000));
    REQUIRE(integrate(p14, l20) == Catch::Approx(541632196607. / 1642265625000));
    REQUIRE(integrate(p15, l20) == Catch::Approx(-287809833862769. / 3350221875000000));
    REQUIRE(integrate(p16, l20) == Catch::Approx(3340311405172793. / 6700443750000000));
    REQUIRE(integrate(p17, l20) == Catch::Approx(-8386984372282772827. / 19096264687500000000.));
    REQUIRE(integrate(p18, l20) == Catch::Approx(-1599289493784003137. / 6820094531250000000.));
    REQUIRE(integrate(p19, l20) == Catch::Approx(5280879341958226453. / 47740661718750000000.));
    REQUIRE(integrate(p20, l20) == Catch::Approx(1390615013183923183. / 85251181640625000000.));
    REQUIRE(integrate(p21, l20) != Catch::Approx(-520205676970121316953. / 215685489550781250000000.).epsilon(1E-10));

    // In a weird way, the formula achieves ~22th order on this test
    REQUIRE(get_order(p21, l20, -520205676970121316953. / 215685489550781250000000.) == Catch::Approx(22).margin(0.5));
  }

  SECTION("max implemented degree")
  {
    REQUIRE(QuadratureManager::instance().maxTriangleDegree(QuadratureType::Gauss) ==
            TriangleGaussQuadrature::max_degree);
    REQUIRE_THROWS_WITH(QuadratureManager::instance().getTriangleFormula(
                          GaussQuadratureDescriptor(TriangleGaussQuadrature::max_degree + 1)),
                        "error: Gauss quadrature formulae handle degrees up to " +
                          stringify(TriangleGaussQuadrature::max_degree) + " on triangles");
  }
}
