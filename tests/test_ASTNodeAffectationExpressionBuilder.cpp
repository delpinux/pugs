#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeAffectationExpressionBuilder.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolInitializationChecker.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <language/utils/TypeDescriptor.hpp>
#include <utils/Demangle.hpp>
#include <utils/Exceptions.hpp>

#include <pegtl/string_input.hpp>

#define CHECK_AST(data, expected_output)                                                                        \
  {                                                                                                             \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);                              \
    static_assert(std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view> or                  \
                  std::is_same_v<std::decay_t<decltype(expected_output)>, std::string>);                        \
                                                                                                                \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                                  \
                                                                                                                \
    auto ast = ASTBuilder::build(input);                                                                        \
    ASTModulesImporter{*ast};                                                                                   \
    BasicAffectationRegisterFor<EmbeddedData>{ASTNodeDataType::build<ASTNodeDataType::type_id_t>("builtin_t")}; \
                                                                                                                \
    ASTSymbolTableBuilder{*ast};                                                                                \
    ASTNodeDataTypeBuilder{*ast};                                                                               \
                                                                                                                \
    ASTNodeDeclarationToAffectationConverter{*ast};                                                             \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                                        \
                                                                                                                \
    ASTNodeExpressionBuilder{*ast};                                                                             \
                                                                                                                \
    std::stringstream ast_output;                                                                               \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::exec_type}};             \
                                                                                                                \
    REQUIRE(ast_output.str() == expected_output);                                                               \
    ast->m_symbol_table->clearValues();                                                                         \
  }

template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const double>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("builtin_t");
const auto builtin_data_type = ast_node_data_type_from<std::shared_ptr<const double>>;

#define CHECK_AST_WITH_BUILTIN(data, expected_output)                                                           \
  {                                                                                                             \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);                              \
    static_assert(std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view> or                  \
                  std::is_same_v<std::decay_t<decltype(expected_output)>, std::string>);                        \
                                                                                                                \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                                  \
    auto ast = ASTBuilder::build(input);                                                                        \
    ASTModulesImporter{*ast};                                                                                   \
    BasicAffectationRegisterFor<EmbeddedData>{ASTNodeDataType::build<ASTNodeDataType::type_id_t>("builtin_t")}; \
                                                                                                                \
    SymbolTable& symbol_table = *ast->m_symbol_table;                                                           \
    auto [i_symbol, success]  = symbol_table.add(builtin_data_type.nameOfTypeId(), ast->begin());               \
    if (not success) {                                                                                          \
      throw UnexpectedError("cannot add '" + builtin_data_type.nameOfTypeId() + "' type for testing");          \
    }                                                                                                           \
                                                                                                                \
    i_symbol->attributes().setDataType(ASTNodeDataType::build<ASTNodeDataType::type_name_id_t>());              \
    i_symbol->attributes().setIsInitialized();                                                                  \
    i_symbol->attributes().value() = symbol_table.typeEmbedderTable().size();                                   \
    symbol_table.typeEmbedderTable().add(std::make_shared<TypeDescriptor>(builtin_data_type.nameOfTypeId()));   \
                                                                                                                \
    auto [i_symbol_a, success_a] = symbol_table.add("a", ast->begin());                                         \
    if (not success_a) {                                                                                        \
      throw UnexpectedError("cannot add 'a' of type builtin_t for testing");                                    \
    }                                                                                                           \
    i_symbol_a->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);               \
    i_symbol_a->attributes().setIsInitialized();                                                                \
    auto [i_symbol_b, success_b] = symbol_table.add("b", ast->begin());                                         \
    if (not success_b) {                                                                                        \
      throw UnexpectedError("cannot add 'b' of type builtin_t for testing");                                    \
    }                                                                                                           \
    i_symbol_b->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);               \
    i_symbol_b->attributes().setIsInitialized();                                                                \
                                                                                                                \
    ASTSymbolTableBuilder{*ast};                                                                                \
    ASTNodeDataTypeBuilder{*ast};                                                                               \
                                                                                                                \
    ASTNodeDeclarationToAffectationConverter{*ast};                                                             \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                                        \
                                                                                                                \
    ASTNodeExpressionBuilder{*ast};                                                                             \
                                                                                                                \
    std::stringstream ast_output;                                                                               \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::exec_type}};             \
                                                                                                                \
    REQUIRE(ast_output.str() == expected_output);                                                               \
                                                                                                                \
    ast->m_symbol_table->clearValues();                                                                         \
    OperatorRepository::instance().reset();                                                                     \
  }

#define CHECK_AST_THROWS_WITH(data, expected_error)                                           \
  {                                                                                           \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);            \
    static_assert(std::is_same_v<std::decay_t<decltype(expected_error)>, std::string_view> or \
                  std::is_same_v<std::decay_t<decltype(expected_error)>, std::string>);       \
                                                                                              \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                \
    auto ast = ASTBuilder::build(input);                                                      \
    OperatorRepository::instance().reset();                                                   \
    ASTModulesImporter{*ast};                                                                 \
                                                                                              \
    ASTSymbolTableBuilder{*ast};                                                              \
    ASTNodeDataTypeBuilder{*ast};                                                             \
                                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                      \
                                                                                              \
    REQUIRE_THROWS_WITH(ASTNodeExpressionBuilder{*ast}, expected_error);                      \
    ast->m_symbol_table->clearValues();                                                       \
  }

#define CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, expected_error)                                                \
  {                                                                                                             \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);                              \
    static_assert(std::is_same_v<std::decay_t<decltype(expected_error)>, std::string_view> or                   \
                  std::is_same_v<std::decay_t<decltype(expected_error)>, std::string>);                         \
                                                                                                                \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                                  \
    auto ast = ASTBuilder::build(input);                                                                        \
    OperatorRepository::instance().reset();                                                                     \
    ASTModulesImporter{*ast};                                                                                   \
                                                                                                                \
    BasicAffectationRegisterFor<EmbeddedData>{ASTNodeDataType::build<ASTNodeDataType::type_id_t>("builtin_t")}; \
                                                                                                                \
    SymbolTable& symbol_table = *ast->m_symbol_table;                                                           \
    auto [i_symbol, success]  = symbol_table.add(builtin_data_type.nameOfTypeId(), ast->begin());               \
    if (not success) {                                                                                          \
      throw UnexpectedError("cannot add '" + builtin_data_type.nameOfTypeId() + "' type for testing");          \
    }                                                                                                           \
                                                                                                                \
    i_symbol->attributes().setDataType(ASTNodeDataType::build<ASTNodeDataType::type_name_id_t>());              \
    i_symbol->attributes().setIsInitialized();                                                                  \
    i_symbol->attributes().value() = symbol_table.typeEmbedderTable().size();                                   \
    symbol_table.typeEmbedderTable().add(std::make_shared<TypeDescriptor>(builtin_data_type.nameOfTypeId()));   \
                                                                                                                \
    auto [i_symbol_a, success_a] = symbol_table.add("a", ast->begin());                                         \
    if (not success_a) {                                                                                        \
      throw UnexpectedError("cannot add 'a' of type builtin_t for testing");                                    \
    }                                                                                                           \
    i_symbol_a->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);               \
    i_symbol_a->attributes().setIsInitialized();                                                                \
    auto [i_symbol_b, success_b] = symbol_table.add("b", ast->begin());                                         \
    if (not success_b) {                                                                                        \
      throw UnexpectedError("cannot add 'b' of type builtin_t for testing");                                    \
    }                                                                                                           \
    i_symbol_b->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);               \
    i_symbol_b->attributes().setIsInitialized();                                                                \
                                                                                                                \
    ASTSymbolTableBuilder{*ast};                                                                                \
    ASTNodeDataTypeBuilder{*ast};                                                                               \
                                                                                                                \
    ASTNodeDeclarationToAffectationConverter{*ast};                                                             \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                                        \
                                                                                                                \
    REQUIRE_THROWS_WITH(ASTNodeExpressionBuilder{*ast}, expected_error);                                        \
                                                                                                                \
    ast->m_symbol_table->clearValues();                                                                         \
    OperatorRepository::instance().reset();                                                                     \
  }

#ifdef __clang__
#pragma clang optimize off
#else   // __clang__
#ifdef __GNUG__
#pragma GCC push_options
#pragma GCC optimize("O0")
#endif   // __GNUG__
#endif   // __clang__

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeAffectationExpressionBuilder", "[language]")
{
  const std::string demangled_stdstring = demangle(typeid(std::string{}).name());

  SECTION("Affectations")
  {
    SECTION("-> B")
    {
      SECTION("B -> B")
      {
        std::string_view data = R"(
let b:B, b=true;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, bool, bool>)
     +-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }
    }

    SECTION("-> N")
    {
      SECTION("B -> N")
      {
        std::string_view data = R"(
let n:N, n=true;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, bool>)
     +-(language::name:n:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("N -> N")
      {
        std::string_view data = R"(
let m : N; let n:N, n=m;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, unsigned long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:m:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Z -> N")
      {
        std::string_view data = R"(
let z:Z; let n :N, n=z;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:z:NameProcessor)
)";

        CHECK_AST(data, result);
      }
    }

    SECTION("-> Z")
    {
      SECTION("B -> Z")
      {
        std::string_view data = R"(
let z : Z, z=true;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, long, bool>)
     +-(language::name:z:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("N -> Z")
      {
        std::string_view data = R"(
let m : N; let z : Z, z=m;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, long, unsigned long>)
     +-(language::name:z:NameProcessor)
     `-(language::name:m:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Z -> Z")
      {
        std::string_view data = R"(
let q : Z; let z : Z, z=q;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, long, long>)
     +-(language::name:z:NameProcessor)
     `-(language::name:q:NameProcessor)
)";

        CHECK_AST(data, result);
      }
    }

    SECTION("-> R")
    {
      SECTION("B -> R")
      {
        std::string_view data = R"(
let r : R, r=true;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, double, bool>)
     +-(language::name:r:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("N -> R")
      {
        std::string_view data = R"(
let m : N; let r : R, r=m;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, double, unsigned long>)
     +-(language::name:r:NameProcessor)
     `-(language::name:m:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Z -> R")
      {
        std::string_view data = R"(
let z : Z; let r : R, r=z;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
     +-(language::name:r:NameProcessor)
     `-(language::name:z:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R -> R")
      {
        std::string_view data = R"(
let s : R; let r : R, r=s;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, double, double>)
     +-(language::name:r:NameProcessor)
     `-(language::name:s:NameProcessor)
)";

        CHECK_AST(data, result);
      }
    }

    SECTION("-> R^d")
    {
      SECTION("R^1 -> R^1")
      {
        std::string_view data = R"(
let x : R^1;
let y : R^1, y = x;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, TinyVector<1ul, double> >)
     +-(language::name:y:NameProcessor)
     `-(language::name:x:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R -> R^1")
      {
        std::string_view data = R"(
let x : R^1, x = 1.3;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, double>)
     +-(language::name:x:NameProcessor)
     `-(language::real:1.3:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Z -> R^1")
      {
        std::string_view data = R"(
let x : R^1, x = -1;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, long>)
     +-(language::name:x:NameProcessor)
     `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
         `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("N -> R^1")
      {
        std::string_view data = R"(
let n : N;
let x : R^1, x = n;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, unsigned long>)
     +-(language::name:x:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("B -> R^1")
      {
        std::string_view data = R"(
let b : B;
let x : R^1, x = b;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, bool>)
     +-(language::name:x:NameProcessor)
     `-(language::name:b:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("'0' -> R^1")
      {
        std::string_view data = R"(
let x : R^1, x = 0;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationFromZeroProcessor<TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::integer:0:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R^2 -> R^2 [variable]")
      {
        std::string_view data = R"(
let x : R^2;
let y : R^2, y = x;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
     +-(language::name:y:NameProcessor)
     `-(language::name:x:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R^2 -> R^2 [value]")
      {
        std::string_view data = R"(
let y : R^2, y = [0,1];
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
     +-(language::name:y:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
         +-(language::integer:0:ValueProcessor)
         `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("'0' -> R^2")
      {
        std::string_view data = R"(
let x : R^2, x = 0;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationFromZeroProcessor<TinyVector<2ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::integer:0:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R^3 -> R^3")
      {
        std::string_view data = R"(
let x : R^3;
let y : R^3, y = x;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
     +-(language::name:y:NameProcessor)
     `-(language::name:x:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("'0' -> R^3")
      {
        std::string_view data = R"(
let x : R^3, x = 0;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationFromZeroProcessor<TinyVector<3ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::integer:0:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R^3 -> R^3 [value]")
      {
        std::string_view data = R"(
let y : R^3, y = [1,2,3];
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
     +-(language::name:y:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         `-(language::integer:3:ValueProcessor)
)";

        CHECK_AST(data, result);
      }
    }

    SECTION("-> string")
    {
      SECTION("B -> string")
      {
        std::string_view data = R"(
let s : string, s=true;
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, )" +
                             demangled_stdstring + R"(, bool>)
     +-(language::name:s:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("N -> string")
      {
        std::string_view data = R"(
let n : N; let s : string, s=n;
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, )" +
                             demangled_stdstring + R"(, unsigned long>)
     +-(language::name:s:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Z -> string")
      {
        std::string_view data = R"(
let z : Z; let s : string, s=z;
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, )" +
                             demangled_stdstring +
                             R"(, long>)
     +-(language::name:s:NameProcessor)
     `-(language::name:z:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R -> string")
      {
        std::string_view data = R"(
let r : R; let s : string, s=r;
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, )" +
                             demangled_stdstring +
                             R"(, double>)
     +-(language::name:s:NameProcessor)
     `-(language::name:r:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("string -> string")
      {
        std::string_view data = R"(
let s : string, s="foo";
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, )" +
                             demangled_stdstring + ", " + demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

        CHECK_AST(data, result);
      }
    }

    SECTION("-> type_id")
    {
      SECTION("type_id -> type_id")
      {
        std::string_view data = R"(
let t : builtin_t, t=a;
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, EmbeddedData, EmbeddedData>)
     +-(language::name:t:NameProcessor)
     `-(language::name:a:NameProcessor)
)";

        CHECK_AST_WITH_BUILTIN(data, result);
      }
    }

    SECTION("list -> tuple")
    {
      SECTION("-> (B)")
      {
        std::string_view data = R"(
let t : (B), t = (true, false);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleFromListProcessor<bool>)
     +-(language::name:t:NameProcessor)
     `-(language::expression_list:ASTNodeExpressionListProcessor)
         +-(language::true_kw:ValueProcessor)
         `-(language::false_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("-> (N)")
      {
        std::string_view data = R"(
let t : (N), t = (1, 2, 3, 5);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleFromListProcessor<unsigned long>)
     +-(language::name:t:NameProcessor)
     `-(language::expression_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:5:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("-> (Z)")
      {
        std::string_view data = R"(
let n : N, n = 3;
let t : (Z), t = (2, n, true);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationToTupleFromListProcessor<long>)
     +-(language::name:t:NameProcessor)
     `-(language::expression_list:ASTNodeExpressionListProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::name:n:NameProcessor)
         `-(language::true_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("-> (R)")
      {
        std::string_view data = R"(
let n : N, n = 2;
let t : (R), t = (n, 3.1, 5, true);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::eq_op:AffectationToTupleFromListProcessor<double>)
     +-(language::name:t:NameProcessor)
     `-(language::expression_list:ASTNodeExpressionListProcessor)
         +-(language::name:n:NameProcessor)
         +-(language::real:3.1:ValueProcessor)
         +-(language::integer:5:ValueProcessor)
         `-(language::true_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("-> (R^d)")
      {
        std::string_view data = R"(
let a : R^2, a = [2,3.1];
let t1 : (R^2), t1 = (a, [1,2], 0);
let t2 : (R^3), t2 = (0, 0);
let t3 : (R^1), t3 = (1, 2.3, 0);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
 |   +-(language::name:a:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       +-(language::integer:2:ValueProcessor)
 |       `-(language::real:3.1:ValueProcessor)
 +-(language::eq_op:AffectationToTupleFromListProcessor<TinyVector<2ul, double> >)
 |   +-(language::name:t1:NameProcessor)
 |   `-(language::expression_list:ASTNodeExpressionListProcessor)
 |       +-(language::name:a:NameProcessor)
 |       +-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       |   +-(language::integer:1:ValueProcessor)
 |       |   `-(language::integer:2:ValueProcessor)
 |       `-(language::integer:0:ValueProcessor)
 +-(language::eq_op:AffectationToTupleFromListProcessor<TinyVector<3ul, double> >)
 |   +-(language::name:t2:NameProcessor)
 |   `-(language::expression_list:ASTNodeExpressionListProcessor)
 |       +-(language::integer:0:ValueProcessor)
 |       `-(language::integer:0:ValueProcessor)
 `-(language::eq_op:AffectationToTupleFromListProcessor<TinyVector<1ul, double> >)
     +-(language::name:t3:NameProcessor)
     `-(language::expression_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::real:2.3:ValueProcessor)
         `-(language::integer:0:ValueProcessor)
)";
        CHECK_AST(data, result);
      }

      SECTION("-> (R^dxd)")
      {
        std::string_view data = R"(
let a : R^2x2, a = [[2, 5],[2, 3.1]];
let t1 : (R^2x2), t1 = (a, [[1,2],[5,6]], 0);
let t2 : (R^3x3), t2 = (0, [[1,2,3],[4,5,6],[7,8,9]]);
let t3 : (R^1x1), t3 = (1, [[2.3]], 0);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyMatrix<2ul, 2ul, double>, TinyMatrix<2ul, 2ul, double> >)
 |   +-(language::name:a:NameProcessor)
 |   `-(language::matrix_expression:TinyMatrixExpressionProcessor<2ul, 2ul>)
 |       +-(language::row_expression:FakeProcessor)
 |       |   +-(language::integer:2:ValueProcessor)
 |       |   `-(language::integer:5:ValueProcessor)
 |       `-(language::row_expression:FakeProcessor)
 |           +-(language::integer:2:ValueProcessor)
 |           `-(language::real:3.1:ValueProcessor)
 +-(language::eq_op:AffectationToTupleFromListProcessor<TinyMatrix<2ul, 2ul, double> >)
 |   +-(language::name:t1:NameProcessor)
 |   `-(language::expression_list:ASTNodeExpressionListProcessor)
 |       +-(language::name:a:NameProcessor)
 |       +-(language::matrix_expression:TinyMatrixExpressionProcessor<2ul, 2ul>)
 |       |   +-(language::row_expression:FakeProcessor)
 |       |   |   +-(language::integer:1:ValueProcessor)
 |       |   |   `-(language::integer:2:ValueProcessor)
 |       |   `-(language::row_expression:FakeProcessor)
 |       |       +-(language::integer:5:ValueProcessor)
 |       |       `-(language::integer:6:ValueProcessor)
 |       `-(language::integer:0:ValueProcessor)
 +-(language::eq_op:AffectationToTupleFromListProcessor<TinyMatrix<3ul, 3ul, double> >)
 |   +-(language::name:t2:NameProcessor)
 |   `-(language::expression_list:ASTNodeExpressionListProcessor)
 |       +-(language::integer:0:ValueProcessor)
 |       `-(language::matrix_expression:TinyMatrixExpressionProcessor<3ul, 3ul>)
 |           +-(language::row_expression:FakeProcessor)
 |           |   +-(language::integer:1:ValueProcessor)
 |           |   +-(language::integer:2:ValueProcessor)
 |           |   `-(language::integer:3:ValueProcessor)
 |           +-(language::row_expression:FakeProcessor)
 |           |   +-(language::integer:4:ValueProcessor)
 |           |   +-(language::integer:5:ValueProcessor)
 |           |   `-(language::integer:6:ValueProcessor)
 |           `-(language::row_expression:FakeProcessor)
 |               +-(language::integer:7:ValueProcessor)
 |               +-(language::integer:8:ValueProcessor)
 |               `-(language::integer:9:ValueProcessor)
 `-(language::eq_op:AffectationToTupleFromListProcessor<TinyMatrix<1ul, 1ul, double> >)
     +-(language::name:t3:NameProcessor)
     `-(language::expression_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::matrix_expression:TinyMatrixExpressionProcessor<1ul, 1ul>)
         |   `-(language::row_expression:FakeProcessor)
         |       `-(language::real:2.3:ValueProcessor)
         `-(language::integer:0:ValueProcessor)
)";
        CHECK_AST(data, result);
      }

      SECTION("-> (string)")
      {
        std::string_view data = R"(
let n : N, n = 3;
let t : (string), t = ("foo", "bar", n, 1, 1.2,
                       [1], [1,2], [1,2,3],
                       [[2]], [[1,2],[3,4]], [[1,2,3],[4,5,6],[7,8,9]]);
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationToTupleFromListProcessor<)" +
                             demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::expression_list:ASTNodeExpressionListProcessor)
         +-(language::literal:"foo":ValueProcessor)
         +-(language::literal:"bar":ValueProcessor)
         +-(language::name:n:NameProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::real:1.2:ValueProcessor)
         +-(language::vector_expression:TinyVectorExpressionProcessor<1ul>)
         |   `-(language::integer:1:ValueProcessor)
         +-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
         |   +-(language::integer:1:ValueProcessor)
         |   `-(language::integer:2:ValueProcessor)
         +-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         |   +-(language::integer:1:ValueProcessor)
         |   +-(language::integer:2:ValueProcessor)
         |   `-(language::integer:3:ValueProcessor)
         +-(language::matrix_expression:TinyMatrixExpressionProcessor<1ul, 1ul>)
         |   `-(language::row_expression:FakeProcessor)
         |       `-(language::integer:2:ValueProcessor)
         +-(language::matrix_expression:TinyMatrixExpressionProcessor<2ul, 2ul>)
         |   +-(language::row_expression:FakeProcessor)
         |   |   +-(language::integer:1:ValueProcessor)
         |   |   `-(language::integer:2:ValueProcessor)
         |   `-(language::row_expression:FakeProcessor)
         |       +-(language::integer:3:ValueProcessor)
         |       `-(language::integer:4:ValueProcessor)
         `-(language::matrix_expression:TinyMatrixExpressionProcessor<3ul, 3ul>)
             +-(language::row_expression:FakeProcessor)
             |   +-(language::integer:1:ValueProcessor)
             |   +-(language::integer:2:ValueProcessor)
             |   `-(language::integer:3:ValueProcessor)
             +-(language::row_expression:FakeProcessor)
             |   +-(language::integer:4:ValueProcessor)
             |   +-(language::integer:5:ValueProcessor)
             |   `-(language::integer:6:ValueProcessor)
             `-(language::row_expression:FakeProcessor)
                 +-(language::integer:7:ValueProcessor)
                 +-(language::integer:8:ValueProcessor)
                 `-(language::integer:9:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("-> (builtin_t)")
      {
        std::string_view data = R"(
let t : (builtin_t), t= (a,b,a);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleFromListProcessor<EmbeddedData>)
     +-(language::name:t:NameProcessor)
     `-(language::expression_list:ASTNodeExpressionListProcessor)
         +-(language::name:a:NameProcessor)
         +-(language::name:b:NameProcessor)
         `-(language::name:a:NameProcessor)
)";

        CHECK_AST_WITH_BUILTIN(data, result);
      }
    }

    SECTION("value -> tuple")
    {
      SECTION(" -> (B)")
      {
        SECTION("B -> (B)")
        {
          std::string_view data = R"(
let t : (B), t = true;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<bool>)
     +-(language::name:t:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> (N)")
      {
        SECTION("B -> (N)")
        {
          std::string_view data = R"(
let t : (N), t = true;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<unsigned long>)
     +-(language::name:t:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("N -> (N)")
        {
          std::string_view data = R"(
let n : N, n = 2;
let t : (N), t = n;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<unsigned long>)
     +-(language::name:t:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z -> (N)")
        {
          std::string_view data = R"(
let t : (N), t = 1;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<unsigned long>)
     +-(language::name:t:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> (Z)")
      {
        SECTION("B -> (Z)")
        {
          std::string_view data = R"(
let t : (Z), t = true;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<long>)
     +-(language::name:t:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("N -> (Z)")
        {
          std::string_view data = R"(
let n : N, n = 3;
let t : (Z), t = n;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<long>)
     +-(language::name:t:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z -> (Z)")
        {
          std::string_view data = R"(
let t : (Z), t = -2;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<long>)
     +-(language::name:t:NameProcessor)
     `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
         `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> (R)")
      {
        SECTION("B -> (R)")
        {
          std::string_view data = R"(
let t : (R), t = true;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<double>)
     +-(language::name:t:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("N -> (R)")
        {
          std::string_view data = R"(
let n : N, n = 2;
let t : (R), t = n;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<double>)
     +-(language::name:t:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z -> (R)")
        {
          std::string_view data = R"(
let t : (R), t = 3;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<double>)
     +-(language::name:t:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R -> (R)")
        {
          std::string_view data = R"(
let t : (R), t = 3.1;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<double>)
     +-(language::name:t:NameProcessor)
     `-(language::real:3.1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> R^d")
      {
        SECTION("B -> (R^1)")
        {
          std::string_view data = R"(
let t : (R^1), t = false;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<TinyVector<1ul, double> >)
     +-(language::name:t:NameProcessor)
     `-(language::false_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("N -> (R^1)")
        {
          std::string_view data = R"(
let n : N, n = 3;
let t : (R^1), t = n;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<TinyVector<1ul, double> >)
     +-(language::name:t:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z -> (R^1)")
        {
          std::string_view data = R"(
let t : (R^1), t = 3;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<TinyVector<1ul, double> >)
     +-(language::name:t:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R -> (R^1)")
        {
          std::string_view data = R"(
let t : (R^1), t = 3.3;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<TinyVector<1ul, double> >)
     +-(language::name:t:NameProcessor)
     `-(language::real:3.3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R^d -> (R^d)")
        {
          std::string_view data = R"(
let a : R^2, a = [2,3.1];
let t1 : (R^2), t1 = a;
let t2 : (R^3), t2 = 0;
let t3 : (R^1), t3 = 2.3;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
 |   +-(language::name:a:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       +-(language::integer:2:ValueProcessor)
 |       `-(language::real:3.1:ValueProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyVector<2ul, double> >)
 |   +-(language::name:t1:NameProcessor)
 |   `-(language::name:a:NameProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyVector<3ul, double> >)
 |   +-(language::name:t2:NameProcessor)
 |   `-(language::integer:0:ValueProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<TinyVector<1ul, double> >)
     +-(language::name:t3:NameProcessor)
     `-(language::real:2.3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> R^dxd")
      {
        SECTION("B -> (R^1x1)")
        {
          std::string_view data = R"(
let t : (R^1x1), t = false;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
     +-(language::name:t:NameProcessor)
     `-(language::false_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("N -> (R^1x1)")
        {
          std::string_view data = R"(
let n : N, n = 3;
let t : (R^1x1), t = n;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
     +-(language::name:t:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z -> (R^1x1)")
        {
          std::string_view data = R"(
let t : (R^1x1), t = 3;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
     +-(language::name:t:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R -> (R^1x1)")
        {
          std::string_view data = R"(
let t : (R^1x1), t = 3.3;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
     +-(language::name:t:NameProcessor)
     `-(language::real:3.3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R^dxd -> (R^dxd)")
        {
          std::string_view data = R"(
let a : R^2x2, a = [[2,3.1],[2,3.2]];
let t1 : (R^2x2), t1 = a;
let t2 : (R^3x3), t2 = 0;
let t3 : (R^1x1), t3 = 2.3;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyMatrix<2ul, 2ul, double>, TinyMatrix<2ul, 2ul, double> >)
 |   +-(language::name:a:NameProcessor)
 |   `-(language::matrix_expression:TinyMatrixExpressionProcessor<2ul, 2ul>)
 |       +-(language::row_expression:FakeProcessor)
 |       |   +-(language::integer:2:ValueProcessor)
 |       |   `-(language::real:3.1:ValueProcessor)
 |       `-(language::row_expression:FakeProcessor)
 |           +-(language::integer:2:ValueProcessor)
 |           `-(language::real:3.2:ValueProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<2ul, 2ul, double> >)
 |   +-(language::name:t1:NameProcessor)
 |   `-(language::name:a:NameProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<3ul, 3ul, double> >)
 |   +-(language::name:t2:NameProcessor)
 |   `-(language::integer:0:ValueProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
     +-(language::name:t3:NameProcessor)
     `-(language::real:2.3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> (string)")
      {
        SECTION("B -> (string)")
        {
          std::string_view data = R"(
let t : (string), t = true;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("N -> (string)")
        {
          std::string_view data = R"(
let n : N, n = 2;
let t : (string), t = true;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z -> (string)")
        {
          std::string_view data = R"(
let t : (string), t = -2;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
         `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R -> (string)")
        {
          std::string_view data = R"(
let t : (string), t = 3.2;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::real:3.2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R^1 -> (string)")
        {
          std::string_view data = R"(
let t : (string), t = [1.5];
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<1ul>)
         `-(language::real:1.5:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R^2 -> (string)")
        {
          std::string_view data = R"(
let t : (string), t = [1.5, 2];
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
         +-(language::real:1.5:ValueProcessor)
         `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R^3 -> (string)")
        {
          std::string_view data = R"(
let t : (string), t = [1.5, 2, 1];
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         +-(language::real:1.5:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R^1x1 -> (string)")
        {
          std::string_view data = R"(
let t : (string), t = [[1.5]];
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::matrix_expression:TinyMatrixExpressionProcessor<1ul, 1ul>)
         `-(language::row_expression:FakeProcessor)
             `-(language::real:1.5:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R^2x2 -> (string)")
        {
          std::string_view data = R"(
let t : (string), t = [[1.5, 1],[-2, 3]];
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::matrix_expression:TinyMatrixExpressionProcessor<2ul, 2ul>)
         +-(language::row_expression:FakeProcessor)
         |   +-(language::real:1.5:ValueProcessor)
         |   `-(language::integer:1:ValueProcessor)
         `-(language::row_expression:FakeProcessor)
             +-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
             |   `-(language::integer:2:ValueProcessor)
             `-(language::integer:3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R^3x3 -> (string)")
        {
          std::string_view data = R"(
let t : (string), t = [[1.5, 1, -1],[-2, 3, 1],[-5, 1, 6]];
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::matrix_expression:TinyMatrixExpressionProcessor<3ul, 3ul>)
         +-(language::row_expression:FakeProcessor)
         |   +-(language::real:1.5:ValueProcessor)
         |   +-(language::integer:1:ValueProcessor)
         |   `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
         |       `-(language::integer:1:ValueProcessor)
         +-(language::row_expression:FakeProcessor)
         |   +-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
         |   |   `-(language::integer:2:ValueProcessor)
         |   +-(language::integer:3:ValueProcessor)
         |   `-(language::integer:1:ValueProcessor)
         `-(language::row_expression:FakeProcessor)
             +-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
             |   `-(language::integer:5:ValueProcessor)
             +-(language::integer:1:ValueProcessor)
             `-(language::integer:6:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("string -> (string)")
        {
          std::string_view data = R"(
let t : (string), t = "foo";
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:t:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> (type_id)")
      {
        std::string_view data = R"(
let t : (builtin_t), t = a;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationToTupleProcessor<EmbeddedData>)
     +-(language::name:t:NameProcessor)
     `-(language::name:a:NameProcessor)
)";

        CHECK_AST_WITH_BUILTIN(data, result);
      }
    }

    SECTION("tuple -> value")
    {
      SECTION(" -> B")
      {
        SECTION("(B) -> B")
        {
          std::string_view data = R"(
let t : (B), t = true;
let b : B, b = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<bool>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<bool>)
     +-(language::name:b:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> N")
      {
        SECTION("(B) -> N")
        {
          std::string_view data = R"(
let t : (B), t = true;
let n : N, n = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<bool>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<unsigned long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(N) -> N")
        {
          std::string_view data = R"(
let t : (N), t = 1;
let n : N, n = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<unsigned long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<unsigned long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(Z) -> N")
        {
          std::string_view data = R"(
let t : (Z), t = 1;
let n : N, n = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<unsigned long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> Z")
      {
        SECTION("(B) -> Z")
        {
          std::string_view data = R"(
let t : (B), t = true;
let z : Z, z = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<bool>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<long>)
     +-(language::name:z:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(N) -> Z")
        {
          std::string_view data = R"(
let t : (N), t = 2;
let z : Z, z = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<unsigned long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<long>)
     +-(language::name:z:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(Z) -> Z")
        {
          std::string_view data = R"(
let t : (Z), t = -2;
let z : Z, z = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
 |       `-(language::integer:2:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<long>)
     +-(language::name:z:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> R")
      {
        SECTION("(B) -> R")
        {
          std::string_view data = R"(
let t : (B), t = true;
let x: R, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<bool>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<double>)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(N) -> R")
        {
          std::string_view data = R"(
let t : (N), t = 2;
let x : R, x = 2;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<unsigned long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
     +-(language::name:x:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(Z) -> R")
        {
          std::string_view data = R"(
let t : (Z), t = 3;
let x : R, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<double>)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R -> (R)")
        {
          std::string_view data = R"(
let t : (R), t = 3.1;
let x : R, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<double>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::real:3.1:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<double>)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> R^d")
      {
        SECTION("(B) -> R^1")
        {
          std::string_view data = R"(
let t : (B), t = false;
let x : R^1, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<bool>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::false_kw:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(N) -> R^1")
        {
          std::string_view data = R"(
let t : (N), t = 3;
let x : R^1, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<unsigned long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(Z) -> R^1")
        {
          std::string_view data = R"(
let t : (Z), t = 3;
let x : R^1, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R) -> R^1")
        {
          std::string_view data = R"(
let t : (R), t = 3.3;
let x : R^1, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<double>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::real:3.3:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^1) -> R^1")
        {
          std::string_view data = R"(
let t : (R^1), t = 2.3;
let x : R^1, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyVector<1ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::real:2.3:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^2) -> R^2")
        {
          std::string_view data = R"(
let t : (R^2), t = [2.3, 1];
let x : R^2, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyVector<2ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       +-(language::real:2.3:ValueProcessor)
 |       `-(language::integer:1:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyVector<2ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^3) -> R^3")
        {
          std::string_view data = R"(
let t : (R^3), t = [2.3, 1, 1];
let x : R^3, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyVector<3ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
 |       +-(language::real:2.3:ValueProcessor)
 |       +-(language::integer:1:ValueProcessor)
 |       `-(language::integer:1:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyVector<3ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> R^dxd")
      {
        SECTION("(B) -> R^1x1")
        {
          std::string_view data = R"(
let t : (B), t = false;
let x : R^1x1, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<bool>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::false_kw:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(N) -> R^1x1")
        {
          std::string_view data = R"(
let t : (N), t = 3;
let x : R^1x1, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<unsigned long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(Z) -> R^1x1")
        {
          std::string_view data = R"(
let t : (Z), t = 3;
let x : R^1x1, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R) -> R^1x1")
        {
          std::string_view data = R"(
let t : (R), t = 3.3;
let x : R^1x1, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<double>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::real:3.3:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^1x1) -> R^1x1")
        {
          std::string_view data = R"(
let t : (R^1x1), t = 2.3;
let x : R^1x1, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::real:2.3:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^2x2) -> R^2x2")
        {
          std::string_view data = R"(
let t : (R^2x2), t = [[2.3, 1], [6,5]];
let x : R^2x2, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<2ul, 2ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::matrix_expression:TinyMatrixExpressionProcessor<2ul, 2ul>)
 |       +-(language::row_expression:FakeProcessor)
 |       |   +-(language::real:2.3:ValueProcessor)
 |       |   `-(language::integer:1:ValueProcessor)
 |       `-(language::row_expression:FakeProcessor)
 |           +-(language::integer:6:ValueProcessor)
 |           `-(language::integer:5:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyMatrix<2ul, 2ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^3x3) -> R^3x3")
        {
          std::string_view data = R"(
let t : (R^3x3), t = [[2.3, 1, 1],[1, 2, 3], [2,1,2]];
let x : R^3x3, x = t;
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<3ul, 3ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::matrix_expression:TinyMatrixExpressionProcessor<3ul, 3ul>)
 |       +-(language::row_expression:FakeProcessor)
 |       |   +-(language::real:2.3:ValueProcessor)
 |       |   +-(language::integer:1:ValueProcessor)
 |       |   `-(language::integer:1:ValueProcessor)
 |       +-(language::row_expression:FakeProcessor)
 |       |   +-(language::integer:1:ValueProcessor)
 |       |   +-(language::integer:2:ValueProcessor)
 |       |   `-(language::integer:3:ValueProcessor)
 |       `-(language::row_expression:FakeProcessor)
 |           +-(language::integer:2:ValueProcessor)
 |           +-(language::integer:1:ValueProcessor)
 |           `-(language::integer:2:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<TinyMatrix<3ul, 3ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> string")
      {
        SECTION("(B) -> string")
        {
          std::string_view data = R"(
let t : (B), t = true;
let s : string, s = t;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<bool>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(N) -> string")
        {
          std::string_view data = R"(
let t : (N), t = true;
let s : string, s = t;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<unsigned long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(Z) -> string")
        {
          std::string_view data = R"(
let t : (Z), t = -2;
let s : string, s = t;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<long>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
 |       `-(language::integer:2:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R) -> string")
        {
          std::string_view data = R"(
let t : (R), t = 3.2;
let s : string, s = t;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<double>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::real:3.2:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^1) -> string")
        {
          std::string_view data = R"(
let t : (R^1), t = [1.5];
let s : string, s = t;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyVector<1ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<1ul>)
 |       `-(language::real:1.5:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^2) -> string")
        {
          std::string_view data = R"(
let t : (R^2), t = [1.5, 2];
let s : string, s = t;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyVector<2ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       +-(language::real:1.5:ValueProcessor)
 |       `-(language::integer:2:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^3) -> string")
        {
          std::string_view data = R"(
let t : (R^3), t = [1.5, 2, 1];
let s : string, s = t;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyVector<3ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
 |       +-(language::real:1.5:ValueProcessor)
 |       +-(language::integer:2:ValueProcessor)
 |       `-(language::integer:1:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^1x1) -> string")
        {
          std::string_view data = R"(
let t : (R^1x1), t = [[1.5]];
let s : string, s = t;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<1ul, 1ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::matrix_expression:TinyMatrixExpressionProcessor<1ul, 1ul>)
 |       `-(language::row_expression:FakeProcessor)
 |           `-(language::real:1.5:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^2x2) -> string")
        {
          std::string_view data = R"(
let t : (R^2x2), t = [[1.5, 1],[-2, 3]];
let s : string, s = t;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<2ul, 2ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::matrix_expression:TinyMatrixExpressionProcessor<2ul, 2ul>)
 |       +-(language::row_expression:FakeProcessor)
 |       |   +-(language::real:1.5:ValueProcessor)
 |       |   `-(language::integer:1:ValueProcessor)
 |       `-(language::row_expression:FakeProcessor)
 |           +-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
 |           |   `-(language::integer:2:ValueProcessor)
 |           `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("(R^3x3) -> string")
        {
          std::string_view data = R"(
let t : (R^3x3), t = [[1.5, 1, -1],[-2, 3, 1],[-5, 1, 6]];
let s : string, s = t;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<TinyMatrix<3ul, 3ul, double> >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::matrix_expression:TinyMatrixExpressionProcessor<3ul, 3ul>)
 |       +-(language::row_expression:FakeProcessor)
 |       |   +-(language::real:1.5:ValueProcessor)
 |       |   +-(language::integer:1:ValueProcessor)
 |       |   `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
 |       |       `-(language::integer:1:ValueProcessor)
 |       +-(language::row_expression:FakeProcessor)
 |       |   +-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
 |       |   |   `-(language::integer:2:ValueProcessor)
 |       |   +-(language::integer:3:ValueProcessor)
 |       |   `-(language::integer:1:ValueProcessor)
 |       `-(language::row_expression:FakeProcessor)
 |           +-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
 |           |   `-(language::integer:5:ValueProcessor)
 |           +-(language::integer:1:ValueProcessor)
 |           `-(language::integer:6:ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("string -> (string)")
        {
          std::string_view data = R"(
let t : (string), t = "foo";
let s : string, s = t;
)";

          std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<)" +
                               demangled_stdstring + R"( >)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::literal:"foo":ValueProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<)" +
                               demangled_stdstring + R"( >)
     +-(language::name:s:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("-> type_id")
      {
        std::string_view data = R"(
let t : (builtin_t), t = a;
let bt: builtin_t, bt = t;
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationToTupleProcessor<EmbeddedData>)
 |   +-(language::name:t:NameProcessor)
 |   `-(language::name:a:NameProcessor)
 `-(language::eq_op:AffectationFromTupleProcessor<EmbeddedData>)
     +-(language::name:bt:NameProcessor)
     `-(language::name:t:NameProcessor)
)";

        CHECK_AST_WITH_BUILTIN(data, result);
      }
    }
  }

  SECTION("+=")
  {
    SECTION("N += N")
    {
      std::string_view data = R"(
let n : N, n=1; n+=n;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::pluseq_op:AffectationProcessor<language::pluseq_op, unsigned long, unsigned long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R += N")
    {
      std::string_view data = R"(
let x : R, x=1; x+=2;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::pluseq_op:AffectationProcessor<language::pluseq_op, double, long>)
     +-(language::name:x:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string += N")
    {
      std::string_view data = R"(
let s : string, s="foo"; s+=2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, )" +
                           demangled_stdstring + ", " + demangled_stdstring + R"( >)
 |   +-(language::name:s:NameProcessor)
 |   `-(language::literal:"foo":ValueProcessor)
 `-(language::pluseq_op:AffectationProcessor<language::pluseq_op, )" +
                           demangled_stdstring +
                           R"(, long>)
     +-(language::name:s:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1 += R^1")
    {
      std::string_view data = R"(
let x : R^1;
let y : R^1;
x += y;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::pluseq_op:AffectationProcessor<language::pluseq_op, TinyVector<1ul, double>, TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2 += R^2")
    {
      std::string_view data = R"(
let x : R^2;
let y : R^2;
x += y;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::pluseq_op:AffectationProcessor<language::pluseq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3 += R^3")
    {
      std::string_view data = R"(
let x : R^3;
let y : R^3;
x += y;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::pluseq_op:AffectationProcessor<language::pluseq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("-=")
  {
    SECTION("Z -= Z")
    {
      std::string_view data = R"(
let z : Z, z=1; z-=2;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, long, long>)
 |   +-(language::name:z:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::minuseq_op:AffectationProcessor<language::minuseq_op, long, long>)
     +-(language::name:z:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R -= R")
    {
      std::string_view data = R"(
let x : R, x=1; x-=2.3;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::minuseq_op:AffectationProcessor<language::minuseq_op, double, double>)
     +-(language::name:x:NameProcessor)
     `-(language::real:2.3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1 -= R^1")
    {
      std::string_view data = R"(
let x : R^1;
let y : R^1;
x -= y;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::minuseq_op:AffectationProcessor<language::minuseq_op, TinyVector<1ul, double>, TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2 -= R^2")
    {
      std::string_view data = R"(
let x : R^2;
let y : R^2;
x -= y;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::minuseq_op:AffectationProcessor<language::minuseq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3 -= R^3")
    {
      std::string_view data = R"(
let x : R^3;
let y : R^3;
x -= y;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::minuseq_op:AffectationProcessor<language::minuseq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("*=")
  {
    SECTION("Z *= Z")
    {
      std::string_view data = R"(
let z : Z, z=1; z*=2;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, long, long>)
 |   +-(language::name:z:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, long, long>)
     +-(language::name:z:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R *= R")
    {
      std::string_view data = R"(
let x : R, x=1; x*=2.3;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, double, double>)
     +-(language::name:x:NameProcessor)
     `-(language::real:2.3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1 *= R")
    {
      std::string_view data = R"(
let x : R^1; x*=2.3;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<1ul, double>, double>)
     +-(language::name:x:NameProcessor)
     `-(language::real:2.3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2 *= R")
    {
      std::string_view data = R"(
let x : R^2; x*= 6.2;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<2ul, double>, double>)
     +-(language::name:x:NameProcessor)
     `-(language::real:6.2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3 *= R")
    {
      std::string_view data = R"(
let x : R^3; x*= 3.1;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<3ul, double>, double>)
     +-(language::name:x:NameProcessor)
     `-(language::real:3.1:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R *= Z")
    {
      std::string_view data = R"(
let x : R, x=1; x*=2;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, double, long>)
     +-(language::name:x:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1 *= Z")
    {
      std::string_view data = R"(
let x : R^1; x *= 3;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<1ul, double>, long>)
     +-(language::name:x:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2 *= Z")
    {
      std::string_view data = R"(
let x : R^2; x *= 6;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<2ul, double>, long>)
     +-(language::name:x:NameProcessor)
     `-(language::integer:6:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3 *= Z")
    {
      std::string_view data = R"(
let x : R^3; x *= 4;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<3ul, double>, long>)
     +-(language::name:x:NameProcessor)
     `-(language::integer:4:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R *= N")
    {
      std::string_view data = R"(
let n : N, n=2; let x : R, x=1; x *= n;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, double, unsigned long>)
     +-(language::name:x:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1 *= N")
    {
      std::string_view data = R"(
let n : N;
let x : R^1; x *= n;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<1ul, double>, unsigned long>)
     +-(language::name:x:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2 *= N")
    {
      std::string_view data = R"(
let n : N;
let x : R^2; x *= n;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<2ul, double>, unsigned long>)
     +-(language::name:x:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3 *= N")
    {
      std::string_view data = R"(
let n : N;
let x : R^3; x *= n;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<3ul, double>, unsigned long>)
     +-(language::name:x:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R *= B")
    {
      std::string_view data = R"(
let x : R, x=1; x *= true;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, double, bool>)
     +-(language::name:x:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1 *= B")
    {
      std::string_view data = R"(
let x : R^1; x *= true;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<1ul, double>, bool>)
     +-(language::name:x:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2 *= B")
    {
      std::string_view data = R"(
let x : R^2; x *= false;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<2ul, double>, bool>)
     +-(language::name:x:NameProcessor)
     `-(language::false_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3 *= B")
    {
      std::string_view data = R"(
let b : B; let x : R^3; x *= b;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, TinyVector<3ul, double>, bool>)
     +-(language::name:x:NameProcessor)
     `-(language::name:b:NameProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("/=")
  {
    SECTION("Z /= Z")
    {
      std::string_view data = R"(
let z : Z, z=6; z/=2;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, long, long>)
 |   +-(language::name:z:NameProcessor)
 |   `-(language::integer:6:ValueProcessor)
 `-(language::divideeq_op:AffectationProcessor<language::divideeq_op, long, long>)
     +-(language::name:z:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R /= R")
    {
      std::string_view data = R"(
let x : R, x=1; x/=2.3;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::divideeq_op:AffectationProcessor<language::divideeq_op, double, double>)
     +-(language::name:x:NameProcessor)
     `-(language::real:2.3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("Errors")
  {
    SECTION("Invalid affectation operator")
    {
      auto ast         = std::make_unique<ASTNode>();
      ast->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
      {
        auto child_0         = std::make_unique<ASTNode>();
        child_0->m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
        auto child_1         = std::make_unique<ASTNode>();
        child_1->m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
        ast->children.emplace_back(std::move(child_0));
        ast->children.emplace_back(std::move(child_1));
      }
      REQUIRE_THROWS_WITH(ASTNodeAffectationExpressionBuilder{*ast},
                          "unexpected error: undefined affectation operator");
    }

    SECTION("Invalid string rhs")
    {
      auto ast = std::make_unique<ASTNode>();
      ast->set_type<language::eq_op>();

      ast->children.emplace_back(std::make_unique<ASTNode>());
      ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
      ast->children.emplace_back(std::make_unique<ASTNode>());
      REQUIRE_THROWS_WITH(ASTNodeAffectationExpressionBuilder{*ast}, "undefined affectation type: string = undefined");
    }

    SECTION("Invalid string affectation operator")
    {
      SECTION("string -= string")
      {
        std::string_view data = R"(
let s : string, s="foo"; s-="bar";
)";

        std::string error_message = "undefined affectation type: string -= string";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("string *= Z")
      {
        std::string_view data = R"(
let s : string, s="foo"; s*=2;
)";

        std::string error_message = "undefined affectation type: string *= Z";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("string /= string")
      {
        std::string_view data = R"(
 let s : string, s="foo"; s/="bar";
)";

        std::string error_message = "undefined affectation type: string /= string";

        CHECK_AST_THROWS_WITH(data, error_message);
      }
    }

    SECTION("type_id operator")
    {
      std::string_view data = R"(
 let s :builtin_t, s = a; s *= b;
)";

      std::string error_message = "undefined affectation type: builtin_t *= builtin_t";

      CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
    }

    SECTION("Invalid tuple operator")
    {
      std::string_view data = R"(
 let s :(R), s=(1,2,3); s *= 4;
)";

      std::string error_message = "undefined affectation type: (R) *= Z";

      CHECK_AST_THROWS_WITH(data, error_message);
    }

    SECTION("Invalid tuple operator 2")
    {
      std::string_view data = R"(
 let s : (builtin_t), s =(a,b); s *= b;
)";

      std::string error_message = "undefined affectation type: (builtin_t) *= builtin_t";

      CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
    }

    SECTION("Invalid R^n -> R^m affectation")
    {
      SECTION("R^3 <- R^1")
      {
        std::string_view data = R"(
let x : R^3; let y : R^1; x = y;
)";

        std::string error_message = "undefined affectation type: R^3 = R^1";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("R^3 <- R^2")
      {
        std::string_view data = R"(
let x : R^3; let y : R^2; x = y;
)";

        std::string error_message = "undefined affectation type: R^3 = R^2";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("R^2 <- R^1")
      {
        std::string_view data = R"(
let x : R^2; let y : R^1; x = y;
)";

        std::string error_message = "undefined affectation type: R^2 = R^1";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("R^2 <- R^3")
      {
        std::string_view data = R"(
let x : R^2; let y : R^3; x = y;
)";

        std::string error_message = "undefined affectation type: R^2 = R^3";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("R^1 <- R^2")
      {
        std::string_view data = R"(
let x : R^1; let y : R^2; x = y;
)";

        std::string error_message = "undefined affectation type: R^1 = R^2";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("R^1 <- R^3")
      {
        std::string_view data = R"(
let x : R^1; let y : R^3; x = y;
)";

        std::string error_message = "undefined affectation type: R^1 = R^3";

        CHECK_AST_THROWS_WITH(data, error_message);
      }
    }

    SECTION("Invalid Z -> R^m affectation [non-zero]")
    {
      SECTION("R^3 <- Z")
      {
        std::string_view data = R"(
let x : R^3, x = 3;
)";

        std::string error_message = "invalid integral value (0 is the solely valid value)";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("R^2 <- Z")
      {
        std::string_view data = R"(
let x : R^2, x = 2;
)";

        std::string error_message = "invalid integral value (0 is the solely valid value)";

        CHECK_AST_THROWS_WITH(data, error_message);
      }
    }

    SECTION("Invalid R^d -> R^d affectation operator")
    {
      SECTION("R^3 <- R^3")
      {
        std::string_view data = R"(
let x : R^3; let y : R^3; x /= y;
)";

        std::string error_message = "undefined affectation type: R^3 /= R^3";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("R^2 <- R^2")
      {
        std::string_view data = R"(
let x : R^2; let y : R^2; x /= y;
)";

        std::string error_message = "undefined affectation type: R^2 /= R^2";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("R^1 <- R^1")
      {
        std::string_view data = R"(
let x : R^1; let y : R^1; x /= y;
)";

        std::string error_message = "undefined affectation type: R^1 /= R^1";

        CHECK_AST_THROWS_WITH(data, error_message);
      }
    }

    SECTION("Invalid R^d -> R^d *= operand")
    {
      SECTION("R^3 <- R^3")
      {
        std::string_view data = R"(
let x : R^3; let y : R^3; x *= y;
)";

        std::string error_message = "undefined affectation type: R^3 *= R^3";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("R^2 <- R^2")
      {
        std::string_view data = R"(
let x : R^2; let y : R^2; x *= y;
)";

        std::string error_message = "undefined affectation type: R^2 *= R^2";

        CHECK_AST_THROWS_WITH(data, error_message);
      }

      SECTION("R^1 <- R^1")
      {
        std::string_view data = R"(
let x : R^1; let y : R^1; x *= y;
)";

        std::string error_message = "undefined affectation type: R^1 *= R^1";

        CHECK_AST_THROWS_WITH(data, error_message);
      }
    }

    SECTION("incorrect declarative/definition number of symbols")
    {
      std::string_view data = R"(
let (x,y,z):R*R*R, (x,y) = (2,3);
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      OperatorRepository::instance().reset();
      ASTModulesImporter{*ast};

      ASTSymbolTableBuilder{*ast};
      REQUIRE_THROWS_WITH(ASTSymbolInitializationChecker{*ast},
                          std::string{"invalid number of definition identifiers, expecting 3 found 2"});
      ast->m_symbol_table->clearValues();
    }

    SECTION("incorrect identifier/expression number of symbols")
    {
      std::string_view data = R"(
let y:R;
let x:R, (x,y) = (2,3);
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      OperatorRepository::instance().reset();
      ASTModulesImporter{*ast};

      ASTSymbolTableBuilder{*ast};
      REQUIRE_THROWS_WITH(ASTSymbolInitializationChecker{*ast},
                          std::string{"unexpected variable list, expecting one identifier"});
      ast->m_symbol_table->clearValues();
    }

    SECTION("incorrect definition variable identifier")
    {
      std::string_view data = R"(
let y:R;
let x:R, y = 3;
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      OperatorRepository::instance().reset();
      ASTModulesImporter{*ast};

      ASTSymbolTableBuilder{*ast};
      REQUIRE_THROWS_WITH(ASTSymbolInitializationChecker{*ast}, std::string{"invalid identifier, expecting 'x'"});
      ast->m_symbol_table->clearValues();
    }

    SECTION("invalid definition variable identifier order")
    {
      std::string_view data = R"(
let (x,y):R, (y,x) = (3,2);
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      OperatorRepository::instance().reset();
      ASTModulesImporter{*ast};

      ASTSymbolTableBuilder{*ast};
      REQUIRE_THROWS_WITH(ASTSymbolInitializationChecker{*ast}, std::string{"invalid identifier, expecting 'x'"});
      ast->m_symbol_table->clearValues();
    }

    SECTION("undefined affectations =")
    {
      SECTION("-> value")
      {
        SECTION("value -> value")
        {
          SECTION("-> B")
          {
            SECTION("N -> B")
            {
              std::string_view data = R"(
let n:N, n = 1;
let b:B, b = n;
)";

              std::string error_message = "undefined affectation type: B = N";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("Z -> B")
            {
              std::string_view data = R"(
let b:B, b = 1;
)";

              std::string error_message = "undefined affectation type: B = Z";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R -> B")
            {
              std::string_view data = R"(
let b:B, b = 1.2;
)";

              std::string error_message = "undefined affectation type: B = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> B")
            {
              std::string_view data = R"(
let b:B, b = [1.2];
)";

              std::string error_message = "undefined affectation type: B = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> B")
            {
              std::string_view data = R"(
let b:B, b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: B = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> B")
            {
              std::string_view data = R"(
let b:B, b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: B = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> B")
            {
              std::string_view data = R"(
let b:B, b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: B = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> B")
            {
              std::string_view data = R"(
let b:B, b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: B = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> B")
            {
              std::string_view data = R"(
let b:B, b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: B = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> B")
            {
              std::string_view data = R"(
let b:B, b = "foo";
)";

              std::string error_message = "undefined affectation type: B = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> B")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : B, v = bt;
)";

              std::string error_message = "undefined affectation type: B = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> N")
          {
            SECTION("R -> N")
            {
              std::string_view data = R"(
let b:N, b = 1.2;
)";

              std::string error_message = "undefined affectation type: N = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> N")
            {
              std::string_view data = R"(
let b:N, b = [1.2];
)";

              std::string error_message = "undefined affectation type: N = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> N")
            {
              std::string_view data = R"(
let b:N, b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: N = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> N")
            {
              std::string_view data = R"(
let b:N, b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: N = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> N")
            {
              std::string_view data = R"(
let b:N, b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: N = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> N")
            {
              std::string_view data = R"(
let b:N, b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: N = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> N")
            {
              std::string_view data = R"(
let b:N, b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: N = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> N")
            {
              std::string_view data = R"(
let b:N, b = "foo";
)";

              std::string error_message = "undefined affectation type: N = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> N")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : N, v = bt;
)";

              std::string error_message = "undefined affectation type: N = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> Z")
          {
            SECTION("R -> Z")
            {
              std::string_view data = R"(
let b:Z, b = 1.2;
)";

              std::string error_message = "undefined affectation type: Z = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> Z")
            {
              std::string_view data = R"(
let b:Z, b = [1.2];
)";

              std::string error_message = "undefined affectation type: Z = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> Z")
            {
              std::string_view data = R"(
let b:Z, b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: Z = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> Z")
            {
              std::string_view data = R"(
let b:Z, b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: Z = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> Z")
            {
              std::string_view data = R"(
let b:Z, b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: Z = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> Z")
            {
              std::string_view data = R"(
let b:Z, b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: Z = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> Z")
            {
              std::string_view data = R"(
let b:Z, b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: Z = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> Z")
            {
              std::string_view data = R"(
let b:Z, b = "foo";
)";

              std::string error_message = "undefined affectation type: Z = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> Z")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : Z, v = bt;
)";

              std::string error_message = "undefined affectation type: Z = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R")
          {
            SECTION("R^1 -> R")
            {
              std::string_view data = R"(
let b:R, b = [1.2];
)";

              std::string error_message = "undefined affectation type: R = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> R")
            {
              std::string_view data = R"(
let b:R, b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: R = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> R")
            {
              std::string_view data = R"(
let b:R, b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: R = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> R")
            {
              std::string_view data = R"(
let b:R, b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: R = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> R")
            {
              std::string_view data = R"(
let b:R, b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: R = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> R")
            {
              std::string_view data = R"(
let b:R, b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: R = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> R")
            {
              std::string_view data = R"(
let b:R, b = "foo";
)";

              std::string error_message = "undefined affectation type: R = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> R")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : R, v = bt;
)";

              std::string error_message = "undefined affectation type: R = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^1")
          {
            SECTION("R^2 -> R^1")
            {
              std::string_view data = R"(
let b:R^1, b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: R^1 = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> R^1")
            {
              std::string_view data = R"(
let b:R^1, b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: R^1 = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> R^1")
            {
              std::string_view data = R"(
let b:R^1, b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: R^1 = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> R^1")
            {
              std::string_view data = R"(
let b:R^1, b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: R^1 = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> R^1")
            {
              std::string_view data = R"(
let b:R^1, b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: R^1 = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> R^1")
            {
              std::string_view data = R"(
let b:R^1, b = "foo";
)";

              std::string error_message = "undefined affectation type: R^1 = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> R^1")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : R^1, v = bt;
)";

              std::string error_message = "undefined affectation type: R^1 = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^2")
          {
            SECTION("N -> R^2")
            {
              std::string_view data = R"(
let n:N, n = 1;
let b:R^2, b = n;
)";

              std::string error_message = "undefined affectation type: R^2 = N";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R -> R^2")
            {
              std::string_view data = R"(
let b:R^2, b = 1.2;
)";

              std::string error_message = "undefined affectation type: R^2 = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> R^2")
            {
              std::string_view data = R"(
let b:R^2, b = [1.2];
)";

              std::string error_message = "undefined affectation type: R^2 = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> R^2")
            {
              std::string_view data = R"(
let b:R^2, b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: R^2 = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> R^2")
            {
              std::string_view data = R"(
let b:R^2, b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: R^2 = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> R^2")
            {
              std::string_view data = R"(
let b:R^2, b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: R^2 = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> R^2")
            {
              std::string_view data = R"(
let b:R^2, b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: R^2 = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> R^2")
            {
              std::string_view data = R"(
let b:R^2, b = "foo";
)";

              std::string error_message = "undefined affectation type: R^2 = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> R^2")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : R^2, v = bt;
)";

              std::string error_message = "undefined affectation type: R^2 = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^3")
          {
            SECTION("N -> R^3")
            {
              std::string_view data = R"(
let n:N, n = 1;
let b:R^3, b = n;
)";

              std::string error_message = "undefined affectation type: R^3 = N";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R -> R^3")
            {
              std::string_view data = R"(
let b:R^3, b = 1.2;
)";

              std::string error_message = "undefined affectation type: R^3 = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> R^3")
            {
              std::string_view data = R"(
let b:R^3, b = [1.2];
)";

              std::string error_message = "undefined affectation type: R^3 = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> R^3")
            {
              std::string_view data = R"(
let b:R^3, b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: R^3 = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> R^3")
            {
              std::string_view data = R"(
let b:R^3, b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: R^3 = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> R^3")
            {
              std::string_view data = R"(
let b:R^3, b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: R^3 = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> R^3")
            {
              std::string_view data = R"(
let b:R^3, b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: R^3 = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> R^3")
            {
              std::string_view data = R"(
let b:R^3, b = "foo";
)";

              std::string error_message = "undefined affectation type: R^3 = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> R^3")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : R^3, v = bt;
)";

              std::string error_message = "undefined affectation type: R^3 = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^1x1")
          {
            SECTION("R^1 -> R^1x1")
            {
              std::string_view data = R"(
let b:R^1x1, b = [1.2];
)";

              std::string error_message = "undefined affectation type: R^1x1 = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> R^1x1")
            {
              std::string_view data = R"(
let b:R^1x1, b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: R^1x1 = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> R^1x1")
            {
              std::string_view data = R"(
let b:R^1x1, b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: R^1x1 = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> R^1x1")
            {
              std::string_view data = R"(
let b:R^1x1, b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: R^1x1 = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> R^1x1")
            {
              std::string_view data = R"(
let b:R^1x1, b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: R^1x1 = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> R^1x1")
            {
              std::string_view data = R"(
let b:R^1x1, b = "foo";
)";

              std::string error_message = "undefined affectation type: R^1x1 = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> R^1x1")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : R^1x1, v = bt;
)";

              std::string error_message = "undefined affectation type: R^1x1 = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^2x2")
          {
            SECTION("N -> R^2x2")
            {
              std::string_view data = R"(
let n:N, n = 1;
let b:R^2x2, b = n;
)";

              std::string error_message = "undefined affectation type: R^2x2 = N";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R -> R^2x2")
            {
              std::string_view data = R"(
let b:R^2x2, b = 1.2;
)";

              std::string error_message = "undefined affectation type: R^2x2 = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> R^2x2")
            {
              std::string_view data = R"(
let b:R^2x2, b = [1.2];
)";

              std::string error_message = "undefined affectation type: R^2x2 = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> R^2x2")
            {
              std::string_view data = R"(
let b:R^2x2, b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: R^2x2 = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> R^2x2")
            {
              std::string_view data = R"(
let b:R^2x2, b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: R^2x2 = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> R^2x2")
            {
              std::string_view data = R"(
let b:R^2x2, b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: R^2x2 = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> R^2x2")
            {
              std::string_view data = R"(
let b:R^2x2, b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: R^2x2 = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> R^2x2")
            {
              std::string_view data = R"(
let b:R^2x2, b = "foo";
)";

              std::string error_message = "undefined affectation type: R^2x2 = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> R^2x2")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : R^2x2, v = bt;
)";

              std::string error_message = "undefined affectation type: R^2x2 = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^3x3")
          {
            SECTION("N -> R^3x3")
            {
              std::string_view data = R"(
let n:N, n = 1;
let b:R^3x3, b = n;
)";

              std::string error_message = "undefined affectation type: R^3x3 = N";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R -> R^3x3")
            {
              std::string_view data = R"(
let b:R^3x3, b = 1.2;
)";

              std::string error_message = "undefined affectation type: R^3x3 = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> R^3x3")
            {
              std::string_view data = R"(
let b:R^3x3, b = [1.2];
)";

              std::string error_message = "undefined affectation type: R^3x3 = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> R^3x3")
            {
              std::string_view data = R"(
let b:R^3x3, b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: R^3x3 = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> R^3x3")
            {
              std::string_view data = R"(
let b:R^3x3, b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: R^3x3 = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> R^3x3")
            {
              std::string_view data = R"(
let b:R^3x3, b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: R^3x3 = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> R^3x3")
            {
              std::string_view data = R"(
let b:R^3x3, b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: R^3x3 = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> R^3x3")
            {
              std::string_view data = R"(
let b:R^3x3, b = "foo";
)";

              std::string error_message = "undefined affectation type: R^3x3 = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> R^3")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : R^3x3, v = bt;
)";

              std::string error_message = "undefined affectation type: R^3x3 = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }
        }

        SECTION("tuple -> value")
        {
          SECTION("-> B")
          {
            SECTION("(N) -> B")
            {
              std::string_view data = R"(
let t:(N), t = 1;
let b:B, b = t;
)";

              std::string error_message = "undefined affectation type: B = (N)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(Z) -> B")
            {
              std::string_view data = R"(
let t:(Z), t = 1;
let b:B, b = t;
)";

              std::string error_message = "undefined affectation type: B = (Z)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R) -> B")
            {
              std::string_view data = R"(
let t:(R), t = 1;
let b:B, b = t;
)";

              std::string error_message = "undefined affectation type: B = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> B")
            {
              std::string_view data = R"(
let t:(R^1), t = 1;
let b:B, b = t;
)";

              std::string error_message = "undefined affectation type: B = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> B")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:B, b = t;
)";

              std::string error_message = "undefined affectation type: B = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> B")
            {
              std::string_view data = R"(
let t:(R^3), t = [1, 2, 3];
let b:B, b = t;
)";

              std::string error_message = "undefined affectation type: B = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> B")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:B, b = t;
)";

              std::string error_message = "undefined affectation type: B = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> B")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:B, b = t;
)";

              std::string error_message = "undefined affectation type: B = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> B")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:B, b = t;
)";

              std::string error_message = "undefined affectation type: B = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> B")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:B, b = t;
)";

              std::string error_message = "undefined affectation type: B = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> B")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v : B, v = bt;
)";

              std::string error_message = "undefined affectation type: B = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> N")
          {
            SECTION("(R) -> N")
            {
              std::string_view data = R"(
let t:(R), t =  1.2;
let b:N, b = t;
)";

              std::string error_message = "undefined affectation type: N = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> N")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:N, b = t;
)";

              std::string error_message = "undefined affectation type: N = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> N")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:N, b = t;
)";

              std::string error_message = "undefined affectation type: N = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> N")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:N, b = t;
)";

              std::string error_message = "undefined affectation type: N = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> N")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:N, b = t;
)";

              std::string error_message = "undefined affectation type: N = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> N")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:N, b = t;
)";

              std::string error_message = "undefined affectation type: N = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> N")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:N, b = t;
)";

              std::string error_message = "undefined affectation type: N = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> N")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:N, b = t;
)";

              std::string error_message = "undefined affectation type: N = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> N")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v : N, v = bt;
)";

              std::string error_message = "undefined affectation type: N = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> Z")
          {
            SECTION("(R) -> Z")
            {
              std::string_view data = R"(
let t:(R), t = 1.2;
let b:Z, b = t;
)";

              std::string error_message = "undefined affectation type: Z = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> Z")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:Z, b = t;
)";

              std::string error_message = "undefined affectation type: Z = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> Z")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:Z, b = t;
)";

              std::string error_message = "undefined affectation type: Z = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> Z")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:Z, b = t;
)";

              std::string error_message = "undefined affectation type: Z = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> Z")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:Z, b = t;
)";

              std::string error_message = "undefined affectation type: Z = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> Z")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:Z, b = t;
)";

              std::string error_message = "undefined affectation type: Z = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> Z")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:Z, b = t;
)";

              std::string error_message = "undefined affectation type: Z = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> Z")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:Z, b = t;
)";

              std::string error_message = "undefined affectation type: Z = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> Z")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v : Z, v = bt;
)";

              std::string error_message = "undefined affectation type: Z = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R")
          {
            SECTION("(R^1) -> R")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:R, b = t;
)";

              std::string error_message = "undefined affectation type: R = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> R")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:R, b = t;
)";

              std::string error_message = "undefined affectation type: R = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> R")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:R, b = t;
)";

              std::string error_message = "undefined affectation type: R = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> R")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:R, b = t;
)";

              std::string error_message = "undefined affectation type: R = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> R")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:R, b = t;
)";

              std::string error_message = "undefined affectation type: R = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> R")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:R, b = t;
)";

              std::string error_message = "undefined affectation type: R = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> R")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:R, b = t;
)";

              std::string error_message = "undefined affectation type: R = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> R")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v : R, v = bt;
)";

              std::string error_message = "undefined affectation type: R = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^1")
          {
            SECTION("(R^2) -> R^1")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:R^1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1 = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> R^1")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:R^1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1 = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> R^1")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:R^1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1 = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> R^1")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:R^1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1 = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> R^1")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:R^1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1 = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> R^1")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:R^1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1 = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> R^1")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v : R^1, v = bt;
)";

              std::string error_message = "undefined affectation type: R^1 = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^2")
          {
            SECTION("(N) -> R^2")
            {
              std::string_view data = R"(
let n:(N), n = 1;
let b:R^2, b = n;
)";

              std::string error_message = "undefined affectation type: R^2 = (N)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R) -> R^2")
            {
              std::string_view data = R"(
let t:(R), t = 1.2;
let b:R^2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2 = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> R^2")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:R^2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2 = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> R^2")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:R^2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2 = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> R^2")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:R^2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2 = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> R^2")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:R^2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2 = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> R^2")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:R^2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2 = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> R^2")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:R^2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2 = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> R^2")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v : R^2, v = bt;
)";

              std::string error_message = "undefined affectation type: R^2 = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^3")
          {
            SECTION("(N) -> R^3")
            {
              std::string_view data = R"(
let n:(N), n = 1;
let b:R^3, b = n;
)";

              std::string error_message = "undefined affectation type: R^3 = (N)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R) -> R^3")
            {
              std::string_view data = R"(
let t:(R), t = 1.2;
let b:R^3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3 = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> R^3")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:R^3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3 = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> R^3")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:R^3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3 = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> R^3")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:R^3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3 = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> R^3")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:R^3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3 = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> R^3")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:R^3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3 = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> R^3")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:R^3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3 = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> R^3")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v : R^3, v = bt;
)";

              std::string error_message = "undefined affectation type: R^3 = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^1x1")
          {
            SECTION("(R^1) -> R^1x1")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:R^1x1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1x1 = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> R^1x1")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:R^1x1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1x1 = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> R^1x1")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:R^1x1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1x1 = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> R^1x1")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:R^1x1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1x1 = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> R^1x1")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:R^1x1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1x1 = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> R^1x1")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:R^1x1, b = t;
)";

              std::string error_message = "undefined affectation type: R^1x1 = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> R^1x1")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v : R^1x1, v = bt;
)";

              std::string error_message = "undefined affectation type: R^1x1 = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^2x2")
          {
            SECTION("(N) -> R^2x2")
            {
              std::string_view data = R"(
let n:(N), n = 1;
let b:R^2x2, b = n;
)";

              std::string error_message = "undefined affectation type: R^2x2 = (N)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R) -> R^2x2")
            {
              std::string_view data = R"(
let t:(R), t = 1.2;
let b:R^2x2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2x2 = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> R^2x2")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:R^2x2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2x2 = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> R^2x2")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:R^2x2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2x2 = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> R^2x2")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:R^2x2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2x2 = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> R^2x2")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:R^2x2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2x2 = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> R^2x2")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:R^2x2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2x2 = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> R^2x2")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:R^2x2, b = t;
)";

              std::string error_message = "undefined affectation type: R^2x2 = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> R^2x2")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v : R^2x2, v = bt;
)";

              std::string error_message = "undefined affectation type: R^2x2 = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> R^3x3")
          {
            SECTION("(N) -> R^3x3")
            {
              std::string_view data = R"(
let n:(N), n = 1;
let b:R^3x3, b = n;
)";

              std::string error_message = "undefined affectation type: R^3x3 = (N)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R) -> R^3x3")
            {
              std::string_view data = R"(
let t:(R), t = 1.2;
let b:R^3x3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3x3 = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> R^3x3")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:R^3x3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3x3 = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> R^3x3")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:R^3x3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3x3 = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> R^3x3")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:R^3x3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3x3 = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> R^3x3")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:R^3x3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3x3 = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> R^3x3")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:R^3x3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3x3 = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> R^3x3")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:R^3x3, b = t;
)";

              std::string error_message = "undefined affectation type: R^3x3 = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> R^3x3")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v : R^3x3, v = bt;
)";

              std::string error_message = "undefined affectation type: R^3x3 = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }
        }
      }

      SECTION("-> tuple")
      {
        SECTION("value -> tuple")
        {
          SECTION("-> (B)")
          {
            SECTION("N -> (B)")
            {
              std::string_view data = R"(
let n:N, n = 1;
let b:(B), b = n;
)";

              std::string error_message = "undefined affectation type: (B) = N";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("Z -> (B)")
            {
              std::string_view data = R"(
let b:(B), b = 1;
)";

              std::string error_message = "undefined affectation type: (B) = Z";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R -> (B)")
            {
              std::string_view data = R"(
let b:(B), b = 1.2;
)";

              std::string error_message = "undefined affectation type: (B) = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> (B)")
            {
              std::string_view data = R"(
let b:(B), b = [1.2];
)";

              std::string error_message = "undefined affectation type: (B) = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> (B)")
            {
              std::string_view data = R"(
let b:(B), b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: (B) = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> (B)")
            {
              std::string_view data = R"(
let b:(B), b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: (B) = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> (B)")
            {
              std::string_view data = R"(
let b:(B), b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: (B) = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> (B)")
            {
              std::string_view data = R"(
let b:(B), b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: (B) = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> (B)")
            {
              std::string_view data = R"(
let b:(B), b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: (B) = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> (B)")
            {
              std::string_view data = R"(
let b:(B), b = "foo";
)";

              std::string error_message = "undefined affectation type: (B) = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> (B)")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v:(B), v = bt;
)";

              std::string error_message = "undefined affectation type: (B) = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (N)")
          {
            SECTION("R -> (N)")
            {
              std::string_view data = R"(
let b:(N), b = 1.2;
)";

              std::string error_message = "undefined affectation type: (N) = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> (N)")
            {
              std::string_view data = R"(
let b:(N), b = [1.2];
)";

              std::string error_message = "undefined affectation type: (N) = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> (N)")
            {
              std::string_view data = R"(
let b:(N), b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: (N) = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> (N)")
            {
              std::string_view data = R"(
let b:(N), b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: (N) = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> (N)")
            {
              std::string_view data = R"(
let b:(N), b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: (N) = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> (N)")
            {
              std::string_view data = R"(
let b:(N), b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: (N) = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> (N)")
            {
              std::string_view data = R"(
let b:(N), b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: (N) = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> (N)")
            {
              std::string_view data = R"(
let b:(N), b = "foo";
)";

              std::string error_message = "undefined affectation type: (N) = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> (N)")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : (N), v = bt;
)";

              std::string error_message = "undefined affectation type: (N) = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (Z)")
          {
            SECTION("R -> (Z)")
            {
              std::string_view data = R"(
let b:(Z), b = 1.2;
)";

              std::string error_message = "undefined affectation type: (Z) = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> (Z)")
            {
              std::string_view data = R"(
let b:(Z), b = [1.2];
)";

              std::string error_message = "undefined affectation type: (Z) = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> (Z)")
            {
              std::string_view data = R"(
let b:(Z), b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: (Z) = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> (Z)")
            {
              std::string_view data = R"(
let b:(Z), b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: (Z) = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> (Z)")
            {
              std::string_view data = R"(
let b:(Z), b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: (Z) = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> (Z)")
            {
              std::string_view data = R"(
let b:(Z), b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: (Z) = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> (Z)")
            {
              std::string_view data = R"(
let b:(Z), b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: (Z) = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> (Z)")
            {
              std::string_view data = R"(
let b:(Z), b = "foo";
)";

              std::string error_message = "undefined affectation type: (Z) = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> (Z)")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : (Z), v = bt;
)";

              std::string error_message = "undefined affectation type: (Z) = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R)")
          {
            SECTION("R^1 -> (R)")
            {
              std::string_view data = R"(
let b:(R), b = [1.2];
)";

              std::string error_message = "undefined affectation type: (R) = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> (R)")
            {
              std::string_view data = R"(
let b:(R), b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: (R) = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> (R)")
            {
              std::string_view data = R"(
let b:(R), b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: (R) = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> (R)")
            {
              std::string_view data = R"(
let b:(R), b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: (R) = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> (R)")
            {
              std::string_view data = R"(
let b:(R), b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: (R) = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> (R)")
            {
              std::string_view data = R"(
let b:(R), b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: (R) = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> (R)")
            {
              std::string_view data = R"(
let b:(R), b = "foo";
)";

              std::string error_message = "undefined affectation type: (R) = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> (R)")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v :(R), v = bt;
)";

              std::string error_message = "undefined affectation type: (R) = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^1)")
          {
            SECTION("R^2 -> (R^1)")
            {
              std::string_view data = R"(
let b:(R^1), b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: (R^1) = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> (R^1)")
            {
              std::string_view data = R"(
let b:(R^1), b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: (R^1) = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> (R^1)")
            {
              std::string_view data = R"(
let b:(R^1), b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: (R^1) = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> (R^1)")
            {
              std::string_view data = R"(
let b:(R^1), b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: (R^1) = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> (R^1)")
            {
              std::string_view data = R"(
let b:(R^1), b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: (R^1) = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> (R^1)")
            {
              std::string_view data = R"(
let b:(R^1), b = "foo";
)";

              std::string error_message = "undefined affectation type: (R^1) = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> (R^1)")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : (R^1), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^1) = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^2)")
          {
            SECTION("N -> (R^2)")
            {
              std::string_view data = R"(
let n:N, n = 1;
let b:(R^2), b = n;
)";

              std::string error_message = "undefined affectation type: (R^2) = N";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R -> (R^2)")
            {
              std::string_view data = R"(
let b:(R^2), b = 1.2;
)";

              std::string error_message = "undefined affectation type: (R^2) = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> (R^2)")
            {
              std::string_view data = R"(
let b:(R^2), b = [1.2];
)";

              std::string error_message = "undefined affectation type: (R^2) = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> (R^2)")
            {
              std::string_view data = R"(
let b:(R^2), b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: (R^2) = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> (R^2)")
            {
              std::string_view data = R"(
let b:(R^2), b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: (R^2) = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> (R^2)")
            {
              std::string_view data = R"(
let b:(R^2), b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: (R^2) = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> (R^2)")
            {
              std::string_view data = R"(
let b:(R^2), b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: (R^2) = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> (R^2)")
            {
              std::string_view data = R"(
let b:(R^2), b = "foo";
)";

              std::string error_message = "undefined affectation type: (R^2) = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> (R^2)")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : (R^2), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^2) = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^3)")
          {
            SECTION("N -> (R^3)")
            {
              std::string_view data = R"(
let n:N, n = 1;
let b:(R^3), b = n;
)";

              std::string error_message = "undefined affectation type: (R^3) = N";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R -> (R^3)")
            {
              std::string_view data = R"(
let b:(R^3), b = 1.2;
)";

              std::string error_message = "undefined affectation type: (R^3) = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> (R^3)")
            {
              std::string_view data = R"(
let b:(R^3), b = [1.2];
)";

              std::string error_message = "undefined affectation type: (R^3) = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> (R^3)")
            {
              std::string_view data = R"(
let b:(R^3), b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: (R^3) = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> (R^3)")
            {
              std::string_view data = R"(
let b:(R^3), b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: (R^3) = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> (R^3)")
            {
              std::string_view data = R"(
let b:(R^3), b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: (R^3) = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> (R^3)")
            {
              std::string_view data = R"(
let b:(R^3), b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: (R^3) = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> (R^3)")
            {
              std::string_view data = R"(
let b:(R^3), b = "foo";
)";

              std::string error_message = "undefined affectation type: (R^3) = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> (R^3)")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : (R^3), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^3) = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^1x1)")
          {
            SECTION("R^1 -> (R^1x1)")
            {
              std::string_view data = R"(
let b:(R^1x1), b = [1.2];
)";

              std::string error_message = "undefined affectation type: (R^1x1) = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> (R^1x1)")
            {
              std::string_view data = R"(
let b:(R^1x1), b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: (R^1x1) = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> (R^1x1)")
            {
              std::string_view data = R"(
let b:(R^1x1), b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: (R^1x1) = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> (R^1x1)")
            {
              std::string_view data = R"(
let b:(R^1x1), b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: (R^1x1) = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> (R^1x1)")
            {
              std::string_view data = R"(
let b:(R^1x1), b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: (R^1x1) = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> (R^1x1)")
            {
              std::string_view data = R"(
let b:(R^1x1), b = "foo";
)";

              std::string error_message = "undefined affectation type: (R^1x1) = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> (R^1x1)")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : (R^1x1), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^1x1) = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^2x2)")
          {
            SECTION("N -> (R^2x2)")
            {
              std::string_view data = R"(
let n:N, n = 1;
let b:(R^2x2), b = n;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = N";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R -> (R^2x2)")
            {
              std::string_view data = R"(
let b:(R^2x2), b = 1.2;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> (R^2x2)")
            {
              std::string_view data = R"(
let b:(R^2x2), b = [1.2];
)";

              std::string error_message = "undefined affectation type: (R^2x2) = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> (R^2x2)")
            {
              std::string_view data = R"(
let b:(R^2x2), b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: (R^2x2) = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> (R^2x2)")
            {
              std::string_view data = R"(
let b:(R^2x2), b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: (R^2x2) = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> (R^2x2)")
            {
              std::string_view data = R"(
let b:(R^2x2), b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: (R^2x2) = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3x3 -> (R^2x2)")
            {
              std::string_view data = R"(
let b:(R^2x2), b = [[1.2, 3, 1],[1,2,3],[4,2,1]];
)";

              std::string error_message = "undefined affectation type: (R^2x2) = R^3x3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> (R^2x2)")
            {
              std::string_view data = R"(
let b:(R^2x2), b = "foo";
)";

              std::string error_message = "undefined affectation type: (R^2x2) = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> (R^2x2)")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : (R^2x2), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^3x3)")
          {
            SECTION("N -> (R^3x3)")
            {
              std::string_view data = R"(
let n:N, n = 1;
let b:(R^3x3), b = n;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = N";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R -> (R^3x3)")
            {
              std::string_view data = R"(
let b:(R^3x3), b = 1.2;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = R";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1 -> (R^3x3)")
            {
              std::string_view data = R"(
let b:(R^3x3), b = [1.2];
)";

              std::string error_message = "undefined affectation type: (R^3x3) = R^1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2 -> (R^3x3)")
            {
              std::string_view data = R"(
let b:(R^3x3), b = [1.2, 3];
)";

              std::string error_message = "undefined affectation type: (R^3x3) = R^2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^3 -> (R^3x3)")
            {
              std::string_view data = R"(
let b:(R^3x3), b = [1.2, 3, 1];
)";

              std::string error_message = "undefined affectation type: (R^3x3) = R^3";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^1x1 -> (R^3x3)")
            {
              std::string_view data = R"(
let b:(R^3x3), b = [[1.2]];
)";

              std::string error_message = "undefined affectation type: (R^3x3) = R^1x1";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("R^2x2 -> (R^3x3)")
            {
              std::string_view data = R"(
let b:(R^3x3), b = [[1.2, 3],[3,4]];
)";

              std::string error_message = "undefined affectation type: (R^3x3) = R^2x2";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("string -> (R^3x3)")
            {
              std::string_view data = R"(
let b:(R^3x3), b = "foo";
)";

              std::string error_message = "undefined affectation type: (R^3x3) = string";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("builtin_t -> (R^3x3)")
            {
              std::string_view data = R"(
let bt:builtin_t, bt = a;
let v : (R^3x3), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = builtin_t";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }
        }

        SECTION("tuple -> tuple")
        {
          SECTION("-> (B)")
          {
            SECTION("(N) -> (B)")
            {
              std::string_view data = R"(
let t:(N), t = 1;
let b:(B), b = t;
)";

              std::string error_message = "undefined affectation type: (B) = (N)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(Z) -> (B)")
            {
              std::string_view data = R"(
let t:(Z), t = 1;
let b:(B), b = t;
)";

              std::string error_message = "undefined affectation type: (B) = (Z)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R) -> (B)")
            {
              std::string_view data = R"(
let t:(R), t = 1;
let b:(B), b = t;
)";

              std::string error_message = "undefined affectation type: (B) = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> (B)")
            {
              std::string_view data = R"(
let t:(R^1), t = 1;
let b:(B), b = t;
)";

              std::string error_message = "undefined affectation type: (B) = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> (B)")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:(B), b = t;
)";

              std::string error_message = "undefined affectation type: (B) = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> (B)")
            {
              std::string_view data = R"(
let t:(R^3), t = [1, 2, 3];
let b:(B), b = t;
)";

              std::string error_message = "undefined affectation type: (B) = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> (B)")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:(B), b = t;
)";

              std::string error_message = "undefined affectation type: (B) = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> (B)")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:(B), b = t;
)";

              std::string error_message = "undefined affectation type: (B) = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> (B)")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:(B), b = t;
)";

              std::string error_message = "undefined affectation type: (B) = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> (B)")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:(B), b = t;
)";

              std::string error_message = "undefined affectation type: (B) = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> (B)")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v :(B), v = bt;
)";

              std::string error_message = "undefined affectation type: (B) = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (N)")
          {
            SECTION("(R) -> (N)")
            {
              std::string_view data = R"(
let t:(R), t =  1.2;
let b:(N), b = t;
)";

              std::string error_message = "undefined affectation type: (N) = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> (N)")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:(N), b = t;
)";

              std::string error_message = "undefined affectation type: (N) = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> (N)")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:(N), b = t;
)";

              std::string error_message = "undefined affectation type: (N) = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> (N)")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:(N), b = t;
)";

              std::string error_message = "undefined affectation type: (N) = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> (N)")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:(N), b = t;
)";

              std::string error_message = "undefined affectation type: (N) = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> (N)")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:(N), b = t;
)";

              std::string error_message = "undefined affectation type: (N) = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> (N)")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:(N), b = t;
)";

              std::string error_message = "undefined affectation type: (N) = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> (N)")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:(N), b = t;
)";

              std::string error_message = "undefined affectation type: (N) = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> (N)")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v :(N), v = bt;
)";

              std::string error_message = "undefined affectation type: (N) = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (Z)")
          {
            SECTION("(R) -> (Z)")
            {
              std::string_view data = R"(
let t:(R), t = 1.2;
let b:(Z), b = t;
)";

              std::string error_message = "undefined affectation type: (Z) = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> (Z)")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:(Z), b = t;
)";

              std::string error_message = "undefined affectation type: (Z) = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> (Z)")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:(Z), b = t;
)";

              std::string error_message = "undefined affectation type: (Z) = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> (Z)")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:(Z), b = t;
)";

              std::string error_message = "undefined affectation type: (Z) = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> (Z)")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:(Z), b = t;
)";

              std::string error_message = "undefined affectation type: (Z) = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> (Z)")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:(Z), b = t;
)";

              std::string error_message = "undefined affectation type: (Z) = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> (Z)")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:(Z), b = t;
)";

              std::string error_message = "undefined affectation type: (Z) = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> (Z)")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:(Z), b = t;
)";

              std::string error_message = "undefined affectation type: (Z) = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> (Z)")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v :(Z), v = bt;
)";

              std::string error_message = "undefined affectation type: (Z) = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R)")
          {
            SECTION("(R^1) -> (R)")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:(R), b = t;
)";

              std::string error_message = "undefined affectation type: (R) = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> (R)")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:(R), b = t;
)";

              std::string error_message = "undefined affectation type: (R) = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> (R)")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:(R), b = t;
)";

              std::string error_message = "undefined affectation type: (R) = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> (R)")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:(R), b = t;
)";

              std::string error_message = "undefined affectation type: (R) = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> (R)")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:(R), b = t;
)";

              std::string error_message = "undefined affectation type: (R) = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> (R)")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:(R), b = t;
)";

              std::string error_message = "undefined affectation type: (R) = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> (R)")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:(R), b = t;
)";

              std::string error_message = "undefined affectation type: (R) = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> (R)")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v :(R), v = bt;
)";

              std::string error_message = "undefined affectation type: (R) = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^1)")
          {
            SECTION("(R^2) -> (R^1)")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:(R^1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1) = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> (R^1)")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:(R^1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1) = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> (R^1)")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:(R^1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1) = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> (R^1)")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:(R^1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1) = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> (R^1)")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:(R^1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1) = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> (R^1)")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:(R^1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1) = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> (R^1)")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v :(R^1), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^1) = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^2)")
          {
            SECTION("(N) -> (R^2)")
            {
              std::string_view data = R"(
let n:(N), n = 1;
let b:(R^2), b = n;
)";

              std::string error_message = "undefined affectation type: (R^2) = (N)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R) -> (R^2)")
            {
              std::string_view data = R"(
let t:(R), t = 1.2;
let b:(R^2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2) = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> (R^2)")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:(R^2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2) = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> (R^2)")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:(R^2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2) = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> (R^2)")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:(R^2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2) = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> (R^2)")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:(R^2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2) = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> (R^2)")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:(R^2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2) = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> (R^2)")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:(R^2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2) = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> (R^2)")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v : (R^2), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^2) = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^3)")
          {
            SECTION("(N) -> (R^3)")
            {
              std::string_view data = R"(
let n:(N), n = 1;
let b:(R^3), b = n;
)";

              std::string error_message = "undefined affectation type: (R^3) = (N)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R) -> (R^3)")
            {
              std::string_view data = R"(
let t:(R), t = 1.2;
let b:(R^3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3) = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> (R^3)")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:(R^3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3) = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> (R^3)")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:(R^3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3) = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> (R^3)")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:(R^3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3) = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> (R^3)")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:(R^3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3) = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> (R^3)")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:(R^3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3) = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> (R^3)")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:(R^3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3) = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> (R^3)")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v :(R^3), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^3) = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^1x1)")
          {
            SECTION("(R^1) -> (R^1x1)")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:(R^1x1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1x1) = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> (R^1x1)")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:(R^1x1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1x1) = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> (R^1x1)")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:(R^1x1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1x1) = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> (R^1x1)")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:(R^1x1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1x1) = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> (R^1x1)")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:(R^1x1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1x1) = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> (R^1x1)")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:(R^1x1), b = t;
)";

              std::string error_message = "undefined affectation type: (R^1x1) = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> (R^1x1)")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v :(R^1x1), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^1x1) = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^2x2)")
          {
            SECTION("(N) -> (R^2x2)")
            {
              std::string_view data = R"(
let n:(N), n = 1;
let b:(R^2x2), b = n;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = (N)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R) -> (R^2x2)")
            {
              std::string_view data = R"(
let t:(R), t = 1.2;
let b:(R^2x2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> (R^2x2)")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:(R^2x2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> (R^2x2)")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:(R^2x2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> (R^2x2)")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:(R^2x2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> (R^2x2)")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:(R^2x2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3x3) -> (R^2x2)")
            {
              std::string_view data = R"(
let t:(R^3x3), t = [[1.2, 3, 1],[1,2,3],[4,2,1]];
let b:(R^2x2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = (R^3x3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> (R^2x2)")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:(R^2x2), b = t;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> (R^2x2)")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v :(R^2x2), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^2x2) = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }

          SECTION("-> (R^3x3)")
          {
            SECTION("(N) -> (R^3x3)")
            {
              std::string_view data = R"(
let n:(N), n = 1;
let b:(R^3x3), b = n;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = (N)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R) -> (R^3x3)")
            {
              std::string_view data = R"(
let t:(R), t = 1.2;
let b:(R^3x3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = (R)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1) -> (R^3x3)")
            {
              std::string_view data = R"(
let t:(R^1), t = [1.2];
let b:(R^3x3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = (R^1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2) -> (R^3x3)")
            {
              std::string_view data = R"(
let t:(R^2), t = [1.2, 3];
let b:(R^3x3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = (R^2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^3) -> (R^3x3)")
            {
              std::string_view data = R"(
let t:(R^3), t = [1.2, 3, 1];
let b:(R^3x3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = (R^3)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^1x1) -> (R^3x3)")
            {
              std::string_view data = R"(
let t:(R^1x1), t = [[1.2]];
let b:(R^3x3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = (R^1x1)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(R^2x2) -> (R^3x3)")
            {
              std::string_view data = R"(
let t:(R^2x2), t = [[1.2, 3],[3,4]];
let b:(R^3x3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = (R^2x2)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(string) -> (R^3x3)")
            {
              std::string_view data = R"(
let t:(string), t = "foo";
let b:(R^3x3), b = t;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = (string)";

              CHECK_AST_THROWS_WITH(data, error_message);
            }

            SECTION("(builtin_t) -> (R^3)")
            {
              std::string_view data = R"(
let bt:(builtin_t), bt = a;
let v :(R^3x3), v = bt;
)";

              std::string error_message = "undefined affectation type: (R^3x3) = (builtin_t)";

              CHECK_AST_WITH_BUILTIN_THROWS_WITH(data, error_message);
            }
          }
        }
      }
    }

    SECTION("void as a type")
    {
      SECTION("declaration")
      {
        std::string_view data     = R"(
let a:void;
)";
        std::string error_message = "'void' keyword does not define a type";

        {
          TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
          auto ast = ASTBuilder::build(input);
          OperatorRepository::instance().reset();
          ASTModulesImporter{*ast};

          ASTSymbolTableBuilder{*ast};
          REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error_message);
          ast->m_symbol_table->clearValues();
        }
      }

      SECTION("definition")
      {
        std::string_view data = R"(
let a:void, a = 3;
)";

        std::string error_message = "'void' keyword does not define a type";

        {
          TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
          auto ast = ASTBuilder::build(input);
          OperatorRepository::instance().reset();
          ASTModulesImporter{*ast};

          ASTSymbolTableBuilder{*ast};
          REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error_message);
          ast->m_symbol_table->clearValues();
        }
      }
    }
  }
}

#ifdef __clang__
#pragma clang optimize on
#else   // __clang__
#ifdef __GNUG__
#pragma GCC pop_options
#endif   // __GNUG__
#endif   // __clang__
