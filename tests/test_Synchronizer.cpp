#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/Synchronizer.hpp>
#include <mesh/SynchronizerManager.hpp>
#include <utils/pugs_config.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("Synchronizer", "[mesh]")
{
  SECTION("ItemValue")
  {
    auto is_same_item_value = [](auto a, auto b) {
      using IndexT = typename decltype(a)::index_type;
      bool is_same = true;
      for (IndexT i = 0; i < a.numberOfItems(); ++i) {
        is_same &= (a[i] == b[i]);
      }
      return parallel::allReduceAnd(is_same);
    };

    SECTION("1D")
    {
      constexpr size_t Dimension = 1;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity =
        MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>()->connectivity();

      SECTION("synchonize NodeValue")
      {
        const auto node_owner  = connectivity.nodeOwner();
        const auto node_number = connectivity.nodeNumber();

        NodeValue<int> node_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfNodes(),
          PUGS_LAMBDA(const NodeId node_id) { node_value_ref[node_id] = node_owner[node_id] + node_number[node_id]; });

        NodeValue<int> node_value{connectivity};
        parallel_for(
          connectivity.numberOfNodes(),
          PUGS_LAMBDA(const NodeId node_id) { node_value[node_id] = parallel::rank() + node_number[node_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(node_value, node_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_value);

        REQUIRE(is_same_item_value(node_value, node_value_ref));
      }

      SECTION("synchonize EdgeValue")
      {
        const auto edge_owner  = connectivity.edgeOwner();
        const auto edge_number = connectivity.edgeNumber();

        EdgeValue<int> edge_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfEdges(),
          PUGS_LAMBDA(const EdgeId edge_id) { edge_value_ref[edge_id] = edge_owner[edge_id] + edge_number[edge_id]; });

        EdgeValue<int> edge_value{connectivity};
        parallel_for(
          connectivity.numberOfEdges(),
          PUGS_LAMBDA(const EdgeId edge_id) { edge_value[edge_id] = parallel::rank() + edge_number[edge_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(edge_value, edge_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_value);

        REQUIRE(is_same_item_value(edge_value, edge_value_ref));
      }

      SECTION("synchonize FaceValue")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        FaceValue<int> face_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfFaces(),
          PUGS_LAMBDA(const FaceId face_id) { face_value_ref[face_id] = face_owner[face_id] + face_number[face_id]; });

        FaceValue<int> face_value{connectivity};
        parallel_for(
          connectivity.numberOfFaces(),
          PUGS_LAMBDA(const FaceId face_id) { face_value[face_id] = parallel::rank() + face_number[face_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(face_value, face_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_value);

        REQUIRE(is_same_item_value(face_value, face_value_ref));
      }

      SECTION("synchonize CellValue")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        CellValue<int> cell_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfCells(),
          PUGS_LAMBDA(const CellId cell_id) { cell_value_ref[cell_id] = cell_owner[cell_id] + cell_number[cell_id]; });

        CellValue<int> cell_value{connectivity};
        parallel_for(
          connectivity.numberOfCells(),
          PUGS_LAMBDA(const CellId cell_id) { cell_value[cell_id] = parallel::rank() + cell_number[cell_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value, cell_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value);

        REQUIRE(is_same_item_value(cell_value, cell_value_ref));
      }
    }

    SECTION("2D")
    {
      constexpr size_t Dimension = 2;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>()->connectivity();

      SECTION("synchonize NodeValue")
      {
        const auto node_owner  = connectivity.nodeOwner();
        const auto node_number = connectivity.nodeNumber();

        NodeValue<int> node_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfNodes(),
          PUGS_LAMBDA(const NodeId node_id) { node_value_ref[node_id] = node_owner[node_id] + node_number[node_id]; });

        NodeValue<int> node_value{connectivity};
        parallel_for(
          connectivity.numberOfNodes(),
          PUGS_LAMBDA(const NodeId node_id) { node_value[node_id] = parallel::rank() + node_number[node_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(node_value, node_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_value);

        REQUIRE(is_same_item_value(node_value, node_value_ref));
      }

      SECTION("synchonize EdgeValue")
      {
        const auto edge_owner  = connectivity.edgeOwner();
        const auto edge_number = connectivity.edgeNumber();

        EdgeValue<int> edge_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfEdges(),
          PUGS_LAMBDA(const EdgeId edge_id) { edge_value_ref[edge_id] = edge_owner[edge_id] + edge_number[edge_id]; });

        EdgeValue<int> edge_value{connectivity};
        parallel_for(
          connectivity.numberOfEdges(),
          PUGS_LAMBDA(const EdgeId edge_id) { edge_value[edge_id] = parallel::rank() + edge_number[edge_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(edge_value, edge_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_value);

        REQUIRE(is_same_item_value(edge_value, edge_value_ref));
      }

      SECTION("synchonize FaceValue")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        FaceValue<int> face_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfFaces(),
          PUGS_LAMBDA(const FaceId face_id) { face_value_ref[face_id] = face_owner[face_id] + face_number[face_id]; });

        FaceValue<int> face_value{connectivity};
        parallel_for(
          connectivity.numberOfFaces(),
          PUGS_LAMBDA(const FaceId face_id) { face_value[face_id] = parallel::rank() + face_number[face_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(face_value, face_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_value);

        REQUIRE(is_same_item_value(face_value, face_value_ref));
      }

      SECTION("synchonize CellValue")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        CellValue<int> cell_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfCells(),
          PUGS_LAMBDA(const CellId cell_id) { cell_value_ref[cell_id] = cell_owner[cell_id] + cell_number[cell_id]; });

        CellValue<int> cell_value{connectivity};
        parallel_for(
          connectivity.numberOfCells(),
          PUGS_LAMBDA(const CellId cell_id) { cell_value[cell_id] = parallel::rank() + cell_number[cell_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value, cell_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value);

        REQUIRE(is_same_item_value(cell_value, cell_value_ref));
      }
    }

    SECTION("3D")
    {
      constexpr size_t Dimension = 3;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity = MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>()->connectivity();

      SECTION("synchonize NodeValue")
      {
        const auto node_owner  = connectivity.nodeOwner();
        const auto node_number = connectivity.nodeNumber();

        NodeValue<int> node_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfNodes(),
          PUGS_LAMBDA(const NodeId node_id) { node_value_ref[node_id] = node_owner[node_id] + node_number[node_id]; });

        NodeValue<int> node_value{connectivity};
        parallel_for(
          connectivity.numberOfNodes(),
          PUGS_LAMBDA(const NodeId node_id) { node_value[node_id] = parallel::rank() + node_number[node_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(node_value, node_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_value);

        REQUIRE(is_same_item_value(node_value, node_value_ref));
      }

      SECTION("synchonize EdgeValue")
      {
        const auto edge_owner  = connectivity.edgeOwner();
        const auto edge_number = connectivity.edgeNumber();

        EdgeValue<int> edge_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfEdges(),
          PUGS_LAMBDA(const EdgeId edge_id) { edge_value_ref[edge_id] = edge_owner[edge_id] + edge_number[edge_id]; });

        EdgeValue<int> edge_value{connectivity};
        parallel_for(
          connectivity.numberOfEdges(),
          PUGS_LAMBDA(const EdgeId edge_id) { edge_value[edge_id] = parallel::rank() + edge_number[edge_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(edge_value, edge_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_value);

        REQUIRE(is_same_item_value(edge_value, edge_value_ref));
      }

      SECTION("synchonize FaceValue")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        FaceValue<int> face_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfFaces(),
          PUGS_LAMBDA(const FaceId face_id) { face_value_ref[face_id] = face_owner[face_id] + face_number[face_id]; });

        FaceValue<int> face_value{connectivity};
        parallel_for(
          connectivity.numberOfFaces(),
          PUGS_LAMBDA(const FaceId face_id) { face_value[face_id] = parallel::rank() + face_number[face_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(face_value, face_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_value);

        REQUIRE(is_same_item_value(face_value, face_value_ref));
      }

      SECTION("synchonize CellValue")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        CellValue<int> cell_value_ref{connectivity};
        parallel_for(
          connectivity.numberOfCells(),
          PUGS_LAMBDA(const CellId cell_id) { cell_value_ref[cell_id] = cell_owner[cell_id] + cell_number[cell_id]; });

        CellValue<int> cell_value{connectivity};
        parallel_for(
          connectivity.numberOfCells(),
          PUGS_LAMBDA(const CellId cell_id) { cell_value[cell_id] = parallel::rank() + cell_number[cell_id]; });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value, cell_value_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value);

        REQUIRE(is_same_item_value(cell_value, cell_value_ref));
      }
    }
  }

  SECTION("ItemArray")
  {
    auto is_same_item_array = [](auto a, auto b) {
      using IndexT = typename decltype(a)::index_type;
      bool is_same = true;
      for (IndexT i = 0; i < a.numberOfItems(); ++i) {
        for (size_t j = 0; j < a.sizeOfArrays(); ++j) {
          is_same &= (a[i][j] == b[i][j]);
        }
      }
      return parallel::allReduceAnd(is_same);
    };

    SECTION("1D")
    {
      constexpr size_t Dimension = 1;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity =
        MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>()->connectivity();

      SECTION("synchonize NodeArray")
      {
        const auto node_owner  = connectivity.nodeOwner();
        const auto node_number = connectivity.nodeNumber();

        NodeArray<int> node_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t i = 0; i < node_array_ref.sizeOfArrays(); ++i) {
              node_array_ref[node_id][i] = (i + 1) * node_owner[node_id] + i + node_number[node_id];
            }
          });

        NodeArray<int> node_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t i = 0; i < node_array.sizeOfArrays(); ++i) {
              node_array[node_id][i] = (i + 1) * parallel::rank() + i + node_number[node_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(node_array, node_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_array);

        REQUIRE(is_same_item_array(node_array, node_array_ref));
      }

      SECTION("synchonize EdgeArray")
      {
        const auto edge_owner  = connectivity.edgeOwner();
        const auto edge_number = connectivity.edgeNumber();

        EdgeArray<int> edge_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t i = 0; i < edge_array_ref.sizeOfArrays(); ++i) {
              edge_array_ref[edge_id][i] = (i + 1) * edge_owner[edge_id] + i + edge_number[edge_id];
            }
          });

        EdgeArray<int> edge_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t i = 0; i < edge_array.sizeOfArrays(); ++i) {
              edge_array[edge_id][i] = (i + 1) * parallel::rank() + i + edge_number[edge_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(edge_array, edge_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_array);

        REQUIRE(is_same_item_array(edge_array, edge_array_ref));
      }

      SECTION("synchonize FaceArray")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        FaceArray<int> face_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t i = 0; i < face_array_ref.sizeOfArrays(); ++i) {
              face_array_ref[face_id][i] = (i + 1) * face_owner[face_id] + i + face_number[face_id];
            }
          });

        FaceArray<int> face_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t i = 0; i < face_array.sizeOfArrays(); ++i) {
              face_array[face_id][i] = (i + 1) * parallel::rank() + i + face_number[face_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(face_array, face_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_array);

        REQUIRE(is_same_item_array(face_array, face_array_ref));
      }

      SECTION("synchonize CellArray")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        CellArray<int> cell_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t i = 0; i < cell_array_ref.sizeOfArrays(); ++i) {
              cell_array_ref[cell_id][i] = (i + 1) * cell_owner[cell_id] + i + cell_number[cell_id];
            }
          });

        CellArray<int> cell_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t i = 0; i < cell_array.sizeOfArrays(); ++i) {
              cell_array[cell_id][i] = (i + 1) * parallel::rank() + i + cell_number[cell_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array, cell_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array);

        REQUIRE(is_same_item_array(cell_array, cell_array_ref));
      }
    }

    SECTION("2D")
    {
      constexpr size_t Dimension = 2;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>()->connectivity();

      SECTION("synchonize NodeArray")
      {
        const auto node_owner  = connectivity.nodeOwner();
        const auto node_number = connectivity.nodeNumber();

        NodeArray<int> node_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t i = 0; i < node_array_ref.sizeOfArrays(); ++i) {
              node_array_ref[node_id][i] = (i + 1) * node_owner[node_id] + i + node_number[node_id];
            }
          });

        NodeArray<int> node_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t i = 0; i < node_array.sizeOfArrays(); ++i) {
              node_array[node_id][i] = (i + 1) * parallel::rank() + i + node_number[node_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(node_array, node_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_array);

        REQUIRE(is_same_item_array(node_array, node_array_ref));
      }

      SECTION("synchonize EdgeArray")
      {
        const auto edge_owner  = connectivity.edgeOwner();
        const auto edge_number = connectivity.edgeNumber();

        EdgeArray<int> edge_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t i = 0; i < edge_array_ref.sizeOfArrays(); ++i) {
              edge_array_ref[edge_id][i] = (i + 1) * edge_owner[edge_id] + i + edge_number[edge_id];
            }
          });

        EdgeArray<int> edge_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t i = 0; i < edge_array.sizeOfArrays(); ++i) {
              edge_array[edge_id][i] = (i + 1) * parallel::rank() + i + edge_number[edge_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(edge_array, edge_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_array);

        REQUIRE(is_same_item_array(edge_array, edge_array_ref));
      }

      SECTION("synchonize FaceArray")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        FaceArray<int> face_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t i = 0; i < face_array_ref.sizeOfArrays(); ++i) {
              face_array_ref[face_id][i] = (i + 1) * face_owner[face_id] + i + face_number[face_id];
            }
          });

        FaceArray<int> face_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t i = 0; i < face_array.sizeOfArrays(); ++i) {
              face_array[face_id][i] = (i + 1) * parallel::rank() + i + face_number[face_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(face_array, face_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_array);

        REQUIRE(is_same_item_array(face_array, face_array_ref));
      }

      SECTION("synchonize CellArray")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        CellArray<int> cell_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t i = 0; i < cell_array_ref.sizeOfArrays(); ++i) {
              cell_array_ref[cell_id][i] = (i + 1) * cell_owner[cell_id] + i + cell_number[cell_id];
            }
          });

        CellArray<int> cell_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t i = 0; i < cell_array.sizeOfArrays(); ++i) {
              cell_array[cell_id][i] = (i + 1) * parallel::rank() + i + cell_number[cell_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array, cell_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array);

        REQUIRE(is_same_item_array(cell_array, cell_array_ref));
      }
    }

    SECTION("3D")
    {
      constexpr size_t Dimension = 3;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity = MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>()->connectivity();

      SECTION("synchonize NodeArray")
      {
        const auto node_owner  = connectivity.nodeOwner();
        const auto node_number = connectivity.nodeNumber();

        NodeArray<int> node_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t i = 0; i < node_array_ref.sizeOfArrays(); ++i) {
              node_array_ref[node_id][i] = (i + 1) * node_owner[node_id] + i + node_number[node_id];
            }
          });

        NodeArray<int> node_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t i = 0; i < node_array.sizeOfArrays(); ++i) {
              node_array[node_id][i] = (i + 1) * parallel::rank() + i + node_number[node_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(node_array, node_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_array);

        REQUIRE(is_same_item_array(node_array, node_array_ref));
      }

      SECTION("synchonize EdgeArray")
      {
        const auto edge_owner  = connectivity.edgeOwner();
        const auto edge_number = connectivity.edgeNumber();

        EdgeArray<int> edge_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t i = 0; i < edge_array_ref.sizeOfArrays(); ++i) {
              edge_array_ref[edge_id][i] = (i + 1) * edge_owner[edge_id] + i + edge_number[edge_id];
            }
          });

        EdgeArray<int> edge_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t i = 0; i < edge_array.sizeOfArrays(); ++i) {
              edge_array[edge_id][i] = (i + 1) * parallel::rank() + i + edge_number[edge_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(edge_array, edge_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_array);

        REQUIRE(is_same_item_array(edge_array, edge_array_ref));
      }

      SECTION("synchonize FaceArray")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        FaceArray<int> face_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t i = 0; i < face_array_ref.sizeOfArrays(); ++i) {
              face_array_ref[face_id][i] = (i + 1) * face_owner[face_id] + i + face_number[face_id];
            }
          });

        FaceArray<int> face_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t i = 0; i < face_array.sizeOfArrays(); ++i) {
              face_array[face_id][i] = (i + 1) * parallel::rank() + i + face_number[face_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(face_array, face_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_array);

        REQUIRE(is_same_item_array(face_array, face_array_ref));
      }

      SECTION("synchonize CellArray")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        CellArray<int> cell_array_ref{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t i = 0; i < cell_array_ref.sizeOfArrays(); ++i) {
              cell_array_ref[cell_id][i] = (i + 1) * cell_owner[cell_id] + i + cell_number[cell_id];
            }
          });

        CellArray<int> cell_array{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t i = 0; i < cell_array.sizeOfArrays(); ++i) {
              cell_array[cell_id][i] = (i + 1) * parallel::rank() + i + cell_number[cell_id];
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array, cell_array_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array);

        REQUIRE(is_same_item_array(cell_array, cell_array_ref));
      }
    }
  }

  SECTION("SubItemValuePerItem")
  {
    auto is_same_item_value = [](auto a, auto b) {
      using IndexT = typename decltype(a)::index_type;
      bool is_same = true;
      for (IndexT i_item = 0; i_item < a.numberOfItems(); ++i_item) {
        for (size_t l = 0; l < a.numberOfSubValues(i_item); ++l) {
          is_same &= (a(i_item, l) == b(i_item, l));
        }
      }
      return parallel::allReduceAnd(is_same);
    };

    auto reset_ghost_values = [](auto sub_item_value_per_item, auto item_owner, auto value) {
      using IndexT = typename decltype(sub_item_value_per_item)::index_type;
      static_assert(std::is_same_v<typename decltype(sub_item_value_per_item)::index_type,
                                   typename decltype(item_owner)::index_type>);
      for (IndexT i_item = 0; i_item < sub_item_value_per_item.numberOfItems(); ++i_item) {
        if (item_owner[i_item] != static_cast<int>(parallel::rank())) {
          for (size_t l = 0; l < sub_item_value_per_item.numberOfSubValues(i_item); ++l) {
            sub_item_value_per_item(i_item, l) = value;
          }
        }
      }
    };

    SECTION("1D")
    {
      constexpr size_t Dimension = 1;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity =
        MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>()->connectivity();

      SECTION("synchonize NodeValuePerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        NodeValuePerCell<int> node_value_per_cell_ref{connectivity};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              node_value_per_cell_ref(cell_id, j) = cell_owner[cell_id] + cell_number[cell_id] + j;
            }
          });

        NodeValuePerCell<int> node_value_per_cell{connectivity};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              node_value_per_cell(cell_id, j) = parallel::rank() + cell_number[cell_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(node_value_per_cell, node_value_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_value_per_cell);

        REQUIRE(is_same_item_value(node_value_per_cell, node_value_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(node_value_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_value(node_value_per_cell, node_value_per_cell_ref));
          synchronizer.synchronize(node_value_per_cell);
          REQUIRE(is_same_item_value(node_value_per_cell, node_value_per_cell_ref));
        }
      }

      SECTION("synchonize EdgeValuePerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        EdgeValuePerCell<int> edge_value_per_cell_ref{connectivity};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              edge_value_per_cell_ref(cell_id, j) = cell_owner[cell_id] + cell_number[cell_id] + j;
            }
          });

        EdgeValuePerCell<int> edge_value_per_cell{connectivity};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              edge_value_per_cell(cell_id, j) = parallel::rank() + cell_number[cell_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_value_per_cell);

        REQUIRE(is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(edge_value_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));
          synchronizer.synchronize(edge_value_per_cell);
          REQUIRE(is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));
        }
      }

      SECTION("synchonize FaceValuePerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        FaceValuePerCell<int> face_value_per_cell_ref{connectivity};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              face_value_per_cell_ref(cell_id, j) = cell_owner[cell_id] + cell_number[cell_id] + j;
            }
          });

        FaceValuePerCell<int> face_value_per_cell{connectivity};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              face_value_per_cell(cell_id, j) = parallel::rank() + cell_number[cell_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(face_value_per_cell, face_value_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_value_per_cell);

        REQUIRE(is_same_item_value(face_value_per_cell, face_value_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(face_value_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_value(face_value_per_cell, face_value_per_cell_ref));
          synchronizer.synchronize(face_value_per_cell);
          REQUIRE(is_same_item_value(face_value_per_cell, face_value_per_cell_ref));
        }
      }

      SECTION("synchonize CellValuePerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto node_to_cell_matrix = connectivity.nodeToCellMatrix();

        CellValuePerNode<int> cell_value_per_node_ref{connectivity};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < cell_value_per_node_ref.numberOfSubValues(node_id); ++j) {
              cell_value_per_node_ref(node_id, j) =   // node_owner[node_id] +
                node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]];
            }
          });

        CellValuePerNode<int> cell_value_per_node{connectivity};
        cell_value_per_node.fill(0);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < cell_value_per_node.numberOfSubValues(node_id); ++j) {
              if (node_is_owned[node_id]) {
                cell_value_per_node(node_id, j) =
                  node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value_per_node, cell_value_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value_per_node);
        REQUIRE(is_same_item_value(cell_value_per_node, cell_value_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(cell_value_per_node, connectivity.nodeOwner(), 0);
          REQUIRE(not is_same_item_value(cell_value_per_node, cell_value_per_node_ref));
          synchronizer.synchronize(cell_value_per_node);
          REQUIRE(is_same_item_value(cell_value_per_node, cell_value_per_node_ref));
        }
      }

      SECTION("synchonize CellValuePerEdge")
      {
        const auto edge_is_owned = connectivity.edgeIsOwned();
        const auto edge_number   = connectivity.edgeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();

        CellValuePerEdge<int> cell_value_per_edge_ref{connectivity};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < cell_value_per_edge_ref.numberOfSubValues(edge_id); ++j) {
              cell_value_per_edge_ref(edge_id, j) =   // edge_owner[edge_id] +
                edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]];
            }
          });

        CellValuePerEdge<int> cell_value_per_edge{connectivity};
        cell_value_per_edge.fill(0);
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < cell_value_per_edge.numberOfSubValues(edge_id); ++j) {
              if (edge_is_owned[edge_id]) {
                cell_value_per_edge(edge_id, j) =
                  edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value_per_edge);
        REQUIRE(is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(cell_value_per_edge, connectivity.edgeOwner(), 0);
          REQUIRE(not is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));
          synchronizer.synchronize(cell_value_per_edge);
          REQUIRE(is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));
        }
      }

      SECTION("synchonize CellValuePerFace")
      {
        const auto face_is_owned = connectivity.faceIsOwned();
        const auto face_number   = connectivity.faceNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto face_to_cell_matrix = connectivity.faceToCellMatrix();

        CellValuePerFace<int> cell_value_per_face_ref{connectivity};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < cell_value_per_face_ref.numberOfSubValues(face_id); ++j) {
              cell_value_per_face_ref(face_id, j) =   // face_owner[face_id] +
                face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]];
            }
          });

        CellValuePerFace<int> cell_value_per_face{connectivity};
        cell_value_per_face.fill(0);
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < cell_value_per_face.numberOfSubValues(face_id); ++j) {
              if (face_is_owned[face_id]) {
                cell_value_per_face(face_id, j) =
                  face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value_per_face, cell_value_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value_per_face);
        REQUIRE(is_same_item_value(cell_value_per_face, cell_value_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(cell_value_per_face, connectivity.faceOwner(), 0);
          REQUIRE(not is_same_item_value(cell_value_per_face, cell_value_per_face_ref));
          synchronizer.synchronize(cell_value_per_face);
          REQUIRE(is_same_item_value(cell_value_per_face, cell_value_per_face_ref));
        }
      }
    }

    SECTION("2D")
    {
      constexpr size_t Dimension = 2;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>()->connectivity();

      SECTION("synchonize NodeValuePerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        NodeValuePerCell<int> node_value_per_cell_ref{connectivity};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              node_value_per_cell_ref(cell_id, j) = cell_owner[cell_id] + cell_number[cell_id] + j;
            }
          });

        NodeValuePerCell<int> node_value_per_cell{connectivity};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              node_value_per_cell(cell_id, j) = parallel::rank() + cell_number[cell_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(node_value_per_cell, node_value_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_value_per_cell);

        REQUIRE(is_same_item_value(node_value_per_cell, node_value_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(node_value_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_value(node_value_per_cell, node_value_per_cell_ref));
          synchronizer.synchronize(node_value_per_cell);
          REQUIRE(is_same_item_value(node_value_per_cell, node_value_per_cell_ref));
        }
      }

      SECTION("synchonize EdgeValuePerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        EdgeValuePerCell<int> edge_value_per_cell_ref{connectivity};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              edge_value_per_cell_ref(cell_id, j) = cell_owner[cell_id] + cell_number[cell_id] + j;
            }
          });

        EdgeValuePerCell<int> edge_value_per_cell{connectivity};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              edge_value_per_cell(cell_id, j) = parallel::rank() + cell_number[cell_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_value_per_cell);

        REQUIRE(is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(edge_value_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));
          synchronizer.synchronize(edge_value_per_cell);
          REQUIRE(is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));
        }
      }

      SECTION("synchonize FaceValuePerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        FaceValuePerCell<int> face_value_per_cell_ref{connectivity};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              face_value_per_cell_ref(cell_id, j) = cell_owner[cell_id] + cell_number[cell_id] + j;
            }
          });

        FaceValuePerCell<int> face_value_per_cell{connectivity};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              face_value_per_cell(cell_id, j) = parallel::rank() + cell_number[cell_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(face_value_per_cell, face_value_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_value_per_cell);

        REQUIRE(is_same_item_value(face_value_per_cell, face_value_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(face_value_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_value(face_value_per_cell, face_value_per_cell_ref));
          synchronizer.synchronize(face_value_per_cell);
          REQUIRE(is_same_item_value(face_value_per_cell, face_value_per_cell_ref));
        }
      }

      SECTION("synchonize NodeValuePerFace")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        NodeValuePerFace<int> node_value_per_face_ref{connectivity};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < node_value_per_face_ref.numberOfSubValues(face_id); ++j) {
              node_value_per_face_ref(face_id, j) = face_owner[face_id] + face_number[face_id] + j;
            }
          });

        NodeValuePerFace<int> node_value_per_face{connectivity};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < node_value_per_face_ref.numberOfSubValues(face_id); ++j) {
              node_value_per_face(face_id, j) = parallel::rank() + face_number[face_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(node_value_per_face, node_value_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_value_per_face);

        REQUIRE(is_same_item_value(node_value_per_face, node_value_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(node_value_per_face, face_owner, 0);
          REQUIRE(not is_same_item_value(node_value_per_face, node_value_per_face_ref));
          synchronizer.synchronize(node_value_per_face);
          REQUIRE(is_same_item_value(node_value_per_face, node_value_per_face_ref));
        }
      }

      SECTION("synchonize NodeValuePerEdge")
      {
        const auto edge_owner  = connectivity.edgeOwner();
        const auto edge_number = connectivity.edgeNumber();

        NodeValuePerEdge<int> node_value_per_edge_ref{connectivity};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < node_value_per_edge_ref.numberOfSubValues(edge_id); ++j) {
              node_value_per_edge_ref(edge_id, j) = edge_owner[edge_id] + edge_number[edge_id] + j;
            }
          });

        NodeValuePerEdge<int> node_value_per_edge{connectivity};
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < node_value_per_edge_ref.numberOfSubValues(edge_id); ++j) {
              node_value_per_edge(edge_id, j) = parallel::rank() + edge_number[edge_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(node_value_per_edge, node_value_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_value_per_edge);

        REQUIRE(is_same_item_value(node_value_per_edge, node_value_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(node_value_per_edge, edge_owner, 0);
          REQUIRE(not is_same_item_value(node_value_per_edge, node_value_per_edge_ref));
          synchronizer.synchronize(node_value_per_edge);
          REQUIRE(is_same_item_value(node_value_per_edge, node_value_per_edge_ref));
        }
      }

      SECTION("synchonize CellValuePerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto node_to_cell_matrix = connectivity.nodeToCellMatrix();

        CellValuePerNode<int> cell_value_per_node_ref{connectivity};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < cell_value_per_node_ref.numberOfSubValues(node_id); ++j) {
              cell_value_per_node_ref(node_id, j) =   // node_owner[node_id] +
                node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]];
            }
          });

        CellValuePerNode<int> cell_value_per_node{connectivity};
        cell_value_per_node.fill(0);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < cell_value_per_node.numberOfSubValues(node_id); ++j) {
              if (node_is_owned[node_id]) {
                cell_value_per_node(node_id, j) =
                  node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value_per_node, cell_value_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value_per_node);
        REQUIRE(is_same_item_value(cell_value_per_node, cell_value_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(cell_value_per_node, connectivity.nodeOwner(), 0);
          REQUIRE(not is_same_item_value(cell_value_per_node, cell_value_per_node_ref));
          synchronizer.synchronize(cell_value_per_node);
          REQUIRE(is_same_item_value(cell_value_per_node, cell_value_per_node_ref));
        }
      }

      SECTION("synchonize CellValuePerEdge")
      {
        const auto edge_is_owned = connectivity.edgeIsOwned();
        const auto edge_number   = connectivity.edgeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();

        CellValuePerEdge<int> cell_value_per_edge_ref{connectivity};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < cell_value_per_edge_ref.numberOfSubValues(edge_id); ++j) {
              cell_value_per_edge_ref(edge_id, j) =   // edge_owner[edge_id] +
                edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]];
            }
          });

        CellValuePerEdge<int> cell_value_per_edge{connectivity};
        cell_value_per_edge.fill(0);
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < cell_value_per_edge.numberOfSubValues(edge_id); ++j) {
              if (edge_is_owned[edge_id]) {
                cell_value_per_edge(edge_id, j) =
                  edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value_per_edge);
        REQUIRE(is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(cell_value_per_edge, connectivity.edgeOwner(), 0);
          REQUIRE(not is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));
          synchronizer.synchronize(cell_value_per_edge);
          REQUIRE(is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));
        }
      }

      SECTION("synchonize CellValuePerFace")
      {
        const auto face_is_owned = connectivity.faceIsOwned();
        const auto face_number   = connectivity.faceNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto face_to_cell_matrix = connectivity.faceToCellMatrix();

        CellValuePerFace<int> cell_value_per_face_ref{connectivity};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < cell_value_per_face_ref.numberOfSubValues(face_id); ++j) {
              cell_value_per_face_ref(face_id, j) =   // face_owner[face_id] +
                face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]];
            }
          });

        CellValuePerFace<int> cell_value_per_face{connectivity};
        cell_value_per_face.fill(0);
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < cell_value_per_face.numberOfSubValues(face_id); ++j) {
              if (face_is_owned[face_id]) {
                cell_value_per_face(face_id, j) =
                  face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value_per_face, cell_value_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value_per_face);
        REQUIRE(is_same_item_value(cell_value_per_face, cell_value_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(cell_value_per_face, connectivity.faceOwner(), 0);
          REQUIRE(not is_same_item_value(cell_value_per_face, cell_value_per_face_ref));
          synchronizer.synchronize(cell_value_per_face);
          REQUIRE(is_same_item_value(cell_value_per_face, cell_value_per_face_ref));
        }
      }

      SECTION("synchonize FaceValuePerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto face_number   = connectivity.faceNumber();

        const auto node_to_face_matrix = connectivity.nodeToFaceMatrix();

        FaceValuePerNode<int> face_value_per_node_ref{connectivity};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < face_value_per_node_ref.numberOfSubValues(node_id); ++j) {
              face_value_per_node_ref(node_id, j) =   // node_owner[node_id] +
                node_number[node_id] + 100 * face_number[node_to_face_matrix[node_id][j]];
            }
          });

        FaceValuePerNode<int> face_value_per_node{connectivity};
        face_value_per_node.fill(0);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < face_value_per_node.numberOfSubValues(node_id); ++j) {
              if (node_is_owned[node_id]) {
                face_value_per_node(node_id, j) =
                  node_number[node_id] + 100 * face_number[node_to_face_matrix[node_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(face_value_per_node, face_value_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_value_per_node);
        REQUIRE(is_same_item_value(face_value_per_node, face_value_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(face_value_per_node, connectivity.nodeOwner(), 0);
          REQUIRE(not is_same_item_value(face_value_per_node, face_value_per_node_ref));
          synchronizer.synchronize(face_value_per_node);
          REQUIRE(is_same_item_value(face_value_per_node, face_value_per_node_ref));
        }
      }

      SECTION("synchonize EdgeValuePerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto edge_number   = connectivity.edgeNumber();

        const auto node_to_edge_matrix = connectivity.nodeToEdgeMatrix();

        EdgeValuePerNode<int> edge_value_per_node_ref{connectivity};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < edge_value_per_node_ref.numberOfSubValues(node_id); ++j) {
              edge_value_per_node_ref(node_id, j) =   // node_owner[node_id] +
                node_number[node_id] + 100 * edge_number[node_to_edge_matrix[node_id][j]];
            }
          });

        EdgeValuePerNode<int> edge_value_per_node{connectivity};
        edge_value_per_node.fill(0);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < edge_value_per_node.numberOfSubValues(node_id); ++j) {
              if (node_is_owned[node_id]) {
                edge_value_per_node(node_id, j) =
                  node_number[node_id] + 100 * edge_number[node_to_edge_matrix[node_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(edge_value_per_node, edge_value_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_value_per_node);
        REQUIRE(is_same_item_value(edge_value_per_node, edge_value_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(edge_value_per_node, connectivity.nodeOwner(), 0);
          REQUIRE(not is_same_item_value(edge_value_per_node, edge_value_per_node_ref));
          synchronizer.synchronize(edge_value_per_node);
          REQUIRE(is_same_item_value(edge_value_per_node, edge_value_per_node_ref));
        }
      }
    }

    SECTION("3D")
    {
      constexpr size_t Dimension = 3;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity = MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>()->connectivity();

      SECTION("synchonize NodeValuePerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        NodeValuePerCell<int> node_value_per_cell_ref{connectivity};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              node_value_per_cell_ref(cell_id, j) = cell_owner[cell_id] + cell_number[cell_id] + j;
            }
          });

        NodeValuePerCell<int> node_value_per_cell{connectivity};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              node_value_per_cell(cell_id, j) = parallel::rank() + cell_number[cell_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(node_value_per_cell, node_value_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_value_per_cell);

        REQUIRE(is_same_item_value(node_value_per_cell, node_value_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(node_value_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_value(node_value_per_cell, node_value_per_cell_ref));
          synchronizer.synchronize(node_value_per_cell);
          REQUIRE(is_same_item_value(node_value_per_cell, node_value_per_cell_ref));
        }
      }

      SECTION("synchonize EdgeValuePerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        EdgeValuePerCell<int> edge_value_per_cell_ref{connectivity};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              edge_value_per_cell_ref(cell_id, j) = cell_owner[cell_id] + cell_number[cell_id] + j;
            }
          });

        EdgeValuePerCell<int> edge_value_per_cell{connectivity};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              edge_value_per_cell(cell_id, j) = parallel::rank() + cell_number[cell_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_value_per_cell);

        REQUIRE(is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(edge_value_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));
          synchronizer.synchronize(edge_value_per_cell);
          REQUIRE(is_same_item_value(edge_value_per_cell, edge_value_per_cell_ref));
        }
      }

      SECTION("synchonize FaceValuePerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        FaceValuePerCell<int> face_value_per_cell_ref{connectivity};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              face_value_per_cell_ref(cell_id, j) = cell_owner[cell_id] + cell_number[cell_id] + j;
            }
          });

        FaceValuePerCell<int> face_value_per_cell{connectivity};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_value_per_cell_ref.numberOfSubValues(cell_id); ++j) {
              face_value_per_cell(cell_id, j) = parallel::rank() + cell_number[cell_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(face_value_per_cell, face_value_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_value_per_cell);

        REQUIRE(is_same_item_value(face_value_per_cell, face_value_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(face_value_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_value(face_value_per_cell, face_value_per_cell_ref));
          synchronizer.synchronize(face_value_per_cell);
          REQUIRE(is_same_item_value(face_value_per_cell, face_value_per_cell_ref));
        }
      }

      SECTION("synchonize NodeValuePerFace")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        NodeValuePerFace<int> node_value_per_face_ref{connectivity};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < node_value_per_face_ref.numberOfSubValues(face_id); ++j) {
              node_value_per_face_ref(face_id, j) = face_owner[face_id] + face_number[face_id] + j;
            }
          });

        NodeValuePerFace<int> node_value_per_face{connectivity};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < node_value_per_face_ref.numberOfSubValues(face_id); ++j) {
              node_value_per_face(face_id, j) = parallel::rank() + face_number[face_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(node_value_per_face, node_value_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_value_per_face);

        REQUIRE(is_same_item_value(node_value_per_face, node_value_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(node_value_per_face, face_owner, 0);
          REQUIRE(not is_same_item_value(node_value_per_face, node_value_per_face_ref));
          synchronizer.synchronize(node_value_per_face);
          REQUIRE(is_same_item_value(node_value_per_face, node_value_per_face_ref));
        }
      }

      SECTION("synchonize EdgeValuePerFace")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        EdgeValuePerFace<int> edge_value_per_face_ref{connectivity};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < edge_value_per_face_ref.numberOfSubValues(face_id); ++j) {
              edge_value_per_face_ref(face_id, j) = face_owner[face_id] + face_number[face_id] + j;
            }
          });

        EdgeValuePerFace<int> edge_value_per_face{connectivity};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < edge_value_per_face_ref.numberOfSubValues(face_id); ++j) {
              edge_value_per_face(face_id, j) = parallel::rank() + face_number[face_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(edge_value_per_face, edge_value_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_value_per_face);

        REQUIRE(is_same_item_value(edge_value_per_face, edge_value_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(edge_value_per_face, face_owner, 0);
          REQUIRE(not is_same_item_value(edge_value_per_face, edge_value_per_face_ref));
          synchronizer.synchronize(edge_value_per_face);
          REQUIRE(is_same_item_value(edge_value_per_face, edge_value_per_face_ref));
        }
      }

      SECTION("synchonize NodeValuePerEdge")
      {
        const auto edge_owner  = connectivity.edgeOwner();
        const auto edge_number = connectivity.edgeNumber();

        NodeValuePerEdge<int> node_value_per_edge_ref{connectivity};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < node_value_per_edge_ref.numberOfSubValues(edge_id); ++j) {
              node_value_per_edge_ref(edge_id, j) = edge_owner[edge_id] + edge_number[edge_id] + j;
            }
          });

        NodeValuePerEdge<int> node_value_per_edge{connectivity};
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < node_value_per_edge_ref.numberOfSubValues(edge_id); ++j) {
              node_value_per_edge(edge_id, j) = parallel::rank() + edge_number[edge_id] + j;
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(node_value_per_edge, node_value_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_value_per_edge);

        REQUIRE(is_same_item_value(node_value_per_edge, node_value_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(node_value_per_edge, edge_owner, 0);
          REQUIRE(not is_same_item_value(node_value_per_edge, node_value_per_edge_ref));
          synchronizer.synchronize(node_value_per_edge);
          REQUIRE(is_same_item_value(node_value_per_edge, node_value_per_edge_ref));
        }
      }

      SECTION("synchonize CellValuePerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto node_to_cell_matrix = connectivity.nodeToCellMatrix();

        CellValuePerNode<int> cell_value_per_node_ref{connectivity};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < cell_value_per_node_ref.numberOfSubValues(node_id); ++j) {
              cell_value_per_node_ref(node_id, j) =   // node_owner[node_id] +
                node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]];
            }
          });

        CellValuePerNode<int> cell_value_per_node{connectivity};
        cell_value_per_node.fill(0);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < cell_value_per_node.numberOfSubValues(node_id); ++j) {
              if (node_is_owned[node_id]) {
                cell_value_per_node(node_id, j) =
                  node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value_per_node, cell_value_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value_per_node);
        REQUIRE(is_same_item_value(cell_value_per_node, cell_value_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(cell_value_per_node, connectivity.nodeOwner(), 0);
          REQUIRE(not is_same_item_value(cell_value_per_node, cell_value_per_node_ref));
          synchronizer.synchronize(cell_value_per_node);
          REQUIRE(is_same_item_value(cell_value_per_node, cell_value_per_node_ref));
        }
      }

      SECTION("synchonize CellValuePerEdge")
      {
        const auto edge_is_owned = connectivity.edgeIsOwned();
        const auto edge_number   = connectivity.edgeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();

        CellValuePerEdge<int> cell_value_per_edge_ref{connectivity};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < cell_value_per_edge_ref.numberOfSubValues(edge_id); ++j) {
              cell_value_per_edge_ref(edge_id, j) =   // edge_owner[edge_id] +
                edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]];
            }
          });

        CellValuePerEdge<int> cell_value_per_edge{connectivity};
        cell_value_per_edge.fill(0);
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < cell_value_per_edge.numberOfSubValues(edge_id); ++j) {
              if (edge_is_owned[edge_id]) {
                cell_value_per_edge(edge_id, j) =
                  edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value_per_edge);
        REQUIRE(is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(cell_value_per_edge, connectivity.edgeOwner(), 0);
          REQUIRE(not is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));
          synchronizer.synchronize(cell_value_per_edge);
          REQUIRE(is_same_item_value(cell_value_per_edge, cell_value_per_edge_ref));
        }
      }

      SECTION("synchonize CellValuePerFace")
      {
        const auto face_is_owned = connectivity.faceIsOwned();
        const auto face_number   = connectivity.faceNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto face_to_cell_matrix = connectivity.faceToCellMatrix();

        CellValuePerFace<int> cell_value_per_face_ref{connectivity};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < cell_value_per_face_ref.numberOfSubValues(face_id); ++j) {
              cell_value_per_face_ref(face_id, j) =   // face_owner[face_id] +
                face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]];
            }
          });

        CellValuePerFace<int> cell_value_per_face{connectivity};
        cell_value_per_face.fill(0);
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < cell_value_per_face.numberOfSubValues(face_id); ++j) {
              if (face_is_owned[face_id]) {
                cell_value_per_face(face_id, j) =
                  face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(cell_value_per_face, cell_value_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_value_per_face);
        REQUIRE(is_same_item_value(cell_value_per_face, cell_value_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(cell_value_per_face, connectivity.faceOwner(), 0);
          REQUIRE(not is_same_item_value(cell_value_per_face, cell_value_per_face_ref));
          synchronizer.synchronize(cell_value_per_face);
          REQUIRE(is_same_item_value(cell_value_per_face, cell_value_per_face_ref));
        }
      }

      SECTION("synchonize FaceValuePerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto face_number   = connectivity.faceNumber();

        const auto node_to_face_matrix = connectivity.nodeToFaceMatrix();

        FaceValuePerNode<int> face_value_per_node_ref{connectivity};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < face_value_per_node_ref.numberOfSubValues(node_id); ++j) {
              face_value_per_node_ref(node_id, j) =   // node_owner[node_id] +
                node_number[node_id] + 100 * face_number[node_to_face_matrix[node_id][j]];
            }
          });

        FaceValuePerNode<int> face_value_per_node{connectivity};
        face_value_per_node.fill(0);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < face_value_per_node.numberOfSubValues(node_id); ++j) {
              if (node_is_owned[node_id]) {
                face_value_per_node(node_id, j) =
                  node_number[node_id] + 100 * face_number[node_to_face_matrix[node_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(face_value_per_node, face_value_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_value_per_node);
        REQUIRE(is_same_item_value(face_value_per_node, face_value_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(face_value_per_node, connectivity.nodeOwner(), 0);
          REQUIRE(not is_same_item_value(face_value_per_node, face_value_per_node_ref));
          synchronizer.synchronize(face_value_per_node);
          REQUIRE(is_same_item_value(face_value_per_node, face_value_per_node_ref));
        }
      }

      SECTION("synchonize FaceValuePerEdge")
      {
        const auto edge_is_owned = connectivity.edgeIsOwned();
        const auto edge_number   = connectivity.edgeNumber();
        const auto face_number   = connectivity.faceNumber();

        const auto edge_to_face_matrix = connectivity.edgeToFaceMatrix();

        FaceValuePerEdge<int> face_value_per_edge_ref{connectivity};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < face_value_per_edge_ref.numberOfSubValues(edge_id); ++j) {
              face_value_per_edge_ref(edge_id, j) =   // edge_owner[edge_id] +
                edge_number[edge_id] + 100 * face_number[edge_to_face_matrix[edge_id][j]];
            }
          });

        FaceValuePerEdge<int> face_value_per_edge{connectivity};
        face_value_per_edge.fill(0);
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < face_value_per_edge.numberOfSubValues(edge_id); ++j) {
              if (edge_is_owned[edge_id]) {
                face_value_per_edge(edge_id, j) =
                  edge_number[edge_id] + 100 * face_number[edge_to_face_matrix[edge_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(face_value_per_edge, face_value_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_value_per_edge);
        REQUIRE(is_same_item_value(face_value_per_edge, face_value_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(face_value_per_edge, connectivity.edgeOwner(), 0);
          REQUIRE(not is_same_item_value(face_value_per_edge, face_value_per_edge_ref));
          synchronizer.synchronize(face_value_per_edge);
          REQUIRE(is_same_item_value(face_value_per_edge, face_value_per_edge_ref));
        }
      }

      SECTION("synchonize EdgeValuePerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto edge_number   = connectivity.edgeNumber();

        const auto node_to_edge_matrix = connectivity.nodeToEdgeMatrix();

        EdgeValuePerNode<int> edge_value_per_node_ref{connectivity};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < edge_value_per_node_ref.numberOfSubValues(node_id); ++j) {
              edge_value_per_node_ref(node_id, j) =   // node_owner[node_id] +
                node_number[node_id] + 100 * edge_number[node_to_edge_matrix[node_id][j]];
            }
          });

        EdgeValuePerNode<int> edge_value_per_node{connectivity};
        edge_value_per_node.fill(0);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < edge_value_per_node.numberOfSubValues(node_id); ++j) {
              if (node_is_owned[node_id]) {
                edge_value_per_node(node_id, j) =
                  node_number[node_id] + 100 * edge_number[node_to_edge_matrix[node_id][j]];
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_value(edge_value_per_node, edge_value_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_value_per_node);
        REQUIRE(is_same_item_value(edge_value_per_node, edge_value_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_values(edge_value_per_node, connectivity.nodeOwner(), 0);
          REQUIRE(not is_same_item_value(edge_value_per_node, edge_value_per_node_ref));
          synchronizer.synchronize(edge_value_per_node);
          REQUIRE(is_same_item_value(edge_value_per_node, edge_value_per_node_ref));
        }
      }
    }
  }

  SECTION("SubItemArrayPerItem")
  {
    auto is_same_item_array = [](auto a, auto b) {
      using IndexT = typename decltype(a)::index_type;
      bool is_same = true;
      for (IndexT i_item = 0; i_item < a.numberOfItems(); ++i_item) {
        for (size_t l = 0; l < a.numberOfSubArrays(i_item); ++l) {
          for (size_t k = 0; k < a.sizeOfArrays(); ++k) {
            is_same &= (a(i_item, l)[k] == b(i_item, l)[k]);

            if (a(i_item, l)[k] != b(i_item, l)[k]) {
              std::cout << i_item << ":" << l << " a[" << k << "]=" << a(i_item, l)[k] << " b[" << k
                        << "]=" << b(i_item, l)[k] << '\n';
            }
          }
        }
      }
      return parallel::allReduceAnd(is_same);
    };

    auto reset_ghost_arrays = [](auto sub_item_array_per_item, auto item_owner, auto value) {
      using IndexT = typename decltype(sub_item_array_per_item)::index_type;
      static_assert(std::is_same_v<typename decltype(sub_item_array_per_item)::index_type,
                                   typename decltype(item_owner)::index_type>);
      for (IndexT i_item = 0; i_item < sub_item_array_per_item.numberOfItems(); ++i_item) {
        if (item_owner[i_item] != static_cast<int>(parallel::rank())) {
          for (size_t l = 0; l < sub_item_array_per_item.numberOfSubArrays(i_item); ++l) {
            sub_item_array_per_item(i_item, l).fill(value);
          }
        }
      }
    };

    SECTION("1D")
    {
      constexpr size_t Dimension = 1;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity =
        MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>()->connectivity();

      SECTION("synchonize NodeArrayPerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        NodeArrayPerCell<int> node_array_per_cell_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < node_array_per_cell_ref.sizeOfArrays(); ++k) {
                node_array_per_cell_ref(cell_id, j)[k] = cell_owner[cell_id] + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        NodeArrayPerCell<int> node_array_per_cell{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < node_array_per_cell_ref.sizeOfArrays(); ++k) {
                node_array_per_cell(cell_id, j)[k] = parallel::rank() + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(node_array_per_cell, node_array_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_array_per_cell);

        REQUIRE(is_same_item_array(node_array_per_cell, node_array_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(node_array_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_array(node_array_per_cell, node_array_per_cell_ref));
          synchronizer.synchronize(node_array_per_cell);
          REQUIRE(is_same_item_array(node_array_per_cell, node_array_per_cell_ref));
        }
      }

      SECTION("synchonize EdgeArrayPerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        EdgeArrayPerCell<int> edge_array_per_cell_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < edge_array_per_cell_ref.sizeOfArrays(); ++k) {
                edge_array_per_cell_ref(cell_id, j)[k] = cell_owner[cell_id] + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        EdgeArrayPerCell<int> edge_array_per_cell{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < edge_array_per_cell_ref.sizeOfArrays(); ++k) {
                edge_array_per_cell(cell_id, j)[k] = parallel::rank() + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_array_per_cell);

        REQUIRE(is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(edge_array_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));
          synchronizer.synchronize(edge_array_per_cell);
          REQUIRE(is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));
        }
      }

      SECTION("synchonize FaceArrayPerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        FaceArrayPerCell<int> face_array_per_cell_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < face_array_per_cell_ref.sizeOfArrays(); ++k) {
                face_array_per_cell_ref(cell_id, j)[k] = cell_owner[cell_id] + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        FaceArrayPerCell<int> face_array_per_cell{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < face_array_per_cell_ref.sizeOfArrays(); ++k) {
                face_array_per_cell(cell_id, j)[k] = parallel::rank() + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(face_array_per_cell, face_array_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_array_per_cell);

        REQUIRE(is_same_item_array(face_array_per_cell, face_array_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(face_array_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_array(face_array_per_cell, face_array_per_cell_ref));
          synchronizer.synchronize(face_array_per_cell);
          REQUIRE(is_same_item_array(face_array_per_cell, face_array_per_cell_ref));
        }
      }

      SECTION("synchonize CellArrayPerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto node_to_cell_matrix = connectivity.nodeToCellMatrix();

        CellArrayPerNode<int> cell_array_per_node_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < cell_array_per_node_ref.numberOfSubArrays(node_id); ++j) {
              for (size_t k = 0; k < cell_array_per_node_ref.sizeOfArrays(); ++k) {
                cell_array_per_node_ref(node_id, j)[k] =
                  node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]] + 2 * k;
              }
            }
          });

        CellArrayPerNode<int> cell_array_per_node{connectivity, 3};
        cell_array_per_node.fill(-1);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            if (node_is_owned[node_id]) {
              for (size_t j = 0; j < cell_array_per_node.numberOfSubArrays(node_id); ++j) {
                for (size_t k = 0; k < cell_array_per_node.sizeOfArrays(); ++k) {
                  cell_array_per_node(node_id, j)[k] =
                    node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array_per_node, cell_array_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array_per_node);

        REQUIRE(is_same_item_array(cell_array_per_node, cell_array_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& node_owner = connectivity.nodeOwner();
          reset_ghost_arrays(cell_array_per_node, node_owner, 0);
          REQUIRE(not is_same_item_array(cell_array_per_node, cell_array_per_node_ref));
          synchronizer.synchronize(cell_array_per_node);
          REQUIRE(is_same_item_array(cell_array_per_node, cell_array_per_node_ref));
        }
      }

      SECTION("synchonize CellArrayPerEdge")
      {
        const auto edge_is_owned = connectivity.edgeIsOwned();
        const auto edge_number   = connectivity.edgeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();

        CellArrayPerEdge<int> cell_array_per_edge_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < cell_array_per_edge_ref.numberOfSubArrays(edge_id); ++j) {
              for (size_t k = 0; k < cell_array_per_edge_ref.sizeOfArrays(); ++k) {
                cell_array_per_edge_ref(edge_id, j)[k] =
                  edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]] + 2 * k;
              }
            }
          });

        CellArrayPerEdge<int> cell_array_per_edge{connectivity, 3};
        cell_array_per_edge.fill(-1);
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            if (edge_is_owned[edge_id]) {
              for (size_t j = 0; j < cell_array_per_edge.numberOfSubArrays(edge_id); ++j) {
                for (size_t k = 0; k < cell_array_per_edge.sizeOfArrays(); ++k) {
                  cell_array_per_edge(edge_id, j)[k] =
                    edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array_per_edge);

        REQUIRE(is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& edge_owner = connectivity.edgeOwner();
          reset_ghost_arrays(cell_array_per_edge, edge_owner, 0);
          REQUIRE(not is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));
          synchronizer.synchronize(cell_array_per_edge);
          REQUIRE(is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));
        }
      }

      SECTION("synchonize CellArrayPerFace")
      {
        const auto face_is_owned = connectivity.faceIsOwned();
        const auto face_number   = connectivity.faceNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto face_to_cell_matrix = connectivity.faceToCellMatrix();

        CellArrayPerFace<int> cell_array_per_face_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < cell_array_per_face_ref.numberOfSubArrays(face_id); ++j) {
              for (size_t k = 0; k < cell_array_per_face_ref.sizeOfArrays(); ++k) {
                cell_array_per_face_ref(face_id, j)[k] =
                  face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]] + 2 * k;
              }
            }
          });

        CellArrayPerFace<int> cell_array_per_face{connectivity, 3};
        cell_array_per_face.fill(-1);
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            if (face_is_owned[face_id]) {
              for (size_t j = 0; j < cell_array_per_face.numberOfSubArrays(face_id); ++j) {
                for (size_t k = 0; k < cell_array_per_face.sizeOfArrays(); ++k) {
                  cell_array_per_face(face_id, j)[k] =
                    face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array_per_face, cell_array_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array_per_face);

        REQUIRE(is_same_item_array(cell_array_per_face, cell_array_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& face_owner = connectivity.faceOwner();
          reset_ghost_arrays(cell_array_per_face, face_owner, 0);
          REQUIRE(not is_same_item_array(cell_array_per_face, cell_array_per_face_ref));
          synchronizer.synchronize(cell_array_per_face);
          REQUIRE(is_same_item_array(cell_array_per_face, cell_array_per_face_ref));
        }
      }
    }

    SECTION("2D")
    {
      constexpr size_t Dimension = 2;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>()->connectivity();

      SECTION("synchonize NodeArrayPerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        NodeArrayPerCell<int> node_array_per_cell_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < node_array_per_cell_ref.sizeOfArrays(); ++k) {
                node_array_per_cell_ref(cell_id, j)[k] = cell_owner[cell_id] + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        NodeArrayPerCell<int> node_array_per_cell{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < node_array_per_cell_ref.sizeOfArrays(); ++k) {
                node_array_per_cell(cell_id, j)[k] = parallel::rank() + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(node_array_per_cell, node_array_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_array_per_cell);

        REQUIRE(is_same_item_array(node_array_per_cell, node_array_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(node_array_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_array(node_array_per_cell, node_array_per_cell_ref));
          synchronizer.synchronize(node_array_per_cell);
          REQUIRE(is_same_item_array(node_array_per_cell, node_array_per_cell_ref));
        }
      }

      SECTION("synchonize EdgeArrayPerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        EdgeArrayPerCell<int> edge_array_per_cell_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < edge_array_per_cell_ref.sizeOfArrays(); ++k) {
                edge_array_per_cell_ref(cell_id, j)[k] = cell_owner[cell_id] + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        EdgeArrayPerCell<int> edge_array_per_cell{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < edge_array_per_cell_ref.sizeOfArrays(); ++k) {
                edge_array_per_cell(cell_id, j)[k] = parallel::rank() + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_array_per_cell);

        REQUIRE(is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(edge_array_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));
          synchronizer.synchronize(edge_array_per_cell);
          REQUIRE(is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));
        }
      }

      SECTION("synchonize FaceArrayPerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        FaceArrayPerCell<int> face_array_per_cell_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < face_array_per_cell_ref.sizeOfArrays(); ++k) {
                face_array_per_cell_ref(cell_id, j)[k] = cell_owner[cell_id] + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        FaceArrayPerCell<int> face_array_per_cell{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < face_array_per_cell_ref.sizeOfArrays(); ++k) {
                face_array_per_cell(cell_id, j)[k] = parallel::rank() + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(face_array_per_cell, face_array_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_array_per_cell);

        REQUIRE(is_same_item_array(face_array_per_cell, face_array_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(face_array_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_array(face_array_per_cell, face_array_per_cell_ref));
          synchronizer.synchronize(face_array_per_cell);
          REQUIRE(is_same_item_array(face_array_per_cell, face_array_per_cell_ref));
        }
      }

      SECTION("synchonize NodeArrayPerFace")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        NodeArrayPerFace<int> node_array_per_face_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < node_array_per_face_ref.numberOfSubArrays(face_id); ++j) {
              for (size_t k = 0; k < node_array_per_face_ref.sizeOfArrays(); ++k) {
                node_array_per_face_ref(face_id, j)[k] = face_owner[face_id] + face_number[face_id] + j + 2 * k;
              }
            }
          });

        NodeArrayPerFace<int> node_array_per_face{connectivity, 3};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < node_array_per_face_ref.numberOfSubArrays(face_id); ++j) {
              for (size_t k = 0; k < node_array_per_face_ref.sizeOfArrays(); ++k) {
                node_array_per_face(face_id, j)[k] = parallel::rank() + face_number[face_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(node_array_per_face, node_array_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_array_per_face);

        REQUIRE(is_same_item_array(node_array_per_face, node_array_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(node_array_per_face, face_owner, 0);
          REQUIRE(not is_same_item_array(node_array_per_face, node_array_per_face_ref));
          synchronizer.synchronize(node_array_per_face);
          REQUIRE(is_same_item_array(node_array_per_face, node_array_per_face_ref));
        }
      }

      SECTION("synchonize NodeArrayPerEdge")
      {
        const auto edge_owner  = connectivity.edgeOwner();
        const auto edge_number = connectivity.edgeNumber();

        NodeArrayPerEdge<int> node_array_per_edge_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < node_array_per_edge_ref.numberOfSubArrays(edge_id); ++j) {
              for (size_t k = 0; k < node_array_per_edge_ref.sizeOfArrays(); ++k) {
                node_array_per_edge_ref(edge_id, j)[k] = edge_owner[edge_id] + edge_number[edge_id] + j + 2 * k;
              }
            }
          });

        NodeArrayPerEdge<int> node_array_per_edge{connectivity, 3};
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < node_array_per_edge_ref.numberOfSubArrays(edge_id); ++j) {
              for (size_t k = 0; k < node_array_per_edge_ref.sizeOfArrays(); ++k) {
                node_array_per_edge(edge_id, j)[k] = parallel::rank() + edge_number[edge_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(node_array_per_edge, node_array_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_array_per_edge);

        REQUIRE(is_same_item_array(node_array_per_edge, node_array_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(node_array_per_edge, edge_owner, 0);
          REQUIRE(not is_same_item_array(node_array_per_edge, node_array_per_edge_ref));
          synchronizer.synchronize(node_array_per_edge);
          REQUIRE(is_same_item_array(node_array_per_edge, node_array_per_edge_ref));
        }
      }

      SECTION("synchonize CellArrayPerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto node_to_cell_matrix = connectivity.nodeToCellMatrix();

        CellArrayPerNode<int> cell_array_per_node_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < cell_array_per_node_ref.numberOfSubArrays(node_id); ++j) {
              for (size_t k = 0; k < cell_array_per_node_ref.sizeOfArrays(); ++k) {
                cell_array_per_node_ref(node_id, j)[k] =
                  node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]] + 2 * k;
              }
            }
          });

        CellArrayPerNode<int> cell_array_per_node{connectivity, 3};
        cell_array_per_node.fill(-1);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            if (node_is_owned[node_id]) {
              for (size_t j = 0; j < cell_array_per_node.numberOfSubArrays(node_id); ++j) {
                for (size_t k = 0; k < cell_array_per_node.sizeOfArrays(); ++k) {
                  cell_array_per_node(node_id, j)[k] =
                    node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array_per_node, cell_array_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array_per_node);

        REQUIRE(is_same_item_array(cell_array_per_node, cell_array_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& node_owner = connectivity.nodeOwner();
          reset_ghost_arrays(cell_array_per_node, node_owner, 0);
          REQUIRE(not is_same_item_array(cell_array_per_node, cell_array_per_node_ref));
          synchronizer.synchronize(cell_array_per_node);
          REQUIRE(is_same_item_array(cell_array_per_node, cell_array_per_node_ref));
        }
      }

      SECTION("synchonize CellArrayPerEdge")
      {
        const auto edge_is_owned = connectivity.edgeIsOwned();
        const auto edge_number   = connectivity.edgeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();

        CellArrayPerEdge<int> cell_array_per_edge_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < cell_array_per_edge_ref.numberOfSubArrays(edge_id); ++j) {
              for (size_t k = 0; k < cell_array_per_edge_ref.sizeOfArrays(); ++k) {
                cell_array_per_edge_ref(edge_id, j)[k] =
                  edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]] + 2 * k;
              }
            }
          });

        CellArrayPerEdge<int> cell_array_per_edge{connectivity, 3};
        cell_array_per_edge.fill(-1);
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            if (edge_is_owned[edge_id]) {
              for (size_t j = 0; j < cell_array_per_edge.numberOfSubArrays(edge_id); ++j) {
                for (size_t k = 0; k < cell_array_per_edge.sizeOfArrays(); ++k) {
                  cell_array_per_edge(edge_id, j)[k] =
                    edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array_per_edge);

        REQUIRE(is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& edge_owner = connectivity.edgeOwner();
          reset_ghost_arrays(cell_array_per_edge, edge_owner, 0);
          REQUIRE(not is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));
          synchronizer.synchronize(cell_array_per_edge);
          REQUIRE(is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));
        }
      }

      SECTION("synchonize CellArrayPerFace")
      {
        const auto face_is_owned = connectivity.faceIsOwned();
        const auto face_number   = connectivity.faceNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto face_to_cell_matrix = connectivity.faceToCellMatrix();

        CellArrayPerFace<int> cell_array_per_face_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < cell_array_per_face_ref.numberOfSubArrays(face_id); ++j) {
              for (size_t k = 0; k < cell_array_per_face_ref.sizeOfArrays(); ++k) {
                cell_array_per_face_ref(face_id, j)[k] =
                  face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]] + 2 * k;
              }
            }
          });

        CellArrayPerFace<int> cell_array_per_face{connectivity, 3};
        cell_array_per_face.fill(-1);
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            if (face_is_owned[face_id]) {
              for (size_t j = 0; j < cell_array_per_face.numberOfSubArrays(face_id); ++j) {
                for (size_t k = 0; k < cell_array_per_face.sizeOfArrays(); ++k) {
                  cell_array_per_face(face_id, j)[k] =
                    face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array_per_face, cell_array_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array_per_face);

        REQUIRE(is_same_item_array(cell_array_per_face, cell_array_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& face_owner = connectivity.faceOwner();
          reset_ghost_arrays(cell_array_per_face, face_owner, 0);
          REQUIRE(not is_same_item_array(cell_array_per_face, cell_array_per_face_ref));
          synchronizer.synchronize(cell_array_per_face);
          REQUIRE(is_same_item_array(cell_array_per_face, cell_array_per_face_ref));
        }
      }

      SECTION("synchonize FaceArrayPerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto face_number   = connectivity.faceNumber();

        const auto node_to_face_matrix = connectivity.nodeToFaceMatrix();

        FaceArrayPerNode<int> face_array_per_node_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < face_array_per_node_ref.numberOfSubArrays(node_id); ++j) {
              for (size_t k = 0; k < face_array_per_node_ref.sizeOfArrays(); ++k) {
                face_array_per_node_ref(node_id, j)[k] =
                  node_number[node_id] + 100 * face_number[node_to_face_matrix[node_id][j]] + 2 * k;
              }
            }
          });

        FaceArrayPerNode<int> face_array_per_node{connectivity, 3};
        face_array_per_node.fill(-1);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            if (node_is_owned[node_id]) {
              for (size_t j = 0; j < face_array_per_node.numberOfSubArrays(node_id); ++j) {
                for (size_t k = 0; k < face_array_per_node.sizeOfArrays(); ++k) {
                  face_array_per_node(node_id, j)[k] =
                    node_number[node_id] + 100 * face_number[node_to_face_matrix[node_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(face_array_per_node, face_array_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_array_per_node);

        REQUIRE(is_same_item_array(face_array_per_node, face_array_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& node_owner = connectivity.nodeOwner();
          reset_ghost_arrays(face_array_per_node, node_owner, 0);
          REQUIRE(not is_same_item_array(face_array_per_node, face_array_per_node_ref));
          synchronizer.synchronize(face_array_per_node);
          REQUIRE(is_same_item_array(face_array_per_node, face_array_per_node_ref));
        }
      }

      SECTION("synchonize EdgeArrayPerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto edge_number   = connectivity.edgeNumber();

        const auto node_to_edge_matrix = connectivity.nodeToEdgeMatrix();

        EdgeArrayPerNode<int> edge_array_per_node_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < edge_array_per_node_ref.numberOfSubArrays(node_id); ++j) {
              for (size_t k = 0; k < edge_array_per_node_ref.sizeOfArrays(); ++k) {
                edge_array_per_node_ref(node_id, j)[k] =
                  node_number[node_id] + 100 * edge_number[node_to_edge_matrix[node_id][j]] + 2 * k;
              }
            }
          });

        EdgeArrayPerNode<int> edge_array_per_node{connectivity, 3};
        edge_array_per_node.fill(-1);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            if (node_is_owned[node_id]) {
              for (size_t j = 0; j < edge_array_per_node.numberOfSubArrays(node_id); ++j) {
                for (size_t k = 0; k < edge_array_per_node.sizeOfArrays(); ++k) {
                  edge_array_per_node(node_id, j)[k] =
                    node_number[node_id] + 100 * edge_number[node_to_edge_matrix[node_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(edge_array_per_node, edge_array_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_array_per_node);

        REQUIRE(is_same_item_array(edge_array_per_node, edge_array_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& node_owner = connectivity.nodeOwner();
          reset_ghost_arrays(edge_array_per_node, node_owner, 0);
          REQUIRE(not is_same_item_array(edge_array_per_node, edge_array_per_node_ref));
          synchronizer.synchronize(edge_array_per_node);
          REQUIRE(is_same_item_array(edge_array_per_node, edge_array_per_node_ref));
        }
      }
    }

    SECTION("3D")
    {
      constexpr size_t Dimension = 3;
      using ConnectivityType     = Connectivity<Dimension>;

      const ConnectivityType& connectivity = MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>()->connectivity();

      SECTION("synchonize NodeArrayPerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        NodeArrayPerCell<int> node_array_per_cell_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < node_array_per_cell_ref.sizeOfArrays(); ++k) {
                node_array_per_cell_ref(cell_id, j)[k] = cell_owner[cell_id] + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        NodeArrayPerCell<int> node_array_per_cell{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < node_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < node_array_per_cell_ref.sizeOfArrays(); ++k) {
                node_array_per_cell(cell_id, j)[k] = parallel::rank() + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(node_array_per_cell, node_array_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_array_per_cell);

        REQUIRE(is_same_item_array(node_array_per_cell, node_array_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(node_array_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_array(node_array_per_cell, node_array_per_cell_ref));
          synchronizer.synchronize(node_array_per_cell);
          REQUIRE(is_same_item_array(node_array_per_cell, node_array_per_cell_ref));
        }
      }

      SECTION("synchonize EdgeArrayPerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        EdgeArrayPerCell<int> edge_array_per_cell_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < edge_array_per_cell_ref.sizeOfArrays(); ++k) {
                edge_array_per_cell_ref(cell_id, j)[k] = cell_owner[cell_id] + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        EdgeArrayPerCell<int> edge_array_per_cell{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < edge_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < edge_array_per_cell_ref.sizeOfArrays(); ++k) {
                edge_array_per_cell(cell_id, j)[k] = parallel::rank() + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_array_per_cell);

        REQUIRE(is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(edge_array_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));
          synchronizer.synchronize(edge_array_per_cell);
          REQUIRE(is_same_item_array(edge_array_per_cell, edge_array_per_cell_ref));
        }
      }

      SECTION("synchonize FaceArrayPerCell")
      {
        const auto cell_owner  = connectivity.cellOwner();
        const auto cell_number = connectivity.cellNumber();

        FaceArrayPerCell<int> face_array_per_cell_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < face_array_per_cell_ref.sizeOfArrays(); ++k) {
                face_array_per_cell_ref(cell_id, j)[k] = cell_owner[cell_id] + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        FaceArrayPerCell<int> face_array_per_cell{connectivity, 3};
        parallel_for(
          connectivity.numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
            for (size_t j = 0; j < face_array_per_cell_ref.numberOfSubArrays(cell_id); ++j) {
              for (size_t k = 0; k < face_array_per_cell_ref.sizeOfArrays(); ++k) {
                face_array_per_cell(cell_id, j)[k] = parallel::rank() + cell_number[cell_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(face_array_per_cell, face_array_per_cell_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_array_per_cell);

        REQUIRE(is_same_item_array(face_array_per_cell, face_array_per_cell_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(face_array_per_cell, cell_owner, 0);
          REQUIRE(not is_same_item_array(face_array_per_cell, face_array_per_cell_ref));
          synchronizer.synchronize(face_array_per_cell);
          REQUIRE(is_same_item_array(face_array_per_cell, face_array_per_cell_ref));
        }
      }

      SECTION("synchonize NodeArrayPerFace")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        NodeArrayPerFace<int> node_array_per_face_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < node_array_per_face_ref.numberOfSubArrays(face_id); ++j) {
              for (size_t k = 0; k < node_array_per_face_ref.sizeOfArrays(); ++k) {
                node_array_per_face_ref(face_id, j)[k] = face_owner[face_id] + face_number[face_id] + j + 2 * k;
              }
            }
          });

        NodeArrayPerFace<int> node_array_per_face{connectivity, 3};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < node_array_per_face_ref.numberOfSubArrays(face_id); ++j) {
              for (size_t k = 0; k < node_array_per_face_ref.sizeOfArrays(); ++k) {
                node_array_per_face(face_id, j)[k] = parallel::rank() + face_number[face_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(node_array_per_face, node_array_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_array_per_face);

        REQUIRE(is_same_item_array(node_array_per_face, node_array_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(node_array_per_face, face_owner, 0);
          REQUIRE(not is_same_item_array(node_array_per_face, node_array_per_face_ref));
          synchronizer.synchronize(node_array_per_face);
          REQUIRE(is_same_item_array(node_array_per_face, node_array_per_face_ref));
        }
      }

      SECTION("synchonize EdgeArrayPerFace")
      {
        const auto face_owner  = connectivity.faceOwner();
        const auto face_number = connectivity.faceNumber();

        EdgeArrayPerFace<int> edge_array_per_face_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < edge_array_per_face_ref.numberOfSubArrays(face_id); ++j) {
              for (size_t k = 0; k < edge_array_per_face_ref.sizeOfArrays(); ++k) {
                edge_array_per_face_ref(face_id, j)[k] = face_owner[face_id] + face_number[face_id] + j + 2 * k;
              }
            }
          });

        EdgeArrayPerFace<int> edge_array_per_face{connectivity, 3};
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < edge_array_per_face_ref.numberOfSubArrays(face_id); ++j) {
              for (size_t k = 0; k < edge_array_per_face_ref.sizeOfArrays(); ++k) {
                edge_array_per_face(face_id, j)[k] = parallel::rank() + face_number[face_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(edge_array_per_face, edge_array_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_array_per_face);

        REQUIRE(is_same_item_array(edge_array_per_face, edge_array_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(edge_array_per_face, face_owner, 0);
          REQUIRE(not is_same_item_array(edge_array_per_face, edge_array_per_face_ref));
          synchronizer.synchronize(edge_array_per_face);
          REQUIRE(is_same_item_array(edge_array_per_face, edge_array_per_face_ref));
        }
      }

      SECTION("synchonize NodeArrayPerEdge")
      {
        const auto edge_owner  = connectivity.edgeOwner();
        const auto edge_number = connectivity.edgeNumber();

        NodeArrayPerEdge<int> node_array_per_edge_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < node_array_per_edge_ref.numberOfSubArrays(edge_id); ++j) {
              for (size_t k = 0; k < node_array_per_edge_ref.sizeOfArrays(); ++k) {
                node_array_per_edge_ref(edge_id, j)[k] = edge_owner[edge_id] + edge_number[edge_id] + j + 2 * k;
              }
            }
          });

        NodeArrayPerEdge<int> node_array_per_edge{connectivity, 3};
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < node_array_per_edge_ref.numberOfSubArrays(edge_id); ++j) {
              for (size_t k = 0; k < node_array_per_edge_ref.sizeOfArrays(); ++k) {
                node_array_per_edge(edge_id, j)[k] = parallel::rank() + edge_number[edge_id] + j + 2 * k;
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(node_array_per_edge, node_array_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(node_array_per_edge);

        REQUIRE(is_same_item_array(node_array_per_edge, node_array_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          reset_ghost_arrays(node_array_per_edge, edge_owner, 0);
          REQUIRE(not is_same_item_array(node_array_per_edge, node_array_per_edge_ref));
          synchronizer.synchronize(node_array_per_edge);
          REQUIRE(is_same_item_array(node_array_per_edge, node_array_per_edge_ref));
        }
      }

      SECTION("synchonize CellArrayPerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto node_to_cell_matrix = connectivity.nodeToCellMatrix();

        CellArrayPerNode<int> cell_array_per_node_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < cell_array_per_node_ref.numberOfSubArrays(node_id); ++j) {
              for (size_t k = 0; k < cell_array_per_node_ref.sizeOfArrays(); ++k) {
                cell_array_per_node_ref(node_id, j)[k] =
                  node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]] + 2 * k;
              }
            }
          });

        CellArrayPerNode<int> cell_array_per_node{connectivity, 3};
        cell_array_per_node.fill(-1);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            if (node_is_owned[node_id]) {
              for (size_t j = 0; j < cell_array_per_node.numberOfSubArrays(node_id); ++j) {
                for (size_t k = 0; k < cell_array_per_node.sizeOfArrays(); ++k) {
                  cell_array_per_node(node_id, j)[k] =
                    node_number[node_id] + 100 * cell_number[node_to_cell_matrix[node_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array_per_node, cell_array_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array_per_node);

        REQUIRE(is_same_item_array(cell_array_per_node, cell_array_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& node_owner = connectivity.nodeOwner();
          reset_ghost_arrays(cell_array_per_node, node_owner, 0);
          REQUIRE(not is_same_item_array(cell_array_per_node, cell_array_per_node_ref));
          synchronizer.synchronize(cell_array_per_node);
          REQUIRE(is_same_item_array(cell_array_per_node, cell_array_per_node_ref));
        }
      }

      SECTION("synchonize CellArrayPerEdge")
      {
        const auto edge_is_owned = connectivity.edgeIsOwned();
        const auto edge_number   = connectivity.edgeNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();

        CellArrayPerEdge<int> cell_array_per_edge_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < cell_array_per_edge_ref.numberOfSubArrays(edge_id); ++j) {
              for (size_t k = 0; k < cell_array_per_edge_ref.sizeOfArrays(); ++k) {
                cell_array_per_edge_ref(edge_id, j)[k] =
                  edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]] + 2 * k;
              }
            }
          });

        CellArrayPerEdge<int> cell_array_per_edge{connectivity, 3};
        cell_array_per_edge.fill(-1);
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            if (edge_is_owned[edge_id]) {
              for (size_t j = 0; j < cell_array_per_edge.numberOfSubArrays(edge_id); ++j) {
                for (size_t k = 0; k < cell_array_per_edge.sizeOfArrays(); ++k) {
                  cell_array_per_edge(edge_id, j)[k] =
                    edge_number[edge_id] + 100 * cell_number[edge_to_cell_matrix[edge_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array_per_edge);

        REQUIRE(is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& edge_owner = connectivity.edgeOwner();
          reset_ghost_arrays(cell_array_per_edge, edge_owner, 0);
          REQUIRE(not is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));
          synchronizer.synchronize(cell_array_per_edge);
          REQUIRE(is_same_item_array(cell_array_per_edge, cell_array_per_edge_ref));
        }
      }

      SECTION("synchonize CellArrayPerFace")
      {
        const auto face_is_owned = connectivity.faceIsOwned();
        const auto face_number   = connectivity.faceNumber();
        const auto cell_number   = connectivity.cellNumber();

        const auto face_to_cell_matrix = connectivity.faceToCellMatrix();

        CellArrayPerFace<int> cell_array_per_face_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            for (size_t j = 0; j < cell_array_per_face_ref.numberOfSubArrays(face_id); ++j) {
              for (size_t k = 0; k < cell_array_per_face_ref.sizeOfArrays(); ++k) {
                cell_array_per_face_ref(face_id, j)[k] =
                  face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]] + 2 * k;
              }
            }
          });

        CellArrayPerFace<int> cell_array_per_face{connectivity, 3};
        cell_array_per_face.fill(-1);
        parallel_for(
          connectivity.numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
            if (face_is_owned[face_id]) {
              for (size_t j = 0; j < cell_array_per_face.numberOfSubArrays(face_id); ++j) {
                for (size_t k = 0; k < cell_array_per_face.sizeOfArrays(); ++k) {
                  cell_array_per_face(face_id, j)[k] =
                    face_number[face_id] + 100 * cell_number[face_to_cell_matrix[face_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(cell_array_per_face, cell_array_per_face_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(cell_array_per_face);

        REQUIRE(is_same_item_array(cell_array_per_face, cell_array_per_face_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& face_owner = connectivity.faceOwner();
          reset_ghost_arrays(cell_array_per_face, face_owner, 0);
          REQUIRE(not is_same_item_array(cell_array_per_face, cell_array_per_face_ref));
          synchronizer.synchronize(cell_array_per_face);
          REQUIRE(is_same_item_array(cell_array_per_face, cell_array_per_face_ref));
        }
      }

      SECTION("synchonize FaceArrayPerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto face_number   = connectivity.faceNumber();

        const auto node_to_face_matrix = connectivity.nodeToFaceMatrix();

        FaceArrayPerNode<int> face_array_per_node_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < face_array_per_node_ref.numberOfSubArrays(node_id); ++j) {
              for (size_t k = 0; k < face_array_per_node_ref.sizeOfArrays(); ++k) {
                face_array_per_node_ref(node_id, j)[k] =
                  node_number[node_id] + 100 * face_number[node_to_face_matrix[node_id][j]] + 2 * k;
              }
            }
          });

        FaceArrayPerNode<int> face_array_per_node{connectivity, 3};
        face_array_per_node.fill(-1);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            if (node_is_owned[node_id]) {
              for (size_t j = 0; j < face_array_per_node.numberOfSubArrays(node_id); ++j) {
                for (size_t k = 0; k < face_array_per_node.sizeOfArrays(); ++k) {
                  face_array_per_node(node_id, j)[k] =
                    node_number[node_id] + 100 * face_number[node_to_face_matrix[node_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(face_array_per_node, face_array_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_array_per_node);

        REQUIRE(is_same_item_array(face_array_per_node, face_array_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& node_owner = connectivity.nodeOwner();
          reset_ghost_arrays(face_array_per_node, node_owner, 0);
          REQUIRE(not is_same_item_array(face_array_per_node, face_array_per_node_ref));
          synchronizer.synchronize(face_array_per_node);
          REQUIRE(is_same_item_array(face_array_per_node, face_array_per_node_ref));
        }
      }

      SECTION("synchonize FaceArrayPerEdge")
      {
        const auto edge_is_owned = connectivity.edgeIsOwned();
        const auto edge_number   = connectivity.edgeNumber();
        const auto face_number   = connectivity.faceNumber();

        const auto edge_to_face_matrix = connectivity.edgeToFaceMatrix();

        FaceArrayPerEdge<int> face_array_per_edge_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            for (size_t j = 0; j < face_array_per_edge_ref.numberOfSubArrays(edge_id); ++j) {
              for (size_t k = 0; k < face_array_per_edge_ref.sizeOfArrays(); ++k) {
                face_array_per_edge_ref(edge_id, j)[k] =
                  edge_number[edge_id] + 100 * face_number[edge_to_face_matrix[edge_id][j]] + 2 * k;
              }
            }
          });

        FaceArrayPerEdge<int> face_array_per_edge{connectivity, 3};
        face_array_per_edge.fill(-1);
        parallel_for(
          connectivity.numberOfEdges(), PUGS_LAMBDA(const EdgeId edge_id) {
            if (edge_is_owned[edge_id]) {
              for (size_t j = 0; j < face_array_per_edge.numberOfSubArrays(edge_id); ++j) {
                for (size_t k = 0; k < face_array_per_edge.sizeOfArrays(); ++k) {
                  face_array_per_edge(edge_id, j)[k] =
                    edge_number[edge_id] + 100 * face_number[edge_to_face_matrix[edge_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(face_array_per_edge, face_array_per_edge_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(face_array_per_edge);

        REQUIRE(is_same_item_array(face_array_per_edge, face_array_per_edge_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& edge_owner = connectivity.edgeOwner();
          reset_ghost_arrays(face_array_per_edge, edge_owner, 0);
          REQUIRE(not is_same_item_array(face_array_per_edge, face_array_per_edge_ref));
          synchronizer.synchronize(face_array_per_edge);
          REQUIRE(is_same_item_array(face_array_per_edge, face_array_per_edge_ref));
        }
      }

      SECTION("synchonize EdgeArrayPerNode")
      {
        const auto node_is_owned = connectivity.nodeIsOwned();
        const auto node_number   = connectivity.nodeNumber();
        const auto edge_number   = connectivity.edgeNumber();

        const auto node_to_edge_matrix = connectivity.nodeToEdgeMatrix();

        EdgeArrayPerNode<int> edge_array_per_node_ref{connectivity, 3};

        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            for (size_t j = 0; j < edge_array_per_node_ref.numberOfSubArrays(node_id); ++j) {
              for (size_t k = 0; k < edge_array_per_node_ref.sizeOfArrays(); ++k) {
                edge_array_per_node_ref(node_id, j)[k] =
                  node_number[node_id] + 100 * edge_number[node_to_edge_matrix[node_id][j]] + 2 * k;
              }
            }
          });

        EdgeArrayPerNode<int> edge_array_per_node{connectivity, 3};
        edge_array_per_node.fill(-1);
        parallel_for(
          connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
            if (node_is_owned[node_id]) {
              for (size_t j = 0; j < edge_array_per_node.numberOfSubArrays(node_id); ++j) {
                for (size_t k = 0; k < edge_array_per_node.sizeOfArrays(); ++k) {
                  edge_array_per_node(node_id, j)[k] =
                    node_number[node_id] + 100 * edge_number[node_to_edge_matrix[node_id][j]] + 2 * k;
                }
              }
            }
          });

        if (parallel::size() > 1) {
          REQUIRE(not is_same_item_array(edge_array_per_node, edge_array_per_node_ref));
        }

        Synchronizer& synchronizer = SynchronizerManager::instance().getConnectivitySynchronizer(&connectivity);
        synchronizer.synchronize(edge_array_per_node);

        REQUIRE(is_same_item_array(edge_array_per_node, edge_array_per_node_ref));

        // Check that exchange sizes are correctly stored (require
        // lines to be covered)
        if (parallel::size() > 1) {
          const auto& node_owner = connectivity.nodeOwner();
          reset_ghost_arrays(edge_array_per_node, node_owner, 0);
          REQUIRE(not is_same_item_array(edge_array_per_node, edge_array_per_node_ref));
          synchronizer.synchronize(edge_array_per_node);
          REQUIRE(is_same_item_array(edge_array_per_node, edge_array_per_node_ref));
        }
      }
    }
  }
}
