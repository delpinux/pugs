#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/DataVariant.hpp>

#include <sstream>

// clazy:excludeall=non-pod-global-static

TEST_CASE("DataVariant", "[language]")
{
  SECTION("AggregateDataVariant")
  {
    AggregateDataVariant aggregate{
      std::vector<DataVariant>{double{1.3}, int64_t{-3}, std::vector<double>{1, 2.7}, bool{true}}};

    SECTION("size")
    {
      REQUIRE(aggregate.size() == 4);
    }

    SECTION("output")
    {
      std::stringstream aggregate_output;
      aggregate_output << aggregate;

      std::stringstream expected_output;
      expected_output << '(' << double{1.3} << ", " << int64_t{-3} << ", (" << 1 << ", " << 2.7 << "), "
                      << std::boolalpha << true << ")";
      REQUIRE(aggregate_output.str() == expected_output.str());
    }

    SECTION("values")
    {
      REQUIRE(std::get<double>(aggregate[0]) == double{1.3});
      REQUIRE(std::get<int64_t>(aggregate[1]) == int64_t{-3});
      REQUIRE(std::get<std::vector<double>>(aggregate[2]) == std::vector<double>{1, 2.7});
    }

    SECTION("copy")
    {
      AggregateDataVariant aggregate_copy{aggregate};

      REQUIRE(aggregate.size() == aggregate_copy.size());

      for (size_t i = 0; i < aggregate.size(); ++i) {
        REQUIRE(aggregate[i].index() == aggregate_copy[i].index());
      }

      REQUIRE(std::get<double>(aggregate[0]) == std::get<double>(aggregate_copy[0]));
      REQUIRE(std::get<int64_t>(aggregate[1]) == std::get<int64_t>(aggregate_copy[1]));
      REQUIRE(std::get<std::vector<double>>(aggregate[2]) == std::get<std::vector<double>>(aggregate_copy[2]));
    }

    SECTION("affectation")
    {
      AggregateDataVariant aggregate_copy;
      aggregate_copy = aggregate;

      REQUIRE(aggregate.size() == aggregate_copy.size());

      for (size_t i = 0; i < aggregate.size(); ++i) {
        REQUIRE(aggregate[i].index() == aggregate_copy[i].index());
      }

      REQUIRE(std::get<double>(aggregate[0]) == std::get<double>(aggregate_copy[0]));
      REQUIRE(std::get<int64_t>(aggregate[1]) == std::get<int64_t>(aggregate_copy[1]));
      REQUIRE(std::get<std::vector<double>>(aggregate[2]) == std::get<std::vector<double>>(aggregate_copy[2]));
    }

    SECTION("move affectation")
    {
      AggregateDataVariant aggregate_move_copy;
      {
        AggregateDataVariant aggregate_copy{aggregate};
        aggregate_move_copy = std::move(aggregate_copy);
      }

      REQUIRE(aggregate.size() == aggregate_move_copy.size());

      for (size_t i = 0; i < aggregate.size(); ++i) {
        REQUIRE(aggregate[i].index() == aggregate_move_copy[i].index());
      }

      REQUIRE(std::get<double>(aggregate[0]) == std::get<double>(aggregate_move_copy[0]));
      REQUIRE(std::get<int64_t>(aggregate[1]) == std::get<int64_t>(aggregate_move_copy[1]));
      REQUIRE(std::get<std::vector<double>>(aggregate[2]) == std::get<std::vector<double>>(aggregate_move_copy[2]));
    }
  }
}
