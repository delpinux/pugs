#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Messenger.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/NamedBoundaryDescriptor.hpp>
#include <mesh/NumberedBoundaryDescriptor.hpp>
#include <utils/checkpointing/ReadIBoundaryDescriptor.hpp>
#include <utils/checkpointing/WriteIBoundaryDescriptor.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_IBoundaryDescriptor", "[utils/checkpointing]")
{
  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});
    HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

    SECTION("IBoundaryDescriptor")
    {
      HighFive::Group symbol_table_group = file.createGroup("symbol_table");
      HighFive::Group useless_group;

      auto p_named_boundary_descriptor = std::make_shared<const NamedBoundaryDescriptor>("boundary_name1");
      checkpointing::writeIBoundaryDescriptor("named_boundary_descriptor",
                                              EmbeddedData{std::make_shared<DataHandler<const IBoundaryDescriptor>>(
                                                p_named_boundary_descriptor)},
                                              file, useless_group, symbol_table_group);

      auto p_numbered_boundary_descriptor = std::make_shared<const NumberedBoundaryDescriptor>(3);
      checkpointing::writeIBoundaryDescriptor("numbered_boundary_descriptor",
                                              EmbeddedData{std::make_shared<DataHandler<const IBoundaryDescriptor>>(
                                                p_numbered_boundary_descriptor)},
                                              file, useless_group, symbol_table_group);

      file.flush();

      EmbeddedData read_named_boundary_descriptor =
        checkpointing::readIBoundaryDescriptor("named_boundary_descriptor", symbol_table_group);

      EmbeddedData read_numbered_boundary_descriptor =
        checkpointing::readIBoundaryDescriptor("numbered_boundary_descriptor", symbol_table_group);

      auto get_value = [](const EmbeddedData& embedded_data) -> const IBoundaryDescriptor& {
        return *dynamic_cast<const DataHandler<const IBoundaryDescriptor>&>(embedded_data.get()).data_ptr();
      };

      REQUIRE_NOTHROW(get_value(read_named_boundary_descriptor));
      REQUIRE_NOTHROW(get_value(read_numbered_boundary_descriptor));

      REQUIRE(get_value(read_named_boundary_descriptor).type() == IBoundaryDescriptor::Type::named);
      REQUIRE(get_value(read_numbered_boundary_descriptor).type() == IBoundaryDescriptor::Type::numbered);

      REQUIRE_NOTHROW(dynamic_cast<const NamedBoundaryDescriptor&>(get_value(read_named_boundary_descriptor)));
      REQUIRE_NOTHROW(dynamic_cast<const NumberedBoundaryDescriptor&>(get_value(read_numbered_boundary_descriptor)));

      auto& read_named = dynamic_cast<const NamedBoundaryDescriptor&>(get_value(read_named_boundary_descriptor));
      auto& read_numbered =
        dynamic_cast<const NumberedBoundaryDescriptor&>(get_value(read_numbered_boundary_descriptor));

      REQUIRE(read_named.name() == p_named_boundary_descriptor->name());
      REQUIRE(read_numbered.number() == p_numbered_boundary_descriptor->number());
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }
}
