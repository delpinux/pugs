#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/SmallArray.hpp>
#include <utils/Types.hpp>

#include <deque>
#include <list>
#include <set>
#include <unordered_set>
#include <valarray>
#include <vector>

// Instantiate to ensure full coverage is performed
template class SmallArray<int>;

// clazy:excludeall=non-pod-global-static

TEST_CASE("SmallArray", "[utils]")
{
  SmallArray<int> a(10);
  REQUIRE(a.size() == 10);

  for (size_t i = 0; i < a.size(); ++i) {
    a[i] = 2 * i;
  }

  REQUIRE(((a[0] == 0) and (a[1] == 2) and (a[2] == 4) and (a[3] == 6) and (a[4] == 8) and (a[5] == 10) and
           (a[6] == 12) and (a[7] == 14) and (a[8] == 16) and (a[9] == 18)));

  SECTION("checking for copies")
  {
    SmallArray<const int> b{a};

    REQUIRE(((b[0] == 0) and (b[1] == 2) and (b[2] == 4) and (b[3] == 6) and (b[4] == 8) and (b[5] == 10) and
             (b[6] == 12) and (b[7] == 14) and (b[8] == 16) and (b[9] == 18)));

    SmallArray<int> c{a};

    REQUIRE(((c[0] == 0) and (c[1] == 2) and (c[2] == 4) and (c[3] == 6) and (c[4] == 8) and (c[5] == 10) and
             (c[6] == 12) and (c[7] == 14) and (c[8] == 16) and (c[9] == 18)));

    SmallArray<int> d = std::move(c);

    REQUIRE(((d[0] == 0) and (d[1] == 2) and (d[2] == 4) and (d[3] == 6) and (d[4] == 8) and (d[5] == 10) and
             (d[6] == 12) and (d[7] == 14) and (d[8] == 16) and (d[9] == 18)));
  }

  SECTION("checking for fill")
  {
    SmallArray<int> b(10);
    b.fill(3);

    REQUIRE(((b[0] == 3) and (b[1] == 3) and (b[2] == 3) and (b[3] == 3) and (b[4] == 3) and (b[5] == 3) and
             (b[6] == 3) and (b[7] == 3) and (b[8] == 3) and (b[9] == 3)));
  }

  SECTION("checking for affectations (shallow copy)")
  {
    SmallArray<const int> b;
    b = a;

    REQUIRE(((b[0] == 0) and (b[1] == 2) and (b[2] == 4) and (b[3] == 6) and (b[4] == 8) and (b[5] == 10) and
             (b[6] == 12) and (b[7] == 14) and (b[8] == 16) and (b[9] == 18)));

    SmallArray<int> c;
    c = a;

    REQUIRE(((c[0] == 0) and (c[1] == 2) and (c[2] == 4) and (c[3] == 6) and (c[4] == 8) and (c[5] == 10) and
             (c[6] == 12) and (c[7] == 14) and (c[8] == 16) and (c[9] == 18)));

    SmallArray<int> d;
    d = std::move(c);

    REQUIRE(((d[0] == 0) and (d[1] == 2) and (d[2] == 4) and (d[3] == 6) and (d[4] == 8) and (d[5] == 10) and
             (d[6] == 12) and (d[7] == 14) and (d[8] == 16) and (d[9] == 18)));
  }

  SECTION("checking for affectations (deep copy)")
  {
    SmallArray<int> b(copy(a));

    REQUIRE(((b[0] == 0) and (b[1] == 2) and (b[2] == 4) and (b[3] == 6) and (b[4] == 8) and (b[5] == 10) and
             (b[6] == 12) and (b[7] == 14) and (b[8] == 16) and (b[9] == 18)));

    b.fill(2);

    REQUIRE(((a[0] == 0) and (a[1] == 2) and (a[2] == 4) and (a[3] == 6) and (a[4] == 8) and (a[5] == 10) and
             (a[6] == 12) and (a[7] == 14) and (a[8] == 16) and (a[9] == 18)));

    REQUIRE(((b[0] == 2) and (b[1] == 2) and (b[2] == 2) and (b[3] == 2) and (b[4] == 2) and (b[5] == 2) and
             (b[6] == 2) and (b[7] == 2) and (b[8] == 2) and (b[9] == 2)));

    SmallArray<int> c;
    c = a;

    REQUIRE(((c[0] == 0) and (c[1] == 2) and (c[2] == 4) and (c[3] == 6) and (c[4] == 8) and (c[5] == 10) and
             (c[6] == 12) and (c[7] == 14) and (c[8] == 16) and (c[9] == 18)));

    c = copy(b);

    REQUIRE(((a[0] == 0) and (a[1] == 2) and (a[2] == 4) and (a[3] == 6) and (a[4] == 8) and (a[5] == 10) and
             (a[6] == 12) and (a[7] == 14) and (a[8] == 16) and (a[9] == 18)));

    REQUIRE(((c[0] == 2) and (c[1] == 2) and (c[2] == 2) and (c[3] == 2) and (c[4] == 2) and (c[5] == 2) and
             (c[6] == 2) and (c[7] == 2) and (c[8] == 2) and (c[9] == 2)));

    SmallArray<int> d{a.size()};
    copy_to(a, d);

    REQUIRE(((d[0] == 0) and (d[1] == 2) and (d[2] == 4) and (d[3] == 6) and (d[4] == 8) and (d[5] == 10) and
             (d[6] == 12) and (d[7] == 14) and (d[8] == 16) and (d[9] == 18)));

    REQUIRE(((c[0] == 2) and (c[1] == 2) and (c[2] == 2) and (c[3] == 2) and (c[4] == 2) and (c[5] == 2) and
             (c[6] == 2) and (c[7] == 2) and (c[8] == 2) and (c[9] == 2)));

    copy_to(c, d);

    REQUIRE(((a[0] == 0) and (a[1] == 2) and (a[2] == 4) and (a[3] == 6) and (a[4] == 8) and (a[5] == 10) and
             (a[6] == 12) and (a[7] == 14) and (a[8] == 16) and (a[9] == 18)));

    REQUIRE(((d[0] == 2) and (d[1] == 2) and (d[2] == 2) and (d[3] == 2) and (d[4] == 2) and (d[5] == 2) and
             (d[6] == 2) and (d[7] == 2) and (d[8] == 2) and (d[9] == 2)));
  }

  SECTION("checking for std container conversion")
  {
    {
      std::vector<int> v{1, 2, 5, 3};
      {
        SmallArray<int> v_array = convert_to_small_array(v);

        REQUIRE(v_array.size() == v.size());
        REQUIRE(((v_array[0] == 1) and (v_array[1] == 2) and (v_array[2] == 5) and (v_array[3] == 3)));
      }

      {
        SmallArray<const int> v_array = convert_to_small_array(v);

        REQUIRE(v_array.size() == v.size());
        REQUIRE(((v_array[0] == 1) and (v_array[1] == 2) and (v_array[2] == 5) and (v_array[3] == 3)));
      }
    }

    {
      std::vector<int> w;
      {
        SmallArray<int> w_array = convert_to_small_array(w);
        REQUIRE(w_array.size() == 0);
      }
      {
        SmallArray<const int> w_array = convert_to_small_array(w);
        REQUIRE(w_array.size() == 0);
      }
    }

    {
      std::valarray<int> v{1, 2, 5, 3};
      SmallArray<int> v_array = convert_to_small_array(v);

      REQUIRE(v_array.size() == v.size());
      REQUIRE(((v_array[0] == 1) and (v_array[1] == 2) and (v_array[2] == 5) and (v_array[3] == 3)));
    }

    {
      std::set<int> s{4, 2, 5, 3, 1, 3, 2};
      SmallArray<int> s_array = convert_to_small_array(s);

      REQUIRE(s_array.size() == s.size());
      REQUIRE(
        ((s_array[0] == 1) and (s_array[1] == 2) and (s_array[2] == 3) and (s_array[3] == 4) and (s_array[4] == 5)));
    }

    {
      std::unordered_set<int> us{4, 2, 5, 3, 1, 3, 2};
      SmallArray<int> us_array = convert_to_small_array(us);

      REQUIRE(us_array.size() == us.size());

      std::set<int> s;
      for (size_t i = 0; i < us_array.size(); ++i) {
        REQUIRE((us.find(us_array[i]) != us.end()));
        s.insert(us_array[i]);
      }
      REQUIRE(s.size() == us_array.size());
    }

    {
      std::multiset<int> ms{4, 2, 5, 3, 1, 3, 2};
      SmallArray<int> ms_array = convert_to_small_array(ms);

      REQUIRE(ms_array.size() == ms.size());
      REQUIRE(((ms_array[0] == 1) and (ms_array[1] == 2) and (ms_array[2] == 2) and (ms_array[3] == 3) and
               (ms_array[4] == 3) and (ms_array[5] == 4) and (ms_array[6] == 5)));
    }

    {
      std::list<int> l{1, 3, 5, 6, 2};
      SmallArray<int> l_array = convert_to_small_array(l);

      REQUIRE(l_array.size() == l.size());
      REQUIRE(
        ((l_array[0] == 1) and (l_array[1] == 3) and (l_array[2] == 5) and (l_array[3] == 6) and (l_array[4] == 2)));
    }

    {
      std::deque<int> q{1, 3, 5, 6, 2};
      q.push_front(2);
      SmallArray<int> q_array = convert_to_small_array(q);

      REQUIRE(q_array.size() == q.size());
      REQUIRE(((q_array[0] == 2) and (q_array[1] == 1) and (q_array[2] == 3) and (q_array[3] == 5) and
               (q_array[4] == 6) and (q_array[5] == 2)));
    }
  }

  SECTION("output")
  {
    SmallArray<int> x{5};
    x[0] = 2;
    x[1] = 6;
    x[2] = 2;
    x[3] = 3;
    x[4] = 7;

    std::ostringstream array_ost;
    array_ost << x;
    std::ostringstream ref_ost;
    ref_ost << 0 << ':' << x[0];
    for (size_t i = 1; i < x.size(); ++i) {
      ref_ost << ' ' << i << ':' << x[i];
    }
    REQUIRE(array_ost.str() == ref_ost.str());
  }

#ifndef NDEBUG

  SECTION("output with signaling NaN")
  {
    SmallArray<double> x{5};
    x[0] = 2;
    x[2] = 3;

    std::ostringstream array_ost;
    array_ost << x;
    std::ostringstream ref_ost;
    ref_ost << 0 << ':' << 2 << ' ';
    ref_ost << 1 << ":nan ";
    ref_ost << 2 << ':' << 3 << ' ';
    ref_ost << 3 << ":nan ";
    ref_ost << 4 << ":nan";
    REQUIRE(array_ost.str() == ref_ost.str());
  }

  SECTION("checking for bounds violation")
  {
    REQUIRE_THROWS_AS(a[10], AssertError);
  }

  SECTION("invalid copy_to")
  {
    SmallArray<int> b{2 * a.size()};
    REQUIRE_THROWS_AS(copy_to(a, b), AssertError);
  }

  SECTION("checking for nan initialization")
  {
    SmallArray<double> array(10);

    for (size_t i = 0; i < array.size(); ++i) {
      REQUIRE(std::isnan(array[i]));
    }
  }

  SECTION("checking for bad initialization")
  {
    SmallArray<int> array(10);

    for (size_t i = 0; i < array.size(); ++i) {
      REQUIRE(array[i] == std::numeric_limits<int>::max() / 2);
    }
  }

  SECTION("checking for SmallArray reductions")
  {
    SmallArray<int> array(10);
    array[0] = 13;
    array[1] = 1;
    array[2] = 8;
    array[3] = -3;
    array[4] = 23;
    array[5] = -1;
    array[6] = 13;
    array[7] = 0;
    array[8] = 12;
    array[9] = 9;

    SECTION("Min")
    {
      REQUIRE(min(array) == -3);
    }

    SECTION("Max")
    {
      REQUIRE(max(array) == 23);
    }

    SECTION("Sum")
    {
      REQUIRE((sum(array) == 75));
    }

    SECTION("TinyVector Sum")
    {
      using N2 = TinyVector<2, int>;
      SmallArray<N2> N2_array(10);
      N2_array[0] = N2{13, 2};
      N2_array[1] = N2{1, 3};
      N2_array[2] = N2{8, -2};
      N2_array[3] = N2{-3, 2};
      N2_array[4] = N2{23, 4};
      N2_array[5] = N2{-1, -3};
      N2_array[6] = N2{13, 17};
      N2_array[7] = N2{0, 9};
      N2_array[8] = N2{12, 13};
      N2_array[9] = N2{9, -17};

      REQUIRE((sum(N2_array) == N2{75, 28}));
    }

    SECTION("TinyMatrix Sum")
    {
      using N22 = TinyMatrix<2, 2, int>;
      SmallArray<N22> N22_array(10);
      N22_array[0] = N22{13, 2, 0, 1};
      N22_array[1] = N22{1, 3, 6, 3};
      N22_array[2] = N22{8, -2, -1, 21};
      N22_array[3] = N22{-3, 2, 5, 12};
      N22_array[4] = N22{23, 4, 7, 1};
      N22_array[5] = N22{-1, -3, 33, 11};
      N22_array[6] = N22{13, 17, 12, 13};
      N22_array[7] = N22{0, 9, 1, 14};
      N22_array[8] = N22{12, 13, -3, -71};
      N22_array[9] = N22{9, -17, 0, 16};

      REQUIRE((sum(N22_array) == N22{75, 28, 60, 21}));
    }
  }
#endif   // NDEBUG
}
