#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/SubItemValuePerItem.hpp>
#include <mesh/SubItemValuePerItemUtils.hpp>
#include <utils/Messenger.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("SubItemValuePerItemUtils", "[mesh]")
{
  SECTION("Synchronize")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

        const Connectivity<2>& connectivity = mesh_2d->connectivity();

        NodeValuePerFace<int> weak_node_value_per_face{connectivity};

        for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
          auto face_array = weak_node_value_per_face.itemArray(face_id);
          for (size_t i_node = 0; i_node < face_array.size(); ++i_node) {
            face_array[i_node] = parallel::rank() + 2 * i_node;
          }
        }

        NodeValuePerFace<const int> node_value_per_face{weak_node_value_per_face};

        REQUIRE(node_value_per_face.connectivity_ptr() == weak_node_value_per_face.connectivity_ptr());

        if (parallel::size() > 1) {
          // before synchronization
          auto face_owner = connectivity.faceOwner();

          bool is_synchronized = true;
          for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
            auto face_array = node_value_per_face.itemArray(face_id);
            for (size_t i_node = 0; i_node < face_array.size(); ++i_node) {
              if (face_array[i_node] != static_cast<int>(face_owner[face_id] + 2 * i_node)) {
                is_synchronized = false;
                break;
              }
            }
          }

          REQUIRE(not is_synchronized);
          REQUIRE(not isSynchronized(node_value_per_face));
        }

        synchronize(weak_node_value_per_face);

        {   // after synchronization
          auto face_owner = connectivity.faceOwner();

          bool is_synchronized = true;
          for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
            auto face_array = node_value_per_face.itemArray(face_id);
            for (size_t i_node = 0; i_node < face_array.size(); ++i_node) {
              if (face_array[i_node] != static_cast<int>(face_owner[face_id] + 2 * i_node)) {
                is_synchronized = false;
                break;
              }
            }
          }
          REQUIRE(is_synchronized);
        }
        REQUIRE(isSynchronized(node_value_per_face));
      }
    }
  }

  SECTION("min")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d = named_mesh.mesh()->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          NodeValuePerCell<int> node_value_per_cell{connectivity};
          node_value_per_cell.fill(-1);

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            connectivity.numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                auto cell_array = node_value_per_cell.itemArray(cell_id);
                for (size_t i = 0; i < cell_array.size(); ++i) {
                  cell_array[i] = 10 + parallel::rank() + i;
                }
              }
            });

          REQUIRE(min(node_value_per_cell) == 10);
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          FaceValuePerCell<int> face_value_per_cell{connectivity};
          face_value_per_cell.fill(-1);

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            connectivity.numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                auto cell_array = face_value_per_cell.itemArray(cell_id);
                for (size_t i = 0; i < cell_array.size(); ++i) {
                  cell_array[i] = 10 + parallel::rank() + i;
                }
              }
            });

          REQUIRE(min(face_value_per_cell) == 10);
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          EdgeValuePerFace<int> edge_value_per_face{connectivity};
          edge_value_per_face.fill(-1);

          auto face_is_owned = connectivity.faceIsOwned();
          parallel_for(
            connectivity.numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
              if (face_is_owned[face_id]) {
                auto face_array = edge_value_per_face.itemArray(face_id);
                for (size_t i = 0; i < face_array.size(); ++i) {
                  face_array[i] = 10 + parallel::rank() + i;
                }
              }
            });

          REQUIRE(min(edge_value_per_face) == 10);
        }
      }
    }
  }

  SECTION("max")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d = named_mesh.mesh()->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          EdgeValuePerCell<size_t> edge_value_per_cell{connectivity};
          edge_value_per_cell.fill(std::numeric_limits<size_t>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            connectivity.numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                auto cell_array = edge_value_per_cell.itemArray(cell_id);
                for (size_t i = 0; i < cell_array.size(); ++i) {
                  cell_array[i] = 10 + parallel::rank() - i;
                }
              }
            });

          REQUIRE(max(edge_value_per_cell) == 9 + parallel::size());
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          EdgeValuePerCell<size_t> edge_value_per_cell{connectivity};
          edge_value_per_cell.fill(std::numeric_limits<size_t>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            connectivity.numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                auto cell_array = edge_value_per_cell.itemArray(cell_id);
                for (size_t i = 0; i < cell_array.size(); ++i) {
                  cell_array[i] = 10 + parallel::rank() - i;
                }
              }
            });

          REQUIRE(max(edge_value_per_cell) == 9 + parallel::size());
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          CellValue<size_t> cell_value{connectivity};

          NodeValuePerEdge<size_t> node_value_per_edge{connectivity};
          node_value_per_edge.fill(std::numeric_limits<size_t>::max());

          auto edge_is_owned = connectivity.edgeIsOwned();
          parallel_for(
            connectivity.numberOfEdges(), PUGS_LAMBDA(EdgeId edge_id) {
              if (edge_is_owned[edge_id]) {
                auto edge_array = node_value_per_edge.itemArray(edge_id);
                for (size_t i = 0; i < edge_array.size(); ++i) {
                  edge_array[i] = 10 + parallel::rank() - i;
                }
              }
            });

          REQUIRE(max(node_value_per_edge) == 9 + parallel::size());
        }
      }
    }

    SECTION("max for all negative values")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          EdgeValuePerCell<double> edge_value_per_cell{connectivity};
          edge_value_per_cell.fill(std::numeric_limits<double>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            connectivity.numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                auto cell_array = edge_value_per_cell.itemArray(cell_id);
                for (size_t i = 0; i < cell_array.size(); ++i) {
                  cell_array[i] = -1. * parallel::size() - 10 + parallel::rank() - i;
                }
              }
            });

          REQUIRE(max(edge_value_per_cell) == -11);
        }
      }
    }
  }

  SECTION("sum")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d = named_mesh.mesh()->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          NodeValuePerCell<size_t> node_value_per_cell{connectivity};
          node_value_per_cell.fill(5);

          auto cell_is_owned = connectivity.cellIsOwned();

          const size_t global_number_of_nodes_per_cells = [&] {
            size_t number_of_nodes_per_cells = 0;
            for (CellId cell_id = 0; cell_id < cell_is_owned.numberOfItems(); ++cell_id) {
              number_of_nodes_per_cells += cell_is_owned[cell_id] * node_value_per_cell.numberOfSubValues(cell_id);
            }
            return parallel::allReduceSum(number_of_nodes_per_cells);
          }();

          REQUIRE(sum(node_value_per_cell) == 5 * global_number_of_nodes_per_cells);
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name() + " for size_t data")
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          NodeValuePerFace<size_t> node_value_per_face{connectivity};
          node_value_per_face.fill(2);

          auto face_is_owned = connectivity.faceIsOwned();

          const size_t global_number_of_nodes_per_faces = [&] {
            size_t number_of_nodes_per_faces = 0;
            for (FaceId face_id = 0; face_id < face_is_owned.numberOfItems(); ++face_id) {
              number_of_nodes_per_faces += face_is_owned[face_id] * node_value_per_face.numberOfSubValues(face_id);
            }
            return parallel::allReduceSum(number_of_nodes_per_faces);
          }();

          REQUIRE(sum(node_value_per_face) == 2 * global_number_of_nodes_per_faces);
        }

        SECTION(named_mesh.name() + " for R^2 data")
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          using R2 = TinyVector<2, double>;
          NodeValuePerFace<R2> node_value_per_face{connectivity};

          auto Nl = MeshDataManager::instance().getMeshData(*mesh_2d).Nl();

          parallel_for(
            mesh_2d->numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
              for (size_t i_node = 0; i_node < node_value_per_face.numberOfSubValues(face_id); ++i_node) {
                node_value_per_face[face_id][i_node] = (i_node + 1) * Nl[face_id];
              }
            });

          FaceValue<R2> face_value{connectivity};
          parallel_for(
            mesh_2d->numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
              face_value[face_id] = node_value_per_face[face_id][0];
              for (size_t i_node = 1; i_node < node_value_per_face.numberOfSubValues(face_id); ++i_node) {
                face_value[face_id] += node_value_per_face[face_id][i_node];
              }
            });

          REQUIRE(sum(node_value_per_face) == sum(face_value));
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name() + " for size_t data")
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          EdgeValuePerFace<size_t> edge_value_per_face{connectivity};
          edge_value_per_face.fill(3);

          auto face_is_owned = connectivity.faceIsOwned();

          const size_t global_number_of_edges_per_faces = [&] {
            size_t number_of_edges_per_faces = 0;
            for (FaceId face_id = 0; face_id < face_is_owned.numberOfItems(); ++face_id) {
              number_of_edges_per_faces += face_is_owned[face_id] * edge_value_per_face.numberOfSubValues(face_id);
            }
            return parallel::allReduceSum(number_of_edges_per_faces);
          }();

          REQUIRE(sum(edge_value_per_face) == 3 * global_number_of_edges_per_faces);
        }

        SECTION(named_mesh.name() + " for N^2x3 data")
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          using N2x3 = TinyMatrix<2, 3, size_t>;
          EdgeValuePerFace<N2x3> edge_value_per_face{connectivity};

          const N2x3 data(1, 3, 4, 6, 2, 5);
          edge_value_per_face.fill(data);

          auto face_is_owned = connectivity.faceIsOwned();

          const size_t global_number_of_edges_per_faces = [&] {
            size_t number_of_edges_per_faces = 0;
            for (FaceId face_id = 0; face_id < face_is_owned.numberOfItems(); ++face_id) {
              number_of_edges_per_faces += face_is_owned[face_id] * edge_value_per_face.numberOfSubValues(face_id);
            }
            return parallel::allReduceSum(number_of_edges_per_faces);
          }();

          REQUIRE(sum(edge_value_per_face) == global_number_of_edges_per_faces * data);
        }

        SECTION(named_mesh.name() + " for R^3x2 float")
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          auto xr = mesh_3d->xr();

          using R3x2 = TinyMatrix<3, 2, float>;
          EdgeValuePerNode<R3x2> edge_value_per_node{connectivity};

          parallel_for(
            mesh_3d->numberOfNodes(), PUGS_LAMBDA(NodeId node_id) {
              for (size_t i_edge = 0; i_edge < edge_value_per_node.numberOfSubValues(node_id); ++i_edge) {
                R3x2 value((2.3 * i_edge + 1.7) * xr[node_id][0],                    //
                           (3. - i_edge) * xr[node_id][1],                           //
                           (2 * i_edge + 1.3) * xr[node_id][2],                      //
                           (i_edge + 1.7) * xr[node_id][0] + 3.2 * xr[node_id][1],   //
                           xr[node_id][2] + 3 * i_edge,                              //
                           7.13 * i_edge * i_edge * l2Norm(xr[node_id]));
                edge_value_per_node[node_id][i_edge] = value;
              }
            });

          NodeValue<R3x2> node_value{connectivity};
          parallel_for(
            mesh_3d->numberOfNodes(), PUGS_LAMBDA(NodeId node_id) {
              node_value[node_id] = edge_value_per_node[node_id][0];
              for (size_t i_edge = 1; i_edge < edge_value_per_node.numberOfSubValues(node_id); ++i_edge) {
                node_value[node_id] += edge_value_per_node[node_id][i_edge];
              }
            });

          REQUIRE(sum(edge_value_per_node) == sum(node_value));
        }
      }
    }
  }
}
