#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <test_BinaryExpressionProcessor_utils.hpp>
#include <utils/Stringify.hpp>

#include <utils/pugs_config.hpp>

#include <fstream>
#include <netdb.h>
#include <regex>
#include <unistd.h>

// clazy:excludeall=non-pod-global-static

TEST_CASE("BinaryExpressionProcessor shift", "[language]")
{
  SECTION("<<")
  {
    std::filesystem::path path{PUGS_BINARY_DIR};
    path.append("tests").append(std::string{"binary_expression_processor_"} + stringify(getpid()));

    std::string filename = path.string();

    {
      std::ostringstream data;
      data << R"(import socket;)";
      data << "let fout:ostream, fout = ofstream(\"" << path.string() << "\");\n";
      data << R"(fout << 2 << " " << true << " " << 2 + 3 << "\n";
fout << createSocketServer(0) << "\n";)";
      data << "let b:(B), b = (true,false);\n";
      data << "let n:(N), n = (1,3);\n";
      data << "let z:(Z), z = (-1,3);\n";
      data << "let r:(R), r = (2.3,4);\n";
      data << "let u1:(R^1), u1 = (2.3,[4]);\n";
      data << "let u2:(R^2), u2 = ([2.3,4], [3,2]);\n";
      data << "let u3:(R^3), u3 = ([2.3,4,-1], [3,2,1]);\n";
      data << "let A1:(R^1x1), A1 = (2.3, [[4]]);\n";
      data << "let A2:(R^2x2), A2 = ([[2.3,4],[2,0.3]], [[3,2.3],[1,-4]]);\n";
      data << "let A3:(R^3x3), A3 = ([[2.3,4,-1],[2,7,2.3],[6,2,8]], [[3,2,1],[1,2,5],[2.1,3,-2.6]]);\n";
      data << R"(let s:(string), s = ("foo", "bar");)";
      data << R"(fout << b  << "\n";)";
      data << R"(fout << n  << "\n";)";
      data << R"(fout << z  << "\n";)";
      data << R"(fout << r  << "\n";)";
      data << R"(fout << u1 << "\n";)";
      data << R"(fout << u2 << "\n";)";
      data << R"(fout << u3 << "\n";)";
      data << R"(fout << A1 << "\n";)";
      data << R"(fout << A2 << "\n";)";
      data << R"(fout << A3 << "\n";)";
      data << R"(fout << s  << "\n";)";
      TAO_PEGTL_NAMESPACE::string_input input{data.str(), "test.pgs"};
      auto ast = ASTBuilder::build(input);

      CheckpointResumeRepository::create();
      ASTModulesImporter{*ast};
      ASTNodeTypeCleaner<language::import_instruction>{*ast};

      ASTSymbolTableBuilder{*ast};
      ASTNodeDataTypeBuilder{*ast};

      ASTNodeDeclarationToAffectationConverter{*ast};
      ASTNodeTypeCleaner<language::var_declaration>{*ast};

      ASTNodeExpressionBuilder{*ast};
      ExecutionPolicy exec_policy;
      ASTExecutionStack::create();
      ast->execute(exec_policy);
      ASTExecutionStack::destroy();
      CheckpointResumeRepository::destroy();
      ast->m_symbol_table->clearValues();
    }

    REQUIRE(std::filesystem::exists(filename));

    {
      std::string file_content;
      std::ifstream fin(filename.c_str());

      do {
        char c = fin.get();
        if (c != EOF) {
          file_content += c;
        }
      } while (fin);

      std::string expected = "2 true 5\n";
      char hbuf[NI_MAXHOST];
      ::gethostname(hbuf, NI_MAXHOST);
      expected += hbuf;
      expected += ':';

      REQUIRE(file_content.size() > expected.size());
      REQUIRE(file_content.substr(0, expected.size()) == expected);

      std::string suffix = file_content.substr(expected.size(), file_content.size() - expected.size());

      auto is_int = [](const std::string& s) {
        for (const char& c : s) {
          if (c == '\n') {
            break;
          } else if (not std::isdigit(c)) {
            return false;
          }
        }
        return true;
      };

      REQUIRE(is_int(suffix));
    }

    {
      std::ifstream fin(filename.c_str());
      char line[1024];

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "2 true 5");

      char hbuf[NI_MAXHOST];
      ::gethostname(hbuf, NI_MAXHOST);
      std::ostringstream os;
      os << '^' << hbuf << ":\\d+$";
      fin.getline(line, 1023);
      REQUIRE(std::regex_match(line, std::regex{os.str()}));

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "(true, false)");

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "(1, 3)");

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "(-1, 3)");

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "(2.3, 4)");

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "([2.3], [4])");

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "([2.3,4], [3,2])");

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "([2.3,4,-1], [3,2,1])");

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "([[2.3]], [[4]])");

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "([[2.3,4],[2,0.3]], [[3,2.3],[1,-4]])");

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "([[2.3,4,-1],[2,7,2.3],[6,2,8]], [[3,2,1],[1,2,5],[2.1,3,-2.6]])");

      fin.getline(line, 1023);
      REQUIRE(std::string_view(line) == "(foo, bar)");
    }

    std::filesystem::remove(filename);
    REQUIRE(not std::filesystem::exists(filename));
  }
}
