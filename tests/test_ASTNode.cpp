#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTNode.hpp>
#include <language/node_processor/FakeProcessor.hpp>
#include <utils/Demangle.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNode", "[language]")
{
  rang::setControlMode(rang::control::Off);
  SECTION("execute")
  {
    ASTNode ast_node;
    ExecutionPolicy exec_policy;

#ifndef NDEBUG
    ASTExecutionStack::create();
    REQUIRE_THROWS(ast_node.execute(exec_policy));
    ASTExecutionStack::destroy();
#endif   // NDEBUG

    ast_node.m_node_processor = std::make_unique<FakeProcessor>();

    ASTExecutionStack::create();
    REQUIRE_NOTHROW(ast_node.execute(exec_policy));
    ASTExecutionStack::destroy();
  }

  SECTION("name")
  {
    ASTNode ast_node;

    ast_node.set_type<language::name>();

    REQUIRE(ast_node.name() == demangle<language::name>());
  }

  SECTION("string / string_view")
  {
    SECTION("no content")
    {
      ASTNode ast_node;

      REQUIRE(ast_node.string() == "<optimized out>");
      REQUIRE(ast_node.string_view() == "<optimized out>");
    }

    SECTION("has content")
    {
      ASTNode ast_node;
      ast_node.source         = "content";
      const char* const start = &ast_node.source[0];
      ast_node.m_begin        = TAO_PEGTL_NAMESPACE::internal::inputerator{start};
      ast_node.m_end          = TAO_PEGTL_NAMESPACE::internal::inputerator{start + 7};

      REQUIRE(ast_node.string() == "content");
      REQUIRE(ast_node.string_view() == "content");
    }

    SECTION("children have content")
    {
      ASTNode ast_node;
      ast_node.source         = "content";
      const char* const start = &ast_node.source[0];
      ast_node.m_begin        = TAO_PEGTL_NAMESPACE::internal::inputerator{start + 2};

      {
        std::unique_ptr<ASTNode> child0_node = std::make_unique<ASTNode>();

        child0_node->m_begin = TAO_PEGTL_NAMESPACE::internal::inputerator{start};
        child0_node->m_end   = TAO_PEGTL_NAMESPACE::internal::inputerator{start + 3};

        ast_node.children.emplace_back(std::move(child0_node));
      }

      {
        std::unique_ptr<ASTNode> child1_node = std::make_unique<ASTNode>();

        child1_node->m_begin = TAO_PEGTL_NAMESPACE::internal::inputerator{start + 4};
        child1_node->m_end   = TAO_PEGTL_NAMESPACE::internal::inputerator{start + 7};

        ast_node.children.emplace_back(std::move(child1_node));
      }

      REQUIRE(ast_node.string() == "content");
      REQUIRE(ast_node.string_view() == "content");
    }
  }
}
