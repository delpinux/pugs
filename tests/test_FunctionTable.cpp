#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/FunctionTable.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("FunctionTable", "[language]")
{
  rang::setControlMode(rang::control::Off);

  SECTION("FunctionDescriptor")
  {
    std::unique_ptr domain_mapping_node = std::make_unique<ASTNode>();
    domain_mapping_node->m_data_type    = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();

    std::unique_ptr definition_node = std::make_unique<ASTNode>();
    definition_node->m_data_type    = ASTNodeDataType::build<ASTNodeDataType::double_t>();
    FunctionDescriptor f{"f", std::move(domain_mapping_node), std::move(definition_node)};

    REQUIRE(f.domainMappingNode().m_data_type == ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>());
    REQUIRE(f.definitionNode().m_data_type == ASTNodeDataType::build<ASTNodeDataType::double_t>());

    REQUIRE(domain_mapping_node == nullptr);
    REQUIRE(definition_node == nullptr);
  }

#ifndef NDEBUG
  SECTION("uninitialized FunctionDescriptor")
  {
    std::unique_ptr domain_mapping_node = std::make_unique<ASTNode>();
    domain_mapping_node->m_data_type    = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();

    std::unique_ptr definition_node = std::make_unique<ASTNode>();
    definition_node->m_data_type    = ASTNodeDataType::build<ASTNodeDataType::double_t>();

    SECTION("nothing initialized")
    {
      FunctionDescriptor f{"function", nullptr, nullptr};
      REQUIRE_THROWS_AS(f.domainMappingNode(), AssertError);
      REQUIRE_THROWS_AS(f.definitionNode(), AssertError);
    }

    SECTION("domain mapping uninitialized")
    {
      FunctionDescriptor f{"function", nullptr, std::move(definition_node)};
      REQUIRE_THROWS_AS(f.domainMappingNode(), AssertError);
      REQUIRE(f.definitionNode().m_data_type == ASTNodeDataType::build<ASTNodeDataType::double_t>());
      REQUIRE(definition_node == nullptr);
    }

    SECTION("definition node uninitialized")
    {
      FunctionDescriptor f{"function", std::move(domain_mapping_node), nullptr};
      REQUIRE_THROWS_AS(f.definitionNode(), AssertError);
      REQUIRE(f.domainMappingNode().m_data_type == ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>());
      REQUIRE(domain_mapping_node == nullptr);
    }
  }
#endif   // NDEBUG

  FunctionTable table;

  REQUIRE(table.size() == 0);

  std::unique_ptr domain_mapping_node = std::make_unique<ASTNode>();
  domain_mapping_node->m_data_type    = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();

  std::unique_ptr definition_node = std::make_unique<ASTNode>();
  definition_node->m_data_type    = ASTNodeDataType::build<ASTNodeDataType::double_t>();

  size_t function_id =
    table.add(FunctionDescriptor{"function", std::move(domain_mapping_node), std::move(definition_node)});

  REQUIRE(domain_mapping_node == nullptr);
  REQUIRE(definition_node == nullptr);

  REQUIRE(function_id == 0);
  REQUIRE(table.size() == 1);

  const auto& const_table = table;
  REQUIRE(const_table.size() == 1);

  auto& f = table[function_id];
  REQUIRE(f.name() == "function");
  REQUIRE(f.domainMappingNode().m_data_type == ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>());
  REQUIRE(f.definitionNode().m_data_type == ASTNodeDataType::build<ASTNodeDataType::double_t>());

  const auto& const_f = const_table[function_id];
  REQUIRE(const_f.name() == "function");
  REQUIRE(const_f.domainMappingNode().m_data_type == ASTNodeDataType::unsigned_int_t);
  REQUIRE(const_f.definitionNode().m_data_type == ASTNodeDataType::double_t);

#ifndef NDEBUG
  REQUIRE_THROWS_AS(table[table.size()], AssertError);
  REQUIRE_THROWS_AS(const_table[const_table.size()], AssertError);
#endif   // NDEBUG
}
