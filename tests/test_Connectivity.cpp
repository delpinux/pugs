#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityUtils.hpp>
#include <mesh/ItemValue.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshVariant.hpp>
#include <utils/Messenger.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("Connectivity", "[mesh]")
{
  SECTION("numberOfItems")
  {
    SECTION("1D")
    {
      SECTION("unordered 1D mesh")
      {
        const auto& mesh = *MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>();

        if (parallel::size() == 1) {
          REQUIRE(mesh.numberOfNodes() == 35);
          REQUIRE(mesh.numberOfEdges() == 35);
          REQUIRE(mesh.numberOfFaces() == 35);
          REQUIRE(mesh.numberOfCells() == 34);
        }

        REQUIRE(mesh.numberOfNodes() == mesh.numberOf<ItemType::node>());
        REQUIRE(mesh.numberOfEdges() == mesh.numberOf<ItemType::edge>());
        REQUIRE(mesh.numberOfFaces() == mesh.numberOf<ItemType::face>());
        REQUIRE(mesh.numberOfCells() == mesh.numberOf<ItemType::cell>());
      }

      SECTION("cartesian 1D mesh")
      {
        const auto& mesh = *MeshDataBaseForTests::get().cartesian1DMesh()->get<Mesh<1>>();

        if (parallel::size() == 1) {
          REQUIRE(mesh.numberOfNodes() == 24);
          REQUIRE(mesh.numberOfEdges() == 24);
          REQUIRE(mesh.numberOfFaces() == 24);
          REQUIRE(mesh.numberOfCells() == 23);
        }

        REQUIRE(mesh.numberOfNodes() == mesh.numberOf<ItemType::node>());
        REQUIRE(mesh.numberOfEdges() == mesh.numberOf<ItemType::edge>());
        REQUIRE(mesh.numberOfFaces() == mesh.numberOf<ItemType::face>());
        REQUIRE(mesh.numberOfCells() == mesh.numberOf<ItemType::cell>());
      }
    }

    SECTION("2D")
    {
      SECTION("hybrid 2D mesh")
      {
        const auto& mesh = *MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>();

        if (parallel::size() == 1) {
          REQUIRE(mesh.numberOfNodes() == 53);
          REQUIRE(mesh.numberOfEdges() == 110);
          REQUIRE(mesh.numberOfFaces() == 110);
          REQUIRE(mesh.numberOfCells() == 58);
        }

        REQUIRE(mesh.numberOfNodes() == mesh.numberOf<ItemType::node>());
        REQUIRE(mesh.numberOfEdges() == mesh.numberOf<ItemType::edge>());
        REQUIRE(mesh.numberOfFaces() == mesh.numberOf<ItemType::face>());
        REQUIRE(mesh.numberOfCells() == mesh.numberOf<ItemType::cell>());
      }

      SECTION("cartesian 2D mesh")
      {
        const auto& mesh = *MeshDataBaseForTests::get().cartesian2DMesh()->get<Mesh<2>>();

        if (parallel::size() == 1) {
          REQUIRE(mesh.numberOfNodes() == 56);
          REQUIRE(mesh.numberOfEdges() == 97);
          REQUIRE(mesh.numberOfFaces() == 97);
          REQUIRE(mesh.numberOfCells() == 42);
        }

        REQUIRE(mesh.numberOfNodes() == mesh.numberOf<ItemType::node>());
        REQUIRE(mesh.numberOfEdges() == mesh.numberOf<ItemType::edge>());
        REQUIRE(mesh.numberOfFaces() == mesh.numberOf<ItemType::face>());
        REQUIRE(mesh.numberOfCells() == mesh.numberOf<ItemType::cell>());
      }
    }

    SECTION("3D")
    {
      SECTION("hybrid 3D mesh")
      {
        const auto& mesh = *MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>();

        if (parallel::size() == 1) {
          REQUIRE(mesh.numberOfNodes() == 132);
          REQUIRE(mesh.numberOfEdges() == 452);
          REQUIRE(mesh.numberOfFaces() == 520);
          REQUIRE(mesh.numberOfCells() == 199);
        }

        REQUIRE(mesh.numberOfNodes() == mesh.numberOf<ItemType::node>());
        REQUIRE(mesh.numberOfEdges() == mesh.numberOf<ItemType::edge>());
        REQUIRE(mesh.numberOfFaces() == mesh.numberOf<ItemType::face>());
        REQUIRE(mesh.numberOfCells() == mesh.numberOf<ItemType::cell>());
      }

      SECTION("cartesian 3D mesh")
      {
        const auto& mesh = *MeshDataBaseForTests::get().cartesian3DMesh()->get<Mesh<3>>();

        if (parallel::size() == 1) {
          REQUIRE(mesh.numberOfNodes() == 280);
          REQUIRE(mesh.numberOfEdges() == 709);
          REQUIRE(mesh.numberOfFaces() == 598);
          REQUIRE(mesh.numberOfCells() == 168);
        }

        REQUIRE(mesh.numberOfNodes() == mesh.numberOf<ItemType::node>());
        REQUIRE(mesh.numberOfEdges() == mesh.numberOf<ItemType::edge>());
        REQUIRE(mesh.numberOfFaces() == mesh.numberOf<ItemType::face>());
        REQUIRE(mesh.numberOfCells() == mesh.numberOf<ItemType::cell>());
      }
    }
  }

  SECTION("isOwned")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d                        = named_mesh.mesh()->get<Mesh<1>>();
          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          SECTION("nodes/edges/faces")
          {
            auto node_owner = connectivity.nodeOwner();
            REQUIRE(decltype(node_owner)::item_t == ItemType::node);
            auto node_owner_generic = connectivity.owner<ItemType::node>();
            REQUIRE(decltype(node_owner_generic)::item_t == ItemType::node);
            REQUIRE(&(node_owner[NodeId{0}]) == &(node_owner_generic[NodeId{0}]));

            auto node_is_owned = connectivity.nodeIsOwned();
            REQUIRE(decltype(node_is_owned)::item_t == ItemType::node);
            auto node_is_owned_generic = connectivity.isOwned<ItemType::node>();
            REQUIRE(decltype(node_is_owned_generic)::item_t == ItemType::node);
            REQUIRE(&(node_is_owned[NodeId{0}]) == &(node_is_owned_generic[NodeId{0}]));

            auto edge_owner = connectivity.edgeOwner();
            REQUIRE(decltype(edge_owner)::item_t == ItemType::edge);
            auto edge_owner_generic = connectivity.owner<ItemType::edge>();
            REQUIRE(decltype(edge_owner_generic)::item_t == ItemType::edge);
            REQUIRE(&(edge_owner[EdgeId{0}]) == &(edge_owner_generic[EdgeId{0}]));

            auto edge_is_owned = connectivity.edgeIsOwned();
            REQUIRE(decltype(edge_is_owned)::item_t == ItemType::edge);
            auto edge_is_owned_generic = connectivity.isOwned<ItemType::edge>();
            REQUIRE(decltype(edge_is_owned_generic)::item_t == ItemType::edge);
            REQUIRE(&(edge_is_owned[EdgeId{0}]) == &(edge_is_owned_generic[EdgeId{0}]));

            auto face_owner = connectivity.faceOwner();
            REQUIRE(decltype(face_owner)::item_t == ItemType::face);
            auto face_owner_generic = connectivity.owner<ItemType::face>();
            REQUIRE(decltype(face_owner_generic)::item_t == ItemType::face);
            REQUIRE(&(face_owner[FaceId{0}]) == &(face_owner_generic[FaceId{0}]));

            auto face_is_owned = connectivity.faceIsOwned();
            REQUIRE(decltype(face_is_owned)::item_t == ItemType::face);
            auto face_is_owned_generic = connectivity.isOwned<ItemType::face>();
            REQUIRE(decltype(face_is_owned_generic)::item_t == ItemType::face);
            REQUIRE(&(face_is_owned[FaceId{0}]) == &(face_is_owned_generic[FaceId{0}]));

            // In 1d these values are the same
            REQUIRE(&(node_owner[NodeId(0)]) == &(face_owner[FaceId(0)]));
            REQUIRE(&(node_owner[NodeId(0)]) == &(edge_owner[EdgeId(0)]));

            REQUIRE(isSynchronized(node_owner));

            const int rank_number = parallel::rank();

            bool node_is_owned_is_valid = true;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              node_is_owned_is_valid &= (node_is_owned[node_id] == (node_owner[node_id] == rank_number));
            }
            REQUIRE(node_is_owned_is_valid);
          }

          SECTION("cells")
          {
            auto cell_owner = connectivity.cellOwner();
            REQUIRE(decltype(cell_owner)::item_t == ItemType::cell);
            auto cell_owner_generic = connectivity.owner<ItemType::cell>();
            REQUIRE(decltype(cell_owner_generic)::item_t == ItemType::cell);
            REQUIRE(&(cell_owner[CellId{0}]) == &(cell_owner_generic[CellId{0}]));

            auto cell_is_owned = connectivity.cellIsOwned();
            REQUIRE(decltype(cell_is_owned)::item_t == ItemType::cell);
            auto cell_is_owned_generic = connectivity.isOwned<ItemType::cell>();
            REQUIRE(decltype(cell_is_owned_generic)::item_t == ItemType::cell);
            REQUIRE(&(cell_is_owned[CellId{0}]) == &(cell_is_owned_generic[CellId{0}]));

            REQUIRE(isSynchronized(cell_owner));

            const int rank_number = parallel::rank();

            bool cell_is_owned_is_valid = true;
            for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
              cell_is_owned_is_valid &= (cell_is_owned[cell_id] == (cell_owner[cell_id] == rank_number));
            }
            REQUIRE(cell_is_owned_is_valid);
          }
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d                        = named_mesh.mesh()->get<Mesh<2>>();
          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          SECTION("nodes")
          {
            auto node_owner = connectivity.nodeOwner();
            REQUIRE(decltype(node_owner)::item_t == ItemType::node);
            auto node_owner_generic = connectivity.owner<ItemType::node>();
            REQUIRE(decltype(node_owner_generic)::item_t == ItemType::node);
            REQUIRE(&(node_owner[NodeId{0}]) == &(node_owner_generic[NodeId{0}]));

            auto node_is_owned = connectivity.nodeIsOwned();
            REQUIRE(decltype(node_is_owned)::item_t == ItemType::node);
            auto node_is_owned_generic = connectivity.isOwned<ItemType::node>();
            REQUIRE(decltype(node_is_owned_generic)::item_t == ItemType::node);
            REQUIRE(&(node_is_owned[NodeId{0}]) == &(node_is_owned_generic[NodeId{0}]));

            REQUIRE(isSynchronized(node_owner));

            const int rank_number = parallel::rank();

            bool node_is_owned_is_valid = true;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              node_is_owned_is_valid &= (node_is_owned[node_id] == (node_owner[node_id] == rank_number));
            }
            REQUIRE(node_is_owned_is_valid);
          }

          SECTION("edges/faces")
          {
            auto edge_owner = connectivity.edgeOwner();
            REQUIRE(decltype(edge_owner)::item_t == ItemType::edge);
            auto edge_owner_generic = connectivity.owner<ItemType::edge>();
            REQUIRE(decltype(edge_owner_generic)::item_t == ItemType::edge);
            REQUIRE(&(edge_owner[EdgeId{0}]) == &(edge_owner_generic[EdgeId{0}]));

            auto edge_is_owned = connectivity.edgeIsOwned();
            REQUIRE(decltype(edge_is_owned)::item_t == ItemType::edge);
            auto edge_is_owned_generic = connectivity.isOwned<ItemType::edge>();
            REQUIRE(decltype(edge_is_owned_generic)::item_t == ItemType::edge);
            REQUIRE(&(edge_is_owned[EdgeId{0}]) == &(edge_is_owned_generic[EdgeId{0}]));

            auto face_owner = connectivity.faceOwner();
            REQUIRE(decltype(face_owner)::item_t == ItemType::face);
            auto face_owner_generic = connectivity.owner<ItemType::face>();
            REQUIRE(decltype(face_owner_generic)::item_t == ItemType::face);
            REQUIRE(&(face_owner[FaceId{0}]) == &(face_owner_generic[FaceId{0}]));

            auto face_is_owned = connectivity.faceIsOwned();
            REQUIRE(decltype(face_is_owned)::item_t == ItemType::face);
            auto face_is_owned_generic = connectivity.isOwned<ItemType::face>();
            REQUIRE(decltype(face_is_owned_generic)::item_t == ItemType::face);
            REQUIRE(&(face_is_owned[FaceId{0}]) == &(face_is_owned_generic[FaceId{0}]));

            // In 2d these values are the same
            REQUIRE(&(face_owner[FaceId(0)]) == &(edge_owner[EdgeId(0)]));

            REQUIRE(isSynchronized(face_owner));

            const int rank_number = parallel::rank();

            bool face_is_owned_is_valid = true;
            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              face_is_owned_is_valid &= (face_is_owned[face_id] == (face_owner[face_id] == rank_number));
            }
            REQUIRE(face_is_owned_is_valid);
          }

          SECTION("cells")
          {
            auto cell_owner = connectivity.cellOwner();
            REQUIRE(decltype(cell_owner)::item_t == ItemType::cell);
            auto cell_owner_generic = connectivity.owner<ItemType::cell>();
            REQUIRE(decltype(cell_owner_generic)::item_t == ItemType::cell);
            REQUIRE(&(cell_owner[CellId{0}]) == &(cell_owner_generic[CellId{0}]));

            auto cell_is_owned = connectivity.cellIsOwned();
            REQUIRE(decltype(cell_is_owned)::item_t == ItemType::cell);
            auto cell_is_owned_generic = connectivity.isOwned<ItemType::cell>();
            REQUIRE(decltype(cell_is_owned_generic)::item_t == ItemType::cell);
            REQUIRE(&(cell_is_owned[CellId{0}]) == &(cell_is_owned_generic[CellId{0}]));

            REQUIRE(isSynchronized(cell_owner));

            const int rank_number = parallel::rank();

            bool cell_is_owned_is_valid = true;
            for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
              cell_is_owned_is_valid &= (cell_is_owned[cell_id] == (cell_owner[cell_id] == rank_number));
            }
            REQUIRE(cell_is_owned_is_valid);
          }
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          SECTION("nodes")
          {
            auto node_owner = connectivity.nodeOwner();
            REQUIRE(decltype(node_owner)::item_t == ItemType::node);
            auto node_owner_generic = connectivity.owner<ItemType::node>();
            REQUIRE(decltype(node_owner_generic)::item_t == ItemType::node);
            REQUIRE(&(node_owner[NodeId{0}]) == &(node_owner_generic[NodeId{0}]));

            auto node_is_owned = connectivity.nodeIsOwned();
            REQUIRE(decltype(node_is_owned)::item_t == ItemType::node);
            auto node_is_owned_generic = connectivity.isOwned<ItemType::node>();
            REQUIRE(decltype(node_is_owned_generic)::item_t == ItemType::node);
            REQUIRE(&(node_is_owned[NodeId{0}]) == &(node_is_owned_generic[NodeId{0}]));

            REQUIRE(isSynchronized(node_owner));

            const int rank_number = parallel::rank();

            bool node_is_owned_is_valid = true;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              node_is_owned_is_valid &= (node_is_owned[node_id] == (node_owner[node_id] == rank_number));
            }
            REQUIRE(node_is_owned_is_valid);
          }

          SECTION("edges")
          {
            auto edge_owner = connectivity.edgeOwner();
            REQUIRE(decltype(edge_owner)::item_t == ItemType::edge);
            auto edge_owner_generic = connectivity.owner<ItemType::edge>();
            REQUIRE(decltype(edge_owner_generic)::item_t == ItemType::edge);
            REQUIRE(&(edge_owner[EdgeId{0}]) == &(edge_owner_generic[EdgeId{0}]));

            auto edge_is_owned = connectivity.edgeIsOwned();
            REQUIRE(decltype(edge_is_owned)::item_t == ItemType::edge);
            auto edge_is_owned_generic = connectivity.isOwned<ItemType::edge>();
            REQUIRE(decltype(edge_is_owned_generic)::item_t == ItemType::edge);
            REQUIRE(&(edge_is_owned[EdgeId{0}]) == &(edge_is_owned_generic[EdgeId{0}]));

            REQUIRE(isSynchronized(edge_owner));

            const int rank_number = parallel::rank();

            bool edge_is_owned_is_valid = true;
            for (EdgeId edge_id = 0; edge_id < connectivity.numberOfEdges(); ++edge_id) {
              edge_is_owned_is_valid &= (edge_is_owned[edge_id] == (edge_owner[edge_id] == rank_number));
            }
            REQUIRE(edge_is_owned_is_valid);
          }

          SECTION("faces")
          {
            auto face_owner = connectivity.faceOwner();
            REQUIRE(decltype(face_owner)::item_t == ItemType::face);
            auto face_owner_generic = connectivity.owner<ItemType::face>();
            REQUIRE(decltype(face_owner_generic)::item_t == ItemType::face);
            REQUIRE(&(face_owner[FaceId{0}]) == &(face_owner_generic[FaceId{0}]));

            auto face_is_owned = connectivity.faceIsOwned();
            REQUIRE(decltype(face_is_owned)::item_t == ItemType::face);
            auto face_is_owned_generic = connectivity.isOwned<ItemType::face>();
            REQUIRE(decltype(face_is_owned_generic)::item_t == ItemType::face);
            REQUIRE(&(face_is_owned[FaceId{0}]) == &(face_is_owned_generic[FaceId{0}]));

            REQUIRE(isSynchronized(face_owner));

            const int rank_number = parallel::rank();

            bool face_is_owned_is_valid = true;
            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              face_is_owned_is_valid &= (face_is_owned[face_id] == (face_owner[face_id] == rank_number));
            }
            REQUIRE(face_is_owned_is_valid);
          }

          SECTION("cells")
          {
            auto cell_owner = connectivity.cellOwner();
            REQUIRE(decltype(cell_owner)::item_t == ItemType::cell);
            auto cell_owner_generic = connectivity.owner<ItemType::cell>();
            REQUIRE(decltype(cell_owner_generic)::item_t == ItemType::cell);
            REQUIRE(&(cell_owner[CellId{0}]) == &(cell_owner_generic[CellId{0}]));

            auto cell_is_owned = connectivity.cellIsOwned();
            REQUIRE(decltype(cell_is_owned)::item_t == ItemType::cell);
            auto cell_is_owned_generic = connectivity.isOwned<ItemType::cell>();
            REQUIRE(decltype(cell_is_owned_generic)::item_t == ItemType::cell);
            REQUIRE(&(cell_is_owned[CellId{0}]) == &(cell_is_owned_generic[CellId{0}]));

            REQUIRE(isSynchronized(cell_owner));

            const int rank_number = parallel::rank();

            bool cell_is_owned_is_valid = true;
            for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
              cell_is_owned_is_valid &= (cell_is_owned[cell_id] == (cell_owner[cell_id] == rank_number));
            }
            REQUIRE(cell_is_owned_is_valid);
          }
        }
      }
    }
  }

  SECTION("isBoundary")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d                        = named_mesh.mesh()->get<Mesh<1>>();
          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          auto is_boundary_node = connectivity.isBoundaryNode();
          REQUIRE(decltype(is_boundary_node)::item_t == ItemType::node);
          auto is_boundary_node_generic = connectivity.isBoundary<ItemType::node>();
          REQUIRE(decltype(is_boundary_node_generic)::item_t == ItemType::node);
          REQUIRE(&(is_boundary_node[NodeId{0}]) == &(is_boundary_node_generic[NodeId{0}]));

          auto is_boundary_edge = connectivity.isBoundaryEdge();
          REQUIRE(decltype(is_boundary_edge)::item_t == ItemType::edge);
          auto is_boundary_edge_generic = connectivity.isBoundary<ItemType::edge>();
          REQUIRE(decltype(is_boundary_edge_generic)::item_t == ItemType::edge);
          REQUIRE(&(is_boundary_edge[EdgeId{0}]) == &(is_boundary_edge_generic[EdgeId{0}]));

          auto is_boundary_face = connectivity.isBoundaryFace();
          REQUIRE(decltype(is_boundary_face)::item_t == ItemType::face);
          auto is_boundary_face_generic = connectivity.isBoundary<ItemType::face>();
          REQUIRE(decltype(is_boundary_face_generic)::item_t == ItemType::face);
          REQUIRE(&(is_boundary_face[FaceId{0}]) == &(is_boundary_face_generic[FaceId{0}]));

          // In 1d these values are the same
          REQUIRE(&(is_boundary_node[NodeId(0)]) == &(is_boundary_face[FaceId(0)]));
          REQUIRE(&(is_boundary_edge[EdgeId(0)]) == &(is_boundary_face[FaceId(0)]));

          REQUIRE(isSynchronized(is_boundary_face));

          auto face_is_owned       = connectivity.faceIsOwned();
          auto face_to_cell_matrix = connectivity.faceToCellMatrix();

          bool face_boundary_is_valid = true;
          for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
            if (face_is_owned[face_id]) {
              const bool is_boundary = (face_to_cell_matrix[face_id].size() == 1);
              face_boundary_is_valid &= (is_boundary == is_boundary_face[face_id]);
            }
          }
          REQUIRE(face_boundary_is_valid);
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d                        = named_mesh.mesh()->get<Mesh<2>>();
          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          SECTION("faces/edges")
          {
            auto is_boundary_edge = connectivity.isBoundaryEdge();
            REQUIRE(decltype(is_boundary_edge)::item_t == ItemType::edge);
            auto is_boundary_edge_generic = connectivity.isBoundary<ItemType::edge>();
            REQUIRE(decltype(is_boundary_edge_generic)::item_t == ItemType::edge);
            REQUIRE(&(is_boundary_edge[EdgeId{0}]) == &(is_boundary_edge_generic[EdgeId{0}]));

            auto is_boundary_face = connectivity.isBoundaryFace();
            REQUIRE(decltype(is_boundary_face)::item_t == ItemType::face);
            auto is_boundary_face_generic = connectivity.isBoundary<ItemType::face>();
            REQUIRE(decltype(is_boundary_face_generic)::item_t == ItemType::face);
            REQUIRE(&(is_boundary_face[FaceId{0}]) == &(is_boundary_face_generic[FaceId{0}]));

            // In 2d these values are the same
            REQUIRE(&(is_boundary_edge[EdgeId(0)]) == &(is_boundary_face[FaceId(0)]));

            REQUIRE(isSynchronized(is_boundary_face));

            auto face_is_owned       = connectivity.faceIsOwned();
            auto face_to_cell_matrix = connectivity.faceToCellMatrix();

            bool face_boundary_is_valid = true;
            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              if (face_is_owned[face_id]) {
                const bool is_boundary = (face_to_cell_matrix[face_id].size() == 1);
                face_boundary_is_valid &= (is_boundary == is_boundary_face[face_id]);
              }
            }
            REQUIRE(face_boundary_is_valid);
          }

          SECTION("nodes")
          {
            auto is_boundary_node = connectivity.isBoundaryNode();
            REQUIRE(decltype(is_boundary_node)::item_t == ItemType::node);
            auto is_boundary_node_generic = connectivity.isBoundary<ItemType::node>();
            REQUIRE(decltype(is_boundary_node_generic)::item_t == ItemType::node);
            REQUIRE(&(is_boundary_node[NodeId{0}]) == &(is_boundary_node_generic[NodeId{0}]));

            REQUIRE(isSynchronized(is_boundary_node));

            auto node_is_owned       = connectivity.nodeIsOwned();
            auto node_to_face_matrix = connectivity.nodeToFaceMatrix();

            auto is_boundary_face = connectivity.isBoundaryFace();

            bool node_boundary_is_valid = true;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              if (node_is_owned[node_id]) {
                bool is_boundary    = false;
                auto node_face_list = node_to_face_matrix[node_id];
                for (size_t i_face = 0; i_face < node_face_list.size(); ++i_face) {
                  if (is_boundary_face[node_face_list[i_face]]) {
                    is_boundary = true;
                    break;
                  }
                }

                node_boundary_is_valid &= (is_boundary == is_boundary_node[node_id]);
              }
            }
            REQUIRE(node_boundary_is_valid);
          }
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          SECTION("faces")
          {
            auto is_boundary_face = connectivity.isBoundaryFace();
            REQUIRE(decltype(is_boundary_face)::item_t == ItemType::face);
            auto is_boundary_face_generic = connectivity.isBoundary<ItemType::face>();
            REQUIRE(decltype(is_boundary_face_generic)::item_t == ItemType::face);
            REQUIRE(&(is_boundary_face[FaceId{0}]) == &(is_boundary_face_generic[FaceId{0}]));

            REQUIRE(isSynchronized(is_boundary_face));

            auto face_is_owned       = connectivity.faceIsOwned();
            auto face_to_cell_matrix = connectivity.faceToCellMatrix();

            bool face_boundary_is_valid = true;
            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              if (face_is_owned[face_id]) {
                const bool is_boundary = (face_to_cell_matrix[face_id].size() == 1);
                face_boundary_is_valid &= (is_boundary == is_boundary_face[face_id]);
              }
            }
            REQUIRE(face_boundary_is_valid);
          }

          SECTION("edges")
          {
            auto is_boundary_edge = connectivity.isBoundaryEdge();
            REQUIRE(decltype(is_boundary_edge)::item_t == ItemType::edge);
            auto is_boundary_edge_generic = connectivity.isBoundary<ItemType::edge>();
            REQUIRE(decltype(is_boundary_edge_generic)::item_t == ItemType::edge);
            REQUIRE(&(is_boundary_edge[EdgeId{0}]) == &(is_boundary_edge_generic[EdgeId{0}]));

            REQUIRE(isSynchronized(is_boundary_edge));

            auto edge_is_owned       = connectivity.edgeIsOwned();
            auto edge_to_face_matrix = connectivity.edgeToFaceMatrix();

            auto is_boundary_face = connectivity.isBoundaryFace();

            bool edge_boundary_is_valid = true;
            for (EdgeId edge_id = 0; edge_id < connectivity.numberOfNodes(); ++edge_id) {
              if (edge_is_owned[edge_id]) {
                bool is_boundary    = false;
                auto edge_face_list = edge_to_face_matrix[edge_id];
                for (size_t i_face = 0; i_face < edge_face_list.size(); ++i_face) {
                  if (is_boundary_face[edge_face_list[i_face]]) {
                    is_boundary = true;
                    break;
                  }
                }

                edge_boundary_is_valid &= (is_boundary == is_boundary_edge[edge_id]);
              }
            }
            REQUIRE(edge_boundary_is_valid);
          }

          SECTION("nodes")
          {
            auto is_boundary_node = connectivity.isBoundaryNode();
            REQUIRE(decltype(is_boundary_node)::item_t == ItemType::node);
            auto is_boundary_node_generic = connectivity.isBoundary<ItemType::node>();
            REQUIRE(decltype(is_boundary_node_generic)::item_t == ItemType::node);
            REQUIRE(&(is_boundary_node[NodeId{0}]) == &(is_boundary_node_generic[NodeId{0}]));

            REQUIRE(isSynchronized(is_boundary_node));

            auto node_is_owned       = connectivity.nodeIsOwned();
            auto node_to_face_matrix = connectivity.nodeToFaceMatrix();

            auto is_boundary_face = connectivity.isBoundaryFace();

            bool node_boundary_is_valid = true;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              if (node_is_owned[node_id]) {
                bool is_boundary    = false;
                auto node_face_list = node_to_face_matrix[node_id];
                for (size_t i_face = 0; i_face < node_face_list.size(); ++i_face) {
                  if (is_boundary_face[node_face_list[i_face]]) {
                    is_boundary = true;
                    break;
                  }
                }

                node_boundary_is_valid &= (is_boundary == is_boundary_node[node_id]);
              }
            }
            REQUIRE(node_boundary_is_valid);
          }
        }
      }
    }
  }

  SECTION("item ordering")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          SECTION("cell -> nodes")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<1>>();
            auto xr   = mesh->xr();

            const Connectivity<1>& connectivity = mesh->connectivity();

            auto cell_to_node_matrix = connectivity.cellToNodeMatrix();

            bool is_correct = true;

            for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
              if (xr[cell_to_node_matrix[cell_id][1]][0] < xr[cell_to_node_matrix[cell_id][0]][0]) {
                is_correct = false;
              }
            }
            REQUIRE(is_correct);
          }

          SECTION("node -> cells")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<1>>();

            const Connectivity<1>& connectivity = mesh->connectivity();

            auto node_to_cell_matrix = connectivity.nodeToCellMatrix();
            auto cell_number         = connectivity.cellNumber();

            bool is_correct = true;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              auto cell_node_list = node_to_cell_matrix[node_id];
              for (size_t i_node = 0; i_node < cell_node_list.size() - 1; ++i_node) {
                is_correct &= (cell_number[cell_node_list[i_node]] < cell_number[cell_node_list[i_node + 1]]);
              }
            }
            REQUIRE(is_correct);
          }
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          SECTION("face -> nodes")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<2>>();

            const Connectivity<2>& connectivity = mesh->connectivity();

            auto face_to_node_matrix = connectivity.faceToNodeMatrix();
            auto node_number         = connectivity.nodeNumber();

            bool is_correct = true;

            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              auto face_node_list = face_to_node_matrix[face_id];
              if (node_number[face_node_list[1]] < node_number[face_node_list[0]]) {
                is_correct = false;
              }
            }
            REQUIRE(is_correct);
          }

          SECTION("node -> faces")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<2>>();

            const Connectivity<2>& connectivity = mesh->connectivity();

            auto node_to_face_matrix = connectivity.nodeToFaceMatrix();
            auto face_number         = connectivity.faceNumber();

            bool is_correct = true;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              auto face_node_list = node_to_face_matrix[node_id];
              for (size_t i_node = 0; i_node < face_node_list.size() - 1; ++i_node) {
                is_correct &= (face_number[face_node_list[i_node]] < face_number[face_node_list[i_node + 1]]);
              }
            }
            REQUIRE(is_correct);
          }

          SECTION("node -> cells")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<2>>();

            const Connectivity<2>& connectivity = mesh->connectivity();

            auto node_to_cell_matrix = connectivity.nodeToCellMatrix();
            auto cell_number         = connectivity.cellNumber();

            bool is_correct = true;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              auto cell_node_list = node_to_cell_matrix[node_id];
              for (size_t i_node = 0; i_node < cell_node_list.size() - 1; ++i_node) {
                is_correct &= (cell_number[cell_node_list[i_node]] < cell_number[cell_node_list[i_node + 1]]);
              }
            }
            REQUIRE(is_correct);
          }

          SECTION("face -> cells")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<2>>();

            const Connectivity<2>& connectivity = mesh->connectivity();

            auto face_to_cell_matrix = connectivity.faceToCellMatrix();
            auto cell_number         = connectivity.cellNumber();

            bool is_correct = true;
            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              auto cell_face_list = face_to_cell_matrix[face_id];
              for (size_t i_face = 0; i_face < cell_face_list.size() - 1; ++i_face) {
                is_correct &= (cell_number[cell_face_list[i_face]] < cell_number[cell_face_list[i_face + 1]]);
              }
            }
            REQUIRE(is_correct);
          }
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          SECTION("edge -> nodes")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<3>>();

            const Connectivity<3>& connectivity = mesh->connectivity();

            auto edge_to_node_matrix = connectivity.edgeToNodeMatrix();
            auto node_number         = connectivity.nodeNumber();

            bool is_correct = true;

            for (EdgeId edge_id = 0; edge_id < connectivity.numberOfEdges(); ++edge_id) {
              auto edge_node_list = edge_to_node_matrix[edge_id];
              if (node_number[edge_node_list[1]] < node_number[edge_node_list[0]]) {
                is_correct = false;
              }
            }
            REQUIRE(is_correct);
          }

          SECTION("face -> nodes")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<3>>();

            const Connectivity<3>& connectivity = mesh->connectivity();

            auto face_to_node_matrix = connectivity.faceToNodeMatrix();
            auto node_number         = connectivity.nodeNumber();

            bool is_correct = true;

            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              auto face_node_list = face_to_node_matrix[face_id];
              for (size_t i = 1; i < face_node_list.size() - 1; ++i) {
                if (node_number[face_node_list[i]] < node_number[face_node_list[0]]) {
                  is_correct = false;
                }
              }
              for (size_t i = 2; i < face_node_list.size() - 1; ++i) {
                if (node_number[face_node_list[i]] < node_number[face_node_list[1]]) {
                  is_correct = false;
                }
              }
            }
            REQUIRE(is_correct);
          }

          SECTION("node -> edges")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<3>>();

            const Connectivity<3>& connectivity = mesh->connectivity();

            REQUIRE(checkConnectivityOrdering(connectivity));

            auto node_to_edge_matrix = connectivity.nodeToEdgeMatrix();
            auto edge_number         = connectivity.edgeNumber();

            bool is_correct = true;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              auto edge_node_list = node_to_edge_matrix[node_id];
              for (size_t i_node = 0; i_node < edge_node_list.size() - 1; ++i_node) {
                is_correct &= (edge_number[edge_node_list[i_node]] < edge_number[edge_node_list[i_node + 1]]);
              }
            }

            REQUIRE(is_correct);
          }

          SECTION("node -> faces")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<3>>();

            const Connectivity<3>& connectivity = mesh->connectivity();

            auto node_to_face_matrix = connectivity.nodeToFaceMatrix();
            auto face_number         = connectivity.faceNumber();

            bool is_correct = true;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              auto face_node_list = node_to_face_matrix[node_id];
              for (size_t i_node = 0; i_node < face_node_list.size() - 1; ++i_node) {
                is_correct &= (face_number[face_node_list[i_node]] < face_number[face_node_list[i_node + 1]]);
              }
            }
            REQUIRE(is_correct);
          }

          SECTION("node -> cells")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<3>>();

            const Connectivity<3>& connectivity = mesh->connectivity();

            auto node_to_cell_matrix = connectivity.nodeToCellMatrix();
            auto cell_number         = connectivity.cellNumber();

            bool is_correct = true;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              auto cell_node_list = node_to_cell_matrix[node_id];
              for (size_t i_node = 0; i_node < cell_node_list.size() - 1; ++i_node) {
                is_correct &= (cell_number[cell_node_list[i_node]] < cell_number[cell_node_list[i_node + 1]]);
              }
            }
            REQUIRE(is_correct);
          }

          SECTION("edge -> faces")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<3>>();

            const Connectivity<3>& connectivity = mesh->connectivity();

            auto edge_to_face_matrix = connectivity.edgeToFaceMatrix();
            auto face_number         = connectivity.faceNumber();

            bool is_correct = true;
            for (EdgeId edge_id = 0; edge_id < connectivity.numberOfEdges(); ++edge_id) {
              auto face_edge_list = edge_to_face_matrix[edge_id];
              for (size_t i_edge = 0; i_edge < face_edge_list.size() - 1; ++i_edge) {
                is_correct &= (face_number[face_edge_list[i_edge]] < face_number[face_edge_list[i_edge + 1]]);
              }
            }
            REQUIRE(is_correct);
          }

          SECTION("edge -> cells")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<3>>();

            const Connectivity<3>& connectivity = mesh->connectivity();

            auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();
            auto cell_number         = connectivity.cellNumber();

            bool is_correct = true;
            for (EdgeId edge_id = 0; edge_id < connectivity.numberOfEdges(); ++edge_id) {
              auto cell_edge_list = edge_to_cell_matrix[edge_id];
              for (size_t i_edge = 0; i_edge < cell_edge_list.size() - 1; ++i_edge) {
                is_correct &= (cell_number[cell_edge_list[i_edge]] < cell_number[cell_edge_list[i_edge + 1]]);
              }
            }
            REQUIRE(is_correct);
          }

          SECTION("face -> cells")
          {
            auto mesh = named_mesh.mesh()->get<Mesh<3>>();

            const Connectivity<3>& connectivity = mesh->connectivity();

            auto face_to_cell_matrix = connectivity.faceToCellMatrix();
            auto cell_number         = connectivity.cellNumber();

            bool is_correct = true;
            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              auto cell_face_list = face_to_cell_matrix[face_id];
              for (size_t i_face = 0; i_face < cell_face_list.size() - 1; ++i_face) {
                is_correct &= (cell_number[cell_face_list[i_face]] < cell_number[cell_face_list[i_face + 1]]);
              }
            }
            REQUIRE(is_correct);
          }
        }
      }
    }
  }

  SECTION("ItemLocalNumbersInTheirSubItems")
  {
    auto check_item_local_numbers_in_their_subitems = [](auto item_to_subitem_matrix, auto subitem_to_item_matrix,
                                                         auto item_local_numbers_in_subitems) -> bool {
      using ItemId    = typename decltype(item_to_subitem_matrix)::SourceItemId;
      using SubItemId = typename decltype(item_to_subitem_matrix)::TargetItemId;

      static_assert(std::is_same_v<typename decltype(item_to_subitem_matrix)::SourceItemId,
                                   typename decltype(subitem_to_item_matrix)::TargetItemId>);
      static_assert(std::is_same_v<typename decltype(item_to_subitem_matrix)::TargetItemId,
                                   typename decltype(subitem_to_item_matrix)::SourceItemId>);
      static_assert(std::is_same_v<typename decltype(item_to_subitem_matrix)::SourceItemId,
                                   typename decltype(item_local_numbers_in_subitems)::ItemId>);

      for (ItemId item_id = 0; item_id < item_local_numbers_in_subitems.numberOfItems(); ++item_id) {
        auto item_subitem_list = item_to_subitem_matrix[item_id];
        auto item_numbers      = item_local_numbers_in_subitems.itemArray(item_id);

        for (size_t i_item_subitem = 0; i_item_subitem < item_subitem_list.size(); ++i_item_subitem) {
          const SubItemId node_cell_id = item_subitem_list[i_item_subitem];
          const size_t i_local_item    = item_numbers[i_item_subitem];
          if (subitem_to_item_matrix[node_cell_id][i_local_item] != item_id) {
            return false;
          }
        }
      }
      return true;
    };

    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh                           = named_mesh.mesh()->get<Mesh<1>>();
          const Connectivity<1>& connectivity = mesh->connectivity();

          SECTION("node <-> cell")
          {
            auto node_to_cell_matrix = connectivity.nodeToCellMatrix();
            auto cell_to_node_matrix = connectivity.cellToNodeMatrix();

            auto node_local_numbers_in_their_cells = connectivity.nodeLocalNumbersInTheirCells();
            REQUIRE(check_item_local_numbers_in_their_subitems(node_to_cell_matrix, cell_to_node_matrix,
                                                               node_local_numbers_in_their_cells));

            auto cell_local_numbers_in_their_nodes = connectivity.cellLocalNumbersInTheirNodes();
            REQUIRE(check_item_local_numbers_in_their_subitems(cell_to_node_matrix, node_to_cell_matrix,
                                                               cell_local_numbers_in_their_nodes));
          }

          SECTION("edge <-> cell")
          {
            auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();
            auto cell_to_edge_matrix = connectivity.cellToEdgeMatrix();

            auto edge_local_numbers_in_their_cells = connectivity.edgeLocalNumbersInTheirCells();
            REQUIRE(check_item_local_numbers_in_their_subitems(edge_to_cell_matrix, cell_to_edge_matrix,
                                                               edge_local_numbers_in_their_cells));

            auto cell_local_numbers_in_their_edges = connectivity.cellLocalNumbersInTheirEdges();
            REQUIRE(check_item_local_numbers_in_their_subitems(cell_to_edge_matrix, edge_to_cell_matrix,
                                                               cell_local_numbers_in_their_edges));
          }

          SECTION("face <-> cell")
          {
            auto face_to_cell_matrix = connectivity.faceToCellMatrix();
            auto cell_to_face_matrix = connectivity.cellToFaceMatrix();

            auto face_local_numbers_in_their_cells = connectivity.faceLocalNumbersInTheirCells();
            REQUIRE(check_item_local_numbers_in_their_subitems(face_to_cell_matrix, cell_to_face_matrix,
                                                               face_local_numbers_in_their_cells));

            auto cell_local_numbers_in_their_faces = connectivity.cellLocalNumbersInTheirFaces();
            REQUIRE(check_item_local_numbers_in_their_subitems(cell_to_face_matrix, face_to_cell_matrix,
                                                               cell_local_numbers_in_their_faces));
          }

          SECTION("shared data")
          {
            auto face_local_numbers_in_their_cells = connectivity.faceLocalNumbersInTheirCells();
            auto edge_local_numbers_in_their_cells = connectivity.edgeLocalNumbersInTheirCells();
            auto node_local_numbers_in_their_cells = connectivity.nodeLocalNumbersInTheirCells();

            REQUIRE(&(face_local_numbers_in_their_cells[0]) == &(edge_local_numbers_in_their_cells[0]));
            REQUIRE(&(face_local_numbers_in_their_cells[0]) == &(node_local_numbers_in_their_cells[0]));

            auto cell_local_numbers_in_their_faces = connectivity.cellLocalNumbersInTheirFaces();
            auto cell_local_numbers_in_their_edges = connectivity.cellLocalNumbersInTheirEdges();
            auto cell_local_numbers_in_their_nodes = connectivity.cellLocalNumbersInTheirNodes();

            REQUIRE(&(cell_local_numbers_in_their_faces[0]) == &(cell_local_numbers_in_their_edges[0]));
            REQUIRE(&(cell_local_numbers_in_their_faces[0]) == &(cell_local_numbers_in_their_nodes[0]));
          }
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh                           = named_mesh.mesh()->get<Mesh<2>>();
          const Connectivity<2>& connectivity = mesh->connectivity();

          SECTION("node <-> cell")
          {
            auto node_to_cell_matrix = connectivity.nodeToCellMatrix();
            auto cell_to_node_matrix = connectivity.cellToNodeMatrix();

            auto node_local_numbers_in_their_cells = connectivity.nodeLocalNumbersInTheirCells();
            REQUIRE(check_item_local_numbers_in_their_subitems(node_to_cell_matrix, cell_to_node_matrix,
                                                               node_local_numbers_in_their_cells));

            auto cell_local_numbers_in_their_nodes = connectivity.cellLocalNumbersInTheirNodes();
            REQUIRE(check_item_local_numbers_in_their_subitems(cell_to_node_matrix, node_to_cell_matrix,
                                                               cell_local_numbers_in_their_nodes));
          }

          SECTION("edge <-> cell")
          {
            auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();
            auto cell_to_edge_matrix = connectivity.cellToEdgeMatrix();

            auto edge_local_numbers_in_their_cells = connectivity.edgeLocalNumbersInTheirCells();
            REQUIRE(check_item_local_numbers_in_their_subitems(edge_to_cell_matrix, cell_to_edge_matrix,
                                                               edge_local_numbers_in_their_cells));

            auto cell_local_numbers_in_their_edges = connectivity.cellLocalNumbersInTheirEdges();
            REQUIRE(check_item_local_numbers_in_their_subitems(cell_to_edge_matrix, edge_to_cell_matrix,
                                                               cell_local_numbers_in_their_edges));
          }

          SECTION("face <-> cell")
          {
            auto face_to_cell_matrix = connectivity.faceToCellMatrix();
            auto cell_to_face_matrix = connectivity.cellToFaceMatrix();

            auto face_local_numbers_in_their_cells = connectivity.faceLocalNumbersInTheirCells();
            REQUIRE(check_item_local_numbers_in_their_subitems(face_to_cell_matrix, cell_to_face_matrix,
                                                               face_local_numbers_in_their_cells));

            auto cell_local_numbers_in_their_faces = connectivity.cellLocalNumbersInTheirFaces();
            REQUIRE(check_item_local_numbers_in_their_subitems(cell_to_face_matrix, face_to_cell_matrix,
                                                               cell_local_numbers_in_their_faces));
          }

          SECTION("node <-> face")
          {
            auto node_to_face_matrix = connectivity.nodeToFaceMatrix();
            auto face_to_node_matrix = connectivity.faceToNodeMatrix();

            auto node_local_numbers_in_their_faces = connectivity.nodeLocalNumbersInTheirFaces();
            REQUIRE(check_item_local_numbers_in_their_subitems(node_to_face_matrix, face_to_node_matrix,
                                                               node_local_numbers_in_their_faces));

            auto face_local_numbers_in_their_nodes = connectivity.faceLocalNumbersInTheirNodes();
            REQUIRE(check_item_local_numbers_in_their_subitems(face_to_node_matrix, node_to_face_matrix,
                                                               face_local_numbers_in_their_nodes));
          }

          SECTION("node <-> edge")
          {
            auto node_to_edge_matrix = connectivity.nodeToEdgeMatrix();
            auto edge_to_node_matrix = connectivity.edgeToNodeMatrix();

            auto node_local_numbers_in_their_edges = connectivity.nodeLocalNumbersInTheirEdges();
            REQUIRE(check_item_local_numbers_in_their_subitems(node_to_edge_matrix, edge_to_node_matrix,
                                                               node_local_numbers_in_their_edges));

            auto edge_local_numbers_in_their_nodes = connectivity.edgeLocalNumbersInTheirNodes();
            REQUIRE(check_item_local_numbers_in_their_subitems(edge_to_node_matrix, node_to_edge_matrix,
                                                               edge_local_numbers_in_their_nodes));
          }

          SECTION("shared data")
          {
            auto face_local_numbers_in_their_cells = connectivity.faceLocalNumbersInTheirCells();
            auto edge_local_numbers_in_their_cells = connectivity.edgeLocalNumbersInTheirCells();
            auto face_local_numbers_in_their_nodes = connectivity.faceLocalNumbersInTheirNodes();
            auto edge_local_numbers_in_their_nodes = connectivity.edgeLocalNumbersInTheirNodes();

            REQUIRE(&(face_local_numbers_in_their_cells[0]) == &(edge_local_numbers_in_their_cells[0]));
            REQUIRE(&(face_local_numbers_in_their_nodes[0]) == &(edge_local_numbers_in_their_nodes[0]));

            auto cell_local_numbers_in_their_faces = connectivity.cellLocalNumbersInTheirFaces();
            auto cell_local_numbers_in_their_edges = connectivity.cellLocalNumbersInTheirEdges();
            auto node_local_numbers_in_their_faces = connectivity.nodeLocalNumbersInTheirFaces();
            auto node_local_numbers_in_their_edges = connectivity.nodeLocalNumbersInTheirEdges();

            REQUIRE(&(cell_local_numbers_in_their_faces[0]) == &(cell_local_numbers_in_their_edges[0]));
            REQUIRE(&(node_local_numbers_in_their_faces[0]) == &(node_local_numbers_in_their_edges[0]));
          }
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh                           = named_mesh.mesh()->get<Mesh<3>>();
          const Connectivity<3>& connectivity = mesh->connectivity();

          SECTION("node <-> cell")
          {
            auto node_to_cell_matrix = connectivity.nodeToCellMatrix();
            auto cell_to_node_matrix = connectivity.cellToNodeMatrix();

            auto node_local_numbers_in_their_cells = connectivity.nodeLocalNumbersInTheirCells();
            REQUIRE(check_item_local_numbers_in_their_subitems(node_to_cell_matrix, cell_to_node_matrix,
                                                               node_local_numbers_in_their_cells));

            auto cell_local_numbers_in_their_nodes = connectivity.cellLocalNumbersInTheirNodes();
            REQUIRE(check_item_local_numbers_in_their_subitems(cell_to_node_matrix, node_to_cell_matrix,
                                                               cell_local_numbers_in_their_nodes));
          }

          SECTION("edge <-> cell")
          {
            auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();
            auto cell_to_edge_matrix = connectivity.cellToEdgeMatrix();

            auto edge_local_numbers_in_their_cells = connectivity.edgeLocalNumbersInTheirCells();
            REQUIRE(check_item_local_numbers_in_their_subitems(edge_to_cell_matrix, cell_to_edge_matrix,
                                                               edge_local_numbers_in_their_cells));

            auto cell_local_numbers_in_their_edges = connectivity.cellLocalNumbersInTheirEdges();
            REQUIRE(check_item_local_numbers_in_their_subitems(cell_to_edge_matrix, edge_to_cell_matrix,
                                                               cell_local_numbers_in_their_edges));
          }

          SECTION("face <-> cell")
          {
            auto face_to_cell_matrix = connectivity.faceToCellMatrix();
            auto cell_to_face_matrix = connectivity.cellToFaceMatrix();

            auto face_local_numbers_in_their_cells = connectivity.faceLocalNumbersInTheirCells();
            REQUIRE(check_item_local_numbers_in_their_subitems(face_to_cell_matrix, cell_to_face_matrix,
                                                               face_local_numbers_in_their_cells));

            auto cell_local_numbers_in_their_faces = connectivity.cellLocalNumbersInTheirFaces();
            REQUIRE(check_item_local_numbers_in_their_subitems(cell_to_face_matrix, face_to_cell_matrix,
                                                               cell_local_numbers_in_their_faces));
          }

          SECTION("node <-> face")
          {
            auto node_to_face_matrix = connectivity.nodeToFaceMatrix();
            auto face_to_node_matrix = connectivity.faceToNodeMatrix();

            auto node_local_numbers_in_their_faces = connectivity.nodeLocalNumbersInTheirFaces();
            REQUIRE(check_item_local_numbers_in_their_subitems(node_to_face_matrix, face_to_node_matrix,
                                                               node_local_numbers_in_their_faces));

            auto face_local_numbers_in_their_nodes = connectivity.faceLocalNumbersInTheirNodes();
            REQUIRE(check_item_local_numbers_in_their_subitems(face_to_node_matrix, node_to_face_matrix,
                                                               face_local_numbers_in_their_nodes));
          }

          SECTION("edge <-> face")
          {
            auto edge_to_face_matrix = connectivity.edgeToFaceMatrix();
            auto face_to_edge_matrix = connectivity.faceToEdgeMatrix();

            auto edge_local_numbers_in_their_faces = connectivity.edgeLocalNumbersInTheirFaces();
            REQUIRE(check_item_local_numbers_in_their_subitems(edge_to_face_matrix, face_to_edge_matrix,
                                                               edge_local_numbers_in_their_faces));

            auto face_local_numbers_in_their_edges = connectivity.faceLocalNumbersInTheirEdges();
            REQUIRE(check_item_local_numbers_in_their_subitems(face_to_edge_matrix, edge_to_face_matrix,
                                                               face_local_numbers_in_their_edges));
          }

          SECTION("node <-> edge")
          {
            auto node_to_edge_matrix = connectivity.nodeToEdgeMatrix();
            auto edge_to_node_matrix = connectivity.edgeToNodeMatrix();

            auto node_local_numbers_in_their_edges = connectivity.nodeLocalNumbersInTheirEdges();
            REQUIRE(check_item_local_numbers_in_their_subitems(node_to_edge_matrix, edge_to_node_matrix,
                                                               node_local_numbers_in_their_edges));

            auto edge_local_numbers_in_their_nodes = connectivity.edgeLocalNumbersInTheirNodes();
            REQUIRE(check_item_local_numbers_in_their_subitems(edge_to_node_matrix, node_to_edge_matrix,
                                                               edge_local_numbers_in_their_nodes));
          }
        }
      }
    }
  }
}
