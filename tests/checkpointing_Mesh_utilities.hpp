#ifndef CHECKPOINTING_MESH_UTILITIES_HPP
#define CHECKPOINTING_MESH_UTILITIES_HPP

#include <checkpointing_Connectivity_utilities.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>

namespace test_only
{

PUGS_INLINE
std::shared_ptr<const MeshVariant>
duplicateMesh(std::shared_ptr<const MeshVariant> mesh_v)
{
  std::shared_ptr<const MeshVariant> new_mesh_v;

  std::visit(
    [&](auto&& mesh) {
      using MeshType = mesh_type_t<decltype(mesh)>;
      if constexpr (is_polygonal_mesh_v<MeshType>) {
        using Rd = TinyVector<MeshType::Dimension>;
        auto new_connectivity =
          std::dynamic_pointer_cast<const typename MeshType::Connectivity>(duplicateConnectivity(mesh->connectivity()));

        NodeValue<const Rd> new_xr{*new_connectivity, mesh->xr().arrayView()};
        new_mesh_v = std::make_shared<MeshVariant>(std::make_shared<const MeshType>(new_connectivity, new_xr));
      } else {
        throw NotImplementedError("unexpected mesh type");
      }
    },
    mesh_v->variant());

  return new_mesh_v;
}

PUGS_INLINE bool
isSameMesh(std::shared_ptr<const MeshVariant> mesh_v, std::shared_ptr<const MeshVariant> read_mesh_v)
{
  auto same_value = [](const auto& a, const auto& b) -> bool {
    bool same = true;
    for (size_t i = 0; i < a.size(); ++i) {
      same &= (a[i] == b[i]);
    }
    return parallel::allReduceAnd(same);
  };

  bool same = true;

  std::visit(
    [&](auto&& mesh, auto&& read_mesh) {
      using MeshType     = mesh_type_t<decltype(mesh)>;
      using ReadMeshType = mesh_type_t<decltype(read_mesh)>;
      if constexpr (std::is_same_v<MeshType, ReadMeshType>) {
        same &= test_only::isSameConnectivity(mesh->connectivity(), read_mesh->connectivity());
        same &= same_value(mesh->xr().arrayView(), read_mesh->xr().arrayView());
      } else {
        throw UnexpectedError("mesh of different kind");
      }
    },
    mesh_v->variant(), read_mesh_v->variant());

  return same;
}

}   // namespace test_only

#endif   // CHECKPOINTING_MESH_UTILITIES_HPP
