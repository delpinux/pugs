#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/node_processor/ASTNodeListProcessor.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeListProcessor", "[language]")
{
  rang::setControlMode(rang::control::Off);

  std::string_view data = R"(
3;
true;
2.3;
)";

  TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
  auto ast = ASTBuilder::build(input);

  ASTSymbolTableBuilder{*ast};
  ASTNodeDataTypeBuilder{*ast};

  ASTNodeDeclarationToAffectationConverter{*ast};
  ASTNodeTypeCleaner<language::var_declaration>{*ast};

  ASTNodeExpressionBuilder{*ast};
  ExecutionPolicy exec_policy;
  ASTExecutionStack::create();
  ast->execute(exec_policy);
  ASTExecutionStack::destroy();

  REQUIRE(ast->children[0]->is_type<language::integer>());
  REQUIRE(ast->children[1]->is_type<language::true_kw>());
  REQUIRE(ast->children[2]->is_type<language::real>());

  REQUIRE(ast->m_node_processor->typeIdName() == demangle<ASTNodeListProcessor>());
  ast->m_symbol_table->clearValues();
}
