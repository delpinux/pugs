#ifndef TEST_EMBEDDED_DISCRETE_FUNCTION_MATH_FUNCTIONS_HPP
#define TEST_EMBEDDED_DISCRETE_FUNCTION_MATH_FUNCTIONS_HPP

#define CHECK_EMBEDDED_VH_TO_VH_FUNCTION_EVALUATION(P_U, FCT, U_TYPE, FU_TYPE) \
  {                                                                            \
    std::shared_ptr p_fu = ::FCT(P_U);                                         \
                                                                               \
    REQUIRE(p_fu.use_count() > 0);                                             \
                                                                               \
    const U_TYPE& u   = P_U->get<U_TYPE>();                                    \
    const FU_TYPE& fu = p_fu->get<FU_TYPE>();                                  \
                                                                               \
    bool is_same  = true;                                                      \
    auto u_values = u.cellValues();                                            \
    for (CellId cell_id = 0; cell_id < u_values.numberOfItems(); ++cell_id) {  \
      using namespace std;                                                     \
      if (fu[cell_id] != FCT(u_values[cell_id])) {                             \
        is_same = false;                                                       \
        break;                                                                 \
      }                                                                        \
    }                                                                          \
                                                                               \
    REQUIRE(is_same);                                                          \
  }

#define CHECK_EMBEDDED_VH2_TO_VH_FUNCTION_EVALUATION(P_U, P_V, FCT, U_TYPE, V_TYPE, FUV_TYPE) \
  {                                                                                           \
    std::shared_ptr p_fuv = ::FCT(P_U, P_V);                                                  \
                                                                                              \
    REQUIRE(p_fuv.use_count() > 0);                                                           \
                                                                                              \
    const U_TYPE& u     = P_U->get<U_TYPE>();                                                 \
    const V_TYPE& v     = P_V->get<V_TYPE>();                                                 \
    const FUV_TYPE& fuv = p_fuv->get<FUV_TYPE>();                                             \
                                                                                              \
    bool is_same  = true;                                                                     \
    auto u_values = u.cellValues();                                                           \
    auto v_values = v.cellValues();                                                           \
    for (CellId cell_id = 0; cell_id < values.numberOfItems(); ++cell_id) {                   \
      using namespace std;                                                                    \
      if (fuv[cell_id] != FCT(u_values[cell_id], v_values[cell_id])) {                        \
        is_same = false;                                                                      \
        break;                                                                                \
      }                                                                                       \
    }                                                                                         \
                                                                                              \
    REQUIRE(is_same);                                                                         \
  }

#define CHECK_EMBEDDED_VHxW_TO_VH_FUNCTION_EVALUATION(P_U, V, FCT, U_TYPE, FUV_TYPE) \
  {                                                                                  \
    std::shared_ptr p_fuv = ::FCT(P_U, V);                                           \
                                                                                     \
    REQUIRE(p_fuv.use_count() > 0);                                                  \
    const U_TYPE& u     = P_U->get<U_TYPE>();                                        \
    const FUV_TYPE& fuv = p_fuv->get<FUV_TYPE>();                                    \
                                                                                     \
    bool is_same  = true;                                                            \
    auto u_values = u.cellValues();                                                  \
    for (CellId cell_id = 0; cell_id < values.numberOfItems(); ++cell_id) {          \
      using namespace std;                                                           \
      if (fuv[cell_id] != FCT(u_values[cell_id], V)) {                               \
        is_same = false;                                                             \
        break;                                                                       \
      }                                                                              \
    }                                                                                \
                                                                                     \
    REQUIRE(is_same);                                                                \
  }

#define CHECK_EMBEDDED_WxVH_TO_VH_FUNCTION_EVALUATION(U, P_V, FCT, V_TYPE, FUV_TYPE) \
  {                                                                                  \
    std::shared_ptr p_fuv = ::FCT(U, P_V);                                           \
                                                                                     \
    REQUIRE(p_fuv.use_count() > 0);                                                  \
                                                                                     \
    const V_TYPE& v     = P_V->get<V_TYPE>();                                        \
    const FUV_TYPE& fuv = p_fuv->get<FUV_TYPE>();                                    \
                                                                                     \
    bool is_same  = true;                                                            \
    auto v_values = v.cellValues();                                                  \
    for (CellId cell_id = 0; cell_id < values.numberOfItems(); ++cell_id) {          \
      using namespace std;                                                           \
      if (fuv[cell_id] != FCT(U, v_values[cell_id])) {                               \
        is_same = false;                                                             \
        break;                                                                       \
      }                                                                              \
    }                                                                                \
                                                                                     \
    REQUIRE(is_same);                                                                \
  }

#endif   // TEST_EMBEDDED_DISCRETE_FUNCTION_MATH_FUNCTIONS_HPP
