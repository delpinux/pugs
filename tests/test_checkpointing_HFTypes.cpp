#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/checkpointing/DiscreteFunctionTypeHFType.hpp>
#include <utils/checkpointing/DualMeshTypeHFType.hpp>
#include <utils/checkpointing/EigenvalueSolverOptionsHFType.hpp>
#include <utils/checkpointing/IBoundaryConditionDescriptorHFType.hpp>
#include <utils/checkpointing/IBoundaryDescriptorHFType.hpp>
#include <utils/checkpointing/IInterfaceDescriptorHFType.hpp>
#include <utils/checkpointing/INamedDiscreteDataHFType.hpp>
#include <utils/checkpointing/IWriterHFType.hpp>
#include <utils/checkpointing/IZoneDescriptorHFType.hpp>
#include <utils/checkpointing/ItemTypeHFType.hpp>
#include <utils/checkpointing/LinearSolverOptionsHFType.hpp>
#include <utils/checkpointing/OStreamTypeHFType.hpp>
#include <utils/checkpointing/ParallelCheckerHFType.hpp>
#include <utils/checkpointing/QuadratureTypeHFType.hpp>
#include <utils/checkpointing/RefItemListHFType.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("HFTypes", "[utils/checkpointing]")
{
  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});
    HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

    SECTION("DiscreteFunctionTypeHFType")
    {
      file.createAttribute("P0", DiscreteFunctionType::P0);
      file.createAttribute("P0Vector", DiscreteFunctionType::P0Vector);

      REQUIRE(file.getAttribute("P0").read<DiscreteFunctionType>() == DiscreteFunctionType::P0);
      REQUIRE(file.getAttribute("P0Vector").read<DiscreteFunctionType>() == DiscreteFunctionType::P0Vector);
    }

    SECTION("DualMeshTypeHFType")
    {
      file.createAttribute("Dual1D", DualMeshType::Dual1D);
      file.createAttribute("Diamond", DualMeshType::Diamond);
      file.createAttribute("Median", DualMeshType::Median);

      REQUIRE(file.getAttribute("Dual1D").read<DualMeshType>() == DualMeshType::Dual1D);
      REQUIRE(file.getAttribute("Diamond").read<DualMeshType>() == DualMeshType::Diamond);
      REQUIRE(file.getAttribute("Median").read<DualMeshType>() == DualMeshType::Median);
    }

    SECTION("IBoundaryConditionDescriptorHFType")
    {
      file.createAttribute("axis", IBoundaryConditionDescriptor::Type::axis);
      file.createAttribute("dirichlet", IBoundaryConditionDescriptor::Type::dirichlet);
      file.createAttribute("external", IBoundaryConditionDescriptor::Type::external);
      file.createAttribute("fixed", IBoundaryConditionDescriptor::Type::fixed);
      file.createAttribute("fourier", IBoundaryConditionDescriptor::Type::fourier);
      file.createAttribute("free", IBoundaryConditionDescriptor::Type::free);
      file.createAttribute("inflow", IBoundaryConditionDescriptor::Type::inflow);
      file.createAttribute("neumann", IBoundaryConditionDescriptor::Type::neumann);
      file.createAttribute("outflow", IBoundaryConditionDescriptor::Type::outflow);
      file.createAttribute("symmetry", IBoundaryConditionDescriptor::Type::symmetry);

      REQUIRE(file.getAttribute("axis").read<IBoundaryConditionDescriptor::Type>() ==
              IBoundaryConditionDescriptor::Type::axis);
      REQUIRE(file.getAttribute("dirichlet").read<IBoundaryConditionDescriptor::Type>() ==
              IBoundaryConditionDescriptor::Type::dirichlet);
      REQUIRE(file.getAttribute("external").read<IBoundaryConditionDescriptor::Type>() ==
              IBoundaryConditionDescriptor::Type::external);
      REQUIRE(file.getAttribute("fixed").read<IBoundaryConditionDescriptor::Type>() ==
              IBoundaryConditionDescriptor::Type::fixed);
      REQUIRE(file.getAttribute("fourier").read<IBoundaryConditionDescriptor::Type>() ==
              IBoundaryConditionDescriptor::Type::fourier);
      REQUIRE(file.getAttribute("free").read<IBoundaryConditionDescriptor::Type>() ==
              IBoundaryConditionDescriptor::Type::free);
      REQUIRE(file.getAttribute("inflow").read<IBoundaryConditionDescriptor::Type>() ==
              IBoundaryConditionDescriptor::Type::inflow);
      REQUIRE(file.getAttribute("neumann").read<IBoundaryConditionDescriptor::Type>() ==
              IBoundaryConditionDescriptor::Type::neumann);
      REQUIRE(file.getAttribute("outflow").read<IBoundaryConditionDescriptor::Type>() ==
              IBoundaryConditionDescriptor::Type::outflow);
      REQUIRE(file.getAttribute("symmetry").read<IBoundaryConditionDescriptor::Type>() ==
              IBoundaryConditionDescriptor::Type::symmetry);
    }

    SECTION("IBoundaryDescriptorHFType")
    {
      file.createAttribute("named", IBoundaryDescriptor::Type::named);
      file.createAttribute("numbered", IBoundaryDescriptor::Type::numbered);

      REQUIRE(file.getAttribute("named").read<IBoundaryDescriptor::Type>() == IBoundaryDescriptor::Type::named);
      REQUIRE(file.getAttribute("numbered").read<IBoundaryDescriptor::Type>() == IBoundaryDescriptor::Type::numbered);
    }

    SECTION("IInterfaceDescriptorHFType")
    {
      file.createAttribute("named", IInterfaceDescriptor::Type::named);
      file.createAttribute("numbered", IInterfaceDescriptor::Type::numbered);

      REQUIRE(file.getAttribute("named").read<IInterfaceDescriptor::Type>() == IInterfaceDescriptor::Type::named);
      REQUIRE(file.getAttribute("numbered").read<IInterfaceDescriptor::Type>() == IInterfaceDescriptor::Type::numbered);
    }

    SECTION("INamedDiscreteDataHFType")
    {
      file.createAttribute("discrete_function", INamedDiscreteData::Type::discrete_function);
      file.createAttribute("item_array", INamedDiscreteData::Type::item_array);
      file.createAttribute("item_value", INamedDiscreteData::Type::item_value);

      REQUIRE(file.getAttribute("discrete_function").read<INamedDiscreteData::Type>() ==
              INamedDiscreteData::Type::discrete_function);
      REQUIRE(file.getAttribute("item_array").read<INamedDiscreteData::Type>() == INamedDiscreteData::Type::item_array);
      REQUIRE(file.getAttribute("item_value").read<INamedDiscreteData::Type>() == INamedDiscreteData::Type::item_value);
    }

    SECTION("IWriterHFType")
    {
      file.createAttribute("gnuplot", IWriter::Type::gnuplot);
      file.createAttribute("gnuplot_1d", IWriter::Type::gnuplot_1d);
      file.createAttribute("vtk", IWriter::Type::vtk);

      REQUIRE(file.getAttribute("gnuplot").read<IWriter::Type>() == IWriter::Type::gnuplot);
      REQUIRE(file.getAttribute("gnuplot_1d").read<IWriter::Type>() == IWriter::Type::gnuplot_1d);
      REQUIRE(file.getAttribute("vtk").read<IWriter::Type>() == IWriter::Type::vtk);
    }

    SECTION("IZoneDescriptorHFType")
    {
      file.createAttribute("named", IZoneDescriptor::Type::named);
      file.createAttribute("numbered", IZoneDescriptor::Type::numbered);

      REQUIRE(file.getAttribute("named").read<IZoneDescriptor::Type>() == IZoneDescriptor::Type::named);
      REQUIRE(file.getAttribute("numbered").read<IZoneDescriptor::Type>() == IZoneDescriptor::Type::numbered);
    }

    SECTION("ItemTypeHFType")
    {
      file.createAttribute("node", ItemType::node);
      file.createAttribute("edge", ItemType::edge);
      file.createAttribute("face", ItemType::face);
      file.createAttribute("cell", ItemType::cell);

      REQUIRE(file.getAttribute("node").read<ItemType>() == ItemType::node);
      REQUIRE(file.getAttribute("edge").read<ItemType>() == ItemType::edge);
      REQUIRE(file.getAttribute("face").read<ItemType>() == ItemType::face);
      REQUIRE(file.getAttribute("cell").read<ItemType>() == ItemType::cell);
    }

    SECTION("LinearSolverOptionsHFType")
    {
      file.createAttribute("builtin", LSLibrary::builtin);
      file.createAttribute("eigen3", LSLibrary::eigen3);
      file.createAttribute("petsc", LSLibrary::petsc);

      file.createAttribute("cg", LSMethod::cg);
      file.createAttribute("bicgstab", LSMethod::bicgstab);
      file.createAttribute("bicgstab2", LSMethod::bicgstab2);
      file.createAttribute("gmres", LSMethod::gmres);
      file.createAttribute("lu", LSMethod::lu);
      file.createAttribute("cholesky", LSMethod::cholesky);

      file.createAttribute("none", LSPrecond::none);
      file.createAttribute("diagonal", LSPrecond::diagonal);
      file.createAttribute("incomplete_cholesky", LSPrecond::incomplete_cholesky);
      file.createAttribute("incomplete_LU", LSPrecond::incomplete_LU);
      file.createAttribute("amg", LSPrecond::amg);

      REQUIRE(file.getAttribute("builtin").read<LSLibrary>() == LSLibrary::builtin);
      REQUIRE(file.getAttribute("petsc").read<LSLibrary>() == LSLibrary::petsc);
      REQUIRE(file.getAttribute("eigen3").read<LSLibrary>() == LSLibrary::eigen3);

      REQUIRE(file.getAttribute("cg").read<LSMethod>() == LSMethod::cg);
      REQUIRE(file.getAttribute("bicgstab").read<LSMethod>() == LSMethod::bicgstab);
      REQUIRE(file.getAttribute("bicgstab2").read<LSMethod>() == LSMethod::bicgstab2);
      REQUIRE(file.getAttribute("gmres").read<LSMethod>() == LSMethod::gmres);
      REQUIRE(file.getAttribute("lu").read<LSMethod>() == LSMethod::lu);
      REQUIRE(file.getAttribute("cholesky").read<LSMethod>() == LSMethod::cholesky);

      REQUIRE(file.getAttribute("none").read<LSPrecond>() == LSPrecond::none);
      REQUIRE(file.getAttribute("diagonal").read<LSPrecond>() == LSPrecond::diagonal);
      REQUIRE(file.getAttribute("incomplete_cholesky").read<LSPrecond>() == LSPrecond::incomplete_cholesky);
      REQUIRE(file.getAttribute("incomplete_LU").read<LSPrecond>() == LSPrecond::incomplete_LU);
      REQUIRE(file.getAttribute("amg").read<LSPrecond>() == LSPrecond::amg);
    }

    SECTION("OStreamTypeHFType")
    {
      file.createAttribute("std_ostream", OStream::Type::std_ostream);
      file.createAttribute("std_ofstream", OStream::Type::std_ofstream);

      REQUIRE(file.getAttribute("std_ostream").read<OStream::Type>() == OStream::Type::std_ostream);
      REQUIRE(file.getAttribute("std_ofstream").read<OStream::Type>() == OStream::Type::std_ofstream);
    }

    SECTION("ParallelCheckerHFType")
    {
      file.createAttribute("automatic", ParallelChecker::Mode::automatic);
      file.createAttribute("read", ParallelChecker::Mode::read);
      file.createAttribute("write", ParallelChecker::Mode::write);

      REQUIRE(file.getAttribute("automatic").read<ParallelChecker::Mode>() == ParallelChecker::Mode::automatic);
      REQUIRE(file.getAttribute("read").read<ParallelChecker::Mode>() == ParallelChecker::Mode::read);
      REQUIRE(file.getAttribute("write").read<ParallelChecker::Mode>() == ParallelChecker::Mode::write);
    }

    SECTION("QuadratureTypeHFType")
    {
      file.createAttribute("gauss", QuadratureType::Gauss);
      file.createAttribute("gauss-legendre", QuadratureType::GaussLegendre);
      file.createAttribute("gauss-lobatto", QuadratureType::GaussLobatto);

      REQUIRE(file.getAttribute("gauss").read<QuadratureType>() == QuadratureType::Gauss);
      REQUIRE(file.getAttribute("gauss-legendre").read<QuadratureType>() == QuadratureType::GaussLegendre);
      REQUIRE(file.getAttribute("gauss-lobatto").read<QuadratureType>() == QuadratureType::GaussLobatto);
    }

    SECTION("RefItemListHFType")
    {
      file.createAttribute("boundary", RefItemListBase::Type::boundary);
      file.createAttribute("interface", RefItemListBase::Type::interface);
      file.createAttribute("set", RefItemListBase::Type::set);
      file.createAttribute("undefined", RefItemListBase::Type::undefined);

      REQUIRE(file.getAttribute("boundary").read<RefItemListBase::Type>() == RefItemListBase::Type::boundary);
      REQUIRE(file.getAttribute("interface").read<RefItemListBase::Type>() == RefItemListBase::Type::interface);
      REQUIRE(file.getAttribute("set").read<RefItemListBase::Type>() == RefItemListBase::Type::set);
      REQUIRE(file.getAttribute("undefined").read<RefItemListBase::Type>() == RefItemListBase::Type::undefined);
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }
}
