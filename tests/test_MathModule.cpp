#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <language/modules/MathModule.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("MathModule", "[language]")
{
  rang::setControlMode(rang::control::Off);

  MathModule math_module;
  const auto& name_builtin_function = math_module.getNameBuiltinFunctionMap();

  REQUIRE(name_builtin_function.size() == 48);

  SECTION("Z -> N")
  {
    SECTION("abs:Z")
    {
      auto i_function = name_builtin_function.find("abs:Z");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;

      int64_t arg = -3;

      DataVariant arg_variant = arg;

      DataVariant result_variant = function_embedder.apply({arg_variant});

      uint64_t result = std::abs(arg);

      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }
  }

  SECTION("R -> R")
  {
    double arg = 0.7;

    DataVariant arg_variant = arg;

    SECTION("sqrt")
    {
      auto i_function = name_builtin_function.find("sqrt:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::sqrt(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("abs:R")
    {
      auto i_function = name_builtin_function.find("abs:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      {
        DataVariant result_variant = function_embedder.apply({arg_variant});

        auto result = std::abs(arg);

        REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
      }

      {
        arg = double{-3};

        DataVariant double_arg_variant = arg;

        DataVariant result_variant = function_embedder.apply({double_arg_variant});

        auto result = std::abs(arg);

        REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
      }
    }

    SECTION("sin")
    {
      auto i_function = name_builtin_function.find("sin:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::sin(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("cos")
    {
      auto i_function = name_builtin_function.find("cos:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::cos(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("tan")
    {
      auto i_function = name_builtin_function.find("tan:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::tan(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("asin")
    {
      auto i_function = name_builtin_function.find("asin:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::asin(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("acos")
    {
      auto i_function = name_builtin_function.find("acos:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::acos(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("atan")
    {
      auto i_function = name_builtin_function.find("atan:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::atan(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("sinh")
    {
      arg         = 1.3;
      arg_variant = arg;

      auto i_function = name_builtin_function.find("sinh:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::sinh(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("cosh")
    {
      auto i_function = name_builtin_function.find("cosh:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::cosh(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("tanh")
    {
      auto i_function = name_builtin_function.find("tanh:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::tanh(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("asinh")
    {
      auto i_function = name_builtin_function.find("asinh:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::asinh(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("acosh")
    {
      arg         = 10;
      arg_variant = arg;

      auto i_function = name_builtin_function.find("acosh:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::acosh(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("atanh")
    {
      auto i_function = name_builtin_function.find("atanh:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::atanh(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("exp")
    {
      auto i_function = name_builtin_function.find("exp:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::exp(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("log")
    {
      auto i_function = name_builtin_function.find("log:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      auto result = std::log(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }
  }

  SECTION("double -> int64_t")
  {
    double arg = 1.3;

    DataVariant arg_variant = arg;

    SECTION("ceil")
    {
      auto i_function = name_builtin_function.find("ceil:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      int64_t result = std::ceil(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == result);
    }

    SECTION("floor")
    {
      auto i_function = name_builtin_function.find("floor:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      int64_t result = std::floor(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == result);
    }

    SECTION("trunc")
    {
      auto i_function = name_builtin_function.find("trunc:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      int64_t result = std::trunc(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == result);
    }

    SECTION("round")
    {
      auto i_function = name_builtin_function.find("round:R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      int64_t result = std::lround(arg);
      REQUIRE(std::get<decltype(result)>(result_variant) == result);
    }
  }

  SECTION("(double, double) -> double")
  {
    double arg0 = 3;
    double arg1 = 2;

    DataVariant arg0_variant = arg0;
    DataVariant arg1_variant = arg1;

    SECTION("atan2")
    {
      auto i_function = name_builtin_function.find("atan2:R*R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      auto result = std::atan2(arg0, arg1);
      REQUIRE(std::get<decltype(result)>(result_variant) == result);
    }

    SECTION("pow")
    {
      auto i_function = name_builtin_function.find("pow:R*R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      auto result = std::pow(arg0, arg1);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("min")
    {
      auto i_function = name_builtin_function.find("min:R*R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      auto result = std::min(arg0, arg1);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("max")
    {
      auto i_function = name_builtin_function.find("max:R*R");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      auto result = std::max(arg0, arg1);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }
  }

  SECTION("(uint64_t, uint64_t) -> uint64_t")
  {
    int64_t arg0 = 3;
    int64_t arg1 = -2;

    DataVariant arg0_variant = arg0;
    DataVariant arg1_variant = arg1;

    SECTION("min")
    {
      auto i_function = name_builtin_function.find("min:Z*Z");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      auto result = std::min(arg0, arg1);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }

    SECTION("max")
    {
      auto i_function = name_builtin_function.find("max:Z*Z");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      auto result = std::max(arg0, arg1);
      REQUIRE(std::get<decltype(result)>(result_variant) == Catch::Approx(result));
    }
  }

  SECTION("(R^d, R^d) -> double")
  {
    SECTION("dot:R^1*R^1")
    {
      TinyVector<1> arg0{3};
      TinyVector<1> arg1{2};

      DataVariant arg0_variant = arg0;
      DataVariant arg1_variant = arg1;

      auto i_function = name_builtin_function.find("dot:R^1*R^1");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      const double result = dot(arg0, arg1);
      REQUIRE(std::get<double>(result_variant) == result);
    }

    SECTION("dot:R^2*R^2")
    {
      TinyVector<2> arg0{3, 2};
      TinyVector<2> arg1{-2, 5};

      DataVariant arg0_variant = arg0;
      DataVariant arg1_variant = arg1;

      auto i_function = name_builtin_function.find("dot:R^2*R^2");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      const double result = dot(arg0, arg1);
      REQUIRE(std::get<double>(result_variant) == result);
    }

    SECTION("dot:R^3*R^3")
    {
      TinyVector<3> arg0{3, 2, 4};
      TinyVector<3> arg1{-2, 5, 2};

      DataVariant arg0_variant = arg0;
      DataVariant arg1_variant = arg1;

      auto i_function = name_builtin_function.find("dot:R^3*R^3");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      const double result = dot(arg0, arg1);
      REQUIRE(std::get<double>(result_variant) == result);
    }
  }

  SECTION("(R^dxd, R^dxd) -> double")
  {
    SECTION("doubleDot:R^1x1*R^1x1")
    {
      TinyMatrix<1> arg0{3};
      TinyMatrix<1> arg1{2};

      DataVariant arg0_variant = arg0;
      DataVariant arg1_variant = arg1;

      auto i_function = name_builtin_function.find("doubleDot:R^1x1*R^1x1");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      const double result = doubleDot(arg0, arg1);
      REQUIRE(std::get<double>(result_variant) == result);
    }

    SECTION("doubleDot:R^2x2*R^2x2")
    {
      TinyMatrix<2> arg0{+3, +2,   //
                         -1, +4};
      TinyMatrix<2> arg1{-2, -5,   //
                         +7, +1.3};

      DataVariant arg0_variant = arg0;
      DataVariant arg1_variant = arg1;

      auto i_function = name_builtin_function.find("doubleDot:R^2x2*R^2x2");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      const double result = doubleDot(arg0, arg1);
      REQUIRE(std::get<double>(result_variant) == result);
    }

    SECTION("doubleDot:R^3x3*R^3x3")
    {
      TinyMatrix<3> arg0{+3, +2, +4,   //
                         -1, +3, -6,   //
                         +2, +5, +1};
      TinyMatrix<3> arg1{-2, +5, +2,   //
                         +1, +7, -2,   //
                         +7, -1, +3};

      DataVariant arg0_variant = arg0;
      DataVariant arg1_variant = arg1;

      auto i_function = name_builtin_function.find("doubleDot:R^3x3*R^3x3");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      const double result = doubleDot(arg0, arg1);
      REQUIRE(std::get<double>(result_variant) == result);
    }
  }

  SECTION("(R^d, R^d) -> R^dxd")
  {
    SECTION("tensorProduct:R^1*R^1")
    {
      TinyVector<1> arg0{3};
      TinyVector<1> arg1{2};

      DataVariant arg0_variant = arg0;
      DataVariant arg1_variant = arg1;

      auto i_function = name_builtin_function.find("tensorProduct:R^1*R^1");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      const TinyMatrix<1> result = tensorProduct(arg0, arg1);
      REQUIRE(std::get<TinyMatrix<1>>(result_variant) == result);
    }

    SECTION("tensorProduct:R^2*R^2")
    {
      TinyVector<2> arg0{3, 2};
      TinyVector<2> arg1{-2, 5};

      DataVariant arg0_variant = arg0;
      DataVariant arg1_variant = arg1;

      auto i_function = name_builtin_function.find("tensorProduct:R^2*R^2");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      const TinyMatrix<2> result = tensorProduct(arg0, arg1);
      REQUIRE(std::get<TinyMatrix<2>>(result_variant) == result);
    }

    SECTION("tensorProduct:R^3*R^3")
    {
      TinyVector<3> arg0{3, 2, 4};
      TinyVector<3> arg1{-2, 5, 2};

      DataVariant arg0_variant = arg0;
      DataVariant arg1_variant = arg1;

      auto i_function = name_builtin_function.find("tensorProduct:R^3*R^3");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg0_variant, arg1_variant});

      const TinyMatrix<3> result = tensorProduct(arg0, arg1);
      REQUIRE(std::get<TinyMatrix<3>>(result_variant) == result);
    }
  }

  SECTION("R^dxd -> double")
  {
    SECTION("det:R^1x1")
    {
      TinyMatrix<1> arg{3};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("det:R^1x1");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const double result = det(arg);
      REQUIRE(std::get<double>(result_variant) == result);
    }

    SECTION("det:R^2x2")
    {
      TinyMatrix<2> arg{3, 2, 1, -2};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("det:R^2x2");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const double result = det(arg);
      REQUIRE(std::get<double>(result_variant) == result);
    }

    SECTION("det:R^3x3")
    {
      TinyMatrix<3> arg{3,  2,  4,   //
                        -1, 4,  1,   //
                        -4, -1, 5};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("det:R^3x3");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const double result = det(arg);
      REQUIRE(std::get<double>(result_variant) == result);
    }

    SECTION("trace:R^1x1")
    {
      TinyMatrix<1> arg{3};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("trace:R^1x1");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const double result = trace(arg);
      REQUIRE(std::get<double>(result_variant) == result);
    }

    SECTION("trace:R^2x2")
    {
      TinyMatrix<2> arg{3, 2, 1, -2};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("trace:R^2x2");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const double result = trace(arg);
      REQUIRE(std::get<double>(result_variant) == result);
    }

    SECTION("trace:R^3x3")
    {
      TinyMatrix<3> arg{3,  2,  4,   //
                        -1, 4,  1,   //
                        -4, -1, 5};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("trace:R^3x3");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const double result = trace(arg);
      REQUIRE(std::get<double>(result_variant) == result);
    }
  }

  SECTION("R^dxd -> R^dxd")
  {
    SECTION("inverse:R^1x1")
    {
      TinyMatrix<1> arg{3};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("inverse:R^1x1");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const TinyMatrix<1> result = inverse(arg);
      REQUIRE(std::get<TinyMatrix<1>>(result_variant) == result);
    }

    SECTION("inverse:R^2x2")
    {
      TinyMatrix<2> arg{3, 2, 1, -2};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("inverse:R^2x2");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const TinyMatrix<2> result = inverse(arg);
      REQUIRE(std::get<TinyMatrix<2>>(result_variant) == result);
    }

    SECTION("inverse:R^3x3")
    {
      TinyMatrix<3> arg{3,  2,  4,   //
                        -1, 4,  1,   //
                        -4, -1, 5};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("inverse:R^3x3");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const TinyMatrix<3> result = inverse(arg);
      REQUIRE(std::get<TinyMatrix<3>>(result_variant) == result);
    }

    SECTION("transpose:R^1x1")
    {
      TinyMatrix<1> arg{3};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("transpose:R^1x1");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const TinyMatrix<1> result = transpose(arg);
      REQUIRE(std::get<TinyMatrix<1>>(result_variant) == result);
    }

    SECTION("transpose:R^2x2")
    {
      TinyMatrix<2> arg{3, 2, 1, -2};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("transpose:R^2x2");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const TinyMatrix<2> result = transpose(arg);
      REQUIRE(std::get<TinyMatrix<2>>(result_variant) == result);
    }

    SECTION("transpose:R^3x3")
    {
      TinyMatrix<3> arg{3,  2,  4,   //
                        -1, 4,  1,   //
                        -4, -1, 5};

      DataVariant arg_variant = arg;

      auto i_function = name_builtin_function.find("transpose:R^3x3");
      REQUIRE(i_function != name_builtin_function.end());

      IBuiltinFunctionEmbedder& function_embedder = *i_function->second;
      DataVariant result_variant                  = function_embedder.apply({arg_variant});

      const TinyMatrix<3> result = transpose(arg);
      REQUIRE(std::get<TinyMatrix<3>>(result_variant) == result);
    }
  }
}
