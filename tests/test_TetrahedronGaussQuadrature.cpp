#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <analysis/TetrahedronGaussQuadrature.hpp>
#include <geometry/TetrahedronTransformation.hpp>
#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("TetrahedronGaussQuadrature", "[analysis]")
{
  auto integrate = [](auto f, auto quadrature_formula) {
    using R3 = TinyVector<3>;

    const R3 A{-1, -1, -1};
    const R3 B{+1, -1, -1};
    const R3 C{-1, +1, -1};
    const R3 D{-1, -1, +1};

    TetrahedronTransformation t{A, B, C, D};

    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    auto value = weight_list[0] * f(t(point_list[0]));
    for (size_t i = 1; i < weight_list.size(); ++i) {
      value += weight_list[i] * f(t(point_list[i]));
    }

    return t.jacobianDeterminant() * value;
  };

  auto integrate_on_tetra = [](auto f, auto quadrature_formula, const TinyVector<3>& a, const TinyVector<3>& b,
                               const TinyVector<3>& c, const TinyVector<3>& d) {
    TetrahedronTransformation t{a, b, c, d};

    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    auto value = weight_list[0] * f(t(point_list[0]));
    for (size_t i = 1; i < weight_list.size(); ++i) {
      value += weight_list[i] * f(t(point_list[i]));
    }

    return t.jacobianDeterminant() * value;
  };

  auto get_order = [&integrate, &integrate_on_tetra](auto f, auto quadrature_formula, const double exact_value) {
    using R3 = TinyVector<3>;

    const R3 A{-1, -1, -1};
    const R3 B{+1, -1, -1};
    const R3 C{-1, +1, -1};
    const R3 D{-1, -1, +1};

    const R3 M_AB = 0.5 * (A + B);
    const R3 M_AC = 0.5 * (A + C);
    const R3 M_AD = 0.5 * (A + D);
    const R3 M_BC = 0.5 * (B + C);
    const R3 M_BD = 0.5 * (B + D);
    const R3 M_CD = 0.5 * (C + D);

    const double int_T_hat = integrate(f, quadrature_formula);
    const double int_refined   //
      = integrate_on_tetra(f, quadrature_formula, A, M_AB, M_AC, M_AD) +
        integrate_on_tetra(f, quadrature_formula, B, M_AB, M_BD, M_BC) +
        integrate_on_tetra(f, quadrature_formula, C, M_AC, M_BC, M_CD) +
        integrate_on_tetra(f, quadrature_formula, D, M_AD, M_CD, M_BD) +
        integrate_on_tetra(f, quadrature_formula, M_BD, M_AC, M_AD, M_CD) +
        integrate_on_tetra(f, quadrature_formula, M_AB, M_AC, M_AD, M_BD) +
        integrate_on_tetra(f, quadrature_formula, M_BC, M_AB, M_BD, M_AC) +
        integrate_on_tetra(f, quadrature_formula, M_BC, M_AC, M_BD, M_CD);

    return -std::log(std::abs(int_refined - exact_value) / std::abs(int_T_hat - exact_value)) / std::log(2);
  };

  auto p0 = [](const TinyVector<3>&) { return 4; };
  auto p1 = [](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return 2 * x + 3 * y + z - 1;
  };
  auto p2 = [&p1](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p1(X) * (2.5 * x - 3 * y + z + 3);
  };
  auto p3 = [&p2](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p2(X) * (3 * x + 2 * y - 3 * z - 1);
  };
  auto p4 = [&p3](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p3(X) * (2 * x - 0.5 * y - 1.3 * z + 1);
  };
  auto p5 = [&p4](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p4(X) * (-0.1 * x + 1.3 * y - 3 * z + 1);
  };
  auto p6 = [&p5](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p5(X) * 7875. / 143443 * (2 * x - y + 4 * z + 1);
  };
  auto p7 = [&p6](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p6(X) * (0.7 * x - 2.7 * y + 1.3 * z - 2);
  };
  auto p8 = [&p7](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p7(X) * (0.3 * x + 1.2 * y - 0.7 * z + 0.2);
  };
  auto p9 = [&p8](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p8(X) * (-0.2 * x + 1.1 * y - 0.5 * z + 0.6);
  };
  auto p10 = [&p9](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p9(X) * (0.7 * x - 0.6 * y - 0.7 * z - 0.2);
  };
  auto p11 = [&p10](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p10(X) * (-1.3 * x + 0.6 * y - 1.3 * z + 0.7);
  };
  auto p12 = [&p11](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p11(X) * (0.3 * x - 0.7 * y + 0.3 * z + 0.7);
  };
  auto p13 = [&p12](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p12(X) * (0.9 * x + 0.2 * y - 0.4 * z + 0.5);
  };
  auto p14 = [&p13](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p13(X) * (0.6 * x - 1.2 * y + 0.7 * z - 0.4);
  };
  auto p15 = [&p14](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p14(X) * (-0.2 * x - 0.7 * y + 0.9 * z + 0.8);
  };
  auto p16 = [&p15](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p15(X) * (0.7 * x + 0.2 * y - 0.6 * z + 0.4);
  };
  auto p17 = [&p16](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p16(X) * (-0.1 * x + 0.8 * y + 0.3 * z - 0.2);
  };
  auto p18 = [&p17](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p17(X) * (0.7 * x - 0.2 * y - 0.3 * z + 0.8);
  };
  auto p19 = [&p18](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p18(X) * (-0.7 * x + 1.2 * y + 1.3 * z + 0.8);
  };
  auto p20 = [&p19](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p19(X) * (0.7 * x - 1.2 * y + 0.3 * z - 0.6);
  };
  auto p21 = [&p20](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p20(X) * (0.7 * x - 1.2 * y + 0.3 * z - 0.6);
  };

  SECTION("degree 1")
  {
    const QuadratureFormula<3>& l1 = QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(1));

    REQUIRE(l1.numberOfPoints() == 1);

    REQUIRE(integrate(p0, l1) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l1) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l1) != Catch::Approx(-47. / 3));

    REQUIRE(get_order(p2, l1, -47. / 3) >= Catch::Approx(2));
  }

  SECTION("degree 2")
  {
    const QuadratureFormula<3>& l2 = QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(2));

    REQUIRE(l2.numberOfPoints() == 4);

    REQUIRE(integrate(p0, l2) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l2) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l2) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l2) != Catch::Approx(557. / 15));

    REQUIRE(get_order(p3, l2, 557. / 15) >= Catch::Approx(3));
  }

  SECTION("degree 3")
  {
    const QuadratureFormula<3>& l3 = QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(3));

    REQUIRE(l3.numberOfPoints() == 8);

    REQUIRE(integrate(p0, l3) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l3) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l3) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l3) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l3) != Catch::Approx(4499. / 1575));
  }

  SECTION("degree 4")
  {
    const QuadratureFormula<3>& l4 = QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(4));

    REQUIRE(l4.numberOfPoints() == 14);

    REQUIRE(integrate(p0, l4) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l4) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l4) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l4) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l4) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l4) != Catch::Approx(143443. / 7875));

    REQUIRE(get_order(p5, l4, 143443. / 7875) >= Catch::Approx(5));
  }

  SECTION("degree 5")
  {
    const QuadratureFormula<3>& l5 = QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(5));

    REQUIRE(l5.numberOfPoints() == 14);

    REQUIRE(integrate(p0, l5) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l5) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l5) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l5) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l5) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l5) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l5) != Catch::Approx(-75773. / 2581974));
  }

  SECTION("degree 6")
  {
    const QuadratureFormula<3>& l6 = QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(6));

    REQUIRE(l6.numberOfPoints() == 24);

    REQUIRE(integrate(p0, l6) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l6) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l6) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l6) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l6) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l6) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l6) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l6) != Catch::Approx(86951548. / 32274675));

    REQUIRE(get_order(p7, l6, 86951548. / 32274675) >= Catch::Approx(7));
  }

  SECTION("degree 7")
  {
    const QuadratureFormula<3>& l7 = QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(7));

    REQUIRE(l7.numberOfPoints() == 35);

    REQUIRE(integrate(p0, l7) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l7) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l7) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l7) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l7) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l7) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l7) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l7) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l7) != Catch::Approx(-863556317. / 322746750));

    REQUIRE(get_order(p8, l7, -863556317. / 322746750) == Catch::Approx(8).margin(0.5));
  }

  SECTION("degree 8")
  {
    const QuadratureFormula<3>& l8 = QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(8));

    REQUIRE(l8.numberOfPoints() == 46);

    REQUIRE(integrate(p0, l8) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l8) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l8) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l8) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l8) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l8) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l8) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l8) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l8) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l8) != Catch::Approx(1168568393. / 3227467500));

    // In a weird way, one gets almost 10th order on this test
    REQUIRE(get_order(p9, l8, 1168568393. / 3227467500) == Catch::Approx(10));
  }

  SECTION("degree 9")
  {
    const QuadratureFormula<3>& l9 = QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(9));

    REQUIRE(l9.numberOfPoints() == 59);

    REQUIRE(integrate(p0, l9) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l9) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l9) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l9) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l9) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l9) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l9) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l9) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l9) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l9) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l9) != Catch::Approx(-473611706567. / 461527852500));

    // No order test. Too bad ~4.5 when one expects 10 asymptotically
  }

  SECTION("degree 10")
  {
    const QuadratureFormula<3>& l10 =
      QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(10));

    REQUIRE(l10.numberOfPoints() == 81);

    REQUIRE(integrate(p0, l10) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l10) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l10) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l10) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l10) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l10) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l10) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l10) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l10) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l10) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l10) == Catch::Approx(-473611706567. / 461527852500));
    REQUIRE(integrate(p11, l10) != Catch::Approx(-340332578501887. / 323069496750000));

    // In a weird way, one gets almost 12th order on this test
    REQUIRE(get_order(p11, l10, -340332578501887. / 323069496750000) == Catch::Approx(12));
  }

  SECTION("degree 11")
  {
    const QuadratureFormula<3>& l11 =
      QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(11));

    REQUIRE(l11.numberOfPoints() == 110);

    REQUIRE(integrate(p0, l11) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l11) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l11) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l11) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l11) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l11) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l11) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l11) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l11) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l11) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l11) == Catch::Approx(-473611706567. / 461527852500));
    REQUIRE(integrate(p11, l11) == Catch::Approx(-340332578501887. / 323069496750000));
    REQUIRE(integrate(p12, l11) != Catch::Approx(-9990191769716047. / 16153474837500000));

    // No order test. Too bad ~11 when one expects 12 asymptotically
  }

  SECTION("degree 12")
  {
    const QuadratureFormula<3>& l12 =
      QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(12));

    REQUIRE(l12.numberOfPoints() == 168);

    REQUIRE(integrate(p0, l12) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l12) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l12) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l12) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l12) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l12) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l12) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l12) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l12) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l12) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l12) == Catch::Approx(-473611706567. / 461527852500));
    REQUIRE(integrate(p11, l12) == Catch::Approx(-340332578501887. / 323069496750000));
    REQUIRE(integrate(p12, l12) == Catch::Approx(-9990191769716047. / 16153474837500000));
    REQUIRE(integrate(p13, l12) != Catch::Approx(-31229729533861. / 5384491612500000));

    // In a weird way, one gets almost 14th order on this test
    REQUIRE(get_order(p13, l12, -31229729533861. / 5384491612500000) == Catch::Approx(14).margin(0.01));
  }

  SECTION("degree 13")
  {
    const QuadratureFormula<3>& l13 =
      QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(13));

    REQUIRE(l13.numberOfPoints() == 172);

    REQUIRE(integrate(p0, l13) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l13) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l13) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l13) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l13) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l13) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l13) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l13) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l13) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l13) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l13) == Catch::Approx(-473611706567. / 461527852500));
    REQUIRE(integrate(p11, l13) == Catch::Approx(-340332578501887. / 323069496750000));
    REQUIRE(integrate(p12, l13) == Catch::Approx(-9990191769716047. / 16153474837500000));
    REQUIRE(integrate(p13, l13) == Catch::Approx(-31229729533861. / 5384491612500000));
    REQUIRE(integrate(p14, l13) != Catch::Approx(3758162710897404343. / 13730453611875000000.));

    REQUIRE(get_order(p14, l13, 3758162710897404343. / 13730453611875000000.) == Catch::Approx(14).margin(0.5));
  }

  SECTION("degree 14")
  {
    const QuadratureFormula<3>& l14 =
      QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(14));

    REQUIRE(l14.numberOfPoints() == 204);

    REQUIRE(integrate(p0, l14) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l14) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l14) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l14) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l14) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l14) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l14) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l14) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l14) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l14) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l14) == Catch::Approx(-473611706567. / 461527852500));
    REQUIRE(integrate(p11, l14) == Catch::Approx(-340332578501887. / 323069496750000));
    REQUIRE(integrate(p12, l14) == Catch::Approx(-9990191769716047. / 16153474837500000));
    REQUIRE(integrate(p13, l14) == Catch::Approx(-31229729533861. / 5384491612500000));
    REQUIRE(integrate(p14, l14) == Catch::Approx(3758162710897404343. / 13730453611875000000.));
    REQUIRE(integrate(p15, l14) != Catch::Approx(49733943385709654587. / 137304536118750000000.));

    // In a weird way, one gets almost 16th order on this test
    REQUIRE(get_order(p15, l14, 49733943385709654587. / 137304536118750000000.) == Catch::Approx(16).margin(0.01));
  }

  SECTION("degree 15")
  {
    const QuadratureFormula<3>& l15 =
      QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(15));

    REQUIRE(l15.numberOfPoints() == 264);

    REQUIRE(integrate(p0, l15) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l15) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l15) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l15) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l15) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l15) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l15) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l15) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l15) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l15) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l15) == Catch::Approx(-473611706567. / 461527852500));
    REQUIRE(integrate(p11, l15) == Catch::Approx(-340332578501887. / 323069496750000));
    REQUIRE(integrate(p12, l15) == Catch::Approx(-9990191769716047. / 16153474837500000));
    REQUIRE(integrate(p13, l15) == Catch::Approx(-31229729533861. / 5384491612500000));
    REQUIRE(integrate(p14, l15) == Catch::Approx(3758162710897404343. / 13730453611875000000.));
    REQUIRE(integrate(p15, l15) == Catch::Approx(49733943385709654587. / 137304536118750000000.));
    REQUIRE(integrate(p16, l15) != Catch::Approx(-7270030713465096260897. / 26087861862562500000000.));

    REQUIRE(get_order(p16, l15, -7270030713465096260897. / 26087861862562500000000.) ==
            Catch::Approx(16.5).margin(0.2));
  }

  SECTION("degree 16")
  {
    const QuadratureFormula<3>& l16 =
      QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(16));

    REQUIRE(l16.numberOfPoints() == 304);

    REQUIRE(integrate(p0, l16) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l16) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l16) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l16) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l16) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l16) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l16) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l16) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l16) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l16) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l16) == Catch::Approx(-473611706567. / 461527852500));
    REQUIRE(integrate(p11, l16) == Catch::Approx(-340332578501887. / 323069496750000));
    REQUIRE(integrate(p12, l16) == Catch::Approx(-9990191769716047. / 16153474837500000));
    REQUIRE(integrate(p13, l16) == Catch::Approx(-31229729533861. / 5384491612500000));
    REQUIRE(integrate(p14, l16) == Catch::Approx(3758162710897404343. / 13730453611875000000.));
    REQUIRE(integrate(p15, l16) == Catch::Approx(49733943385709654587. / 137304536118750000000.));
    REQUIRE(integrate(p16, l16) == Catch::Approx(-7270030713465096260897. / 26087861862562500000000.));
    REQUIRE(integrate(p17, l16) != Catch::Approx(34859214238288098805889. / 195658963969218750000000.));

    // In a weird way, one gets almost 18th order on this test
    REQUIRE(get_order(p17, l16, 34859214238288098805889. / 195658963969218750000000.) == Catch::Approx(18).margin(0.1));
  }

  SECTION("degree 17")
  {
    const QuadratureFormula<3>& l17 =
      QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(17));

    REQUIRE(l17.numberOfPoints() == 364);

    REQUIRE(integrate(p0, l17) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l17) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l17) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l17) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l17) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l17) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l17) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l17) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l17) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l17) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l17) == Catch::Approx(-473611706567. / 461527852500));
    REQUIRE(integrate(p11, l17) == Catch::Approx(-340332578501887. / 323069496750000));
    REQUIRE(integrate(p12, l17) == Catch::Approx(-9990191769716047. / 16153474837500000));
    REQUIRE(integrate(p13, l17) == Catch::Approx(-31229729533861. / 5384491612500000));
    REQUIRE(integrate(p14, l17) == Catch::Approx(3758162710897404343. / 13730453611875000000.));
    REQUIRE(integrate(p15, l17) == Catch::Approx(49733943385709654587. / 137304536118750000000.));
    REQUIRE(integrate(p16, l17) == Catch::Approx(-7270030713465096260897. / 26087861862562500000000.));
    REQUIRE(integrate(p17, l17) == Catch::Approx(34859214238288098805889. / 195658963969218750000000.));
    REQUIRE(integrate(p18, l17) != Catch::Approx(115510477527288033058991. / 13696127477845312500000000.));

    REQUIRE(get_order(p18, l17, 115510477527288033058991. / 13696127477845312500000000.) ==
            Catch::Approx(18).margin(0.2));
  }

  SECTION("degree 18")
  {
    const QuadratureFormula<3>& l18 =
      QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(18));

    REQUIRE(l18.numberOfPoints() == 436);

    REQUIRE(integrate(p0, l18) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l18) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l18) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l18) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l18) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l18) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l18) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l18) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l18) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l18) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l18) == Catch::Approx(-473611706567. / 461527852500));
    REQUIRE(integrate(p11, l18) == Catch::Approx(-340332578501887. / 323069496750000));
    REQUIRE(integrate(p12, l18) == Catch::Approx(-9990191769716047. / 16153474837500000));
    REQUIRE(integrate(p13, l18) == Catch::Approx(-31229729533861. / 5384491612500000));
    REQUIRE(integrate(p14, l18) == Catch::Approx(3758162710897404343. / 13730453611875000000.));
    REQUIRE(integrate(p15, l18) == Catch::Approx(49733943385709654587. / 137304536118750000000.));
    REQUIRE(integrate(p16, l18) == Catch::Approx(-7270030713465096260897. / 26087861862562500000000.));
    REQUIRE(integrate(p17, l18) == Catch::Approx(34859214238288098805889. / 195658963969218750000000.));
    REQUIRE(integrate(p18, l18) == Catch::Approx(115510477527288033058991. / 13696127477845312500000000.));
    REQUIRE(integrate(p19, l18) !=
            Catch::Approx(2328326230028547427138903. / 57945154713960937500000000.).epsilon(1E-10));

    // In a weird way, one gets almost 20th order on this test
    REQUIRE(get_order(p19, l18, 2328326230028547427138903. / 57945154713960937500000000.) ==
            Catch::Approx(20).margin(0.01));
  }

  SECTION("degree 19")
  {
    const QuadratureFormula<3>& l19 =
      QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(19));

    REQUIRE(l19.numberOfPoints() == 487);

    REQUIRE(integrate(p0, l19) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l19) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l19) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l19) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l19) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l19) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l19) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l19) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l19) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l19) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l19) == Catch::Approx(-473611706567. / 461527852500));
    REQUIRE(integrate(p11, l19) == Catch::Approx(-340332578501887. / 323069496750000));
    REQUIRE(integrate(p12, l19) == Catch::Approx(-9990191769716047. / 16153474837500000));
    REQUIRE(integrate(p13, l19) == Catch::Approx(-31229729533861. / 5384491612500000));
    REQUIRE(integrate(p14, l19) == Catch::Approx(3758162710897404343. / 13730453611875000000.));
    REQUIRE(integrate(p15, l19) == Catch::Approx(49733943385709654587. / 137304536118750000000.));
    REQUIRE(integrate(p16, l19) == Catch::Approx(-7270030713465096260897. / 26087861862562500000000.));
    REQUIRE(integrate(p17, l19) == Catch::Approx(34859214238288098805889. / 195658963969218750000000.));
    REQUIRE(integrate(p18, l19) == Catch::Approx(115510477527288033058991. / 13696127477845312500000000.));
    REQUIRE(integrate(p19, l19) == Catch::Approx(2328326230028547427138903. / 57945154713960937500000000.));
    REQUIRE(integrate(p20, l19) !=
            Catch::Approx(-20113634155270986416106151. / 19250668066082578125000000000.).epsilon(1E-10));

    // No order test. Too bad ~16 when one expects 20 asymptotically
  }

  SECTION("degree 20")
  {
    const QuadratureFormula<3>& l20 =
      QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(20));

    REQUIRE(l20.numberOfPoints() == 552);

    REQUIRE(integrate(p0, l20) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l20) == Catch::Approx(-16. / 3));
    REQUIRE(integrate(p2, l20) == Catch::Approx(-47. / 3));
    REQUIRE(integrate(p3, l20) == Catch::Approx(557. / 15));
    REQUIRE(integrate(p4, l20) == Catch::Approx(4499. / 1575));
    REQUIRE(integrate(p5, l20) == Catch::Approx(143443. / 7875));
    REQUIRE(integrate(p6, l20) == Catch::Approx(-75773. / 2581974));
    REQUIRE(integrate(p7, l20) == Catch::Approx(86951548. / 32274675));
    REQUIRE(integrate(p8, l20) == Catch::Approx(-863556317. / 322746750));
    REQUIRE(integrate(p9, l20) == Catch::Approx(1168568393. / 3227467500));
    REQUIRE(integrate(p10, l20) == Catch::Approx(-473611706567. / 461527852500));
    REQUIRE(integrate(p11, l20) == Catch::Approx(-340332578501887. / 323069496750000));
    REQUIRE(integrate(p12, l20) == Catch::Approx(-9990191769716047. / 16153474837500000));
    REQUIRE(integrate(p13, l20) == Catch::Approx(-31229729533861. / 5384491612500000));
    REQUIRE(integrate(p14, l20) == Catch::Approx(3758162710897404343. / 13730453611875000000.));
    REQUIRE(integrate(p15, l20) == Catch::Approx(49733943385709654587. / 137304536118750000000.));
    REQUIRE(integrate(p16, l20) == Catch::Approx(-7270030713465096260897. / 26087861862562500000000.));
    REQUIRE(integrate(p17, l20) == Catch::Approx(34859214238288098805889. / 195658963969218750000000.));
    REQUIRE(integrate(p18, l20) == Catch::Approx(115510477527288033058991. / 13696127477845312500000000.));
    REQUIRE(integrate(p19, l20) == Catch::Approx(2328326230028547427138903. / 57945154713960937500000000.));
    REQUIRE(integrate(p20, l20) == Catch::Approx(-20113634155270986416106151. / 19250668066082578125000000000.));
    REQUIRE(integrate(p21, l20) != Catch::Approx(16760093568330570728566871. / 7598947920822070312500000000.));

    // In a weird way, one gets almost 22th order on this test
    REQUIRE(get_order(p21, l20, 16760093568330570728566871. / 7598947920822070312500000000.) ==
            Catch::Approx(22).margin(0.01));
  }

  SECTION("max implemented degree")
  {
    REQUIRE(QuadratureManager::instance().maxTetrahedronDegree(QuadratureType::Gauss) ==
            TetrahedronGaussQuadrature::max_degree);
    REQUIRE_THROWS_WITH(QuadratureManager::instance().getTetrahedronFormula(
                          GaussQuadratureDescriptor(TetrahedronGaussQuadrature ::max_degree + 1)),
                        "error: Gauss quadrature formulae handle degrees up to " +
                          stringify(TetrahedronGaussQuadrature ::max_degree) + " on tetrahedra");
  }
}
