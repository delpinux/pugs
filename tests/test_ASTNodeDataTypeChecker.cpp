#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDataTypeChecker.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ParseError.hpp>

#include <pegtl/string_input.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeDataTypeChecker", "[language]")
{
  SECTION("everything ok: nothrow")
  {
    std::string_view data = R"(
for(let i:Z, i=0; i<10; ++i) {
 cout << "i=" << i;
 cout<< "\n";
}
)";

    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
    auto ast = ASTBuilder::build(input);

    ASTModulesImporter{*ast};
    ASTNodeTypeCleaner<language::import_instruction>{*ast};

    ASTSymbolTableBuilder{*ast};
    ASTNodeDataTypeBuilder{*ast};

    REQUIRE_NOTHROW(ASTNodeDataTypeChecker{*ast});
    ast->m_symbol_table->clearValues();
  }

  SECTION("everything uninitialized node type")
  {
    std::string_view data = R"(
for(let i:Z, i=0; i<10; ++i) {
 cout << "i=" << i;
 cout<< "\n";
}
)";

    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
    auto ast = ASTBuilder::build(input);

    ASTModulesImporter{*ast};
    ASTNodeTypeCleaner<language::import_instruction>{*ast};

    ASTSymbolTableBuilder{*ast};
    ASTNodeDataTypeBuilder{*ast};

    ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::undefined_t>();

    REQUIRE_THROWS_AS(ASTNodeDataTypeChecker{*ast}, ParseError);
    ast->m_symbol_table->clearValues();
  }
}
