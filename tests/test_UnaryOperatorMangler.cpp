#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/UnaryOperatorMangler.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("UnaryOperatorMangler", "[language]")
{
  SECTION("unary operators")
  {
    const ASTNodeDataType Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

    REQUIRE(unaryOperatorMangler<language::unary_minus>(Z) == "- Z");
    REQUIRE(unaryOperatorMangler<language::unary_not>(Z) == "not Z");
  }
}
