#include <test_EmbeddedDiscreteFunctionOperators.hpp>

#ifdef __clang__
#pragma clang optimize off
#endif   // __clang__

TEST_CASE("EmbeddedDiscreteFunctionOperators1D", "[scheme]")
{
  constexpr size_t Dimension = 1;

  using Rd = TinyVector<Dimension>;

  std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

  using DiscreteFunctionR    = DiscreteFunctionP0<const double>;
  using DiscreteFunctionR1   = DiscreteFunctionP0<const TinyVector<1>>;
  using DiscreteFunctionR2   = DiscreteFunctionP0<const TinyVector<2>>;
  using DiscreteFunctionR3   = DiscreteFunctionP0<const TinyVector<3>>;
  using DiscreteFunctionR1x1 = DiscreteFunctionP0<const TinyMatrix<1>>;
  using DiscreteFunctionR2x2 = DiscreteFunctionP0<const TinyMatrix<2>>;
  using DiscreteFunctionR3x3 = DiscreteFunctionP0<const TinyMatrix<3>>;

  using DiscreteFunctionVector = DiscreteFunctionP0Vector<const double>;

  for (const auto& named_mesh : mesh_list) {
    SECTION(named_mesh.name())
    {
      auto mesh = named_mesh.mesh()->get<Mesh<Dimension>>();

      std::shared_ptr other_mesh = std::make_shared<const Mesh<Dimension>>(mesh->shared_connectivity(), mesh->xr());

      CellValue<const Rd> xj = MeshDataManager::instance().getMeshData(*mesh).xj();

      CellValue<double> u_R_values = [=] {
        CellValue<double> build_values{mesh->connectivity()};
        parallel_for(
          build_values.numberOfItems(),
          PUGS_LAMBDA(const CellId cell_id) { build_values[cell_id] = 0.2 + std::cos(l2Norm(xj[cell_id])); });
        return build_values;
      }();

      CellValue<double> v_R_values = [=] {
        CellValue<double> build_values{mesh->connectivity()};
        parallel_for(
          build_values.numberOfItems(),
          PUGS_LAMBDA(const CellId cell_id) { build_values[cell_id] = 0.6 + std::sin(l2Norm(xj[cell_id])); });
        return build_values;
      }();

      std::shared_ptr p_R_u = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR(mesh, u_R_values));
      std::shared_ptr p_other_mesh_R_u =
        std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR(other_mesh, u_R_values));
      std::shared_ptr p_R_v = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR(mesh, v_R_values));

      std::shared_ptr p_R1_u = [=] {
        CellValue<TinyVector<1>> uj{mesh->connectivity()};
        parallel_for(
          uj.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) { uj[cell_id][0] = 2 * xj[cell_id][0] + 1; });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR1(mesh, uj));
      }();

      std::shared_ptr p_R1_v = [=] {
        CellValue<TinyVector<1>> vj{mesh->connectivity()};
        parallel_for(
          vj.numberOfItems(),
          PUGS_LAMBDA(const CellId cell_id) { vj[cell_id][0] = xj[cell_id][0] * xj[cell_id][0] + 1; });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR1(mesh, vj));
      }();

      std::shared_ptr p_other_mesh_R1_u = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionR1(other_mesh, p_R1_u->get<DiscreteFunctionR1>().cellValues()));

      constexpr auto to_2d = [&](const TinyVector<Dimension>& x) -> TinyVector<2> {
        if constexpr (Dimension == 1) {
          return TinyVector<2>{x[0], 1 + x[0] * x[0]};
        } else if constexpr (Dimension == 2) {
          return TinyVector<2>{x[0], x[1]};
        } else if constexpr (Dimension == 3) {
          return TinyVector<2>{x[0], x[1] + x[2]};
        }
      };

      std::shared_ptr p_R2_u = [=] {
        CellValue<TinyVector<2>> uj{mesh->connectivity()};
        parallel_for(
          uj.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
            const TinyVector<2> x = to_2d(xj[cell_id]);
            uj[cell_id]           = TinyVector<2>{2 * x[0] + 1, 1 - x[1]};
          });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR2(mesh, uj));
      }();

      std::shared_ptr p_R2_v = [=] {
        CellValue<TinyVector<2>> vj{mesh->connectivity()};
        parallel_for(
          vj.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
            const TinyVector<2> x = to_2d(xj[cell_id]);
            vj[cell_id]           = TinyVector<2>{x[0] * x[1] + 1, 2 * x[1]};
          });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR2(mesh, vj));
      }();

      std::shared_ptr p_other_mesh_R2_u = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionR2(other_mesh, p_R2_u->get<DiscreteFunctionR2>().cellValues()));

      constexpr auto to_3d = [&](const TinyVector<Dimension>& x) -> TinyVector<3> {
        if constexpr (Dimension == 1) {
          return TinyVector<3>{x[0], 1 + x[0] * x[0], 2 - x[0]};
        } else if constexpr (Dimension == 2) {
          return TinyVector<3>{x[0], x[1], x[0] + x[1]};
        } else if constexpr (Dimension == 3) {
          return TinyVector<3>{x[0], x[1], x[2]};
        }
      };

      std::shared_ptr p_R3_u = [=] {
        CellValue<TinyVector<3>> uj{mesh->connectivity()};
        parallel_for(
          uj.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
            const TinyVector<3> x = to_3d(xj[cell_id]);
            uj[cell_id]           = TinyVector<3>{2 * x[0] + 1, 1 - x[1] * x[2], x[0] + x[2]};
          });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR3(mesh, uj));
      }();

      std::shared_ptr p_R3_v = [=] {
        CellValue<TinyVector<3>> vj{mesh->connectivity()};
        parallel_for(
          vj.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
            const TinyVector<3> x = to_3d(xj[cell_id]);
            vj[cell_id]           = TinyVector<3>{x[0] * x[1] + 1, 2 * x[1], x[2] * x[0]};
          });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR3(mesh, vj));
      }();

      std::shared_ptr p_other_mesh_R3_u = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionR3(other_mesh, p_R3_u->get<DiscreteFunctionR3>().cellValues()));

      std::shared_ptr p_R1x1_u = [=] {
        CellValue<TinyMatrix<1>> uj{mesh->connectivity()};
        parallel_for(
          uj.numberOfItems(),
          PUGS_LAMBDA(const CellId cell_id) { uj[cell_id] = TinyMatrix<1>{2 * xj[cell_id][0] + 1}; });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR1x1(mesh, uj));
      }();

      std::shared_ptr p_other_mesh_R1x1_u = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionR1x1(other_mesh, p_R1x1_u->get<DiscreteFunctionR1x1>().cellValues()));

      std::shared_ptr p_R1x1_v = [=] {
        CellValue<TinyMatrix<1>> vj{mesh->connectivity()};
        parallel_for(
          vj.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) { vj[cell_id] = TinyMatrix<1>{0.3 - xj[cell_id][0]}; });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR1x1(mesh, vj));
      }();

      std::shared_ptr p_R2x2_u = [=] {
        CellValue<TinyMatrix<2>> uj{mesh->connectivity()};
        parallel_for(
          uj.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
            const TinyVector<2> x = to_2d(xj[cell_id]);

            uj[cell_id] = TinyMatrix<2>{2 * x[0] + 1, 1 - x[1],   //
                                        2 * x[1], -x[0]};
          });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR2x2(mesh, uj));
      }();

      std::shared_ptr p_other_mesh_R2x2_u = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionR2x2(other_mesh, p_R2x2_u->get<DiscreteFunctionR2x2>().cellValues()));

      std::shared_ptr p_R2x2_v = [=] {
        CellValue<TinyMatrix<2>> vj{mesh->connectivity()};
        parallel_for(
          vj.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
            const TinyVector<2> x = to_2d(xj[cell_id]);

            vj[cell_id] = TinyMatrix<2>{x[0] + 0.3, 1 - x[1] - x[0],   //
                                        2 * x[1] + x[0], x[1] - x[0]};
          });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR2x2(mesh, vj));
      }();

      std::shared_ptr p_R3x3_u = [=] {
        CellValue<TinyMatrix<3>> uj{mesh->connectivity()};
        parallel_for(
          uj.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
            const TinyVector<3> x = to_3d(xj[cell_id]);

            uj[cell_id] = TinyMatrix<3>{2 * x[0] + 1,    1 - x[1],        3,             //
                                        2 * x[1],        -x[0],           x[0] - x[1],   //
                                        3 * x[2] - x[1], x[1] - 2 * x[2], x[2] - x[0]};
          });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR3x3(mesh, uj));
      }();

      std::shared_ptr p_other_mesh_R3x3_u = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionR3x3(other_mesh, p_R3x3_u->get<DiscreteFunctionR3x3>().cellValues()));

      std::shared_ptr p_R3x3_v = [=] {
        CellValue<TinyMatrix<3>> vj{mesh->connectivity()};
        parallel_for(
          vj.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
            const TinyVector<3> x = to_3d(xj[cell_id]);

            vj[cell_id] = TinyMatrix<3>{0.2 * x[0] + 1,  2 + x[1],          3 - x[2],      //
                                        2.3 * x[2],      x[1] - x[0],       x[2] - x[1],   //
                                        2 * x[2] + x[0], x[1] + 0.2 * x[2], x[2] - 2 * x[0]};
          });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionR3x3(mesh, vj));
      }();

      std::shared_ptr p_Vector3_u = [=] {
        CellArray<double> uj_vector{mesh->connectivity(), 3};
        parallel_for(
          uj_vector.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
            const TinyVector<3> x = to_3d(xj[cell_id]);
            uj_vector[cell_id][0] = 2 * x[0] + 1;
            uj_vector[cell_id][1] = 1 - x[1] * x[2];
            uj_vector[cell_id][2] = x[0] + x[2];
          });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionVector(mesh, uj_vector));
      }();

      std::shared_ptr p_other_mesh_Vector3_u = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionVector(other_mesh, p_Vector3_u->get<DiscreteFunctionVector>().cellArrays()));

      std::shared_ptr p_Vector3_v = [=] {
        CellArray<double> vj_vector{mesh->connectivity(), 3};
        parallel_for(
          vj_vector.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
            const TinyVector<3> x = to_3d(xj[cell_id]);
            vj_vector[cell_id][0] = x[0] * x[1] + 1;
            vj_vector[cell_id][1] = 2 * x[1];
            vj_vector[cell_id][2] = x[2] * x[0];
          });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionVector(mesh, vj_vector));
      }();

      std::shared_ptr p_Vector2_w = [=] {
        CellArray<double> wj_vector{mesh->connectivity(), 2};
        parallel_for(
          wj_vector.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
            const TinyVector<3> x = to_3d(xj[cell_id]);
            wj_vector[cell_id][0] = x[0] + x[1] * 2;
            wj_vector[cell_id][1] = x[0] * x[1];
          });

        return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionVector(mesh, wj_vector));
      }();

      SECTION("binary operators")
      {
        SECTION("sum")
        {
          SECTION("Vh + Vh -> Vh")
          {
            CHECK_EMBEDDED_VH2_TO_VH(p_R_u, +, p_R_v,   //
                                     DiscreteFunctionR, DiscreteFunctionR, DiscreteFunctionR);

            CHECK_EMBEDDED_VH2_TO_VH(p_R1_u, +, p_R1_v,   //
                                     DiscreteFunctionR1, DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_VH2_TO_VH(p_R2_u, +, p_R2_v,   //
                                     DiscreteFunctionR2, DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_VH2_TO_VH(p_R3_u, +, p_R3_v,   //
                                     DiscreteFunctionR3, DiscreteFunctionR3, DiscreteFunctionR3);

            CHECK_EMBEDDED_VH2_TO_VH(p_R1x1_u, +, p_R1x1_v,   //
                                     DiscreteFunctionR1x1, DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_VH2_TO_VH(p_R2x2_u, +, p_R2x2_v,   //
                                     DiscreteFunctionR2x2, DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_VH2_TO_VH(p_R3x3_u, +, p_R3x3_v,   //
                                     DiscreteFunctionR3x3, DiscreteFunctionR3x3, DiscreteFunctionR3x3);

            CHECK_EMBEDDED_VECTOR_VH2_TO_VH(p_Vector3_u, +, p_Vector3_v, DiscreteFunctionVector);

            REQUIRE_THROWS_WITH(p_R_u + p_R1_v, "error: incompatible operand types Vh(P0:R) and Vh(P0:R^1)");
            REQUIRE_THROWS_WITH(p_R2_u + p_R1_v, "error: incompatible operand types Vh(P0:R^2) and Vh(P0:R^1)");
            REQUIRE_THROWS_WITH(p_R3_u + p_R1x1_v, "error: incompatible operand types Vh(P0:R^3) and Vh(P0:R^1x1)");
            REQUIRE_THROWS_WITH(p_R_u + p_R2x2_v, "error: incompatible operand types Vh(P0:R) and Vh(P0:R^2x2)");
            REQUIRE_THROWS_WITH(p_R_u + p_R2x2_v, "error: incompatible operand types Vh(P0:R) and Vh(P0:R^2x2)");
            REQUIRE_THROWS_WITH(p_Vector3_u + p_R_v, "error: incompatible operand types Vh(P0Vector:R) and Vh(P0:R)");
            REQUIRE_THROWS_WITH(p_Vector3_u + p_Vector2_w, "error: Vh(P0Vector:R) spaces have different sizes");

            REQUIRE_THROWS_WITH(p_R_u + p_other_mesh_R_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R1_u + p_other_mesh_R1_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R2_u + p_other_mesh_R2_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R3_u + p_other_mesh_R3_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R1x1_u + p_other_mesh_R1x1_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R2x2_u + p_other_mesh_R2x2_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R3x3_u + p_other_mesh_R3x3_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_Vector3_u + p_other_mesh_Vector3_u,
                                "error: operands are defined on different meshes");
          }

          SECTION("Vh + X -> Vh")
          {
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, +, bool{true},   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, +, uint64_t{1},   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, +, int64_t{2},   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, +, double{1.3},   //
                                      DiscreteFunctionR, DiscreteFunctionR);

            CHECK_EMBEDDED_VHxX_TO_VH(p_R1_u, +, (TinyVector<1>{1.3}),   //
                                      DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R2_u, +, (TinyVector<2>{1.2, 2.3}),   //
                                      DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R3_u, +, (TinyVector<3>{3.2, 7.1, 5.2}),   //
                                      DiscreteFunctionR3, DiscreteFunctionR3);

            CHECK_EMBEDDED_VHxX_TO_VH(p_R1x1_u, +, (TinyMatrix<1>{1.3}),   //
                                      DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R2x2_u, +, (TinyMatrix<2>{1.2, 2.3, 4.2, 5.1}),   //
                                      DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R3x3_u, +,
                                      (TinyMatrix<3>{3.2, 7.1, 5.2,     //
                                                     4.7, 2.3, 7.1,     //
                                                     9.7, 3.2, 6.8}),   //
                                      DiscreteFunctionR3x3, DiscreteFunctionR3x3);

            REQUIRE_THROWS_WITH(p_R_u + (TinyVector<1>{1}), "error: incompatible operand types Vh(P0:R) and R^1");
            REQUIRE_THROWS_WITH(p_R_u + (TinyVector<2>{1, 2}), "error: incompatible operand types Vh(P0:R) and R^2");
            REQUIRE_THROWS_WITH(p_R_u + (TinyVector<3>{2, 3, 2}), "error: incompatible operand types Vh(P0:R) and R^3");
            REQUIRE_THROWS_WITH(p_R_u + (TinyMatrix<1>{2}), "error: incompatible operand types Vh(P0:R) and R^1x1");
            REQUIRE_THROWS_WITH(p_R_u + (TinyMatrix<2>{2, 3, 1, 4}),
                                "error: incompatible operand types Vh(P0:R) and R^2x2");
            REQUIRE_THROWS_WITH(p_R_u + (TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}),
                                "error: incompatible operand types Vh(P0:R) and R^3x3");

            REQUIRE_THROWS_WITH(p_Vector3_u + (double{1}), "error: incompatible operand types Vh(P0Vector:R) and R");
            REQUIRE_THROWS_WITH(p_Vector3_u + (TinyVector<1>{1}),
                                "error: incompatible operand types Vh(P0Vector:R) and R^1");
            REQUIRE_THROWS_WITH(p_Vector3_u + (TinyVector<2>{1, 2}),
                                "error: incompatible operand types Vh(P0Vector:R) and R^2");
          }

          SECTION("X + Vh -> Vh")
          {
            CHECK_EMBEDDED_XxVH_TO_VH(bool{true}, +, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(uint64_t{1}, +, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(int64_t{2}, +, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(double{1.3}, +, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);

            CHECK_EMBEDDED_XxVH_TO_VH((TinyVector<1>{1.3}), +, p_R1_u,   //
                                      DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyVector<2>{1.2, 2.3}), +, p_R2_u,   //
                                      DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyVector<3>{3.2, 7.1, 5.2}), +, p_R3_u,   //
                                      DiscreteFunctionR3, DiscreteFunctionR3);

            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<1>{1.3}), +, p_R1x1_u,   //
                                      DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<2>{1.2, 2.3, 4.2, 5.1}), +, p_R2x2_u,   //
                                      DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<3>{3.2, 7.1, 5.2,   //
                                                     4.7, 2.3, 7.1,   //
                                                     9.7, 3.2, 6.8}),
                                      +, p_R3x3_u,   //
                                      DiscreteFunctionR3x3, DiscreteFunctionR3x3);

            REQUIRE_THROWS_WITH((TinyVector<1>{1}) + p_R_u, "error: incompatible operand types R^1 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyVector<2>{1, 2}) + p_R_u, "error: incompatible operand types R^2 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyVector<3>{2, 3, 2}) + p_R_u, "error: incompatible operand types R^3 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyMatrix<1>{2}) + p_R_u, "error: incompatible operand types R^1x1 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyMatrix<2>{2, 3, 1, 4}) + p_R_u,
                                "error: incompatible operand types R^2x2 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}) + p_R_u,
                                "error: incompatible operand types R^3x3 and Vh(P0:R)");

            REQUIRE_THROWS_WITH((double{1}) + p_Vector3_u, "error: incompatible operand types R and Vh(P0Vector:R)");
            REQUIRE_THROWS_WITH((TinyVector<1>{1}) + p_Vector3_u,
                                "error: incompatible operand types R^1 and Vh(P0Vector:R)");
            REQUIRE_THROWS_WITH((TinyVector<2>{1, 2}) + p_Vector3_u,
                                "error: incompatible operand types R^2 and Vh(P0Vector:R)");
          }
        }

        SECTION("difference")
        {
          SECTION("Vh - Vh -> Vh")
          {
            CHECK_EMBEDDED_VH2_TO_VH(p_R_u, -, p_R_v,   //
                                     DiscreteFunctionR, DiscreteFunctionR, DiscreteFunctionR);

            CHECK_EMBEDDED_VH2_TO_VH(p_R1_u, -, p_R1_v,   //
                                     DiscreteFunctionR1, DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_VH2_TO_VH(p_R2_u, -, p_R2_v,   //
                                     DiscreteFunctionR2, DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_VH2_TO_VH(p_R3_u, -, p_R3_v,   //
                                     DiscreteFunctionR3, DiscreteFunctionR3, DiscreteFunctionR3);

            CHECK_EMBEDDED_VH2_TO_VH(p_R1x1_u, -, p_R1x1_v,   //
                                     DiscreteFunctionR1x1, DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_VH2_TO_VH(p_R2x2_u, -, p_R2x2_v,   //
                                     DiscreteFunctionR2x2, DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_VH2_TO_VH(p_R3x3_u, -, p_R3x3_v,   //
                                     DiscreteFunctionR3x3, DiscreteFunctionR3x3, DiscreteFunctionR3x3);

            CHECK_EMBEDDED_VECTOR_VH2_TO_VH(p_Vector3_u, -, p_Vector3_v, DiscreteFunctionVector);

            REQUIRE_THROWS_WITH(p_R_u - p_R1_v, "error: incompatible operand types Vh(P0:R) and Vh(P0:R^1)");
            REQUIRE_THROWS_WITH(p_R2_u - p_R1_v, "error: incompatible operand types Vh(P0:R^2) and Vh(P0:R^1)");
            REQUIRE_THROWS_WITH(p_R3_u - p_R1x1_v, "error: incompatible operand types Vh(P0:R^3) and Vh(P0:R^1x1)");
            REQUIRE_THROWS_WITH(p_R_u - p_R2x2_v, "error: incompatible operand types Vh(P0:R) and Vh(P0:R^2x2)");
            REQUIRE_THROWS_WITH(p_Vector3_u - p_R_v, "error: incompatible operand types Vh(P0Vector:R) and Vh(P0:R)");
            REQUIRE_THROWS_WITH(p_Vector3_u - p_Vector2_w, "error: Vh(P0Vector:R) spaces have different sizes");

            REQUIRE_THROWS_WITH(p_R_u - p_other_mesh_R_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R1_u - p_other_mesh_R1_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R2_u - p_other_mesh_R2_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R3_u - p_other_mesh_R3_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R1x1_u - p_other_mesh_R1x1_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R2x2_u - p_other_mesh_R2x2_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R3x3_u - p_other_mesh_R3x3_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_Vector3_u - p_other_mesh_Vector3_u,
                                "error: operands are defined on different meshes");
          }

          SECTION("Vh - X -> Vh")
          {
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, -, bool{true},   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, -, uint64_t{1},   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, -, int64_t{2},   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, -, double{1.3},   //
                                      DiscreteFunctionR, DiscreteFunctionR);

            CHECK_EMBEDDED_VHxX_TO_VH(p_R1_u, -, (TinyVector<1>{1.3}),   //
                                      DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R2_u, -, (TinyVector<2>{1.2, 2.3}),   //
                                      DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R3_u, -, (TinyVector<3>{3.2, 7.1, 5.2}),   //
                                      DiscreteFunctionR3, DiscreteFunctionR3);

            CHECK_EMBEDDED_VHxX_TO_VH(p_R1x1_u, -, (TinyMatrix<1>{1.3}),   //
                                      DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R2x2_u, -, (TinyMatrix<2>{1.2, 2.3, 4.2, 5.1}),   //
                                      DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R3x3_u, -,
                                      (TinyMatrix<3>{3.2, 7.1, 5.2,     //
                                                     4.7, 2.3, 7.1,     //
                                                     9.7, 3.2, 6.8}),   //
                                      DiscreteFunctionR3x3, DiscreteFunctionR3x3);

            REQUIRE_THROWS_WITH(p_R_u - (TinyVector<1>{1}), "error: incompatible operand types Vh(P0:R) and R^1");
            REQUIRE_THROWS_WITH(p_R_u - (TinyVector<2>{1, 2}), "error: incompatible operand types Vh(P0:R) and R^2");
            REQUIRE_THROWS_WITH(p_R_u - (TinyVector<3>{2, 3, 2}), "error: incompatible operand types Vh(P0:R) and R^3");
            REQUIRE_THROWS_WITH(p_R_u - (TinyMatrix<1>{2}), "error: incompatible operand types Vh(P0:R) and R^1x1");
            REQUIRE_THROWS_WITH(p_R_u - (TinyMatrix<2>{2, 3, 1, 4}),
                                "error: incompatible operand types Vh(P0:R) and R^2x2");
            REQUIRE_THROWS_WITH(p_R_u - (TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}),
                                "error: incompatible operand types Vh(P0:R) and R^3x3");

            REQUIRE_THROWS_WITH(p_Vector3_u - (double{1}), "error: incompatible operand types Vh(P0Vector:R) and R");
            REQUIRE_THROWS_WITH(p_Vector3_u - (TinyVector<1>{1}),
                                "error: incompatible operand types Vh(P0Vector:R) and R^1");
            REQUIRE_THROWS_WITH(p_Vector3_u - (TinyVector<2>{1, 2}),
                                "error: incompatible operand types Vh(P0Vector:R) and R^2");
          }

          SECTION("X - Vh -> Vh")
          {
            CHECK_EMBEDDED_XxVH_TO_VH(bool{true}, -, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(uint64_t{1}, -, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(int64_t{2}, -, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(double{1.3}, -, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);

            CHECK_EMBEDDED_XxVH_TO_VH((TinyVector<1>{1.3}), -, p_R1_u,   //
                                      DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyVector<2>{1.2, 2.3}), -, p_R2_u,   //
                                      DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyVector<3>{3.2, 7.1, 5.2}), -, p_R3_u,   //
                                      DiscreteFunctionR3, DiscreteFunctionR3);

            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<1>{1.3}), -, p_R1x1_u,   //
                                      DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<2>{1.2, 2.3, 4.2, 5.1}), -, p_R2x2_u,   //
                                      DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<3>{3.2, 7.1, 5.2,   //
                                                     4.7, 2.3, 7.1,   //
                                                     9.7, 3.2, 6.8}),
                                      -, p_R3x3_u,   //
                                      DiscreteFunctionR3x3, DiscreteFunctionR3x3);

            REQUIRE_THROWS_WITH((TinyVector<1>{1}) - p_R_u, "error: incompatible operand types R^1 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyVector<2>{1, 2}) - p_R_u, "error: incompatible operand types R^2 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyVector<3>{2, 3, 2}) - p_R_u, "error: incompatible operand types R^3 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyMatrix<1>{2}) - p_R_u, "error: incompatible operand types R^1x1 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyMatrix<2>{2, 3, 1, 4}) - p_R_u,
                                "error: incompatible operand types R^2x2 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}) - p_R_u,
                                "error: incompatible operand types R^3x3 and Vh(P0:R)");

            REQUIRE_THROWS_WITH((double{1}) - p_Vector3_u, "error: incompatible operand types R and Vh(P0Vector:R)");
            REQUIRE_THROWS_WITH((TinyVector<1>{1}) - p_Vector3_u,
                                "error: incompatible operand types R^1 and Vh(P0Vector:R)");
            REQUIRE_THROWS_WITH((TinyVector<2>{1, 2}) - p_Vector3_u,
                                "error: incompatible operand types R^2 and Vh(P0Vector:R)");
          }
        }

        SECTION("product")
        {
          SECTION("Vh * Vh -> Vh")
          {
            CHECK_EMBEDDED_VH2_TO_VH(p_R_u, *, p_R_v,   //
                                     DiscreteFunctionR, DiscreteFunctionR, DiscreteFunctionR);

            CHECK_EMBEDDED_VH2_TO_VH(p_R1x1_u, *, p_R1x1_v,   //
                                     DiscreteFunctionR1x1, DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_VH2_TO_VH(p_R2x2_u, *, p_R2x2_v,   //
                                     DiscreteFunctionR2x2, DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_VH2_TO_VH(p_R3x3_u, *, p_R3x3_v,   //
                                     DiscreteFunctionR3x3, DiscreteFunctionR3x3, DiscreteFunctionR3x3);

            CHECK_EMBEDDED_VH2_TO_VH(p_R_u, *, p_R1_v,   //
                                     DiscreteFunctionR, DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_VH2_TO_VH(p_R_u, *, p_R2_v,   //
                                     DiscreteFunctionR, DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_VH2_TO_VH(p_R_u, *, p_R3_v,   //
                                     DiscreteFunctionR, DiscreteFunctionR3, DiscreteFunctionR3);

            CHECK_EMBEDDED_VH2_TO_VH(p_R_u, *, p_R1x1_v,   //
                                     DiscreteFunctionR, DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_VH2_TO_VH(p_R_u, *, p_R2x2_v,   //
                                     DiscreteFunctionR, DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_VH2_TO_VH(p_R_u, *, p_R3x3_v,   //
                                     DiscreteFunctionR, DiscreteFunctionR3x3, DiscreteFunctionR3x3);

            CHECK_EMBEDDED_VH2_TO_VH(p_R1x1_u, *, p_R1_v,   //
                                     DiscreteFunctionR1x1, DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_VH2_TO_VH(p_R2x2_u, *, p_R2_v,   //
                                     DiscreteFunctionR2x2, DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_VH2_TO_VH(p_R3x3_u, *, p_R3_v,   //
                                     DiscreteFunctionR3x3, DiscreteFunctionR3, DiscreteFunctionR3);

            {
              std::shared_ptr p_u_op_v = p_R_u * p_Vector3_v;

              REQUIRE(p_u_op_v.use_count() > 0);
              DiscreteFunctionVector u_op_v = p_u_op_v->get<DiscreteFunctionVector>();

              auto u_values = p_R_u->get<DiscreteFunctionR>().cellValues();
              auto v_arrays = p_Vector3_v->get<DiscreteFunctionVector>().cellArrays();
              bool is_same  = true;
              for (CellId cell_id = 0; cell_id < u_values.numberOfItems(); ++cell_id) {
                for (size_t i = 0; i < u_op_v.size(); ++i) {
                  if (u_op_v[cell_id][i] != (u_values[cell_id] * v_arrays[cell_id][i])) {
                    is_same = false;
                    break;
                  }
                }
              }

              REQUIRE(is_same);
            }

            REQUIRE_THROWS_WITH(p_R1_u * p_R1_v, "error: incompatible operand types Vh(P0:R^1) and Vh(P0:R^1)");
            REQUIRE_THROWS_WITH(p_R2_u * p_R1_v, "error: incompatible operand types Vh(P0:R^2) and Vh(P0:R^1)");
            REQUIRE_THROWS_WITH(p_R3_u * p_R1x1_v, "error: incompatible operand types Vh(P0:R^3) and Vh(P0:R^1x1)");
            REQUIRE_THROWS_WITH(p_R1_u * p_R2x2_v, "error: incompatible operand types Vh(P0:R^1) and Vh(P0:R^2x2)");

            REQUIRE_THROWS_WITH(p_R1x1_u * p_R2x2_v, "error: incompatible operand types Vh(P0:R^1x1) and Vh(P0:R^2x2)");
            REQUIRE_THROWS_WITH(p_R2x2_u * p_R3x3_v, "error: incompatible operand types Vh(P0:R^2x2) and Vh(P0:R^3x3)");
            REQUIRE_THROWS_WITH(p_R3x3_u * p_R1x1_v, "error: incompatible operand types Vh(P0:R^3x3) and Vh(P0:R^1x1)");

            REQUIRE_THROWS_WITH(p_R1x1_u * p_R2_v, "error: incompatible operand types Vh(P0:R^1x1) and Vh(P0:R^2)");
            REQUIRE_THROWS_WITH(p_R2x2_u * p_R3_v, "error: incompatible operand types Vh(P0:R^2x2) and Vh(P0:R^3)");
            REQUIRE_THROWS_WITH(p_R3x3_u * p_R1_v, "error: incompatible operand types Vh(P0:R^3x3) and Vh(P0:R^1)");

            REQUIRE_THROWS_WITH(p_R1_u * p_Vector3_v,
                                "error: incompatible operand types Vh(P0:R^1) and Vh(P0Vector:R)");
            REQUIRE_THROWS_WITH(p_R2_u * p_Vector3_v,
                                "error: incompatible operand types Vh(P0:R^2) and Vh(P0Vector:R)");
            REQUIRE_THROWS_WITH(p_R3_u * p_Vector3_v,
                                "error: incompatible operand types Vh(P0:R^3) and Vh(P0Vector:R)");
            REQUIRE_THROWS_WITH(p_R1x1_u * p_Vector3_v,
                                "error: incompatible operand types Vh(P0:R^1x1) and Vh(P0Vector:R)");
            REQUIRE_THROWS_WITH(p_R2x2_u * p_Vector3_v,
                                "error: incompatible operand types Vh(P0:R^2x2) and Vh(P0Vector:R)");
            REQUIRE_THROWS_WITH(p_R3x3_u * p_Vector3_v,
                                "error: incompatible operand types Vh(P0:R^3x3) and Vh(P0Vector:R)");
            REQUIRE_THROWS_WITH(p_Vector3_u * p_Vector3_v,
                                "error: incompatible operand types Vh(P0Vector:R) and Vh(P0Vector:R)");

            REQUIRE_THROWS_WITH(p_Vector3_v * p_R_u, "error: incompatible operand types Vh(P0Vector:R) and Vh(P0:R)");
            REQUIRE_THROWS_WITH(p_Vector3_v * p_R1_u,
                                "error: incompatible operand types Vh(P0Vector:R) and Vh(P0:R^1)");
            REQUIRE_THROWS_WITH(p_Vector3_v * p_R2_u,
                                "error: incompatible operand types Vh(P0Vector:R) and Vh(P0:R^2)");
            REQUIRE_THROWS_WITH(p_Vector3_v * p_R3_u,
                                "error: incompatible operand types Vh(P0Vector:R) and Vh(P0:R^3)");
            REQUIRE_THROWS_WITH(p_Vector3_v * p_R1x1_u,
                                "error: incompatible operand types Vh(P0Vector:R) and Vh(P0:R^1x1)");
            REQUIRE_THROWS_WITH(p_Vector3_v * p_R2x2_u,
                                "error: incompatible operand types Vh(P0Vector:R) and Vh(P0:R^2x2)");
            REQUIRE_THROWS_WITH(p_Vector3_v * p_R3x3_u,
                                "error: incompatible operand types Vh(P0Vector:R) and Vh(P0:R^3x3)");

            REQUIRE_THROWS_WITH(p_R_u * p_other_mesh_R1_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R_u * p_other_mesh_R2_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R_u * p_other_mesh_R3_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R_u * p_other_mesh_R1x1_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R_u * p_other_mesh_R2x2_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R_u * p_other_mesh_R3x3_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R1x1_u * p_other_mesh_R1_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R2x2_u * p_other_mesh_R2_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R3x3_u * p_other_mesh_R3_u, "error: operands are defined on different meshes");
            REQUIRE_THROWS_WITH(p_R_u * p_other_mesh_Vector3_u, "error: operands are defined on different meshes");
          }

          SECTION("Vh * X -> Vh")
          {
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, *, bool{true},   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, *, uint64_t{1},   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, *, int64_t{2},   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R_u, *, double{1.3},   //
                                      DiscreteFunctionR, DiscreteFunctionR);

            CHECK_EMBEDDED_VHxX_TO_VH(p_R1x1_u, *, (TinyMatrix<1>{1.3}),   //
                                      DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R2x2_u, *, (TinyMatrix<2>{1.2, 2.3, 4.2, 5.1}),   //
                                      DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R3x3_u, *,
                                      (TinyMatrix<3>{3.2, 7.1, 5.2,     //
                                                     4.7, 2.3, 7.1,     //
                                                     9.7, 3.2, 6.8}),   //
                                      DiscreteFunctionR3x3, DiscreteFunctionR3x3);

            CHECK_EMBEDDED_VHxX_TO_VH(p_R1x1_u, *, (TinyVector<1>{1.3}),   //
                                      DiscreteFunctionR1x1, DiscreteFunctionR1);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R2x2_u, *, (TinyVector<2>{1.2, 2.3}),   //
                                      DiscreteFunctionR2x2, DiscreteFunctionR2);
            CHECK_EMBEDDED_VHxX_TO_VH(p_R3x3_u, *, (TinyVector<3>{3.2, 7.1, 5.2}),   //
                                      DiscreteFunctionR3x3, DiscreteFunctionR3);

            REQUIRE_THROWS_WITH(p_R1_u * (TinyVector<1>{1}), "error: incompatible operand types Vh(P0:R^1) and R^1");
            REQUIRE_THROWS_WITH(p_R2_u * (TinyVector<2>{1, 2}), "error: incompatible operand types Vh(P0:R^2) and R^2");
            REQUIRE_THROWS_WITH(p_R3_u * (TinyVector<3>{2, 3, 2}),
                                "error: incompatible operand types Vh(P0:R^3) and R^3");
            REQUIRE_THROWS_WITH(p_R1_u * (TinyMatrix<1>{2}), "error: incompatible operand types Vh(P0:R^1) and R^1x1");
            REQUIRE_THROWS_WITH(p_R2_u * (TinyMatrix<2>{2, 3, 1, 4}),
                                "error: incompatible operand types Vh(P0:R^2) and R^2x2");
            REQUIRE_THROWS_WITH(p_R3_u * (TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}),
                                "error: incompatible operand types Vh(P0:R^3) and R^3x3");
            REQUIRE_THROWS_WITH(p_R2x2_u * (TinyMatrix<1>{2}),
                                "error: incompatible operand types Vh(P0:R^2x2) and R^1x1");
            REQUIRE_THROWS_WITH(p_R1x1_u * (TinyMatrix<2>{2, 3, 1, 4}),
                                "error: incompatible operand types Vh(P0:R^1x1) and R^2x2");
            REQUIRE_THROWS_WITH(p_R2x2_u * (TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}),
                                "error: incompatible operand types Vh(P0:R^2x2) and R^3x3");

            REQUIRE_THROWS_WITH(p_R2x2_u * (TinyVector<1>{2}),
                                "error: incompatible operand types Vh(P0:R^2x2) and R^1");

            REQUIRE_THROWS_WITH(p_R3x3_u * (TinyVector<2>{2, 1}),
                                "error: incompatible operand types Vh(P0:R^3x3) and R^2");

            REQUIRE_THROWS_WITH(p_Vector3_u * (TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}),
                                "error: incompatible operand types Vh(P0Vector:R) and R^3x3");
            REQUIRE_THROWS_WITH(p_Vector3_u * (double{2}), "error: incompatible operand types Vh(P0Vector:R) and R");
          }

          SECTION("X * Vh -> Vh")
          {
            CHECK_EMBEDDED_XxVH_TO_VH(bool{true}, *, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(uint64_t{1}, *, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(int64_t{2}, *, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(double{1.3}, *, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);

            CHECK_EMBEDDED_XxVH_TO_VH(bool{true}, *, p_R1_u,   //
                                      DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_XxVH_TO_VH(uint64_t{1}, *, p_R1_u,   //
                                      DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_XxVH_TO_VH(int64_t{2}, *, p_R1_u,   //
                                      DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_XxVH_TO_VH(double{1.3}, *, p_R1_u,   //
                                      DiscreteFunctionR1, DiscreteFunctionR1);

            CHECK_EMBEDDED_XxVH_TO_VH(bool{true}, *, p_R2_u,   //
                                      DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_XxVH_TO_VH(uint64_t{1}, *, p_R2_u,   //
                                      DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_XxVH_TO_VH(int64_t{2}, *, p_R2_u,   //
                                      DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_XxVH_TO_VH(double{1.3}, *, p_R2_u,   //
                                      DiscreteFunctionR2, DiscreteFunctionR2);

            CHECK_EMBEDDED_XxVH_TO_VH(bool{true}, *, p_R3_u,   //
                                      DiscreteFunctionR3, DiscreteFunctionR3);
            CHECK_EMBEDDED_XxVH_TO_VH(uint64_t{1}, *, p_R3_u,   //
                                      DiscreteFunctionR3, DiscreteFunctionR3);
            CHECK_EMBEDDED_XxVH_TO_VH(int64_t{2}, *, p_R3_u,   //
                                      DiscreteFunctionR3, DiscreteFunctionR3);
            CHECK_EMBEDDED_XxVH_TO_VH(double{1.3}, *, p_R3_u,   //
                                      DiscreteFunctionR3, DiscreteFunctionR3);

            CHECK_EMBEDDED_XxVH_TO_VH(bool{true}, *, p_R1x1_u,   //
                                      DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_XxVH_TO_VH(uint64_t{1}, *, p_R1x1_u,   //
                                      DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_XxVH_TO_VH(int64_t{2}, *, p_R1x1_u,   //
                                      DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_XxVH_TO_VH(double{1.3}, *, p_R1x1_u,   //
                                      DiscreteFunctionR1x1, DiscreteFunctionR1x1);

            CHECK_EMBEDDED_XxVH_TO_VH(bool{true}, *, p_R2x2_u,   //
                                      DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_XxVH_TO_VH(uint64_t{1}, *, p_R2x2_u,   //
                                      DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_XxVH_TO_VH(int64_t{2}, *, p_R2x2_u,   //
                                      DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_XxVH_TO_VH(double{1.3}, *, p_R2x2_u,   //
                                      DiscreteFunctionR2x2, DiscreteFunctionR2x2);

            CHECK_EMBEDDED_XxVH_TO_VH(bool{true}, *, p_R3x3_u,   //
                                      DiscreteFunctionR3x3, DiscreteFunctionR3x3);
            CHECK_EMBEDDED_XxVH_TO_VH(uint64_t{1}, *, p_R3x3_u,   //
                                      DiscreteFunctionR3x3, DiscreteFunctionR3x3);
            CHECK_EMBEDDED_XxVH_TO_VH(int64_t{2}, *, p_R3x3_u,   //
                                      DiscreteFunctionR3x3, DiscreteFunctionR3x3);
            CHECK_EMBEDDED_XxVH_TO_VH(double{1.3}, *, p_R3x3_u,   //
                                      DiscreteFunctionR3x3, DiscreteFunctionR3x3);

            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<1>{1.3}), *, p_R1_u,   //
                                      DiscreteFunctionR1, DiscreteFunctionR1);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<2>{1.2, 2.3, 4.2, 5.1}), *, p_R2_u,   //
                                      DiscreteFunctionR2, DiscreteFunctionR2);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<3>{3.2, 7.1, 5.2,   //
						       4.7, 2.3, 7.1,   //
						       9.7, 3.2, 6.8}), *, p_R3_u,   //
                                        DiscreteFunctionR3, DiscreteFunctionR3);

            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<1>{1.3}), *, p_R1x1_u,   //
                                      DiscreteFunctionR1x1, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<2>{1.2, 2.3, 4.2, 5.1}), *, p_R2x2_u,   //
                                      DiscreteFunctionR2x2, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_XxVH_TO_VH((TinyMatrix<3>{3.2, 7.1, 5.2,   //
						       4.7, 2.3, 7.1,   //
						       9.7, 3.2, 6.8}), *, p_R3x3_u,   //
	      DiscreteFunctionR3x3, DiscreteFunctionR3x3);

            CHECK_EMBEDDED_VECTOR_XxVH_TO_VH(bool{true}, *, p_Vector3_u, DiscreteFunctionVector);
            CHECK_EMBEDDED_VECTOR_XxVH_TO_VH(uint64_t{1}, *, p_Vector3_u, DiscreteFunctionVector);
            CHECK_EMBEDDED_VECTOR_XxVH_TO_VH(int64_t{2}, *, p_Vector3_u, DiscreteFunctionVector);
            CHECK_EMBEDDED_VECTOR_XxVH_TO_VH(double{1.3}, *, p_Vector3_u, DiscreteFunctionVector);

            REQUIRE_THROWS_WITH((TinyMatrix<1>{2}) * p_R_u, "error: incompatible operand types R^1x1 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyMatrix<2>{2, 3, 1, 4}) * p_R_u,
                                "error: incompatible operand types R^2x2 and Vh(P0:R)");
            REQUIRE_THROWS_WITH((TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}) * p_R_u,
                                "error: incompatible operand types R^3x3 and Vh(P0:R)");

            REQUIRE_THROWS_WITH((TinyMatrix<1>{2}) * p_R2_u, "error: incompatible operand types R^1x1 and Vh(P0:R^2)");
            REQUIRE_THROWS_WITH((TinyMatrix<2>{2, 3, 1, 4}) * p_R3_u,
                                "error: incompatible operand types R^2x2 and Vh(P0:R^3)");
            REQUIRE_THROWS_WITH((TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}) * p_R2_u,
                                "error: incompatible operand types R^3x3 and Vh(P0:R^2)");
            REQUIRE_THROWS_WITH((TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}) * p_R1_u,
                                "error: incompatible operand types R^3x3 and Vh(P0:R^1)");

            REQUIRE_THROWS_WITH((TinyMatrix<1>{2}) * p_R2x2_u,
                                "error: incompatible operand types R^1x1 and Vh(P0:R^2x2)");
            REQUIRE_THROWS_WITH((TinyMatrix<2>{2, 3, 1, 4}) * p_R3x3_u,
                                "error: incompatible operand types R^2x2 and Vh(P0:R^3x3)");
            REQUIRE_THROWS_WITH((TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}) * p_R2x2_u,
                                "error: incompatible operand types R^3x3 and Vh(P0:R^2x2)");
            REQUIRE_THROWS_WITH((TinyMatrix<2>{2, 3, 1, 4}) * p_R1x1_u,
                                "error: incompatible operand types R^2x2 and Vh(P0:R^1x1)");

            REQUIRE_THROWS_WITH((TinyMatrix<3>{1, 3, 6, 4, 7, 2, 5, 9, 8}) * p_Vector3_u,
                                "error: incompatible operand types R^3x3 and Vh(P0Vector:R)");
            REQUIRE_THROWS_WITH((TinyMatrix<1>{2}) * p_Vector3_u,
                                "error: incompatible operand types R^1x1 and Vh(P0Vector:R)");
          }
        }

        SECTION("ratio")
        {
          SECTION("Vh / Vh -> Vh")
          {
            CHECK_EMBEDDED_VH2_TO_VH(p_R_u, /, p_R_v,   //
                                     DiscreteFunctionR, DiscreteFunctionR, DiscreteFunctionR);

            REQUIRE_THROWS_WITH(p_R_u / p_R1_v, "error: incompatible operand types Vh(P0:R) and Vh(P0:R^1)");
            REQUIRE_THROWS_WITH(p_R2_u / p_R1_v, "error: incompatible operand types Vh(P0:R^2) and Vh(P0:R^1)");
            REQUIRE_THROWS_WITH(p_R3_u / p_R1x1_v, "error: incompatible operand types Vh(P0:R^3) and Vh(P0:R^1x1)");
            REQUIRE_THROWS_WITH(p_R_u / p_R2x2_v, "error: incompatible operand types Vh(P0:R) and Vh(P0:R^2x2)");

            REQUIRE_THROWS_WITH(p_R_u / p_other_mesh_R_u, "error: operands are defined on different meshes");
          }

          SECTION("X / Vh -> Vh")
          {
            CHECK_EMBEDDED_XxVH_TO_VH(bool{true}, /, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(uint64_t{1}, /, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(int64_t{2}, /, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);
            CHECK_EMBEDDED_XxVH_TO_VH(double{1.3}, /, p_R_u,   //
                                      DiscreteFunctionR, DiscreteFunctionR);

            REQUIRE_THROWS_WITH(3.2 / p_R1_v, "error: incompatible operand types R and Vh(P0:R^1)");
          }
        }
      }

      SECTION("unary operators")
      {
        SECTION("unary minus")
        {
          SECTION("- Vh -> Vh")
          {
            CHECK_EMBEDDED_VH_TO_VH(-, p_R_u, DiscreteFunctionR);

            CHECK_EMBEDDED_VH_TO_VH(-, p_R1_u, DiscreteFunctionR1);
            CHECK_EMBEDDED_VH_TO_VH(-, p_R2_u, DiscreteFunctionR2);
            CHECK_EMBEDDED_VH_TO_VH(-, p_R3_u, DiscreteFunctionR3);

            CHECK_EMBEDDED_VH_TO_VH(-, p_R1x1_u, DiscreteFunctionR1x1);
            CHECK_EMBEDDED_VH_TO_VH(-, p_R2x2_u, DiscreteFunctionR2x2);
            CHECK_EMBEDDED_VH_TO_VH(-, p_R3x3_u, DiscreteFunctionR3x3);

            CHECK_EMBEDDED_VECTOR_VH_TO_VH(-, p_Vector3_u, DiscreteFunctionVector);
          }
        }
      }
    }
  }
}

#ifdef __clang__
#pragma clang optimize on
#endif   // __clang__
