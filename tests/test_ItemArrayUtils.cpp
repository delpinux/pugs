#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemArray.hpp>
#include <mesh/ItemArrayUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <utils/Messenger.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ItemArrayUtils", "[mesh]")
{
  SECTION("Synchronize")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d_v = named_mesh.mesh();
          auto mesh_1d   = mesh_1d_v->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          SECTION("node")
          {
            WeakNodeArray<size_t> weak_node_array{connectivity, 4};
            auto node_number = connectivity.nodeNumber();

            for (NodeId i_node = 0; i_node < mesh_1d->numberOfNodes(); ++i_node) {
              auto array          = weak_node_array[i_node];
              const size_t number = node_number[i_node];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            NodeArray<const size_t> node_array{weak_node_array};

            REQUIRE(node_array.connectivity_ptr() == weak_node_array.connectivity_ptr());

            {   // before synchronization
              auto node_owner    = connectivity.nodeOwner();
              auto node_is_owned = connectivity.nodeIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (NodeId i_node = 0; i_node < mesh_1d->numberOfNodes(); ++i_node) {
                auto array          = node_array[i_node];
                const size_t number = node_number[i_node];
                const size_t owner  = node_owner[i_node];
                if (node_is_owned[i_node]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(node_array));
              }
            }

            synchronize(weak_node_array);

            {   // after synchronization
              auto node_owner = connectivity.nodeOwner();

              bool is_synchronized = true;
              for (NodeId i_node = 0; i_node < mesh_1d->numberOfNodes(); ++i_node) {
                auto array          = node_array[i_node];
                const size_t number = node_number[i_node];
                const size_t owner  = node_owner[i_node];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
              REQUIRE(isSynchronized(node_array));
            }
          }

          SECTION("edge")
          {
            WeakEdgeArray<size_t> weak_edge_array{connectivity, 4};
            auto edge_number = connectivity.edgeNumber();

            for (EdgeId i_edge = 0; i_edge < mesh_1d->numberOfEdges(); ++i_edge) {
              auto array          = weak_edge_array[i_edge];
              const size_t number = edge_number[i_edge];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            EdgeArray<const size_t> edge_array{weak_edge_array};

            REQUIRE(edge_array.connectivity_ptr() == weak_edge_array.connectivity_ptr());

            {   // before synchronization
              auto edge_owner    = connectivity.edgeOwner();
              auto edge_is_owned = connectivity.edgeIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (EdgeId i_edge = 0; i_edge < mesh_1d->numberOfEdges(); ++i_edge) {
                auto array          = edge_array[i_edge];
                const size_t number = edge_number[i_edge];
                const size_t owner  = edge_owner[i_edge];
                if (edge_is_owned[i_edge]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(edge_array));
              }
            }

            synchronize(weak_edge_array);

            {   // after synchronization
              auto edge_owner = connectivity.edgeOwner();

              bool is_synchronized = true;
              for (EdgeId i_edge = 0; i_edge < mesh_1d->numberOfEdges(); ++i_edge) {
                auto array          = edge_array[i_edge];
                const size_t number = edge_number[i_edge];
                const size_t owner  = edge_owner[i_edge];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
            }
            REQUIRE(isSynchronized(edge_array));
          }

          SECTION("face")
          {
            WeakFaceArray<size_t> weak_face_array{connectivity, 4};
            auto face_number = connectivity.faceNumber();

            for (FaceId i_face = 0; i_face < mesh_1d->numberOfFaces(); ++i_face) {
              auto array          = weak_face_array[i_face];
              const size_t number = face_number[i_face];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            FaceArray<const size_t> face_array{weak_face_array};

            REQUIRE(face_array.connectivity_ptr() == weak_face_array.connectivity_ptr());

            {   // before synchronization
              auto face_owner    = connectivity.faceOwner();
              auto face_is_owned = connectivity.faceIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (FaceId i_face = 0; i_face < mesh_1d->numberOfFaces(); ++i_face) {
                auto array          = face_array[i_face];
                const size_t number = face_number[i_face];
                const size_t owner  = face_owner[i_face];
                if (face_is_owned[i_face]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(face_array));
              }
            }

            synchronize(weak_face_array);

            {   // after synchronization
              auto face_owner = connectivity.faceOwner();

              bool is_synchronized = true;
              for (FaceId i_face = 0; i_face < mesh_1d->numberOfFaces(); ++i_face) {
                auto array          = face_array[i_face];
                const size_t number = face_number[i_face];
                const size_t owner  = face_owner[i_face];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
            }
            REQUIRE(isSynchronized(face_array));
          }

          SECTION("cell")
          {
            WeakCellArray<size_t> weak_cell_array{connectivity, 4};
            auto cell_number = connectivity.cellNumber();

            for (CellId i_cell = 0; i_cell < mesh_1d->numberOfCells(); ++i_cell) {
              auto array          = weak_cell_array[i_cell];
              const size_t number = cell_number[i_cell];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            CellArray<const size_t> cell_array{weak_cell_array};

            REQUIRE(cell_array.connectivity_ptr() == weak_cell_array.connectivity_ptr());

            {   // before synchronization
              auto cell_owner    = connectivity.cellOwner();
              auto cell_is_owned = connectivity.cellIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (CellId i_cell = 0; i_cell < mesh_1d->numberOfCells(); ++i_cell) {
                auto array          = cell_array[i_cell];
                const size_t number = cell_number[i_cell];
                const size_t owner  = cell_owner[i_cell];
                if (cell_is_owned[i_cell]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(cell_array));
              }
            }

            synchronize(weak_cell_array);

            {   // after synchronization
              auto cell_owner = connectivity.cellOwner();

              bool is_synchronized = true;
              for (CellId i_cell = 0; i_cell < mesh_1d->numberOfCells(); ++i_cell) {
                auto array          = cell_array[i_cell];
                const size_t number = cell_number[i_cell];
                const size_t owner  = cell_owner[i_cell];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
            }
            REQUIRE(isSynchronized(cell_array));
          }
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          SECTION("node")
          {
            WeakNodeArray<size_t> weak_node_array{connectivity, 3};
            auto node_number = connectivity.nodeNumber();

            for (NodeId i_node = 0; i_node < mesh_2d->numberOfNodes(); ++i_node) {
              auto array          = weak_node_array[i_node];
              const size_t number = node_number[i_node];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            NodeArray<const size_t> node_array{weak_node_array};

            REQUIRE(node_array.connectivity_ptr() == weak_node_array.connectivity_ptr());

            {   // before synchronization
              auto node_owner    = connectivity.nodeOwner();
              auto node_is_owned = connectivity.nodeIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (NodeId i_node = 0; i_node < mesh_2d->numberOfNodes(); ++i_node) {
                auto array          = node_array[i_node];
                const size_t number = node_number[i_node];
                const size_t owner  = node_owner[i_node];
                if (node_is_owned[i_node]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(node_array));
              }
            }

            synchronize(weak_node_array);

            {   // after synchronization
              auto node_owner = connectivity.nodeOwner();

              bool is_synchronized = true;
              for (NodeId i_node = 0; i_node < mesh_2d->numberOfNodes(); ++i_node) {
                auto array          = node_array[i_node];
                const size_t number = node_number[i_node];
                const size_t owner  = node_owner[i_node];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
              REQUIRE(isSynchronized(node_array));
            }
          }

          SECTION("edge")
          {
            WeakEdgeArray<size_t> weak_edge_array{connectivity, 3};
            auto edge_number = connectivity.edgeNumber();

            for (EdgeId i_edge = 0; i_edge < mesh_2d->numberOfEdges(); ++i_edge) {
              auto array          = weak_edge_array[i_edge];
              const size_t number = edge_number[i_edge];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            EdgeArray<const size_t> edge_array{weak_edge_array};

            REQUIRE(edge_array.connectivity_ptr() == weak_edge_array.connectivity_ptr());

            {   // before synchronization
              auto edge_owner    = connectivity.edgeOwner();
              auto edge_is_owned = connectivity.edgeIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (EdgeId i_edge = 0; i_edge < mesh_2d->numberOfEdges(); ++i_edge) {
                auto array          = edge_array[i_edge];
                const size_t number = edge_number[i_edge];
                const size_t owner  = edge_owner[i_edge];
                if (edge_is_owned[i_edge]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(edge_array));
              }
            }

            synchronize(weak_edge_array);

            {   // after synchronization
              auto edge_owner = connectivity.edgeOwner();

              bool is_synchronized = true;
              for (EdgeId i_edge = 0; i_edge < mesh_2d->numberOfEdges(); ++i_edge) {
                auto array          = edge_array[i_edge];
                const size_t number = edge_number[i_edge];
                const size_t owner  = edge_owner[i_edge];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
            }
            REQUIRE(isSynchronized(edge_array));
          }

          SECTION("face")
          {
            WeakFaceArray<size_t> weak_face_array{connectivity, 3};
            auto face_number = connectivity.faceNumber();

            for (FaceId i_face = 0; i_face < mesh_2d->numberOfFaces(); ++i_face) {
              auto array          = weak_face_array[i_face];
              const size_t number = face_number[i_face];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            FaceArray<const size_t> face_array{weak_face_array};

            REQUIRE(face_array.connectivity_ptr() == weak_face_array.connectivity_ptr());

            {   // before synchronization
              auto face_owner    = connectivity.faceOwner();
              auto face_is_owned = connectivity.faceIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (FaceId i_face = 0; i_face < mesh_2d->numberOfFaces(); ++i_face) {
                auto array          = face_array[i_face];
                const size_t number = face_number[i_face];
                const size_t owner  = face_owner[i_face];
                if (face_is_owned[i_face]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(face_array));
              }
            }

            synchronize(weak_face_array);

            {   // after synchronization
              auto face_owner = connectivity.faceOwner();

              bool is_synchronized = true;
              for (FaceId i_face = 0; i_face < mesh_2d->numberOfFaces(); ++i_face) {
                auto array          = face_array[i_face];
                const size_t number = face_number[i_face];
                const size_t owner  = face_owner[i_face];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
            }
            REQUIRE(isSynchronized(face_array));
          }

          SECTION("cell")
          {
            WeakCellArray<size_t> weak_cell_array{connectivity, 3};
            auto cell_number = connectivity.cellNumber();

            for (CellId i_cell = 0; i_cell < mesh_2d->numberOfCells(); ++i_cell) {
              auto array          = weak_cell_array[i_cell];
              const size_t number = cell_number[i_cell];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            CellArray<const size_t> cell_array{weak_cell_array};

            REQUIRE(cell_array.connectivity_ptr() == weak_cell_array.connectivity_ptr());

            {   // before synchronization
              auto cell_owner    = connectivity.cellOwner();
              auto cell_is_owned = connectivity.cellIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (CellId i_cell = 0; i_cell < mesh_2d->numberOfCells(); ++i_cell) {
                auto array          = cell_array[i_cell];
                const size_t number = cell_number[i_cell];
                const size_t owner  = cell_owner[i_cell];
                if (cell_is_owned[i_cell]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(cell_array));
              }
            }

            synchronize(weak_cell_array);

            {   // after synchronization
              auto cell_owner = connectivity.cellOwner();

              bool is_synchronized = true;
              for (CellId i_cell = 0; i_cell < mesh_2d->numberOfCells(); ++i_cell) {
                auto array          = cell_array[i_cell];
                const size_t number = cell_number[i_cell];
                const size_t owner  = cell_owner[i_cell];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
            }
            REQUIRE(isSynchronized(cell_array));
          }
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          SECTION("node")
          {
            WeakNodeArray<size_t> weak_node_array{connectivity, 4};
            auto node_number = connectivity.nodeNumber();

            for (NodeId i_node = 0; i_node < mesh_3d->numberOfNodes(); ++i_node) {
              auto array          = weak_node_array[i_node];
              const size_t number = node_number[i_node];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            NodeArray<const size_t> node_array{weak_node_array};

            REQUIRE(node_array.connectivity_ptr() == weak_node_array.connectivity_ptr());

            {   // before synchronization
              auto node_owner    = connectivity.nodeOwner();
              auto node_is_owned = connectivity.nodeIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (NodeId i_node = 0; i_node < mesh_3d->numberOfNodes(); ++i_node) {
                auto array          = node_array[i_node];
                const size_t number = node_number[i_node];
                const size_t owner  = node_owner[i_node];
                if (node_is_owned[i_node]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(node_array));
              }
            }

            synchronize(weak_node_array);

            {   // after synchronization
              auto node_owner = connectivity.nodeOwner();

              bool is_synchronized = true;
              for (NodeId i_node = 0; i_node < mesh_3d->numberOfNodes(); ++i_node) {
                auto array          = node_array[i_node];
                const size_t number = node_number[i_node];
                const size_t owner  = node_owner[i_node];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
            }
            REQUIRE(isSynchronized(node_array));
          }

          SECTION("edge")
          {
            WeakEdgeArray<size_t> weak_edge_array{connectivity, 4};
            auto edge_number = connectivity.edgeNumber();

            for (EdgeId i_edge = 0; i_edge < mesh_3d->numberOfEdges(); ++i_edge) {
              auto array          = weak_edge_array[i_edge];
              const size_t number = edge_number[i_edge];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            EdgeArray<const size_t> edge_array{weak_edge_array};

            REQUIRE(edge_array.connectivity_ptr() == weak_edge_array.connectivity_ptr());

            {   // before synchronization
              auto edge_owner    = connectivity.edgeOwner();
              auto edge_is_owned = connectivity.edgeIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (EdgeId i_edge = 0; i_edge < mesh_3d->numberOfEdges(); ++i_edge) {
                auto array          = edge_array[i_edge];
                const size_t number = edge_number[i_edge];
                const size_t owner  = edge_owner[i_edge];
                if (edge_is_owned[i_edge]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(edge_array));
              }
            }

            synchronize(weak_edge_array);

            {   // after synchronization
              auto edge_owner = connectivity.edgeOwner();

              bool is_synchronized = true;
              for (EdgeId i_edge = 0; i_edge < mesh_3d->numberOfEdges(); ++i_edge) {
                auto array          = edge_array[i_edge];
                const size_t number = edge_number[i_edge];
                const size_t owner  = edge_owner[i_edge];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
            }
            REQUIRE(isSynchronized(edge_array));
          }

          SECTION("face")
          {
            WeakFaceArray<size_t> weak_face_array{connectivity, 4};
            auto face_number = connectivity.faceNumber();

            for (FaceId i_face = 0; i_face < mesh_3d->numberOfFaces(); ++i_face) {
              auto array          = weak_face_array[i_face];
              const size_t number = face_number[i_face];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            FaceArray<const size_t> face_array{weak_face_array};

            REQUIRE(face_array.connectivity_ptr() == weak_face_array.connectivity_ptr());

            {   // before synchronization
              auto face_owner    = connectivity.faceOwner();
              auto face_is_owned = connectivity.faceIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (FaceId i_face = 0; i_face < mesh_3d->numberOfFaces(); ++i_face) {
                auto array          = face_array[i_face];
                const size_t number = face_number[i_face];
                const size_t owner  = face_owner[i_face];
                if (face_is_owned[i_face]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(face_array));
              }
            }

            synchronize(weak_face_array);

            {   // after synchronization
              auto face_owner = connectivity.faceOwner();

              bool is_synchronized = true;
              for (FaceId i_face = 0; i_face < mesh_3d->numberOfFaces(); ++i_face) {
                auto array          = face_array[i_face];
                const size_t number = face_number[i_face];
                const size_t owner  = face_owner[i_face];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
            }
            REQUIRE(isSynchronized(face_array));
          }

          SECTION("cell")
          {
            WeakCellArray<size_t> weak_cell_array{connectivity, 4};
            auto cell_number = connectivity.cellNumber();

            for (CellId i_cell = 0; i_cell < mesh_3d->numberOfCells(); ++i_cell) {
              auto array          = weak_cell_array[i_cell];
              const size_t number = cell_number[i_cell];
              for (size_t i = 0; i < array.size(); ++i) {
                array[i] = number + (parallel::rank() + 1) * i;
              }
            }

            CellArray<const size_t> cell_array{weak_cell_array};

            REQUIRE(cell_array.connectivity_ptr() == weak_cell_array.connectivity_ptr());

            {   // before synchronization
              auto cell_owner    = connectivity.cellOwner();
              auto cell_is_owned = connectivity.cellIsOwned();

              bool is_synchronized = (parallel::size() > 1);
              bool is_valid        = true;
              for (CellId i_cell = 0; i_cell < mesh_3d->numberOfCells(); ++i_cell) {
                auto array          = cell_array[i_cell];
                const size_t number = cell_number[i_cell];
                const size_t owner  = cell_owner[i_cell];
                if (cell_is_owned[i_cell]) {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_valid &= (array[i] == number + (owner + 1) * i);
                  }
                } else {
                  for (size_t i = 0; i < array.size(); ++i) {
                    is_synchronized &= (array[i] == number + (owner + 1) * i);
                  }
                }
              }

              REQUIRE(is_valid);
              REQUIRE(not is_synchronized);
              if (parallel::size() > 1) {
                REQUIRE(not isSynchronized(cell_array));
              }
            }

            synchronize(weak_cell_array);

            {   // after synchronization
              auto cell_owner = connectivity.cellOwner();

              bool is_synchronized = true;
              for (CellId i_cell = 0; i_cell < mesh_3d->numberOfCells(); ++i_cell) {
                auto array          = cell_array[i_cell];
                const size_t number = cell_number[i_cell];
                const size_t owner  = cell_owner[i_cell];
                for (size_t i = 0; i < array.size(); ++i) {
                  is_synchronized &= (array[i] == number + (owner + 1) * i);
                }
              }

              REQUIRE(is_synchronized);
            }
            REQUIRE(isSynchronized(cell_array));
          }
        }
      }
    }
  }

  SECTION("min")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d_v = named_mesh.mesh();
          auto mesh_1d   = mesh_1d_v->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          CellArray<int> cell_array{connectivity, 3};
          cell_array.fill(-1);

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_1d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                for (size_t i = 0; i < cell_array.sizeOfArrays(); ++i) {
                  cell_array[cell_id][i] = 10 + parallel::rank() + i;
                }
              }
            });

          REQUIRE(min(cell_array) == 10);
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          CellArray<int> cell_array{connectivity, 3};
          cell_array.fill(-1);

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_2d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                for (size_t i = 0; i < cell_array.sizeOfArrays(); ++i) {
                  cell_array[cell_id][i] = 10 + parallel::rank() - i;
                }
              }
            });

          REQUIRE(min(cell_array) == 8);
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          CellArray<int> cell_array{connectivity, 3};
          cell_array.fill(-1);

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_3d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                for (size_t i = 0; i < cell_array.sizeOfArrays(); ++i) {
                  cell_array[cell_id][i] = 10 + parallel::rank() + 2 - i;
                }
              }
            });

          REQUIRE(min(cell_array) == 10);
        }
      }
    }
  }

  SECTION("max")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d_v = named_mesh.mesh();
          auto mesh_1d   = mesh_1d_v->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          CellArray<size_t> cell_array{connectivity, 3};
          cell_array.fill(std::numeric_limits<size_t>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_1d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                for (size_t i = 0; i < cell_array.sizeOfArrays(); ++i) {
                  cell_array[cell_id][i] = parallel::rank() + 1 + i;
                }
              }
            });

          REQUIRE(max(cell_array) == parallel::size() + 2);
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          CellArray<size_t> cell_array{connectivity, 3};
          cell_array.fill(std::numeric_limits<size_t>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_2d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                for (size_t i = 0; i < cell_array.sizeOfArrays(); ++i) {
                  cell_array[cell_id][i] = parallel::rank() + i;
                }
              }
            });

          REQUIRE(max(cell_array) == parallel::size() + 1);
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          CellArray<size_t> cell_array{connectivity, 2};
          cell_array.fill(std::numeric_limits<size_t>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_3d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                for (size_t i = 0; i < cell_array.sizeOfArrays(); ++i) {
                  cell_array[cell_id][i] = parallel::rank() + 1 + i;
                }
              }
            });

          REQUIRE(max(cell_array) == parallel::size() + 1);
        }
      }
    }

    SECTION("max for negative double values")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          CellArray<double> cell_array{connectivity, 2};
          cell_array.fill(std::numeric_limits<double>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_3d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                for (size_t i = 0; i < cell_array.sizeOfArrays(); ++i) {
                  cell_array[cell_id][i] = -parallel::rank() - 10 + i;
                }
              }
            });

          REQUIRE(max(cell_array) == -parallel::size() - 10);
        }
      }
    }

    SECTION("max for negative integral values")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          CellArray<int> cell_array{connectivity, 2};
          cell_array.fill(std::numeric_limits<int>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_3d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                for (size_t i = 0; i < cell_array.sizeOfArrays(); ++i) {
                  cell_array[cell_id][i] = -2 * parallel::size() + parallel::rank() - 10 + i;
                }
              }
            });

          REQUIRE(max(cell_array) == -static_cast<int>(parallel::size() + 10));
        }
      }
    }
  }

  SECTION("sum")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d_v = named_mesh.mesh();
          auto mesh_1d   = mesh_1d_v->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          CellArray<size_t> cell_array{connectivity, 3};
          cell_array.fill(5);

          auto cell_is_owned = connectivity.cellIsOwned();

          const size_t global_number_of_cells = [&] {
            size_t number_of_cells = 0;
            for (CellId cell_id = 0; cell_id < cell_is_owned.numberOfItems(); ++cell_id) {
              number_of_cells += cell_is_owned[cell_id];
            }
            return parallel::allReduceSum(number_of_cells);
          }();

          REQUIRE(sum(cell_array) == 5 * cell_array.sizeOfArrays() * global_number_of_cells);
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name() + " for double data")
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          auto ll = MeshDataManager::instance().getMeshData(*mesh_2d).ll();

          FaceArray<double> face_array{connectivity, 3};
          parallel_for(
            connectivity.numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
              face_array[face_id][0] = ll[face_id];
              face_array[face_id][1] = (3 + ll[face_id]) * ll[face_id];
              face_array[face_id][2] = 2 * ll[face_id] - 3;
            });

          auto face_is_owned = connectivity.faceIsOwned();

          FaceValue<double> face_sum{connectivity};
          parallel_for(
            connectivity.numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
              face_sum[face_id] = face_array[face_id][0] + face_array[face_id][1] + face_array[face_id][2];
            });

          REQUIRE(sum(face_array) == sum(face_sum));
        }

        SECTION(named_mesh.name() + " for N^2 data")
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          using N2 = TinyVector<2, size_t>;
          FaceArray<N2> face_array{connectivity, 3};

          const N2 data(3, 1);
          face_array.fill(data);

          auto face_is_owned = connectivity.faceIsOwned();

          const size_t global_number_of_faces = [&] {
            size_t number_of_faces = 0;
            for (FaceId face_id = 0; face_id < face_is_owned.numberOfItems(); ++face_id) {
              number_of_faces += face_is_owned[face_id];
            }
            return parallel::allReduceSum(number_of_faces);
          }();

          REQUIRE(sum(face_array) == face_array.sizeOfArrays() * global_number_of_faces * data);
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name() + " for size_t data")
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          NodeArray<size_t> node_array{connectivity, 3};
          node_array.fill(3);

          auto node_is_owned = connectivity.nodeIsOwned();

          const size_t global_number_of_nodes = [&] {
            size_t number_of_nodes = 0;
            for (NodeId node_id = 0; node_id < node_is_owned.numberOfItems(); ++node_id) {
              number_of_nodes += node_is_owned[node_id];
            }
            return parallel::allReduceSum(number_of_nodes);
          }();

          REQUIRE(sum(node_array) == 3 * node_array.sizeOfArrays() * global_number_of_nodes);
        }

        SECTION(named_mesh.name() + " for N^3x2 data")
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          using N3x2 = TinyMatrix<3, 2, size_t>;

          NodeArray<N3x2> node_array{connectivity, 3};

          const N3x2 data(1, 3, 2, 4, 6, 5);
          node_array.fill(data);

          auto node_is_owned = connectivity.nodeIsOwned();

          const size_t global_number_of_nodes = [&] {
            size_t number_of_nodes = 0;
            for (NodeId node_id = 0; node_id < node_is_owned.numberOfItems(); ++node_id) {
              number_of_nodes += node_is_owned[node_id];
            }
            return parallel::allReduceSum(number_of_nodes);
          }();

          REQUIRE(sum(node_array) == node_array.sizeOfArrays() * global_number_of_nodes * data);
        }
      }
    }
  }
}
