#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/DualConnectivityManager.hpp>
#include <mesh/PrimalToDual1DConnectivityDataMapper.hpp>
#include <mesh/PrimalToMedianDualConnectivityDataMapper.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("PrimalToMedianDualConnectivityDataMapper", "[mesh]")
{
  SECTION("1D")
  {
    constexpr static size_t Dimension = 1;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    std::shared_ptr<const MeshType> mesh        = MeshDataBaseForTests::get().unordered1DMesh()->get<MeshType>();
    const ConnectivityType& primal_connectivity = mesh->connectivity();

    std::shared_ptr p_median_dual_connectivity =
      DualConnectivityManager::instance().getMedianDualConnectivity(primal_connectivity);

    std::shared_ptr p_dual_1d_connectivity =
      DualConnectivityManager::instance().getDual1DConnectivity(primal_connectivity);

    REQUIRE(p_median_dual_connectivity == p_dual_1d_connectivity);

    std::shared_ptr p_primal_to_median_dual_connectivity_mapper =
      DualConnectivityManager::instance().getPrimalToMedianDualConnectivityDataMapper(primal_connectivity);

    std::shared_ptr p_primal_to_dual_1d_connectivity_mapper =
      DualConnectivityManager::instance().getPrimalToDual1DConnectivityDataMapper(primal_connectivity);

    REQUIRE(p_primal_to_median_dual_connectivity_mapper == p_primal_to_dual_1d_connectivity_mapper);
  }

  SECTION("2D")
  {
    constexpr static size_t Dimension = 2;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    std::shared_ptr<const MeshType> mesh        = MeshDataBaseForTests::get().hybrid2DMesh()->get<MeshType>();
    const ConnectivityType& primal_connectivity = mesh->connectivity();

    std::shared_ptr p_median_dual_connectivity =
      DualConnectivityManager::instance().getMedianDualConnectivity(primal_connectivity);
    const ConnectivityType& dual_connectivity = *p_median_dual_connectivity;

    std::shared_ptr p_primal_to_dual_connectivity_mapper =
      DualConnectivityManager::instance().getPrimalToMedianDualConnectivityDataMapper(primal_connectivity);
    const auto& primal_to_dual_connectivity_mapper = *p_primal_to_dual_connectivity_mapper;

    SECTION("check data transfer between primal nodes to dual cells")
    {
      const auto& primal_node_to_face_matrix = primal_connectivity.nodeToFaceMatrix();

      NodeValue<size_t> primal_node_value{primal_connectivity};
      parallel_for(
        primal_node_value.numberOfItems(), PUGS_LAMBDA(const NodeId node_id) {
          primal_node_value[node_id] = 2 * primal_node_to_face_matrix[node_id].size() + node_id;
        });

      CellValue<size_t> dual_from_primal_node_value{dual_connectivity};

      primal_to_dual_connectivity_mapper.toDualCell(primal_node_value, dual_from_primal_node_value);

      {
        const auto& dual_cell_to_node_matrix = dual_connectivity.cellToNodeMatrix();
        bool correct_value                   = true;
        for (CellId cell_id = 0; cell_id < dual_connectivity.numberOfCells(); ++cell_id) {
          correct_value &= (dual_cell_to_node_matrix[cell_id].size() + cell_id == dual_from_primal_node_value[cell_id]);
        }
        REQUIRE(correct_value);
      }

      {
        bool is_same = true;
        NodeValue<size_t> primal_from_dual_cell_value{primal_connectivity};
        primal_to_dual_connectivity_mapper.fromDualCell(dual_from_primal_node_value, primal_from_dual_cell_value);
        for (NodeId node_id = 0; node_id < primal_connectivity.numberOfNodes(); ++node_id) {
          is_same &= (primal_from_dual_cell_value[node_id] == primal_node_value[node_id]);
        }
        REQUIRE(is_same);
      }

#ifndef NDEBUG
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualCell(NodeValue<size_t>{dual_connectivity},
                                                                        dual_from_primal_node_value),
                          "unexpected connectivity for primal NodeValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualCell(primal_node_value,
                                                                        CellValue<size_t>{primal_connectivity}),
                          "unexpected connectivity for dual CellValue");

      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualCell(dual_from_primal_node_value,
                                                                          NodeValue<size_t>{dual_connectivity}),
                          "unexpected connectivity for primal NodeValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualCell(CellValue<size_t>{primal_connectivity},
                                                                          primal_node_value),
                          "unexpected connectivity for dual CellValue");

#endif   // NDEBUG
    }

    SECTION("check data transfer between primal faces and cells to dual nodes")
    {
      const auto& primal_cell_to_node_matrix = primal_connectivity.cellToNodeMatrix();
      CellValue<size_t> primal_cell_value{primal_connectivity};
      parallel_for(
        primal_cell_value.numberOfItems(),
        PUGS_LAMBDA(const CellId cell_id) { primal_cell_value[cell_id] = primal_cell_to_node_matrix[cell_id].size(); });

      const auto& primal_face_to_cell_matrix = primal_connectivity.faceToCellMatrix();
      FaceValue<size_t> primal_face_value{primal_connectivity};
      parallel_for(
        primal_face_value.numberOfItems(), PUGS_LAMBDA(const FaceId face_id) {
          primal_face_value[face_id] = 2 + (primal_face_to_cell_matrix[face_id].size() == 1);
        });

      NodeValue<size_t> primal_node_value{primal_connectivity};
      primal_node_value.fill(2);

      NodeValue<size_t> dual_from_primal_value{dual_connectivity};
      primal_to_dual_connectivity_mapper.toDualNode(primal_node_value, primal_face_value, primal_cell_value,
                                                    dual_from_primal_value);

      {
        const auto& dual_node_to_face_matrix = dual_connectivity.nodeToFaceMatrix();
        auto dual_node_number                = dual_connectivity.nodeNumber();
        bool correct_dual_value              = true;
        for (NodeId node_id = 0; node_id < dual_connectivity.numberOfNodes(); ++node_id) {
          correct_dual_value &= (dual_from_primal_value[node_id] == dual_node_to_face_matrix[node_id].size());
        }
        REQUIRE(correct_dual_value);
      }

      {
        bool is_same = true;
        NodeValue<size_t> primal_from_dual_node_value{primal_connectivity};
        FaceValue<size_t> primal_from_dual_face_value{primal_connectivity};
        CellValue<size_t> primal_from_dual_cell_value{primal_connectivity};
        primal_to_dual_connectivity_mapper.fromDualNode(dual_from_primal_value,        //
                                                        primal_from_dual_node_value,   //
                                                        primal_from_dual_face_value,   //
                                                        primal_from_dual_cell_value);

        for (CellId cell_id = 0; cell_id < primal_connectivity.numberOfCells(); ++cell_id) {
          is_same &= (primal_cell_value[cell_id] == primal_from_dual_cell_value[cell_id]);
        }
        for (FaceId face_id = 0; face_id < primal_connectivity.numberOfFaces(); ++face_id) {
          is_same &= (primal_face_value[face_id] == primal_from_dual_face_value[face_id]);
        }
        auto primal_face_to_node_matrix = primal_connectivity.faceToNodeMatrix();
        for (FaceId face_id = 0; face_id < primal_connectivity.numberOfFaces(); ++face_id) {
          if (primal_face_to_cell_matrix[face_id].size() == 1) {
            for (size_t i_node = 0; i_node < primal_face_to_node_matrix[face_id].size(); ++i_node) {
              NodeId node_id = primal_face_to_node_matrix[face_id][i_node];
              is_same &= (primal_node_value[node_id] == primal_from_dual_node_value[node_id]);
            }
          }
        }

        REQUIRE(is_same);
      }

      {
        const auto& primal_edge_to_cell_matrix = primal_connectivity.edgeToCellMatrix();
        EdgeValue<size_t> primal_edge_value{primal_connectivity};
        parallel_for(
          primal_edge_value.numberOfItems(), PUGS_LAMBDA(const EdgeId edge_id) {
            primal_edge_value[edge_id] = 2 + (primal_edge_to_cell_matrix[edge_id].size() == 1);
          });

        {
          const auto& dual_node_to_edge_matrix = dual_connectivity.nodeToEdgeMatrix();
          auto dual_node_number                = dual_connectivity.nodeNumber();
          bool correct_dual_value              = true;
          for (NodeId node_id = 0; node_id < dual_connectivity.numberOfNodes(); ++node_id) {
            correct_dual_value &= (dual_from_primal_value[node_id] == dual_node_to_edge_matrix[node_id].size());
          }
          REQUIRE(correct_dual_value);
        }

        {
          bool is_same = true;
          NodeValue<size_t> primal_from_dual_node_value{primal_connectivity};
          EdgeValue<size_t> primal_from_dual_edge_value{primal_connectivity};
          CellValue<size_t> primal_from_dual_cell_value{primal_connectivity};
          primal_to_dual_connectivity_mapper.fromDualNode(dual_from_primal_value,        //
                                                          primal_from_dual_node_value,   //
                                                          primal_from_dual_edge_value,   //
                                                          primal_from_dual_cell_value);

          for (CellId cell_id = 0; cell_id < primal_connectivity.numberOfCells(); ++cell_id) {
            is_same &= (primal_cell_value[cell_id] == primal_from_dual_cell_value[cell_id]);
          }
          for (EdgeId edge_id = 0; edge_id < primal_connectivity.numberOfFaces(); ++edge_id) {
            is_same &= (primal_edge_value[edge_id] == primal_from_dual_edge_value[edge_id]);
          }
          auto primal_face_to_node_matrix = primal_connectivity.faceToNodeMatrix();
          for (FaceId face_id = 0; face_id < primal_connectivity.numberOfFaces(); ++face_id) {
            if (primal_face_to_cell_matrix[face_id].size() == 1) {
              for (size_t i_node = 0; i_node < primal_face_to_node_matrix[face_id].size(); ++i_node) {
                NodeId node_id = primal_face_to_node_matrix[face_id][i_node];
                is_same &= (primal_node_value[node_id] == primal_from_dual_node_value[node_id]);
              }
            }
          }

          REQUIRE(is_same);
        }
      }

#ifndef NDEBUG
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualNode(NodeValue<size_t>{dual_connectivity},
                                                                        primal_face_value,   //
                                                                        primal_cell_value, dual_from_primal_value),
                          "unexpected connectivity for primal NodeValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualNode(primal_node_value,
                                                                        FaceValue<size_t>{dual_connectivity},
                                                                        primal_cell_value, dual_from_primal_value),
                          "unexpected connectivity for primal FaceValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualNode(primal_node_value,   //
                                                                        primal_face_value,   //
                                                                        CellValue<size_t>{dual_connectivity},
                                                                        dual_from_primal_value),
                          "unexpected connectivity for primal CellValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualNode(primal_node_value,   //
                                                                        primal_face_value,   //
                                                                        primal_cell_value,
                                                                        NodeValue<size_t>{primal_connectivity}),
                          "unexpected connectivity for dual NodeValue");

      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualNode(dual_from_primal_value,
                                                                          NodeValue<size_t>{dual_connectivity},
                                                                          primal_face_value,   //
                                                                          primal_cell_value),
                          "unexpected connectivity for primal NodeValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualNode(dual_from_primal_value, primal_node_value,
                                                                          FaceValue<size_t>{dual_connectivity},
                                                                          primal_cell_value),
                          "unexpected connectivity for primal FaceValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualNode(dual_from_primal_value,
                                                                          primal_node_value,   //
                                                                          primal_face_value,   //
                                                                          CellValue<size_t>{dual_connectivity}),
                          "unexpected connectivity for primal CellValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualNode(NodeValue<size_t>{primal_connectivity},
                                                                          primal_node_value,   //
                                                                          primal_face_value,   //
                                                                          primal_cell_value),
                          "unexpected connectivity for dual NodeValue");

#endif   // NDEBUG
    }
  }
}
