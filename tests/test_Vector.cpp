#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/PugsAssert.hpp>

#include <algebra/Vector.hpp>

// Instantiate to ensure full coverage is performed
template class Vector<int>;

// clazy:excludeall=non-pod-global-static

TEST_CASE("Vector", "[algebra]")
{
  SECTION("size")
  {
    Vector<int> x{5};
    REQUIRE(x.size() == 5);
  }

  SECTION("write access")
  {
    Vector<int> x{5};
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;

    REQUIRE(x[0] == 0);
    REQUIRE(x[1] == 1);
    REQUIRE(x[2] == 2);
    REQUIRE(x[3] == 3);
    REQUIRE(x[4] == 4);
  }

  SECTION("fill")
  {
    Vector<int> x{5};
    x.fill(2);

    REQUIRE(x[0] == 2);
    REQUIRE(x[1] == 2);
    REQUIRE(x[2] == 2);
    REQUIRE(x[3] == 2);
    REQUIRE(x[4] == 2);
  }

  SECTION("copy constructor (shallow)")
  {
    Vector<int> x{5};
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;

    const Vector<int> y = x;
    REQUIRE(y[0] == 0);
    REQUIRE(y[1] == 1);
    REQUIRE(y[2] == 2);
    REQUIRE(y[3] == 3);
    REQUIRE(y[4] == 4);
  }

  SECTION("copy constructor (move)")
  {
    Vector<int> x{5};
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;

    const Vector<int> y = std::move(x);
    REQUIRE(y[0] == 0);
    REQUIRE(y[1] == 1);
    REQUIRE(y[2] == 2);
    REQUIRE(y[3] == 3);
    REQUIRE(y[4] == 4);
  }

  SECTION("copy (shallow)")
  {
    Vector<int> x{5};
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;

    Vector<int> y{5};
    y = x;
    REQUIRE(y[0] == 0);
    REQUIRE(y[1] == 1);
    REQUIRE(y[2] == 2);
    REQUIRE(y[3] == 3);
    REQUIRE(y[4] == 4);

    x[0] = 14;
    x[1] = 13;
    x[2] = 12;
    x[3] = 11;
    x[4] = 10;

    REQUIRE(x[0] == 14);
    REQUIRE(x[1] == 13);
    REQUIRE(x[2] == 12);
    REQUIRE(x[3] == 11);
    REQUIRE(x[4] == 10);

    REQUIRE(y[0] == 14);
    REQUIRE(y[1] == 13);
    REQUIRE(y[2] == 12);
    REQUIRE(y[3] == 11);
    REQUIRE(y[4] == 10);
  }

  SECTION("copy (deep)")
  {
    Vector<int> x{5};
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;

    Vector<int> y{5};
    y = copy(x);

    x[0] = 14;
    x[1] = 13;
    x[2] = 12;
    x[3] = 11;
    x[4] = 10;

    REQUIRE(y[0] == 0);
    REQUIRE(y[1] == 1);
    REQUIRE(y[2] == 2);
    REQUIRE(y[3] == 3);
    REQUIRE(y[4] == 4);

    REQUIRE(x[0] == 14);
    REQUIRE(x[1] == 13);
    REQUIRE(x[2] == 12);
    REQUIRE(x[3] == 11);
    REQUIRE(x[4] == 10);
  }

  SECTION("copy to const (shallow)")
  {
    Vector<int> x{5};
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;

    Vector<const int> y;
    y = x;
    REQUIRE(y[0] == 0);
    REQUIRE(y[1] == 1);
    REQUIRE(y[2] == 2);
    REQUIRE(y[3] == 3);
    REQUIRE(y[4] == 4);

    Vector<int> z{5};
    z[0] = 14;
    z[1] = 13;
    z[2] = 12;
    z[3] = 11;
    z[4] = 10;

    y = z;
    REQUIRE(z[0] == 14);
    REQUIRE(z[1] == 13);
    REQUIRE(z[2] == 12);
    REQUIRE(z[3] == 11);
    REQUIRE(z[4] == 10);

    REQUIRE(y[0] == 14);
    REQUIRE(y[1] == 13);
    REQUIRE(y[2] == 12);
    REQUIRE(y[3] == 11);
    REQUIRE(y[4] == 10);
  }

  SECTION("self scalar multiplication")
  {
    Vector<int> x{5};
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;

    x *= 2;

    REQUIRE(x[0] == 0);
    REQUIRE(x[1] == 2);
    REQUIRE(x[2] == 4);
    REQUIRE(x[3] == 6);
    REQUIRE(x[4] == 8);
  }

  SECTION("left scalar multiplication")
  {
    Vector<int> x{5};
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;

    const Vector<int> y = 2 * x;

    REQUIRE(y[0] == 0);
    REQUIRE(y[1] == 2);
    REQUIRE(y[2] == 4);
    REQUIRE(y[3] == 6);
    REQUIRE(y[4] == 8);
  }

  SECTION("dot product")
  {
    Vector<int> x{5};
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;

    Vector<int> y{5};
    y[0] = 7;
    y[1] = 3;
    y[2] = 4;
    y[3] = 2;
    y[4] = 8;

    const int s = dot(x, y);
    REQUIRE(s == 49);
  }

  SECTION("self scalar division")
  {
    Vector<int> x{5};
    x[0] = 2;
    x[1] = 3;
    x[2] = 5;
    x[3] = 7;
    x[4] = 8;

    x /= 2;

    REQUIRE(x[0] == 1);
    REQUIRE(x[1] == 1);
    REQUIRE(x[2] == 2);
    REQUIRE(x[3] == 3);
    REQUIRE(x[4] == 4);
  }

  SECTION("self minus")
  {
    Vector<int> x{5};
    x[0] = 2;
    x[1] = 3;
    x[2] = 5;
    x[3] = 7;
    x[4] = 8;

    Vector<int> y{5};
    y[0] = 3;
    y[1] = 8;
    y[2] = 6;
    y[3] = 2;
    y[4] = 4;

    x -= y;

    REQUIRE(x[0] == -1);
    REQUIRE(x[1] == -5);
    REQUIRE(x[2] == -1);
    REQUIRE(x[3] == 5);
    REQUIRE(x[4] == 4);
  }

  SECTION("self sum")
  {
    Vector<int> x{5};
    x[0] = 2;
    x[1] = 3;
    x[2] = 5;
    x[3] = 7;
    x[4] = 8;

    Vector<int> y{5};
    y[0] = 3;
    y[1] = 8;
    y[2] = 6;
    y[3] = 2;
    y[4] = 4;

    x += y;

    REQUIRE(x[0] == 5);
    REQUIRE(x[1] == 11);
    REQUIRE(x[2] == 11);
    REQUIRE(x[3] == 9);
    REQUIRE(x[4] == 12);
  }

  SECTION("sum")
  {
    Vector<int> x{5};
    x[0] = 2;
    x[1] = 3;
    x[2] = 5;
    x[3] = 7;
    x[4] = 8;

    Vector<int> y{5};
    y[0] = 3;
    y[1] = 8;
    y[2] = 6;
    y[3] = 2;
    y[4] = 4;

    Vector z = x + y;

    REQUIRE(z[0] == 5);
    REQUIRE(z[1] == 11);
    REQUIRE(z[2] == 11);
    REQUIRE(z[3] == 9);
    REQUIRE(z[4] == 12);
  }

  SECTION("difference")
  {
    Vector<int> x{5};
    x[0] = 2;
    x[1] = 3;
    x[2] = 5;
    x[3] = 7;
    x[4] = 8;

    Vector<int> y{5};
    y[0] = 3;
    y[1] = 8;
    y[2] = 6;
    y[3] = 2;
    y[4] = 4;

    Vector z = x - y;

    REQUIRE(z[0] == -1);
    REQUIRE(z[1] == -5);
    REQUIRE(z[2] == -1);
    REQUIRE(z[3] == 5);
    REQUIRE(z[4] == 4);
  }

  SECTION("unary minus")
  {
    Vector<int> x{5};
    x[0] = 2;
    x[1] = 3;
    x[2] = 5;
    x[3] = 7;
    x[4] = 8;

    Vector<const int> y = x;

    Vector<int> z = -x;

    REQUIRE(z[0] == -2);
    REQUIRE(z[1] == -3);
    REQUIRE(z[2] == -5);
    REQUIRE(z[3] == -7);
    REQUIRE(z[4] == -8);

    z = -y;
    REQUIRE(z[0] == -2);
    REQUIRE(z[1] == -3);
    REQUIRE(z[2] == -5);
    REQUIRE(z[3] == -7);
    REQUIRE(z[4] == -8);
  }

  SECTION("min/max")
  {
    Vector<int> u(5);
    u[0] = 1;
    u[1] = 7;
    u[2] = 6;
    u[3] = 2;
    u[4] = 9;

    REQUIRE(min(u) == 1);
    REQUIRE(max(u) == 9);
    REQUIRE(min(-u) == -9);
    REQUIRE(max(-u) == -1);

    Vector<int> v(5);
    v[0] = 1;
    v[1] = 11;
    v[2] = 6;
    v[3] = -2;
    v[4] = 9;

    REQUIRE(min(v) == -2);
    REQUIRE(max(v) == 11);
    REQUIRE(min(-v) == -11);
    REQUIRE(max(-v) == 2);
  }

  SECTION("output")
  {
    Vector<int> x{5};
    x[0] = 3;
    x[1] = 7;
    x[2] = 2;
    x[3] = 1;
    x[4] = -4;

    std::ostringstream vector_ost;
    vector_ost << x;
    std::ostringstream ref_ost;
    ref_ost << 0 << ':' << x[0];
    for (size_t i = 1; i < x.size(); ++i) {
      ref_ost << ' ' << i << ':' << x[i];
    }
    REQUIRE(vector_ost.str() == ref_ost.str());
  }

  SECTION("Vector view from Array")
  {
    Array<double> values{5};
    for (size_t i = 0; i < values.size(); ++i) {
      values[i] = 2 * i + 1;
    }

    Vector vector{values};

    SECTION("Constructor")
    {
      REQUIRE(vector[0] == 1);
      REQUIRE(vector[1] == 3);
      REQUIRE(vector[2] == 5);
      REQUIRE(vector[3] == 7);
      REQUIRE(vector[4] == 9);
    }

    SECTION("Affectation (implicit deep copy)")
    {
      Vector<double> x{vector.size()};
      for (size_t i = 0; i < x.size(); ++i) {
        x[i] = i;
      }

      vector = x;
      REQUIRE(values[0] == 0);
      REQUIRE(values[1] == 1);
      REQUIRE(values[2] == 2);
      REQUIRE(values[3] == 3);
      REQUIRE(values[4] == 4);

      for (size_t i = 0; i < x.size(); ++i) {
        x[i] = x.size() - i;
      }

      REQUIRE(values[0] == 0);
      REQUIRE(values[1] == 1);
      REQUIRE(values[2] == 2);
      REQUIRE(values[3] == 3);
      REQUIRE(values[4] == 4);

      vector = std::move(x);
      x.fill(2);

      REQUIRE(values[0] == 5);
      REQUIRE(values[1] == 4);
      REQUIRE(values[2] == 3);
      REQUIRE(values[3] == 2);
      REQUIRE(values[4] == 1);
    }
  }

#ifndef NDEBUG

  SECTION("output with signaling NaN")
  {
    Vector<double> x{5};
    x[0] = 3;
    x[2] = 2;

    std::ostringstream vector_ost;
    vector_ost << x;
    std::ostringstream ref_ost;
    ref_ost << 0 << ':' << 3 << ' ';
    ref_ost << 1 << ":nan ";
    ref_ost << 2 << ':' << 2 << ' ';
    ref_ost << 3 << ":nan ";
    ref_ost << 4 << ":nan";
    REQUIRE(vector_ost.str() == ref_ost.str());
  }

  SECTION("invalid dot product")
  {
    Vector<int> x{5};
    Vector<int> y{4};

    REQUIRE_THROWS_WITH(dot(x, y), "cannot compute dot product: incompatible vector sizes");
  }

  SECTION("invalid substract")
  {
    Vector<int> x{5};
    Vector<int> y{4};

    REQUIRE_THROWS_WITH(x -= y, "cannot substract vector: incompatible sizes");
  }

  SECTION("invalid add")
  {
    Vector<int> x{5};
    Vector<int> y{4};

    REQUIRE_THROWS_WITH(x += y, "cannot add vector: incompatible sizes");
  }

  SECTION("invalid difference")
  {
    Vector<int> x{5};
    Vector<int> y{4};

    REQUIRE_THROWS_WITH(x - y, "cannot compute vector difference: incompatible sizes");
  }

  SECTION("invalid sum")
  {
    Vector<int> x{5};
    Vector<int> y{4};

    REQUIRE_THROWS_WITH(x + y, "cannot compute vector sum: incompatible sizes");
  }

  SECTION("Affectation to Vector of const values built from Array")
  {
    Vector<int> x{5};
    x[0] = 0;
    x[1] = 1;
    x[2] = 2;
    x[3] = 3;
    x[4] = 4;

    Array<int> y_values{5};
    y_values[0] = 0;
    y_values[1] = 1;
    y_values[2] = 2;
    y_values[3] = 3;
    y_values[4] = 4;
    Vector<const int> y{y_values};

    REQUIRE_THROWS_WITH(y = x, "cannot assign value to a Vector of const that required deep copies");

    Vector<const int> z = x;

    REQUIRE_THROWS_WITH(y = z, "cannot assign value to a Vector of const that required deep copies");
  }
#endif   // NDEBUG
}
