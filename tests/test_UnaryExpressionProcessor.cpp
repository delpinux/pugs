#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <FixturesForBuiltinT.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/node_processor/UnaryExpressionProcessor.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/AffectationProcessorBuilder.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <language/utils/TypeDescriptor.hpp>
#include <language/utils/UnaryOperatorProcessorBuilder.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_UNARY_EXPRESSION_RESULT(data, variable_name, expected_value)    \
  {                                                                           \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                \
    auto ast = ASTBuilder::build(input);                                      \
                                                                              \
    ASTExecutionStack::create();                                              \
                                                                              \
    ASTModulesImporter{*ast};                                                 \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                   \
                                                                              \
    ASTSymbolTableBuilder{*ast};                                              \
    ASTNodeDataTypeBuilder{*ast};                                             \
                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                      \
                                                                              \
    ASTNodeExpressionBuilder{*ast};                                           \
    ExecutionPolicy exec_policy;                                              \
    ast->execute(exec_policy);                                                \
                                                                              \
    auto symbol_table = ast->m_symbol_table;                                  \
                                                                              \
    using namespace TAO_PEGTL_NAMESPACE;                                      \
    position use_position{10000, 1000, 10, "fixture"};                        \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);   \
                                                                              \
    auto attributes = symbol->attributes();                                   \
    auto value      = std::get<decltype(expected_value)>(attributes.value()); \
                                                                              \
    ASTExecutionStack::destroy();                                             \
                                                                              \
    REQUIRE(value == expected_value);                                         \
    ast->m_symbol_table->clearValues();                                       \
  }

#define CHECK_UNARY_EXPRESSION_THROWS_WITH(data, error_message)       \
  {                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};        \
    auto ast = ASTBuilder::build(input);                              \
                                                                      \
    ASTExecutionStack::create();                                      \
                                                                      \
    ASTSymbolTableBuilder{*ast};                                      \
                                                                      \
    REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error_message); \
                                                                      \
    ASTExecutionStack::destroy();                                     \
    ast->m_symbol_table->clearValues();                               \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("UnaryExpressionProcessor", "[language]")
{
  SECTION("unary minus")
  {
    CHECK_UNARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let z:Z, z = -n;)", "z", -2l);
    CHECK_UNARY_EXPRESSION_RESULT(R"(let p:Z, p = 2; let q:Z, q = -p;)", "q", -2l);
    CHECK_UNARY_EXPRESSION_RESULT(R"(let r:R, r = 2; r = -r;)", "r", -2.);
  }

  SECTION("unary minus [builtin]")
  {
    std::string_view data = R"(let r:builtin_t, r = -bt;)";

    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
    auto ast = ASTBuilder::build(input);

    ASTExecutionStack::create();

    ASTModulesImporter{*ast};

    BasicAffectationRegisterFor<EmbeddedData>{ASTNodeDataType::build<ASTNodeDataType::type_id_t>("builtin_t")};

    OperatorRepository& repository = OperatorRepository::instance();

    repository.addUnaryOperator<language::unary_minus>(
      std::make_shared<UnaryOperatorProcessorBuilder<language::unary_minus, std::shared_ptr<const double>,
                                                     std::shared_ptr<const double>>>());

    SymbolTable& symbol_table = *ast->m_symbol_table;
    auto [i_symbol, success]  = symbol_table.add(builtin_data_type.nameOfTypeId(), ast->begin());
    if (not success) {
      throw UnexpectedError("cannot add '" + builtin_data_type.nameOfTypeId() + "' type for testing");
    }

    i_symbol->attributes().setDataType(ASTNodeDataType::build<ASTNodeDataType::type_name_id_t>());
    i_symbol->attributes().setIsInitialized();
    i_symbol->attributes().value() = symbol_table.typeEmbedderTable().size();
    symbol_table.typeEmbedderTable().add(std::make_shared<TypeDescriptor>(builtin_data_type.nameOfTypeId()));

    auto [i_symbol_bt, success_bt] = symbol_table.add("bt", ast->begin());
    if (not success_bt) {
      throw UnexpectedError("cannot add 'bt' of type builtin_t for testing");
    }
    i_symbol_bt->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);
    i_symbol_bt->attributes().setIsInitialized();
    i_symbol_bt->attributes().value() =
      EmbeddedData(std::make_shared<DataHandler<const double>>(std::make_shared<double>(3.2)));

    ASTNodeTypeCleaner<language::import_instruction>{*ast};

    ASTSymbolTableBuilder{*ast};
    ASTNodeDataTypeBuilder{*ast};

    ASTNodeDeclarationToAffectationConverter{*ast};
    ASTNodeTypeCleaner<language::var_declaration>{*ast};

    ASTNodeExpressionBuilder{*ast};
    ExecutionPolicy exec_policy;
    ast->execute(exec_policy);

    using namespace TAO_PEGTL_NAMESPACE;
    position use_position{10000, 1000, 10, "fixture"};
    auto [symbol, found] = symbol_table.find("r", use_position);

    auto attributes     = symbol->attributes();
    auto embedded_value = std::get<EmbeddedData>(attributes.value());

    double value = *dynamic_cast<const DataHandler<const double>&>(embedded_value.get()).data_ptr();

    ASTExecutionStack::destroy();

    REQUIRE(value == double{-3.2});
    ast->m_symbol_table->clearValues();
  }

  SECTION("unary not")
  {
    CHECK_UNARY_EXPRESSION_RESULT(R"(let b:B, b = false; b = not b;)", "b", true);
    CHECK_UNARY_EXPRESSION_RESULT(R"(let b:B, b = true; b = not b;)", "b", false);
  }

  SECTION("errors")
  {
    SECTION("undefined not operator")
    {
      auto error_message = [](std::string type_name) {
        return std::string{R"(undefined unary operator
note: unexpected operand type )"} +
               type_name;
      };

      CHECK_UNARY_EXPRESSION_THROWS_WITH(R"(let n:N, n = 0; not n;)", error_message("N"));
      CHECK_UNARY_EXPRESSION_THROWS_WITH(R"(not 1;)", error_message("Z"));
      CHECK_UNARY_EXPRESSION_THROWS_WITH(R"(not 1.3;)", error_message("R"));
      CHECK_UNARY_EXPRESSION_THROWS_WITH(R"(not "foo";)", error_message("string"));
    }

    SECTION("undefined unary minus operator")
    {
      auto error_message = [](std::string type_name) {
        return std::string{R"(undefined unary operator
note: unexpected operand type )"} +
               type_name;
      };

      CHECK_UNARY_EXPRESSION_THROWS_WITH(R"(-"foo";)", error_message("string"));
    }
  }
}
