#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <scheme/DiscreteFunctionDescriptorP0Vector.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("DiscreteFunctionDescriptorP0Vector", "[scheme]")
{
  SECTION("type")
  {
    DiscreteFunctionDescriptorP0Vector descriptor;
    REQUIRE(descriptor.type() == DiscreteFunctionType::P0Vector);

    {
      auto copy = [](const DiscreteFunctionDescriptorP0Vector& d) -> DiscreteFunctionDescriptorP0Vector { return d; };

      DiscreteFunctionDescriptorP0Vector descriptor_copy{copy(descriptor)};
      REQUIRE(descriptor_copy.type() == DiscreteFunctionType::P0Vector);
    }

    DiscreteFunctionDescriptorP0Vector temp;
    DiscreteFunctionDescriptorP0Vector descriptor_move{std::move(temp)};
    REQUIRE(descriptor_move.type() == DiscreteFunctionType::P0Vector);
  }
}
