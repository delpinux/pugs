#ifndef TEST_BINARY_EXPRESSION_PROCESSOR_UTILS_HPP
#define TEST_BINARY_EXPRESSION_PROCESSOR_UTILS_HPP

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/CheckpointResumeRepository.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_BINARY_EXPRESSION_RESULT(data, variable_name, expected_value)   \
  {                                                                           \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                \
    auto ast = ASTBuilder::build(input);                                      \
                                                                              \
    ASTModulesImporter{*ast};                                                 \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                   \
                                                                              \
    CheckpointResumeRepository::create();                                     \
    ASTSymbolTableBuilder{*ast};                                              \
    ASTNodeDataTypeBuilder{*ast};                                             \
                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                      \
                                                                              \
    ASTNodeExpressionBuilder{*ast};                                           \
    ExecutionPolicy exec_policy;                                              \
    ASTExecutionStack::create();                                              \
    ast->execute(exec_policy);                                                \
    ASTExecutionStack::destroy();                                             \
                                                                              \
    auto symbol_table = ast->m_symbol_table;                                  \
                                                                              \
    using namespace TAO_PEGTL_NAMESPACE;                                      \
    position use_position{10000, 1000, 1, "fixture"};                         \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);   \
                                                                              \
    auto attributes = symbol->attributes();                                   \
    auto value      = std::get<decltype(expected_value)>(attributes.value()); \
    CheckpointResumeRepository::destroy();                                    \
                                                                              \
    REQUIRE(value == expected_value);                                         \
  }

#define CHECK_BINARY_EXPRESSION_THROWS_WITH(data, error_message)      \
  {                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};        \
    auto ast = ASTBuilder::build(input);                              \
                                                                      \
    ASTModulesImporter{*ast};                                         \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};           \
                                                                      \
    ASTSymbolTableBuilder{*ast};                                      \
                                                                      \
    REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error_message); \
  }

#endif   // TEST_BINARY_EXPRESSION_PROCESSOR_UTILS_HPP
