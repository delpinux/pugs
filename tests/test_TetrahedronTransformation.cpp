#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/TetrahedronTransformation.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("TetrahedronTransformation", "[geometry]")
{
  using R3 = TinyVector<3>;

  const R3 a{1, 2, 1};
  const R3 b{3, 1, 3};
  const R3 c{2, 5, 2};
  const R3 d{2, 3, 4};

  const TetrahedronTransformation t(a, b, c, d);

  REQUIRE(t(R3{0, 0, 0})[0] == Catch::Approx(1));
  REQUIRE(t(R3{0, 0, 0})[1] == Catch::Approx(2));
  REQUIRE(t(R3{0, 0, 0})[2] == Catch::Approx(1));

  REQUIRE(t(R3{1, 0, 0})[0] == Catch::Approx(3));
  REQUIRE(t(R3{1, 0, 0})[1] == Catch::Approx(1));
  REQUIRE(t(R3{1, 0, 0})[2] == Catch::Approx(3));

  REQUIRE(t(R3{0, 1, 0})[0] == Catch::Approx(2));
  REQUIRE(t(R3{0, 1, 0})[1] == Catch::Approx(5));
  REQUIRE(t(R3{0, 1, 0})[2] == Catch::Approx(2));

  REQUIRE(t(R3{0, 0, 1})[0] == Catch::Approx(2));
  REQUIRE(t(R3{0, 0, 1})[1] == Catch::Approx(3));
  REQUIRE(t(R3{0, 0, 1})[2] == Catch::Approx(4));

  REQUIRE(t(R3{0.25, 0.25, 0.25})[0] == Catch::Approx(2));
  REQUIRE(t(R3{0.25, 0.25, 0.25})[1] == Catch::Approx(11. / 4));
  REQUIRE(t(R3{0.25, 0.25, 0.25})[2] == Catch::Approx(2.5));

  REQUIRE(t.jacobianDeterminant() == Catch::Approx(14));

  SECTION("Polynomial integral")
  {
    auto p = [](const R3& X) {
      const double x = X[0];
      const double y = X[1];
      const double z = X[2];
      return 2 * x * x + 3 * x * y + y * y + 3 * y - z * z + 2 * x * z + 2 * z;
    };

    QuadratureFormula<3> qf = QuadratureManager::instance().getTetrahedronFormula(GaussQuadratureDescriptor(2));

    double sum = 0;
    for (size_t i = 0; i < qf.numberOfPoints(); ++i) {
      sum += qf.weight(i) * t.jacobianDeterminant() * p(t(qf.point(i)));
    }

    REQUIRE(sum == Catch::Approx(231. / 2));
  }
}
