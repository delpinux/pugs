#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTExecutionStack", "[language]")
{
  REQUIRE_NOTHROW(ASTExecutionStack::create());
  REQUIRE_THROWS_WITH(ASTExecutionStack::create(), "unexpected error: ASTExecutionStack was already created!");

#ifndef NDEBUG
  REQUIRE_THROWS_WITH(ASTExecutionStack::getInstance().sourceLocation(), "stack.size() > 0");
#endif   // NDEBUG

  REQUIRE_NOTHROW(ASTExecutionStack::destroy());

  std::string data = R"(
import math;

for (let i:N, i=0; i<5; ++i) {
  cout << 2 * i << "\n";
  i += 1;
}
)";

  auto input = std::make_shared<TAO_PEGTL_NAMESPACE::string_input<>>(data, "test.pgs");

  ASTExecutionStack::create(input, data);

  REQUIRE_THROWS_WITH(ASTExecutionStack::create(input, data),
                      "unexpected error: ASTExecutionStack was already created!");

  auto ast = ASTBuilder::build(*input);

  ASTModulesImporter{*ast};
  ASTNodeTypeCleaner<language::import_instruction>{*ast};

  ASTSymbolTableBuilder{*ast};
  ASTNodeDataTypeBuilder{*ast};

  ASTNodeDeclarationToAffectationConverter{*ast};
  ASTNodeTypeCleaner<language::var_declaration>{*ast};

  ASTNodeExpressionBuilder{*ast};

  REQUIRE(ASTExecutionStack::getInstance().errorMessageAt("error_msg") == "error_msg");

  ASTExecutionStack::getInstance().push(ast->children[0].get());
  ASTExecutionStack::getInstance().push(ast->children[0]->children[3]->children[1].get());

  REQUIRE(ASTExecutionStack::getInstance().errorMessageAt("error_msg") == R"(test.pgs:6:5: error_msg
  i += 1;
    ^
)");

  auto source_location = ASTExecutionStack::getInstance().sourceLocation();
  REQUIRE(source_location.filename() == "test.pgs");
  REQUIRE(source_location.line() == 6);
  REQUIRE(source_location.column() == 5);
  REQUIRE(source_location.function() == "");

  ASTExecutionStack::getInstance().pop();
  ASTExecutionStack::getInstance().pop();

  ast->m_symbol_table->clearValues();

  REQUIRE_NOTHROW(ASTExecutionStack::destroy());
  REQUIRE_THROWS_WITH(ASTExecutionStack::destroy(), "unexpected error: ASTExecutionStack was not created!");
}
