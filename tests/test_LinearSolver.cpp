#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/pugs_config.hpp>

#include <algebra/CRSMatrixDescriptor.hpp>
#include <algebra/LinearSolver.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("LinearSolver", "[algebra]")
{
  SECTION("check has library")
  {
    LinearSolver linear_solver;

    REQUIRE(linear_solver.hasLibrary(LSLibrary::builtin) == true);

#ifdef PUGS_HAS_PETSC
    REQUIRE(linear_solver.hasLibrary(LSLibrary::petsc) == true);
#else    // PUGS_HAS_PETSC
    REQUIRE(linear_solver.hasLibrary(LSLibrary::petsc) == false);
#endif   // PUGS_HAS_PETSC

#ifdef PUGS_HAS_EIGEN3
    REQUIRE(linear_solver.hasLibrary(LSLibrary::eigen3) == true);
#else    // PUGS_HAS_PETSC
    REQUIRE(linear_solver.hasLibrary(LSLibrary::eigen3) == false);
#endif   // PUGS_HAS_PETSC
  }

  SECTION("check linear solver building")
  {
    LinearSolverOptions options;

    SECTION("builtin")
    {
      SECTION("builtin methods")
      {
        options.library() = LSLibrary::builtin;
        options.precond() = LSPrecond::none;

        options.method() = LSMethod::cg;
        REQUIRE_NOTHROW(LinearSolver{options});

        options.method() = LSMethod::bicgstab;
        REQUIRE_NOTHROW(LinearSolver{options});

        options.method() = LSMethod::bicgstab2;
        REQUIRE_THROWS_WITH(LinearSolver{options}, "error: BICGStab2 is not a builtin linear solver!");

        options.method() = LSMethod::lu;
        REQUIRE_THROWS_WITH(LinearSolver{options}, "error: LU is not a builtin linear solver!");

        options.method() = LSMethod::cholesky;
        REQUIRE_THROWS_WITH(LinearSolver{options}, "error: Cholesky is not a builtin linear solver!");

        options.method() = LSMethod::gmres;
        REQUIRE_THROWS_WITH(LinearSolver{options}, "error: GMRES is not a builtin linear solver!");
      }

      SECTION("builtin precond")
      {
        options.library() = LSLibrary::builtin;
        options.method()  = LSMethod::cg;

        options.precond() = LSPrecond::none;
        REQUIRE_NOTHROW(LinearSolver{options});

        options.precond() = LSPrecond::diagonal;
        REQUIRE_THROWS_WITH(LinearSolver{options}, "error: diagonal is not a builtin preconditioner!");

        options.precond() = LSPrecond::incomplete_LU;
        REQUIRE_THROWS_WITH(LinearSolver{options}, "error: ILU is not a builtin preconditioner!");

        options.precond() = LSPrecond::incomplete_cholesky;
        REQUIRE_THROWS_WITH(LinearSolver{options}, "error: ICholesky is not a builtin preconditioner!");

        options.precond() = LSPrecond::amg;
        REQUIRE_THROWS_WITH(LinearSolver{options}, "error: AMG is not a builtin preconditioner!");
      }
    }

    SECTION("PETSc")
    {
      LinearSolverOptions always_valid;
      always_valid.library() = LSLibrary::builtin;
      always_valid.method()  = LSMethod::cg;
      always_valid.precond() = LSPrecond::none;

      LinearSolver linear_solver{always_valid};

      SECTION("PETSc methods")
      {
        options.library() = LSLibrary::petsc;
        options.precond() = LSPrecond::none;

        options.method() = LSMethod::cg;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.method() = LSMethod::bicgstab;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.method() = LSMethod::bicgstab2;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.method() = LSMethod::lu;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.method() = LSMethod::cholesky;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.method() = LSMethod::gmres;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));
      }

      SECTION("PETSc precond")
      {
        options.library() = LSLibrary::petsc;
        options.method()  = LSMethod::cg;

        options.precond() = LSPrecond::none;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.precond() = LSPrecond::diagonal;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.precond() = LSPrecond::incomplete_LU;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.precond() = LSPrecond::incomplete_cholesky;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.precond() = LSPrecond::amg;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));
      }
    }

#ifndef PUGS_HAS_PETSC
    SECTION("not linked PETSc")
    {
      options.library() = LSLibrary::petsc;
      options.method()  = LSMethod::cg;
      options.precond() = LSPrecond::none;

      REQUIRE_THROWS_WITH(LinearSolver{options}, "error: PETSc is not linked to pugs. Cannot use it!");
    }
#endif   // PUGS_HAS_PETSC

    SECTION("Eigen3")
    {
      LinearSolverOptions always_valid;
      always_valid.library() = LSLibrary::builtin;
      always_valid.method()  = LSMethod::cg;
      always_valid.precond() = LSPrecond::none;

      LinearSolver linear_solver{always_valid};

      SECTION("Eigen3 methods")
      {
        options.library() = LSLibrary::eigen3;
        options.precond() = LSPrecond::none;

        options.method() = LSMethod::cg;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.method() = LSMethod::bicgstab;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.method() = LSMethod::lu;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.method() = LSMethod::cholesky;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.method() = LSMethod::gmres;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));
      }

      SECTION("Eigen3 precond")
      {
        options.library() = LSLibrary::eigen3;
        options.method()  = LSMethod::cg;

        options.precond() = LSPrecond::none;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.precond() = LSPrecond::diagonal;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.precond() = LSPrecond::incomplete_LU;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));

        options.precond() = LSPrecond::incomplete_cholesky;
        REQUIRE_NOTHROW(linear_solver.checkOptions(options));
      }
    }

#ifndef PUGS_HAS_EIGEN3
    SECTION("not linked Eigen3")
    {
      options.library() = LSLibrary::eigen3;
      options.method()  = LSMethod::cg;
      options.precond() = LSPrecond::none;

      REQUIRE_THROWS_WITH(LinearSolver{options}, "error: Eigen3 is not linked to pugs. Cannot use it!");
    }
#endif   // PUGS_HAS_EIGEN3
  }

  SECTION("Sparse Matrices")
  {
    SECTION("check linear solvers")
    {
      SECTION("symmetric system")
      {
        Array<int> non_zeros{5};
        non_zeros.fill(3);
        non_zeros[0] = 2;
        non_zeros[4] = 2;
        CRSMatrixDescriptor S{5, 5, non_zeros};
        S(0, 0) = 2;
        S(0, 1) = -1;

        S(1, 0) = -1;
        S(1, 1) = 2;
        S(1, 2) = -1;

        S(2, 1) = -1;
        S(2, 2) = 2;
        S(2, 3) = -1;

        S(3, 2) = -1;
        S(3, 3) = 2;
        S(3, 4) = -1;

        S(4, 3) = -1;
        S(4, 4) = 2;

        CRSMatrix A{S.getCRSMatrix()};

        Vector<const double> x_exact = [] {
          Vector<double> y{5};
          y[0] = 1;
          y[1] = 3;
          y[2] = 2;
          y[3] = 4;
          y[4] = 5;
          return y;
        }();

        Vector<double> b = A * x_exact;

        SECTION("builtin")
        {
          SECTION("CG no preconditioner")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::builtin;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;

            Vector<double> x{5};
            x = zero;

            LinearSolver solver{options};

            solver.solveLocalSystem(A, x, b);
            Vector error = x - x_exact;
            REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
          }
        }

        SECTION("Eigen3")
        {
#ifdef PUGS_HAS_EIGEN3

          SECTION("CG")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::eigen3;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;
            options.verbose() = true;

            SECTION("CG no preconditioner")
            {
              options.precond() = LSPrecond::none;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("CG Diagonal")
            {
              options.precond() = LSPrecond::diagonal;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("CG ICholesky")
            {
              options.precond() = LSPrecond::incomplete_cholesky;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("CG AMG")
            {
              options.precond() = LSPrecond::amg;

              Vector<double> x{5};
              x = zero;

              REQUIRE_THROWS_WITH(LinearSolver{options}, "error: AMG is not an Eigen3 preconditioner!");
            }
          }

          SECTION("Cholesky")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::eigen3;
            options.method()  = LSMethod::cholesky;
            options.precond() = LSPrecond::none;

            Vector<double> x{5};
            x = zero;

            LinearSolver solver{options};

            solver.solveLocalSystem(A, x, b);
            Vector error = x - x_exact;
            REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
          }

#else    // PUGS_HAS_EIGEN3
          SECTION("Eigen3 not linked")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::eigen3;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;

            REQUIRE_THROWS_WITH(LinearSolver{options}, "error: Eigen3 is not linked to pugs. Cannot use it!");
          }
#endif   // PUGS_HAS_EIGEN3
        }

        SECTION("PETSc")
        {
#ifdef PUGS_HAS_PETSC

          SECTION("CG")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::petsc;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;
            options.verbose() = true;

            SECTION("CG no preconditioner")
            {
              options.precond() = LSPrecond::none;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("CG Diagonal")
            {
              options.precond() = LSPrecond::diagonal;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("CG ICholesky")
            {
              options.precond() = LSPrecond::incomplete_cholesky;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("CG AMG")
            {
              options.precond() = LSPrecond::amg;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }
          }

          SECTION("Cholesky")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::petsc;
            options.method()  = LSMethod::cholesky;
            options.precond() = LSPrecond::none;

            Vector<double> x{5};
            x = zero;

            LinearSolver solver{options};

            solver.solveLocalSystem(A, x, b);
            Vector error = x - x_exact;
            REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
          }

#else    // PUGS_HAS_PETSC
          SECTION("PETSc not linked")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::petsc;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;

            REQUIRE_THROWS_WITH(LinearSolver{options}, "error: PETSc is not linked to pugs. Cannot use it!");
          }
#endif   // PUGS_HAS_PETSC
        }
      }

      SECTION("none symmetric system")
      {
        Array<int> non_zeros{5};
        non_zeros.fill(3);
        non_zeros[0] = 2;
        non_zeros[4] = 2;
        CRSMatrixDescriptor S{5, 5, non_zeros};

        S(0, 0) = 2;
        S(0, 1) = -1;

        S(1, 0) = -0.2;
        S(1, 1) = 2;
        S(1, 2) = -1;

        S(2, 1) = -1;
        S(2, 2) = 4;
        S(2, 3) = -2;

        S(3, 2) = -1;
        S(3, 3) = 2;
        S(3, 4) = -0.1;

        S(4, 3) = 1;
        S(4, 4) = 3;

        CRSMatrix A{S.getCRSMatrix()};

        Vector<const double> x_exact = [] {
          Vector<double> y{5};
          y[0] = 1;
          y[1] = 3;
          y[2] = 2;
          y[3] = 4;
          y[4] = 5;
          return y;
        }();

        Vector<double> b = A * x_exact;

        SECTION("builtin")
        {
          SECTION("BICGStab no preconditioner")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::builtin;
            options.method()  = LSMethod::bicgstab;
            options.precond() = LSPrecond::none;

            Vector<double> x{5};
            x = zero;

            LinearSolver solver{options};

            solver.solveLocalSystem(A, x, b);
            Vector error = x - x_exact;
            REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
          }
        }

        SECTION("PETSc")
        {
#ifdef PUGS_HAS_PETSC

          SECTION("BICGStab")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::petsc;
            options.method()  = LSMethod::bicgstab;
            options.precond() = LSPrecond::none;
            options.verbose() = true;

            SECTION("BICGStab no preconditioner")
            {
              options.precond() = LSPrecond::none;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("BICGStab Diagonal")
            {
              options.precond() = LSPrecond::diagonal;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("BICGStab ILU")
            {
              options.precond() = LSPrecond::incomplete_LU;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }
          }

          SECTION("BICGStab2")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::petsc;
            options.method()  = LSMethod::bicgstab2;
            options.precond() = LSPrecond::none;

            SECTION("BICGStab2 no preconditioner")
            {
              options.precond() = LSPrecond::none;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("BICGStab2 Diagonal")
            {
              options.precond() = LSPrecond::diagonal;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }
          }

          SECTION("GMRES")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::petsc;
            options.method()  = LSMethod::gmres;
            options.precond() = LSPrecond::none;

            SECTION("GMRES no preconditioner")
            {
              options.precond() = LSPrecond::none;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("GMRES Diagonal")
            {
              options.precond() = LSPrecond::diagonal;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("GMRES ILU")
            {
              options.precond() = LSPrecond::incomplete_LU;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }
          }

          SECTION("LU")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::petsc;
            options.method()  = LSMethod::lu;
            options.precond() = LSPrecond::none;

            Vector<double> x{5};
            x = zero;

            LinearSolver solver{options};

            solver.solveLocalSystem(A, x, b);
            Vector error = x - x_exact;
            REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
          }

#else    // PUGS_HAS_PETSC
          SECTION("PETSc not linked")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::petsc;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;

            REQUIRE_THROWS_WITH(LinearSolver{options}, "error: PETSc is not linked to pugs. Cannot use it!");
          }
#endif   // PUGS_HAS_PETSC
        }

        SECTION("Eigen3")
        {
#ifdef PUGS_HAS_EIGEN3

          SECTION("BICGStab")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::eigen3;
            options.method()  = LSMethod::bicgstab;
            options.precond() = LSPrecond::none;
            options.verbose() = true;

            SECTION("BICGStab no preconditioner")
            {
              options.precond() = LSPrecond::none;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("BICGStab Diagonal")
            {
              options.precond() = LSPrecond::diagonal;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("BICGStab ILU")
            {
              options.precond() = LSPrecond::incomplete_LU;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }
          }

          SECTION("GMRES")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::eigen3;
            options.method()  = LSMethod::gmres;
            options.precond() = LSPrecond::none;

            SECTION("GMRES no preconditioner")
            {
              options.precond() = LSPrecond::none;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("GMRES Diagonal")
            {
              options.precond() = LSPrecond::diagonal;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("GMRES ILU")
            {
              options.precond() = LSPrecond::incomplete_LU;

              Vector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              Vector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }
          }

          SECTION("LU")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::eigen3;
            options.method()  = LSMethod::lu;
            options.precond() = LSPrecond::none;

            Vector<double> x{5};
            x = zero;

            LinearSolver solver{options};

            solver.solveLocalSystem(A, x, b);
            Vector error = x - x_exact;
            REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
          }

#else    // PUGS_HAS_EIGEN3
          SECTION("Eigen3 not linked")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::eigen3;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;

            REQUIRE_THROWS_WITH(LinearSolver{options}, "error: Eigen3 is not linked to pugs. Cannot use it!");
          }
#endif   // PUGS_HAS_EIGEN3
        }
      }
    }
  }

  SECTION("Dense Matrices")
  {
    SECTION("check linear solvers")
    {
      SECTION("symmetric system")
      {
        SmallMatrix<double> A{5};
        A = zero;

        A(0, 0) = 2;
        A(0, 1) = -1;

        A(1, 0) = -1;
        A(1, 1) = 2;
        A(1, 2) = -1;

        A(2, 1) = -1;
        A(2, 2) = 2;
        A(2, 3) = -1;

        A(3, 2) = -1;
        A(3, 3) = 2;
        A(3, 4) = -1;

        A(4, 3) = -1;
        A(4, 4) = 2;

        SmallVector<const double> x_exact = [] {
          SmallVector<double> y{5};
          y[0] = 1;
          y[1] = 3;
          y[2] = 2;
          y[3] = 4;
          y[4] = 5;
          return y;
        }();

        SmallVector<double> b = A * x_exact;

        SECTION("builtin")
        {
          SECTION("CG no preconditioner")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::builtin;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;

            SmallVector<double> x{5};
            x = zero;

            LinearSolver solver{options};

            solver.solveLocalSystem(A, x, b);
            SmallVector error = x - x_exact;
            REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
          }
        }

        SECTION("Eigen3")
        {
#ifdef PUGS_HAS_EIGEN3

          SECTION("CG")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::eigen3;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;
            options.verbose() = true;

            SECTION("CG no preconditioner")
            {
              options.precond() = LSPrecond::none;

              SmallVector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              SmallVector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("CG Diagonal")
            {
              options.precond() = LSPrecond::diagonal;

              SmallVector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              SmallVector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("CG ICholesky")
            {
              options.precond() = LSPrecond::incomplete_cholesky;

              SmallVector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              REQUIRE_THROWS_WITH(solver.solveLocalSystem(A, x, b),
                                  "error: incomplete cholesky is not available for dense matrices in Eigen3");
            }

            SECTION("CG AMG")
            {
              options.precond() = LSPrecond::amg;

              SmallVector<double> x{5};
              x = zero;

              REQUIRE_THROWS_WITH(LinearSolver{options}, "error: AMG is not an Eigen3 preconditioner!");
            }
          }

          SECTION("Cholesky")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::eigen3;
            options.method()  = LSMethod::cholesky;
            options.precond() = LSPrecond::none;

            SmallVector<double> x{5};
            x = zero;

            LinearSolver solver{options};

            solver.solveLocalSystem(A, x, b);
            SmallVector error = x - x_exact;
            REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
          }

#else    // PUGS_HAS_EIGEN3
          SECTION("Eigen3 not linked")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::eigen3;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;

            REQUIRE_THROWS_WITH(LinearSolver{options}, "error: Eigen3 is not linked to pugs. Cannot use it!");
          }
#endif   // PUGS_HAS_EIGEN3
        }

        SECTION("PETSc")
        {
#ifdef PUGS_HAS_PETSC

          SECTION("CG")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::petsc;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;
            options.verbose() = true;

            SECTION("CG no preconditioner")
            {
              options.precond() = LSPrecond::none;

              SmallVector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              SmallVector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("CG Diagonal")
            {
              options.precond() = LSPrecond::diagonal;

              SmallVector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              SmallVector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("CG ICholesky")
            {
              options.precond() = LSPrecond::incomplete_cholesky;

              SmallVector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              SmallVector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

            SECTION("CG AMG")
            {
              options.precond() = LSPrecond::amg;

              SmallVector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              SmallVector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }
          }

          SECTION("Cholesky")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::petsc;
            options.method()  = LSMethod::cholesky;
            options.precond() = LSPrecond::none;

            SmallVector<double> x{5};
            x = zero;

            LinearSolver solver{options};

            solver.solveLocalSystem(A, x, b);
            SmallVector error = x - x_exact;
            REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
          }

#else    // PUGS_HAS_PETSC
          SECTION("PETSc not linked")
          {
            LinearSolverOptions options;
            options.library() = LSLibrary::petsc;
            options.method()  = LSMethod::cg;
            options.precond() = LSPrecond::none;

            REQUIRE_THROWS_WITH(LinearSolver{options}, "error: PETSc is not linked to pugs. Cannot use it!");
          }
#endif   // PUGS_HAS_PETSC
        }
      }

      SECTION("none symmetric system")
      {
        SECTION("Dense matrix")
        {
          SmallMatrix<double> A{5};
          A = zero;

          A(0, 0) = 2;
          A(0, 1) = -1;

          A(1, 0) = -0.2;
          A(1, 1) = 2;
          A(1, 2) = -1;

          A(2, 1) = -1;
          A(2, 2) = 4;
          A(2, 3) = -2;

          A(3, 2) = -1;
          A(3, 3) = 2;
          A(3, 4) = -0.1;

          A(4, 3) = 1;
          A(4, 4) = 3;

          SmallVector<const double> x_exact = [] {
            SmallVector<double> y{5};
            y[0] = 1;
            y[1] = 3;
            y[2] = 2;
            y[3] = 4;
            y[4] = 5;
            return y;
          }();

          SmallVector<double> b = A * x_exact;

          SECTION("builtin")
          {
            SECTION("BICGStab no preconditioner")
            {
              LinearSolverOptions options;
              options.library() = LSLibrary::builtin;
              options.method()  = LSMethod::bicgstab;
              options.precond() = LSPrecond::none;

              SmallVector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              SmallVector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }
          }

          SECTION("Eigen3")
          {
#ifdef PUGS_HAS_EIGEN3

            SECTION("BICGStab")
            {
              LinearSolverOptions options;
              options.library() = LSLibrary::eigen3;
              options.method()  = LSMethod::bicgstab;
              options.precond() = LSPrecond::none;
              options.verbose() = true;

              SECTION("BICGStab no preconditioner")
              {
                options.precond() = LSPrecond::none;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }

              SECTION("BICGStab Diagonal")
              {
                options.precond() = LSPrecond::diagonal;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }

              SECTION("BICGStab ILU")
              {
                options.precond() = LSPrecond::incomplete_LU;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                REQUIRE_THROWS_WITH(solver.solveLocalSystem(A, x, b),
                                    "error: incomplete LU is not available for dense matrices in Eigen3");
              }
            }

            SECTION("BICGStab2")
            {
              LinearSolverOptions options;
              options.library() = LSLibrary::eigen3;
              options.method()  = LSMethod::bicgstab2;
              options.precond() = LSPrecond::none;

              SECTION("BICGStab2 no preconditioner")
              {
                options.precond() = LSPrecond::none;

                SmallVector<double> x{5};
                x = zero;

                REQUIRE_THROWS_WITH(LinearSolver{options}, "error: BICGStab2 is not an Eigen3 linear solver!");
              }
            }

            SECTION("GMRES")
            {
              LinearSolverOptions options;
              options.library() = LSLibrary::eigen3;
              options.method()  = LSMethod::gmres;
              options.precond() = LSPrecond::none;

              SECTION("GMRES no preconditioner")
              {
                options.precond() = LSPrecond::none;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }

              SECTION("GMRES Diagonal")
              {
                options.precond() = LSPrecond::diagonal;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }

              SECTION("GMRES ILU")
              {
                options.precond() = LSPrecond::incomplete_LU;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                REQUIRE_THROWS_WITH(solver.solveLocalSystem(A, x, b),
                                    "error: incomplete LU is not available for dense matrices in Eigen3");
              }
            }

            SECTION("LU")
            {
              LinearSolverOptions options;
              options.library() = LSLibrary::eigen3;
              options.method()  = LSMethod::lu;
              options.precond() = LSPrecond::none;

              SmallVector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              SmallVector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

#else    // PUGS_HAS_EIGEN3
            SECTION("Eigen3 not linked")
            {
              LinearSolverOptions options;
              options.library() = LSLibrary::eigen3;
              options.method()  = LSMethod::cg;
              options.precond() = LSPrecond::none;

              REQUIRE_THROWS_WITH(LinearSolver{options}, "error: Eigen3 is not linked to pugs. Cannot use it!");
            }
#endif   // PUGS_HAS_EIGEN3
          }

          SECTION("PETSc")
          {
#ifdef PUGS_HAS_PETSC

            SECTION("BICGStab")
            {
              LinearSolverOptions options;
              options.library() = LSLibrary::petsc;
              options.method()  = LSMethod::bicgstab;
              options.precond() = LSPrecond::none;
              options.verbose() = true;

              SECTION("BICGStab no preconditioner")
              {
                options.precond() = LSPrecond::none;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }

              SECTION("BICGStab Diagonal")
              {
                options.precond() = LSPrecond::diagonal;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }

              SECTION("BICGStab ILU")
              {
                options.precond() = LSPrecond::incomplete_LU;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }
            }

            SECTION("BICGStab2")
            {
              LinearSolverOptions options;
              options.library() = LSLibrary::petsc;
              options.method()  = LSMethod::bicgstab2;
              options.precond() = LSPrecond::none;

              SECTION("BICGStab2 no preconditioner")
              {
                options.precond() = LSPrecond::none;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }

              SECTION("BICGStab2 Diagonal")
              {
                options.precond() = LSPrecond::diagonal;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }
            }

            SECTION("GMRES")
            {
              LinearSolverOptions options;
              options.library() = LSLibrary::petsc;
              options.method()  = LSMethod::gmres;
              options.precond() = LSPrecond::none;

              SECTION("GMRES no preconditioner")
              {
                options.precond() = LSPrecond::none;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }

              SECTION("GMRES Diagonal")
              {
                options.precond() = LSPrecond::diagonal;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }

              SECTION("GMRES ILU")
              {
                options.precond() = LSPrecond::incomplete_LU;

                SmallVector<double> x{5};
                x = zero;

                LinearSolver solver{options};

                solver.solveLocalSystem(A, x, b);
                SmallVector error = x - x_exact;
                REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
              }
            }

            SECTION("LU")
            {
              LinearSolverOptions options;
              options.library() = LSLibrary::petsc;
              options.method()  = LSMethod::lu;
              options.precond() = LSPrecond::none;

              SmallVector<double> x{5};
              x = zero;

              LinearSolver solver{options};

              solver.solveLocalSystem(A, x, b);
              SmallVector error = x - x_exact;
              REQUIRE(std::sqrt(dot(error, error)) < 1E-10 * std::sqrt(dot(x_exact, x_exact)));
            }

#else    // PUGS_HAS_PETSC
            SECTION("PETSc not linked")
            {
              LinearSolverOptions options;
              options.library() = LSLibrary::petsc;
              options.method()  = LSMethod::cg;
              options.precond() = LSPrecond::none;

              REQUIRE_THROWS_WITH(LinearSolver{options}, "error: PETSc is not linked to pugs. Cannot use it!");
            }
#endif   // PUGS_HAS_PETSC
          }
        }
      }
    }
  }
}
