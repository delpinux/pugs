#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/DualConnectivityManager.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityUtils.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>

template <ItemType item_type, typename ConnectivityType>
inline auto
get_item_ref_ids(const ConnectivityType& connectivity)
{
  std::map<std::string, size_t> ref_id_set;
  for (size_t i = 0; i < connectivity.template numberOfRefItemList<item_type>(); ++i) {
    const auto& ref_id_list = connectivity.template refItemList<item_type>(i);
    std::ostringstream os;
    os << ref_id_list.refId();
    ItemValue<size_t, item_type> item_tag{connectivity};
    item_tag.fill(0);
    for (size_t i_item = 0; i_item < ref_id_list.list().size(); ++i_item) {
      item_tag[ref_id_list.list()[i_item]] = 1;
    }

    ref_id_set[os.str()] = sum(item_tag);
  }
  return ref_id_set;
}

// clazy:excludeall=non-pod-global-static

TEST_CASE("DiamondDualConnectivityBuilder", "[mesh]")
{
  SECTION("2D")
  {
    constexpr static size_t Dimension = 2;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    std::shared_ptr<const MeshType> mesh        = MeshDataBaseForTests::get().hybrid2DMesh()->get<MeshType>();
    const ConnectivityType& primal_connectivity = mesh->connectivity();

    REQUIRE(primal_connectivity.numberOfNodes() == 53);
    REQUIRE(primal_connectivity.numberOfFaces() == 110);
    REQUIRE(primal_connectivity.numberOfCells() == 58);

    std::shared_ptr p_diamond_dual_connectivity =
      DualConnectivityManager::instance().getDiamondDualConnectivity(primal_connectivity);
    const ConnectivityType& dual_connectivity = *p_diamond_dual_connectivity;

    REQUIRE(dual_connectivity.numberOfNodes() == 111);
    REQUIRE(dual_connectivity.numberOfFaces() == 220);
    REQUIRE(dual_connectivity.numberOfCells() == 110);

    REQUIRE(checkConnectivityOrdering(dual_connectivity));

    SECTION("ref node list")
    {
      REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::node>() == 4);
      REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::node>() == 4);

      const auto primal_ref_node_list = get_item_ref_ids<ItemType::node>(primal_connectivity);
      REQUIRE(primal_ref_node_list.size() == 4);

      REQUIRE(primal_ref_node_list.count("XMAXYMAX(11)") == 1);
      REQUIRE(primal_ref_node_list.count("XMAXYMIN(10)") == 1);
      REQUIRE(primal_ref_node_list.count("XMINYMAX(9)") == 1);
      REQUIRE(primal_ref_node_list.count("XMINYMIN(8)") == 1);

      REQUIRE(primal_ref_node_list.at("XMAXYMAX(11)") == 1);
      REQUIRE(primal_ref_node_list.at("XMAXYMIN(10)") == 1);
      REQUIRE(primal_ref_node_list.at("XMINYMAX(9)") == 1);
      REQUIRE(primal_ref_node_list.at("XMINYMIN(8)") == 1);

      const auto dual_ref_node_list = get_item_ref_ids<ItemType::node>(dual_connectivity);
      REQUIRE(dual_ref_node_list.size() == 4);

      REQUIRE(dual_ref_node_list.count("XMAXYMAX(11)") == 1);
      REQUIRE(dual_ref_node_list.count("XMAXYMIN(10)") == 1);
      REQUIRE(dual_ref_node_list.count("XMINYMAX(9)") == 1);
      REQUIRE(dual_ref_node_list.count("XMINYMIN(8)") == 1);

      REQUIRE(dual_ref_node_list.at("XMAXYMAX(11)") == 1);
      REQUIRE(dual_ref_node_list.at("XMAXYMIN(10)") == 1);
      REQUIRE(dual_ref_node_list.at("XMINYMAX(9)") == 1);
      REQUIRE(dual_ref_node_list.at("XMINYMIN(8)") == 1);
    }

    SECTION("ref face list")
    {
      REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::face>() == 5);
      REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::face>() == 4);

      const auto primal_ref_face_list = get_item_ref_ids<ItemType::face>(primal_connectivity);
      REQUIRE(primal_ref_face_list.size() == 5);

      REQUIRE(primal_ref_face_list.count("INTERFACE(5)") == 1);
      REQUIRE(primal_ref_face_list.count("XMAX(2)") == 1);
      REQUIRE(primal_ref_face_list.count("XMIN(1)") == 1);
      REQUIRE(primal_ref_face_list.count("YMAX(3)") == 1);
      REQUIRE(primal_ref_face_list.count("YMIN(4)") == 1);

      REQUIRE(primal_ref_face_list.at("INTERFACE(5)") == 4);
      REQUIRE(primal_ref_face_list.at("XMAX(2)") == 4);
      REQUIRE(primal_ref_face_list.at("XMIN(1)") == 4);
      REQUIRE(primal_ref_face_list.at("YMAX(3)") == 8);
      REQUIRE(primal_ref_face_list.at("YMIN(4)") == 8);

      const auto dual_ref_face_list = get_item_ref_ids<ItemType::face>(dual_connectivity);
      REQUIRE(dual_ref_face_list.size() == 4);

      REQUIRE(dual_ref_face_list.count("XMAX(2)") == 1);
      REQUIRE(dual_ref_face_list.count("XMIN(1)") == 1);
      REQUIRE(dual_ref_face_list.count("YMAX(3)") == 1);
      REQUIRE(dual_ref_face_list.count("YMIN(4)") == 1);

      REQUIRE(dual_ref_face_list.at("XMAX(2)") == 4);
      REQUIRE(dual_ref_face_list.at("XMIN(1)") == 4);
      REQUIRE(dual_ref_face_list.at("YMAX(3)") == 8);
      REQUIRE(dual_ref_face_list.at("YMIN(4)") == 8);
    }

    SECTION("ref cell list")
    {
      REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::cell>() == 2);
      // Diamond dual cell references are not computed
      REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::cell>() == 0);

      const auto primal_ref_cell_list = get_item_ref_ids<ItemType::cell>(primal_connectivity);
      REQUIRE(primal_ref_cell_list.size() == 2);

      REQUIRE(primal_ref_cell_list.count("LEFT(6)") == 1);
      REQUIRE(primal_ref_cell_list.count("RIGHT(7)") == 1);

      REQUIRE(primal_ref_cell_list.at("LEFT(6)") == 22);
      REQUIRE(primal_ref_cell_list.at("RIGHT(7)") == 36);

      const auto dual_ref_cell_list = get_item_ref_ids<ItemType::cell>(dual_connectivity);
      REQUIRE(dual_ref_cell_list.size() == 0);
    }
  }

  SECTION("3D")
  {
    constexpr static size_t Dimension = 3;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    std::shared_ptr<const MeshType> mesh        = MeshDataBaseForTests::get().hybrid3DMesh()->get<MeshType>();
    const ConnectivityType& primal_connectivity = mesh->connectivity();

    REQUIRE(primal_connectivity.numberOfNodes() == 132);
    REQUIRE(primal_connectivity.numberOfEdges() == 452);
    REQUIRE(primal_connectivity.numberOfFaces() == 520);
    REQUIRE(primal_connectivity.numberOfCells() == 199);

    std::shared_ptr p_diamond_dual_connectivity =
      DualConnectivityManager::instance().getDiamondDualConnectivity(primal_connectivity);
    const ConnectivityType& dual_connectivity = *p_diamond_dual_connectivity;

    REQUIRE(checkConnectivityOrdering(dual_connectivity));

    REQUIRE(dual_connectivity.numberOfNodes() == 331);
    REQUIRE(dual_connectivity.numberOfEdges() == 1461);
    REQUIRE(dual_connectivity.numberOfFaces() == 1651);
    REQUIRE(dual_connectivity.numberOfCells() == 520);

    SECTION("ref node list")
    {
      REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::node>() == 8);
      REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::node>() == 8);

      const auto primal_ref_node_list = get_item_ref_ids<ItemType::node>(primal_connectivity);
      REQUIRE(primal_ref_node_list.size() == 8);

      REQUIRE(primal_ref_node_list.count("XMAXYMAXZMAX(47)") == 1);
      REQUIRE(primal_ref_node_list.count("XMAXYMAXZMIN(51)") == 1);
      REQUIRE(primal_ref_node_list.count("XMAXYMINZMAX(45)") == 1);
      REQUIRE(primal_ref_node_list.count("XMAXYMINZMIN(41)") == 1);
      REQUIRE(primal_ref_node_list.count("XMINYMAXZMAX(43)") == 1);
      REQUIRE(primal_ref_node_list.count("XMINYMAXZMIN(42)") == 1);
      REQUIRE(primal_ref_node_list.count("XMINYMINZMAX(44)") == 1);
      REQUIRE(primal_ref_node_list.count("XMINYMINZMIN(40)") == 1);

      REQUIRE(primal_ref_node_list.at("XMAXYMAXZMAX(47)") == 1);
      REQUIRE(primal_ref_node_list.at("XMAXYMAXZMIN(51)") == 1);
      REQUIRE(primal_ref_node_list.at("XMAXYMINZMAX(45)") == 1);
      REQUIRE(primal_ref_node_list.at("XMAXYMINZMIN(41)") == 1);
      REQUIRE(primal_ref_node_list.at("XMINYMAXZMAX(43)") == 1);
      REQUIRE(primal_ref_node_list.at("XMINYMAXZMIN(42)") == 1);
      REQUIRE(primal_ref_node_list.at("XMINYMINZMAX(44)") == 1);
      REQUIRE(primal_ref_node_list.at("XMINYMINZMIN(40)") == 1);

      const auto dual_ref_node_list = get_item_ref_ids<ItemType::node>(dual_connectivity);
      REQUIRE(dual_ref_node_list.size() == 8);

      REQUIRE(dual_ref_node_list.count("XMAXYMAXZMAX(47)") == 1);
      REQUIRE(dual_ref_node_list.count("XMAXYMAXZMIN(51)") == 1);
      REQUIRE(dual_ref_node_list.count("XMAXYMINZMAX(45)") == 1);
      REQUIRE(dual_ref_node_list.count("XMAXYMINZMIN(41)") == 1);
      REQUIRE(dual_ref_node_list.count("XMINYMAXZMAX(43)") == 1);
      REQUIRE(dual_ref_node_list.count("XMINYMAXZMIN(42)") == 1);
      REQUIRE(dual_ref_node_list.count("XMINYMINZMAX(44)") == 1);
      REQUIRE(dual_ref_node_list.count("XMINYMINZMIN(40)") == 1);

      REQUIRE(dual_ref_node_list.at("XMAXYMAXZMAX(47)") == 1);
      REQUIRE(dual_ref_node_list.at("XMAXYMAXZMIN(51)") == 1);
      REQUIRE(dual_ref_node_list.at("XMAXYMINZMAX(45)") == 1);
      REQUIRE(dual_ref_node_list.at("XMAXYMINZMIN(41)") == 1);
      REQUIRE(dual_ref_node_list.at("XMINYMAXZMAX(43)") == 1);
      REQUIRE(dual_ref_node_list.at("XMINYMAXZMIN(42)") == 1);
      REQUIRE(dual_ref_node_list.at("XMINYMINZMAX(44)") == 1);
      REQUIRE(dual_ref_node_list.at("XMINYMINZMIN(40)") == 1);
    }

    SECTION("ref edge list")
    {
      REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::edge>() == 12);
      REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::edge>() == 12);

      const auto primal_ref_edge_list = get_item_ref_ids<ItemType::edge>(primal_connectivity);
      REQUIRE(primal_ref_edge_list.size() == 12);

      REQUIRE(primal_ref_edge_list.count("XMAXYMAX(34)") == 1);
      REQUIRE(primal_ref_edge_list.count("XMAXYMIN(35)") == 1);
      REQUIRE(primal_ref_edge_list.count("XMAXZMAX(33)") == 1);
      REQUIRE(primal_ref_edge_list.count("XMAXZMIN(32)") == 1);
      REQUIRE(primal_ref_edge_list.count("XMINYMAX(30)") == 1);
      REQUIRE(primal_ref_edge_list.count("XMINYMIN(31)") == 1);
      REQUIRE(primal_ref_edge_list.count("XMINZMAX(29)") == 1);
      REQUIRE(primal_ref_edge_list.count("XMINZMIN(28)") == 1);
      REQUIRE(primal_ref_edge_list.count("YMAXZMAX(39)") == 1);
      REQUIRE(primal_ref_edge_list.count("YMAXZMIN(38)") == 1);
      REQUIRE(primal_ref_edge_list.count("YMINZMAX(37)") == 1);
      REQUIRE(primal_ref_edge_list.count("YMINZMIN(36)") == 1);

      REQUIRE(primal_ref_edge_list.at("XMAXYMAX(34)") == 2);
      REQUIRE(primal_ref_edge_list.at("XMAXYMIN(35)") == 2);
      REQUIRE(primal_ref_edge_list.at("XMAXZMAX(33)") == 2);
      REQUIRE(primal_ref_edge_list.at("XMAXZMIN(32)") == 2);
      REQUIRE(primal_ref_edge_list.at("XMINYMAX(30)") == 3);
      REQUIRE(primal_ref_edge_list.at("XMINYMIN(31)") == 3);
      REQUIRE(primal_ref_edge_list.at("XMINZMAX(29)") == 4);
      REQUIRE(primal_ref_edge_list.at("XMINZMIN(28)") == 4);
      REQUIRE(primal_ref_edge_list.at("YMAXZMAX(39)") == 5);
      REQUIRE(primal_ref_edge_list.at("YMAXZMIN(38)") == 5);
      REQUIRE(primal_ref_edge_list.at("YMINZMAX(37)") == 6);
      REQUIRE(primal_ref_edge_list.at("YMINZMIN(36)") == 6);

      const auto dual_ref_edge_list = get_item_ref_ids<ItemType::edge>(dual_connectivity);
      REQUIRE(dual_ref_edge_list.size() == 12);

      REQUIRE(dual_ref_edge_list.count("XMAXYMAX(34)") == 1);
      REQUIRE(dual_ref_edge_list.count("XMAXYMIN(35)") == 1);
      REQUIRE(dual_ref_edge_list.count("XMAXZMAX(33)") == 1);
      REQUIRE(dual_ref_edge_list.count("XMAXZMIN(32)") == 1);
      REQUIRE(dual_ref_edge_list.count("XMINYMAX(30)") == 1);
      REQUIRE(dual_ref_edge_list.count("XMINYMIN(31)") == 1);
      REQUIRE(dual_ref_edge_list.count("XMINZMAX(29)") == 1);
      REQUIRE(dual_ref_edge_list.count("XMINZMIN(28)") == 1);
      REQUIRE(dual_ref_edge_list.count("YMAXZMAX(39)") == 1);
      REQUIRE(dual_ref_edge_list.count("YMAXZMIN(38)") == 1);
      REQUIRE(dual_ref_edge_list.count("YMINZMAX(37)") == 1);
      REQUIRE(dual_ref_edge_list.count("YMINZMIN(36)") == 1);

      REQUIRE(dual_ref_edge_list.at("XMAXYMAX(34)") == 2);
      REQUIRE(dual_ref_edge_list.at("XMAXYMIN(35)") == 2);
      REQUIRE(dual_ref_edge_list.at("XMAXZMAX(33)") == 2);
      REQUIRE(dual_ref_edge_list.at("XMAXZMIN(32)") == 2);
      REQUIRE(dual_ref_edge_list.at("XMINYMAX(30)") == 3);
      REQUIRE(dual_ref_edge_list.at("XMINYMIN(31)") == 3);
      REQUIRE(dual_ref_edge_list.at("XMINZMAX(29)") == 4);
      REQUIRE(dual_ref_edge_list.at("XMINZMIN(28)") == 4);
      REQUIRE(dual_ref_edge_list.at("YMAXZMAX(39)") == 5);
      REQUIRE(dual_ref_edge_list.at("YMAXZMIN(38)") == 5);
      REQUIRE(dual_ref_edge_list.at("YMINZMAX(37)") == 6);
      REQUIRE(dual_ref_edge_list.at("YMINZMIN(36)") == 6);
    }

    SECTION("ref face list")
    {
      REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::face>() == 8);
      REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::face>() == 6);

      const auto primal_ref_face_list = get_item_ref_ids<ItemType::face>(primal_connectivity);
      REQUIRE(primal_ref_face_list.size() == 8);

      REQUIRE(primal_ref_face_list.count("INTERFACE1(55)") == 1);
      REQUIRE(primal_ref_face_list.count("INTERFACE2(56)") == 1);
      REQUIRE(primal_ref_face_list.count("XMAX(23)") == 1);
      REQUIRE(primal_ref_face_list.count("XMIN(22)") == 1);
      REQUIRE(primal_ref_face_list.count("YMAX(26)") == 1);
      REQUIRE(primal_ref_face_list.count("YMIN(27)") == 1);
      REQUIRE(primal_ref_face_list.count("ZMAX(24)") == 1);
      REQUIRE(primal_ref_face_list.count("ZMIN(25)") == 1);

      REQUIRE(primal_ref_face_list.at("INTERFACE1(55)") == 12);
      REQUIRE(primal_ref_face_list.at("INTERFACE2(56)") == 9);
      REQUIRE(primal_ref_face_list.at("XMAX(23)") == 12);
      REQUIRE(primal_ref_face_list.at("XMIN(22)") == 12);
      REQUIRE(primal_ref_face_list.at("YMAX(26)") == 18);
      REQUIRE(primal_ref_face_list.at("YMIN(27)") == 21);
      REQUIRE(primal_ref_face_list.at("ZMAX(24)") == 35);
      REQUIRE(primal_ref_face_list.at("ZMIN(25)") == 35);

      const auto dual_ref_face_list = get_item_ref_ids<ItemType::face>(dual_connectivity);
      REQUIRE(dual_ref_face_list.size() == 6);

      REQUIRE(dual_ref_face_list.count("XMAX(23)") == 1);
      REQUIRE(dual_ref_face_list.count("XMIN(22)") == 1);
      REQUIRE(dual_ref_face_list.count("YMAX(26)") == 1);
      REQUIRE(dual_ref_face_list.count("YMIN(27)") == 1);
      REQUIRE(dual_ref_face_list.count("ZMAX(24)") == 1);
      REQUIRE(dual_ref_face_list.count("ZMIN(25)") == 1);

      REQUIRE(dual_ref_face_list.at("XMAX(23)") == 12);
      REQUIRE(dual_ref_face_list.at("XMIN(22)") == 12);
      REQUIRE(dual_ref_face_list.at("YMAX(26)") == 18);
      REQUIRE(dual_ref_face_list.at("YMIN(27)") == 21);
      REQUIRE(dual_ref_face_list.at("ZMAX(24)") == 35);
      REQUIRE(dual_ref_face_list.at("ZMIN(25)") == 35);
    }

    SECTION("ref cell list")
    {
      REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::cell>() == 3);

      const auto primal_ref_cell_list = get_item_ref_ids<ItemType::cell>(primal_connectivity);
      REQUIRE(primal_ref_cell_list.size() == 3);

      REQUIRE(primal_ref_cell_list.count("LEFT(52)") == 1);
      REQUIRE(primal_ref_cell_list.count("MIDDLE(53)") == 1);
      REQUIRE(primal_ref_cell_list.count("RIGHT(54)") == 1);

      REQUIRE(primal_ref_cell_list.at("LEFT(52)") == 30);
      REQUIRE(primal_ref_cell_list.at("MIDDLE(53)") == 42);
      REQUIRE(primal_ref_cell_list.at("RIGHT(54)") == 127);

      // Diamond dual cell references are not computed
      REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::cell>() == 0);

      const auto dual_ref_cell_list = get_item_ref_ids<ItemType::cell>(dual_connectivity);
      REQUIRE(dual_ref_cell_list.size() == 0);
    }
  }
}
