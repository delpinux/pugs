#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/RevisionInfo.hpp>

#include <utils/pugs_git_revision.hpp>
#include <utils/pugs_version.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("RevisionInfo", "[utils]")
{
  SECTION("checking pugs version")
  {
    REQUIRE((RevisionInfo::version() == PUGS_VERSION));
  }
  SECTION("checking git info")
  {
#ifdef HAS_PUGS_GIT_INFO
    REQUIRE((RevisionInfo::hasGitInfo() == true));
    REQUIRE((RevisionInfo::gitTag() == PUGS_GIT_TAG));
    REQUIRE((RevisionInfo::gitHead() == PUGS_GIT_HEAD));
    REQUIRE((RevisionInfo::gitHash() == PUGS_GIT_HASH));
    REQUIRE((RevisionInfo::gitIsClean() == PUGS_GIT_IS_CLEAN));

    SECTION("checking tag")
    {
      std::string tag_from_version = "v";
      tag_from_version += std::string(PUGS_VERSION);
      REQUIRE(tag_from_version == RevisionInfo::gitTag());
    }
#else
    REQUIRE((RevisionInfo::hasGitInfo() == false));
    REQUIRE((RevisionInfo::gitTag() == "unknown tag"));
    REQUIRE((RevisionInfo::gitHead() == "unknown head"));
    REQUIRE((RevisionInfo::gitHash() == "unknown hash"));
    REQUIRE((RevisionInfo::gitIsClean() == false));
#endif
  }
}
