#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/node_processor/IfProcessor.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_IF_PROCESSOR_RESULT(data, variable_name, expected_value)        \
  {                                                                           \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                \
    auto ast = ASTBuilder::build(input);                                      \
                                                                              \
    ASTExecutionStack::create();                                              \
                                                                              \
    ASTModulesImporter{*ast};                                                 \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                   \
                                                                              \
    ASTSymbolTableBuilder{*ast};                                              \
    ASTNodeDataTypeBuilder{*ast};                                             \
                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                      \
                                                                              \
    ASTNodeExpressionBuilder{*ast};                                           \
    ExecutionPolicy exec_policy;                                              \
    ast->execute(exec_policy);                                                \
                                                                              \
    auto symbol_table = ast->m_symbol_table;                                  \
                                                                              \
    using namespace TAO_PEGTL_NAMESPACE;                                      \
    position use_position{10000, 1000, 10, "fixture"};                        \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);   \
                                                                              \
    auto attributes = symbol->attributes();                                   \
    auto value      = std::get<decltype(expected_value)>(attributes.value()); \
                                                                              \
    ASTExecutionStack::destroy();                                             \
                                                                              \
    REQUIRE(value == expected_value);                                         \
    ast->m_symbol_table->clearValues();                                       \
  }

#define CHECK_IF_PROCESSOR_THROWS_WITH(data, error_message)           \
  {                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};        \
    auto ast = ASTBuilder::build(input);                              \
                                                                      \
    ASTExecutionStack::create();                                      \
                                                                      \
    ASTModulesImporter{*ast};                                         \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};           \
                                                                      \
    ASTSymbolTableBuilder{*ast};                                      \
                                                                      \
    ASTExecutionStack::destroy();                                     \
                                                                      \
    REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error_message); \
    ast->m_symbol_table->clearValues();                               \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("IfProcessor", "[language]")
{
  SECTION("simple if(true)")
  {
    std::string_view data = R"(
let i:N, i = 0;
if(true) {
  i = 1;
}
)";
    CHECK_IF_PROCESSOR_RESULT(data, "i", 1ul);
  }

  SECTION("simple if(false)")
  {
    std::string_view data = R"(
let i:N, i = 0;
if(false) {
  i = 1;
}
)";
    CHECK_IF_PROCESSOR_RESULT(data, "i", 0ul);
  }

  SECTION("simple if(true)else")
  {
    std::string_view data = R"(
let i:N, i = 0;
if(true) {
  i = 1;
} else {
  i = 2;
}
)";
    CHECK_IF_PROCESSOR_RESULT(data, "i", 1ul);
  }

  SECTION("simple if(false)")
  {
    std::string_view data = R"(
let i:N, i = 0;
if(false) {
  i = 1;
} else {
  i = 2;
}
)";
    CHECK_IF_PROCESSOR_RESULT(data, "i", 2ul);
  }

  SECTION("simple if(true) with local variable")
  {
    std::string_view data = R"(
let i:N, i = 0;
if(true) {
  let j:N, j = 1;
  i = j;
}
)";
    CHECK_IF_PROCESSOR_RESULT(data, "i", 1ul);
  }

  SECTION("simple if(false) with else local variable")
  {
    std::string_view data = R"(
let i:N, i = 0;
if(false) {} else {
  let j:N, j = 1;
  i = j;
}
)";
    CHECK_IF_PROCESSOR_RESULT(data, "i", 1ul);
  }

  SECTION("errors")
  {
    SECTION("bad test type")
    {
      std::string_view data = R"(
if (1.2) {
}

)";

      CHECK_IF_PROCESSOR_THROWS_WITH(data, "invalid implicit conversion: R -> B");
    }
  }

  SECTION("expression type")
  {
    ASTNode node;
    REQUIRE(IfProcessor{node}.type() == INodeProcessor::Type::if_processor);
  }
}
