#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <mesh/RefId.hpp>

#include <sstream>

// clazy:excludeall=non-pod-global-static

TEST_CASE("RefId", "[mesh]")
{
  SECTION("tag with name")
  {
    RefId ref_id0(3, "tag_name");
    REQUIRE(ref_id0.tagNumber() == 3);
    REQUIRE(ref_id0.tagName() == "tag_name");

    RefId ref_id1(ref_id0);
    REQUIRE(ref_id1.tagNumber() == 3);
    REQUIRE(ref_id1.tagName() == "tag_name");
    REQUIRE(ref_id0 == ref_id1);

    RefId ref_id2(2, "another_tag_name");
    REQUIRE(ref_id2 < ref_id1);

    RefId ref_id3;
    ref_id3 = ref_id0;
    REQUIRE(ref_id3 == ref_id0);

    RefId ref_id4 = std::move(ref_id3);
    REQUIRE(ref_id4 == ref_id0);

    RefId ref_id5(std::move(ref_id4));
    REQUIRE(ref_id5 == ref_id0);

    std::stringstream os;
    os << ref_id0;
    REQUIRE(os.str() == "tag_name(3)");
  }

  SECTION("tag with no name")
  {
    RefId ref_id0(3);
    REQUIRE(ref_id0.tagNumber() == 3);
    REQUIRE(ref_id0.tagName() == "3");

    RefId ref_id1(ref_id0);
    REQUIRE(ref_id1.tagNumber() == 3);
    REQUIRE(ref_id1.tagName() == "3");
    REQUIRE(ref_id0 == ref_id1);

    RefId ref_id2(2);
    REQUIRE(ref_id2 < ref_id1);

    RefId ref_id3;
    ref_id3 = ref_id0;
    REQUIRE(ref_id3 == ref_id0);

    RefId ref_id4 = std::move(ref_id3);
    REQUIRE(ref_id4 == ref_id0);

    RefId ref_id5(std::move(ref_id4));
    REQUIRE(ref_id5 == ref_id0);

    std::stringstream os;
    os << ref_id0;
    REQUIRE(os.str() == "3");
  }
}
