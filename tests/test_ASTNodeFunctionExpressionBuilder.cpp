#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeFunctionEvaluationExpressionBuilder.hpp>
#include <language/ast/ASTNodeFunctionExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/TypeDescriptor.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const double>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("builtin_t");
const auto builtin_data_type = ast_node_data_type_from<std::shared_ptr<const double>>;

#define CHECK_AST(data, expected_output)                                                                        \
  {                                                                                                             \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);                              \
    static_assert((std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view>) or                \
                  (std::is_same_v<std::decay_t<decltype(expected_output)>, std::string>));                      \
                                                                                                                \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                                  \
    auto ast = ASTBuilder::build(input);                                                                        \
                                                                                                                \
    ASTModulesImporter{*ast};                                                                                   \
    BasicAffectationRegisterFor<EmbeddedData>{ASTNodeDataType::build<ASTNodeDataType::type_id_t>("builtin_t")}; \
                                                                                                                \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                                     \
    SymbolTable& symbol_table = *ast->m_symbol_table;                                                           \
    auto [i_symbol, success]  = symbol_table.add(builtin_data_type.nameOfTypeId(), ast->begin());               \
    if (not success) {                                                                                          \
      throw UnexpectedError("cannot add '" + builtin_data_type.nameOfTypeId() + "' type for testing");          \
    }                                                                                                           \
                                                                                                                \
    i_symbol->attributes().setDataType(ASTNodeDataType::build<ASTNodeDataType::type_name_id_t>());              \
    i_symbol->attributes().setIsInitialized();                                                                  \
    i_symbol->attributes().value() = symbol_table.typeEmbedderTable().size();                                   \
    symbol_table.typeEmbedderTable().add(std::make_shared<TypeDescriptor>(builtin_data_type.nameOfTypeId()));   \
                                                                                                                \
    ASTSymbolTableBuilder{*ast};                                                                                \
    ASTNodeDataTypeBuilder{*ast};                                                                               \
                                                                                                                \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                                        \
    ASTNodeTypeCleaner<language::fct_declaration>{*ast};                                                        \
    ASTNodeExpressionBuilder{*ast};                                                                             \
                                                                                                                \
    std::stringstream ast_output;                                                                               \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::exec_type}};             \
                                                                                                                \
    REQUIRE(ast_output.str() == expected_output);                                                               \
    ast->m_symbol_table->clearValues();                                                                         \
  }

#define CHECK_AST_THROWS(data)                                                     \
  {                                                                                \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>); \
                                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                     \
    auto ast = ASTBuilder::build(input);                                           \
                                                                                   \
    ASTModulesImporter{*ast};                                                      \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                        \
                                                                                   \
    ASTSymbolTableBuilder{*ast};                                                   \
    ASTNodeDataTypeBuilder{*ast};                                                  \
                                                                                   \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                           \
    ASTNodeTypeCleaner<language::fct_declaration>{*ast};                           \
    REQUIRE_THROWS_AS(ASTNodeExpressionBuilder{*ast}, ParseError);                 \
    ast->m_symbol_table->clearValues();                                            \
  }

#define CHECK_TYPE_BUILDER_THROWS_WITH(data, error)                                \
  {                                                                                \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>); \
    static_assert(std::is_same_v<std::decay_t<decltype(error)>, std::string>);     \
                                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                     \
    auto ast = ASTBuilder::build(input);                                           \
                                                                                   \
    ASTModulesImporter{*ast};                                                      \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                        \
                                                                                   \
    ASTSymbolTableBuilder{*ast};                                                   \
    REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error);                      \
    ast->m_symbol_table->clearValues();                                            \
  }

#define CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, error)                          \
  {                                                                                \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>); \
    static_assert(std::is_same_v<std::decay_t<decltype(error)>, std::string>);     \
                                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                     \
    auto ast = ASTBuilder::build(input);                                           \
                                                                                   \
    ASTModulesImporter{*ast};                                                      \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                        \
                                                                                   \
    ASTSymbolTableBuilder{*ast};                                                   \
    ASTNodeDataTypeBuilder{*ast};                                                  \
                                                                                   \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                           \
    ASTNodeTypeCleaner<language::fct_declaration>{*ast};                           \
    REQUIRE_THROWS_WITH(ASTNodeExpressionBuilder{*ast}, error);                    \
    ast->m_symbol_table->clearValues();                                            \
  }

#ifdef __clang__
#else   // __clang__
#ifdef __GNUG__
#pragma GCC push_options
#pragma GCC optimize("O0")
#endif   // __GNUG__
#endif   // __clang__

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeFunctionExpressionBuilder", "[language]")
{
  SECTION("value or compounds of values")
  {
    SECTION("without conversion")
    {
      SECTION("return a B")
      {
        SECTION("B argument")
        {
          SECTION("B parameter")
          {
            std::string_view data = R"(
let not_v : B -> B, a -> not a;
not_v(true);
)";

            std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:not_v:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

            CHECK_AST(data, result);
          }
        }

        SECTION("N argument")
        {
          std::string_view data = R"(
let test : N -> B, n -> n<10;
test(2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:test:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z argument")
        {
          std::string_view data = R"(
let test : Z -> B, z -> z>3;
test(2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:test:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R argument")
        {
          std::string_view data = R"(
let test : R -> B, x -> x>2.3;
test(2.1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:test:NameProcessor)
     `-(language::real:2.1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("return a N")
      {
        SECTION("N argument")
        {
          std::string_view data = R"(
let test : N -> N, n -> n+2;
test(2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:test:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z argument")
        {
          std::string_view data = R"(
let absolute : Z -> N, z -> (z>0)*z -(z<=0)*z;
absolute(-2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:absolute:NameProcessor)
     `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
         `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("return a Z")
      {
        SECTION("N argument")
        {
          std::string_view data = R"(
let minus : N -> Z, n -> -n;
minus(true);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:minus:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z argument")
        {
          std::string_view data = R"(
let times_2 : Z -> Z, z -> z*2;
times_2(-2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:times_2:NameProcessor)
     `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
         `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("return a string")
      {
        SECTION("string argument")
        {
          std::string_view data = R"(
let cat : string*string -> string, (s1,s2) -> s1+s2;
cat("foo", "bar");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:cat:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::literal:"foo":ValueProcessor)
         `-(language::literal:"bar":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("B argument conversion")
        {
          std::string_view data = R"(
let cat : string*string -> string, (s1,s2) -> s1+s2;
cat("foo", true);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:cat:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::literal:"foo":ValueProcessor)
         `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("N argument conversion")
        {
          std::string_view data = R"(
let cat : string*string -> string, (s1,s2) -> s1+s2;
let n : N, n = 2;
cat("foo", n);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:cat:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::literal:"foo":ValueProcessor)
         `-(language::name:n:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z argument conversion")
        {
          std::string_view data = R"(
let cat : string*string -> string, (s1,s2) -> s1+s2;
cat("foo", -1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:cat:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::literal:"foo":ValueProcessor)
         `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
             `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R argument conversion")
        {
          std::string_view data = R"(
let cat : string*string -> string, (s1,s2) -> s1+s2;
cat("foo", 2.5e-3);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:cat:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::literal:"foo":ValueProcessor)
         `-(language::real:2.5e-3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("Return a R^1")
      {
        std::string_view data = R"(
let f : R^1 -> R^1, x -> x+x;
let x : R^1, x = 1;
f(x);
let n:N, n=1;
f(true);
f(n);
f(2);
f(1.4);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::name:x:NameProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::name:n:NameProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::real:1.4:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a R^2")
      {
        std::string_view data = R"(
let f : R^2 -> R^2, x -> x+x;
let x : R^2, x = (1,2);
f(x);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::name:x:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a R^3")
      {
        std::string_view data = R"(
let f : R^3 -> R^3, x -> x+x;
let x : R^3, x = (1,2,3);
f(x);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::name:x:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a R^1x1")
      {
        std::string_view data = R"(
let f : R^1x1 -> R^1x1, x -> x+x;
let x : R^1x1, x = 1;
f(x);
let n:N, n=1;
f(true);
f(n);
f(2);
f(1.4);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::name:x:NameProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::name:n:NameProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::real:1.4:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a R^2x2")
      {
        std::string_view data = R"(
let f : R^2x2 -> R^2x2, x -> x+x;
let x : R^2x2, x = (1,2,3,4);
f(x);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::name:x:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a R^3x3")
      {
        std::string_view data = R"(
let f : R^3x3 -> R^3x3, x -> x+x;
let x : R^3x3, x = (1,2,3,4,5,6,7,8,9);
f(x);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::name:x:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a R^1")
      {
        std::string_view data = R"(
let f : R -> R^1, x -> [x+1];
f(1);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a R^2")
      {
        std::string_view data = R"(
let f : R*R -> R^2, (x,y) -> [x,y];
f(1,2);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         `-(language::integer:2:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a R^3")
      {
        std::string_view data = R"(
let f : R*R*R -> R^3, (x,y,z) -> [x,y,z];
f(1,2,3);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         `-(language::integer:3:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a R^1x1")
      {
        std::string_view data = R"(
let f : R -> R^1x1, x -> [[x+1]];
f(1);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a R^2x2")
      {
        std::string_view data = R"(
let f : R*R*R*R -> R^2x2, (x,y,z,t) -> [[x,y],[z,t]];
f(1,2,3,4);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:4:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a R^3x3")
      {
        std::string_view data = R"(
let f : R^3*R^3*R^3 -> R^3x3, (x,y,z) -> [[x[0],x[1],x[2]],[y[0],y[1],y[2]],[z[0],z[1],z[2]]];
f([1,2,3],[4,5,6],[7,8,9]);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         |   +-(language::integer:1:ValueProcessor)
         |   +-(language::integer:2:ValueProcessor)
         |   `-(language::integer:3:ValueProcessor)
         +-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         |   +-(language::integer:4:ValueProcessor)
         |   +-(language::integer:5:ValueProcessor)
         |   `-(language::integer:6:ValueProcessor)
         `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
             +-(language::integer:7:ValueProcessor)
             +-(language::integer:8:ValueProcessor)
             `-(language::integer:9:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("return a builtin_t")
      {
        SECTION("builtin_t argument")
        {
          std::string_view data = R"(
let foo : builtin_t -> builtin_t, b -> b;
let b0 : builtin_t;
foo(b0);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:foo:NameProcessor)
     `-(language::name:b0:NameProcessor)
)";

          CHECK_AST(data, result);
        }
      }
    }

    SECTION("with return conversion")
    {
      SECTION("Return scalar -> R^1")
      {
        std::string_view data = R"(
let f : R -> R^1, x -> x+1;
f(1);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return scalar -> R^1x1")
      {
        std::string_view data = R"(
let f : R -> R^1x1, x -> x+1;
f(1);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return '0' -> R^1")
      {
        std::string_view data = R"(
let f : R -> R^1, x -> 0;
f(1);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionExpressionProcessor<TinyVector<1ul, double>, ZeroType>)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return '0' -> R^2")
      {
        std::string_view data = R"(
let f : R -> R^2, x -> 0;
f(1);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionExpressionProcessor<TinyVector<2ul, double>, ZeroType>)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return '0' -> R^3")
      {
        std::string_view data = R"(
let f : R -> R^3, x -> 0;
f(1);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionExpressionProcessor<TinyVector<3ul, double>, ZeroType>)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return '0' -> R^1x1")
      {
        std::string_view data = R"(
let f : R -> R^1x1, x -> 0;
f(1);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionExpressionProcessor<TinyMatrix<1ul, 1ul, double>, ZeroType>)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return '0' -> R^2x2")
      {
        std::string_view data = R"(
let f : R -> R^2x2, x -> 0;
f(1);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionExpressionProcessor<TinyMatrix<2ul, 2ul, double>, ZeroType>)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return '0' -> R^3x3")
      {
        std::string_view data = R"(
let f : R -> R^3x3, x -> 0;
f(1);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionExpressionProcessor<TinyMatrix<3ul, 3ul, double>, ZeroType>)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Arguments '0' -> R^1")
      {
        std::string_view data = R"(
let f : R^1 -> R^1, x -> x;
f(0);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:0:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Arguments '0' -> R^2")
      {
        std::string_view data = R"(
let f : R^2 -> R^2, x -> x;
f(0);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:0:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Arguments '0' -> R^3")
      {
        std::string_view data = R"(
let f : R^3 -> R^3, x -> x;
f(0);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:0:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Arguments '0' -> R^1x1")
      {
        std::string_view data = R"(
let f : R^1x1 -> R^1x1, x -> x;
f(0);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:0:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Arguments '0' -> R^2x2")
      {
        std::string_view data = R"(
let f : R^2x2 -> R^2x2, x -> x;
f(0);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:0:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Arguments '0' -> R^3x3")
      {
        std::string_view data = R"(
let f : R^3x3 -> R^3x3, x -> x;
f(0);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:0:ValueProcessor)
)";

        CHECK_AST(data, result);
      }
    }
  }

  SECTION("compound return type")
  {
    SECTION("Return compound with R^d")
    {
      std::string_view data = R"(
let f : R*R*R*R -> R*R^1*R^2*R^3, (x,y,z,t) -> (t, [x], [x,y], [x,y,z]);
f(1,2,3,4);
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:4:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Return compound with R^dxd")
    {
      std::string_view data = R"(
let f : R*R*R*R -> R*R^1x1*R^2x2*R^3x3, (x,y,z,t) -> (t, [[x]], [[x,y],[z,t]], [[x,y,z], [x,x,x], [t,t,t]]);
f(1,2,3,4);
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:4:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Return R^d compound with '0'")
    {
      std::string_view data = R"(
let f : R*R*R*R -> R*R^1*R^2*R^3, (x,y,z,t) -> (t, 0, 0, [x,y,z]);
f(1,2,3,4);
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:4:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Return R^dxd compound with '0'")
    {
      std::string_view data = R"(
let f : R*R*R*R -> R*R^1x1*R^2x2*R^3x3, (x,y,z,t) -> (t, 0, 0, [[x, y, z], [t, x, y], [z, t, x]]);
f(1,2,3,4);
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:4:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Arguments R^d")
    {
      std::string_view data = R"(
let f: R^3 -> R, x -> x[0]+x[1]+x[2];
f([1,2,3]);
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         `-(language::integer:3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Arguments tuple -> R^dxd")
    {
      std::string_view data = R"(
let f: R^3x3 -> R, x -> x[0,0]+x[0,1]+x[0,2];
f([[1,2,3],[4,5,6],[7,8,9]]);
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::matrix_expression:TinyMatrixExpressionProcessor<3ul, 3ul>)
         +-(language::row_expression:FakeProcessor)
         |   +-(language::integer:1:ValueProcessor)
         |   +-(language::integer:2:ValueProcessor)
         |   `-(language::integer:3:ValueProcessor)
         +-(language::row_expression:FakeProcessor)
         |   +-(language::integer:4:ValueProcessor)
         |   +-(language::integer:5:ValueProcessor)
         |   `-(language::integer:6:ValueProcessor)
         `-(language::row_expression:FakeProcessor)
             +-(language::integer:7:ValueProcessor)
             +-(language::integer:8:ValueProcessor)
             `-(language::integer:9:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Arguments compound with R^d and R^dxd")
    {
      std::string_view data = R"(
let f: R*R^3*R^2x2->R, (t,x,y) -> t*(x[0]+x[1]+x[2])*y[0,0]+y[1,1];
f(2,[1,2,3],[[2,3],[-1,1.3]]);
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         |   +-(language::integer:1:ValueProcessor)
         |   +-(language::integer:2:ValueProcessor)
         |   `-(language::integer:3:ValueProcessor)
         `-(language::matrix_expression:TinyMatrixExpressionProcessor<2ul, 2ul>)
             +-(language::row_expression:FakeProcessor)
             |   +-(language::integer:2:ValueProcessor)
             |   `-(language::integer:3:ValueProcessor)
             `-(language::row_expression:FakeProcessor)
                 +-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
                 |   `-(language::integer:1:ValueProcessor)
                 `-(language::real:1.3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("errors")
    {
      SECTION("wrong argument number")
      {
        std::string_view data = R"(
let Id : Z -> Z, z -> z;
Id(2,3);
)";

        CHECK_AST_THROWS(data);
      }

      SECTION("wrong argument number 2")
      {
        std::string_view data = R"(
let sum : R*R -> R, (x,y) -> x+y;
sum(2);
)";

        CHECK_AST_THROWS(data);
      }

      SECTION("invalid return implicit conversion")
      {
        SECTION("string -> R")
        {
          std::string_view data = R"(
let bad_conv : string -> R, s -> s;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: string -> R"});
        }

        SECTION("R -> B")
        {
          std::string_view data = R"(
let bad_B : R -> B, x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> B"});
        }

        SECTION("R -> N")
        {
          std::string_view data = R"(
let next : R -> N, x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> N"});
        }

        SECTION("R -> Z")
        {
          std::string_view data = R"(
let prev : R -> Z, x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> Z"});
        }

        SECTION("N -> B")
        {
          std::string_view data = R"(
let bad_B : N -> B, n -> n;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> B"});
        }

        SECTION("Z -> B")
        {
          std::string_view data = R"(
let bad_B : Z -> B, n -> n;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> B"});
        }
      }

      SECTION("invalid argument implicit conversion")
      {
        SECTION("N -> B")
        {
          std::string_view data = R"(
let negate : B -> B, b -> not b;
let n : N, n = 2;
negate(n);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> B"});
        }

        SECTION("Z -> B")
        {
          std::string_view data = R"(
let negate : B -> B, b -> not b;
negate(3-4);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> B"});
        }

        SECTION("R -> B")
        {
          std::string_view data = R"(
let negate : B -> B, b -> not b;
negate(3.24);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> B"});
        }

        SECTION("R -> N")
        {
          std::string_view data = R"(
let next : N -> N, n -> n+1;
next(3.24);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> N"});
        }

        SECTION("R -> Z")
        {
          std::string_view data = R"(
let prev : Z -> Z, z -> z-1;
prev(3 + .24);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> Z"});
        }

        SECTION("B -> R^2")
        {
          std::string_view data = R"(
let f : R^2 -> R^2, x -> x;
f(true);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: B -> R^2"});
        }

        SECTION("N -> R^2")
        {
          std::string_view data = R"(
let f : R^2 -> R^2, x -> x;
let n : N, n = 2;
f(n);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> R^2"});
        }

        SECTION("Z -> R^2")
        {
          std::string_view data = R"(
let f : R^2 -> R^2, x -> x;
f(-2);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> R^2"});
        }

        SECTION("R -> R^2")
        {
          std::string_view data = R"(
let f : R^2 -> R^2, x -> x;
f(1.3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> R^2"});
        }

        SECTION("B -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R^3, x -> x;
f(true);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: B -> R^3"});
        }

        SECTION("N -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R^3, x -> x;
let n : N, n = 2;
f(n);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> R^3"});
        }

        SECTION("Z -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R^3, x -> x;
f(-2);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> R^3"});
        }

        SECTION("R -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R^3, x -> x;
f(1.3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> R^3"});
        }

        SECTION("B -> R^2x2")
        {
          std::string_view data = R"(
let f : R^2x2 -> R^2x2, x -> x;
f(true);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: B -> R^2x2"});
        }

        SECTION("N -> R^2x2")
        {
          std::string_view data = R"(
let f : R^2x2 -> R^2x2, x -> x;
let n : N, n = 2;
f(n);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> R^2x2"});
        }

        SECTION("Z -> R^2x2")
        {
          std::string_view data = R"(
let f : R^2x2 -> R^2x2, x -> x;
f(-2);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> R^2x2"});
        }

        SECTION("R -> R^2x2")
        {
          std::string_view data = R"(
let f : R^2x2 -> R^2x2, x -> x;
f(1.3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> R^2x2"});
        }

        SECTION("B -> R^3x3")
        {
          std::string_view data = R"(
let f : R^3x3 -> R^3x3, x -> x;
f(true);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: B -> R^3x3"});
        }

        SECTION("N -> R^3x3")
        {
          std::string_view data = R"(
let f : R^3x3 -> R^3x3, x -> x;
let n : N, n = 2;
f(n);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> R^3x3"});
        }

        SECTION("Z -> R^3x3")
        {
          std::string_view data = R"(
let f : R^3x3 -> R^3x3, x -> x;
f(-2);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> R^3x3"});
        }

        SECTION("R -> R^3x3")
        {
          std::string_view data = R"(
let f : R^3x3 -> R^3x3, x -> x;
f(1.3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> R^3x3"});
        }
      }

      SECTION("arguments invalid tuple -> R^d conversion")
      {
        SECTION("tuple[2] -> R^2")
        {
          std::string_view data = R"(
let f : R^2 -> R, x->x[0];
f((1,2));
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"cannot convert list to R^2"});
        }

        SECTION("tuple[3] -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R, x->x[0];
f((1,2,3));
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"cannot convert list to R^3"});
        }

        SECTION("compound -> R^2")
        {
          std::string_view data = R"(
let f : R*R^2 -> R, (t,x)->x[0];
f(1,(1,2));
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"cannot convert list to R^2"});
        }

        SECTION("compound -> R^3")
        {
          std::string_view data = R"(
let f : R^3*R^2 -> R, (x,y)->x[0]*y[1];
f((1,2,3),[3,4]);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"cannot convert list to R^3"});
        }

        SECTION("list instead of tuple -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R, x -> x[0]*x[1];
f(1,2,3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"bad number of arguments: expecting 1, provided 3"});
        }

        SECTION("list instead of tuple -> R^3*R^2")
        {
          std::string_view data = R"(
let f : R^3*R^2 -> R, (x,y) -> x[0]*x[1]-y[0];
f([1,2,3],2,3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"bad number of arguments: expecting 2, provided 3"});
        }
      }

      SECTION("non pure function")
      {
        SECTION("argument modification")
        {
          SECTION("++ argument")
          {
            std::string_view data = R"(
let non_pure : N -> N, x -> 3 * ++x;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("argument ++")
          {
            std::string_view data = R"(
let non_pure : N -> N, x -> 1 + x ++;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("-- argument")
          {
            std::string_view data = R"(
let non_pure : Z -> Z, x -> 3 * --x;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("argument --")
          {
            std::string_view data = R"(
let non_pure : Z -> Z, x -> 1 + x --;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }
        }

        SECTION("outer variable modification")
        {
          SECTION("++ outer variable")
          {
            std::string_view data = R"(
let a:N, a = 4;
let non_pure : Z -> Z, x -> x * ++a;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("outer variable ++")
          {
            std::string_view data = R"(
let a:N, a = 4;
let non_pure : N -> N, x -> x + a++;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("-- outer variable")
          {
            std::string_view data = R"(
let a:Z, a = 4;
let non_pure : Z -> Z, x -> x * --a;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("outer variable --")
          {
            std::string_view data = R"(
let a:Z, a = 4;
let non_pure : Z -> Z, x -> x + a--;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }
        }
      }
    }
  }

  SECTION("tuples")
  {
    SECTION("from list of values")
    {
      SECTION("return a (B)")
      {
        SECTION("B argument")
        {
          SECTION("B parameter")
          {
            std::string_view data = R"(
let not_v : B -> (B), a -> (not a, a, true);
not_v(true);
)";

            std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:not_v:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

            CHECK_AST(data, result);
          }
        }

        SECTION("N argument")
        {
          std::string_view data = R"(
let test : N -> (B), n -> (n<10, n>1, false);
test(2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:test:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z argument")
        {
          std::string_view data = R"(
let test : Z -> (B), z -> (z>3, z<5);
test(2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:test:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R argument")
        {
          std::string_view data = R"(
let test : R -> (B), x -> (x>2.3, 2*x>3);
test(2.1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:test:NameProcessor)
     `-(language::real:2.1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("return a (N)")
      {
        SECTION("N argument")
        {
          std::string_view data = R"(
let test : N -> (N), n -> (n+2, n, true);
test(2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:test:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z argument")
        {
          std::string_view data = R"(
let absolute : Z -> (N), z -> ((z>0)*z -(z<=0)*z, 3, true);
absolute(-2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:absolute:NameProcessor)
     `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
         `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("return a (Z)")
      {
        SECTION("B argument")
        {
          std::string_view data = R"(
let minus : B -> (Z), b -> (-b, b, true);
minus(true);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:minus:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("N argument")
        {
          std::string_view data = R"(
let minus : N -> (Z), n -> (-n, n, true);
minus(true);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:minus:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z argument")
        {
          std::string_view data = R"(
let times_2 : Z -> (Z), z -> (z*2, 2, false);
times_2(-2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:times_2:NameProcessor)
     `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
         `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("return a (R)")
      {
        SECTION("B argument")
        {
          std::string_view data = R"(
let minus : B -> (Z), b -> (-b, b, true);
minus(true);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:minus:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("N argument")
        {
          std::string_view data = R"(
let minus : N -> (R), n -> (-n, n, true);
minus(true);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:minus:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z argument")
        {
          std::string_view data = R"(
let times_2 : Z -> (R), z -> (z*2, 2, false);
times_2(-2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:times_2:NameProcessor)
     `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
         `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R argument")
        {
          std::string_view data = R"(
let times_2 : R -> (R), r -> (r*2, 2, false);
times_2(-2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:times_2:NameProcessor)
     `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
         `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("return a (string)")
      {
        SECTION("string argument")
        {
          std::string_view data = R"(
let cat : string*string -> (string), (s1,s2) -> (s1+s2, true, -3, 2.4);
cat("foo", "bar");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:cat:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::literal:"foo":ValueProcessor)
         `-(language::literal:"bar":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("B argument conversion")
        {
          std::string_view data = R"(
let cat : string*string -> (string), (s1,s2) -> (s1+s2, true, 2, 2.5);
cat("foo", true);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:cat:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::literal:"foo":ValueProcessor)
         `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("N argument conversion")
        {
          std::string_view data = R"(
let cat : string*string -> (string), (s1,s2) -> (s1+s2, 4, [1.2], [2,2]);
let n : N, n = 2;
cat("foo", n);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:cat:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::literal:"foo":ValueProcessor)
         `-(language::name:n:NameProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Z argument conversion")
        {
          std::string_view data = R"(
let cat : string*string -> (string), (s1,s2) -> (s1+s2, s2);
cat("foo", -1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:cat:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::literal:"foo":ValueProcessor)
         `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
             `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("R argument conversion")
        {
          std::string_view data   = R"(
let cat : string*string -> (string), (s1,s2) -> (s1+s2, 1.3);
cat("foo", 2.5e-3);
)";
          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:cat:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::literal:"foo":ValueProcessor)
         `-(language::real:2.5e-3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("Return a (R^1)")
      {
        std::string_view data = R"(
let f : R^1 -> (R^1), x -> (x+x, 2.3, [1.2]);
let x : R^1, x = 1;
f(x);
let n:N, n=1;
f(true);
f(n);
f(2);
f(1.4);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::name:x:NameProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::name:n:NameProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::real:1.4:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a (R^2)")
      {
        std::string_view data = R"(
let f : R^2 -> (R^2), x -> (x+x, 0, [1, 3]);
let x : R^2, x = (1,2);
f(x);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::name:x:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a (R^3)")
      {
        std::string_view data = R"(
let f : R^3 -> (R^3), x -> (x+x, 2*x, [1,2,3], 0);
let x : R^3, x = (1,2,3);
f(x);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::name:x:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a (R^1x1)")
      {
        std::string_view data = R"(
let f : R^1x1 -> (R^1x1), x -> (x+x, [[1]], 3, 0);
let x : R^1x1, x = 1;
f(x);
let n:N, n=1;
f(true);
f(n);
f(2);
f(1.4);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::name:x:NameProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::name:n:NameProcessor)
 +-(language::function_evaluation:FunctionProcessor)
 |   +-(language::name:f:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::real:1.4:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a (R^2x2)")
      {
        std::string_view data = R"(
let f : R^2x2 -> (R^2x2), x -> (x+x, 0, [[1,2],[3,4]]);
let x : R^2x2, x = (1,2,3,4);
f(x);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::name:x:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a (R^3x3)")
      {
        std::string_view data = R"(
let f : R^3x3 -> (R^3x3), x -> (x+x, 0, [[1,2,3],[4,5,6],[7,8,9]]);
let x : R^3x3, x = (1,2,3,4,5,6,7,8,9);
f(x);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::name:x:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return a (builtin_t)")
      {
        std::string_view data = R"(
let foo : builtin_t -> (builtin_t), b -> (b, b, b);
let b0 : builtin_t;
foo(b0);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:foo:NameProcessor)
     `-(language::name:b0:NameProcessor)
)";

        CHECK_AST(data, result);
      }
    }

    SECTION("from single value")
    {
      SECTION("without conversion")
      {
        SECTION("Return a (B)")
        {
          std::string_view data = R"(
let f : B -> (B), b -> b;
f(true);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (N)")
        {
          std::string_view data = R"(
let f : N -> (N), n -> n;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (Z)")
        {
          std::string_view data = R"(
let f : Z -> (Z), x -> x+1;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R)")
        {
          std::string_view data = R"(
let f : R -> (R), x -> x+1;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (string)")
        {
          std::string_view data = R"(
let f : string -> (string), s -> s;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^1)")
        {
          std::string_view data = R"(
let f : R -> (R^1), x -> [x+1];
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^2)")
        {
          std::string_view data = R"(
let f : R*R -> (R^2), (x,y) -> [x,y];
f(1,2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^3)")
        {
          std::string_view data = R"(
let f : R*R*R -> (R^3), (x,y,z) -> [x,y,z];
f(1,2,3);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         `-(language::integer:3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^1x1)")
        {
          std::string_view data = R"(
let f : R -> (R^1x1), x -> [[x+1]];
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^2x2)")
        {
          std::string_view data = R"(
let f : R*R*R*R -> (R^2x2), (x,y,z,t) -> [[x,y],[z,t]];
f(1,2,3,4);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:4:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^3x3)")
        {
          std::string_view data = R"(
let f : R^3*R^3*R^3 -> (R^3x3), (x,y,z) -> [[x[0],x[1],x[2]],[y[0],y[1],y[2]],[z[0],z[1],z[2]]];
f([1,2,3],[4,5,6],[7,8,9]);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         |   +-(language::integer:1:ValueProcessor)
         |   +-(language::integer:2:ValueProcessor)
         |   `-(language::integer:3:ValueProcessor)
         +-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         |   +-(language::integer:4:ValueProcessor)
         |   +-(language::integer:5:ValueProcessor)
         |   `-(language::integer:6:ValueProcessor)
         `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
             +-(language::integer:7:ValueProcessor)
             +-(language::integer:8:ValueProcessor)
             `-(language::integer:9:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("return a (builtin_t)")
        {
          std::string_view data = R"(
let foo : builtin_t -> (builtin_t), b -> b;
let b0 : builtin_t;
foo(b0);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:foo:NameProcessor)
     `-(language::name:b0:NameProcessor)
)";

          CHECK_AST(data, result);
        }
      }

      SECTION("with conversion")
      {
        SECTION("Return a B -> (R^1)")
        {
          std::string_view data = R"(
let f : B -> (R^1), b -> b;
f(true);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a N -> (R^1)")
        {
          std::string_view data = R"(
let f : N -> (R^1), n -> n;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a Z -> (R^1)")
        {
          std::string_view data = R"(
let f : Z -> (R^1), x -> x+1;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a R -> (R^1)")
        {
          std::string_view data = R"(
let f : R -> (R^1), x -> x+1;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a B -> (R^1x1)")
        {
          std::string_view data = R"(
let f : B -> (R^1x1), b -> b;
f(true);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a N -> (R^1x1)")
        {
          std::string_view data = R"(
let f : N -> (R^1x1), n -> n;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a Z -> (R^1x1)")
        {
          std::string_view data = R"(
let f : Z -> (R^1x1), x -> x+1;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a R -> (R^1x1)")
        {
          std::string_view data = R"(
let f : R -> (R^1x1), x -> x+1;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return '0' -> (R^1)")
        {
          std::string_view data = R"(
let f : R -> (R^1), x -> 0;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return '0' -> (R^2)")
        {
          std::string_view data = R"(
let f : R -> (R^2), x -> 0;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return '0' -> (R^3)")
        {
          std::string_view data = R"(
let f : R -> (R^3), x -> 0;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return '0' -> (R^1x1)")
        {
          std::string_view data = R"(
let f : R -> (R^1x1), x -> 0;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return '0' -> (R^2x2)")
        {
          std::string_view data = R"(
let f : R -> (R^2x2), x -> 0;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return '0' -> (R^3x3)")
        {
          std::string_view data = R"(
let f : R -> (R^3x3), x -> 0;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }
      }
    }

    SECTION("from tuple value")
    {
      SECTION("without conversion")
      {
        SECTION("Return a (B)")
        {
          std::string_view data = R"(
let b:(B), b = (true, false);
let f : Z -> (B), z -> b;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (B) -> (N)")
        {
          std::string_view data = R"(
let b : (B), b = (true, false);
let f : N -> (N), n -> b;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (N)")
        {
          std::string_view data = R"(
let n : (N), n = (1, 2, 3);
let f : N -> (N), i -> n;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (Z) -> (N)")
        {
          std::string_view data = R"(
let z : (Z), z = (1, 2, 3);
let f : N -> (Z), i -> z;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (B) -> (Z)")
        {
          std::string_view data = R"(
let b : (B), b = (true, true);
let f : Z -> (Z), x -> b;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (N) -> (Z)")
        {
          std::string_view data = R"(
let n : (N), n = (1, 2, 3);
let f : Z -> (Z), x -> n;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (Z)")
        {
          std::string_view data = R"(
let z : (Z), z = (1, 2, 3);
let f : Z -> (Z), x -> z;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (B) -> (R)")
        {
          std::string_view data = R"(
let b:(B), b = (true, false);
let f : R -> (R), x -> b;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (N) -> (R)")
        {
          std::string_view data = R"(
let n:(N), n = (1, 2, 3);
let f : R -> (R), x -> n;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (Z) -> (R)")
        {
          std::string_view data = R"(
let z:(Z), z = (1, -2, 3);
let f : R -> (R), x -> z;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R)")
        {
          std::string_view data = R"(
let r:(R), r = (1.1, -2, 3.2);
let f : R -> (R), x -> r;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (B) -> (string)")
        {
          std::string_view data = R"(
let b : (B), b = (true, false);
let f : string -> (string), t -> b;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (N) -> (string)")
        {
          std::string_view data = R"(
let n : (N), n = (1, 2, 3);
let f : string -> (string), t -> n;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (Z) -> (string)")
        {
          std::string_view data = R"(
let z : (Z), z = (1, -2, 3);
let f : string -> (string), t -> z;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R) -> (string)")
        {
          std::string_view data = R"(
let r : (R), r = (1, -2, 3);
let f : string -> (string), t -> r;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^1) -> (string)")
        {
          std::string_view data = R"(
let r : (R^1), r = (1, [-2], 3);
let f : string -> (string), t -> r;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^2) -> (string)")
        {
          std::string_view data = R"(
let r : (R^2), r = ([1, 2], [-2, 3], [3, -4]);
let f : string -> (string), t -> r;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^3) -> (string)")
        {
          std::string_view data = R"(
let r : (R^3), r = ([1, 2, 3], [0, -2, 3], [3, -4, 2]);
let f : string -> (string), t -> r;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^1x1) -> (string)")
        {
          std::string_view data = R"(
let r : (R^1x1), r = (1, [[-2]], 3);
let f : string -> (string), t -> r;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^2x2) -> (string)")
        {
          std::string_view data = R"(
let r : (R^2x2), r = ([[1, 2], [-2, 3]], [[3, -4], [2, 1]]);
let f : string -> (string), t -> r;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^3x3) -> (string)")
        {
          std::string_view data = R"(
let r : (R^3x3), r = ([[1, 2, 3], [0, -2, 3], [3, -4, 2]], 0);
let f : string -> (string), t -> r;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (string)")
        {
          std::string_view data = R"(
let s : (string), s = ("foo", "bar");
let f : string -> (string), t -> s;
f("foo");
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (B) -> (R^1)")
        {
          std::string_view data = R"(
let b : (B), b = (true, false);
let f : R -> (R^1), x -> b;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (N) -> (R^1)")
        {
          std::string_view data = R"(
let n : (N), n = (1, 2, 3);
let f : R -> (R^1), x -> n;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (Z) -> (R^1)")
        {
          std::string_view data = R"(
let z : (Z), z = (1, 2, 3);
let f : R -> (R^1), x -> z;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R) -> (R^1)")
        {
          std::string_view data = R"(
let r : (R), r = (1, 2, 3);
let f : R -> (R^1), x -> r;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^1)")
        {
          std::string_view data = R"(
let r1 : (R^1), r1 = (1, 2, 3);
let f : R -> (R^1), x -> r1;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^2)")
        {
          std::string_view data = R"(
let r2: (R^2), r2 = ([1, 2], [3, 4], 0);
let f : R*R -> (R^2), (x,y) -> r2;
f(1,2);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         `-(language::integer:2:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^3)")
        {
          std::string_view data = R"(
let r3: (R^3), r3 = ([1, 2, 3], [1, 3, 4], 0);
let f : R*R*R -> (R^3), (x,y,z) -> r3;
f(1,2,3);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         `-(language::integer:3:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (B) -> (R^1x1)")
        {
          std::string_view data = R"(
let b: (B), b = (false, true);
let f : R -> (R^1x1), x -> b;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (N) -> (R^1x1)")
        {
          std::string_view data = R"(
let n: (N), n = (1, 2, 3);
let f : R -> (R^1x1), x -> n;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (Z) -> (R^1x1)")
        {
          std::string_view data = R"(
let z: (Z), z = (1, -2, 3);
let f : R -> (R^1x1), x -> z;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R) -> (R^1x1)")
        {
          std::string_view data = R"(
let r: (R), r = (1, -2, 3);
let f : R -> (R^1x1), x -> r;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^1x1)")
        {
          std::string_view data = R"(
let r1: (R^1x1), r1 = (1, -2, 3);
let f : R -> (R^1x1), x -> r1;
f(1);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^2x2)")
        {
          std::string_view data = R"(
let r2: (R^2x2), r2 = ([1, -2], [3, 2], 0);
let f : R*R*R*R -> (R^2x2), (x,y,z,t) -> r2;
f(1,2,3,4);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:4:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("Return a (R^3x3)")
        {
          std::string_view data = R"(
let r3: (R^3x3), r3 = ([1, -2, 2], [3, 1, 2], 0);
let f : R^3*R^3*R^3 -> (R^3x3), (x,y,z) -> r3;
f([1,2,3],[4,5,6],[7,8,9]);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         |   +-(language::integer:1:ValueProcessor)
         |   +-(language::integer:2:ValueProcessor)
         |   `-(language::integer:3:ValueProcessor)
         +-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         |   +-(language::integer:4:ValueProcessor)
         |   +-(language::integer:5:ValueProcessor)
         |   `-(language::integer:6:ValueProcessor)
         `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
             +-(language::integer:7:ValueProcessor)
             +-(language::integer:8:ValueProcessor)
             `-(language::integer:9:ValueProcessor)
)";

          CHECK_AST(data, result);
        }

        SECTION("return a (builtin_t)")
        {
          std::string_view data = R"(
let b0 : builtin_t;
let bt : (builtin_t), bt = (b0, b0, b0);
let foo : builtin_t -> (builtin_t), b -> bt;
foo(b0);
)";

          std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:foo:NameProcessor)
     `-(language::name:b0:NameProcessor)
)";

          CHECK_AST(data, result);
        }
      }
    }

    SECTION("Return tuple in compound")
    {
      SECTION("Return (R^d) in compound")
      {
        std::string_view data = R"(
let b0 : builtin_t;
let f : R*R*builtin_t*R*R -> R*R^1*(R^2)*(builtin_t)*(R^3), (x,y,b,z,t) -> (t, [x], [x,y], (b,b), ([x,y,z], 0));
f(1,2,b0,3,4);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::name:b0:NameProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:4:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Return (R^dxd) in compound")
      {
        std::string_view data = R"(
let f : R*R*R*R -> R*R^1x1*(R^2x2)*(R^3x3), (x,y,z,t) -> (t, [[x]], (0,[[x,y],[z,t]]), ([[x,y,z], [x,x,x], [t,t,t]], 0));
f(1,2,3,4);
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
         +-(language::integer:1:ValueProcessor)
         +-(language::integer:2:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:4:ValueProcessor)
)";

        CHECK_AST(data, result);
      }
    }

    SECTION("errors")
    {
      SECTION("wrong argument number")
      {
        std::string_view data = R"(
let Id : Z -> Z, z -> z;
Id(2,3);
)";

        CHECK_AST_THROWS(data);
      }

      SECTION("wrong argument number 2")
      {
        std::string_view data = R"(
let sum : R*R -> R, (x,y) -> x+y;
sum(2);
)";

        CHECK_AST_THROWS(data);
      }

      SECTION("tuple as domain")
      {
        SECTION("basic type string")
        {
          std::string_view data = R"(
let bad_tuple : (string) -> N, s -> 2;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"cannot use tuple (string) as a domain for user functions"});
        }

        SECTION("basic type R")
        {
          std::string_view data = R"(
let bad_tuple : (R) -> R, x -> 2;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"cannot use tuple (R) as a domain for user functions"});
        }
      }

      SECTION("invalid return implicit conversion")
      {
        SECTION("string -> R")
        {
          std::string_view data = R"(
let bad_conv : string -> R, s -> s;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: string -> R"});
        }

        SECTION("string -> R")
        {
          std::string_view data = R"(
let bad_conv : R -> R, x -> "foo";
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: string -> R"});
        }

        SECTION("string -> (R)")
        {
          std::string_view data = R"(
let bad_conv : R -> (R), x -> (x, "bar");
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: string -> R"});
        }

        SECTION("string -> (R) 2")
        {
          std::string_view data = R"(
let bad_conv : R -> (R), x -> "bar";
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: string -> R"});
        }

        SECTION("R -> B")
        {
          std::string_view data = R"(
let bad_B : R -> B, x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> B"});
        }

        SECTION("R -> (B)")
        {
          std::string_view data = R"(
let bad_B : R -> (B), x -> (true, x, false);
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> B"});
        }

        SECTION("R -> (B) 2")
        {
          std::string_view data = R"(
let bad_B : R -> (B), x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> B"});
        }

        SECTION("R -> N")
        {
          std::string_view data = R"(
let next : R -> N, x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> N"});
        }

        SECTION("R -> (N)")
        {
          std::string_view data = R"(
let next : R -> (N), x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> N"});
        }

        SECTION("R -> (N) 2")
        {
          std::string_view data = R"(
let next : R -> (N), x -> (6, 2, x);
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> N"});
        }

        SECTION("R -> Z")
        {
          std::string_view data = R"(
let prev : R -> Z, x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> Z"});
        }

        SECTION("R -> (Z)")
        {
          std::string_view data = R"(
let prev : R -> (Z), x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> Z"});
        }

        SECTION("R -> (Z) 2")
        {
          std::string_view data = R"(
let prev : R -> (Z), x -> (x, 2, -3);
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> Z"});
        }

        SECTION("N -> B")
        {
          std::string_view data = R"(
let bad_B : N -> B, n -> n;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> B"});
        }

        SECTION("N -> (B)")
        {
          std::string_view data = R"(
let bad_B : N -> (B), n -> n;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> B"});
        }

        SECTION("N -> (B)")
        {
          std::string_view data = R"(
let bad_B : N -> (B), n -> (true, n);
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> B"});
        }

        SECTION("Z -> B")
        {
          std::string_view data = R"(
let bad_B : Z -> B, n -> n;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> B"});
        }

        SECTION("Z -> (B)")
        {
          std::string_view data = R"(
let bad_B : Z -> (B), n -> n;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> B"});
        }

        SECTION("Z -> (B)")
        {
          std::string_view data = R"(
let bad_B : Z -> (B), n -> (true, n, true);
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> B"});
        }

        SECTION("Z -> R^2")
        {
          std::string_view data = R"(
let bad_B : Z -> R^2, n -> 1;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> R^2"});
        }

        SECTION("Z -> (R^2)")
        {
          std::string_view data = R"(
let bad_B : Z -> (R^2), n -> (n, [2,1]);
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> R^2"});
        }

        SECTION("R^1 -> R^2")
        {
          std::string_view data = R"(
let bad_B : R^1 -> R^2, x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R^1 -> R^2"});
        }

        SECTION("R^1 -> (R^2)")
        {
          std::string_view data = R"(
let bad_B : R^1 -> (R^2), x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R^1 -> R^2"});
        }

        SECTION("R^1 -> (R^2)")
        {
          std::string_view data = R"(
let bad_B : R^1 -> (R^2), x -> ([1,2], x);
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R^1 -> R^2"});
        }

        SECTION("Z -> (R^3)")
        {
          std::string_view data = R"(
let bad_B : Z -> (R^3), n -> (n, [2,1,2]);
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> R^3"});
        }

        SECTION("R^1 -> R^3")
        {
          std::string_view data = R"(
let bad_B : R^1 -> R^3, x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R^1 -> R^3"});
        }

        SECTION("R^1 -> (R^3)")
        {
          std::string_view data = R"(
let bad_B : R^1 -> (R^3), x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R^1 -> R^3"});
        }

        SECTION("R^1 -> (R^3)")
        {
          std::string_view data = R"(
let bad_B : R^1 -> (R^3), x -> ([1,2,2], x);
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R^1 -> R^3"});
        }

        SECTION("R^2 -> R^3")
        {
          std::string_view data = R"(
let bad_B : R^2 -> R^3, x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R^2 -> R^3"});
        }

        SECTION("R^2 -> (R^3)")
        {
          std::string_view data = R"(
let bad_B : R^2 -> (R^3), x -> x;
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R^2 -> R^3"});
        }

        SECTION("R^2 -> (R^3)")
        {
          std::string_view data = R"(
let bad_B : R^2 -> (R^3), x -> ([1,2,5], x);
)";

          CHECK_TYPE_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R^2 -> R^3"});
        }
      }

      SECTION("invalid argument implicit conversion")
      {
        SECTION("N -> B")
        {
          std::string_view data = R"(
let negate : B -> B, b -> not b;
let n : N, n = 2;
negate(n);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> B"});
        }

        SECTION("Z -> B")
        {
          std::string_view data = R"(
let negate : B -> B, b -> not b;
negate(3-4);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> B"});
        }

        SECTION("R -> B")
        {
          std::string_view data = R"(
let negate : B -> B, b -> not b;
negate(3.24);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> B"});
        }

        SECTION("R -> N")
        {
          std::string_view data = R"(
let next : N -> N, n -> n+1;
next(3.24);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> N"});
        }

        SECTION("R -> Z")
        {
          std::string_view data = R"(
let prev : Z -> Z, z -> z-1;
prev(3 + .24);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> Z"});
        }

        SECTION("B -> R^2")
        {
          std::string_view data = R"(
let f : R^2 -> R^2, x -> x;
f(true);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: B -> R^2"});
        }

        SECTION("N -> R^2")
        {
          std::string_view data = R"(
let f : R^2 -> R^2, x -> x;
let n : N, n = 2;
f(n);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> R^2"});
        }

        SECTION("Z -> R^2")
        {
          std::string_view data = R"(
let f : R^2 -> R^2, x -> x;
f(-2);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> R^2"});
        }

        SECTION("R -> R^2")
        {
          std::string_view data = R"(
let f : R^2 -> R^2, x -> x;
f(1.3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> R^2"});
        }

        SECTION("B -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R^3, x -> x;
f(true);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: B -> R^3"});
        }

        SECTION("N -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R^3, x -> x;
let n : N, n = 2;
f(n);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> R^3"});
        }

        SECTION("Z -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R^3, x -> x;
f(-2);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> R^3"});
        }

        SECTION("R -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R^3, x -> x;
f(1.3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> R^3"});
        }

        SECTION("B -> R^2x2")
        {
          std::string_view data = R"(
let f : R^2x2 -> R^2x2, x -> x;
f(true);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: B -> R^2x2"});
        }

        SECTION("N -> R^2x2")
        {
          std::string_view data = R"(
let f : R^2x2 -> R^2x2, x -> x;
let n : N, n = 2;
f(n);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> R^2x2"});
        }

        SECTION("Z -> R^2x2")
        {
          std::string_view data = R"(
let f : R^2x2 -> R^2x2, x -> x;
f(-2);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> R^2x2"});
        }

        SECTION("R -> R^2x2")
        {
          std::string_view data = R"(
let f : R^2x2 -> R^2x2, x -> x;
f(1.3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> R^2x2"});
        }

        SECTION("B -> R^3x3")
        {
          std::string_view data = R"(
let f : R^3x3 -> R^3x3, x -> x;
f(true);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: B -> R^3x3"});
        }

        SECTION("N -> R^3x3")
        {
          std::string_view data = R"(
let f : R^3x3 -> R^3x3, x -> x;
let n : N, n = 2;
f(n);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: N -> R^3x3"});
        }

        SECTION("Z -> R^3x3")
        {
          std::string_view data = R"(
let f : R^3x3 -> R^3x3, x -> x;
f(-2);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: Z -> R^3x3"});
        }

        SECTION("R -> R^3x3")
        {
          std::string_view data = R"(
let f : R^3x3 -> R^3x3, x -> x;
f(1.3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> R^3x3"});
        }
      }

      SECTION("arguments invalid tuple -> R^d conversion")
      {
        SECTION("tuple[2] -> R^2")
        {
          std::string_view data = R"(
let f : R^2 -> R, x->x[0];
f((1,2));
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"cannot convert list to R^2"});
        }

        SECTION("tuple[3] -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R, x->x[0];
f((1,2,3));
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"cannot convert list to R^3"});
        }

        SECTION("compound -> R^2")
        {
          std::string_view data = R"(
let f : R*R^2 -> R, (t,x)->x[0];
f(1,(1,2));
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"cannot convert list to R^2"});
        }

        SECTION("compound -> R^3")
        {
          std::string_view data = R"(
let f : R^3*R^2 -> R, (x,y)->x[0]*y[1];
f((1,2,3),[3,4]);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"cannot convert list to R^3"});
        }

        SECTION("list instead of tuple -> R^3")
        {
          std::string_view data = R"(
let f : R^3 -> R, x -> x[0]*x[1];
f(1,2,3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"bad number of arguments: expecting 1, provided 3"});
        }

        SECTION("list instead of tuple -> R^3*R^2")
        {
          std::string_view data = R"(
let f : R^3*R^2 -> R, (x,y) -> x[0]*x[1]-y[0];
f([1,2,3],2,3);
)";

          CHECK_EXPRESSION_BUILDER_THROWS_WITH(data, std::string{"bad number of arguments: expecting 2, provided 3"});
        }
      }

      SECTION("non pure function")
      {
        SECTION("argument modification")
        {
          SECTION("++ argument")
          {
            std::string_view data = R"(
let non_pure : N -> N, x -> 3 * ++x;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("argument ++")
          {
            std::string_view data = R"(
let non_pure : N -> N, x -> 1 + x ++;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("-- argument")
          {
            std::string_view data = R"(
let non_pure : Z -> Z, x -> 3 * --x;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("argument --")
          {
            std::string_view data = R"(
let non_pure : Z -> Z, x -> 1 + x --;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }
        }

        SECTION("outer variable modification")
        {
          SECTION("++ outer variable")
          {
            std::string_view data = R"(
let a:N, a = 4;
let non_pure : Z -> Z, x -> x * ++a;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("outer variable ++")
          {
            std::string_view data = R"(
let a:N, a = 4;
let non_pure : N -> N, x -> x + a++;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("-- outer variable")
          {
            std::string_view data = R"(
let a:Z, a = 4;
let non_pure : Z -> Z, x -> x * --a;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }

          SECTION("outer variable --")
          {
            std::string_view data = R"(
let a:Z, a = 4;
let non_pure : Z -> Z, x -> x + a--;
)";

            CHECK_EXPRESSION_BUILDER_THROWS_WITH(data,
                                                 std::string{
                                                   "invalid function definition. Function data must be constant!"});
          }
        }
      }
    }
  }

  SECTION("empty functions")
  {
    SECTION("void -> value")
    {
      std::string_view data = R"(
let f:void -> R, void  -> 2.3;

f();
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("void -> compound")
    {
      std::string_view data = R"(
let f:void -> R*(Z)*R^2, void  -> (2.3, (1,2,3), [1.2, 2.3]);

f();
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::function_evaluation:FunctionProcessor)
     +-(language::name:f:NameProcessor)
     `-(language::function_argument_list:ASTNodeExpressionListProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("errors")
    {
      SECTION("void as a parameter")
      {
        std::string_view data = R"(
let g:R -> R, void -> 2;
)";

        std::string error = "unexpected 'void' keyword";

        TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
        auto ast = ASTBuilder::build(input);

        ASTModulesImporter{*ast};
        ASTNodeTypeCleaner<language::import_instruction>{*ast};

        ASTSymbolTableBuilder{*ast};

        REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error);
        ast->m_symbol_table->clearValues();
      }

      SECTION("void as a parameter")
      {
        std::string_view data = R"(
let g:void -> R, x -> 2;
)";

        std::string error = "expecting 'void' keyword";

        TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
        auto ast = ASTBuilder::build(input);

        ASTModulesImporter{*ast};
        ASTNodeTypeCleaner<language::import_instruction>{*ast};

        ASTSymbolTableBuilder{*ast};

        REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error);
        ast->m_symbol_table->clearValues();
      }

      SECTION("void in compound domain")
      {
        std::string_view data = R"(
let h:R*void -> R, (x,void) -> 2.5;
)";

        std::string error = "parse error, expecting type specifier";

        TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};

        REQUIRE_THROWS_WITH(ASTBuilder::build(input), error);
      }

      SECTION("void in compound codomain")
      {
        std::string_view data = R"(
let h:R*void -> R, (x,void) -> 2.5;
)";
        std::string error     = "parse error, expecting type specifier";

        TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};

        REQUIRE_THROWS_WITH(ASTBuilder::build(input), error);
      }
    }
  }
}

#ifdef __clang__
#else   // __clang__
#ifdef __GNUG__
#pragma GCC pop_options
#endif   // __GNUG__
#endif   // __clang__
