#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/node_processor/FakeProcessor.hpp>
#include <utils/Demangle.hpp>

#include <rang.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("FakeProcessor", "[language]")
{
  rang::setControlMode(rang::control::Off);

  FakeProcessor fake_processor;
  REQUIRE(fake_processor.typeIdName() == demangle<FakeProcessor>());

  ExecutionPolicy exec_policy;
  REQUIRE_NOTHROW(fake_processor.execute(exec_policy));
}
