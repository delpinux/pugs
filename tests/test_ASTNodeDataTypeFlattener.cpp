#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDataTypeFlattener.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/TypeDescriptor.hpp>

#include <test_BuiltinFunctionRegister.hpp>

#include <pegtl/string_input.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeDataTypeFlattener", "[language]")
{
  SECTION("simple types")
  {
    SECTION("B")
    {
      std::string_view data = R"(
let b : B, b = true;
b;
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto root_node = ASTBuilder::build(input);

      ASTModulesImporter{*root_node};
      ASTNodeTypeCleaner<language::import_instruction>{*root_node};

      ASTSymbolTableBuilder{*root_node};
      ASTNodeDataTypeBuilder{*root_node};

      REQUIRE(root_node->children[1]->is_type<language::name>());

      ASTNodeDataTypeFlattener::FlattenedDataTypeList flattened_datatype_list;
      ASTNodeDataTypeFlattener{*root_node->children[1], flattened_datatype_list};

      REQUIRE(flattened_datatype_list.size() == 1);
      REQUIRE(flattened_datatype_list[0].m_data_type == ASTNodeDataType::bool_t);
      REQUIRE(&flattened_datatype_list[0].m_parent_node == root_node->children[1].get());
      root_node->m_symbol_table->clearValues();
    }

    SECTION("N")
    {
      std::string_view data = R"(
let n : N;
n;
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto root_node = ASTBuilder::build(input);

      ASTModulesImporter{*root_node};
      ASTNodeTypeCleaner<language::import_instruction>{*root_node};

      ASTSymbolTableBuilder{*root_node};
      ASTNodeDataTypeBuilder{*root_node};

      REQUIRE(root_node->children[1]->is_type<language::name>());

      ASTNodeDataTypeFlattener::FlattenedDataTypeList flattened_datatype_list;
      ASTNodeDataTypeFlattener{*root_node->children[1], flattened_datatype_list};

      REQUIRE(flattened_datatype_list.size() == 1);
      REQUIRE(flattened_datatype_list[0].m_data_type == ASTNodeDataType::unsigned_int_t);
      REQUIRE(&flattened_datatype_list[0].m_parent_node == root_node->children[1].get());
      root_node->m_symbol_table->clearValues();
    }
  }

  SECTION("Compound types")
  {
    SECTION("function evaluation -> N")
    {
      std::string_view data = R"(
let f: N -> N, n -> n;
f(2);
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto root_node = ASTBuilder::build(input);

      ASTModulesImporter{*root_node};
      ASTNodeTypeCleaner<language::import_instruction>{*root_node};

      ASTSymbolTableBuilder{*root_node};
      ASTNodeDataTypeBuilder{*root_node};

      // optimizations
      ASTNodeDeclarationToAffectationConverter{*root_node};

      ASTNodeTypeCleaner<language::var_declaration>{*root_node};
      ASTNodeTypeCleaner<language::fct_declaration>{*root_node};

      REQUIRE(root_node->children[0]->is_type<language::function_evaluation>());

      ASTNodeDataTypeFlattener::FlattenedDataTypeList flattened_datatype_list;
      ASTNodeDataTypeFlattener{*root_node->children[0], flattened_datatype_list};

      REQUIRE(flattened_datatype_list.size() == 1);
      REQUIRE(flattened_datatype_list[0].m_data_type == ASTNodeDataType::unsigned_int_t);
      REQUIRE(&flattened_datatype_list[0].m_parent_node == root_node->children[0].get());
      root_node->m_symbol_table->clearValues();
    }

    SECTION("function evaluation -> N*R*B*string*Z*builtin_t")
    {
      std::string_view data = R"(
let f: N*builtin_t -> N*R*B*string*Z*builtin_t, (n,b) -> (n, 0.5*n, n>2, n, 3-n, b);
f(2, b);
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto root_node = ASTBuilder::build(input);

      ASTModulesImporter{*root_node};
      ASTNodeTypeCleaner<language::import_instruction>{*root_node};
      SymbolTable& symbol_table = *root_node->m_symbol_table;

      auto [i_symbol, success] = symbol_table.add(builtin_data_type.nameOfTypeId(), root_node->begin());
      if (not success) {
        throw UnexpectedError("cannot add '" + builtin_data_type.nameOfTypeId() + "' type for testing");
      }

      i_symbol->attributes().setDataType(ASTNodeDataType::build<ASTNodeDataType::type_name_id_t>());
      i_symbol->attributes().setIsInitialized();
      i_symbol->attributes().value() = symbol_table.typeEmbedderTable().size();
      symbol_table.typeEmbedderTable().add(std::make_shared<TypeDescriptor>(builtin_data_type.nameOfTypeId()));

      auto [i_b_symbol, b_success] = symbol_table.add("b", root_node->begin());
      i_b_symbol->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);
      i_b_symbol->attributes().setIsInitialized();

      ASTSymbolTableBuilder{*root_node};
      ASTNodeDataTypeBuilder{*root_node};

      // optimizations
      ASTNodeDeclarationToAffectationConverter{*root_node};

      ASTNodeTypeCleaner<language::var_declaration>{*root_node};
      ASTNodeTypeCleaner<language::fct_declaration>{*root_node};

      REQUIRE(root_node->children[0]->is_type<language::function_evaluation>());

      ASTNodeDataTypeFlattener::FlattenedDataTypeList flattened_datatype_list;
      ASTNodeDataTypeFlattener{*root_node->children[0], flattened_datatype_list};

      REQUIRE(flattened_datatype_list.size() == 6);
      REQUIRE(flattened_datatype_list[0].m_data_type == ASTNodeDataType::unsigned_int_t);
      REQUIRE(flattened_datatype_list[1].m_data_type == ASTNodeDataType::double_t);
      REQUIRE(flattened_datatype_list[2].m_data_type == ASTNodeDataType::bool_t);
      REQUIRE(flattened_datatype_list[3].m_data_type == ASTNodeDataType::string_t);
      REQUIRE(flattened_datatype_list[4].m_data_type == ASTNodeDataType::int_t);
      REQUIRE(flattened_datatype_list[5].m_data_type == ASTNodeDataType::type_id_t);
      root_node->m_symbol_table->clearValues();
    }

    SECTION("function evaluation -> R*R^3")
    {
      std::string_view data = R"(
let f: R -> R*R^3, x -> (0.5*x, (x, x+1, x-1));
f(2);
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto root_node = ASTBuilder::build(input);

      ASTModulesImporter{*root_node};
      ASTNodeTypeCleaner<language::import_instruction>{*root_node};

      ASTSymbolTableBuilder{*root_node};
      ASTNodeDataTypeBuilder{*root_node};

      // optimizations
      ASTNodeDeclarationToAffectationConverter{*root_node};

      ASTNodeTypeCleaner<language::var_declaration>{*root_node};
      ASTNodeTypeCleaner<language::fct_declaration>{*root_node};

      REQUIRE(root_node->children[0]->is_type<language::function_evaluation>());

      ASTNodeDataTypeFlattener::FlattenedDataTypeList flattened_datatype_list;
      ASTNodeDataTypeFlattener{*root_node->children[0], flattened_datatype_list};

      REQUIRE(flattened_datatype_list.size() == 2);
      REQUIRE(flattened_datatype_list[0].m_data_type == ASTNodeDataType::double_t);
      REQUIRE(flattened_datatype_list[1].m_data_type == ASTNodeDataType::vector_t);
      REQUIRE(flattened_datatype_list[1].m_data_type.dimension() == 3);
      root_node->m_symbol_table->clearValues();
    }

    SECTION("builtin_function -> R*R")
    {
      std::string_view data = R"(
sum_vector(2,3);
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto root_node = ASTBuilder::build(input);

      ASTModulesImporter{*root_node};
      ASTNodeTypeCleaner<language::import_instruction>{*root_node};

      test_only::test_BuiltinFunctionRegister{*root_node};

      ASTSymbolTableBuilder{*root_node};
      ASTNodeDataTypeBuilder{*root_node};

      // optimizations
      ASTNodeDeclarationToAffectationConverter{*root_node};

      ASTNodeTypeCleaner<language::var_declaration>{*root_node};
      ASTNodeTypeCleaner<language::fct_declaration>{*root_node};

      REQUIRE(root_node->children[0]->is_type<language::function_evaluation>());

      ASTNodeDataTypeFlattener::FlattenedDataTypeList flattened_datatype_list;
      ASTNodeDataTypeFlattener{*root_node->children[0], flattened_datatype_list};

      REQUIRE(flattened_datatype_list.size() == 2);
      REQUIRE(flattened_datatype_list[0].m_data_type == ASTNodeDataType::double_t);
      REQUIRE(flattened_datatype_list[1].m_data_type == ASTNodeDataType::vector_t);
      REQUIRE(flattened_datatype_list[1].m_data_type.dimension() == 2);
      root_node->m_symbol_table->clearValues();
    }
  }
}
