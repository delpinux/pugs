#ifndef MESH_DATA_BASE_FOR_TESTS_HPP
#define MESH_DATA_BASE_FOR_TESTS_HPP

#include <array>
#include <memory>
#include <string>

class MeshVariant;

class MeshDataBaseForTests
{
 public:
  class NamedMesh
  {
   private:
    const std::string m_name;
    const std::shared_ptr<const MeshVariant> m_mesh_v;

   public:
    NamedMesh(const std::string& name, const std::shared_ptr<const MeshVariant>& mesh) : m_name(name), m_mesh_v(mesh) {}

    const std::string&
    name() const
    {
      return m_name;
    }

    auto
    mesh() const
    {
      return m_mesh_v;
    }
  };

 private:
  explicit MeshDataBaseForTests();

  static const MeshDataBaseForTests* m_instance;

  std::shared_ptr<const MeshVariant> m_cartesian_1d_mesh;
  std::shared_ptr<const MeshVariant> m_cartesian_2d_mesh;
  std::shared_ptr<const MeshVariant> m_cartesian_3d_mesh;

  std::shared_ptr<const MeshVariant> m_unordered_1d_mesh;
  std::shared_ptr<const MeshVariant> m_hybrid_2d_mesh;
  std::shared_ptr<const MeshVariant> m_hybrid_3d_mesh;

  std::shared_ptr<const MeshVariant> _buildUnordered1dMesh();
  std::shared_ptr<const MeshVariant> _buildHybrid2dMesh();
  std::shared_ptr<const MeshVariant> _buildHybrid3dMesh();

 public:
  std::shared_ptr<const MeshVariant> cartesian1DMesh() const;
  std::shared_ptr<const MeshVariant> unordered1DMesh() const;

  std::shared_ptr<const MeshVariant> cartesian2DMesh() const;
  std::shared_ptr<const MeshVariant> hybrid2DMesh() const;

  std::shared_ptr<const MeshVariant> cartesian3DMesh() const;
  std::shared_ptr<const MeshVariant> hybrid3DMesh() const;

  static const MeshDataBaseForTests& get();

  auto
  all1DMeshes() const
  {
    return std::array{NamedMesh{"cartesian 1d mesh", cartesian1DMesh()},   //
                      NamedMesh{"unordered 1d mesh", unordered1DMesh()}};
  }

  auto
  all2DMeshes() const
  {
    return std::array{NamedMesh{"cartesian 2d mesh", cartesian2DMesh()},   //
                      NamedMesh{"hybrid 2d mesh", hybrid2DMesh()}};
  }

  auto
  all3DMeshes() const
  {
    return std::array{NamedMesh{"cartesian 3d mesh", cartesian3DMesh()},   //
                      NamedMesh{std::string("hybrid 3d mesh"), hybrid3DMesh()}};
  }

  static void create();
  static void destroy();

  ~MeshDataBaseForTests() = default;
};

#endif   // MESH_DATA_BASE_FOR_TESTS_HPP
