#include <catch2/catch_test_macros.hpp>

#include <mesh/CellType.hpp>

#include <limits>
#include <type_traits>

// clazy:excludeall=non-pod-global-static

TEST_CASE("CellType", "[mesh]")
{
  REQUIRE(name(CellType::Line) == "line");

  REQUIRE(name(CellType::Triangle) == "triangle");
  REQUIRE(name(CellType::Quadrangle) == "quadrangle");
  REQUIRE(name(CellType::Polygon) == "polygon");

  REQUIRE(name(CellType::Diamond) == "diamond");
  REQUIRE(name(CellType::Hexahedron) == "hexahedron");
  REQUIRE(name(CellType::Prism) == "prism");
  REQUIRE(name(CellType::Pyramid) == "pyramid");
  REQUIRE(name(CellType::Tetrahedron) == "tetrahedron");

  REQUIRE(name(static_cast<CellType>(std::numeric_limits<std::underlying_type_t<CellType>>::max())) ==
          "unknown cell type");
}
