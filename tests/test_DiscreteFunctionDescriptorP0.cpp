#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <scheme/DiscreteFunctionDescriptorP0.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("DiscreteFunctionDescriptorP0", "[scheme]")
{
  SECTION("type")
  {
    DiscreteFunctionDescriptorP0 descriptor;
    REQUIRE(descriptor.type() == DiscreteFunctionType::P0);

    {
      auto copy = [](const DiscreteFunctionDescriptorP0& d) -> DiscreteFunctionDescriptorP0 { return d; };

      DiscreteFunctionDescriptorP0 descriptor_copy{copy(descriptor)};
      REQUIRE(descriptor_copy.type() == DiscreteFunctionType::P0);
    }

    DiscreteFunctionDescriptorP0 temp;
    DiscreteFunctionDescriptorP0 descriptor_move{std::move(temp)};
    REQUIRE(descriptor_move.type() == DiscreteFunctionType::P0);
  }
}
