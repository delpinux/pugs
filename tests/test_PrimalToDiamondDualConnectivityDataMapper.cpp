#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/DualConnectivityManager.hpp>
#include <mesh/PrimalToDiamondDualConnectivityDataMapper.hpp>
#include <mesh/PrimalToDual1DConnectivityDataMapper.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("PrimalToDiamondDualConnectivityDataMapper", "[mesh]")
{
  SECTION("1D")
  {
    constexpr static size_t Dimension = 1;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    std::shared_ptr<const MeshType> mesh        = MeshDataBaseForTests::get().unordered1DMesh()->get<MeshType>();
    const ConnectivityType& primal_connectivity = mesh->connectivity();

    std::shared_ptr p_diamond_dual_connectivity =
      DualConnectivityManager::instance().getDiamondDualConnectivity(primal_connectivity);

    std::shared_ptr p_dual_1d_connectivity =
      DualConnectivityManager::instance().getDual1DConnectivity(primal_connectivity);

    REQUIRE(p_diamond_dual_connectivity == p_dual_1d_connectivity);

    std::shared_ptr p_primal_to_diamond_dual_connectivity_mapper =
      DualConnectivityManager::instance().getPrimalToDiamondDualConnectivityDataMapper(primal_connectivity);

    std::shared_ptr p_primal_to_dual_1d_connectivity_mapper =
      DualConnectivityManager::instance().getPrimalToDual1DConnectivityDataMapper(primal_connectivity);

    REQUIRE(p_primal_to_diamond_dual_connectivity_mapper == p_primal_to_dual_1d_connectivity_mapper);
  }

  SECTION("2D")
  {
    constexpr static size_t Dimension = 2;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    std::shared_ptr<const MeshType> mesh        = MeshDataBaseForTests::get().hybrid2DMesh()->get<MeshType>();
    const ConnectivityType& primal_connectivity = mesh->connectivity();

    std::shared_ptr p_diamond_dual_connectivity =
      DualConnectivityManager::instance().getDiamondDualConnectivity(primal_connectivity);
    const ConnectivityType& dual_connectivity = *p_diamond_dual_connectivity;

    std::shared_ptr p_primal_to_dual_connectivity_mapper =
      DualConnectivityManager::instance().getPrimalToDiamondDualConnectivityDataMapper(primal_connectivity);
    const auto& primal_to_dual_connectivity_mapper = *p_primal_to_dual_connectivity_mapper;

    SECTION("check data transfer between primal faces to dual cells")
    {
      const auto& primal_face_to_cell_matrix = primal_connectivity.faceToCellMatrix();

      FaceValue<size_t> primal_face_value{primal_connectivity};
      parallel_for(
        primal_face_value.numberOfItems(), PUGS_LAMBDA(const FaceId face_id) {
          primal_face_value[face_id] = primal_face_to_cell_matrix[face_id].size() + face_id;
        });

      CellValue<size_t> dual_from_primal_face_value{dual_connectivity};

      primal_to_dual_connectivity_mapper.toDualCell(primal_face_value, dual_from_primal_face_value);

      {
        const auto& dual_cell_to_node_matrix = dual_connectivity.cellToNodeMatrix();
        bool correct_value                   = true;
        for (CellId cell_id = 0; cell_id < dual_connectivity.numberOfCells(); ++cell_id) {
          correct_value &=
            (dual_cell_to_node_matrix[cell_id].size() + cell_id == dual_from_primal_face_value[cell_id] + 2);
        }
        REQUIRE(correct_value);
      }

      {
        bool is_same = true;
        FaceValue<size_t> primal_from_dual_cell_value{primal_connectivity};
        primal_to_dual_connectivity_mapper.fromDualCell(dual_from_primal_face_value, primal_from_dual_cell_value);
        for (FaceId face_id = 0; face_id < primal_connectivity.numberOfFaces(); ++face_id) {
          is_same &= (primal_from_dual_cell_value[face_id] == primal_face_value[face_id]);
        }
        REQUIRE(is_same);
      }

      {
        const auto& primal_edge_to_cell_matrix = primal_connectivity.edgeToCellMatrix();

        EdgeValue<size_t> primal_edge_value{primal_connectivity};
        parallel_for(
          primal_edge_value.numberOfItems(), PUGS_LAMBDA(const EdgeId edge_id) {
            primal_edge_value[edge_id] = primal_edge_to_cell_matrix[edge_id].size() + edge_id;
          });

        CellValue<size_t> dual_from_primal_edge_value{dual_connectivity};

        primal_to_dual_connectivity_mapper.toDualCell(primal_edge_value, dual_from_primal_edge_value);

        {
          bool is_same = true;
          for (CellId cell_id = 0; cell_id < primal_connectivity.numberOfCells(); ++cell_id) {
            is_same &= (dual_from_primal_edge_value[cell_id] == dual_from_primal_face_value[cell_id]);
          }
          REQUIRE(is_same);
        }

        {
          bool is_same = true;
          EdgeValue<size_t> primal_from_dual_cell_value{primal_connectivity};
          primal_to_dual_connectivity_mapper.fromDualCell(dual_from_primal_edge_value, primal_from_dual_cell_value);
          for (EdgeId edge_id = 0; edge_id < primal_connectivity.numberOfEdges(); ++edge_id) {
            is_same &= (primal_from_dual_cell_value[edge_id] == primal_edge_value[edge_id]);
          }
          REQUIRE(is_same);
        }
      }

#ifndef NDEBUG
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualCell(FaceValue<size_t>{dual_connectivity},
                                                                        dual_from_primal_face_value),
                          "unexpected connectivity for primal FaceValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualCell(primal_face_value,
                                                                        CellValue<size_t>{primal_connectivity}),
                          "unexpected connectivity for dual CellValue");

      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualCell(dual_from_primal_face_value,
                                                                          FaceValue<size_t>{dual_connectivity}),
                          "unexpected connectivity for primal FaceValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualCell(CellValue<size_t>{primal_connectivity},
                                                                          primal_face_value),
                          "unexpected connectivity for dual CellValue");

#endif   // NDEBUG
    }

    SECTION("check data transfer between primal nodes and cells to dual nodes")
    {
      const auto& primal_cell_to_node_matrix = primal_connectivity.cellToNodeMatrix();
      CellValue<size_t> primal_cell_value{primal_connectivity};
      const size_t number_of_nodes = primal_connectivity.numberOfNodes();
      parallel_for(
        primal_cell_value.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
          primal_cell_value[cell_id] = primal_cell_to_node_matrix[cell_id].size() + number_of_nodes + cell_id;
        });

      const auto& primal_node_to_face_matrix = primal_connectivity.nodeToFaceMatrix();
      NodeValue<size_t> primal_node_value{primal_connectivity};
      parallel_for(
        primal_node_value.numberOfItems(), PUGS_LAMBDA(const NodeId node_id) {
          primal_node_value[node_id] = primal_node_to_face_matrix[node_id].size() + node_id;
        });

      NodeValue<size_t> dual_from_primal_cell_and_node_value{dual_connectivity};

      primal_to_dual_connectivity_mapper.toDualNode(primal_node_value, primal_cell_value,
                                                    dual_from_primal_cell_and_node_value);

      {
        const auto& dual_node_to_cell_matrix = dual_connectivity.nodeToCellMatrix();
        bool correct_dual_value              = true;
        for (NodeId node_id = 0; node_id < dual_connectivity.numberOfNodes(); ++node_id) {
          correct_dual_value &=
            (dual_from_primal_cell_and_node_value[node_id] == dual_node_to_cell_matrix[node_id].size() + node_id);
        }
        REQUIRE(correct_dual_value);
      }

      {
        bool is_same = true;
        NodeValue<size_t> primal_from_dual_node_value{primal_connectivity};
        CellValue<size_t> primal_from_dual_cell_value{primal_connectivity};
        primal_to_dual_connectivity_mapper.fromDualNode(dual_from_primal_cell_and_node_value,
                                                        primal_from_dual_node_value, primal_from_dual_cell_value);
        for (CellId cell_id = 0; cell_id < primal_connectivity.numberOfCells(); ++cell_id) {
          is_same &= (primal_cell_value[cell_id] == primal_from_dual_cell_value[cell_id]);
        }
        for (NodeId node_id = 0; node_id < primal_connectivity.numberOfNodes(); ++node_id) {
          is_same &= (primal_node_value[node_id] == primal_from_dual_node_value[node_id]);
        }

        REQUIRE(is_same);
      }

#ifndef NDEBUG
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualNode(NodeValue<size_t>{dual_connectivity},
                                                                        primal_cell_value,
                                                                        dual_from_primal_cell_and_node_value),
                          "unexpected connectivity for primal NodeValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualNode(primal_node_value,
                                                                        CellValue<size_t>{dual_connectivity},
                                                                        dual_from_primal_cell_and_node_value),
                          "unexpected connectivity for primal CellValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualNode(primal_node_value, primal_cell_value,
                                                                        NodeValue<size_t>{primal_connectivity}),
                          "unexpected connectivity for dual NodeValue");

      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualNode(dual_from_primal_cell_and_node_value,
                                                                          NodeValue<size_t>{dual_connectivity},
                                                                          primal_cell_value),
                          "unexpected connectivity for primal NodeValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualNode(dual_from_primal_cell_and_node_value,
                                                                          primal_node_value,
                                                                          CellValue<size_t>{dual_connectivity}),
                          "unexpected connectivity for primal CellValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualNode(NodeValue<size_t>{primal_connectivity},
                                                                          primal_node_value, primal_cell_value),
                          "unexpected connectivity for dual NodeValue");

#endif   // NDEBUG
    }
  }

  SECTION("3D")
  {
    constexpr static size_t Dimension = 3;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    std::shared_ptr<const MeshType> mesh        = MeshDataBaseForTests::get().hybrid3DMesh()->get<MeshType>();
    const ConnectivityType& primal_connectivity = mesh->connectivity();

    std::shared_ptr p_diamond_dual_connectivity =
      DualConnectivityManager::instance().getDiamondDualConnectivity(primal_connectivity);
    const ConnectivityType& dual_connectivity = *p_diamond_dual_connectivity;

    std::shared_ptr p_primal_to_dual_connectivity_mapper =
      DualConnectivityManager::instance().getPrimalToDiamondDualConnectivityDataMapper(primal_connectivity);
    const auto& primal_to_dual_connectivity_mapper = *p_primal_to_dual_connectivity_mapper;

    SECTION("check data transfer between primal faces to dual cells")
    {
      const auto& primal_face_to_cell_matrix = primal_connectivity.faceToCellMatrix();
      const auto& primal_face_to_node_matrix = primal_connectivity.faceToNodeMatrix();

      FaceValue<size_t> primal_face_value{primal_connectivity};
      parallel_for(
        primal_face_value.numberOfItems(), PUGS_LAMBDA(const FaceId face_id) {
          primal_face_value[face_id] =
            primal_face_to_cell_matrix[face_id].size() + primal_face_to_node_matrix[face_id].size() + face_id;
        });

      CellValue<size_t> dual_from_primal_face_value{dual_connectivity};

      primal_to_dual_connectivity_mapper.toDualCell(primal_face_value, dual_from_primal_face_value);

      {
        const auto& dual_cell_to_node_matrix = dual_connectivity.cellToNodeMatrix();
        bool correct_value                   = true;
        for (CellId cell_id = 0; cell_id < dual_connectivity.numberOfCells(); ++cell_id) {
          correct_value &= (dual_cell_to_node_matrix[cell_id].size() + cell_id == dual_from_primal_face_value[cell_id]);
        }
        REQUIRE(correct_value);
      }

      {
        bool is_same = true;
        FaceValue<size_t> primal_from_dual_cell_value{primal_connectivity};
        primal_to_dual_connectivity_mapper.fromDualCell(dual_from_primal_face_value, primal_from_dual_cell_value);
        for (FaceId face_id = 0; face_id < primal_connectivity.numberOfFaces(); ++face_id) {
          is_same &= (primal_from_dual_cell_value[face_id] == primal_face_value[face_id]);
        }
        REQUIRE(is_same);
      }

#ifndef NDEBUG
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualCell(FaceValue<size_t>{dual_connectivity},
                                                                        dual_from_primal_face_value),
                          "unexpected connectivity for primal FaceValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualCell(primal_face_value,
                                                                        CellValue<size_t>{primal_connectivity}),
                          "unexpected connectivity for dual CellValue");

      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualCell(dual_from_primal_face_value,
                                                                          FaceValue<size_t>{dual_connectivity}),
                          "unexpected connectivity for primal FaceValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualCell(CellValue<size_t>{primal_connectivity},
                                                                          primal_face_value),
                          "unexpected connectivity for dual CellValue");

#endif   // NDEBUG
    }

    SECTION("check data transfer between primal nodes and cells to dual nodes")
    {
      const auto& primal_cell_to_face_matrix = primal_connectivity.cellToFaceMatrix();
      CellValue<size_t> primal_cell_value{primal_connectivity};
      const size_t number_of_nodes = primal_connectivity.numberOfNodes();
      parallel_for(
        primal_cell_value.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
          primal_cell_value[cell_id] = primal_cell_to_face_matrix[cell_id].size() + number_of_nodes + cell_id;
        });

      const auto& primal_node_to_face_matrix = primal_connectivity.nodeToFaceMatrix();
      NodeValue<size_t> primal_node_value{primal_connectivity};
      parallel_for(
        primal_node_value.numberOfItems(), PUGS_LAMBDA(const NodeId node_id) {
          primal_node_value[node_id] = primal_node_to_face_matrix[node_id].size() + node_id;
        });

      NodeValue<size_t> dual_from_primal_cell_and_node_value{dual_connectivity};

      primal_to_dual_connectivity_mapper.toDualNode(primal_node_value, primal_cell_value,
                                                    dual_from_primal_cell_and_node_value);

      {
        const auto& dual_node_to_cell_matrix = dual_connectivity.nodeToCellMatrix();
        bool correct_dual_value              = true;
        for (NodeId node_id = 0; node_id < dual_connectivity.numberOfNodes(); ++node_id) {
          correct_dual_value &=
            (dual_from_primal_cell_and_node_value[node_id] == dual_node_to_cell_matrix[node_id].size() + node_id);
        }
        REQUIRE(correct_dual_value);
      }

      {
        bool is_same = true;
        NodeValue<size_t> primal_from_dual_node_value{primal_connectivity};
        CellValue<size_t> primal_from_dual_cell_value{primal_connectivity};
        primal_to_dual_connectivity_mapper.fromDualNode(dual_from_primal_cell_and_node_value,
                                                        primal_from_dual_node_value, primal_from_dual_cell_value);
        for (CellId cell_id = 0; cell_id < primal_connectivity.numberOfCells(); ++cell_id) {
          is_same &= (primal_cell_value[cell_id] == primal_from_dual_cell_value[cell_id]);
        }
        for (NodeId node_id = 0; node_id < primal_connectivity.numberOfNodes(); ++node_id) {
          is_same &= (primal_node_value[node_id] == primal_from_dual_node_value[node_id]);
        }

        REQUIRE(is_same);
      }

#ifndef NDEBUG
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualNode(NodeValue<size_t>{dual_connectivity},
                                                                        primal_cell_value,
                                                                        dual_from_primal_cell_and_node_value),
                          "unexpected connectivity for primal NodeValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualNode(primal_node_value,
                                                                        CellValue<size_t>{dual_connectivity},
                                                                        dual_from_primal_cell_and_node_value),
                          "unexpected connectivity for primal CellValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.toDualNode(primal_node_value, primal_cell_value,
                                                                        NodeValue<size_t>{primal_connectivity}),
                          "unexpected connectivity for dual NodeValue");

      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualNode(dual_from_primal_cell_and_node_value,
                                                                          NodeValue<size_t>{dual_connectivity},
                                                                          primal_cell_value),
                          "unexpected connectivity for primal NodeValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualNode(dual_from_primal_cell_and_node_value,
                                                                          primal_node_value,
                                                                          CellValue<size_t>{dual_connectivity}),
                          "unexpected connectivity for primal CellValue");
      REQUIRE_THROWS_WITH(primal_to_dual_connectivity_mapper.fromDualNode(NodeValue<size_t>{primal_connectivity},
                                                                          primal_node_value, primal_cell_value),
                          "unexpected connectivity for dual NodeValue");

#endif   // NDEBUG
    }
  }
}
