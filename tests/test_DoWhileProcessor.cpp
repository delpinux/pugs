#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeAffectationExpressionBuilder.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/node_processor/DoWhileProcessor.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_WHILE_PROCESSOR_RESULT(data, variable_name, expected_value)     \
  {                                                                           \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                \
    auto ast = ASTBuilder::build(input);                                      \
                                                                              \
    ASTExecutionStack::create();                                              \
                                                                              \
    ASTModulesImporter{*ast};                                                 \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                   \
                                                                              \
    ASTSymbolTableBuilder{*ast};                                              \
    ASTNodeDataTypeBuilder{*ast};                                             \
                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                      \
                                                                              \
    ASTNodeExpressionBuilder{*ast};                                           \
    ExecutionPolicy exec_policy;                                              \
    ast->execute(exec_policy);                                                \
                                                                              \
    auto symbol_table = ast->m_symbol_table;                                  \
                                                                              \
    using namespace TAO_PEGTL_NAMESPACE;                                      \
    position use_position{10000, 1000, 10, "fixture"};                        \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);   \
                                                                              \
    auto attributes = symbol->attributes();                                   \
    auto value      = std::get<decltype(expected_value)>(attributes.value()); \
                                                                              \
    ASTExecutionStack::destroy();                                             \
                                                                              \
    REQUIRE(value == expected_value);                                         \
    ast->m_symbol_table->clearValues();                                       \
  }

#define CHECK_DO_WHILE_PROCESSOR_THROWS_WITH(data, error_message)     \
  {                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};        \
    auto ast = ASTBuilder::build(input);                              \
                                                                      \
    ASTExecutionStack::create();                                      \
                                                                      \
    ASTModulesImporter{*ast};                                         \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};           \
                                                                      \
    ASTSymbolTableBuilder{*ast};                                      \
                                                                      \
    ASTExecutionStack::destroy();                                     \
                                                                      \
    REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error_message); \
    ast->m_symbol_table->clearValues();                               \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("DoWhileProcessor", "[language]")
{
  SECTION("simple loop")
  {
    std::string_view data = R"(
let i:N, i = 3;
let j:N, j = 0;
do {
  j++;
  i += j;
} while(i<10);
)";
    CHECK_WHILE_PROCESSOR_RESULT(data, "i", 13ul);
  }

  SECTION("do-while with break")
  {
    std::string_view data = R"(
let i:N, i = 3;
let j:N, j = 0;
do {
  j++;
  if (j==2) break;
  i += j;
} while(i<10);
)";
    CHECK_WHILE_PROCESSOR_RESULT(data, "i", 4ul);
  }

  SECTION("do-while with continue")
  {
    std::string_view data = R"(
let i:N, i = 3;
let j:N, j = 0;
do {
  j++;
  if (j<=3) continue;
  i += j;
} while(i<10);
)";
    CHECK_WHILE_PROCESSOR_RESULT(data, "i", 12ul);
  }

  SECTION("empty do-while symbol table untouched")
  {
    std::string_view data = R"(
let i:N, i = 3;
do {} while(false);
)";
    CHECK_WHILE_PROCESSOR_RESULT(data, "i", 3ul);
  }

  SECTION("single instruction do-while symbol table untouched")
  {
    std::string_view data = R"(
let i:N, i = 3;
do
  i = 2;
while(false);
)";
    CHECK_WHILE_PROCESSOR_RESULT(data, "i", 2ul);
  }

  SECTION("errors")
  {
    SECTION("bad test type")
    {
      std::string_view data = R"(
do {
} while(1);
)";

      CHECK_DO_WHILE_PROCESSOR_THROWS_WITH(data, "invalid implicit conversion: Z -> B");
    }
  }

  SECTION("expression type")
  {
    ASTNode node;
    REQUIRE(DoWhileProcessor{node}.type() == INodeProcessor::Type::do_while_processor);
  }
}
