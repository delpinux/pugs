#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/ConsoleManager.hpp>

#include <rang.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ConsoleManager", "[utils]")
{
  SECTION("is terminal")
  {
    const bool is_terminal = rang::rang_implementation::isTerminal(std::cout.rdbuf());

    REQUIRE(is_terminal == ConsoleManager::isTerminal(std::cout));
  }

  SECTION("control settings")
  {
    const rang::control saved_control = rang::rang_implementation::controlMode();

    ConsoleManager::init(true);

    REQUIRE(rang::rang_implementation::controlMode() == rang::control::Force);

    ConsoleManager::init(false);

    REQUIRE(rang::rang_implementation::controlMode() == rang::control::Off);

    rang::setControlMode(saved_control);

    const bool show_preamble = ConsoleManager::showPreamble();

    ConsoleManager::setShowPreamble(not show_preamble);
    REQUIRE(show_preamble != ConsoleManager::showPreamble());

    ConsoleManager::setShowPreamble(show_preamble);
    REQUIRE(show_preamble == ConsoleManager::showPreamble());
  }
}
