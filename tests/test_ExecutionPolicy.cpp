#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/node_processor/ExecutionPolicy.hpp>

#include <rang.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ExecutionPolicy", "[language]")
{
  rang::setControlMode(rang::control::Off);

  ExecutionPolicy exec_policy;
  SECTION("no jump")
  {
    exec_policy = ExecutionPolicy{ExecutionPolicy{}, ExecutionPolicy::JumpType::no_jump};
    REQUIRE(exec_policy.exec() == true);
    REQUIRE(exec_policy.jumpType() == ExecutionPolicy::JumpType::no_jump);
  }

  SECTION("break jump")
  {
    exec_policy = ExecutionPolicy{ExecutionPolicy{}, ExecutionPolicy::JumpType::break_jump};
    REQUIRE(exec_policy.exec() == false);
    REQUIRE(exec_policy.jumpType() == ExecutionPolicy::JumpType::break_jump);
  }

  SECTION("continue jump")
  {
    exec_policy = ExecutionPolicy{ExecutionPolicy{}, ExecutionPolicy::JumpType::continue_jump};
    REQUIRE(exec_policy.exec() == false);
    REQUIRE(exec_policy.jumpType() == ExecutionPolicy::JumpType::continue_jump);
  }

  SECTION("context")
  {
    ExecutionPolicy::Context::SharedValues shared_values;

    exec_policy = ExecutionPolicy{ExecutionPolicy{}, ExecutionPolicy::Context{1, shared_values}};

    REQUIRE(exec_policy.contextOfId(1).id() == 1);
    REQUIRE_THROWS_WITH(exec_policy.contextOfId(2).id(), std::string{"unable to find context"});
  }
}
