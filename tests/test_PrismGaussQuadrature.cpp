#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/PrismGaussQuadrature.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/TetrahedronTransformation.hpp>
#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("PrismGaussQuadrature", "[analysis]")
{
  auto integrate = [](auto f, auto quadrature_formula) {
    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    auto value = weight_list[0] * f(point_list[0]);
    for (size_t i = 1; i < weight_list.size(); ++i) {
      value += weight_list[i] * f(point_list[i]);
    }

    return value;
  };

  auto integrate_on_prism = [](auto f, auto quadrature_formula, const std::array<TinyVector<3>, 4>& tetrahedron) {
    const auto& A = tetrahedron[0];
    const auto& B = tetrahedron[1];
    const auto& C = tetrahedron[2];
    const auto& D = tetrahedron[3];

    TinyMatrix<3> J;
    for (size_t i = 0; i < 3; ++i) {
      J(i, 0) = B[i] - A[i];
      J(i, 1) = C[i] - A[i];
      J(i, 2) = 0.5 * (D[i] - A[i]);
    }
    TinyVector s = 0.5 * (A + D);

    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();
    auto value       = weight_list[0] * f(J * (point_list[0]) + s);
    for (size_t i = 1; i < weight_list.size(); ++i) {
      value += weight_list[i] * f(J * (point_list[i]) + s);
    }

    return det(J) * value;
  };

  auto get_order = [&integrate, &integrate_on_prism](auto f, auto quadrature_formula, const double exact_value) {
    using R3 = TinyVector<3>;

    const R3 A{+0, +0, -1};
    const R3 B{+1, +0, -1};
    const R3 C{+0, +1, -1};
    const R3 D{+0, +0, +1};
    const R3 E{+1, +0, +1};
    const R3 F{+0, +1, +1};

    const R3 M_AB   = 0.5 * (A + B);
    const R3 M_AC   = 0.5 * (A + C);
    const R3 M_BC   = 0.5 * (B + C);
    const R3 M_DE   = 0.5 * (D + E);
    const R3 M_AD   = 0.5 * (A + D);
    const R3 M_BE   = 0.5 * (B + E);
    const R3 M_CF   = 0.5 * (C + F);
    const R3 M_ABED = 0.25 * (A + B + E + D);
    const R3 M_BCFE = 0.25 * (B + C + E + F);
    const R3 M_ADFC = 0.25 * (A + D + F + C);

    const double int_T_hat = integrate(f, quadrature_formula);
    const double int_refined   //
      = integrate_on_prism(f, quadrature_formula, {A, M_AB, M_AC, M_AD}) +
        integrate_on_prism(f, quadrature_formula, {B, M_BC, M_AB, M_BE}) +
        integrate_on_prism(f, quadrature_formula, {C, M_AC, M_BC, M_CF}) +
        integrate_on_prism(f, quadrature_formula, {M_AB, M_BC, M_AC, M_ABED}) +
        integrate_on_prism(f, quadrature_formula, {M_AD, M_ABED, M_ADFC, D}) +
        integrate_on_prism(f, quadrature_formula, {M_BE, M_BCFE, M_ABED, E}) +
        integrate_on_prism(f, quadrature_formula, {M_CF, M_ADFC, M_BCFE, F}) +
        integrate_on_prism(f, quadrature_formula, {M_ABED, M_BCFE, M_ADFC, M_DE});

    return -std::log(std::abs(int_refined - exact_value) / std::abs(int_T_hat - exact_value)) / std::log(2);
  };

  auto p0 = [](const TinyVector<3>&) { return 4; };
  auto p1 = [](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return 2 * x + 3 * y + z - 1;
  };
  auto p2 = [&p1](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p1(X) * (2.5 * x - 3 * y + z + 3);
  };
  auto p3 = [&p2](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p2(X) * (3 * x + 2 * y - 3 * z - 1);
  };
  auto p4 = [&p3](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p3(X) * (2 * x - 0.5 * y - 1.3 * z + 1);
  };
  auto p5 = [&p4](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p4(X) * (-0.1 * x + 1.3 * y - 3 * z + 1);
  };
  auto p6 = [&p5](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p5(X) * 7875. / 143443 * (2 * x - y + 4 * z + 1);
  };
  auto p7 = [&p6](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p6(X) * (0.7 * x - 2.7 * y + 1.3 * z - 2);
  };
  auto p8 = [&p7](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p7(X) * (0.3 * x + 1.2 * y - 0.7 * z + 0.2);
  };
  auto p9 = [&p8](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p8(X) * (-0.2 * x + 1.1 * y - 0.5 * z + 0.6);
  };
  auto p10 = [&p9](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p9(X) * (0.7 * x - 0.6 * y - 0.7 * z - 0.2);
  };
  auto p11 = [&p10](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p10(X) * (-1.3 * x + 0.6 * y - 1.3 * z + 0.7);
  };
  auto p12 = [&p11](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p11(X) * (0.3 * x - 0.7 * y + 0.3 * z + 0.7);
  };
  auto p13 = [&p12](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p12(X) * (0.9 * x + 0.2 * y - 0.4 * z + 0.5);
  };
  auto p14 = [&p13](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p13(X) * (0.6 * x - 1.2 * y + 0.7 * z - 0.4);
  };
  auto p15 = [&p14](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p14(X) * (-0.2 * x - 0.7 * y + 0.9 * z + 0.8);
  };
  auto p16 = [&p15](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p15(X) * (0.7 * x + 0.2 * y - 0.6 * z + 0.4);
  };
  auto p17 = [&p16](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p16(X) * (-0.1 * x + 0.8 * y + 0.3 * z - 0.2);
  };
  auto p18 = [&p17](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p17(X) * (0.7 * x - 0.2 * y - 0.3 * z + 0.8);
  };
  auto p19 = [&p18](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p18(X) * (-0.7 * x + 1.2 * y + 1.3 * z + 0.8);
  };
  auto p20 = [&p19](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p19(X) * (0.7 * x - 1.2 * y + 0.3 * z - 0.6);
  };
  auto p21 = [&p20](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p20(X) * (0.7 * x - 1.2 * y + 0.3 * z - 0.6);
  };

  SECTION("degree 1")
  {
    const QuadratureFormula<3>& l1 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(1));

    REQUIRE(l1.numberOfPoints() == 1);

    REQUIRE(integrate(p0, l1) == Catch::Approx(4));
    REQUIRE(integrate(p1, l1) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l1) != Catch::Approx(47. / 24));

    REQUIRE(get_order(p2, l1, 47. / 24) == Catch::Approx(2));
  }

  SECTION("degree 2")
  {
    const QuadratureFormula<3>& l2 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(2));

    REQUIRE(l2.numberOfPoints() == 5);

    REQUIRE(integrate(p0, l2) == Catch::Approx(4));
    REQUIRE(integrate(p1, l2) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l2) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l2) != Catch::Approx(-427. / 360));

    // In a weird way, one gets 4th order on this test
    REQUIRE(get_order(p3, l2, -427. / 360) == Catch::Approx(4));
  }

  SECTION("degree 3")
  {
    const QuadratureFormula<3>& l3 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(3));

    REQUIRE(l3.numberOfPoints() == 8);

    REQUIRE(integrate(p0, l3) == Catch::Approx(4));
    REQUIRE(integrate(p1, l3) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l3) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l3) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l3) != Catch::Approx(1003. / 3600));

    REQUIRE(get_order(p4, l3, 1003. / 3600) == Catch::Approx(4));
  }

  SECTION("degree 4")
  {
    const QuadratureFormula<3>& l4 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(4));

    REQUIRE(l4.numberOfPoints() == 11);

    REQUIRE(integrate(p0, l4) == Catch::Approx(4));
    REQUIRE(integrate(p1, l4) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l4) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l4) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l4) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l4) != Catch::Approx(34031. / 18000));

    REQUIRE(get_order(p5, l4, 34031. / 18000) >= Catch::Approx(5));
  }

  SECTION("degree 5")
  {
    const QuadratureFormula<3>& l5 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(5));

    REQUIRE(l5.numberOfPoints() == 16);

    REQUIRE(integrate(p0, l5) == Catch::Approx(4));
    REQUIRE(integrate(p1, l5) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l5) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l5) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l5) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l5) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l5) != Catch::Approx(36346369. / 55082112));

    REQUIRE(get_order(p6, l5, 36346369. / 55082112) == Catch::Approx(6));
  }

  SECTION("degree 6")
  {
    const QuadratureFormula<3>& l6 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(6));

    REQUIRE(l6.numberOfPoints() == 28);

    REQUIRE(integrate(p0, l6) == Catch::Approx(4));
    REQUIRE(integrate(p1, l6) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l6) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l6) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l6) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l6) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l6) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l6) != Catch::Approx(-1128861017. / 918035200));

    // In a weird way, one gets 8th order on this test
    REQUIRE(get_order(p7, l6, -1128861017. / 918035200) == Catch::Approx(8));
  }

  SECTION("degree 7")
  {
    const QuadratureFormula<3>& l7 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(7));

    REQUIRE(l7.numberOfPoints() == 35);

    REQUIRE(integrate(p0, l7) == Catch::Approx(4));
    REQUIRE(integrate(p1, l7) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l7) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l7) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l7) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l7) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l7) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l7) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l7) != Catch::Approx(-21178319419. / 27541056000));

    REQUIRE(get_order(p8, l7, -21178319419. / 27541056000) == Catch::Approx(8));
  }

  SECTION("degree 8")
  {
    const QuadratureFormula<3>& l8 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(8));

    REQUIRE(l8.numberOfPoints() == 46);

    REQUIRE(integrate(p0, l8) == Catch::Approx(4));
    REQUIRE(integrate(p1, l8) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l8) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l8) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l8) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l8) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l8) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l8) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l8) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l8) != Catch::Approx(-5483758803191. / 9088548480000));

    // In a weird way, one gets 10th order on this test
    REQUIRE(get_order(p9, l8, -5483758803191. / 9088548480000) == Catch::Approx(10));
  }

  SECTION("degree 9")
  {
    const QuadratureFormula<3>& l9 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(9));

    REQUIRE(l9.numberOfPoints() == 59);

    REQUIRE(integrate(p0, l9) == Catch::Approx(4));
    REQUIRE(integrate(p1, l9) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l9) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l9) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l9) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l9) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l9) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l9) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l9) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l9) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l9) != Catch::Approx(-9456848221657. / 22721371200000));

    REQUIRE(get_order(p10, l9, -9456848221657. / 22721371200000) == Catch::Approx(10));
  }

  SECTION("degree 10")
  {
    const QuadratureFormula<3>& l10 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(10));

    REQUIRE(l10.numberOfPoints() == 84);

    REQUIRE(integrate(p0, l10) == Catch::Approx(4));
    REQUIRE(integrate(p1, l10) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l10) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l10) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l10) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l10) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l10) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l10) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l10) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l10) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l10) == Catch::Approx(-9456848221657. / 22721371200000));
    REQUIRE(integrate(p11, l10) != Catch::Approx(-4571362439539697. / 7518708288000000));

    // In a weird way, one gets 12th order on this test
    REQUIRE(get_order(p11, l10, -4571362439539697. / 7518708288000000) == Catch::Approx(12));
  }

  SECTION("degree 11")
  {
    const QuadratureFormula<3>& l11 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(11));

    REQUIRE(l11.numberOfPoints() == 99);

    REQUIRE(integrate(p0, l11) == Catch::Approx(4));
    REQUIRE(integrate(p1, l11) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l11) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l11) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l11) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l11) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l11) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l11) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l11) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l11) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l11) == Catch::Approx(-9456848221657. / 22721371200000));
    REQUIRE(integrate(p11, l11) == Catch::Approx(-4571362439539697. / 7518708288000000));
    REQUIRE(integrate(p12, l11) != Catch::Approx(-491755535075074133. / 1378429852800000000));

    REQUIRE(get_order(p12, l11, -491755535075074133. / 1378429852800000000) == Catch::Approx(12));
  }

  SECTION("degree 12")
  {
    const QuadratureFormula<3>& l12 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(12));

    REQUIRE(l12.numberOfPoints() == 136);

    REQUIRE(integrate(p0, l12) == Catch::Approx(4));
    REQUIRE(integrate(p1, l12) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l12) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l12) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l12) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l12) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l12) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l12) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l12) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l12) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l12) == Catch::Approx(-9456848221657. / 22721371200000));
    REQUIRE(integrate(p11, l12) == Catch::Approx(-4571362439539697. / 7518708288000000));
    REQUIRE(integrate(p12, l12) == Catch::Approx(-491755535075074133. / 1378429852800000000));
    REQUIRE(integrate(p13, l12) != Catch::Approx(-1620413117251976393. / 4135289558400000000));

    // In a weird way, one gets almost 14th order on this test
    REQUIRE(get_order(p13, l12, -1620413117251976393. / 4135289558400000000) == Catch::Approx(14).margin(0.01));
  }

  SECTION("degree 13")
  {
    const QuadratureFormula<3>& l13 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(13));

    REQUIRE(l13.numberOfPoints() == 162);

    REQUIRE(integrate(p0, l13) == Catch::Approx(4));
    REQUIRE(integrate(p1, l13) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l13) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l13) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l13) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l13) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l13) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l13) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l13) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l13) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l13) == Catch::Approx(-9456848221657. / 22721371200000));
    REQUIRE(integrate(p11, l13) == Catch::Approx(-4571362439539697. / 7518708288000000));
    REQUIRE(integrate(p12, l13) == Catch::Approx(-491755535075074133. / 1378429852800000000));
    REQUIRE(integrate(p13, l13) == Catch::Approx(-1620413117251976393. / 4135289558400000000));
    REQUIRE(integrate(p14, l13) != Catch::Approx(296918520496968826367. / 827057911680000000000.));

    REQUIRE(get_order(p14, l13, 296918520496968826367. / 827057911680000000000.) == Catch::Approx(14));
  }

  SECTION("degree 14")
  {
    const QuadratureFormula<3>& l14 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(14));

    REQUIRE(l14.numberOfPoints() == 194);

    REQUIRE(integrate(p0, l14) == Catch::Approx(4));
    REQUIRE(integrate(p1, l14) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l14) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l14) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l14) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l14) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l14) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l14) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l14) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l14) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l14) == Catch::Approx(-9456848221657. / 22721371200000));
    REQUIRE(integrate(p11, l14) == Catch::Approx(-4571362439539697. / 7518708288000000));
    REQUIRE(integrate(p12, l14) == Catch::Approx(-491755535075074133. / 1378429852800000000));
    REQUIRE(integrate(p13, l14) == Catch::Approx(-1620413117251976393. / 4135289558400000000));
    REQUIRE(integrate(p14, l14) == Catch::Approx(296918520496968826367. / 827057911680000000000.));
    REQUIRE(integrate(p15, l14) != Catch::Approx(-7727953154629829488841. / 210899767478400000000000.));

    // In a weird way, one gets almost 16th order on this test
    REQUIRE(get_order(p15, l14, -7727953154629829488841. / 210899767478400000000000.) ==
            Catch::Approx(16).margin(0.01));
  }

  SECTION("degree 15")
  {
    const QuadratureFormula<3>& l15 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(15));

    REQUIRE(l15.numberOfPoints() == 238);

    REQUIRE(integrate(p0, l15) == Catch::Approx(4));
    REQUIRE(integrate(p1, l15) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l15) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l15) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l15) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l15) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l15) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l15) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l15) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l15) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l15) == Catch::Approx(-9456848221657. / 22721371200000));
    REQUIRE(integrate(p11, l15) == Catch::Approx(-4571362439539697. / 7518708288000000));
    REQUIRE(integrate(p12, l15) == Catch::Approx(-491755535075074133. / 1378429852800000000));
    REQUIRE(integrate(p13, l15) == Catch::Approx(-1620413117251976393. / 4135289558400000000));
    REQUIRE(integrate(p14, l15) == Catch::Approx(296918520496968826367. / 827057911680000000000.));
    REQUIRE(integrate(p15, l15) == Catch::Approx(-7727953154629829488841. / 210899767478400000000000.));
    REQUIRE(integrate(p16, l15) != Catch::Approx(-18153283669186101815689. / 527249418696000000000000.));

    REQUIRE(get_order(p16, l15, -18153283669186101815689. / 527249418696000000000000.) == Catch::Approx(16));
  }

  SECTION("degree 16")
  {
    const QuadratureFormula<3>& l16 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(16));

    REQUIRE(l16.numberOfPoints() == 287);

    REQUIRE(integrate(p0, l16) == Catch::Approx(4));
    REQUIRE(integrate(p1, l16) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l16) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l16) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l16) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l16) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l16) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l16) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l16) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l16) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l16) == Catch::Approx(-9456848221657. / 22721371200000));
    REQUIRE(integrate(p11, l16) == Catch::Approx(-4571362439539697. / 7518708288000000));
    REQUIRE(integrate(p12, l16) == Catch::Approx(-491755535075074133. / 1378429852800000000));
    REQUIRE(integrate(p13, l16) == Catch::Approx(-1620413117251976393. / 4135289558400000000));
    REQUIRE(integrate(p14, l16) == Catch::Approx(296918520496968826367. / 827057911680000000000.));
    REQUIRE(integrate(p15, l16) == Catch::Approx(-7727953154629829488841. / 210899767478400000000000.));
    REQUIRE(integrate(p16, l16) == Catch::Approx(-18153283669186101815689. / 527249418696000000000000.));
    REQUIRE(integrate(p17, l16) != Catch::Approx(5157361121064510230030071. / 200354779104480000000000000.));

    // In a weird way, one gets almost 18th order on this test
    REQUIRE(get_order(p17, l16, 5157361121064510230030071. / 200354779104480000000000000.) ==
            Catch::Approx(18).margin(0.1));
  }

  SECTION("degree 17")
  {
    const QuadratureFormula<3>& l17 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(17));

    REQUIRE(l17.numberOfPoints() == 338);

    REQUIRE(integrate(p0, l17) == Catch::Approx(4));
    REQUIRE(integrate(p1, l17) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l17) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l17) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l17) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l17) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l17) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l17) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l17) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l17) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l17) == Catch::Approx(-9456848221657. / 22721371200000));
    REQUIRE(integrate(p11, l17) == Catch::Approx(-4571362439539697. / 7518708288000000));
    REQUIRE(integrate(p12, l17) == Catch::Approx(-491755535075074133. / 1378429852800000000));
    REQUIRE(integrate(p13, l17) == Catch::Approx(-1620413117251976393. / 4135289558400000000));
    REQUIRE(integrate(p14, l17) == Catch::Approx(296918520496968826367. / 827057911680000000000.));
    REQUIRE(integrate(p15, l17) == Catch::Approx(-7727953154629829488841. / 210899767478400000000000.));
    REQUIRE(integrate(p16, l17) == Catch::Approx(-18153283669186101815689. / 527249418696000000000000.));
    REQUIRE(integrate(p17, l17) == Catch::Approx(5157361121064510230030071. / 200354779104480000000000000.));
    REQUIRE(integrate(p18, l17) !=
            Catch::Approx(195337148397715128549413507. / 5609933814925440000000000000.).epsilon(1E-10));

    REQUIRE(get_order(p18, l17, 195337148397715128549413507. / 5609933814925440000000000000.) == Catch::Approx(18));
  }

  SECTION("degree 18")
  {
    const QuadratureFormula<3>& l18 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(18));

    REQUIRE(l18.numberOfPoints() == 396);

    REQUIRE(integrate(p0, l18) == Catch::Approx(4));
    REQUIRE(integrate(p1, l18) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l18) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l18) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l18) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l18) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l18) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l18) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l18) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l18) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l18) == Catch::Approx(-9456848221657. / 22721371200000));
    REQUIRE(integrate(p11, l18) == Catch::Approx(-4571362439539697. / 7518708288000000));
    REQUIRE(integrate(p12, l18) == Catch::Approx(-491755535075074133. / 1378429852800000000));
    REQUIRE(integrate(p13, l18) == Catch::Approx(-1620413117251976393. / 4135289558400000000));
    REQUIRE(integrate(p14, l18) == Catch::Approx(296918520496968826367. / 827057911680000000000.));
    REQUIRE(integrate(p15, l18) == Catch::Approx(-7727953154629829488841. / 210899767478400000000000.));
    REQUIRE(integrate(p16, l18) == Catch::Approx(-18153283669186101815689. / 527249418696000000000000.));
    REQUIRE(integrate(p17, l18) == Catch::Approx(5157361121064510230030071. / 200354779104480000000000000.));
    REQUIRE(integrate(p18, l18) == Catch::Approx(195337148397715128549413507. / 5609933814925440000000000000.));
    REQUIRE(integrate(p19, l18) !=
            Catch::Approx(-417563570921497136922189149. / 14024834537313600000000000000.).epsilon(1E-10));

    // In a weird way, one gets almost 20th order on this test
    REQUIRE(get_order(p19, l18, -417563570921497136922189149. / 14024834537313600000000000000.) ==
            Catch::Approx(20).margin(0.01));
  }

  SECTION("degree 19")
  {
    const QuadratureFormula<3>& l19 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(19));

    REQUIRE(l19.numberOfPoints() == 420);

    REQUIRE(integrate(p0, l19) == Catch::Approx(4));
    REQUIRE(integrate(p1, l19) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l19) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l19) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l19) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l19) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l19) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l19) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l19) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l19) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l19) == Catch::Approx(-9456848221657. / 22721371200000));
    REQUIRE(integrate(p11, l19) == Catch::Approx(-4571362439539697. / 7518708288000000));
    REQUIRE(integrate(p12, l19) == Catch::Approx(-491755535075074133. / 1378429852800000000));
    REQUIRE(integrate(p13, l19) == Catch::Approx(-1620413117251976393. / 4135289558400000000));
    REQUIRE(integrate(p14, l19) == Catch::Approx(296918520496968826367. / 827057911680000000000.));
    REQUIRE(integrate(p15, l19) == Catch::Approx(-7727953154629829488841. / 210899767478400000000000.));
    REQUIRE(integrate(p16, l19) == Catch::Approx(-18153283669186101815689. / 527249418696000000000000.));
    REQUIRE(integrate(p17, l19) == Catch::Approx(5157361121064510230030071. / 200354779104480000000000000.));
    REQUIRE(integrate(p18, l19) == Catch::Approx(195337148397715128549413507. / 5609933814925440000000000000.));
    REQUIRE(integrate(p19, l19) == Catch::Approx(-417563570921497136922189149. / 14024834537313600000000000000.));
    REQUIRE(integrate(p20, l19) !=
            Catch::Approx(4740816174053415637444760963. / 205697573213932800000000000000.).epsilon(1E-10));

    REQUIRE(get_order(p20, l19, 4740816174053415637444760963. / 205697573213932800000000000000.) ==
            Catch::Approx(20).margin(0.01));
  }

  SECTION("degree 20")
  {
    const QuadratureFormula<3>& l20 = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(20));

    REQUIRE(l20.numberOfPoints() == 518);

    REQUIRE(integrate(p0, l20) == Catch::Approx(4));
    REQUIRE(integrate(p1, l20) == Catch::Approx(2. / 3));
    REQUIRE(integrate(p2, l20) == Catch::Approx(47. / 24));
    REQUIRE(integrate(p3, l20) == Catch::Approx(-427. / 360));
    REQUIRE(integrate(p4, l20) == Catch::Approx(1003. / 3600));
    REQUIRE(integrate(p5, l20) == Catch::Approx(34031. / 18000));
    REQUIRE(integrate(p6, l20) == Catch::Approx(36346369. / 55082112));
    REQUIRE(integrate(p7, l20) == Catch::Approx(-1128861017. / 918035200));
    REQUIRE(integrate(p8, l20) == Catch::Approx(-21178319419. / 27541056000));
    REQUIRE(integrate(p9, l20) == Catch::Approx(-5483758803191. / 9088548480000));
    REQUIRE(integrate(p10, l20) == Catch::Approx(-9456848221657. / 22721371200000));
    REQUIRE(integrate(p11, l20) == Catch::Approx(-4571362439539697. / 7518708288000000));
    REQUIRE(integrate(p12, l20) == Catch::Approx(-491755535075074133. / 1378429852800000000));
    REQUIRE(integrate(p13, l20) == Catch::Approx(-1620413117251976393. / 4135289558400000000));
    REQUIRE(integrate(p14, l20) == Catch::Approx(296918520496968826367. / 827057911680000000000.));
    REQUIRE(integrate(p15, l20) == Catch::Approx(-7727953154629829488841. / 210899767478400000000000.));
    REQUIRE(integrate(p16, l20) == Catch::Approx(-18153283669186101815689. / 527249418696000000000000.));
    REQUIRE(integrate(p17, l20) == Catch::Approx(5157361121064510230030071. / 200354779104480000000000000.));
    REQUIRE(integrate(p18, l20) == Catch::Approx(195337148397715128549413507. / 5609933814925440000000000000.));
    REQUIRE(integrate(p19, l20) == Catch::Approx(-417563570921497136922189149. / 14024834537313600000000000000.));
    REQUIRE(integrate(p20, l20) == Catch::Approx(4740816174053415637444760963. / 205697573213932800000000000000.));
    REQUIRE(integrate(p21, l20) !=
            Catch::Approx(-164372186128198750911065614811351. / 7096566275880681600000000000000000.).epsilon(1E-10));

    // In a weird way, one gets almost 22th order on this test
    REQUIRE(get_order(p21, l20, -164372186128198750911065614811351. / 7096566275880681600000000000000000.) ==
            Catch::Approx(22).margin(0.01));
  }

  SECTION("max implemented degree")
  {
    REQUIRE(QuadratureManager::instance().maxPrismDegree(QuadratureType::Gauss) == PrismGaussQuadrature::max_degree);
    REQUIRE_THROWS_WITH(QuadratureManager::instance().getPrismFormula(
                          GaussQuadratureDescriptor(PrismGaussQuadrature ::max_degree + 1)),
                        "error: Gauss quadrature formulae handle degrees up to " +
                          stringify(PrismGaussQuadrature ::max_degree) + " on prisms");
  }
}
