#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Demangle.hpp>

#include <cxxabi.h>

// clazy:excludeall=non-pod-global-static

TEST_CASE("Demangle", "[utils]")
{
  SECTION("demangle success")
  {
    const std::string mangled = typeid(std::string).name();

    int status          = -1;
    char* cxa_demangled = abi::__cxa_demangle(mangled.data(), NULL, NULL, &status);

    REQUIRE(status == 0);

    std::string demangled{cxa_demangled};
    free(cxa_demangled);

    REQUIRE(demangled == demangle<std::string>());
  }

  SECTION("demangle failed")
  {
    const std::string mangled = "not_mangled";

    int status = -1;
    abi::__cxa_demangle(mangled.data(), NULL, NULL, &status);

    REQUIRE(status != 0);

    REQUIRE((std::string{"not_mangled"} == demangle("not_mangled")));
  }
}
