#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/PugsAssert.hpp>

#include <utils/Socket.hpp>

#include <sstream>
#include <thread>

#include <netdb.h>

// clazy:excludeall=non-pod-global-static

TEST_CASE("Socket", "[utils]")
{
  SECTION("create/connect and simple read/write")
  {
    auto self_client = [](int port) {
      Socket self_server      = connectServerSocket("localhost", port);
      std::vector<int> values = {1, 2, 4, 7};
      write(self_server, values.size());
      write(self_server, values);
    };

    Socket server = createServerSocket(0);

    std::thread t(self_client, server.portNumber());

    Socket client = acceptClientSocket(server);

    const size_t vector_size = [&] {
      size_t read_vector_size;
      read(client, read_vector_size);
      return read_vector_size;
    }();

    REQUIRE(vector_size == 4);

    std::vector<int> v(vector_size);
    read(client, v);

    REQUIRE(v == std::vector<int>{1, 2, 4, 7});

    t.join();
  }

  SECTION("move constructor")
  {
    Socket server   = createServerSocket(0);
    int port_number = server.portNumber();

    Socket moved_server(std::move(server));

    REQUIRE(port_number == moved_server.portNumber());
  }

  SECTION("host and port info")
  {
    Socket server   = createServerSocket(0);
    int port_number = server.portNumber();

    std::ostringstream info;
    info << server;

    std::ostringstream expected;
    char hbuf[NI_MAXHOST];
    ::gethostname(hbuf, NI_MAXHOST);
    expected << hbuf << ':' << port_number;

    REQUIRE(info.str() == expected.str());
  }

  SECTION("errors")
  {
    SECTION("connection")
    {
      {
        auto server = createServerSocket(0);
        REQUIRE_THROWS_WITH(createServerSocket(server.portNumber()), "error: Address already in use");
      }
      REQUIRE_THROWS_WITH(connectServerSocket("localhost", 1), "error: Connection refused");

      // The error message is not checked since it can depend on the
      // network connection itself
      REQUIRE_THROWS(connectServerSocket("an invalid host name", 1));
    }

    SECTION("server <-> server")
    {
      Socket server = createServerSocket(0);
      REQUIRE_THROWS_WITH(write(server, 12), "error: Server cannot write to server socket");
      REQUIRE_THROWS_WITH(write(server, std::vector<int>{1, 2, 3}), "error: Server cannot write to server socket");

      double x;
      REQUIRE_THROWS_WITH(read(server, x), "error: Server cannot read from server socket");
      std::vector<double> v(1);
      REQUIRE_THROWS_WITH(read(server, v), "error: Server cannot read from server socket");
    }

    SECTION("invalid read")
    {
      auto self_client = [](int port) { Socket self_server = connectServerSocket("localhost", port); };

      Socket server = createServerSocket(0);

      std::thread t(self_client, server.portNumber());
      Socket client = acceptClientSocket(server);
      t.join();

      double x;
      REQUIRE_THROWS_WITH(read(client, x), "error: Could not read data");
      // One cannot test easily write errors...
    }
  }
}
