#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <analysis/GaussLegendreQuadratureDescriptor.hpp>
#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/CubeTransformation.hpp>
#include <geometry/PyramidTransformation.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("PyramidTransformation", "[geometry]")
{
  using R3 = TinyVector<3>;

  const R3 a_hat{-1, -1, +0};
  const R3 b_hat{+1, -1, +0};
  const R3 c_hat{+1, +1, +0};
  const R3 d_hat{-1, +1, +0};
  const R3 e_hat{+0, +0, +1};

  const R3 m_hat{0, 0, 1. / 5};

  const R3 a{1, 2, 0};
  const R3 b{3, 1, 3};
  const R3 c{2, 5, 2};
  const R3 d{0, 3, 1};
  const R3 e{1, 2, 5};

  const PyramidTransformation t(a, b, c, d, e);

  SECTION("points")
  {
    REQUIRE(l2Norm(t(a_hat) - a) == Catch::Approx(0));
    REQUIRE(l2Norm(t(b_hat) - b) == Catch::Approx(0));
    REQUIRE(l2Norm(t(c_hat) - c) == Catch::Approx(0));
    REQUIRE(l2Norm(t(d_hat) - d) == Catch::Approx(0));
    REQUIRE(l2Norm(t(e_hat) - e) == Catch::Approx(0));

    R3 m = (1. / 5) * (a + b + c + d + e);
    REQUIRE(l2Norm(t(m_hat) - m) == Catch::Approx(0).margin(1E-14));
  }

  SECTION("Jacobian determinant")
  {
    SECTION("at points")
    {
      auto detJ = [](const R3 X) {
        const double& x = X[0];
        const double& y = X[1];

        return (43 * x + 13 * y + 93) / 16;
      };

      REQUIRE(t.jacobianDeterminant(a_hat) == Catch::Approx(detJ(a_hat)));
      REQUIRE(t.jacobianDeterminant(b_hat) == Catch::Approx(detJ(b_hat)));
      REQUIRE(t.jacobianDeterminant(c_hat) == Catch::Approx(detJ(c_hat)));
      REQUIRE(t.jacobianDeterminant(d_hat) == Catch::Approx(detJ(d_hat)));
      REQUIRE(t.jacobianDeterminant(e_hat) == Catch::Approx(detJ(e_hat)));

      REQUIRE(t.jacobianDeterminant(m_hat) == Catch::Approx(detJ(m_hat)));
    }

    SECTION("volume calculation")
    {
      // The jacobian determinant is a degree 1 polynomial
      const QuadratureFormula<3>& gauss = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(1));

      double volume = 0;
      for (size_t i = 0; i < gauss.numberOfPoints(); ++i) {
        volume += gauss.weight(i) * t.jacobianDeterminant(gauss.point(i));
      }

      // 31 / 4 is actually the volume of the pyramid
      REQUIRE(volume == Catch::Approx(31. / 4));
    }

    SECTION("exact polynomial integration")
    {
      auto p = [](const R3& X) {
        const double x = X[0];
        const double y = X[1];
        const double z = X[2];

        return 3 * x * x + 2 * y * y + 3 * z * z + 4 * x + 3 * y + 2 * z + 1;
      };

      // 4 is the minimum quadrature rule to integrate the polynomial on the pyramid
      const QuadratureFormula<3>& gauss = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(4));
      double integral                   = 0;
      for (size_t i = 0; i < gauss.numberOfPoints(); ++i) {
        integral += gauss.weight(i) * t.jacobianDeterminant(gauss.point(i)) * p(t(gauss.point(i)));
      }

      REQUIRE(integral == Catch::Approx(213095. / 448));
    }
  }
}
