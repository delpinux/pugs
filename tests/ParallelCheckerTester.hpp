#ifndef PARALLEL_CHECKER_TESTER_HPP
#define PARALLEL_CHECKER_TESTER_HPP

#include <dev/ParallelChecker.hpp>

class ParallelCheckerTester
{
 public:
  bool isCreated() const;

  std::string getFilename() const;
  ParallelChecker::Mode getMode() const;
  size_t getTag() const;

  void setFilename(const std::string& filename) const;
  void setMode(ParallelChecker::Mode mode) const;
  void setTag(size_t tag) const;
  void setTagWithCheck(size_t tag) const;

  ParallelCheckerTester()  = default;
  ~ParallelCheckerTester() = default;
};

#endif   // PARALLEL_CHECKER_TESTER_HPP
