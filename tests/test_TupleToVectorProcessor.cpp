#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_EVALUATION_RESULT(data, variable_name, expected_value)          \
  {                                                                           \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                \
    auto ast = ASTBuilder::build(input);                                      \
                                                                              \
    ASTExecutionStack::create();                                              \
                                                                              \
    ASTModulesImporter{*ast};                                                 \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                   \
                                                                              \
    ASTSymbolTableBuilder{*ast};                                              \
    ASTNodeDataTypeBuilder{*ast};                                             \
                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                      \
    ASTNodeTypeCleaner<language::fct_declaration>{*ast};                      \
                                                                              \
    ASTNodeExpressionBuilder{*ast};                                           \
    ExecutionPolicy exec_policy;                                              \
    ast->execute(exec_policy);                                                \
                                                                              \
    auto symbol_table = ast->m_symbol_table;                                  \
                                                                              \
    using namespace TAO_PEGTL_NAMESPACE;                                      \
    position use_position{10000, 1000, 10, "fixture"};                        \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);   \
                                                                              \
    auto attributes = symbol->attributes();                                   \
    auto value      = std::get<decltype(expected_value)>(attributes.value()); \
                                                                              \
    ASTExecutionStack::destroy();                                             \
                                                                              \
    REQUIRE(value == expected_value);                                         \
    ast->m_symbol_table->clearValues();                                       \
  }

#define CHECK_EVALUATION_THROWS_WITH(data, error_message)        \
  {                                                              \
    auto eval = [&] {                                            \
      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"}; \
      auto ast = ASTBuilder::build(input);                       \
                                                                 \
      ASTExecutionStack::create();                               \
                                                                 \
      ASTModulesImporter{*ast};                                  \
      ASTNodeTypeCleaner<language::import_instruction>{*ast};    \
                                                                 \
      ASTSymbolTableBuilder{*ast};                               \
      ASTNodeDataTypeBuilder{*ast};                              \
                                                                 \
      ASTNodeDeclarationToAffectationConverter{*ast};            \
      ASTNodeTypeCleaner<language::var_declaration>{*ast};       \
      ASTNodeTypeCleaner<language::fct_declaration>{*ast};       \
                                                                 \
      ASTNodeExpressionBuilder{*ast};                            \
      ExecutionPolicy exec_policy;                               \
      ast->execute(exec_policy);                                 \
    };                                                           \
                                                                 \
    ASTExecutionStack::destroy();                                \
                                                                 \
    REQUIRE_THROWS_WITH(eval(), error_message);                  \
    ast->m_symbol_table->clearValues();                          \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("TupleToVectorProcessor", "[language]")
{
  SECTION("Return tuple -> R^3")
  {
    std::string_view data = R"(
let f: R*R*R->R^3, (x,y,z) -> [y,z,x];
let x:R^3, x = f(1,2,3);
)";
    CHECK_EVALUATION_RESULT(data, "x", (TinyVector<3>{2, 3, 1}));
  }

  SECTION("Return tuple -> R^2")
  {
    std::string_view data = R"(
let f: R^3->R^2, x -> [x[2],x[1]];
let x:R^3, x = [1,2,3];
let y:R^2, y = f(x);
)";
    CHECK_EVALUATION_RESULT(data, "y", (TinyVector<2>{3, 2}));
  }
}
