#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Messenger.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/NamedZoneDescriptor.hpp>
#include <mesh/NumberedZoneDescriptor.hpp>
#include <utils/checkpointing/ReadIZoneDescriptor.hpp>
#include <utils/checkpointing/WriteIZoneDescriptor.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_IZoneDescriptor", "[utils/checkpointing]")
{
  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});
    HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

    SECTION("IZoneDescriptor")
    {
      HighFive::Group symbol_table_group = file.createGroup("symbol_table");
      HighFive::Group useless_group;

      auto p_named_zone_descriptor = std::make_shared<const NamedZoneDescriptor>("zone_name1");
      checkpointing::writeIZoneDescriptor("named_zone_descriptor",
                                          EmbeddedData{std::make_shared<DataHandler<const IZoneDescriptor>>(
                                            p_named_zone_descriptor)},
                                          file, useless_group, symbol_table_group);

      auto p_numbered_zone_descriptor = std::make_shared<const NumberedZoneDescriptor>(3);
      checkpointing::writeIZoneDescriptor("numbered_zone_descriptor",
                                          EmbeddedData{std::make_shared<DataHandler<const IZoneDescriptor>>(
                                            p_numbered_zone_descriptor)},
                                          file, useless_group, symbol_table_group);

      file.flush();

      EmbeddedData read_named_zone_descriptor =
        checkpointing::readIZoneDescriptor("named_zone_descriptor", symbol_table_group);

      EmbeddedData read_numbered_zone_descriptor =
        checkpointing::readIZoneDescriptor("numbered_zone_descriptor", symbol_table_group);

      auto get_value = [](const EmbeddedData& embedded_data) -> const IZoneDescriptor& {
        return *dynamic_cast<const DataHandler<const IZoneDescriptor>&>(embedded_data.get()).data_ptr();
      };

      REQUIRE_NOTHROW(get_value(read_named_zone_descriptor));
      REQUIRE_NOTHROW(get_value(read_numbered_zone_descriptor));

      REQUIRE(get_value(read_named_zone_descriptor).type() == IZoneDescriptor::Type::named);
      REQUIRE(get_value(read_numbered_zone_descriptor).type() == IZoneDescriptor::Type::numbered);

      REQUIRE_NOTHROW(dynamic_cast<const NamedZoneDescriptor&>(get_value(read_named_zone_descriptor)));
      REQUIRE_NOTHROW(dynamic_cast<const NumberedZoneDescriptor&>(get_value(read_numbered_zone_descriptor)));

      auto& read_named    = dynamic_cast<const NamedZoneDescriptor&>(get_value(read_named_zone_descriptor));
      auto& read_numbered = dynamic_cast<const NumberedZoneDescriptor&>(get_value(read_numbered_zone_descriptor));

      REQUIRE(read_named.name() == p_named_zone_descriptor->name());
      REQUIRE(read_numbered.number() == p_numbered_zone_descriptor->number());
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }
}
