#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshFaceInterface.hpp>
#include <mesh/NamedInterfaceDescriptor.hpp>
#include <mesh/NumberedInterfaceDescriptor.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("MeshFaceInterface", "[mesh]")
{
  auto is_same = [](const auto& a, const auto& b) -> bool {
    if (a.size() > 0 and b.size() > 0) {
      return (a.size() == b.size()) and (&(a[0]) == &(b[0]));
    } else {
      return (a.size() == b.size());
    }
  };

  auto get_face_list_from_tag = [](const size_t tag, const auto& connectivity) -> Array<const FaceId> {
    for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::face>(); ++i) {
      const auto& ref_face_list = connectivity.template refItemList<ItemType::face>(i);
      const RefId ref_id        = ref_face_list.refId();
      if (ref_id.tagNumber() == tag) {
        return ref_face_list.list();
      }
    }
    return {};
  };

  auto get_face_list_from_name = [](const std::string& name, const auto& connectivity) -> Array<const FaceId> {
    for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::face>(); ++i) {
      const auto& ref_face_list = connectivity.template refItemList<ItemType::face>(i);
      const RefId ref_id        = ref_face_list.refId();
      if (ref_id.tagName() == name) {
        return ref_face_list.list();
      }
    }
    return {};
  };

  SECTION("1D")
  {
    static constexpr size_t Dimension = 1;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    SECTION("unordered 1d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().unordered1DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {3};

        for (auto tag : tag_set) {
          NumberedInterfaceDescriptor numbered_interface_descriptor(tag);
          const auto& face_interface = getMeshFaceInterface(mesh, numbered_interface_descriptor);

          auto face_list = get_face_list_from_tag(tag, connectivity);
          REQUIRE(is_same(face_interface.faceList(), face_list));
        }
      }

      {
        const std::set<std::string> name_set = {"INTERFACE"};

        for (const auto& name : name_set) {
          NamedInterfaceDescriptor named_interface_descriptor(name);
          const auto& face_interface = getMeshFaceInterface(mesh, named_interface_descriptor);

          auto face_list = get_face_list_from_name(name, connectivity);
          REQUIRE(is_same(face_interface.faceList(), face_list));
        }
      }
    }
  }

  SECTION("2D")
  {
    static constexpr size_t Dimension = 2;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    SECTION("hybrid 2d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid2DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {5};

        for (auto tag : tag_set) {
          NumberedInterfaceDescriptor numbered_interface_descriptor(tag);
          const auto& face_interface = getMeshFaceInterface(mesh, numbered_interface_descriptor);

          auto face_list = get_face_list_from_tag(tag, connectivity);
          REQUIRE(is_same(face_interface.faceList(), face_list));
        }
      }

      {
        const std::set<std::string> name_set = {"INTERFACE"};

        for (const auto& name : name_set) {
          NamedInterfaceDescriptor numbered_interface_descriptor(name);
          const auto& face_interface = getMeshFaceInterface(mesh, numbered_interface_descriptor);

          auto face_list = get_face_list_from_name(name, connectivity);
          REQUIRE(is_same(face_interface.faceList(), face_list));
        }
      }
    }
  }

  SECTION("3D")
  {
    static constexpr size_t Dimension = 3;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    SECTION("hybrid 3d")
    {
      std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid3DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      const ConnectivityType& connectivity = mesh.connectivity();

      {
        const std::set<size_t> tag_set = {55, 56};

        for (auto tag : tag_set) {
          NumberedInterfaceDescriptor numbered_interface_descriptor(tag);
          const auto& face_interface = getMeshFaceInterface(mesh, numbered_interface_descriptor);

          auto face_list = get_face_list_from_tag(tag, connectivity);
          REQUIRE(is_same(face_interface.faceList(), face_list));
        }
      }

      {
        const std::set<std::string> name_set = {"INTERFACE1", "INTERFACE2"};

        for (const auto& name : name_set) {
          NamedInterfaceDescriptor numbered_interface_descriptor(name);
          const auto& face_interface = getMeshFaceInterface(mesh, numbered_interface_descriptor);

          auto face_list = get_face_list_from_name(name, connectivity);
          REQUIRE(is_same(face_interface.faceList(), face_list));
        }
      }
    }
  }

  SECTION("errors")
  {
    SECTION("cannot find interface")
    {
      static constexpr size_t Dimension = 3;

      using MeshType = Mesh<Dimension>;

      std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid3DMesh();
      const MeshType& mesh   = *p_mesh->get<MeshType>();

      NamedInterfaceDescriptor named_interface_descriptor("invalid_interface");

      REQUIRE_THROWS_WITH(getMeshFaceInterface(mesh, named_interface_descriptor),
                          "error: cannot find face list with name \"invalid_interface\"");
    }

    SECTION("surface is inside")
    {
      SECTION("1D")
      {
        static constexpr size_t Dimension = 1;

        using MeshType = Mesh<Dimension>;

        std::shared_ptr p_mesh = MeshDataBaseForTests::get().unordered1DMesh();
        const MeshType& mesh   = *p_mesh->get<MeshType>();

        NamedInterfaceDescriptor named_interface_descriptor("XMIN");

        REQUIRE_THROWS_WITH(getMeshFaceInterface(mesh, named_interface_descriptor),
                            "error: invalid interface \"XMIN\": boundary faces cannot be used to define mesh "
                            "interfaces");
      }

      SECTION("2D")
      {
        static constexpr size_t Dimension = 2;

        using MeshType = Mesh<Dimension>;

        std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid2DMesh();
        const MeshType& mesh   = *p_mesh->get<MeshType>();

        NamedInterfaceDescriptor named_interface_descriptor("XMIN");

        REQUIRE_THROWS_WITH(getMeshFaceInterface(mesh, named_interface_descriptor),
                            "error: invalid interface \"XMIN\": boundary faces cannot be used to define mesh "
                            "interfaces");
      }

      SECTION("3D")
      {
        static constexpr size_t Dimension = 3;

        using MeshType = Mesh<Dimension>;

        std::shared_ptr p_mesh = MeshDataBaseForTests::get().hybrid3DMesh();
        const MeshType& mesh   = *p_mesh->get<MeshType>();

        NamedInterfaceDescriptor named_interface_descriptor("XMIN");

        REQUIRE_THROWS_WITH(getMeshFaceInterface(mesh, named_interface_descriptor),
                            "error: invalid interface \"XMIN\": boundary faces cannot be used to define mesh "
                            "interfaces");
      }
    }
  }
}
