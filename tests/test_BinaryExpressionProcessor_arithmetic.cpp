#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <FixturesForBuiltinT.hpp>

#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/BinaryOperatorProcessorBuilder.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <language/utils/TypeDescriptor.hpp>

#include <test_BinaryExpressionProcessor_utils.hpp>

// clazy:excludeall=non-pod-global-static

#define CHECK_BUILTIN_BINARY_EXPRESSION_RESULT(data, result)                                                    \
  {                                                                                                             \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                                  \
    auto ast = ASTBuilder::build(input);                                                                        \
                                                                                                                \
    ASTModulesImporter{*ast};                                                                                   \
                                                                                                                \
    BasicAffectationRegisterFor<EmbeddedData>{ASTNodeDataType::build<ASTNodeDataType::type_id_t>("builtin_t")}; \
                                                                                                                \
    OperatorRepository& repository = OperatorRepository::instance();                                            \
                                                                                                                \
    repository.addBinaryOperator<language::plus_op>(                                                            \
      std::make_shared<                                                                                         \
        BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const double>,                        \
                                       std::shared_ptr<const double>, std::shared_ptr<const double>>>());       \
                                                                                                                \
    repository.addBinaryOperator<language::plus_op>(                                                            \
      std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const double>,         \
                                                      std::shared_ptr<const double>, double>>());               \
                                                                                                                \
    repository.addBinaryOperator<language::plus_op>(                                                            \
      std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const double>, double, \
                                                      std::shared_ptr<const double>>>());                       \
                                                                                                                \
    SymbolTable& symbol_table = *ast->m_symbol_table;                                                           \
    auto [i_symbol, success]  = symbol_table.add(builtin_data_type.nameOfTypeId(), ast->begin());               \
    if (not success) {                                                                                          \
      throw UnexpectedError("cannot add '" + builtin_data_type.nameOfTypeId() + "' type for testing");          \
    }                                                                                                           \
                                                                                                                \
    i_symbol->attributes().setDataType(ASTNodeDataType::build<ASTNodeDataType::type_name_id_t>());              \
    i_symbol->attributes().setIsInitialized();                                                                  \
    i_symbol->attributes().value() = symbol_table.typeEmbedderTable().size();                                   \
    symbol_table.typeEmbedderTable().add(std::make_shared<TypeDescriptor>(builtin_data_type.nameOfTypeId()));   \
                                                                                                                \
    auto [i_symbol_bt_a, success_bt_a] = symbol_table.add("bt_a", ast->begin());                                \
    if (not success_bt_a) {                                                                                     \
      throw UnexpectedError("cannot add 'bt_a' of type builtin_t for testing");                                 \
    }                                                                                                           \
    i_symbol_bt_a->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);            \
    i_symbol_bt_a->attributes().setIsInitialized();                                                             \
    i_symbol_bt_a->attributes().value() =                                                                       \
      EmbeddedData(std::make_shared<DataHandler<const double>>(std::make_shared<double>(3.2)));                 \
                                                                                                                \
    auto [i_symbol_bt_b, success_bt_b] = symbol_table.add("bt_b", ast->begin());                                \
    if (not success_bt_b) {                                                                                     \
      throw UnexpectedError("cannot add 'bt_b' of type builtin_t for testing");                                 \
    }                                                                                                           \
    i_symbol_bt_b->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);            \
    i_symbol_bt_b->attributes().setIsInitialized();                                                             \
    i_symbol_bt_b->attributes().value() =                                                                       \
      EmbeddedData(std::make_shared<DataHandler<const double>>(std::make_shared<double>(5.3)));                 \
                                                                                                                \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                                     \
                                                                                                                \
    ASTSymbolTableBuilder{*ast};                                                                                \
    ASTNodeDataTypeBuilder{*ast};                                                                               \
                                                                                                                \
    ASTNodeDeclarationToAffectationConverter{*ast};                                                             \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                                        \
                                                                                                                \
    ASTNodeExpressionBuilder{*ast};                                                                             \
    ExecutionPolicy exec_policy;                                                                                \
    ASTExecutionStack::create();                                                                                \
    ast->execute(exec_policy);                                                                                  \
    ASTExecutionStack::destroy();                                                                               \
                                                                                                                \
    using namespace TAO_PEGTL_NAMESPACE;                                                                        \
    position use_position{10000, 1000, 10, "fixture"};                                                          \
    auto [symbol, found] = symbol_table.find("r", use_position);                                                \
                                                                                                                \
    auto attributes     = symbol->attributes();                                                                 \
    auto embedded_value = std::get<EmbeddedData>(attributes.value());                                           \
                                                                                                                \
    double value = *dynamic_cast<const DataHandler<const double>&>(embedded_value.get()).data_ptr();            \
    REQUIRE(value == expected);                                                                                 \
    ast->m_symbol_table->clearValues();                                                                         \
  }

#define CHECK_BUILTIN_BINARY_EXPRESSION_ERROR(data, error_msg)                                                    \
  {                                                                                                               \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                                    \
    auto ast = ASTBuilder::build(input);                                                                          \
                                                                                                                  \
    ASTModulesImporter{*ast};                                                                                     \
                                                                                                                  \
    BasicAffectationRegisterFor<EmbeddedData>{ASTNodeDataType::build<ASTNodeDataType::type_id_t>("builtin_t")};   \
                                                                                                                  \
    OperatorRepository& repository = OperatorRepository::instance();                                              \
                                                                                                                  \
    repository.addBinaryOperator<language::divide_op>(                                                            \
      std::make_shared<                                                                                           \
        BinaryOperatorProcessorBuilder<language::divide_op, std::shared_ptr<const double>,                        \
                                       std::shared_ptr<const double>, std::shared_ptr<const double>>>());         \
                                                                                                                  \
    repository.addBinaryOperator<language::divide_op>(                                                            \
      std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, std::shared_ptr<const double>,         \
                                                      std::shared_ptr<const double>, double>>());                 \
                                                                                                                  \
    repository.addBinaryOperator<language::divide_op>(                                                            \
      std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, std::shared_ptr<const double>, double, \
                                                      std::shared_ptr<const double>>>());                         \
                                                                                                                  \
    SymbolTable& symbol_table = *ast->m_symbol_table;                                                             \
    auto [i_symbol, success]  = symbol_table.add(builtin_data_type.nameOfTypeId(), ast->begin());                 \
    if (not success) {                                                                                            \
      throw UnexpectedError("cannot add '" + builtin_data_type.nameOfTypeId() + "' type for testing");            \
    }                                                                                                             \
                                                                                                                  \
    i_symbol->attributes().setDataType(ASTNodeDataType::build<ASTNodeDataType::type_name_id_t>());                \
    i_symbol->attributes().setIsInitialized();                                                                    \
    i_symbol->attributes().value() = symbol_table.typeEmbedderTable().size();                                     \
    symbol_table.typeEmbedderTable().add(std::make_shared<TypeDescriptor>(builtin_data_type.nameOfTypeId()));     \
                                                                                                                  \
    auto [i_symbol_bt_a, success_bt_a] = symbol_table.add("bt_a", ast->begin());                                  \
    if (not success_bt_a) {                                                                                       \
      throw UnexpectedError("cannot add 'bt_a' of type builtin_t for testing");                                   \
    }                                                                                                             \
    i_symbol_bt_a->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);              \
    i_symbol_bt_a->attributes().setIsInitialized();                                                               \
    i_symbol_bt_a->attributes().value() =                                                                         \
      EmbeddedData(std::make_shared<DataHandler<const double>>(std::make_shared<double>(3.2)));                   \
                                                                                                                  \
    auto [i_symbol_bt_b, success_bt_b] = symbol_table.add("bt_b", ast->begin());                                  \
    if (not success_bt_b) {                                                                                       \
      throw UnexpectedError("cannot add 'bt_b' of type builtin_t for testing");                                   \
    }                                                                                                             \
    i_symbol_bt_b->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);              \
    i_symbol_bt_b->attributes().setIsInitialized();                                                               \
    i_symbol_bt_b->attributes().value() =                                                                         \
      EmbeddedData(std::make_shared<DataHandler<const double>>(std::make_shared<double>(5.3)));                   \
                                                                                                                  \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                                       \
                                                                                                                  \
    ASTSymbolTableBuilder{*ast};                                                                                  \
    ASTNodeDataTypeBuilder{*ast};                                                                                 \
                                                                                                                  \
    ASTNodeDeclarationToAffectationConverter{*ast};                                                               \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                                          \
                                                                                                                  \
    ASTNodeExpressionBuilder{*ast};                                                                               \
    ExecutionPolicy exec_policy;                                                                                  \
    ASTExecutionStack::create();                                                                                  \
    REQUIRE_THROWS_WITH(ast->execute(exec_policy), error_msg);                                                    \
    ASTExecutionStack::destroy();                                                                                 \
    ast->m_symbol_table->clearValues();                                                                           \
  }

TEST_CASE("BinaryExpressionProcessor arithmetic", "[language]")
{
  SECTION("+")
  {
    SECTION("lhs is B")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = true + true;)", "z", 2l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; n = true + 1;)", "n", 2ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; n = true + 1;)", "n", 2ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; n = false + 1;)", "n", 1ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = true + 3;)", "z", 4l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = false + 3;)", "z", 3l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = false + 3.6;)", "r", 3.6);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let s:R, s = -3.3; let r:R, r = true + s;)", "r", -2.3);
    }

    SECTION("lhs is N")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; n = n + true;)", "n", 2ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let m:N, m = 2; n = n + m;)", "n", 6ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let z:Z, z = -1; n = n + z;)", "n", 3ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; let x:R, x = n + 2.3;)", "x", 3.3);
    }

    SECTION("lhs is Z")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = -1 + true;)", "z", 0l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let z:Z, z = -3 + n;)", "z", 1l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let z:Z, z = (-3 + n);)", "z", -1l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = 1 + 2;)", "z", 3l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let x:R, x = 3 + 2.5;)", "x", 5.5);
    }

    SECTION("lhs is R")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = -1.2 + true;)", "r", (-1.2 + true));
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let r:R, r = (-1.2 + n);)", "r", (-1.2 + uint64_t{2}));
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = -1.2 + 1;)", "r", (-1.2 + 1));
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = -1.2 + 2.3;)", "r", (-1.2 + 2.3));
    }
  }

  SECTION("-")
  {
    SECTION("lhs is B")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = true - false;)", "n", 1ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 1; n = true - n;)", "n", 0ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = false - 1;)", "z", -1l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = true - 1.2;)", "r", (true - 1.2));
    }

    SECTION("lhs is N")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 3; n = n - true;)", "n", 2ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let m:N, m = 2; n = n - m;)", "n", 2ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 5; n = n - 4;)", "n", 1ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 3; let x:R, x = n - 2.3;)", "x", double{3ul - 2.3});
    }

    SECTION("lhs is Z")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = -1 - true;)", "z", -2l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let z:Z, z = (-3 - n + 6);)", "z", -1l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = 7 - 2;)", "z", 5l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let x:R, x = 4 - 2.5;)", "x", 1.5);
    }

    SECTION("lhs is R")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = -1.2 - true;)", "r", (-1.2 - true));
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let r:R, r = -1.2 - n;)", "r", (-1.2 - uint64_t{2}));
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = -1.2 - 1;)", "r", (-1.2 - 1));
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = -1.2 - 2.3;)", "r", (-1.2 - 2.3));
    }
  }

  SECTION("*")
  {
    SECTION("lhs is B")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = true * false;)", "n", 0ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; n = true * n;)", "n", 2ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = false * 1;)", "z", 0l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = true * 1.2;)", "r", (true * 1.2));
    }

    SECTION("lhs is N")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 3; n = n * true;)", "n", 3ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let m:N, m = 2; n = n * m;)", "n", 8ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 5; n = n * 4;)", "n", 20ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 3; let x:R, x = n * 2.3;)", "x", double{3ul * 2.3});
    }

    SECTION("lhs is Z")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = -1 * true;)", "z", -1l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let z:Z, z = 3 * n;)", "z", 12l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = 7 * 2;)", "z", 14l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let x:R, x = 4 * 2.4;)", "x", double{4l * 2.4});
    }

    SECTION("lhs is R")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = -1.2 * true;)", "r", (-1.2 * true));
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let r:R, r = -1.2 * n;)", "r", (-1.2 * uint64_t{2}));
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = -1.2 * 11;)", "r", (-1.2 * 11));
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = -1.2 * 2.3;)", "r", (-1.2 * 2.3));
    }
  }

  SECTION("/")
  {
    SECTION("lhs is B")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; n = true / n;)", "n", 0ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = false / 1;)", "z", 0l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = true / 1.2;)", "r", (true / 1.2));
    }

    SECTION("lhs is N")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let m:N, m = 2; n = n / m;)", "n", 2ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 5; n = n / 4;)", "n", 1ul);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 3; let x:R, x = n / 2.3;)", "x", double{3ul / 2.3});
    }

    SECTION("lhs is Z")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 4; let z:Z, z = 3 / n;)", "z", 0l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let z:Z, z = 7 / 2;)", "z", 3l);
      CHECK_BINARY_EXPRESSION_RESULT(R"(let x:R, x = 4 / 2.4;)", "x", double{4l / 2.4});
    }

    SECTION("lhs is R")
    {
      CHECK_BINARY_EXPRESSION_RESULT(R"(let n:N, n = 2; let r:R, r = -1.2 / n;)", "r", (-1.2 / uint64_t{2}));
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = -1.2 / 11;)", "r", (-1.2 / 11));
      CHECK_BINARY_EXPRESSION_RESULT(R"(let r:R, r = -1.2 / 2.3;)", "r", (-1.2 / 2.3));
    }
  }

  SECTION("binary operator [builtin]")
  {
    SECTION("builtin both side")
    {
      std::string_view data = R"(let r:builtin_t, r = bt_a + bt_b;)";
      const double expected = double{3.2} + double{5.3};

      CHECK_BUILTIN_BINARY_EXPRESSION_RESULT(data, expected);
    }

    SECTION("builtin lhs")
    {
      std::string_view data = R"(let r:builtin_t, r = bt_a + 5.;)";
      const double expected = double{3.2} + double{5};

      CHECK_BUILTIN_BINARY_EXPRESSION_RESULT(data, expected);
    }

    SECTION("builtin rhs")
    {
      std::string_view data = R"(let r:builtin_t, r = 5. + bt_a;)";
      const double expected = double{3.2} + double{5};

      CHECK_BUILTIN_BINARY_EXPRESSION_RESULT(data, expected);
    }

    SECTION("runtime error")
    {
      std::string_view data_both = R"(let r:builtin_t, r = bt_a / bt_b;)";
      CHECK_BUILTIN_BINARY_EXPRESSION_ERROR(data_both, "runtime error both");

      std::string_view data_lhs = R"(let r:builtin_t, r = bt_a / 2.3;)";
      CHECK_BUILTIN_BINARY_EXPRESSION_ERROR(data_lhs, "runtime error lhs");

      std::string_view data_rhs = R"(let r:builtin_t, r = 2.3/ bt_a;)";
      CHECK_BUILTIN_BINARY_EXPRESSION_ERROR(data_rhs, "runtime error rhs");
    }
  }
}
