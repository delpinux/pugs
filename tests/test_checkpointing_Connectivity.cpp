#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Messenger.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshVariant.hpp>
#include <utils/GlobalVariableManager.hpp>
#include <utils/checkpointing/ResumingData.hpp>
#include <utils/checkpointing/WriteConnectivity.hpp>

#include <MeshDataBaseForTests.hpp>
#include <checkpointing_Connectivity_utilities.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_Connectivity", "[utils/checkpointing]")
{
  std::string tmp_dirname;
  {
    std::map<size_t, size_t> connectivity_id_map;

    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});
    HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

    const size_t initial_connectivity_id = GlobalVariableManager::instance().getConnectivityId();

    SECTION("Connectivity")
    {
      HighFive::Group checkpoint_group_0 = file.createGroup("checkpoint_0");
      HighFive::Group checkpoint_group_1 = file.createGroup("checkpoint_1");

      {   // Write
        auto mesh_1d = MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>();

        // creates artificially holes in numbering
        test_only::duplicateConnectivity(mesh_1d->connectivity());

        auto new_connectivity_1d = test_only::duplicateConnectivity(mesh_1d->connectivity());
        checkpointing::writeConnectivity(*new_connectivity_1d, file, checkpoint_group_0);
        checkpointing::writeConnectivity(*new_connectivity_1d, file, checkpoint_group_1);
        connectivity_id_map[mesh_1d->connectivity().id()] = new_connectivity_1d->id();

        auto mesh_2d = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>();

        // creates artificially holes in numbering
        test_only::duplicateConnectivity(mesh_2d->connectivity());
        test_only::duplicateConnectivity(mesh_2d->connectivity());

        auto new_connectivity_2d = test_only::duplicateConnectivity(mesh_2d->connectivity());

        checkpointing::writeConnectivity(*new_connectivity_2d, file, checkpoint_group_0);
        checkpointing::writeConnectivity(*new_connectivity_2d, file, checkpoint_group_1);
        connectivity_id_map[mesh_2d->connectivity().id()] = new_connectivity_2d->id();

        HighFive::Group global_variables_group_0 = checkpoint_group_0.createGroup("singleton/global_variables");
        global_variables_group_0.createAttribute("connectivity_id",
                                                 GlobalVariableManager::instance().getConnectivityId());
        global_variables_group_0.createAttribute("mesh_id", GlobalVariableManager::instance().getMeshId());

        auto mesh_3d = MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>();

        // creates artificially holes in numbering
        test_only::duplicateConnectivity(mesh_3d->connectivity());

        auto new_connectivity_3d = test_only::duplicateConnectivity(mesh_3d->connectivity());
        checkpointing::writeConnectivity(*new_connectivity_3d, file, checkpoint_group_1);
        connectivity_id_map[mesh_3d->connectivity().id()] = new_connectivity_3d->id();

        // creates artificially holes in numbering
        test_only::duplicateConnectivity(mesh_3d->connectivity());
        test_only::duplicateConnectivity(mesh_3d->connectivity());
        test_only::duplicateConnectivity(mesh_3d->connectivity());

        HighFive::Group global_variables_group_1 = checkpoint_group_1.createGroup("singleton/global_variables");
        global_variables_group_1.createAttribute("connectivity_id",
                                                 GlobalVariableManager::instance().getConnectivityId());
        global_variables_group_1.createAttribute("mesh_id", GlobalVariableManager::instance().getMeshId());
      }

      // reset to reuse after resuming
      GlobalVariableManager::instance().setConnectivityId(initial_connectivity_id);

      file.flush();

      checkpointing::ResumingData::create();
      checkpointing::ResumingData::instance().readData(checkpoint_group_1, nullptr);

      GlobalVariableManager::instance().setConnectivityId(initial_connectivity_id);
      {   // Read

        auto mesh_1d = MeshDataBaseForTests::get().unordered1DMesh()->get<Mesh<1>>();

        const Connectivity<1>& connectivity_1d = mesh_1d->connectivity();

        std::shared_ptr<const IConnectivity> p_new_connectivity_1d =
          checkpointing::ResumingData::instance().iConnectivity(connectivity_id_map.at(connectivity_1d.id()));

        const Connectivity<1>& read_connectivity_1d = dynamic_cast<const Connectivity<1>&>(*p_new_connectivity_1d);

        REQUIRE(test_only::isSameConnectivity(connectivity_1d, read_connectivity_1d));

        auto mesh_2d = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>();

        const Connectivity<2>& connectivity_2d = mesh_2d->connectivity();

        std::shared_ptr<const IConnectivity> p_new_connectivity_2d =
          checkpointing::ResumingData::instance().iConnectivity(connectivity_id_map.at(connectivity_2d.id()));

        const Connectivity<2>& read_connectivity_2d = dynamic_cast<const Connectivity<2>&>(*p_new_connectivity_2d);

        REQUIRE(test_only::isSameConnectivity(connectivity_2d, read_connectivity_2d));

        auto mesh_3d = MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>();

        const Connectivity<3>& connectivity_3d = mesh_3d->connectivity();

        std::shared_ptr<const IConnectivity> p_new_connectivity_3d =
          checkpointing::ResumingData::instance().iConnectivity(connectivity_id_map.at(connectivity_3d.id()));

        const Connectivity<3>& read_connectivity_3d = dynamic_cast<const Connectivity<3>&>(*p_new_connectivity_3d);

        REQUIRE(test_only::isSameConnectivity(connectivity_3d, read_connectivity_3d));
      }
      checkpointing::ResumingData::destroy();
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }
}
