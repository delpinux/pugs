#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/EscapedString.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("EscapedString", "[utils]")
{
  SECTION("escape string")
  {
    const std::string s = "foo\'\\\"\?\a\b\f\n\r\t\vbar";

    REQUIRE(escapeString(s) == R"(foo\'\\\"\?\a\b\f\n\r\t\vbar)");
  }

  SECTION("unescape string")
  {
    const std::string s = R"(foo\'\\\"\?\a\b\f\n\r\t\vbar)";

    REQUIRE(unescapeString(s) == std::string{"foo\'\\\"\?\a\b\f\n\r\t\vbar"});
  }
}
