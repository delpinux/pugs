#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/DualMeshManager.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>

#include <cmath>

// clazy:excludeall=non-pod-global-static

TEST_CASE("Dual1DMeshBuilder", "[mesh]")
{
  constexpr static size_t Dimension = 1;

  using MeshType = Mesh<Dimension>;

  std::shared_ptr primal_mesh_v = MeshDataBaseForTests::get().unordered1DMesh();

  const MeshType& primal_mesh = *primal_mesh_v->get<MeshType>();

  REQUIRE(primal_mesh.numberOfNodes() == 35);
  REQUIRE(primal_mesh.numberOfCells() == 34);

  std::shared_ptr dual_1d_mesh_v = DualMeshManager::instance().getDual1DMesh(primal_mesh_v);
  const MeshType& dual_mesh      = *dual_1d_mesh_v->get<MeshType>();

  REQUIRE(dual_mesh.numberOfNodes() == 36);
  REQUIRE(dual_mesh.numberOfCells() == 35);

  auto cell_volume = MeshDataManager::instance().getMeshData(dual_mesh).Vj();
  REQUIRE(Catch::Approx(sum(cell_volume)) == 2);

  auto primal_xr = primal_mesh.xr();
  auto dual_xr   = dual_mesh.xr();

  NodeId primal_node_id = std::numeric_limits<NodeId::base_type>::max();
  for (size_t i = 0; i < primal_mesh.connectivity().numberOfRefItemList<ItemType::node>(); ++i) {
    auto ref_item_list = primal_mesh.connectivity().refItemList<ItemType::node>(0);
    if (ref_item_list.refId().tagName() == "XMIN") {
      REQUIRE(ref_item_list.list().size() == 1);
      primal_node_id = ref_item_list.list()[0];
    }
  }

  REQUIRE(primal_node_id != std::numeric_limits<NodeId::base_type>::max());

  NodeId dual_node_id = std::numeric_limits<NodeId::base_type>::max();
  for (size_t i = 0; i < dual_mesh.connectivity().numberOfRefItemList<ItemType::node>(); ++i) {
    auto ref_item_list = dual_mesh.connectivity().refItemList<ItemType::node>(0);
    if (ref_item_list.refId().tagName() == "XMIN") {
      REQUIRE(ref_item_list.list().size() == 1);
      dual_node_id = ref_item_list.list()[0];
    }
  }

  REQUIRE(dual_node_id != std::numeric_limits<NodeId::base_type>::max());

  REQUIRE(dual_xr[dual_node_id] == primal_xr[primal_node_id]);

  auto primal_node_to_cell_matrix = primal_mesh.connectivity().nodeToCellMatrix();
  auto dual_node_to_cell_matrix   = dual_mesh.connectivity().nodeToCellMatrix();

  REQUIRE(primal_node_to_cell_matrix[primal_node_id].size() == 1);
  REQUIRE(dual_node_to_cell_matrix[dual_node_id].size() == 1);

  auto primal_xj = MeshDataManager::instance().getMeshData(primal_mesh).xj();

  auto primal_cell_to_node_matrix = primal_mesh.connectivity().cellToNodeMatrix();
  auto dual_cell_to_node_matrix   = dual_mesh.connectivity().cellToNodeMatrix();

  CellId primal_cell_id = primal_node_to_cell_matrix[primal_node_id][0];
  CellId dual_cell_id   = dual_node_to_cell_matrix[dual_node_id][0];

  bool finished = false;
  do {
    NodeId right_dual_node_id   = dual_cell_to_node_matrix[dual_cell_id][1];
    NodeId right_primal_node_id = primal_cell_to_node_matrix[primal_cell_id][1];
    REQUIRE(right_dual_node_id != dual_node_id);
    REQUIRE(primal_xj[primal_cell_id] == dual_xr[right_dual_node_id]);

    dual_node_id = right_dual_node_id;
    dual_cell_id = [&] {
      const size_t i = (dual_node_to_cell_matrix[dual_node_id][0] == dual_cell_id) ? 1 : 0;
      return dual_node_to_cell_matrix[dual_node_id][i];
    }();

    if (primal_node_to_cell_matrix[right_primal_node_id].size() == 1) {
      right_dual_node_id = dual_cell_to_node_matrix[dual_cell_id][1];
      REQUIRE(primal_xr[right_primal_node_id] == dual_xr[right_dual_node_id]);
      finished = true;
    } else {
      primal_cell_id = [&] {
        const size_t i = (primal_node_to_cell_matrix[right_primal_node_id][0] == primal_cell_id) ? 1 : 0;
        return primal_node_to_cell_matrix[right_primal_node_id][i];
      }();
      primal_node_id = right_primal_node_id;
    }
  } while (not finished);
}
