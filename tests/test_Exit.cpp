#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/Exit.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("Exit", "[language/utils]")
{
  language::Exit exit0 = language::Exit{0};

  REQUIRE(exit0.code() == 0);

  language::Exit exit1{1};
  REQUIRE(exit1.code() == 1);

  language::Exit exit_move = std::move(exit1);
  REQUIRE(exit_move.code() == 1);
}
