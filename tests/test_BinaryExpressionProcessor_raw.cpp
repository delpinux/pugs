#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/node_processor/BinaryExpressionProcessor.hpp>
#include <language/utils/OFStream.hpp>
#include <utils/Stringify.hpp>

#include <utils/pugs_config.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("BinaryExpressionProcessor raw operators", "[language]")
{
  REQUIRE(BinOp<language::and_op>{}.eval(true, true) == true);
  REQUIRE(BinOp<language::and_op>{}.eval(true, false) == false);
  REQUIRE(BinOp<language::and_op>{}.eval(false, true) == false);
  REQUIRE(BinOp<language::and_op>{}.eval(false, false) == false);

  REQUIRE(BinOp<language::or_op>{}.eval(true, true) == true);
  REQUIRE(BinOp<language::or_op>{}.eval(true, false) == true);
  REQUIRE(BinOp<language::or_op>{}.eval(false, true) == true);
  REQUIRE(BinOp<language::or_op>{}.eval(false, false) == false);

  REQUIRE(BinOp<language::xor_op>{}.eval(true, true) == false);
  REQUIRE(BinOp<language::xor_op>{}.eval(true, false) == true);
  REQUIRE(BinOp<language::xor_op>{}.eval(false, true) == true);
  REQUIRE(BinOp<language::xor_op>{}.eval(false, false) == false);

  REQUIRE(BinOp<language::eqeq_op>{}.eval(3, 3) == true);
  REQUIRE(BinOp<language::eqeq_op>{}.eval(2, 3) == false);

  REQUIRE(BinOp<language::not_eq_op>{}.eval(3, 3) == false);
  REQUIRE(BinOp<language::not_eq_op>{}.eval(2, 3) == true);

  REQUIRE(BinOp<language::lesser_op>{}.eval(2.9, 3) == true);
  REQUIRE(BinOp<language::lesser_op>{}.eval(3, 3) == false);
  REQUIRE(BinOp<language::lesser_op>{}.eval(3.1, 3) == false);

  REQUIRE(BinOp<language::lesser_or_eq_op>{}.eval(2.9, 3) == true);
  REQUIRE(BinOp<language::lesser_or_eq_op>{}.eval(3, 3) == true);
  REQUIRE(BinOp<language::lesser_or_eq_op>{}.eval(3.1, 3) == false);

  REQUIRE(BinOp<language::greater_op>{}.eval(2.9, 3) == false);
  REQUIRE(BinOp<language::greater_op>{}.eval(3, 3) == false);
  REQUIRE(BinOp<language::greater_op>{}.eval(3.1, 3) == true);

  REQUIRE(BinOp<language::greater_or_eq_op>{}.eval(2.9, 3) == false);
  REQUIRE(BinOp<language::greater_or_eq_op>{}.eval(3, 3) == true);
  REQUIRE(BinOp<language::greater_or_eq_op>{}.eval(3.1, 3) == true);

  REQUIRE(BinOp<language::plus_op>{}.eval(2.9, 3) == (2.9 + 3));
  REQUIRE(BinOp<language::minus_op>{}.eval(2.9, 3) == (2.9 - 3));
  REQUIRE(BinOp<language::multiply_op>{}.eval(2.9, 3) == (2.9 * 3));
  REQUIRE(BinOp<language::divide_op>{}.eval(2.9, 3) == (2.9 / 3));

  {
    std::filesystem::path path{PUGS_BINARY_DIR};
    path.append("tests").append(std::string{"binary_expression_processor_shift_left_"} + stringify(getpid()));

    std::string filename = path.string();

    std::shared_ptr<const OStream> p_fout = std::make_shared<OFStream>(filename);
    BinOp<language::shift_left_op>{}.eval(p_fout, true);
    BinOp<language::shift_left_op>{}.eval(p_fout, std::string{" bar\n"});
    p_fout.reset();

    REQUIRE(std::filesystem::exists(filename));

    {
      std::string file_content;
      std::ifstream fin(filename.c_str());

      do {
        char c = fin.get();
        if (c != EOF) {
          file_content += c;
        }
      } while (fin);

      REQUIRE(file_content == "true bar\n");
    }

    std::filesystem::remove(filename);
    REQUIRE(not std::filesystem::exists(filename));
  }

  REQUIRE(BinOp<language::shift_left_op>{}.eval(3, 2) == (3 << 2));
  REQUIRE(BinOp<language::shift_right_op>{}.eval(17, 2) == (17 >> 2));
}
