#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <mesh/DualMeshType.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("DualMeshType", "[mesh]")
{
  REQUIRE(name(DualMeshType::Diamond) == "diamond");
  REQUIRE(name(DualMeshType::Dual1D) == "dual 1d");
  REQUIRE(name(DualMeshType::Median) == "median");
  REQUIRE_THROWS_WITH(name(DualMeshType{-1}), "unexpected error: unexpected dual mesh type");
}
