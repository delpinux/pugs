#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/PugsAssert.hpp>
#include <utils/Table.hpp>
#include <utils/Types.hpp>

// Instantiate to ensure full coverage is performed
template class Table<int>;

// clazy:excludeall=non-pod-global-static

TEST_CASE("Table", "[utils]")
{
  Table<int> a(4, 3);
  REQUIRE(a.numberOfRows() == 4);
  REQUIRE(a.numberOfColumns() == 3);
  REQUIRE(a[0].size() == 3);
  REQUIRE(a[1].size() == 3);
  REQUIRE(a[2].size() == 3);
  REQUIRE(a[3].size() == 3);

  for (size_t i = 0; i < a.numberOfRows(); ++i) {
    for (size_t j = 0; j < a.numberOfColumns(); ++j) {
      a(i, j) = 2 * i + j;
    }
  }

  REQUIRE(((a(0, 0) == 0) and (a(1, 0) == 2) and (a(2, 0) == 4) and (a(3, 0) == 6) and   //
           (a(0, 1) == 1) and (a(1, 1) == 3) and (a(2, 1) == 5) and (a(3, 1) == 7) and   //
           (a(0, 2) == 2) and (a(1, 2) == 4) and (a(2, 2) == 6) and (a(3, 2) == 8)));

  SECTION("checking for rows")
  {
    REQUIRE(((a[0][0] == 0) and (a[1][0] == 2) and (a[2][0] == 4) and (a[3][0] == 6) and   //
             (a[0][1] == 1) and (a[1][1] == 3) and (a[2][1] == 5) and (a[3][1] == 7) and   //
             (a[0][2] == 2) and (a[1][2] == 4) and (a[2][2] == 6) and (a[3][2] == 8)));

    a[2][1] = 17;
    REQUIRE(a[2][1] == a(2, 1));
    a[2][1] = 5;
  }

  SECTION("checking for copies")
  {
    Table<const int> b{a};

    REQUIRE(((b(0, 0) == 0) and (b(1, 0) == 2) and (b(2, 0) == 4) and (b(3, 0) == 6) and   //
             (b(0, 1) == 1) and (b(1, 1) == 3) and (b(2, 1) == 5) and (b(3, 1) == 7) and   //
             (b(0, 2) == 2) and (b(1, 2) == 4) and (b(2, 2) == 6) and (b(3, 2) == 8)));

    Table<int> c{a};

    REQUIRE(((c(0, 0) == 0) and (c(1, 0) == 2) and (c(2, 0) == 4) and (c(3, 0) == 6) and   //
             (c(0, 1) == 1) and (c(1, 1) == 3) and (c(2, 1) == 5) and (c(3, 1) == 7) and   //
             (c(0, 2) == 2) and (c(1, 2) == 4) and (c(2, 2) == 6) and (c(3, 2) == 8)));

    Table<int> d = std::move(c);

    REQUIRE(((d(0, 0) == 0) and (d(1, 0) == 2) and (d(2, 0) == 4) and (d(3, 0) == 6) and   //
             (d(0, 1) == 1) and (d(1, 1) == 3) and (d(2, 1) == 5) and (d(3, 1) == 7) and   //
             (d(0, 2) == 2) and (d(1, 2) == 4) and (d(2, 2) == 6) and (d(3, 2) == 8)));

    a(2, 2) = 17;

    REQUIRE(a(2, 2) == 17);
    REQUIRE(b(2, 2) == 17);
    REQUIRE(d(2, 2) == 17);

    a(2, 2) = 6;

    REQUIRE(a(2, 2) == 6);
    REQUIRE(b(2, 2) == 6);
    REQUIRE(d(2, 2) == 6);
  }

  SECTION("checking for fill")
  {
    Table<int> b(3, 2);
    b.fill(3);

    REQUIRE(((b(0, 0) == 3) and (b(1, 0) == 3) and (b(2, 0) == 3) and   //
             (b(0, 1) == 3) and (b(1, 1) == 3) and (b(2, 1) == 3)));
  }

  SECTION("checking for affectations (shallow copy)")
  {
    Table<const int> b;
    b = a;

    REQUIRE(((b(0, 0) == 0) and (b(1, 0) == 2) and (b(2, 0) == 4) and (b(3, 0) == 6) and   //
             (b(0, 1) == 1) and (b(1, 1) == 3) and (b(2, 1) == 5) and (b(3, 1) == 7) and   //
             (b(0, 2) == 2) and (b(1, 2) == 4) and (b(2, 2) == 6) and (b(3, 2) == 8)));

    Table<int> c;
    c = a;

    REQUIRE(((c(0, 0) == 0) and (c(1, 0) == 2) and (c(2, 0) == 4) and (c(3, 0) == 6) and   //
             (c(0, 1) == 1) and (c(1, 1) == 3) and (c(2, 1) == 5) and (c(3, 1) == 7) and   //
             (c(0, 2) == 2) and (c(1, 2) == 4) and (c(2, 2) == 6) and (c(3, 2) == 8)));

    Table<int> d;
    d = std::move(c);

    REQUIRE(((d(0, 0) == 0) and (d(1, 0) == 2) and (d(2, 0) == 4) and (d(3, 0) == 6) and   //
             (d(0, 1) == 1) and (d(1, 1) == 3) and (d(2, 1) == 5) and (d(3, 1) == 7) and   //
             (d(0, 2) == 2) and (d(1, 2) == 4) and (d(2, 2) == 6) and (d(3, 2) == 8)));
  }

  SECTION("checking for affectations (deep copy)")
  {
    Table<int> b(copy(a));

    REQUIRE(((b(0, 0) == 0) and (b(1, 0) == 2) and (b(2, 0) == 4) and (b(3, 0) == 6) and   //
             (b(0, 1) == 1) and (b(1, 1) == 3) and (b(2, 1) == 5) and (b(3, 1) == 7) and   //
             (b(0, 2) == 2) and (b(1, 2) == 4) and (b(2, 2) == 6) and (b(3, 2) == 8)));

    b.fill(2);

    REQUIRE(((b(0, 0) == 2) and (b(1, 0) == 2) and (b(2, 0) == 2) and (b(3, 0) == 2) and   //
             (b(0, 1) == 2) and (b(1, 1) == 2) and (b(2, 1) == 2) and (b(3, 1) == 2) and   //
             (b(0, 2) == 2) and (b(1, 2) == 2) and (b(2, 2) == 2) and (b(3, 2) == 2)));

    REQUIRE(((a(0, 0) == 0) and (a(1, 0) == 2) and (a(2, 0) == 4) and (a(3, 0) == 6) and   //
             (a(0, 1) == 1) and (a(1, 1) == 3) and (a(2, 1) == 5) and (a(3, 1) == 7) and   //
             (a(0, 2) == 2) and (a(1, 2) == 4) and (a(2, 2) == 6) and (a(3, 2) == 8)));

    Table<int> c;
    c = a;

    REQUIRE(((c(0, 0) == 0) and (c(1, 0) == 2) and (c(2, 0) == 4) and (c(3, 0) == 6) and   //
             (c(0, 1) == 1) and (c(1, 1) == 3) and (c(2, 1) == 5) and (c(3, 1) == 7) and   //
             (c(0, 2) == 2) and (c(1, 2) == 4) and (c(2, 2) == 6) and (c(3, 2) == 8)));

    c = copy(b);

    REQUIRE(((c(0, 0) == 2) and (c(1, 0) == 2) and (c(2, 0) == 2) and (c(3, 0) == 2) and   //
             (c(0, 1) == 2) and (c(1, 1) == 2) and (c(2, 1) == 2) and (c(3, 1) == 2) and   //
             (c(0, 2) == 2) and (c(1, 2) == 2) and (c(2, 2) == 2) and (c(3, 2) == 2)));

    Table<int> d{a.numberOfRows(), a.numberOfColumns()};
    copy_to(a, d);

    REQUIRE(((a(0, 0) == 0) and (a(1, 0) == 2) and (a(2, 0) == 4) and (a(3, 0) == 6) and   //
             (a(0, 1) == 1) and (a(1, 1) == 3) and (a(2, 1) == 5) and (a(3, 1) == 7) and   //
             (a(0, 2) == 2) and (a(1, 2) == 4) and (a(2, 2) == 6) and (a(3, 2) == 8)));

    REQUIRE(((d(0, 0) == 0) and (d(1, 0) == 2) and (d(2, 0) == 4) and (d(3, 0) == 6) and   //
             (d(0, 1) == 1) and (d(1, 1) == 3) and (d(2, 1) == 5) and (d(3, 1) == 7) and   //
             (d(0, 2) == 2) and (d(1, 2) == 4) and (d(2, 2) == 6) and (d(3, 2) == 8)));

    copy_to(c, d);

    REQUIRE(((d(0, 0) == 2) and (d(1, 0) == 2) and (d(2, 0) == 2) and (d(3, 0) == 2) and   //
             (d(0, 1) == 2) and (d(1, 1) == 2) and (d(2, 1) == 2) and (d(3, 1) == 2) and   //
             (d(0, 2) == 2) and (d(1, 2) == 2) and (d(2, 2) == 2) and (d(3, 2) == 2)));
  }

  SECTION("checking for Kokkos::View encaspulation")
  {
    Kokkos::View<double**> kokkos_view("anonymous", 10, 3);
    for (size_t i = 0; i < 10; ++i) {
      for (size_t j = 0; j < 3; ++j) {
        kokkos_view(i, j) = 3 * i + j;
      }
    }

    Table table = encapsulate(kokkos_view);

    REQUIRE(table.numberOfRows() == kokkos_view.extent(0));
    REQUIRE(table.numberOfColumns() == kokkos_view.extent(1));
    for (size_t i = 0; i < table.numberOfRows(); ++i) {
      for (size_t j = 0; j < table.numberOfColumns(); ++j) {
        REQUIRE(&table(i, j) == &kokkos_view(i, j));
      }
    }
  }

  SECTION("output")
  {
    Table<double> table(3, 2);
    table(0, 0) = 1;
    table(0, 1) = 3;
    table(1, 0) = 7;
    table(1, 1) = -2;
    table(2, 0) = 4;
    table(2, 1) = -5;
    std::ostringstream table_ost;
    table_ost << table;

    std::ostringstream ref_ost;
    ref_ost << "0| " << 0 << ':' << 1. << ' ' << 1 << ':' << 3. << '\n';
    ref_ost << "1| " << 0 << ':' << 7. << ' ' << 1 << ':' << -2. << '\n';
    ref_ost << "2| " << 0 << ':' << 4. << ' ' << 1 << ':' << -5. << '\n';

    REQUIRE(table_ost.str() == ref_ost.str());
  }

  SECTION("UnsafeTableView")
  {
    Table<int> table(3, 4);
    for (size_t i = 0; i < table.numberOfRows(); ++i) {
      for (size_t j = 0; j < table.numberOfColumns(); ++j) {
        table(i, j) = 2 * i + 3 * j;
      }
    }

    auto sub_table_view = subTableView(table, 1, 2, 1, 3);
    REQUIRE(sub_table_view.numberOfRows() == 2);
    REQUIRE(sub_table_view.numberOfColumns() == 3);

    {
      bool is_same_memory = true;
      for (size_t i = 0; i < sub_table_view.numberOfRows(); ++i) {
        for (size_t j = 0; j < sub_table_view.numberOfColumns(); ++j) {
          is_same_memory &= (&(sub_table_view(i, j)) == &(table(i + 1, j + 1)));
        }
      }
      REQUIRE(is_same_memory);
    }

    for (size_t i = 0; i < sub_table_view.numberOfRows(); ++i) {
      int ip1 = i + 1;
      for (size_t j = 0; j < sub_table_view.numberOfColumns(); ++j) {
        int jp1 = j + 1;
        REQUIRE(sub_table_view(i, j) == 2 * ip1 + 3 * jp1);
      }
    }

    sub_table_view.fill(-2);
    for (size_t i = 0; i < table.numberOfRows(); ++i) {
      int int_i = i;
      for (size_t j = 0; j < table.numberOfColumns(); ++j) {
        int int_j = j;
        if ((i > 0) and (j > 0)) {
          REQUIRE(table(i, j) == -2);
        } else {
          REQUIRE(table(i, j) == 2 * int_i + 3 * int_j);
        }
      }
    }

    for (size_t i = 0; i < sub_table_view.numberOfRows(); ++i) {
      for (size_t j = 0; j < sub_table_view.numberOfColumns(); ++j) {
        sub_table_view(i, j) = -3 * i + 2 * j;
      }
    }

    for (size_t i = 0; i < table.numberOfRows(); ++i) {
      int int_i = i;
      for (size_t j = 0; j < table.numberOfColumns(); ++j) {
        int int_j = j;
        if ((i > 0) and (j > 0)) {
          REQUIRE(table(i, j) == -3 * (int_i - 1) + 2 * (int_j - 1));
        } else {
          REQUIRE(table(i, j) == 2 * int_i + 3 * int_j);
        }
      }
    }
  }

  SECTION("UnsafeTableView::RowView")
  {
    Table<int> table(3, 4);
    for (size_t i = 0; i < table.numberOfRows(); ++i) {
      for (size_t j = 0; j < table.numberOfColumns(); ++j) {
        table(i, j) = 2 * i + 3 * j;
      }
    }

    auto sub_table_view = subTableView(table, 1, 2, 1, 3);

    auto sub_table_row_view = sub_table_view[1];

    REQUIRE(sub_table_row_view.size() == 3);

    {
      bool is_same_memory = true;
      for (size_t i = 0; i < sub_table_row_view.size(); ++i) {
        is_same_memory &= (&(sub_table_row_view[i]) == &(table(2, i + 1)));
      }
      REQUIRE(is_same_memory);
    }

    sub_table_row_view.fill(-5);
    for (size_t i = 0; i < table.numberOfRows(); ++i) {
      int int_i = i;
      for (size_t j = 0; j < table.numberOfColumns(); ++j) {
        int int_j = j;
        if ((i == 2) and (j > 0)) {
          REQUIRE(table(i, j) == -5);
        } else {
          REQUIRE(table(i, j) == 2 * int_i + 3 * int_j);
        }
      }
    }

    for (size_t i = 0; i < sub_table_row_view.size(); ++i) {
      sub_table_row_view[i] = -3 * i + 2;
    }

    for (size_t i = 0; i < table.numberOfRows(); ++i) {
      int int_i = i;
      for (size_t j = 0; j < table.numberOfColumns(); ++j) {
        int int_j = j;
        if ((i == 2) and (j > 0)) {
          REQUIRE(table(i, j) == -3 * (int_j - 1) + 2);
        } else {
          REQUIRE(table(i, j) == 2 * int_i + 3 * int_j);
        }
      }
    }

    std::ostringstream os;
    os << sub_table_view;
    REQUIRE(os.str() == R"(0| 0:5 1:8 2:11
1| 0:2 1:-1 2:-4
)");
  }

  SECTION("UnsafeRowView")
  {
    Table<int> table(3, 4);
    for (size_t i = 0; i < table.numberOfRows(); ++i) {
      for (size_t j = 0; j < table.numberOfColumns(); ++j) {
        table(i, j) = 2 * i + 3 * j;
      }
    }

    auto table_row_view = table[1];

    REQUIRE(table_row_view.size() == table.numberOfColumns());

    {
      bool is_same_memory = true;
      for (size_t i = 0; i < table_row_view.size(); ++i) {
        is_same_memory &= (&(table_row_view[i]) == &(table(1, i)));
      }
      REQUIRE(is_same_memory);
    }

    table_row_view.fill(-5);
    for (size_t i = 0; i < table.numberOfRows(); ++i) {
      int int_i = i;
      for (size_t j = 0; j < table.numberOfColumns(); ++j) {
        int int_j = j;
        if (i == 1) {
          REQUIRE(table(i, j) == -5);
        } else {
          REQUIRE(table(i, j) == 2 * int_i + 3 * int_j);
        }
      }
    }

    for (size_t i = 0; i < table_row_view.size(); ++i) {
      table_row_view[i] = -3 * i + 2;
    }

    for (size_t i = 0; i < table.numberOfRows(); ++i) {
      int int_i = i;
      for (size_t j = 0; j < table.numberOfColumns(); ++j) {
        int int_j = j;
        if (i == 1) {
          REQUIRE(table(i, j) == -3 * int_j + 2);
        } else {
          REQUIRE(table(i, j) == 2 * int_i + 3 * int_j);
        }
      }
    }
  }

#ifndef NDEBUG
  SECTION("output with signaling NaN")
  {
    Table<double> table(3, 2);
    table(0, 0) = 1;
    table(1, 1) = -2;
    table(2, 1) = -5;
    std::ostringstream table_ost;
    table_ost << table;

    std::ostringstream ref_ost;
    ref_ost << "0| " << 0 << ':' << 1. << ' ' << 1 << ":nan\n";
    ref_ost << "1| " << 0 << ":nan " << 1 << ':' << -2. << '\n';
    ref_ost << "2| " << 0 << ":nan " << 1 << ':' << -5. << '\n';

    REQUIRE(table_ost.str() == ref_ost.str());
  }

  SECTION("checking for bounds violation")
  {
    REQUIRE_THROWS_WITH(a(4, 0), "invalid row index");
    REQUIRE_THROWS_WITH(a(0, 3), "invalid column index");
  }

  SECTION("invalid copy_to")
  {
    SECTION("wrong row number")
    {
      Table<int> b{2 * a.numberOfRows(), a.numberOfColumns()};
      REQUIRE_THROWS_WITH(copy_to(a, b), "incompatible number of rows");
    }

    SECTION("wrong column number")
    {
      Table<int> c{a.numberOfRows(), 2 * a.numberOfColumns()};
      REQUIRE_THROWS_WITH(copy_to(a, c), "incompatible number of columns");
    }
  }

  SECTION("checking for nan initialization")
  {
    Table<double> B(3, 4);

    for (size_t i = 0; i < B.numberOfRows(); ++i) {
      for (size_t j = 0; j < B.numberOfColumns(); ++j) {
        REQUIRE(std::isnan(B(i, j)));
      }
    }
  }

  SECTION("checking for bad initialization")
  {
    Table<int> B(4, 2);

    for (size_t i = 0; i < B.numberOfRows(); ++i) {
      for (size_t j = 0; j < B.numberOfColumns(); ++j) {
        REQUIRE(B(i, j) == std::numeric_limits<int>::max() / 2);
      }
    }
  }

  SECTION("invalid UnsafeTableView")
  {
    Table<int> table(3, 4);

    REQUIRE_THROWS_WITH(subTableView(table, 2, 2, 2, 2), "required view has rows not contained in the Table");
    REQUIRE_THROWS_WITH(subTableView(table, 4, 2, 2, 2), "required view has rows not contained in the Table");

    REQUIRE_THROWS_WITH(subTableView(table, 1, 2, 2, 4), "required view has columns not contained in the Table");
    REQUIRE_THROWS_WITH(subTableView(table, 1, 2, 4, 2), "required view has columns not contained in the Table");
  }

  SECTION("check for bounds in UnsafeTableView")
  {
    Table<int> table(3, 4);
    auto view = subTableView(table, 1, 2, 1, 3);

    REQUIRE_THROWS_WITH(view(2, 1), "invalid row index");
    REQUIRE_THROWS_WITH(view(1, 3), "invalid column index");
  }

  SECTION("invalid UnsafeTableView")
  {
    Table<int> table(3, 4);
    auto sub_table_view = subTableView(table, 1, 2, 1, 3);

    REQUIRE_THROWS_WITH(sub_table_view[2], "invalid index");
  }

  SECTION("check for bounds in UnsafeTableView::RowView")
  {
    Table<int> table(3, 4);
    auto sub_table_view = subTableView(table, 1, 2, 1, 3);

    auto sub_table_row_view = sub_table_view[1];

    REQUIRE_THROWS_WITH(sub_table_row_view[3], "invalid index");
  }

  SECTION("invalid UnsafeRowView")
  {
    Table<int> table(3, 4);

    REQUIRE_THROWS_WITH(table[3], "required row view is not contained in the Table");
  }

  SECTION("check for bounds in UnsafeRowView")
  {
    Table<int> table(3, 4);
    auto table_row_view = table[1];

    REQUIRE_THROWS_WITH(table_row_view[4], "invalid index");
  }

#endif   // NDEBUG
}
