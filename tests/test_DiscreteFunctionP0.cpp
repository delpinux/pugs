#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/Mesh.hpp>
#include <scheme/DiscreteFunctionP0.hpp>

#ifdef __clang__
#pragma clang optimize off
#endif   // __clang__

// clazy:excludeall=non-pod-global-static

TEST_CASE("DiscreteFunctionP0", "[scheme]")
{
  auto same_values = [](const auto& f, const auto& g) {
    size_t number_of_cells = f.cellValues().numberOfItems();
    for (CellId cell_id = 0; cell_id < number_of_cells; ++cell_id) {
      if (f[cell_id] != g[cell_id]) {
        return false;
      }
    }
    return true;
  };

  SECTION("constructors")
  {
    SECTION("1D")
    {
      constexpr size_t Dimension = 1;

      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_v = named_mesh.mesh();
          auto mesh   = mesh_v->get<Mesh<1>>();
          DiscreteFunctionP0<double> f{mesh};
          REQUIRE(f.dataType() == ASTNodeDataType::double_t);
          REQUIRE(f.descriptor().type() == DiscreteFunctionType::P0);

          REQUIRE(f.meshVariant()->id() == mesh_v->id());

          DiscreteFunctionP0 g{f};
          REQUIRE(g.dataType() == ASTNodeDataType::double_t);
          REQUIRE(g.descriptor().type() == DiscreteFunctionType::P0);

          CellValue<TinyVector<Dimension>> h_values{mesh_v->connectivity()};
          h_values.fill(ZeroType{});

          DiscreteFunctionP0 zero_function{mesh_v, [&] {
                                             CellValue<TinyVector<Dimension>> cell_value{mesh->connectivity()};
                                             cell_value.fill(ZeroType{});
                                             return cell_value;
                                           }()};

          DiscreteFunctionP0 h{mesh_v, h_values};
          REQUIRE(same_values(h, zero_function));
          REQUIRE(same_values(h, h_values));

          DiscreteFunctionP0<TinyVector<Dimension>> shallow_h{mesh};
          shallow_h = h;

          copy_to(MeshDataManager::instance().getMeshData(*mesh).xj(), h_values);

          REQUIRE(same_values(shallow_h, h_values));
          REQUIRE(same_values(h, h_values));
          REQUIRE(not same_values(h, zero_function));

          DiscreteFunctionP0 moved_h{std::move(h)};
          REQUIRE(same_values(moved_h, h_values));
        }
      }
    }

    SECTION("2D")
    {
      constexpr size_t Dimension = 2;

      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_v = named_mesh.mesh();
          auto mesh   = mesh_v->get<Mesh<2>>();

          DiscreteFunctionP0<double> f{mesh_v};
          REQUIRE(f.dataType() == ASTNodeDataType::double_t);
          REQUIRE(f.descriptor().type() == DiscreteFunctionType::P0);

          REQUIRE(f.meshVariant()->id() == mesh->id());

          DiscreteFunctionP0 g{f};
          REQUIRE(g.dataType() == ASTNodeDataType::double_t);
          REQUIRE(g.descriptor().type() == DiscreteFunctionType::P0);

          CellValue<TinyVector<Dimension>> h_values{mesh->connectivity()};
          h_values.fill(ZeroType{});

          DiscreteFunctionP0 zero_function{mesh, [&] {
                                             CellValue<TinyVector<Dimension>> cell_value{mesh->connectivity()};
                                             cell_value.fill(ZeroType{});
                                             return cell_value;
                                           }()};

          DiscreteFunctionP0 h{mesh, h_values};
          REQUIRE(same_values(h, zero_function));
          REQUIRE(same_values(h, h_values));

          DiscreteFunctionP0<TinyVector<Dimension>> shallow_h{mesh};
          shallow_h = h;

          copy_to(MeshDataManager::instance().getMeshData(*mesh).xj(), h_values);

          REQUIRE(same_values(shallow_h, h_values));
          REQUIRE(same_values(h, h_values));
          REQUIRE(not same_values(h, zero_function));

          DiscreteFunctionP0 moved_h{std::move(h)};
          REQUIRE(same_values(moved_h, h_values));
        }
      }
    }

    SECTION("3D")
    {
      constexpr size_t Dimension = 3;

      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_v = named_mesh.mesh();
          auto mesh   = mesh_v->get<Mesh<3>>();

          DiscreteFunctionP0<double> f{mesh_v};
          REQUIRE(f.dataType() == ASTNodeDataType::double_t);
          REQUIRE(f.descriptor().type() == DiscreteFunctionType::P0);

          REQUIRE(f.meshVariant()->id() == mesh_v->id());

          DiscreteFunctionP0 g{f};
          REQUIRE(g.dataType() == ASTNodeDataType::double_t);
          REQUIRE(g.descriptor().type() == DiscreteFunctionType::P0);

          CellValue<TinyVector<Dimension>> h_values{mesh->connectivity()};
          h_values.fill(ZeroType{});

          DiscreteFunctionP0 zero_function{mesh_v, [&] {
                                             CellValue<TinyVector<Dimension>> cell_value{mesh->connectivity()};
                                             cell_value.fill(ZeroType{});
                                             return cell_value;
                                           }()};

          DiscreteFunctionP0 h{mesh, h_values};
          REQUIRE(same_values(h, zero_function));
          REQUIRE(same_values(h, h_values));

          DiscreteFunctionP0<TinyVector<Dimension>> shallow_h{mesh};
          shallow_h = h;

          copy_to(MeshDataManager::instance().getMeshData(*mesh).xj(), h_values);

          REQUIRE(same_values(shallow_h, h_values));
          REQUIRE(same_values(h, h_values));
          REQUIRE(not same_values(h, zero_function));

          DiscreteFunctionP0 moved_h{std::move(h)};
          REQUIRE(same_values(moved_h, h_values));
        }
      }
    }
  }

  SECTION("fill")
  {
    auto all_values_equal = [](const auto& f, const auto& g) {
      size_t number_of_cells = f.cellValues().numberOfItems();
      for (CellId cell_id = 0; cell_id < number_of_cells; ++cell_id) {
        if (f[cell_id] != g) {
          return false;
        }
      }
      return true;
    };

    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh = named_mesh.mesh()->get<Mesh<1>>();

          DiscreteFunctionP0<double> f{mesh};
          f.fill(3);

          REQUIRE(all_values_equal(f, 3));

          DiscreteFunctionP0<TinyVector<3>> v{mesh};
          v.fill(TinyVector<3>{1, 2, 3});

          REQUIRE(all_values_equal(v, TinyVector<3>{1, 2, 3}));

          DiscreteFunctionP0<TinyMatrix<3>> A{mesh};
          A.fill(TinyMatrix<3>{1, 2, 3, 4, 5, 6, 7, 8, 9});

          REQUIRE(all_values_equal(A, TinyMatrix<3>{1, 2, 3, 4, 5, 6, 7, 8, 9}));
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_v = named_mesh.mesh();

          DiscreteFunctionP0<double> f{mesh_v};
          f.fill(3);

          REQUIRE(all_values_equal(f, 3));

          DiscreteFunctionP0<TinyVector<3>> v{mesh_v};
          v.fill(TinyVector<3>{1, 2, 3});

          REQUIRE(all_values_equal(v, TinyVector<3>{1, 2, 3}));

          DiscreteFunctionP0<TinyMatrix<3>> A{mesh_v};
          A.fill(TinyMatrix<3>{1, 2, 3, 4, 5, 6, 7, 8, 9});

          REQUIRE(all_values_equal(A, TinyMatrix<3>{1, 2, 3, 4, 5, 6, 7, 8, 9}));
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh = named_mesh.mesh()->get<Mesh<3>>();

          DiscreteFunctionP0<double> f{mesh};
          f.fill(3);

          REQUIRE(all_values_equal(f, 3));

          DiscreteFunctionP0<TinyVector<3>> v{mesh};
          v.fill(TinyVector<3>{1, 2, 3});

          REQUIRE(all_values_equal(v, TinyVector<3>{1, 2, 3}));

          DiscreteFunctionP0<TinyMatrix<3>> A{mesh};
          A.fill(TinyMatrix<3>{1, 2, 3, 4, 5, 6, 7, 8, 9});

          REQUIRE(all_values_equal(A, TinyMatrix<3>{1, 2, 3, 4, 5, 6, 7, 8, 9}));
        }
      }
    }
  }

  SECTION("copies")
  {
    auto all_values_equal = [](const auto& f, const auto& g) {
      size_t number_of_cells = f.cellValues().numberOfItems();
      for (CellId cell_id = 0; cell_id < number_of_cells; ++cell_id) {
        if (f[cell_id] != g) {
          return false;
        }
      }
      return true;
    };

    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_v = named_mesh.mesh();

          SECTION("scalar")
          {
            const size_t value      = parallel::rank() + 1;
            const size_t zero_value = 0;

            DiscreteFunctionP0<size_t> f{mesh_v};
            f.fill(value);

            REQUIRE(all_values_equal(f, value));

            DiscreteFunctionP0 g = copy(f);
            f.fill(zero_value);

            REQUIRE(all_values_equal(f, zero_value));
            REQUIRE(all_values_equal(g, value));

            copy_to(g, f);
            g.fill(zero_value);

            DiscreteFunctionP0<const size_t> h = copy(f);

            REQUIRE(all_values_equal(f, value));
            REQUIRE(all_values_equal(g, zero_value));
            REQUIRE(all_values_equal(h, value));

            copy_to(h, g);

            REQUIRE(all_values_equal(g, value));
          }

          SECTION("vector")
          {
            const TinyVector<2, size_t> value{parallel::rank() + 1, 3};
            const TinyVector<2, size_t> zero_vector{ZeroType{}};
            DiscreteFunctionP0<TinyVector<2, size_t>> f{mesh_v};
            f.fill(value);

            REQUIRE(all_values_equal(f, value));

            DiscreteFunctionP0 g = copy(f);
            f.fill(zero_vector);

            REQUIRE(all_values_equal(f, zero_vector));
            REQUIRE(all_values_equal(g, value));

            copy_to(g, f);
            g.fill(zero_vector);

            DiscreteFunctionP0<const TinyVector<2, size_t>> h = copy(f);

            REQUIRE(all_values_equal(f, value));
            REQUIRE(all_values_equal(g, zero_vector));
            REQUIRE(all_values_equal(h, value));

            copy_to(h, g);

            REQUIRE(all_values_equal(g, value));
          }

          SECTION("matrix")
          {
            const TinyMatrix<3, 3, size_t> value{1, 2, 3, 4, 5, 6, 7, 8, 9};
            const TinyMatrix<3, 3, size_t> zero_matrix{ZeroType{}};
            DiscreteFunctionP0<TinyMatrix<3, 3, size_t>> f{mesh_v};
            f.fill(value);

            REQUIRE(all_values_equal(f, value));

            DiscreteFunctionP0 g = copy(f);
            f.fill(zero_matrix);

            REQUIRE(all_values_equal(f, zero_matrix));
            REQUIRE(all_values_equal(g, value));

            copy_to(g, f);
            g.fill(zero_matrix);

            DiscreteFunctionP0<const TinyMatrix<3, 3, size_t>> h = copy(f);

            REQUIRE(all_values_equal(f, value));
            REQUIRE(all_values_equal(g, zero_matrix));
            REQUIRE(all_values_equal(h, value));

            copy_to(h, g);

            REQUIRE(all_values_equal(g, value));
          }
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh = named_mesh.mesh()->get<Mesh<2>>();

          SECTION("scalar")
          {
            const size_t value      = parallel::rank() + 1;
            const size_t zero_value = 0;

            DiscreteFunctionP0<size_t> f{mesh};
            f.fill(value);

            REQUIRE(all_values_equal(f, value));

            DiscreteFunctionP0 g = copy(f);
            f.fill(zero_value);

            REQUIRE(all_values_equal(f, zero_value));
            REQUIRE(all_values_equal(g, value));

            copy_to(g, f);
            g.fill(zero_value);

            DiscreteFunctionP0<const size_t> h = copy(f);

            REQUIRE(all_values_equal(f, value));
            REQUIRE(all_values_equal(g, zero_value));
            REQUIRE(all_values_equal(h, value));

            copy_to(h, g);

            REQUIRE(all_values_equal(g, value));
          }

          SECTION("vector")
          {
            const TinyVector<2, size_t> value{parallel::rank() + 1, 3};
            const TinyVector<2, size_t> zero_vector{ZeroType{}};
            DiscreteFunctionP0<TinyVector<2, size_t>> f{mesh};
            f.fill(value);

            REQUIRE(all_values_equal(f, value));

            DiscreteFunctionP0 g = copy(f);
            f.fill(zero_vector);

            REQUIRE(all_values_equal(f, zero_vector));
            REQUIRE(all_values_equal(g, value));

            copy_to(g, f);
            g.fill(zero_vector);

            DiscreteFunctionP0<const TinyVector<2, size_t>> h = copy(f);

            REQUIRE(all_values_equal(f, value));
            REQUIRE(all_values_equal(g, zero_vector));
            REQUIRE(all_values_equal(h, value));

            copy_to(h, g);

            REQUIRE(all_values_equal(g, value));
          }

          SECTION("matrix")
          {
            const TinyMatrix<3, 3, size_t> value{1, 2, 3, 4, 5, 6, 7, 8, 9};
            const TinyMatrix<3, 3, size_t> zero_vector{ZeroType{}};
            DiscreteFunctionP0<TinyMatrix<3, 3, size_t>> f{mesh->meshVariant()};
            f.fill(value);

            REQUIRE(all_values_equal(f, value));

            DiscreteFunctionP0 g = copy(f);
            f.fill(zero_vector);

            REQUIRE(all_values_equal(f, zero_vector));
            REQUIRE(all_values_equal(g, value));

            copy_to(g, f);
            g.fill(zero_vector);

            DiscreteFunctionP0<const TinyMatrix<3, 3, size_t>> h = copy(f);

            REQUIRE(all_values_equal(f, value));
            REQUIRE(all_values_equal(g, zero_vector));
            REQUIRE(all_values_equal(h, value));

            copy_to(h, g);

            REQUIRE(all_values_equal(g, value));
          }
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_v = named_mesh.mesh();

          SECTION("scalar")
          {
            const size_t value      = parallel::rank() + 1;
            const size_t zero_value = 0;

            DiscreteFunctionP0<size_t> f{mesh_v};
            f.fill(value);

            REQUIRE(all_values_equal(f, value));

            DiscreteFunctionP0 g = copy(f);
            f.fill(zero_value);

            REQUIRE(all_values_equal(f, zero_value));
            REQUIRE(all_values_equal(g, value));

            copy_to(g, f);
            g.fill(zero_value);

            DiscreteFunctionP0<const size_t> h = copy(f);

            REQUIRE(all_values_equal(f, value));
            REQUIRE(all_values_equal(g, zero_value));
            REQUIRE(all_values_equal(h, value));

            copy_to(h, g);

            REQUIRE(all_values_equal(g, value));
          }

          SECTION("vector")
          {
            const TinyVector<2, size_t> value{parallel::rank() + 1, 3};
            const TinyVector<2, size_t> zero_vector{ZeroType{}};
            DiscreteFunctionP0<TinyVector<2, size_t>> f{mesh_v};
            f.fill(value);

            REQUIRE(all_values_equal(f, value));

            DiscreteFunctionP0 g = copy(f);
            f.fill(zero_vector);

            REQUIRE(all_values_equal(f, zero_vector));
            REQUIRE(all_values_equal(g, value));

            copy_to(g, f);
            g.fill(zero_vector);

            DiscreteFunctionP0<const TinyVector<2, size_t>> h = copy(f);

            REQUIRE(all_values_equal(f, value));
            REQUIRE(all_values_equal(g, zero_vector));
            REQUIRE(all_values_equal(h, value));

            copy_to(h, g);

            REQUIRE(all_values_equal(g, value));
          }

          SECTION("matrix")
          {
            const TinyMatrix<3, 3, size_t> value{1, 2, 3, 4, 5, 6, 7, 8, 9};
            const TinyMatrix<3, 3, size_t> zero_matrix{ZeroType{}};
            DiscreteFunctionP0<TinyMatrix<3, 3, size_t>> f{mesh_v};
            f.fill(value);

            REQUIRE(all_values_equal(f, value));

            DiscreteFunctionP0 g = copy(f);
            f.fill(zero_matrix);

            REQUIRE(all_values_equal(f, zero_matrix));
            REQUIRE(all_values_equal(g, value));

            copy_to(g, f);
            g.fill(zero_matrix);

            DiscreteFunctionP0<const TinyMatrix<3, 3, size_t>> h = copy(f);

            REQUIRE(all_values_equal(f, value));
            REQUIRE(all_values_equal(g, zero_matrix));
            REQUIRE(all_values_equal(h, value));

            copy_to(h, g);

            REQUIRE(all_values_equal(g, value));
          }
        }
      }
    }
  }

  SECTION("unary operators")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh = named_mesh.mesh()->get<Mesh<1>>();

          auto xj = MeshDataManager::instance().getMeshData(*mesh).xj();

          SECTION("unary minus")
          {
            SECTION("scalar functions")
            {
              DiscreteFunctionP0<double> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  f[cell_id]     = 2 * x + 1;
                });

              DiscreteFunctionP0<const double> const_f = f;

              Array<double> minus_values{mesh->numberOfCells()};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { minus_values[cell_id] = -f[cell_id]; });

              REQUIRE(same_values(-f, minus_values));
              REQUIRE(same_values(-const_f, minus_values));
            }

            SECTION("vector functions")
            {
              constexpr std::uint64_t VectorDimension = 2;

              DiscreteFunctionP0<TinyVector<VectorDimension>> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyVector<VectorDimension> X{x, 2 - x};
                  f[cell_id] = 2 * X + TinyVector<2>{1, 2};
                });

              DiscreteFunctionP0<const TinyVector<VectorDimension>> const_f = f;

              Array<TinyVector<VectorDimension>> minus_values{mesh->numberOfCells()};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { minus_values[cell_id] = -f[cell_id]; });

              REQUIRE(same_values(-f, minus_values));
              REQUIRE(same_values(-const_f, minus_values));
            }

            SECTION("matrix functions")
            {
              constexpr std::uint64_t MatrixDimension = 2;

              DiscreteFunctionP0<TinyMatrix<MatrixDimension>> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyMatrix<MatrixDimension> A{x, 2 - x, 2 * x, x * x - 3};
                  f[cell_id] = 2 * A + TinyMatrix<2>{1, 2, 3, 4};
                });

              DiscreteFunctionP0<const TinyMatrix<MatrixDimension>> const_f = f;

              Array<TinyMatrix<MatrixDimension>> minus_values{mesh->numberOfCells()};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { minus_values[cell_id] = -f[cell_id]; });

              REQUIRE(same_values(-f, minus_values));
              REQUIRE(same_values(-const_f, minus_values));
            }
          }
        }
      }
    }
  }

  SECTION("binary operators")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh = named_mesh.mesh()->get<Mesh<1>>();

          auto xj = MeshDataManager::instance().getMeshData(*mesh).xj();

          SECTION("inner operators")
          {
            SECTION("scalar functions")
            {
              DiscreteFunctionP0<double> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  f[cell_id]     = 2 * x + 1;
                });

              DiscreteFunctionP0<double> g{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  g[cell_id]     = std::abs((x + 1) * (x - 2)) + 1;
                });

              DiscreteFunctionP0<const double> const_f = f;
              DiscreteFunctionP0<const double> const_g{g};

              SECTION("sum")
              {
                Array<double> sum_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + g[cell_id]; });

                REQUIRE(same_values(f + g, sum_values));
                REQUIRE(same_values(const_f + g, sum_values));
                REQUIRE(same_values(f + const_g, sum_values));
                REQUIRE(same_values(const_f + const_g, sum_values));
              }

              SECTION("difference")
              {
                Array<double> difference_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - g[cell_id]; });

                REQUIRE(same_values(f - g, difference_values));
                REQUIRE(same_values(const_f - g, difference_values));
                REQUIRE(same_values(f - const_g, difference_values));
                REQUIRE(same_values(const_f - const_g, difference_values));
              }

              SECTION("product")
              {
                Array<double> product_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * g[cell_id]; });

                REQUIRE(same_values(f * g, product_values));
                REQUIRE(same_values(const_f * g, product_values));
                REQUIRE(same_values(f * const_g, product_values));
                REQUIRE(same_values(const_f * const_g, product_values));
              }

              SECTION("ratio")
              {
                Array<double> ratio_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { ratio_values[cell_id] = f[cell_id] / g[cell_id]; });

                REQUIRE(same_values(f / g, ratio_values));
                REQUIRE(same_values(const_f / g, ratio_values));
                REQUIRE(same_values(f / const_g, ratio_values));
                REQUIRE(same_values(const_f / const_g, ratio_values));
              }
            }

            SECTION("vector functions")
            {
              constexpr std::uint64_t VectorDimension = 2;

              DiscreteFunctionP0<TinyVector<VectorDimension>> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyVector<VectorDimension> X{x, 2 - x};
                  f[cell_id] = 2 * X + TinyVector<2>{1, 2};
                });

              DiscreteFunctionP0<TinyVector<VectorDimension>> g{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyVector<VectorDimension> X{3 * x + 1, 2 + x};
                  g[cell_id] = X;
                });

              DiscreteFunctionP0<const TinyVector<VectorDimension>> const_f = f;
              DiscreteFunctionP0<const TinyVector<VectorDimension>> const_g{g};

              SECTION("sum")
              {
                Array<TinyVector<VectorDimension>> sum_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + g[cell_id]; });

                REQUIRE(same_values(f + g, sum_values));
                REQUIRE(same_values(const_f + g, sum_values));
                REQUIRE(same_values(f + const_g, sum_values));
                REQUIRE(same_values(const_f + const_g, sum_values));
              }

              SECTION("difference")
              {
                Array<TinyVector<VectorDimension>> difference_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - g[cell_id]; });

                REQUIRE(same_values(f - g, difference_values));
                REQUIRE(same_values(const_f - g, difference_values));
                REQUIRE(same_values(f - const_g, difference_values));
                REQUIRE(same_values(const_f - const_g, difference_values));
              }
            }

            SECTION("matrix functions")
            {
              constexpr std::uint64_t MatrixDimension = 2;

              DiscreteFunctionP0<TinyMatrix<MatrixDimension>> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyMatrix<MatrixDimension> A{x, 2 - x, 2 * x, x * x - 3};
                  f[cell_id] = 2 * A + TinyMatrix<2>{1, 2, 3, 4};
                });

              DiscreteFunctionP0<TinyMatrix<MatrixDimension>> g{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyMatrix<MatrixDimension> A{3 * x + 1, 2 + x, 1 - 2 * x, 2 * x * x};
                  g[cell_id] = A;
                });

              DiscreteFunctionP0<const TinyMatrix<MatrixDimension>> const_f = f;
              DiscreteFunctionP0<const TinyMatrix<MatrixDimension>> const_g{g};

              SECTION("sum")
              {
                Array<TinyMatrix<MatrixDimension>> sum_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + g[cell_id]; });

                REQUIRE(same_values(f + g, sum_values));
                REQUIRE(same_values(const_f + g, sum_values));
                REQUIRE(same_values(f + const_g, sum_values));
                REQUIRE(same_values(const_f + const_g, sum_values));
              }

              SECTION("difference")
              {
                Array<TinyMatrix<MatrixDimension>> difference_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - g[cell_id]; });

                REQUIRE(same_values(f - g, difference_values));
                REQUIRE(same_values(const_f - g, difference_values));
                REQUIRE(same_values(f - const_g, difference_values));
                REQUIRE(same_values(const_f - const_g, difference_values));
              }

              SECTION("product")
              {
                Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * g[cell_id]; });

                REQUIRE(same_values(f * g, product_values));
                REQUIRE(same_values(const_f * g, product_values));
                REQUIRE(same_values(f * const_g, product_values));
                REQUIRE(same_values(const_f * const_g, product_values));
              }
            }
          }

          SECTION("external operators")
          {
            SECTION("scalar functions")
            {
              DiscreteFunctionP0<double> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  f[cell_id]     = std::abs(2 * x) + 1;
                });

              const double a = 3;

              DiscreteFunctionP0<const double> const_f = f;

              SECTION("sum")
              {
                {
                  Array<double> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = a + f[cell_id]; });

                  REQUIRE(same_values(a + f, sum_values));
                  REQUIRE(same_values(a + const_f, sum_values));
                }
                {
                  Array<double> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + a; });

                  REQUIRE(same_values(f + a, sum_values));
                  REQUIRE(same_values(const_f + a, sum_values));
                }
              }

              SECTION("difference")
              {
                {
                  Array<double> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = a - f[cell_id]; });
                  REQUIRE(same_values(a - f, difference_values));
                  REQUIRE(same_values(a - const_f, difference_values));
                }

                {
                  Array<double> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - a; });
                  REQUIRE(same_values(f - a, difference_values));
                  REQUIRE(same_values(const_f - a, difference_values));
                }
              }

              SECTION("product")
              {
                {
                  Array<double> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }
                {
                  Array<double> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * a; });

                  REQUIRE(same_values(f * a, product_values));
                  REQUIRE(same_values(const_f * a, product_values));
                }

                {
                  Array<TinyVector<3>> product_values{mesh->numberOfCells()};
                  const TinyVector<3> v{1, 2, 3};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * v; });

                  REQUIRE(same_values(f * v, product_values));
                  REQUIRE(same_values(const_f * v, product_values));
                }

                {
                  Array<TinyVector<3>> product_values{mesh->numberOfCells()};
                  DiscreteFunctionP0<TinyVector<3>> v{mesh};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      v[cell_id]     = TinyVector<3>{x, 2 * x, 1 - x};
                    });

                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * v[cell_id]; });

                  REQUIRE(same_values(f * v, product_values));
                  REQUIRE(same_values(const_f * v, product_values));
                }

                {
                  Array<TinyMatrix<2>> product_values{mesh->numberOfCells()};
                  const TinyMatrix<2> A{1, 2, 3, 4};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * A; });

                  REQUIRE(same_values(f * A, product_values));
                  REQUIRE(same_values(const_f * A, product_values));
                }

                {
                  Array<TinyMatrix<2>> product_values{mesh->numberOfCells()};
                  DiscreteFunctionP0<TinyMatrix<2>> M{mesh};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      M[cell_id]     = TinyMatrix<2>{x, 2 * x, 1 - x, 2 - x * x};
                    });

                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * M[cell_id]; });

                  REQUIRE(same_values(f * M, product_values));
                  REQUIRE(same_values(const_f * M, product_values));
                }
              }

              SECTION("ratio")
              {
                {
                  Array<double> ratio_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { ratio_values[cell_id] = a / f[cell_id]; });

                  REQUIRE(same_values(a / f, ratio_values));
                  REQUIRE(same_values(a / const_f, ratio_values));
                }
                {
                  Array<double> ratio_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { ratio_values[cell_id] = f[cell_id] / a; });

                  REQUIRE(same_values(f / a, ratio_values));
                  REQUIRE(same_values(const_f / a, ratio_values));
                }
              }
            }

            SECTION("vector functions")
            {
              constexpr std::uint64_t VectorDimension = 2;

              DiscreteFunctionP0<TinyVector<VectorDimension>> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyVector<VectorDimension> X{x, 2 - x};
                  f[cell_id] = 2 * X + TinyVector<2>{1, 2};
                });

              DiscreteFunctionP0<const TinyVector<VectorDimension>> const_f = f;

              SECTION("sum")
              {
                const TinyVector<VectorDimension> v{1, 2};
                {
                  Array<TinyVector<VectorDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = v + f[cell_id]; });

                  REQUIRE(same_values(v + f, sum_values));
                  REQUIRE(same_values(v + const_f, sum_values));
                }
                {
                  Array<TinyVector<VectorDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + v; });

                  REQUIRE(same_values(f + v, sum_values));
                  REQUIRE(same_values(const_f + v, sum_values));
                }
              }

              SECTION("difference")
              {
                const TinyVector<VectorDimension> v{1, 2};
                {
                  Array<TinyVector<VectorDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = v - f[cell_id]; });

                  REQUIRE(same_values(v - f, difference_values));
                  REQUIRE(same_values(v - const_f, difference_values));
                }
                {
                  Array<TinyVector<VectorDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - v; });

                  REQUIRE(same_values(f - v, difference_values));
                  REQUIRE(same_values(const_f - v, difference_values));
                }
              }

              SECTION("product")
              {
                {
                  const double a = 2.3;
                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  DiscreteFunctionP0<double> a{mesh};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      a[cell_id]     = 2 * x * x - 1;
                    });

                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a[cell_id] * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  const TinyMatrix<VectorDimension> A{1, 2, 3, 4};
                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = A * f[cell_id]; });

                  REQUIRE(same_values(A * f, product_values));
                  REQUIRE(same_values(A * const_f, product_values));
                }

                {
                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  DiscreteFunctionP0<TinyMatrix<VectorDimension>> M{mesh};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      M[cell_id]     = TinyMatrix<2>{x, 2 * x, 1 - x, 2 - x * x};
                    });

                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = M[cell_id] * f[cell_id]; });

                  REQUIRE(same_values(M * f, product_values));
                  REQUIRE(same_values(M * const_f, product_values));
                }
              }
            }

            SECTION("matrix functions")
            {
              constexpr std::uint64_t MatrixDimension = 2;

              DiscreteFunctionP0<TinyMatrix<MatrixDimension>> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyMatrix<MatrixDimension> X{x, 2 - x, x * x, x * 3};
                  f[cell_id] = 2 * X + TinyMatrix<2>{1, 2, 3, 4};
                });

              DiscreteFunctionP0<const TinyMatrix<MatrixDimension>> const_f = f;

              SECTION("sum")
              {
                const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                {
                  Array<TinyMatrix<MatrixDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = A + f[cell_id]; });

                  REQUIRE(same_values(A + f, sum_values));
                  REQUIRE(same_values(A + const_f, sum_values));
                }
                {
                  Array<TinyMatrix<MatrixDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + A; });

                  REQUIRE(same_values(f + A, sum_values));
                  REQUIRE(same_values(const_f + A, sum_values));
                }
              }

              SECTION("difference")
              {
                const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                {
                  Array<TinyMatrix<MatrixDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = A - f[cell_id]; });

                  REQUIRE(same_values(A - f, difference_values));
                  REQUIRE(same_values(A - const_f, difference_values));
                }
                {
                  Array<TinyMatrix<MatrixDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - A; });

                  REQUIRE(same_values(f - A, difference_values));
                  REQUIRE(same_values(const_f - A, difference_values));
                }
              }

              SECTION("product")
              {
                {
                  const double a = 2.3;
                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  DiscreteFunctionP0<double> a{mesh};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      a[cell_id]     = 2 * x * x - 1;
                    });

                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a[cell_id] * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = A * f[cell_id]; });

                  REQUIRE(same_values(A * f, product_values));
                  REQUIRE(same_values(A * const_f, product_values));
                }

                {
                  const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * A; });

                  REQUIRE(same_values(f * A, product_values));
                  REQUIRE(same_values(const_f * A, product_values));
                }
              }
            }
          }
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_v = named_mesh.mesh();
          auto mesh   = mesh_v->get<Mesh<2>>();
          auto xj     = MeshDataManager::instance().getMeshData(*mesh).xj();

          SECTION("inner operators")
          {
            SECTION("scalar functions")
            {
              DiscreteFunctionP0<double> f{mesh_v};
              parallel_for(
                mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const double y = xj[cell_id][1];
                  f[cell_id]     = 2 * x + y + 1;
                });

              DiscreteFunctionP0<double> g{mesh_v};
              parallel_for(
                mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const double y = xj[cell_id][1];
                  g[cell_id]     = std::abs((x + 1) * (x - 2) + y * (1 + y)) + 1;
                });

              DiscreteFunctionP0<const double> const_f = f;
              DiscreteFunctionP0<const double> const_g{g};

              SECTION("sum")
              {
                Array<double> sum_values{mesh_v->numberOfCells()};
                parallel_for(
                  mesh_v->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + g[cell_id]; });

                REQUIRE(same_values(f + g, sum_values));
                REQUIRE(same_values(const_f + g, sum_values));
                REQUIRE(same_values(f + const_g, sum_values));
                REQUIRE(same_values(const_f + const_g, sum_values));
              }

              SECTION("difference")
              {
                Array<double> difference_values{mesh_v->numberOfCells()};
                parallel_for(
                  mesh_v->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - g[cell_id]; });

                REQUIRE(same_values(f - g, difference_values));
                REQUIRE(same_values(const_f - g, difference_values));
                REQUIRE(same_values(f - const_g, difference_values));
                REQUIRE(same_values(const_f - const_g, difference_values));
              }

              SECTION("product")
              {
                Array<double> product_values{mesh_v->numberOfCells()};
                parallel_for(
                  mesh_v->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * g[cell_id]; });

                REQUIRE(same_values(f * g, product_values));
                REQUIRE(same_values(const_f * g, product_values));
                REQUIRE(same_values(f * const_g, product_values));
                REQUIRE(same_values(const_f * const_g, product_values));
              }

              SECTION("ratio")
              {
                Array<double> ratio_values{mesh_v->numberOfCells()};
                parallel_for(
                  mesh_v->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { ratio_values[cell_id] = f[cell_id] / g[cell_id]; });

                REQUIRE(same_values(f / g, ratio_values));
                REQUIRE(same_values(const_f / g, ratio_values));
                REQUIRE(same_values(f / const_g, ratio_values));
                REQUIRE(same_values(const_f / const_g, ratio_values));
              }
            }

            SECTION("vector functions")
            {
              constexpr std::uint64_t VectorDimension = 2;

              DiscreteFunctionP0<TinyVector<VectorDimension>> f{mesh_v};
              parallel_for(
                mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyVector<VectorDimension> X{x, 2 - x};
                  f[cell_id] = 2 * X + TinyVector<2>{1, 2};
                });

              DiscreteFunctionP0<TinyVector<VectorDimension>> g{mesh_v};
              parallel_for(
                mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyVector<VectorDimension> X{3 * x + 1, 2 + x};
                  g[cell_id] = X;
                });

              DiscreteFunctionP0<const TinyVector<VectorDimension>> const_f = f;
              DiscreteFunctionP0<const TinyVector<VectorDimension>> const_g{g};

              SECTION("sum")
              {
                Array<TinyVector<VectorDimension>> sum_values{mesh_v->numberOfCells()};
                parallel_for(
                  mesh_v->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + g[cell_id]; });

                REQUIRE(same_values(f + g, sum_values));
                REQUIRE(same_values(const_f + g, sum_values));
                REQUIRE(same_values(f + const_g, sum_values));
                REQUIRE(same_values(const_f + const_g, sum_values));
              }

              SECTION("difference")
              {
                Array<TinyVector<VectorDimension>> difference_values{mesh_v->numberOfCells()};
                parallel_for(
                  mesh_v->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - g[cell_id]; });

                REQUIRE(same_values(f - g, difference_values));
                REQUIRE(same_values(const_f - g, difference_values));
                REQUIRE(same_values(f - const_g, difference_values));
                REQUIRE(same_values(const_f - const_g, difference_values));
              }
            }

            SECTION("matrix functions")
            {
              constexpr std::uint64_t MatrixDimension = 2;

              DiscreteFunctionP0<TinyMatrix<MatrixDimension>> f{mesh_v};
              parallel_for(
                mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyMatrix<MatrixDimension> A{x, 2 - x, 2 * x, x * x - 3};
                  f[cell_id] = 2 * A + TinyMatrix<2>{1, 2, 3, 4};
                });

              DiscreteFunctionP0<TinyMatrix<MatrixDimension>> g{mesh_v};
              parallel_for(
                mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyMatrix<MatrixDimension> A{3 * x + 1, 2 + x, 1 - 2 * x, 2 * x * x};
                  g[cell_id] = A;
                });

              DiscreteFunctionP0<const TinyMatrix<MatrixDimension>> const_f = f;
              DiscreteFunctionP0<const TinyMatrix<MatrixDimension>> const_g{g};

              SECTION("sum")
              {
                Array<TinyMatrix<MatrixDimension>> sum_values{mesh_v->numberOfCells()};
                parallel_for(
                  mesh_v->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + g[cell_id]; });

                REQUIRE(same_values(f + g, sum_values));
                REQUIRE(same_values(const_f + g, sum_values));
                REQUIRE(same_values(f + const_g, sum_values));
                REQUIRE(same_values(const_f + const_g, sum_values));
              }

              SECTION("difference")
              {
                Array<TinyMatrix<MatrixDimension>> difference_values{mesh_v->numberOfCells()};
                parallel_for(
                  mesh_v->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - g[cell_id]; });

                REQUIRE(same_values(f - g, difference_values));
                REQUIRE(same_values(const_f - g, difference_values));
                REQUIRE(same_values(f - const_g, difference_values));
                REQUIRE(same_values(const_f - const_g, difference_values));
              }

              SECTION("product")
              {
                Array<TinyMatrix<MatrixDimension>> product_values{mesh_v->numberOfCells()};
                parallel_for(
                  mesh_v->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * g[cell_id]; });

                REQUIRE(same_values(f * g, product_values));
                REQUIRE(same_values(const_f * g, product_values));
                REQUIRE(same_values(f * const_g, product_values));
                REQUIRE(same_values(const_f * const_g, product_values));
              }
            }
          }

          SECTION("external operators")
          {
            SECTION("scalar functions")
            {
              DiscreteFunctionP0<double> f{mesh_v};
              parallel_for(
                mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const double y = xj[cell_id][1];
                  f[cell_id]     = std::abs(2 * x + y) + 1;
                });

              const double a = 3;

              DiscreteFunctionP0<const double> const_f = f;

              SECTION("sum")
              {
                {
                  Array<double> sum_values{mesh_v->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = a + f[cell_id]; });

                  REQUIRE(same_values(a + f, sum_values));
                  REQUIRE(same_values(a + const_f, sum_values));
                }
                {
                  Array<double> sum_values{mesh_v->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + a; });

                  REQUIRE(same_values(f + a, sum_values));
                  REQUIRE(same_values(const_f + a, sum_values));
                }
              }

              SECTION("difference")
              {
                {
                  Array<double> difference_values{mesh_v->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = a - f[cell_id]; });
                  REQUIRE(same_values(a - f, difference_values));
                  REQUIRE(same_values(a - const_f, difference_values));
                }

                {
                  Array<double> difference_values{mesh_v->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - a; });
                  REQUIRE(same_values(f - a, difference_values));
                  REQUIRE(same_values(const_f - a, difference_values));
                }
              }

              SECTION("product")
              {
                {
                  Array<double> product_values{mesh_v->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }
                {
                  Array<double> product_values{mesh_v->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * a; });

                  REQUIRE(same_values(f * a, product_values));
                  REQUIRE(same_values(const_f * a, product_values));
                }

                {
                  Array<TinyVector<3>> product_values{mesh_v->numberOfCells()};
                  const TinyVector<3> v{1, 2, 3};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * v; });

                  REQUIRE(same_values(f * v, product_values));
                  REQUIRE(same_values(const_f * v, product_values));
                }

                {
                  Array<TinyVector<3>> product_values{mesh_v->numberOfCells()};
                  DiscreteFunctionP0<TinyVector<3>> v{mesh};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      v[cell_id]     = TinyVector<3>{x, 2 * x, 1 - x};
                    });

                  parallel_for(
                    mesh_v->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * v[cell_id]; });

                  REQUIRE(same_values(f * v, product_values));
                  REQUIRE(same_values(const_f * v, product_values));
                }

                {
                  Array<TinyMatrix<2>> product_values{mesh->numberOfCells()};
                  const TinyMatrix<2> A{1, 2, 3, 4};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * A; });

                  REQUIRE(same_values(f * A, product_values));
                  REQUIRE(same_values(const_f * A, product_values));
                }

                {
                  Array<TinyMatrix<2>> product_values{mesh->numberOfCells()};
                  DiscreteFunctionP0<TinyMatrix<2>> M{mesh};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      M[cell_id]     = TinyMatrix<2>{x, 2 * x, 1 - x, 2 - x * x};
                    });

                  parallel_for(
                    mesh_v->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * M[cell_id]; });

                  REQUIRE(same_values(f * M, product_values));
                  REQUIRE(same_values(const_f * M, product_values));
                }
              }

              SECTION("ratio")
              {
                {
                  Array<double> ratio_values{mesh_v->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { ratio_values[cell_id] = a / f[cell_id]; });

                  REQUIRE(same_values(a / f, ratio_values));
                  REQUIRE(same_values(a / const_f, ratio_values));
                }
                {
                  Array<double> ratio_values{mesh_v->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { ratio_values[cell_id] = f[cell_id] / a; });

                  REQUIRE(same_values(f / a, ratio_values));
                  REQUIRE(same_values(const_f / a, ratio_values));
                }
              }
            }

            SECTION("vector functions")
            {
              constexpr std::uint64_t VectorDimension = 2;

              DiscreteFunctionP0<TinyVector<VectorDimension>> f{mesh};
              parallel_for(
                mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const double y = xj[cell_id][1];
                  const TinyVector<VectorDimension> X{x + y, 2 - x * y};
                  f[cell_id] = 2 * X + TinyVector<2>{1, 2};
                });

              DiscreteFunctionP0<const TinyVector<VectorDimension>> const_f = f;

              SECTION("sum")
              {
                const TinyVector<VectorDimension> v{1, 2};
                {
                  Array<TinyVector<VectorDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = v + f[cell_id]; });

                  REQUIRE(same_values(v + f, sum_values));
                  REQUIRE(same_values(v + const_f, sum_values));
                }
                {
                  Array<TinyVector<VectorDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + v; });

                  REQUIRE(same_values(f + v, sum_values));
                  REQUIRE(same_values(const_f + v, sum_values));
                }
              }

              SECTION("difference")
              {
                const TinyVector<VectorDimension> v{1, 2};
                {
                  Array<TinyVector<VectorDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = v - f[cell_id]; });

                  REQUIRE(same_values(v - f, difference_values));
                  REQUIRE(same_values(v - const_f, difference_values));
                }
                {
                  Array<TinyVector<VectorDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - v; });

                  REQUIRE(same_values(f - v, difference_values));
                  REQUIRE(same_values(const_f - v, difference_values));
                }
              }

              SECTION("product")
              {
                {
                  const double a = 2.3;
                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  DiscreteFunctionP0<double> a{mesh};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      a[cell_id]     = 2 * x * x - 1;
                    });

                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a[cell_id] * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  const TinyMatrix<VectorDimension> A{1, 2, 3, 4};
                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = A * f[cell_id]; });

                  REQUIRE(same_values(A * f, product_values));
                  REQUIRE(same_values(A * const_f, product_values));
                }

                {
                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  DiscreteFunctionP0<TinyMatrix<VectorDimension>> M{mesh};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      M[cell_id]     = TinyMatrix<2>{x, 2 * x, 1 - x, 2 - x * x};
                    });

                  parallel_for(
                    mesh_v->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = M[cell_id] * f[cell_id]; });

                  REQUIRE(same_values(M * f, product_values));
                  REQUIRE(same_values(M * const_f, product_values));
                }
              }
            }

            SECTION("matrix functions")
            {
              constexpr std::uint64_t MatrixDimension = 2;

              DiscreteFunctionP0<TinyMatrix<MatrixDimension>> f{mesh};
              parallel_for(
                mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const double y = xj[cell_id][1];
                  const TinyMatrix<MatrixDimension> X{x, 2 - y, x * y, y * 3};
                  f[cell_id] = 2 * X + TinyMatrix<2>{1, 2, 3, 4};
                });

              DiscreteFunctionP0<const TinyMatrix<MatrixDimension>> const_f = f;

              SECTION("sum")
              {
                const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                {
                  Array<TinyMatrix<MatrixDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = A + f[cell_id]; });

                  REQUIRE(same_values(A + f, sum_values));
                  REQUIRE(same_values(A + const_f, sum_values));
                }
                {
                  Array<TinyMatrix<MatrixDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + A; });

                  REQUIRE(same_values(f + A, sum_values));
                  REQUIRE(same_values(const_f + A, sum_values));
                }
              }

              SECTION("difference")
              {
                const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                {
                  Array<TinyMatrix<MatrixDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = A - f[cell_id]; });

                  REQUIRE(same_values(A - f, difference_values));
                  REQUIRE(same_values(A - const_f, difference_values));
                }
                {
                  Array<TinyMatrix<MatrixDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - A; });

                  REQUIRE(same_values(f - A, difference_values));
                  REQUIRE(same_values(const_f - A, difference_values));
                }
              }

              SECTION("product")
              {
                {
                  const double a = 2.3;
                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  DiscreteFunctionP0<double> a{mesh};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      a[cell_id]     = 2 * x * x - 1;
                    });

                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a[cell_id] * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = A * f[cell_id]; });

                  REQUIRE(same_values(A * f, product_values));
                  REQUIRE(same_values(A * const_f, product_values));
                }

                {
                  const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * A; });

                  REQUIRE(same_values(f * A, product_values));
                  REQUIRE(same_values(const_f * A, product_values));
                }
              }
            }
          }
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh = named_mesh.mesh()->get<Mesh<3>>();

          auto xj = MeshDataManager::instance().getMeshData(*mesh).xj();

          SECTION("inner operators")
          {
            SECTION("scalar functions")
            {
              DiscreteFunctionP0<double> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const double y = xj[cell_id][1];
                  const double z = xj[cell_id][2];
                  f[cell_id]     = 2 * x + y - z;
                });

              DiscreteFunctionP0<double> g{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const double y = xj[cell_id][1];
                  const double z = xj[cell_id][2];
                  g[cell_id]     = std::abs((x + 1) * (x - 2) + y * (1 + y) + 2 * z) + 1;
                });

              DiscreteFunctionP0<const double> const_f = f;
              DiscreteFunctionP0<const double> const_g{g};

              SECTION("sum")
              {
                Array<double> sum_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + g[cell_id]; });

                REQUIRE(same_values(f + g, sum_values));
                REQUIRE(same_values(const_f + g, sum_values));
                REQUIRE(same_values(f + const_g, sum_values));
                REQUIRE(same_values(const_f + const_g, sum_values));
              }

              SECTION("difference")
              {
                Array<double> difference_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - g[cell_id]; });

                REQUIRE(same_values(f - g, difference_values));
                REQUIRE(same_values(const_f - g, difference_values));
                REQUIRE(same_values(f - const_g, difference_values));
                REQUIRE(same_values(const_f - const_g, difference_values));
              }

              SECTION("product")
              {
                Array<double> product_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * g[cell_id]; });

                REQUIRE(same_values(f * g, product_values));
                REQUIRE(same_values(const_f * g, product_values));
                REQUIRE(same_values(f * const_g, product_values));
                REQUIRE(same_values(const_f * const_g, product_values));
              }

              SECTION("ratio")
              {
                Array<double> ratio_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { ratio_values[cell_id] = f[cell_id] / g[cell_id]; });

                REQUIRE(same_values(f / g, ratio_values));
                REQUIRE(same_values(const_f / g, ratio_values));
                REQUIRE(same_values(f / const_g, ratio_values));
                REQUIRE(same_values(const_f / const_g, ratio_values));
              }
            }

            SECTION("vector functions")
            {
              constexpr std::uint64_t VectorDimension = 2;

              DiscreteFunctionP0<TinyVector<VectorDimension>> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyVector<VectorDimension> X{x, 2 - x};
                  f[cell_id] = 2 * X + TinyVector<2>{1, 2};
                });

              DiscreteFunctionP0<TinyVector<VectorDimension>> g{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyVector<VectorDimension> X{3 * x + 1, 2 + x};
                  g[cell_id] = X;
                });

              DiscreteFunctionP0<const TinyVector<VectorDimension>> const_f = f;
              DiscreteFunctionP0<const TinyVector<VectorDimension>> const_g{g};

              SECTION("sum")
              {
                Array<TinyVector<VectorDimension>> sum_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + g[cell_id]; });

                REQUIRE(same_values(f + g, sum_values));
                REQUIRE(same_values(const_f + g, sum_values));
                REQUIRE(same_values(f + const_g, sum_values));
                REQUIRE(same_values(const_f + const_g, sum_values));
              }

              SECTION("difference")
              {
                Array<TinyVector<VectorDimension>> difference_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - g[cell_id]; });

                REQUIRE(same_values(f - g, difference_values));
                REQUIRE(same_values(const_f - g, difference_values));
                REQUIRE(same_values(f - const_g, difference_values));
                REQUIRE(same_values(const_f - const_g, difference_values));
              }
            }

            SECTION("matrix functions")
            {
              constexpr std::uint64_t MatrixDimension = 2;

              DiscreteFunctionP0<TinyMatrix<MatrixDimension>> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyMatrix<MatrixDimension> A{x, 2 - x, 2 * x, x * x - 3};
                  f[cell_id] = 2 * A + TinyMatrix<2>{1, 2, 3, 4};
                });

              DiscreteFunctionP0<TinyMatrix<MatrixDimension>> g{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const TinyMatrix<MatrixDimension> A{3 * x + 1, 2 + x, 1 - 2 * x, 2 * x * x};
                  g[cell_id] = A;
                });

              DiscreteFunctionP0<const TinyMatrix<MatrixDimension>> const_f = f;
              DiscreteFunctionP0<const TinyMatrix<MatrixDimension>> const_g{g};

              SECTION("sum")
              {
                Array<TinyMatrix<MatrixDimension>> sum_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + g[cell_id]; });

                REQUIRE(same_values(f + g, sum_values));
                REQUIRE(same_values(const_f + g, sum_values));
                REQUIRE(same_values(f + const_g, sum_values));
                REQUIRE(same_values(const_f + const_g, sum_values));
              }

              SECTION("difference")
              {
                Array<TinyMatrix<MatrixDimension>> difference_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - g[cell_id]; });

                REQUIRE(same_values(f - g, difference_values));
                REQUIRE(same_values(const_f - g, difference_values));
                REQUIRE(same_values(f - const_g, difference_values));
                REQUIRE(same_values(const_f - const_g, difference_values));
              }

              SECTION("product")
              {
                Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                parallel_for(
                  mesh->numberOfCells(),
                  PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * g[cell_id]; });

                REQUIRE(same_values(f * g, product_values));
                REQUIRE(same_values(const_f * g, product_values));
                REQUIRE(same_values(f * const_g, product_values));
                REQUIRE(same_values(const_f * const_g, product_values));
              }
            }
          }

          SECTION("external operators")
          {
            SECTION("scalar functions")
            {
              DiscreteFunctionP0<double> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const double y = xj[cell_id][1];
                  const double z = xj[cell_id][2];
                  f[cell_id]     = std::abs(2 * x + y * z) + 1;
                });

              const double a = 3;

              DiscreteFunctionP0<const double> const_f = f;

              SECTION("sum")
              {
                {
                  Array<double> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = a + f[cell_id]; });

                  REQUIRE(same_values(a + f, sum_values));
                  REQUIRE(same_values(a + const_f, sum_values));
                }
                {
                  Array<double> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + a; });

                  REQUIRE(same_values(f + a, sum_values));
                  REQUIRE(same_values(const_f + a, sum_values));
                }
              }

              SECTION("difference")
              {
                {
                  Array<double> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = a - f[cell_id]; });
                  REQUIRE(same_values(a - f, difference_values));
                  REQUIRE(same_values(a - const_f, difference_values));
                }

                {
                  Array<double> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - a; });
                  REQUIRE(same_values(f - a, difference_values));
                  REQUIRE(same_values(const_f - a, difference_values));
                }
              }

              SECTION("product")
              {
                {
                  Array<double> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }
                {
                  Array<double> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * a; });

                  REQUIRE(same_values(f * a, product_values));
                  REQUIRE(same_values(const_f * a, product_values));
                }

                {
                  Array<TinyVector<3>> product_values{mesh->numberOfCells()};
                  const TinyVector<3> v{1, 2, 3};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * v; });

                  REQUIRE(same_values(f * v, product_values));
                  REQUIRE(same_values(const_f * v, product_values));
                }

                {
                  Array<TinyVector<3>> product_values{mesh->numberOfCells()};
                  DiscreteFunctionP0<TinyVector<3>> v{mesh};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      v[cell_id]     = TinyVector<3>{x, 2 * x, 1 - x};
                    });

                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * v[cell_id]; });

                  REQUIRE(same_values(f * v, product_values));
                  REQUIRE(same_values(const_f * v, product_values));
                }

                {
                  Array<TinyMatrix<2>> product_values{mesh->numberOfCells()};
                  const TinyMatrix<2> A{1, 2, 3, 4};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * A; });

                  REQUIRE(same_values(f * A, product_values));
                  REQUIRE(same_values(const_f * A, product_values));
                }

                {
                  Array<TinyMatrix<2>> product_values{mesh->numberOfCells()};
                  DiscreteFunctionP0<TinyMatrix<2>> M{mesh};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      M[cell_id]     = TinyMatrix<2>{x, 2 * x, 1 - x, 2 - x * x};
                    });

                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * M[cell_id]; });

                  REQUIRE(same_values(f * M, product_values));
                  REQUIRE(same_values(const_f * M, product_values));
                }
              }

              SECTION("ratio")
              {
                {
                  Array<double> ratio_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { ratio_values[cell_id] = a / f[cell_id]; });

                  REQUIRE(same_values(a / f, ratio_values));
                  REQUIRE(same_values(a / const_f, ratio_values));
                }
                {
                  Array<double> ratio_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { ratio_values[cell_id] = f[cell_id] / a; });

                  REQUIRE(same_values(f / a, ratio_values));
                  REQUIRE(same_values(const_f / a, ratio_values));
                }
              }
            }

            SECTION("vector functions")
            {
              constexpr std::uint64_t VectorDimension = 2;

              DiscreteFunctionP0<TinyVector<VectorDimension>> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const double y = xj[cell_id][1];
                  const double z = xj[cell_id][2];
                  const TinyVector<VectorDimension> X{x + y - z, 2 - x * y};
                  f[cell_id] = 2 * X + TinyVector<2>{1, 2};
                });

              DiscreteFunctionP0<const TinyVector<VectorDimension>> const_f = f;

              SECTION("sum")
              {
                const TinyVector<VectorDimension> v{1, 2};
                {
                  Array<TinyVector<VectorDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = v + f[cell_id]; });

                  REQUIRE(same_values(v + f, sum_values));
                  REQUIRE(same_values(v + const_f, sum_values));
                }
                {
                  Array<TinyVector<VectorDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + v; });

                  REQUIRE(same_values(f + v, sum_values));
                  REQUIRE(same_values(const_f + v, sum_values));
                }
              }

              SECTION("difference")
              {
                const TinyVector<VectorDimension> v{1, 2};
                {
                  Array<TinyVector<VectorDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = v - f[cell_id]; });

                  REQUIRE(same_values(v - f, difference_values));
                  REQUIRE(same_values(v - const_f, difference_values));
                }
                {
                  Array<TinyVector<VectorDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - v; });

                  REQUIRE(same_values(f - v, difference_values));
                  REQUIRE(same_values(const_f - v, difference_values));
                }
              }

              SECTION("product")
              {
                {
                  const double a = 2.3;
                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  DiscreteFunctionP0<double> a{mesh};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      a[cell_id]     = 2 * x * x - 1;
                    });

                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a[cell_id] * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  const TinyMatrix<VectorDimension> A{1, 2, 3, 4};
                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = A * f[cell_id]; });

                  REQUIRE(same_values(A * f, product_values));
                  REQUIRE(same_values(A * const_f, product_values));
                }

                {
                  Array<TinyVector<VectorDimension>> product_values{mesh->numberOfCells()};
                  DiscreteFunctionP0<TinyMatrix<VectorDimension>> M{mesh};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      M[cell_id]     = TinyMatrix<2>{x, 2 * x, 1 - x, 2 - x * x};
                    });

                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = M[cell_id] * f[cell_id]; });

                  REQUIRE(same_values(M * f, product_values));
                  REQUIRE(same_values(M * const_f, product_values));
                }
              }
            }

            SECTION("matrix functions")
            {
              constexpr std::uint64_t MatrixDimension = 2;

              DiscreteFunctionP0<TinyMatrix<MatrixDimension>> f{mesh};
              parallel_for(
                mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                  const double x = xj[cell_id][0];
                  const double y = xj[cell_id][1];
                  const double z = xj[cell_id][2];
                  const TinyMatrix<MatrixDimension> X{x, 2 - y, x * y, y * z + 3};
                  f[cell_id] = 2 * X + TinyMatrix<2>{1, 2, 3, 4};
                });

              DiscreteFunctionP0<const TinyMatrix<MatrixDimension>> const_f = f;

              SECTION("sum")
              {
                const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                {
                  Array<TinyMatrix<MatrixDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = A + f[cell_id]; });

                  REQUIRE(same_values(A + f, sum_values));
                  REQUIRE(same_values(A + const_f, sum_values));
                }
                {
                  Array<TinyMatrix<MatrixDimension>> sum_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum_values[cell_id] = f[cell_id] + A; });

                  REQUIRE(same_values(f + A, sum_values));
                  REQUIRE(same_values(const_f + A, sum_values));
                }
              }

              SECTION("difference")
              {
                const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                {
                  Array<TinyMatrix<MatrixDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = A - f[cell_id]; });

                  REQUIRE(same_values(A - f, difference_values));
                  REQUIRE(same_values(A - const_f, difference_values));
                }
                {
                  Array<TinyMatrix<MatrixDimension>> difference_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { difference_values[cell_id] = f[cell_id] - A; });

                  REQUIRE(same_values(f - A, difference_values));
                  REQUIRE(same_values(const_f - A, difference_values));
                }
              }

              SECTION("product")
              {
                {
                  const double a = 2.3;
                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  DiscreteFunctionP0<double> a{mesh};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                      const double x = xj[cell_id][0];
                      a[cell_id]     = 2 * x * x - 1;
                    });

                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(),
                    PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = a[cell_id] * f[cell_id]; });

                  REQUIRE(same_values(a * f, product_values));
                  REQUIRE(same_values(a * const_f, product_values));
                }

                {
                  const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = A * f[cell_id]; });

                  REQUIRE(same_values(A * f, product_values));
                  REQUIRE(same_values(A * const_f, product_values));
                }

                {
                  const TinyMatrix<MatrixDimension> A{1, 2, 3, 4};
                  Array<TinyMatrix<MatrixDimension>> product_values{mesh->numberOfCells()};
                  parallel_for(
                    mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product_values[cell_id] = f[cell_id] * A; });

                  REQUIRE(same_values(f * A, product_values));
                  REQUIRE(same_values(const_f * A, product_values));
                }
              }
            }
          }
        }
      }
    }
  }

  SECTION("math functions")
  {
#define CHECK_STD_MATH_FUNCTION(data_expression, FCT)                           \
  {                                                                             \
    DiscreteFunctionP0 data   = data_expression;                                \
    DiscreteFunctionP0 result = FCT(data);                                      \
    bool is_same              = true;                                           \
    parallel_for(data.cellValues().numberOfItems(), [&](const CellId cell_id) { \
      if (result[cell_id] != std::FCT(data[cell_id])) {                         \
        is_same = false;                                                        \
      }                                                                         \
    });                                                                         \
    REQUIRE(is_same);                                                           \
  }

#define CHECK_STD_BINARY_MATH_FUNCTION(lhs_expression, rhs_expression, FCT)    \
  {                                                                            \
    DiscreteFunctionP0 lhs    = lhs_expression;                                \
    DiscreteFunctionP0 rhs    = rhs_expression;                                \
    DiscreteFunctionP0 result = FCT(lhs, rhs);                                 \
    using namespace std;                                                       \
    bool is_same = true;                                                       \
    parallel_for(lhs.cellValues().numberOfItems(), [&](const CellId cell_id) { \
      if (result[cell_id] != FCT(lhs[cell_id], rhs[cell_id])) {                \
        is_same = false;                                                       \
      }                                                                        \
    });                                                                        \
    REQUIRE(is_same);                                                          \
  }

#define CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(lhs, rhs_expression, FCT) \
  {                                                                             \
    DiscreteFunctionP0 rhs    = rhs_expression;                                 \
    DiscreteFunctionP0 result = FCT(lhs, rhs);                                  \
    bool is_same              = true;                                           \
    using namespace std;                                                        \
    parallel_for(rhs.cellValues().numberOfItems(), [&](const CellId cell_id) {  \
      if (result[cell_id] != FCT(lhs, rhs[cell_id])) {                          \
        is_same = false;                                                        \
      }                                                                         \
    });                                                                         \
    REQUIRE(is_same);                                                           \
  }

#define CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(lhs_expression, rhs, FCT) \
  {                                                                             \
    DiscreteFunctionP0 lhs    = lhs_expression;                                 \
    DiscreteFunctionP0 result = FCT(lhs, rhs);                                  \
    bool is_same              = true;                                           \
    using namespace std;                                                        \
    parallel_for(lhs.cellValues().numberOfItems(), [&](const CellId cell_id) {  \
      if (result[cell_id] != FCT(lhs[cell_id], rhs)) {                          \
        is_same = false;                                                        \
      }                                                                         \
    });                                                                         \
    REQUIRE(is_same);                                                           \
  }

    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh = named_mesh.mesh()->get<Mesh<1>>();

          auto xj = MeshDataManager::instance().getMeshData(*mesh).xj();

          DiscreteFunctionP0<double> positive_function{mesh};

          parallel_for(
            mesh->numberOfCells(),
            PUGS_LAMBDA(const CellId cell_id) { positive_function[cell_id] = 1 + std::abs(xj[cell_id][0]); });

          const double min_value = min(positive_function);
          SECTION("min")
          {
            double local_min = std::numeric_limits<double>::max();
            for (CellId cell_id = 0; cell_id < mesh->numberOfCells(); ++cell_id) {
              local_min = std::min(local_min, positive_function[cell_id]);
            }
            REQUIRE(min_value == parallel::allReduceMin(local_min));
          }

          const double max_value = max(positive_function);
          SECTION("max")
          {
            double local_max = -std::numeric_limits<double>::max();
            for (CellId cell_id = 0; cell_id < mesh->numberOfCells(); ++cell_id) {
              local_max = std::max(local_max, positive_function[cell_id]);
            }
            REQUIRE(max_value == parallel::allReduceMax(local_max));
          }

          REQUIRE(min_value < max_value);

          DiscreteFunctionP0 unsigned_function = positive_function - 0.5 * (min_value + max_value);

          SECTION("sqrt")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, sqrt);
          }

          SECTION("abs")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, abs);
          }

          SECTION("cos")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, cos);
          }

          SECTION("sin")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, sin);
          }

          SECTION("tan")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, tan);
          }

          DiscreteFunctionP0<double> unit_function{mesh};

          parallel_for(
            mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
              unit_function[cell_id] =
                (2 * (positive_function[cell_id] - min_value) / (max_value - min_value) - 1) * 0.95;
            });

          SECTION("acos")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, acos);
          }

          SECTION("asin")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, asin);
          }

          SECTION("atan")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, atan);
          }

          SECTION("cosh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, cosh);
          }

          SECTION("sinh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, sinh);
          }

          SECTION("tanh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, tanh);
          }

          SECTION("acosh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, acosh);
          }

          SECTION("asinh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, asinh);
          }

          SECTION("atanh")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, atanh);
          }

          SECTION("exp")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, exp);
          }

          SECTION("log")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, log);
          }

          SECTION("max(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(cos(positive_function), sin(positive_function), max);
          }

          SECTION("max(0.2,vh)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.2, sin(positive_function), max);
          }

          SECTION("max(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(cos(positive_function), 0.2, max);
          }

          SECTION("atan2(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(positive_function, 2 + positive_function, atan2);
          }

          SECTION("atan2(0.5,uh)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.5, 2 + positive_function, atan2);
          }

          SECTION("atan2(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(2 + cos(positive_function), 0.2, atan2);
          }

          SECTION("pow(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(positive_function, 0.5 * positive_function, pow);
          }

          SECTION("pow(uh,0.5)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.5, positive_function, pow);
          }

          SECTION("pow(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(positive_function, 1.3, pow);
          }

          SECTION("min(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(sin(positive_function), cos(positive_function), min);
          }

          SECTION("min(uh,0.5)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.5, cos(positive_function), min);
          }

          SECTION("min(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(sin(positive_function), 0.5, min);
          }

          SECTION("max(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(sin(positive_function), cos(positive_function), max);
          }

          SECTION("min(uh,0.5)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.1, cos(positive_function), max);
          }

          SECTION("min(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(sin(positive_function), 0.1, max);
          }

          SECTION("dot(uh,hv)")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });

            DiscreteFunctionP0<TinyVector<2>> vh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                vh[cell_id]    = TinyVector<2>{2.3 * x, 1 - x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION(uh, vh, dot);
          }

          SECTION("dot(uh,v)")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });

            const TinyVector<2> v{1, 2};

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(uh, v, dot);
          }

          SECTION("dot(u,hv)")
          {
            const TinyVector<2> u{3, -2};

            DiscreteFunctionP0<TinyVector<2>> vh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                vh[cell_id]    = TinyVector<2>{2.3 * x, 1 - x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(u, vh, dot);
          }

          SECTION("doubleDot(Ah,Bh)")
          {
            DiscreteFunctionP0<TinyMatrix<2>> Ah{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Ah[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 3 - x, 3 * x - 2};
              });

            DiscreteFunctionP0<TinyMatrix<2>> Bh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Bh[cell_id]    = TinyMatrix<2>{2.3 * x, 1 - x, 2 * x, 1.5 * x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION(Ah, Bh, doubleDot);
          }

          SECTION("doubleDot(Ah,B)")
          {
            DiscreteFunctionP0<TinyMatrix<2>> Ah{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Ah[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 3 - x, 3 * x - 2};
              });

            const TinyMatrix<2> B{1, 2, 3, 4};

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(Ah, B, doubleDot);
          }

          SECTION("doubleDot(A,Bh)")
          {
            const TinyMatrix<2> A{3, -2, 4, 5};

            DiscreteFunctionP0<TinyMatrix<2>> Bh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Bh[cell_id]    = TinyMatrix<2>{2.3 * x, 1 - x, 2 * x, -x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(A, Bh, doubleDot);
          }

          SECTION("tensorProduct(uh,hv)")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });

            DiscreteFunctionP0<TinyVector<2>> vh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                vh[cell_id]    = TinyVector<2>{2.3 * x, 1 - x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION(uh, vh, tensorProduct);
          }

          SECTION("tensorProduct(uh,v)")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });

            const TinyVector<2> v{1, 2};

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(uh, v, tensorProduct);
          }

          SECTION("tensorProduct(u,hv)")
          {
            const TinyVector<2> u{3, -2};

            DiscreteFunctionP0<TinyVector<2>> vh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                vh[cell_id]    = TinyVector<2>{2.3 * x, 1 - x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(u, vh, tensorProduct);
          }

          SECTION("scalar sum")
          {
            const CellValue<const double> cell_value = positive_function.cellValues();

            REQUIRE(sum(cell_value) == sum(positive_function));
          }

          SECTION("vector sum")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });
            const CellValue<const TinyVector<2>> cell_value = uh.cellValues();

            REQUIRE(sum(cell_value) == sum(uh));
          }

          SECTION("matrix sum")
          {
            DiscreteFunctionP0<TinyMatrix<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 2 * x, 3 * x - 1};
              });
            const CellValue<const TinyMatrix<2>> cell_value = uh.cellValues();

            REQUIRE(sum(cell_value) == sum(uh));
          }

          SECTION("integrate scalar")
          {
            const CellValue<const double> cell_volume = MeshDataManager::instance().getMeshData(*mesh).Vj();

            CellValue<double> cell_value{mesh->connectivity()};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                cell_value[cell_id] = cell_volume[cell_id] * positive_function[cell_id];
              });

            REQUIRE(integrate(positive_function) == sum(cell_value));
          }

          SECTION("integrate vector")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });

            const CellValue<const double> cell_volume = MeshDataManager::instance().getMeshData(*mesh).Vj();

            CellValue<TinyVector<2>> cell_value{mesh->connectivity()};
            parallel_for(
              mesh->numberOfCells(),
              PUGS_LAMBDA(const CellId cell_id) { cell_value[cell_id] = cell_volume[cell_id] * uh[cell_id]; });

            REQUIRE(integrate(uh)[0] == sum(cell_value)[0]);
            REQUIRE(integrate(uh)[1] == sum(cell_value)[1]);
          }

          SECTION("integrate matrix")
          {
            DiscreteFunctionP0<TinyMatrix<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 2 * x, 1 - x};
              });

            const CellValue<const double> cell_volume = MeshDataManager::instance().getMeshData(*mesh).Vj();

            CellValue<TinyMatrix<2>> cell_value{mesh->connectivity()};
            parallel_for(
              mesh->numberOfCells(),
              PUGS_LAMBDA(const CellId cell_id) { cell_value[cell_id] = cell_volume[cell_id] * uh[cell_id]; });

            REQUIRE(integrate(uh)(0, 0) == sum(cell_value)(0, 0));
            REQUIRE(integrate(uh)(0, 1) == sum(cell_value)(0, 1));
            REQUIRE(integrate(uh)(1, 0) == sum(cell_value)(1, 0));
            REQUIRE(integrate(uh)(1, 1) == sum(cell_value)(1, 1));
          }
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh = named_mesh.mesh()->get<Mesh<2>>();

          auto xj = MeshDataManager::instance().getMeshData(*mesh).xj();

          DiscreteFunctionP0<double> positive_function{mesh};

          parallel_for(
            mesh->numberOfCells(),
            PUGS_LAMBDA(const CellId cell_id) { positive_function[cell_id] = 1 + std::abs(xj[cell_id][0]); });

          const double min_value = min(positive_function);
          SECTION("min")
          {
            double local_min = std::numeric_limits<double>::max();
            for (CellId cell_id = 0; cell_id < mesh->numberOfCells(); ++cell_id) {
              local_min = std::min(local_min, positive_function[cell_id]);
            }
            REQUIRE(min_value == parallel::allReduceMin(local_min));
          }

          const double max_value = max(positive_function);
          SECTION("max")
          {
            double local_max = -std::numeric_limits<double>::max();
            for (CellId cell_id = 0; cell_id < mesh->numberOfCells(); ++cell_id) {
              local_max = std::max(local_max, positive_function[cell_id]);
            }
            REQUIRE(max_value == parallel::allReduceMax(local_max));
          }

          REQUIRE(min_value < max_value);

          DiscreteFunctionP0 unsigned_function = positive_function - 0.5 * (min_value + max_value);

          SECTION("sqrt")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, sqrt);
          }

          SECTION("abs")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, abs);
          }

          SECTION("cos")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, cos);
          }

          SECTION("sin")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, sin);
          }

          SECTION("tan")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, tan);
          }

          DiscreteFunctionP0<double> unit_function{mesh};

          parallel_for(
            mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
              unit_function[cell_id] =
                (2 * (positive_function[cell_id] - min_value) / (max_value - min_value) - 1) * 0.95;
            });

          SECTION("acos")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, acos);
          }

          SECTION("asin")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, asin);
          }

          SECTION("atan")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, atan);
          }

          SECTION("cosh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, cosh);
          }

          SECTION("sinh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, sinh);
          }

          SECTION("tanh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, tanh);
          }

          SECTION("acosh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, acosh);
          }

          SECTION("asinh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, asinh);
          }

          SECTION("atanh")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, atanh);
          }

          SECTION("exp")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, exp);
          }

          SECTION("log")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, log);
          }

          SECTION("max(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(cos(positive_function), sin(positive_function), max);
          }

          SECTION("max(0.2,vh)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.2, sin(positive_function), max);
          }

          SECTION("max(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(cos(positive_function), 0.2, max);
          }

          SECTION("atan2(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(positive_function, 2 + positive_function, atan2);
          }

          SECTION("atan2(0.5,uh)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.5, 2 + positive_function, atan2);
          }

          SECTION("atan2(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(2 + cos(positive_function), 0.2, atan2);
          }

          SECTION("pow(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(positive_function, 0.5 * positive_function, pow);
          }

          SECTION("pow(uh,0.5)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.5, positive_function, pow);
          }

          SECTION("pow(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(positive_function, 1.3, pow);
          }

          SECTION("min(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(sin(positive_function), cos(positive_function), min);
          }

          SECTION("min(uh,0.5)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.5, cos(positive_function), min);
          }

          SECTION("min(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(sin(positive_function), 0.5, min);
          }

          SECTION("max(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(sin(positive_function), cos(positive_function), max);
          }

          SECTION("min(uh,0.5)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.1, cos(positive_function), max);
          }

          SECTION("min(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(sin(positive_function), 0.1, max);
          }

          SECTION("dot(uh,hv)")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });

            DiscreteFunctionP0<TinyVector<2>> vh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                vh[cell_id]    = TinyVector<2>{2.3 * x, 1 - x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION(uh, vh, dot);
          }

          SECTION("dot(uh,v)")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });

            const TinyVector<2> v{1, 2};

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(uh, v, dot);
          }

          SECTION("dot(u,hv)")
          {
            const TinyVector<2> u{3, -2};

            DiscreteFunctionP0<TinyVector<2>> vh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                vh[cell_id]    = TinyVector<2>{2.3 * x, 1 - x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(u, vh, dot);
          }

          SECTION("doubleDot(Ah,Bh)")
          {
            DiscreteFunctionP0<TinyMatrix<2>> Ah{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Ah[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 3 - x, 3 * x - 2};
              });

            DiscreteFunctionP0<TinyMatrix<2>> Bh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Bh[cell_id]    = TinyMatrix<2>{2.3 * x, 1 - x, 2 * x, 1.5 * x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION(Ah, Bh, doubleDot);
          }

          SECTION("doubleDot(Ah,B)")
          {
            DiscreteFunctionP0<TinyMatrix<2>> Ah{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Ah[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 3 - x, 3 * x - 2};
              });

            const TinyMatrix<2> B{1, 2, 3, 4};

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(Ah, B, doubleDot);
          }

          SECTION("doubleDot(A,Bh)")
          {
            const TinyMatrix<2> A{3, -2, 4, 5};

            DiscreteFunctionP0<TinyMatrix<2>> Bh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Bh[cell_id]    = TinyMatrix<2>{2.3 * x, 1 - x, 2 * x, -x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(A, Bh, doubleDot);
          }

          SECTION("scalar sum")
          {
            const CellValue<const double> cell_value = positive_function.cellValues();

            REQUIRE(sum(cell_value) == sum(positive_function));
          }

          SECTION("vector sum")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });
            const CellValue<const TinyVector<2>> cell_value = uh.cellValues();

            REQUIRE(sum(cell_value) == sum(uh));
          }

          SECTION("matrix sum")
          {
            DiscreteFunctionP0<TinyMatrix<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 2 * x, 3 * x - 1};
              });
            const CellValue<const TinyMatrix<2>> cell_value = uh.cellValues();

            REQUIRE(sum(cell_value) == sum(uh));
          }

          SECTION("integrate scalar")
          {
            const CellValue<const double> cell_volume = MeshDataManager::instance().getMeshData(*mesh).Vj();

            CellValue<double> cell_value{mesh->connectivity()};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                cell_value[cell_id] = cell_volume[cell_id] * positive_function[cell_id];
              });

            REQUIRE(integrate(positive_function) == sum(cell_value));
          }

          SECTION("integrate vector")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });

            const CellValue<const double> cell_volume = MeshDataManager::instance().getMeshData(*mesh).Vj();

            CellValue<TinyVector<2>> cell_value{mesh->connectivity()};
            parallel_for(
              mesh->numberOfCells(),
              PUGS_LAMBDA(const CellId cell_id) { cell_value[cell_id] = cell_volume[cell_id] * uh[cell_id]; });

            REQUIRE(integrate(uh)[0] == sum(cell_value)[0]);
            REQUIRE(integrate(uh)[1] == sum(cell_value)[1]);
          }

          SECTION("integrate matrix")
          {
            DiscreteFunctionP0<TinyMatrix<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 2 * x, 1 - x};
              });

            const CellValue<const double> cell_volume = MeshDataManager::instance().getMeshData(*mesh).Vj();

            CellValue<TinyMatrix<2>> cell_value{mesh->connectivity()};
            parallel_for(
              mesh->numberOfCells(),
              PUGS_LAMBDA(const CellId cell_id) { cell_value[cell_id] = cell_volume[cell_id] * uh[cell_id]; });

            REQUIRE(integrate(uh)(0, 0) == sum(cell_value)(0, 0));
            REQUIRE(integrate(uh)(0, 1) == sum(cell_value)(0, 1));
            REQUIRE(integrate(uh)(1, 0) == sum(cell_value)(1, 0));
            REQUIRE(integrate(uh)(1, 1) == sum(cell_value)(1, 1));
          }
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh = named_mesh.mesh()->get<Mesh<3>>();

          auto xj = MeshDataManager::instance().getMeshData(*mesh).xj();

          DiscreteFunctionP0<double> positive_function{mesh};

          parallel_for(
            mesh->numberOfCells(),
            PUGS_LAMBDA(const CellId cell_id) { positive_function[cell_id] = 1 + std::abs(xj[cell_id][0]); });

          const double min_value = min(positive_function);
          SECTION("min")
          {
            double local_min = std::numeric_limits<double>::max();
            for (CellId cell_id = 0; cell_id < mesh->numberOfCells(); ++cell_id) {
              local_min = std::min(local_min, positive_function[cell_id]);
            }
            REQUIRE(min_value == parallel::allReduceMin(local_min));
          }

          const double max_value = max(positive_function);
          SECTION("max")
          {
            double local_max = -std::numeric_limits<double>::max();
            for (CellId cell_id = 0; cell_id < mesh->numberOfCells(); ++cell_id) {
              local_max = std::max(local_max, positive_function[cell_id]);
            }
            REQUIRE(max_value == parallel::allReduceMax(local_max));
          }

          REQUIRE(min_value < max_value);

          DiscreteFunctionP0 unsigned_function = positive_function - 0.5 * (min_value + max_value);

          SECTION("sqrt")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, sqrt);
          }

          SECTION("abs")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, abs);
          }

          SECTION("cos")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, cos);
          }

          SECTION("sin")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, sin);
          }

          SECTION("tan")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, tan);
          }

          DiscreteFunctionP0<double> unit_function{mesh};

          parallel_for(
            mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
              unit_function[cell_id] =
                (2 * (positive_function[cell_id] - min_value) / (max_value - min_value) - 1) * 0.95;
            });

          SECTION("acos")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, acos);
          }

          SECTION("asin")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, asin);
          }

          SECTION("atan")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, atan);
          }

          SECTION("cosh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, cosh);
          }

          SECTION("sinh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, sinh);
          }

          SECTION("tanh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, tanh);
          }

          SECTION("acosh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, acosh);
          }

          SECTION("asinh")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, asinh);
          }

          SECTION("atanh")
          {
            CHECK_STD_MATH_FUNCTION(unit_function, atanh);
          }

          SECTION("exp")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, exp);
          }

          SECTION("log")
          {
            CHECK_STD_MATH_FUNCTION(positive_function, log);
          }

          SECTION("max(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(cos(positive_function), sin(positive_function), max);
          }

          SECTION("max(0.2,vh)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.2, sin(positive_function), max);
          }

          SECTION("max(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(cos(positive_function), 0.2, max);
          }

          SECTION("atan2(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(positive_function, 2 + positive_function, atan2);
          }

          SECTION("atan2(0.5,uh)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.5, 2 + positive_function, atan2);
          }

          SECTION("atan2(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(2 + cos(positive_function), 0.2, atan2);
          }

          SECTION("pow(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(positive_function, 0.5 * positive_function, pow);
          }

          SECTION("pow(uh,0.5)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.5, positive_function, pow);
          }

          SECTION("pow(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(positive_function, 1.3, pow);
          }

          SECTION("min(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(sin(positive_function), cos(positive_function), min);
          }

          SECTION("min(uh,0.5)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.5, cos(positive_function), min);
          }

          SECTION("min(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(sin(positive_function), 0.5, min);
          }

          SECTION("max(uh,hv)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION(sin(positive_function), cos(positive_function), max);
          }

          SECTION("min(uh,0.5)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(0.1, cos(positive_function), max);
          }

          SECTION("min(uh,0.2)")
          {
            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(sin(positive_function), 0.1, max);
          }

          SECTION("dot(uh,hv)")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });

            DiscreteFunctionP0<TinyVector<2>> vh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                vh[cell_id]    = TinyVector<2>{2.3 * x, 1 - x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION(uh, vh, dot);
          }

          SECTION("dot(uh,v)")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });

            const TinyVector<2> v{1, 2};

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(uh, v, dot);
          }

          SECTION("dot(u,hv)")
          {
            const TinyVector<2> u{3, -2};

            DiscreteFunctionP0<TinyVector<2>> vh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                vh[cell_id]    = TinyVector<2>{2.3 * x, 1 - x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(u, vh, dot);
          }

          SECTION("doubleDot(Ah,Bh)")
          {
            DiscreteFunctionP0<TinyMatrix<2>> Ah{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Ah[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 3 - x, 3 * x - 2};
              });

            DiscreteFunctionP0<TinyMatrix<2>> Bh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Bh[cell_id]    = TinyMatrix<2>{2.3 * x, 1 - x, 2 * x, 1.5 * x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION(Ah, Bh, doubleDot);
          }

          SECTION("doubleDot(Ah,B)")
          {
            DiscreteFunctionP0<TinyMatrix<2>> Ah{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Ah[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 3 - x, 3 * x - 2};
              });

            const TinyMatrix<2> B{1, 2, 3, 4};

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_RHS_VALUE(Ah, B, doubleDot);
          }

          SECTION("doubleDot(A,Bh)")
          {
            const TinyMatrix<2> A{3, -2, 4, 5};

            DiscreteFunctionP0<TinyMatrix<2>> Bh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Bh[cell_id]    = TinyMatrix<2>{2.3 * x, 1 - x, 2 * x, -x};
              });

            CHECK_STD_BINARY_MATH_FUNCTION_WITH_LHS_VALUE(A, Bh, doubleDot);
          }

          SECTION("det(Ah)")
          {
            DiscreteFunctionP0<TinyMatrix<2>> Ah{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Ah[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3,   //
                                               -0.2 * x - 1, 2 + x};
              });

            {
              DiscreteFunctionP0 result = det(Ah);
              bool is_same              = true;
              parallel_for(Ah.cellValues().numberOfItems(), [&](const CellId cell_id) {
                if (result[cell_id] != det(Ah[cell_id])) {
                  is_same = false;
                }
              });
              REQUIRE(is_same);
            }
          }

          SECTION("trace(Ah)")
          {
            DiscreteFunctionP0<TinyMatrix<2>> Ah{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Ah[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3,   //
                                               -0.2 * x - 1, 2 + x};
              });

            {
              DiscreteFunctionP0 result = trace(Ah);
              bool is_same              = true;
              parallel_for(Ah.cellValues().numberOfItems(), [&](const CellId cell_id) {
                if (result[cell_id] != trace(Ah[cell_id])) {
                  is_same = false;
                }
              });
              REQUIRE(is_same);
            }
          }

          SECTION("inverse(Ah)")
          {
            DiscreteFunctionP0<TinyMatrix<2>> Ah{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Ah[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3,   //
                                               -0.2 * x - 1, 2 + x};
              });

            {
              DiscreteFunctionP0 result = inverse(Ah);
              bool is_same              = true;
              parallel_for(Ah.cellValues().numberOfItems(), [&](const CellId cell_id) {
                if (result[cell_id] != inverse(Ah[cell_id])) {
                  is_same = false;
                }
              });
              REQUIRE(is_same);
            }
          }

          SECTION("transpose(Ah)")
          {
            DiscreteFunctionP0<TinyMatrix<2>> Ah{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                Ah[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3,   //
                                               -0.2 * x - 1, 2 + x};
              });

            {
              DiscreteFunctionP0 result = transpose(Ah);
              bool is_same              = true;
              parallel_for(Ah.cellValues().numberOfItems(), [&](const CellId cell_id) {
                if (result[cell_id] != transpose(Ah[cell_id])) {
                  is_same = false;
                }
              });
              REQUIRE(is_same);
            }
          }

          SECTION("scalar sum")
          {
            const CellValue<const double> cell_value = positive_function.cellValues();

            REQUIRE(sum(cell_value) == sum(positive_function));
          }

          SECTION("vector sum")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });
            const CellValue<const TinyVector<2>> cell_value = uh.cellValues();

            REQUIRE(sum(cell_value) == sum(uh));
          }

          SECTION("matrix sum")
          {
            DiscreteFunctionP0<TinyMatrix<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 2 * x, 3 * x - 1};
              });
            const CellValue<const TinyMatrix<2>> cell_value = uh.cellValues();

            REQUIRE(sum(cell_value) == sum(uh));
          }

          SECTION("integrate scalar")
          {
            const CellValue<const double> cell_volume = MeshDataManager::instance().getMeshData(*mesh).Vj();

            CellValue<double> cell_value{mesh->connectivity()};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                cell_value[cell_id] = cell_volume[cell_id] * positive_function[cell_id];
              });

            REQUIRE(integrate(positive_function) == sum(cell_value));
          }

          SECTION("integrate vector")
          {
            DiscreteFunctionP0<TinyVector<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyVector<2>{x + 1, 2 * x - 3};
              });

            const CellValue<const double> cell_volume = MeshDataManager::instance().getMeshData(*mesh).Vj();

            CellValue<TinyVector<2>> cell_value{mesh->connectivity()};
            parallel_for(
              mesh->numberOfCells(),
              PUGS_LAMBDA(const CellId cell_id) { cell_value[cell_id] = cell_volume[cell_id] * uh[cell_id]; });

            REQUIRE(integrate(uh)[0] == sum(cell_value)[0]);
            REQUIRE(integrate(uh)[1] == sum(cell_value)[1]);
          }

          SECTION("integrate matrix")
          {
            DiscreteFunctionP0<TinyMatrix<2>> uh{mesh};
            parallel_for(
              mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) {
                const double x = xj[cell_id][0];
                uh[cell_id]    = TinyMatrix<2>{x + 1, 2 * x - 3, 2 * x, 1 - x};
              });

            const CellValue<const double> cell_volume = MeshDataManager::instance().getMeshData(*mesh).Vj();

            CellValue<TinyMatrix<2>> cell_value{mesh->connectivity()};
            parallel_for(
              mesh->numberOfCells(),
              PUGS_LAMBDA(const CellId cell_id) { cell_value[cell_id] = cell_volume[cell_id] * uh[cell_id]; });

            REQUIRE(integrate(uh)(0, 0) == sum(cell_value)(0, 0));
            REQUIRE(integrate(uh)(0, 1) == sum(cell_value)(0, 1));
            REQUIRE(integrate(uh)(1, 0) == sum(cell_value)(1, 0));
            REQUIRE(integrate(uh)(1, 1) == sum(cell_value)(1, 1));
          }
        }
      }
    }
  }

#ifndef NDEBUG
  SECTION("error")
  {
    SECTION("different meshes")
    {
      SECTION("1D")
      {
        constexpr size_t Dimension = 1;

        std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

        for (const auto& named_mesh : mesh_list) {
          SECTION(named_mesh.name())
          {
            auto mesh_1 = named_mesh.mesh()->get<Mesh<1>>();

            std::shared_ptr mesh_2 =
              std::make_shared<const Mesh<Dimension>>(mesh_1->shared_connectivity(), mesh_1->xr());

            DiscreteFunctionP0<double> f1{mesh_1};
            DiscreteFunctionP0<double> f2{mesh_2};

            REQUIRE_THROWS_AS(f1 = f2, AssertError);
            REQUIRE_THROWS_AS(copy_to(f1, f2), AssertError);
            REQUIRE_THROWS_AS(f1 + f2, AssertError);
            REQUIRE_THROWS_AS(f1 - f2, AssertError);
            REQUIRE_THROWS_AS(f1 * f2, AssertError);
            REQUIRE_THROWS_AS(f1 / f2, AssertError);
          }
        }
      }

      SECTION("2D")
      {
        constexpr size_t Dimension = 2;

        std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

        for (const auto& named_mesh : mesh_list) {
          SECTION(named_mesh.name())
          {
            auto mesh_1 = named_mesh.mesh()->get<Mesh<2>>();

            std::shared_ptr mesh_2 =
              std::make_shared<const Mesh<Dimension>>(mesh_1->shared_connectivity(), mesh_1->xr());

            DiscreteFunctionP0<double> f1{mesh_1};
            DiscreteFunctionP0<double> f2{mesh_2};

            REQUIRE_THROWS_AS(f1 = f2, AssertError);
            REQUIRE_THROWS_AS(copy_to(f1, f2), AssertError);
            REQUIRE_THROWS_AS(f1 + f2, AssertError);
            REQUIRE_THROWS_AS(f1 - f2, AssertError);
            REQUIRE_THROWS_AS(f1 * f2, AssertError);
            REQUIRE_THROWS_AS(f1 / f2, AssertError);
          }
        }
      }

      SECTION("3D")
      {
        constexpr size_t Dimension = 3;

        std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

        for (const auto& named_mesh : mesh_list) {
          SECTION(named_mesh.name())
          {
            auto mesh_1 = named_mesh.mesh()->get<Mesh<3>>();

            std::shared_ptr mesh_2 =
              std::make_shared<const Mesh<Dimension>>(mesh_1->shared_connectivity(), mesh_1->xr());

            DiscreteFunctionP0<double> f1{mesh_1};
            DiscreteFunctionP0<double> f2{mesh_2};

            REQUIRE_THROWS_AS(f1 = f2, AssertError);
            REQUIRE_THROWS_AS(copy_to(f1, f2), AssertError);
            REQUIRE_THROWS_AS(f1 + f2, AssertError);
            REQUIRE_THROWS_AS(f1 - f2, AssertError);
            REQUIRE_THROWS_AS(f1 * f2, AssertError);
            REQUIRE_THROWS_AS(f1 / f2, AssertError);
          }
        }
      }
    }
  }
#endif   // NDEBUG
}

#ifdef __clang__
#pragma clang optimize on
#endif   // __clang__
