#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTNodeUnaryOperatorExpressionBuilder.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#define CHECK_AST(data, expected_output)                                                            \
  {                                                                                                 \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);                  \
    static_assert(std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view> or      \
                  std::is_same_v<std::decay_t<decltype(expected_output)>, std::string>);            \
                                                                                                    \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                      \
    auto ast = ASTBuilder::build(input);                                                            \
                                                                                                    \
    ASTModulesImporter{*ast};                                                                       \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                         \
                                                                                                    \
    ASTSymbolTableBuilder{*ast};                                                                    \
    ASTNodeDataTypeBuilder{*ast};                                                                   \
                                                                                                    \
    ASTNodeDeclarationToAffectationConverter{*ast};                                                 \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                            \
                                                                                                    \
    ASTNodeExpressionBuilder{*ast};                                                                 \
                                                                                                    \
    std::stringstream ast_output;                                                                   \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::exec_type}}; \
                                                                                                    \
    REQUIRE(ast_output.str() == expected_output);                                                   \
    ast->m_symbol_table->clearValues();                                                             \
  }

#define CHECK_AST_THROWS_WITH(data, error_message)                    \
  {                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};        \
    auto ast = ASTBuilder::build(input);                              \
                                                                      \
    ASTModulesImporter{*ast};                                         \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};           \
                                                                      \
    ASTSymbolTableBuilder{*ast};                                      \
                                                                      \
    REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error_message); \
    ast->m_symbol_table->clearValues();                               \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeUnaryOperatorExpressionBuilder", "[language]")
{
  SECTION("unary minus")
  {
    SECTION("B")
    {
      std::string_view data = R"(
let b:B;
-b;
-true;
-false;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, bool>)
 |   `-(language::name:b:NameProcessor)
 +-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, bool>)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, bool>)
     `-(language::false_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("N")
    {
      std::string_view data = R"(
let n:N;
-n;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, unsigned long>)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Z")
    {
      std::string_view data = R"(
let i:Z;
-i;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
     `-(language::name:i:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R")
    {
      std::string_view data = R"(
let x:R;
-x;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, double, double>)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1")
    {
      std::string_view data = R"(
let x:R^1;
-x;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, TinyVector<1ul, double>, TinyVector<1ul, double> >)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2")
    {
      std::string_view data = R"(
let x:R^2;
-x;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, TinyVector<2ul, double>, TinyVector<2ul, double> >)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3")
    {
      std::string_view data = R"(
let x:R^3;
-x;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, TinyVector<3ul, double>, TinyVector<3ul, double> >)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("not")
  {
    SECTION("B")
    {
      std::string_view data = R"(
let b:B;
not b;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::unary_not:UnaryExpressionProcessor<language::unary_not, bool, bool>)
     `-(language::name:b:NameProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("Errors")
  {
    SECTION("Invalid value type for unary minus")
    {
      auto ast = std::make_unique<ASTNode>();
      ast->set_type<language::unary_minus>();
      ast->children.emplace_back(std::make_unique<ASTNode>());

      REQUIRE_THROWS_WITH(ASTNodeUnaryOperatorExpressionBuilder{*ast}, "undefined unary operator type: - undefined");
    }

    SECTION("errors")
    {
      auto error_message = [](std::string type_name) {
        return std::string{R"(undefined unary operator
note: unexpected operand type )"} +
               type_name;
      };

      CHECK_AST_THROWS_WITH(R"(let n:N; not n;)", error_message("N"));
      CHECK_AST_THROWS_WITH(R"(not 2;)", error_message("Z"));
      CHECK_AST_THROWS_WITH(R"(not -2.3;)", error_message("R"));
      CHECK_AST_THROWS_WITH(R"(not "foo";)", error_message("string"));
    }

    SECTION("Invalid value type for unary not")
    {
      auto ast = std::make_unique<ASTNode>();
      ast->set_type<language::unary_not>();
      ast->children.emplace_back(std::make_unique<ASTNode>());

      REQUIRE_THROWS_WITH(ASTNodeUnaryOperatorExpressionBuilder{*ast}, "undefined unary operator type: not undefined");
    }
  }
}
