#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeAffectationExpressionBuilder.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_AFFECTATION_RESULT(data, variable_name, expected_value)         \
  {                                                                           \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                \
    auto ast = ASTBuilder::build(input);                                      \
                                                                              \
    ASTModulesImporter{*ast};                                                 \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                   \
                                                                              \
    ASTSymbolTableBuilder{*ast};                                              \
    ASTNodeDataTypeBuilder{*ast};                                             \
                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                      \
                                                                              \
    ASTNodeExpressionBuilder{*ast};                                           \
    ExecutionPolicy exec_policy;                                              \
    ASTExecutionStack::create();                                              \
    ast->execute(exec_policy);                                                \
    ASTExecutionStack::destroy();                                             \
                                                                              \
    auto symbol_table = ast->m_symbol_table;                                  \
                                                                              \
    using namespace TAO_PEGTL_NAMESPACE;                                      \
    position use_position{10000, 1000, 10, "fixture"};                        \
    use_position.byte    = 10000;                                             \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);   \
                                                                              \
    auto attributes = symbol->attributes();                                   \
    auto value      = std::get<decltype(expected_value)>(attributes.value()); \
                                                                              \
    REQUIRE(value == expected_value);                                         \
    ast->m_symbol_table->clearValues();                                       \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTAffectationToStringProcessor", "[language]")
{
  auto stringify = [](auto&& value) {
    std::ostringstream os;
    os << std::boolalpha << value;
    return os.str();
  };

  SECTION("Affectations")
  {
    CHECK_AFFECTATION_RESULT(R"(let s : string; s = "foo";)", "s", std::string("foo"));
    CHECK_AFFECTATION_RESULT(R"(let n : N, n = 2; let s : string; s = n;)", "s", stringify(2ul));
    CHECK_AFFECTATION_RESULT(R"(let s : string; s = -1;)", "s", stringify(-1l));
    CHECK_AFFECTATION_RESULT(R"(let s : string; s = true;)", "s", stringify(true));
    CHECK_AFFECTATION_RESULT(R"(let s : string; s = 2.3;)", "s", stringify(2.3));
    CHECK_AFFECTATION_RESULT(R"(let x : R^1, x = 13; let s : string; s = x;)", "s", stringify(TinyVector<1>{13}));
    CHECK_AFFECTATION_RESULT(R"(let x : R^2, x = [2,3]; let s : string; s = x;)", "s", stringify(TinyVector<2>{2, 3}));
    CHECK_AFFECTATION_RESULT(R"(let x : R^3, x = [1,2,3]; let s : string; s = x;)", "s",
                             stringify(TinyVector<3>{1, 2, 3}));
  }

  SECTION("+=")
  {
    CHECK_AFFECTATION_RESULT(R"(let s : string, s = "foo"; s += "bar";)", "s", std::string("foobar"));
    CHECK_AFFECTATION_RESULT(R"(let n : N, n = 2; let s : string, s = "foo"; s += n;)", "s",
                             (std::string("foo") + stringify(2ul)));
    CHECK_AFFECTATION_RESULT(R"(let s : string, s = "foo"; s += -1;)", "s", (std::string("foo") + stringify(-1l)));
    CHECK_AFFECTATION_RESULT(R"(let s : string, s = "foo"; s += true;)", "s", (std::string("foo") + stringify(true)));
    CHECK_AFFECTATION_RESULT(R"(let s : string, s = "foo"; s += 2.3;)", "s", (std::string("foo") + stringify(2.3)));
    CHECK_AFFECTATION_RESULT(R"(let x : R^1, x = 13; let s : string, s="foo"; s += x;)", "s",
                             (std::string("foo") + stringify(TinyVector<1>{13})));
    CHECK_AFFECTATION_RESULT(R"(let x : R^2, x = [2,3]; let s : string, s="foo"; s += x;)", "s",
                             (std::string("foo") + stringify(TinyVector<2>{2, 3})));
    CHECK_AFFECTATION_RESULT(R"(let x : R^3, x = [1,2,3]; let s : string, s="foo"; s += x;)", "s",
                             (std::string("foo") + stringify(TinyVector<3>{1, 2, 3})));
  }
}
