#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_predicate.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/modules/MathModule.hpp>
#include <language/node_processor/BuiltinFunctionProcessor.hpp>

#include <test_BuiltinFunctionRegister.hpp>

#define CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, variable_name, expected_value)                                 \
  {                                                                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                                        \
    auto ast = ASTBuilder::build(input);                                                                              \
                                                                                                                      \
    test_only::test_BuiltinFunctionRegister{*ast};                                                                    \
                                                                                                                      \
    ASTModulesImporter{*ast};                                                                                         \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                                           \
                                                                                                                      \
    ASTSymbolTableBuilder{*ast};                                                                                      \
    ASTNodeDataTypeBuilder{*ast};                                                                                     \
                                                                                                                      \
    ASTNodeDeclarationToAffectationConverter{*ast};                                                                   \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                                              \
    ASTNodeTypeCleaner<language::fct_declaration>{*ast};                                                              \
                                                                                                                      \
    ASTNodeExpressionBuilder{*ast};                                                                                   \
    ExecutionPolicy exec_policy;                                                                                      \
    ASTExecutionStack::create();                                                                                      \
    ast->execute(exec_policy);                                                                                        \
    ASTExecutionStack::destroy();                                                                                     \
                                                                                                                      \
    auto symbol_table = ast->m_symbol_table;                                                                          \
                                                                                                                      \
    using namespace TAO_PEGTL_NAMESPACE;                                                                              \
    position use_position{10000, 1000, 10, "fixture"};                                                                \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);                                           \
                                                                                                                      \
    REQUIRE_THAT(found, Catch::Matchers::Predicate<bool>([](bool is_found) -> bool { return is_found; },              \
                                                         std::string{"Cannot find symbol '"} + variable_name + "'")); \
                                                                                                                      \
    auto attributes = symbol->attributes();                                                                           \
    auto value      = std::get<decltype(expected_value)>(attributes.value());                                         \
                                                                                                                      \
    REQUIRE(value == expected_value);                                                                                 \
    ast->m_symbol_table->clearValues();                                                                               \
  }

#define CHECK_AST_THROWS_WITH(data, expected_error)                                             \
  {                                                                                             \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);              \
    static_assert((std::is_same_v<std::decay_t<decltype(expected_error)>, std::string_view>) or \
                  (std::is_same_v<std::decay_t<decltype(expected_error)>, std::string>));       \
                                                                                                \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                  \
    auto ast = ASTBuilder::build(input);                                                        \
                                                                                                \
    test_only::test_BuiltinFunctionRegister{*ast};                                              \
                                                                                                \
    ASTSymbolTableBuilder{*ast};                                                                \
    ASTNodeDataTypeBuilder{*ast};                                                               \
                                                                                                \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                        \
    ASTNodeTypeCleaner<language::fct_declaration>{*ast};                                        \
                                                                                                \
    ASTNodeExpressionBuilder{*ast};                                                             \
    ExecutionPolicy exec_policy;                                                                \
    using namespace Catch::Matchers;                                                            \
    ASTExecutionStack::create();                                                                \
    REQUIRE_THROWS_WITH(ast->execute(exec_policy), expected_error);                             \
    ASTExecutionStack::destroy();                                                               \
    ast->m_symbol_table->clearValues();                                                         \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("BuiltinFunctionProcessor", "[language]")
{
  SECTION("math module functions")
  {
    // @note HERE we do not use SECTION to be able to count tests and to check
    // that all math functions are actually tested

    std::set<std::string> tested_function_set;
    {   // sqrt
      tested_function_set.insert("sqrt:R");
      std::string_view data = R"(
import math;
let x:R, x = sqrt(4);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::sqrt(4l)});
    }

    {   // abs
      tested_function_set.insert("abs:Z");
      std::string_view data = R"(
import math;
let z:Z, z = abs(-3);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "z", int64_t{std::abs(-3)});
    }

    {   // abs
      tested_function_set.insert("abs:R");
      std::string_view data = R"(
import math;
let x:R, x = abs(-3.4);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::abs(-3.4)});
    }

    {   // sin
      tested_function_set.insert("sin:R");
      std::string_view data = R"(
import math;
let x:R, x = sin(1.3);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::sin(1.3)});
    }

    {   // cos
      tested_function_set.insert("cos:R");
      std::string_view data = R"(
import math;
let x:R, x = cos(1.3);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::cos(1.3)});
    }

    {   // tan
      tested_function_set.insert("tan:R");
      std::string_view data = R"(
import math;
let x:R, x = tan(1.3);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::tan(1.3)});
    }

    {   // asin
      tested_function_set.insert("asin:R");
      std::string_view data = R"(
import math;
let x:R, x = asin(0.7);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::asin(0.7)});
    }

    {   // acos
      tested_function_set.insert("acos:R");
      std::string_view data = R"(
import math;
let x:R, x = acos(0.7);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::acos(0.7)});
    }

    {   // atan
      tested_function_set.insert("atan:R");
      std::string_view data = R"(
import math;
let x:R, x = atan(0.7);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::atan(0.7)});
    }

    {   // atan2
      tested_function_set.insert("atan2:R*R");
      std::string_view data = R"(
import math;
let x:R, x = atan2(0.7, 0.4);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::atan2(0.7, 0.4)});
    }

    {   // sinh
      tested_function_set.insert("sinh:R");
      std::string_view data = R"(
import math;
let x:R, x = sinh(0.6);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::sinh(0.6)});
    }

    {   // cosh
      tested_function_set.insert("cosh:R");
      std::string_view data = R"(
import math;
let x:R, x = cosh(1.7);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::cosh(1.7)});
    }

    {   // tanh
      tested_function_set.insert("tanh:R");
      std::string_view data = R"(
import math;
let x:R, x = tanh(0.6);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::tanh(0.6)});
    }

    {   // asinh
      tested_function_set.insert("asinh:R");
      std::string_view data = R"(
import math;
let x:R, x = asinh(0.6);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::asinh(0.6)});
    }

    {   // acosh
      tested_function_set.insert("acosh:R");
      std::string_view data = R"(
import math;
let x:R, x = acosh(1.7);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::acosh(1.7)});
    }

    {   // tanh
      tested_function_set.insert("atanh:R");
      std::string_view data = R"(
import math;
let x:R, x = atanh(0.6);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::atanh(0.6)});
    }

    {   // exp
      tested_function_set.insert("exp:R");
      std::string_view data = R"(
import math;
let x:R, x = exp(1.7);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::exp(1.7)});
    }

    {   // log
      tested_function_set.insert("log:R");
      std::string_view data = R"(
import math;
let x:R, x = log(1.6);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::log(1.6)});
    }

    {   // pow
      tested_function_set.insert("pow:R*R");
      std::string_view data = R"(
import math;
let x:R, x = pow(1.6, 2.3);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{std::pow(1.6, 2.3)});
    }

    {   // ceil
      tested_function_set.insert("ceil:R");
      std::string_view data = R"(
import math;
let z:Z, z = ceil(-1.2);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "z", int64_t{-1});
    }

    {   // floor
      tested_function_set.insert("floor:R");
      std::string_view data = R"(
import math;
let z:Z, z = floor(-1.2);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "z", int64_t{-2});
    }

    {   // trunc
      tested_function_set.insert("trunc:R");
      std::string_view data = R"(
import math;
let z:Z, z = trunc(-0.2) + trunc(0.7);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "z", int64_t{0});
    }

    {   // round
      tested_function_set.insert("round:R");
      std::string_view data = R"(
import math;
let z:Z, z = round(-1.2);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "z", int64_t{-1});
    }

    {   // min
      tested_function_set.insert("min:R*R");
      std::string_view data = R"(
import math;
let x:R, x = min(-2,2.3);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{-2});
    }

    {   // min
      tested_function_set.insert("min:Z*Z");
      std::string_view data = R"(
import math;
let z:Z, z = min(-1,2);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "z", int64_t{-1});
    }

    {   // max
      tested_function_set.insert("max:R*R");
      std::string_view data = R"(
import math;
let x:R, x = max(-1,2.3);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", double{2.3});
    }

    {   // max
      tested_function_set.insert("max:Z*Z");
      std::string_view data = R"(
import math;
let z:Z, z = max(-1,2);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "z", int64_t{2});
    }

    {   // dot
      tested_function_set.insert("dot:R^1*R^1");
      std::string_view data = R"(
import math;
let x:R^1, x = -2;
let y:R^1, y = 4;
let s:R, s = dot(x,y);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "s", double{-2 * 4});
    }

    {   // dot
      tested_function_set.insert("dot:R^2*R^2");
      std::string_view data = R"(
import math;
let x:R^2, x = [-2, 3];
let y:R^2, y = [4, 3];
let s:R, s = dot(x,y);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "s", dot(TinyVector<2>{-2, 3}, TinyVector<2>{4, 3}));
    }

    {   // dot
      tested_function_set.insert("dot:R^3*R^3");
      std::string_view data = R"(
import math;
let x:R^3, x = [-2, 3, 4];
let y:R^3, y = [4, 3, 5];
let s:R, s = dot(x,y);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "s", dot(TinyVector<3>{-2, 3, 4}, TinyVector<3>{4, 3, 5}));
    }

    {   // double-dot
      tested_function_set.insert("doubleDot:R^1x1*R^1x1");
      std::string_view data = R"(
import math;
let A1:R^1x1, A1 = [[-2]];
let A2:R^1x1, A2 = [[4]];
let s:R, s = doubleDot(A1,A2);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "s", double{-2 * 4});
    }

    {   // double-dot
      tested_function_set.insert("doubleDot:R^2x2*R^2x2");
      std::string_view data = R"(
import math;
let A1:R^2x2, A1 = [[-2, 3],[5,-2]];
let A2:R^2x2, A2 = [[4, 3],[7,3]];
let s:R, s = doubleDot(A1,A2);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "s",
                                               doubleDot(TinyMatrix<2>{-2, 3, 5, -2}, TinyMatrix<2>{4, 3, 7, 3}));
    }

    {   // double-dot
      tested_function_set.insert("doubleDot:R^3x3*R^3x3");
      std::string_view data = R"(
import math;
let A1:R^3x3, A1 = [[-2, 3, 4],[1,2,3],[6,3,2]];
let A2:R^3x3, A2 = [[4, 3, 5],[2, 3, 1],[2, 6, 1]];
let s:R, s = doubleDot(A1,A2);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "s",
                                               doubleDot(TinyMatrix<3>{-2, 3, 4, 1, 2, 3, 6, 3, 2},
                                                         TinyMatrix<3>{4, 3, 5, 2, 3, 1, 2, 6, 1}));
    }

    {   // tensor product
      tested_function_set.insert("tensorProduct:R^1*R^1");
      std::string_view data = R"(
import math;
let x:R^1, x = -2;
let y:R^1, y = 4;
let s:R^1x1, s = tensorProduct(x,y);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "s", TinyMatrix<1>{-2 * 4});
    }

    {   // tensor product
      tested_function_set.insert("tensorProduct:R^2*R^2");
      std::string_view data = R"(
import math;
let x:R^2, x = [-2, 3];
let y:R^2, y = [4, 3];
let s:R^2x2, s = tensorProduct(x,y);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "s", tensorProduct(TinyVector<2>{-2, 3}, TinyVector<2>{4, 3}));
    }

    {   // tensor product
      tested_function_set.insert("tensorProduct:R^3*R^3");
      std::string_view data = R"(
import math;
let x:R^3, x = [-2, 3, 4];
let y:R^3, y = [4, 3, 5];
let s:R^3x3, s = tensorProduct(x,y);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "s",
                                               tensorProduct(TinyVector<3>{-2, 3, 4}, TinyVector<3>{4, 3, 5}));
    }

    {   // det
      tested_function_set.insert("det:R^1x1");
      std::string_view data = R"(
import math;
let A:R^1x1, A = -2;
let d:R, d = det(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "d", det(TinyMatrix<1>{-2}));
    }

    {   // det
      tested_function_set.insert("det:R^2x2");
      std::string_view data = R"(
import math;
let A:R^2x2, A = [[-2.5, 3.2],
                  [ 3.2, 2.3]];
let d:R, d = det(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "d",
                                               det(TinyMatrix<2>{-2.5, 3.2,   //
                                                                 +3.2, 2.3}));
    }

    {   // det
      tested_function_set.insert("det:R^3x3");
      std::string_view data = R"(
import math;
let A:R^3x3, A = [[-2, 2,-1],
                  [ 3, 2, 2],
                  [-2, 5,-3]];
let d:R, d = det(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "d",
                                               det(TinyMatrix<3>{-2, 2, -1,   //
                                                                 +3, 2, +2,   //
                                                                 -2, 5, -3}));
    }

    {   // trace
      tested_function_set.insert("trace:R^1x1");
      std::string_view data = R"(
import math;
let A:R^1x1, A = -2;
let t:R, t = trace(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "t", trace(TinyMatrix<1>{-2}));
    }

    {   // trace
      tested_function_set.insert("trace:R^2x2");
      std::string_view data = R"(
import math;
let A:R^2x2, A = [[-2.5, 3.2],
                  [ 3.2, 2.3]];
let t:R, t = trace(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "t",
                                               trace(TinyMatrix<2>{-2.5, 3.2,   //
                                                                   +3.2, 2.3}));
    }

    {   // trace
      tested_function_set.insert("trace:R^3x3");
      std::string_view data = R"(
import math;
let A:R^3x3, A = [[-2.5, 2.9,-1.3],
                  [ 3.2, 2.3, 2.7],
                  [-2.6, 5.2,-3.5]];
let t:R, t = trace(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "t",
                                               trace(TinyMatrix<3>{-2.5, 2.9, -1.3,   //
                                                                   +3.2, 2.3, +2.7,   //
                                                                   -2.6, 5.2, -3.5}));
    }

    {   // inverse
      tested_function_set.insert("inverse:R^1x1");
      std::string_view data = R"(
import math;
let A:R^1x1, A = -2;
let invA:R^1x1, invA = inverse(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "invA", inverse(TinyMatrix<1>{-2}));
    }

    {   // inverse
      tested_function_set.insert("inverse:R^2x2");
      std::string_view data = R"(
import math;
let A:R^2x2, A = [[-2.5, 3.2],
                  [ 3.2, 2.3]];
let invA:R^2x2, invA = inverse(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "invA",
                                               inverse(TinyMatrix<2>{-2.5, 3.2,   //
                                                                     +3.2, 2.3}));
    }

    {   // inverse
      tested_function_set.insert("inverse:R^3x3");
      std::string_view data = R"(
import math;
let A:R^3x3, A = [[-2, 2,-1],
                  [ 3, 2, 2],
                  [-2, 5,-3]];
let invA:R^3x3, invA = inverse(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "invA",
                                               inverse(TinyMatrix<3>{-2, 2, -1,   //
                                                                     +3, 2, +2,   //
                                                                     -2, 5, -3}));
    }

    {   // transpose
      tested_function_set.insert("transpose:R^1x1");
      std::string_view data = R"(
import math;
let A:R^1x1, A = -2;
let tA:R^1x1, tA = transpose(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "tA", transpose(TinyMatrix<1>{-2}));
    }

    {   // transpose
      tested_function_set.insert("transpose:R^2x2");
      std::string_view data = R"(
import math;
let A:R^2x2, A = [[-2.5, 3.2],
                  [ 3.2, 2.3]];
let tA:R^2x2, tA = transpose(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "tA",
                                               transpose(TinyMatrix<2>{-2.5, 3.2,   //
                                                                       +3.2, 2.3}));
    }

    {   // transpose
      tested_function_set.insert("transpose:R^3x3");
      std::string_view data = R"(
import math;
let A:R^3x3, A = [[-2.5, 2.9,-1.3],
                  [ 3.2, 2.3, 2.7],
                  [-2.6, 5.2,-3.5]];
let tA:R^3x3, tA = transpose(A);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "tA",
                                               transpose(TinyMatrix<3>{-2.5, 2.9, -1.3,   //
                                                                       +3.2, 2.3, +2.7,   //
                                                                       -2.6, 5.2, -3.5}));
    }

    MathModule math_module;

    bool missing_test = false;
    for (const auto& [function_name, builtin_function] : math_module.getNameBuiltinFunctionMap()) {
      if (tested_function_set.find(function_name) == tested_function_set.end()) {
        UNSCOPED_INFO("function '" << function_name << "' is NOT tested");
        missing_test = true;
      }
    }
    REQUIRE_FALSE(missing_test);

    SECTION("catch runtime error")
    {
      std::string_view data = R"(
runtimeError();
)";

      std::string error = "error: runtime error";

      CHECK_AST_THROWS_WITH(data, error);
    }

    SECTION("negative Z to N conversion")
    {
      std::string_view data = R"(
NtoR(3);
NtoR(-4);
)";

      CHECK_AST_THROWS_WITH(data, std::string{"trying to convert negative value (-4)"});
    }

    SECTION("negative Z*Z to N*N conversion")
    {
      std::string_view data = R"(
NNtoN(3,4);
NNtoN(6,-7);
)";

      CHECK_AST_THROWS_WITH(data, std::string{"trying to convert negative value (-7)"});
    }

    SECTION("negative Z in list  to (N) conversion")
    {
      std::string_view data = R"(
tuple_NtoR(2);
tuple_NtoR(-1);
)";

      CHECK_AST_THROWS_WITH(data, std::string{"trying to convert negative value (-1)"});
    }

    SECTION("negative Z in list  to (N) conversion")
    {
      std::string_view data = R"(
tuple_NtoR((3, 2, -3, 2));
)";

      CHECK_AST_THROWS_WITH(data, std::string{"trying to convert negative value (-3)"});
    }
  }

  SECTION("int list -> tuple args evalation")
  {
    {
      std::string_view data = R"(
let x:R, x = tuple_ZtoR((1,2,3,-4));
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", 0.5 * (1 + 2 + 3 - 4));
    }

    {
      std::string_view data = R"(
let (X,x):(R)*R, (X,x) = R22ToTupleRxR([[1,1], [2,3]]);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "X", (std::vector<double>{1, 1, 2, 3}));
    }
  }

  SECTION("simple N to tuple args evalation")
  {
    {
      std::string_view data = R"(
let x:R, x = tuple_ZtoR(3);
)";
      CHECK_BUILTIN_FUNCTION_EVALUATION_RESULT(data, "x", 0.5 * 3);
    }
  }

  SECTION("expression type")
  {
    ASTNode node;
    REQUIRE(BuiltinFunctionExpressionProcessor{nullptr}.type() ==
            INodeProcessor::Type::builtin_function_expression_processor);
    REQUIRE(BuiltinFunctionProcessor{node}.type() == INodeProcessor::Type::builtin_function_processor);
  }
}
