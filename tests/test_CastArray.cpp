#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/CastArray.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("CastArray", "[utils]")
{
  SECTION("explicit cast Array -> CastArray")
  {
    Array<double> x_double{5};
    x_double[0] = 1;
    x_double[1] = 2;
    x_double[2] = 3;
    x_double[3] = 4;
    x_double[4] = 5;

    CastArray<double, char> x_char{x_double};

    REQUIRE(x_char.size() * sizeof(char) == x_double.size() * sizeof(double));

    Array<char> y_char{x_char.size()};
    for (size_t i = 0; i < x_char.size(); ++i) {
      y_char[i] = x_char[i];
    }

    CastArray<char, double> y_double{y_char};
    REQUIRE(y_char.size() * sizeof(char) == y_double.size() * sizeof(double));

    REQUIRE(&(y_double[0]) != &(x_double[0]));

    for (size_t i = 0; i < y_double.size(); ++i) {
      REQUIRE(y_double[i] == x_double[i]);
    }
  }

  SECTION("explicit cast value -> CastArray")
  {
    double x = 3;

    CastArray<double, char> x_char(x);

    REQUIRE(x_char.size() * sizeof(char) == sizeof(double));
  }

  SECTION("invalid cast array")
  {
    Array<char> x_char{13};

    REQUIRE_THROWS_WITH((CastArray<char, double>{x_char}),
                        "unexpected error: cannot cast array to the chosen data type");
  }

  SECTION("cast array utilities")
  {
    SECTION("Array -> CastArray")
    {
      Array<double> x_double{5};
      x_double[0] = 1.3;
      x_double[1] = 3.2;
      x_double[2] = -4;
      x_double[3] = 6.2;
      x_double[4] = -1.6;

      CastArray<double, short> x_short{x_double};
      auto x_short_from = cast_array_to<short>::from(x_double);

      REQUIRE(x_short_from.size() == x_short.size());
      for (size_t i = 0; i < x_short_from.size(); ++i) {
        REQUIRE(x_short_from[i] == x_short[i]);
      }
    }

    SECTION("Value -> CastArray")
    {
      double x = 3.14;

      CastArray<double, short> x_short{x};
      auto x_short_from = cast_value_to<short>::from(x);

      REQUIRE(x_short_from.size() == x_short.size());
      for (size_t i = 0; i < x_short_from.size(); ++i) {
        REQUIRE(x_short_from[i] == x_short[i]);
      }
    }
  }
}
