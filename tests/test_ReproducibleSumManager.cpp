#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/ReproducibleSumManager.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ReproducibleSumManager", "[utils]")
{
  SECTION("control settings")
  {
    const bool init_value = ReproducibleSumManager::reproducibleSums();

    ReproducibleSumManager::setReproducibleSums(not init_value);

    REQUIRE(init_value == not ReproducibleSumManager::reproducibleSums());

    ReproducibleSumManager::setReproducibleSums(init_value);

    REQUIRE(init_value == ReproducibleSumManager::reproducibleSums());
  }
}
