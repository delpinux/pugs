#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <FixturesForBuiltinT.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeBinaryOperatorExpressionBuilder.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/BinaryOperatorProcessorBuilder.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <language/utils/TypeDescriptor.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#define CHECK_AST(data, expected_output)                                                            \
  {                                                                                                 \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);                  \
    static_assert(std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view> or      \
                  std::is_same_v<std::decay_t<decltype(expected_output)>, std::string>);            \
                                                                                                    \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                      \
    auto ast = ASTBuilder::build(input);                                                            \
                                                                                                    \
    ASTModulesImporter{*ast};                                                                       \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                         \
                                                                                                    \
    ASTSymbolTableBuilder{*ast};                                                                    \
    ASTNodeDataTypeBuilder{*ast};                                                                   \
                                                                                                    \
    ASTNodeDeclarationToAffectationConverter{*ast};                                                 \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                            \
                                                                                                    \
    ASTNodeExpressionBuilder{*ast};                                                                 \
                                                                                                    \
    std::stringstream ast_output;                                                                   \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::exec_type}}; \
                                                                                                    \
    REQUIRE(ast_output.str() == expected_output);                                                   \
    ast->m_symbol_table->clearValues();                                                             \
  }

#define REQUIRE_AST_THROWS_WITH(data, expected_output)                             \
  {                                                                                \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>); \
                                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                     \
    auto ast = ASTBuilder::build(input);                                           \
                                                                                   \
    ASTSymbolTableBuilder{*ast};                                                   \
    REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, expected_output);            \
    ast->m_symbol_table->clearValues();                                            \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeBinaryOperatorExpressionBuilder", "[language]")
{
  SECTION("multiply")
  {
    SECTION("B")
    {
      std::string_view data = R"(
let b : B;
b*true;
false*b*true;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, unsigned long, bool, bool>)
 |   +-(language::name:b:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, unsigned long, unsigned long, bool>)
     +-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, unsigned long, bool, bool>)
     |   +-(language::false_kw:ValueProcessor)
     |   `-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("N")
    {
      std::string_view data = R"(
let n : N;
let m : N;
n*m*n;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, unsigned long, unsigned long, unsigned long>)
     +-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, unsigned long, unsigned long, unsigned long>)
     |   +-(language::name:n:NameProcessor)
     |   `-(language::name:m:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Z")
    {
      std::string_view data = R"(
let a : Z;
a*3*a;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, long, long, long>)
     +-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, long, long, long>)
     |   +-(language::name:a:NameProcessor)
     |   `-(language::integer:3:ValueProcessor)
     `-(language::name:a:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R")
    {
      std::string_view data = R"(
2.3*1.2*2*false;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, double, double, bool>)
     +-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, double, double, long>)
     |   +-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, double, double, double>)
     |   |   +-(language::real:2.3:ValueProcessor)
     |   |   `-(language::real:1.2:ValueProcessor)
     |   `-(language::integer:2:ValueProcessor)
     `-(language::false_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1")
    {
      std::string_view data = R"(
let x : R^1, x = 3.7;
2*x;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, double>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::real:3.7:ValueProcessor)
 `-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, TinyVector<1ul, double>, long, TinyVector<1ul, double> >)
     +-(language::integer:2:ValueProcessor)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2")
    {
      std::string_view data = R"(
let x : R^2, x = [3.2,6];
2*x;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       +-(language::real:3.2:ValueProcessor)
 |       `-(language::integer:6:ValueProcessor)
 `-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, TinyVector<2ul, double>, long, TinyVector<2ul, double> >)
     +-(language::integer:2:ValueProcessor)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3")
    {
      std::string_view data = R"(
let x : R^3, x = [3.2,6,1.2];
2*x;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
 |       +-(language::real:3.2:ValueProcessor)
 |       +-(language::integer:6:ValueProcessor)
 |       `-(language::real:1.2:ValueProcessor)
 `-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, TinyVector<3ul, double>, long, TinyVector<3ul, double> >)
     +-(language::integer:2:ValueProcessor)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("divide")
  {
    SECTION("N")
    {
      std::string_view data = R"(
let n : N;
let m : N;
n/m/n;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::divide_op:BinaryExpressionProcessor<language::divide_op, unsigned long, unsigned long, unsigned long>)
     +-(language::divide_op:BinaryExpressionProcessor<language::divide_op, unsigned long, unsigned long, unsigned long>)
     |   +-(language::name:n:NameProcessor)
     |   `-(language::name:m:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Z")
    {
      std::string_view data = R"(
let a : Z;
a/3/a;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::divide_op:BinaryExpressionProcessor<language::divide_op, long, long, long>)
     +-(language::divide_op:BinaryExpressionProcessor<language::divide_op, long, long, long>)
     |   +-(language::name:a:NameProcessor)
     |   `-(language::integer:3:ValueProcessor)
     `-(language::name:a:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R")
    {
      std::string_view data = R"(
2.3/1.2/2;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::divide_op:BinaryExpressionProcessor<language::divide_op, double, double, long>)
     +-(language::divide_op:BinaryExpressionProcessor<language::divide_op, double, double, double>)
     |   +-(language::real:2.3:ValueProcessor)
     |   `-(language::real:1.2:ValueProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("plus")
  {
    SECTION("B")
    {
      std::string_view data = R"(
let b : B;
b+true;
false+b+true;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::plus_op:BinaryExpressionProcessor<language::plus_op, unsigned long, bool, bool>)
 |   +-(language::name:b:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::plus_op:BinaryExpressionProcessor<language::plus_op, unsigned long, unsigned long, bool>)
     +-(language::plus_op:BinaryExpressionProcessor<language::plus_op, unsigned long, bool, bool>)
     |   +-(language::false_kw:ValueProcessor)
     |   `-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("N")
    {
      std::string_view data = R"(
let n : N;
let m : N;
n+m+n;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:BinaryExpressionProcessor<language::plus_op, unsigned long, unsigned long, unsigned long>)
     +-(language::plus_op:BinaryExpressionProcessor<language::plus_op, unsigned long, unsigned long, unsigned long>)
     |   +-(language::name:n:NameProcessor)
     |   `-(language::name:m:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Z")
    {
      std::string_view data = R"(
let a : Z;
a+3+a;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:BinaryExpressionProcessor<language::plus_op, long, long, long>)
     +-(language::plus_op:BinaryExpressionProcessor<language::plus_op, long, long, long>)
     |   +-(language::name:a:NameProcessor)
     |   `-(language::integer:3:ValueProcessor)
     `-(language::name:a:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R")
    {
      std::string_view data = R"(
2.3+1.2+2+false;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:BinaryExpressionProcessor<language::plus_op, double, double, bool>)
     +-(language::plus_op:BinaryExpressionProcessor<language::plus_op, double, double, long>)
     |   +-(language::plus_op:BinaryExpressionProcessor<language::plus_op, double, double, double>)
     |   |   +-(language::real:2.3:ValueProcessor)
     |   |   `-(language::real:1.2:ValueProcessor)
     |   `-(language::integer:2:ValueProcessor)
     `-(language::false_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1 with variables")
    {
      std::string_view data = R"(
let x : R^1, x = 1;
let y : R^1, y = 2;
x+y;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, long>)
 |   +-(language::name:y:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::plus_op:BinaryExpressionProcessor<language::plus_op, TinyVector<1ul, double>, TinyVector<1ul, double>, TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1 raw")
    {
      std::string_view data = R"(
[1]+[2];
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:BinaryExpressionProcessor<language::plus_op, TinyVector<1ul, double>, TinyVector<1ul, double>, TinyVector<1ul, double> >)
     +-(language::vector_expression:TinyVectorExpressionProcessor<1ul>)
     |   `-(language::integer:1:ValueProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<1ul>)
         `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2 with variables")
    {
      std::string_view data = R"(
let x : R^2, x = [1,2];
let y : R^2, y = [0.3,0.7];
x+y;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       +-(language::integer:1:ValueProcessor)
 |       `-(language::integer:2:ValueProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
 |   +-(language::name:y:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       +-(language::real:0.3:ValueProcessor)
 |       `-(language::real:0.7:ValueProcessor)
 `-(language::plus_op:BinaryExpressionProcessor<language::plus_op, TinyVector<2ul, double>, TinyVector<2ul, double>, TinyVector<2ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2 raw")
    {
      std::string_view data = R"(
[1,2]+[0.3,0.7];
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:BinaryExpressionProcessor<language::plus_op, TinyVector<2ul, double>, TinyVector<2ul, double>, TinyVector<2ul, double> >)
     +-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
     |   +-(language::integer:1:ValueProcessor)
     |   `-(language::integer:2:ValueProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
         +-(language::real:0.3:ValueProcessor)
         `-(language::real:0.7:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3 with variables")
    {
      std::string_view data = R"(
let x : R^3, x = [1,2,3];
let y : R^3, y = [4,3,2];
x+y;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
 |       +-(language::integer:1:ValueProcessor)
 |       +-(language::integer:2:ValueProcessor)
 |       `-(language::integer:3:ValueProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
 |   +-(language::name:y:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
 |       +-(language::integer:4:ValueProcessor)
 |       +-(language::integer:3:ValueProcessor)
 |       `-(language::integer:2:ValueProcessor)
 `-(language::plus_op:BinaryExpressionProcessor<language::plus_op, TinyVector<3ul, double>, TinyVector<3ul, double>, TinyVector<3ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3 raw")
    {
      std::string_view data = R"(
[1,2,3]+[4,3,2];
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:BinaryExpressionProcessor<language::plus_op, TinyVector<3ul, double>, TinyVector<3ul, double>, TinyVector<3ul, double> >)
     +-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
     |   +-(language::integer:1:ValueProcessor)
     |   +-(language::integer:2:ValueProcessor)
     |   `-(language::integer:3:ValueProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         +-(language::integer:4:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate B")

    {
      std::string_view data = R"(
"foo"+true;
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<)" +
                           string_name + R"(, bool>)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate N")
    {
      std::string_view data   = R"(
let n : N, n=0;
"foo"+n;
)";
      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:0:ValueProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<)" +
                           string_name + R"(, unsigned long>)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate Z")
    {
      std::string_view data = R"(
"foo"+1;
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<)" +
                           string_name + R"(, long>)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::integer:1:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate R")
    {
      std::string_view data = R"(
"foo"+1.2;
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<)" +
                           string_name + R"(, double>)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::real:1.2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate R^1")
    {
      std::string_view data = R"(
let x:R^1;
"foo"+x;
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<)" +
                           string_name + R"(, TinyVector<1ul, double> >)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate R^2")
    {
      std::string_view data = R"(
let x:R^2;
"foo"+x;
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<)" +
                           string_name + R"(, TinyVector<2ul, double> >)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate R^3")
    {
      std::string_view data = R"(
let x:R^3;
"foo"+x;
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<)" +
                           string_name + R"(, TinyVector<3ul, double> >)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate R^1x1")
    {
      std::string_view data = R"(
let x:R^1x1;
"foo"+x;
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<)" +
                           string_name + R"(, TinyMatrix<1ul, 1ul, double> >)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate R^2x2")
    {
      std::string_view data = R"(
let x:R^2x2;
"foo"+x;
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<)" +
                           string_name + R"(, TinyMatrix<2ul, 2ul, double> >)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate R^3x3")
    {
      std::string_view data = R"(
let x:R^3x3;
"foo"+x;
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<)" +
                           string_name + R"(, TinyMatrix<3ul, 3ul, double> >)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::name:x:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("B concatenate string")
    {
      std::string_view data = R"(
false+"foo";
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<bool, )" +
                           string_name + R"( >)
     +-(language::false_kw:ValueProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("N concatenate string")
    {
      std::string_view data   = R"(
let n : N, n=0;
n+"foo";
)";
      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:n:NameProcessor)
 |   `-(language::integer:0:ValueProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<unsigned long, )" +
                           string_name + R"( >)
     +-(language::name:n:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Z concatenate string")
    {
      std::string_view data = R"(
1+"foo";
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<long, )" +
                           string_name + R"( >)
     +-(language::integer:1:ValueProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R concatenate string")
    {
      std::string_view data = R"(
1.2+"foo";
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<double, )" +
                           string_name + R"( >)
     +-(language::real:1.2:ValueProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1 concatenate string ")
    {
      std::string_view data = R"(
let x:R^1;
x+"foo";
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<TinyVector<1ul, double>, )" +
                           string_name + R"( >)
     +-(language::name:x:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2 concatenate string")
    {
      std::string_view data = R"(
let x:R^2;
x+"foo";
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<TinyVector<2ul, double>, )" +
                           string_name + R"( >)
     +-(language::name:x:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3 concatenate string")
    {
      std::string_view data = R"(
let x:R^3;
x+"foo";
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<TinyVector<3ul, double>, )" +
                           string_name + R"( >)
     +-(language::name:x:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1x1 concatenate string")
    {
      std::string_view data = R"(
let x:R^1x1;
x+"foo";
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<TinyMatrix<1ul, 1ul, double>, )" +
                           string_name + R"( >)
     +-(language::name:x:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate R^2x2")
    {
      std::string_view data = R"(
let x:R^2x2;
x+"foo";
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<TinyMatrix<2ul, 2ul, double>, )" +
                           string_name + R"( >)
     +-(language::name:x:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3x3 concatenate string")
    {
      std::string_view data = R"(
let x:R^3x3;
x+"foo";
)";

      std::string string_name = demangle(typeid(std::string{}).name());
      std::string result      = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<TinyMatrix<3ul, 3ul, double>, )" +
                           string_name + R"( >)
     +-(language::name:x:NameProcessor)
     `-(language::literal:"foo":ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("string concatenate string")
    {
      std::string_view data = R"(
"foo"+"bar";
)";

      std::string string_name = demangle(typeid(std::string{}).name());

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:ConcatExpressionProcessor<)" +
                           string_name + ", " + string_name + R"( >)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::literal:"bar":ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("minus")
  {
    SECTION("B")
    {
      std::string_view data = R"(
let b : B;
b-true;
false-b-true;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::minus_op:BinaryExpressionProcessor<language::minus_op, long, bool, bool>)
 |   +-(language::name:b:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::minus_op:BinaryExpressionProcessor<language::minus_op, long, long, bool>)
     +-(language::minus_op:BinaryExpressionProcessor<language::minus_op, long, bool, bool>)
     |   +-(language::false_kw:ValueProcessor)
     |   `-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("N")
    {
      std::string_view data = R"(
let n : N;
let m : N;
n-m-n;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::minus_op:BinaryExpressionProcessor<language::minus_op, long, long, unsigned long>)
     +-(language::minus_op:BinaryExpressionProcessor<language::minus_op, long, unsigned long, unsigned long>)
     |   +-(language::name:n:NameProcessor)
     |   `-(language::name:m:NameProcessor)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Z")
    {
      std::string_view data = R"(
let a : Z;
a-3-a;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::minus_op:BinaryExpressionProcessor<language::minus_op, long, long, long>)
     +-(language::minus_op:BinaryExpressionProcessor<language::minus_op, long, long, long>)
     |   +-(language::name:a:NameProcessor)
     |   `-(language::integer:3:ValueProcessor)
     `-(language::name:a:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R")
    {
      std::string_view data = R"(
2.3-1.2-2-false;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::minus_op:BinaryExpressionProcessor<language::minus_op, double, double, bool>)
     +-(language::minus_op:BinaryExpressionProcessor<language::minus_op, double, double, long>)
     |   +-(language::minus_op:BinaryExpressionProcessor<language::minus_op, double, double, double>)
     |   |   +-(language::real:2.3:ValueProcessor)
     |   |   `-(language::real:1.2:ValueProcessor)
     |   `-(language::integer:2:ValueProcessor)
     `-(language::false_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1")
    {
      std::string_view data = R"(
let x : R^1, x = 1;
let y : R^1, y = 2;
x-y;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, long>)
 |   +-(language::name:y:NameProcessor)
 |   `-(language::integer:2:ValueProcessor)
 `-(language::minus_op:BinaryExpressionProcessor<language::minus_op, TinyVector<1ul, double>, TinyVector<1ul, double>, TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^1 raw")
    {
      std::string_view data = R"(
[1]-[2];
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::minus_op:BinaryExpressionProcessor<language::minus_op, TinyVector<1ul, double>, TinyVector<1ul, double>, TinyVector<1ul, double> >)
     +-(language::vector_expression:TinyVectorExpressionProcessor<1ul>)
     |   `-(language::integer:1:ValueProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<1ul>)
         `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2 with variables")
    {
      std::string_view data = R"(
let x : R^2, x = [1,2];
let y : R^2, y = [0.3,0.7];
x-y;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       +-(language::integer:1:ValueProcessor)
 |       `-(language::integer:2:ValueProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
 |   +-(language::name:y:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       +-(language::real:0.3:ValueProcessor)
 |       `-(language::real:0.7:ValueProcessor)
 `-(language::minus_op:BinaryExpressionProcessor<language::minus_op, TinyVector<2ul, double>, TinyVector<2ul, double>, TinyVector<2ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^2 raw")
    {
      std::string_view data = R"(
[1,2]-[0.3,0.7];
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::minus_op:BinaryExpressionProcessor<language::minus_op, TinyVector<2ul, double>, TinyVector<2ul, double>, TinyVector<2ul, double> >)
     +-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
     |   +-(language::integer:1:ValueProcessor)
     |   `-(language::integer:2:ValueProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
         +-(language::real:0.3:ValueProcessor)
         `-(language::real:0.7:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3 with variables")
    {
      std::string_view data = R"(
let x : R^3, x = [1,2,3];
let y : R^3, y = [4,3,2];
x-y;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
 |       +-(language::integer:1:ValueProcessor)
 |       +-(language::integer:2:ValueProcessor)
 |       `-(language::integer:3:ValueProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
 |   +-(language::name:y:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
 |       +-(language::integer:4:ValueProcessor)
 |       +-(language::integer:3:ValueProcessor)
 |       `-(language::integer:2:ValueProcessor)
 `-(language::minus_op:BinaryExpressionProcessor<language::minus_op, TinyVector<3ul, double>, TinyVector<3ul, double>, TinyVector<3ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::name:y:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R^3 raw")
    {
      std::string_view data = R"(
[1,2,3]-[4,3,2];
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::minus_op:BinaryExpressionProcessor<language::minus_op, TinyVector<3ul, double>, TinyVector<3ul, double>, TinyVector<3ul, double> >)
     +-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
     |   +-(language::integer:1:ValueProcessor)
     |   +-(language::integer:2:ValueProcessor)
     |   `-(language::integer:3:ValueProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         +-(language::integer:4:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("or")
  {
    SECTION("B")
    {
      std::string_view data = R"(
let b : B;
b or true;
false or b or true;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::or_op:BinaryExpressionProcessor<language::or_op, bool, bool, bool>)
 |   +-(language::name:b:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::or_op:BinaryExpressionProcessor<language::or_op, bool, bool, bool>)
     +-(language::or_op:BinaryExpressionProcessor<language::or_op, bool, bool, bool>)
     |   +-(language::false_kw:ValueProcessor)
     |   `-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("and")
  {
    SECTION("B")
    {
      std::string_view data = R"(
let b : B;
b and true;
false and b and true;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::and_op:BinaryExpressionProcessor<language::and_op, bool, bool, bool>)
 |   +-(language::name:b:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::and_op:BinaryExpressionProcessor<language::and_op, bool, bool, bool>)
     +-(language::and_op:BinaryExpressionProcessor<language::and_op, bool, bool, bool>)
     |   +-(language::false_kw:ValueProcessor)
     |   `-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("xor")
  {
    SECTION("B")
    {
      std::string_view data = R"(
let b : B;
b xor true;
false xor b xor true;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::xor_op:BinaryExpressionProcessor<language::xor_op, bool, bool, bool>)
 |   +-(language::name:b:NameProcessor)
 |   `-(language::true_kw:ValueProcessor)
 `-(language::xor_op:BinaryExpressionProcessor<language::xor_op, bool, bool, bool>)
     +-(language::xor_op:BinaryExpressionProcessor<language::xor_op, bool, bool, bool>)
     |   +-(language::false_kw:ValueProcessor)
     |   `-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("greater")
  {
    SECTION("B")
    {
      std::string_view data = R"(
let b : B;
b > true;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::greater_op:BinaryExpressionProcessor<language::greater_op, bool, bool, bool>)
     +-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("N")
    {
      std::string_view data = R"(
let n : N;
let m : N;
n > m;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::greater_op:BinaryExpressionProcessor<language::greater_op, bool, unsigned long, unsigned long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:m:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Z")
    {
      std::string_view data = R"(
let a : Z;
a > 3;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::greater_op:BinaryExpressionProcessor<language::greater_op, bool, long, long>)
     +-(language::name:a:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R")
    {
      std::string_view data = R"(
2.3 > 1.2;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::greater_op:BinaryExpressionProcessor<language::greater_op, bool, double, double>)
     +-(language::real:2.3:ValueProcessor)
     `-(language::real:1.2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("lesser")
  {
    SECTION("B")
    {
      std::string_view data = R"(
let b : B;
b < true;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::lesser_op:BinaryExpressionProcessor<language::lesser_op, bool, bool, bool>)
     +-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("N")
    {
      std::string_view data = R"(
let n : N;
let m : N;
n < m;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::lesser_op:BinaryExpressionProcessor<language::lesser_op, bool, unsigned long, unsigned long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:m:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("Z")
    {
      std::string_view data = R"(
let a : Z;
a < 3;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::lesser_op:BinaryExpressionProcessor<language::lesser_op, bool, long, long>)
     +-(language::name:a:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("R")
    {
      std::string_view data = R"(
2.3 < 1.2;
)";

      std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::lesser_op:BinaryExpressionProcessor<language::lesser_op, bool, double, double>)
     +-(language::real:2.3:ValueProcessor)
     `-(language::real:1.2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("greater or equal")
    {
      SECTION("B")
      {
        std::string_view data = R"(
let b : B;
b >= true;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::greater_or_eq_op:BinaryExpressionProcessor<language::greater_or_eq_op, bool, bool, bool>)
     +-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("N")
      {
        std::string_view data = R"(
let n : N;
let m : N;
n >= m;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::greater_or_eq_op:BinaryExpressionProcessor<language::greater_or_eq_op, bool, unsigned long, unsigned long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:m:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Z")
      {
        std::string_view data = R"(
let a : Z;
a >= 3;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::greater_or_eq_op:BinaryExpressionProcessor<language::greater_or_eq_op, bool, long, long>)
     +-(language::name:a:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R")
      {
        std::string_view data = R"(
2.3 >= 1.2;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::greater_or_eq_op:BinaryExpressionProcessor<language::greater_or_eq_op, bool, double, double>)
     +-(language::real:2.3:ValueProcessor)
     `-(language::real:1.2:ValueProcessor)
)";

        CHECK_AST(data, result);
      }
    }

    SECTION("lesser or equal")
    {
      SECTION("B")
      {
        std::string_view data = R"(
let b : B;
b <= true;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::lesser_or_eq_op:BinaryExpressionProcessor<language::lesser_or_eq_op, bool, bool, bool>)
     +-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("N")
      {
        std::string_view data = R"(
let n : N;
let m : N;
n <= m;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::lesser_or_eq_op:BinaryExpressionProcessor<language::lesser_or_eq_op, bool, unsigned long, unsigned long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:m:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Z")
      {
        std::string_view data = R"(
let a : Z;
a <= 3;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::lesser_or_eq_op:BinaryExpressionProcessor<language::lesser_or_eq_op, bool, long, long>)
     +-(language::name:a:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R")
      {
        std::string_view data = R"(
2.3 <= 1.2;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::lesser_or_eq_op:BinaryExpressionProcessor<language::lesser_or_eq_op, bool, double, double>)
     +-(language::real:2.3:ValueProcessor)
     `-(language::real:1.2:ValueProcessor)
)";

        CHECK_AST(data, result);
      }
    }

    SECTION("equal equal")
    {
      SECTION("B")
      {
        std::string_view data = R"(
let b : B;
b == true;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eqeq_op:BinaryExpressionProcessor<language::eqeq_op, bool, bool, bool>)
     +-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("N")
      {
        std::string_view data = R"(
let n : N;
let m : N;
n == m;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eqeq_op:BinaryExpressionProcessor<language::eqeq_op, bool, unsigned long, unsigned long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:m:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Z")
      {
        std::string_view data = R"(
let a : Z;
a == 3;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eqeq_op:BinaryExpressionProcessor<language::eqeq_op, bool, long, long>)
     +-(language::name:a:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R")
      {
        std::string_view data = R"(
2.3 == 1.2;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::eqeq_op:BinaryExpressionProcessor<language::eqeq_op, bool, double, double>)
     +-(language::real:2.3:ValueProcessor)
     `-(language::real:1.2:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("string == string")
      {
        std::string_view data = R"(
"foo" == "bar";
)";

        std::string string_name = demangle(typeid(std::string{}).name());

        std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eqeq_op:BinaryExpressionProcessor<language::eqeq_op, bool, )" +
                             string_name + ", " + string_name + R"( >)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::literal:"bar":ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R^1")
      {
        std::string_view data = R"(
let x : R^1, x = 1;
x==[2];
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::eqeq_op:BinaryExpressionProcessor<language::eqeq_op, bool, TinyVector<1ul, double>, TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<1ul>)
         `-(language::integer:2:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R^2")
      {
        std::string_view data = R"(
let x : R^2, x = [1,2];
x==[0.3,0.7];
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       +-(language::integer:1:ValueProcessor)
 |       `-(language::integer:2:ValueProcessor)
 `-(language::eqeq_op:BinaryExpressionProcessor<language::eqeq_op, bool, TinyVector<2ul, double>, TinyVector<2ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
         +-(language::real:0.3:ValueProcessor)
         `-(language::real:0.7:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R^3")
      {
        std::string_view data = R"(
let x : R^3, x = [1,2,3];
x==[4,3,2];
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
 |       +-(language::integer:1:ValueProcessor)
 |       +-(language::integer:2:ValueProcessor)
 |       `-(language::integer:3:ValueProcessor)
 `-(language::eqeq_op:BinaryExpressionProcessor<language::eqeq_op, bool, TinyVector<3ul, double>, TinyVector<3ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         +-(language::integer:4:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:2:ValueProcessor)
)";

        CHECK_AST(data, result);
      }
    }

    SECTION("not equal")
    {
      SECTION("B")
      {
        std::string_view data = R"(
let b : B;
b != true;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::not_eq_op:BinaryExpressionProcessor<language::not_eq_op, bool, bool, bool>)
     +-(language::name:b:NameProcessor)
     `-(language::true_kw:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("N")
      {
        std::string_view data = R"(
let n : N;
let m : N;
n != m;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::not_eq_op:BinaryExpressionProcessor<language::not_eq_op, bool, unsigned long, unsigned long>)
     +-(language::name:n:NameProcessor)
     `-(language::name:m:NameProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("Z")
      {
        std::string_view data = R"(
let a : Z;
a != 3;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::not_eq_op:BinaryExpressionProcessor<language::not_eq_op, bool, long, long>)
     +-(language::name:a:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R")
      {
        std::string_view data = R"(
2.3 != 1.2;
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::not_eq_op:BinaryExpressionProcessor<language::not_eq_op, bool, double, double>)
     +-(language::real:2.3:ValueProcessor)
     `-(language::real:1.2:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R^1")
      {
        std::string_view data = R"(
let x : R^1, x = 1;
x!=[2];
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<1ul, double>, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::not_eq_op:BinaryExpressionProcessor<language::not_eq_op, bool, TinyVector<1ul, double>, TinyVector<1ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<1ul>)
         `-(language::integer:2:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R^2")
      {
        std::string_view data = R"(
let x : R^2, x = [1,2];
x!=[0.3,0.7];
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<2ul, double>, TinyVector<2ul, double> >)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
 |       +-(language::integer:1:ValueProcessor)
 |       `-(language::integer:2:ValueProcessor)
 `-(language::not_eq_op:BinaryExpressionProcessor<language::not_eq_op, bool, TinyVector<2ul, double>, TinyVector<2ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
         +-(language::real:0.3:ValueProcessor)
         `-(language::real:0.7:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("R^3")
      {
        std::string_view data = R"(
let x : R^3, x = [1,2,3];
x!=[4,3,2];
)";

        std::string_view result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
 |       +-(language::integer:1:ValueProcessor)
 |       +-(language::integer:2:ValueProcessor)
 |       `-(language::integer:3:ValueProcessor)
 `-(language::not_eq_op:BinaryExpressionProcessor<language::not_eq_op, bool, TinyVector<3ul, double>, TinyVector<3ul, double> >)
     +-(language::name:x:NameProcessor)
     `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
         +-(language::integer:4:ValueProcessor)
         +-(language::integer:3:ValueProcessor)
         `-(language::integer:2:ValueProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("string != string")
      {
        std::string_view data = R"(
"foo" != "bar";
)";

        std::string string_name = demangle(typeid(std::string{}).name());

        std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::not_eq_op:BinaryExpressionProcessor<language::not_eq_op, bool, )" +
                             string_name + ", " + string_name + R"( >)
     +-(language::literal:"foo":ValueProcessor)
     `-(language::literal:"bar":ValueProcessor)
)";

        CHECK_AST(data, result);
      }
    }
  }

  SECTION("shift")
  {
#define CHECK_AST_BUILTIN_SHIFT_EXPRESSION(data, expected_output)                                               \
  {                                                                                                             \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                                  \
    auto ast = ASTBuilder::build(input);                                                                        \
                                                                                                                \
    ASTModulesImporter{*ast};                                                                                   \
                                                                                                                \
    BasicAffectationRegisterFor<EmbeddedData>{ASTNodeDataType::build<ASTNodeDataType::type_id_t>("builtin_t")}; \
                                                                                                                \
    OperatorRepository& repository = OperatorRepository::instance();                                            \
                                                                                                                \
    repository.addBinaryOperator<language::shift_left_op>(                                                      \
      std::make_shared<                                                                                         \
        BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const double>,                  \
                                       std::shared_ptr<const double>, std::shared_ptr<const double>>>());       \
                                                                                                                \
    repository.addBinaryOperator<language::shift_right_op>(                                                     \
      std::make_shared<                                                                                         \
        BinaryOperatorProcessorBuilder<language::shift_right_op, std::shared_ptr<const double>,                 \
                                       std::shared_ptr<const double>, std::shared_ptr<const double>>>());       \
                                                                                                                \
    SymbolTable& symbol_table = *ast->m_symbol_table;                                                           \
    auto [i_symbol, success]  = symbol_table.add(builtin_data_type.nameOfTypeId(), ast->begin());               \
    if (not success) {                                                                                          \
      throw UnexpectedError("cannot add '" + builtin_data_type.nameOfTypeId() + "' type for testing");          \
    }                                                                                                           \
                                                                                                                \
    i_symbol->attributes().setDataType(ASTNodeDataType::build<ASTNodeDataType::type_name_id_t>());              \
    i_symbol->attributes().setIsInitialized();                                                                  \
    i_symbol->attributes().value() = symbol_table.typeEmbedderTable().size();                                   \
    symbol_table.typeEmbedderTable().add(std::make_shared<TypeDescriptor>(builtin_data_type.nameOfTypeId()));   \
                                                                                                                \
    auto [i_symbol_bt_a, success_bt_a] = symbol_table.add("bt_a", ast->begin());                                \
    if (not success_bt_a) {                                                                                     \
      throw UnexpectedError("cannot add 'bt_a' of type builtin_t for testing");                                 \
    }                                                                                                           \
    i_symbol_bt_a->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);            \
    i_symbol_bt_a->attributes().setIsInitialized();                                                             \
    i_symbol_bt_a->attributes().value() =                                                                       \
      EmbeddedData(std::make_shared<DataHandler<const double>>(std::make_shared<double>(3.2)));                 \
                                                                                                                \
    auto [i_symbol_bt_b, success_bt_b] = symbol_table.add("bt_b", ast->begin());                                \
    if (not success_bt_b) {                                                                                     \
      throw UnexpectedError("cannot add 'bt_b' of type builtin_t for testing");                                 \
    }                                                                                                           \
    i_symbol_bt_b->attributes().setDataType(ast_node_data_type_from<std::shared_ptr<const double>>);            \
    i_symbol_bt_b->attributes().setIsInitialized();                                                             \
    i_symbol_bt_b->attributes().value() =                                                                       \
      EmbeddedData(std::make_shared<DataHandler<const double>>(std::make_shared<double>(5.3)));                 \
                                                                                                                \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                                     \
                                                                                                                \
    ASTSymbolTableBuilder{*ast};                                                                                \
    ASTNodeDataTypeBuilder{*ast};                                                                               \
                                                                                                                \
    ASTNodeDeclarationToAffectationConverter{*ast};                                                             \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                                        \
                                                                                                                \
    ASTNodeExpressionBuilder{*ast};                                                                             \
                                                                                                                \
    std::stringstream ast_output;                                                                               \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::exec_type}};             \
                                                                                                                \
    REQUIRE(ast_output.str() == expected_output);                                                               \
    ast->m_symbol_table->clearValues();                                                                         \
  }

    SECTION("shift left (builtin)")
    {
      std::string_view data          = R"(
let m : builtin_t;
let n : builtin_t;
n << m;
)";
      const std::string sptr_mangled = demangle<std::shared_ptr<double const>>();

      std::string result =
        R"(
(root:ASTNodeListProcessor)
 `-(language::shift_left_op:BinaryExpressionProcessor<language::shift_left_op, )" +
        sptr_mangled + ", " + sptr_mangled + ", " + sptr_mangled + R"( >)
     +-(language::name:n:NameProcessor)
     `-(language::name:m:NameProcessor)
)";

      CHECK_AST_BUILTIN_SHIFT_EXPRESSION(data, result);
    }

    SECTION("shift right (builtin)")
    {
      std::string_view data = R"(
let m : builtin_t;
let n : builtin_t;
n >> m;
)";

      const std::string sptr_mangled = demangle<std::shared_ptr<double const>>();

      std::string result =
        R"(
(root:ASTNodeListProcessor)
 `-(language::shift_right_op:BinaryExpressionProcessor<language::shift_right_op, )" +
        sptr_mangled + ", " + sptr_mangled + ", " + sptr_mangled + R"( >)
     +-(language::name:n:NameProcessor)
     `-(language::name:m:NameProcessor)
)";

      CHECK_AST_BUILTIN_SHIFT_EXPRESSION(data, result);
    }
  }

  SECTION("Errors")
  {
    SECTION("Invalid binary operator type")
    {
      auto ast = std::make_unique<ASTNode>();
      ast->set_type<language::ignored>();
      ast->children.emplace_back(std::make_unique<ASTNode>());
      ast->children.emplace_back(std::make_unique<ASTNode>());
      ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
      ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

      REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "unexpected error: undefined binary operator");
    }

    SECTION("Invalid divide by bool operators")
    {
      SECTION("B lhs")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::divide_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: B / B");
      }

      SECTION("N lhs")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::divide_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: N / B");
      }

      SECTION("Z lhs")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::divide_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: Z / B");
      }

      SECTION("R lhs")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::divide_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::double_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: R / B");
      }

      SECTION("string lhs")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::divide_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: string / B");
      }
    }

    SECTION("Invalid string binary operators")
    {
      SECTION("lhs bad multiply")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::multiply_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void * Z");
      }

      SECTION("left string multiply")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::multiply_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: string * Z");
      }

      SECTION("right string multiply")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::multiply_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: Z * string");
      }

      SECTION("lhs bad divide")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::divide_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void / Z");
      }

      SECTION("left string divide")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::divide_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: string / Z");
      }

      SECTION("right string divide")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::divide_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: Z / string");
      }

      SECTION("lhs bad plus")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::plus_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void + Z");
      }

      SECTION("left string plus bad rhs")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::plus_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: string + void");
      }

      SECTION("right string plus bad lhs")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::plus_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: void + string");
      }

      SECTION("lhs bad minus")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::minus_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void - Z");
      }

      SECTION("left string minus")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::minus_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: string - Z");
      }

      SECTION("right string minus")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::minus_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: Z - string");
      }

      SECTION("lhs bad or")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::or_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void or Z");
      }

      SECTION("left string or")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::or_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: string or Z");
      }

      SECTION("right string or")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::or_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: Z or string");
      }

      SECTION("lhs bad and")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::and_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void and Z");
      }

      SECTION("left string and")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::and_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: string and Z");
      }

      SECTION("right string and")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::and_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: Z and string");
      }

      SECTION("lhs bad xor")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::xor_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void xor Z");
      }

      SECTION("left string xor")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::xor_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: string xor Z");
      }

      SECTION("right string xor")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::xor_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: Z xor string");
      }

      SECTION("lhs bad >")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::greater_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void > Z");
      }

      SECTION("left string >")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::greater_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: string > Z");
      }

      SECTION("right string >")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::greater_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: Z > string");
      }

      SECTION("lhs bad <")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::lesser_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void < Z");
      }

      SECTION("left string <")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::lesser_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: string < Z");
      }

      SECTION("right string <")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::lesser_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: Z < string");
      }

      SECTION("lhs bad >=")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::greater_or_eq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void >= Z");
      }

      SECTION("left string >=")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::greater_or_eq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: string >= Z");
      }

      SECTION("right string >=")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::greater_or_eq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: Z >= string");
      }

      SECTION("lhs bad <=")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::lesser_or_eq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void <= Z");
      }

      SECTION("left string <=")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::lesser_or_eq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: string <= Z");
      }

      SECTION("right string <=")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::lesser_or_eq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: Z <= string");
      }

      SECTION("lhs bad ==")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::eqeq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void == Z");
      }

      SECTION("left string ==")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::eqeq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: string == Z");
      }

      SECTION("right string ==")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::eqeq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: Z == string");
      }

      SECTION("lhs bad !=")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::not_eq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast}, "undefined binary operator type: void != Z");
      }

      SECTION("left string !=")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::not_eq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: string != Z");
      }

      SECTION("right string !=")
      {
        auto ast = std::make_unique<ASTNode>();
        ast->set_type<language::not_eq_op>();
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children.emplace_back(std::make_unique<ASTNode>());
        ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        ast->children[1]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

        REQUIRE_THROWS_WITH(ASTNodeBinaryOperatorExpressionBuilder{*ast},
                            "undefined binary operator type: Z != string");
      }
    }

    SECTION("invalid R^d operators")
    {
      SECTION("invalid operator R^1 > R^1")
      {
        std::string_view data = R"(
let x : R^1, x = 0;
let y : R^1, y = 0;
x > y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^1 and R^1)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^1 >= R^1")
      {
        std::string_view data = R"(
let x : R^1, x = 0;
let y : R^1, y = 0;
x >= y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^1 and R^1)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^1 < R^1")
      {
        std::string_view data = R"(
let x : R^1, x = 0;
let y : R^1, y = 0;
x < y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^1 and R^1)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^1 <= R^1")
      {
        std::string_view data = R"(
let x : R^1, x = 0;
let y : R^1, y = 1;
x <= y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^1 and R^1)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^1 * R^1")
      {
        std::string_view data = R"(
let x : R^1, x = 0;
let y : R^1, y = 0;
x * y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^1 and R^1)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^1 / R^1")
      {
        std::string_view data = R"(
let x : R^1, x = 0;
let y : R^1, y = 0;
x / y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^1 and R^1)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^2 > R^2")
      {
        std::string_view data = R"(
let x : R^2, x = 0;
let y : R^2, y = 0;
x > y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^2 and R^2)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^2 >= R^2")
      {
        std::string_view data = R"(
let x : R^2, x = 0;
let y : R^2, y = 0;
x >= y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^2 and R^2)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^2 < R^2")
      {
        std::string_view data = R"(
let x : R^2, x = 0;
let y : R^2, y = 0;
x < y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^2 and R^2)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^2 <= R^2")
      {
        std::string_view data = R"(
let x : R^2, x = 0;
let y : R^2, y = 0;
x <= y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^2 and R^2)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^2 * R^2")
      {
        std::string_view data = R"(
let x : R^2, x = 0;
let y : R^2, y = 0;
x * y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^2 and R^2)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^2 / R^2")
      {
        std::string_view data = R"(
let x : R^2, x = 0;
let y : R^2, y = 0;
x / y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^2 and R^2)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^3 > R^3")
      {
        std::string_view data = R"(
let x : R^3, x = 0;
let y : R^3, y = 0;
x > y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^3 and R^3)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^3 >= R^3")
      {
        std::string_view data = R"(
let x : R^3, x = 0;
let y : R^3, y = 0;
x >= y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^3 and R^3)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^3 < R^3")
      {
        std::string_view data = R"(
let x : R^3, x = 0;
let y : R^3, y = 0;
x < y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^3 and R^3)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^3 <= R^3")
      {
        std::string_view data = R"(
let x : R^3, x = 0;
let y : R^3, y = 0;
x <= y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^3 and R^3)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^3 * R^3")
      {
        std::string_view data = R"(
let x : R^3, x = 0;
let y : R^3, y = 0;
x * y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^3 and R^3)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("invalid operator R^3 / R^3")
      {
        std::string_view data = R"(
let x : R^3, x = 0;
let y : R^3, y = 0;
x / y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^3 and R^3)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }
    }

    SECTION("invalid operand types")
    {
      SECTION("incompatible operand dimensions")
      {
        std::string_view data = R"(
let x : R^3, x = 0;
let y : R^1, y = 0;
x + y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^3 and R^1)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("incompatible operand dimensions")
      {
        std::string_view data = R"(
let x : R^1, x = 0;
let y : R^2, y = 0;
x - y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^1 and R^2)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("incompatible operand dimensions")
      {
        std::string_view data = R"(
let x : R^3, x = 0;
let y : R^2, y = 0;
x == y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^3 and R^2)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }

      SECTION("incompatible operand dimensions")
      {
        std::string_view data = R"(
let x : R^1, x = 0;
let y : R^2, y = 0;
x != y;
)";

        const std::string error_mesh = R"(undefined binary operator
note: incompatible operand types R^1 and R^2)";

        REQUIRE_AST_THROWS_WITH(data, error_mesh);
      }
    }
  }
}
