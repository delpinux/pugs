#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Messenger.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <utils/checkpointing/ReadArray.hpp>
#include <utils/checkpointing/WriteArray.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_Array", "[utils/checkpointing]")
{
  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});
    HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

    SECTION("Array")
    {
      HighFive::Group checkpoint_group = file.createGroup("checkpoint_group");
      HighFive::Group useless_group;

      Array<CellType> cell_type_array{17 + 3 * parallel::rank()};
      cell_type_array.fill(CellType::Line);
      for (size_t i = 0; i < 10; ++i) {
        cell_type_array[std::rand() / (RAND_MAX / cell_type_array.size())] = CellType::Triangle;
        cell_type_array[std::rand() / (RAND_MAX / cell_type_array.size())] = CellType::Quadrangle;
        cell_type_array[std::rand() / (RAND_MAX / cell_type_array.size())] = CellType::Polygon;
        cell_type_array[std::rand() / (RAND_MAX / cell_type_array.size())] = CellType::Tetrahedron;
        cell_type_array[std::rand() / (RAND_MAX / cell_type_array.size())] = CellType::Pyramid;
        cell_type_array[std::rand() / (RAND_MAX / cell_type_array.size())] = CellType::Prism;
        cell_type_array[std::rand() / (RAND_MAX / cell_type_array.size())] = CellType::Diamond;
        cell_type_array[std::rand() / (RAND_MAX / cell_type_array.size())] = CellType::Hexahedron;
        cell_type_array[std::rand() / (RAND_MAX / cell_type_array.size())] = CellType::Line;
      }
      checkpointing::write(checkpoint_group, "cell_type_array", cell_type_array);

      Array<CellId> cell_id_array{27 + 2 * parallel::rank()};
      cell_id_array.fill(0);
      for (size_t i = 0; i < 10; ++i) {
        cell_id_array[std::rand() / (RAND_MAX / cell_id_array.size())] =
          std::rand() / (RAND_MAX / cell_id_array.size());
      }
      checkpointing::write(checkpoint_group, "cell_id_array", cell_id_array);

      Array<FaceId> face_id_array{29 + 2 * parallel::rank()};
      face_id_array.fill(0);
      for (size_t i = 0; i < 10; ++i) {
        face_id_array[std::rand() / (RAND_MAX / face_id_array.size())] =
          std::rand() / (RAND_MAX / face_id_array.size());
      }
      checkpointing::write(checkpoint_group, "face_id_array", face_id_array);

      Array<EdgeId> edge_id_array{13 + 2 * parallel::rank()};
      edge_id_array.fill(0);
      for (size_t i = 0; i < 10; ++i) {
        edge_id_array[std::rand() / (RAND_MAX / edge_id_array.size())] =
          std::rand() / (RAND_MAX / edge_id_array.size());
      }
      checkpointing::write(checkpoint_group, "edge_id_array", edge_id_array);

      Array<NodeId> node_id_array{22 + 2 * parallel::rank()};
      node_id_array.fill(0);
      for (size_t i = 0; i < 10; ++i) {
        node_id_array[std::rand() / (RAND_MAX / node_id_array.size())] =
          std::rand() / (RAND_MAX / node_id_array.size());
      }
      checkpointing::write(checkpoint_group, "node_id_array", node_id_array);

      Array<double> double_array{16 + 3 * parallel::rank()};
      double_array.fill(0);
      for (size_t i = 0; i < 10; ++i) {
        double_array[std::rand() / (RAND_MAX / double_array.size())] =
          (1. * std::rand()) / (1. * RAND_MAX / double_array.size());
      }
      checkpointing::write(checkpoint_group, "double_array", double_array);

      file.flush();

      auto is_same = [](const auto& a, const auto& b) {
        bool same = true;
        for (size_t i = 0; i < a.size(); ++i) {
          if (a[i] != b[i]) {
            same = false;
          }
        }
        return parallel::allReduceAnd(same);
      };

      Array read_cell_type_array = checkpointing::readArray<CellType>(checkpoint_group, "cell_type_array");
      REQUIRE(is_same(cell_type_array, read_cell_type_array));

      Array read_cell_id_array = checkpointing::readArray<CellId>(checkpoint_group, "cell_id_array");
      REQUIRE(is_same(cell_id_array, read_cell_id_array));

      Array read_face_id_array = checkpointing::readArray<FaceId>(checkpoint_group, "face_id_array");
      REQUIRE(is_same(face_id_array, read_face_id_array));

      Array read_edge_id_array = checkpointing::readArray<EdgeId>(checkpoint_group, "edge_id_array");
      REQUIRE(is_same(edge_id_array, read_edge_id_array));

      Array read_node_id_array = checkpointing::readArray<NodeId>(checkpoint_group, "node_id_array");
      REQUIRE(is_same(node_id_array, read_node_id_array));

      Array read_double_array = checkpointing::readArray<double>(checkpoint_group, "double_array");
      REQUIRE(is_same(double_array, read_double_array));
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }
}
