#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemArray.hpp>
#include <mesh/Mesh.hpp>
#include <utils/Messenger.hpp>

template class ItemArray<int, ItemType::node>;

// clazy:excludeall=non-pod-global-static

TEST_CASE("ItemArray", "[mesh]")
{
  SECTION("default constructors")
  {
    REQUIRE_NOTHROW(NodeArray<int>{});
    REQUIRE_NOTHROW(EdgeArray<int>{});
    REQUIRE_NOTHROW(FaceArray<int>{});
    REQUIRE_NOTHROW(CellArray<int>{});

    REQUIRE(not NodeArray<int>{}.isBuilt());
    REQUIRE(not EdgeArray<int>{}.isBuilt());
    REQUIRE(not FaceArray<int>{}.isBuilt());
    REQUIRE(not CellArray<int>{}.isBuilt());
  }

  SECTION("1D")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_1d_v = named_mesh.mesh();
        auto mesh_1d   = mesh_1d_v->get<Mesh<1>>();

        const Connectivity<1>& connectivity = mesh_1d->connectivity();

        REQUIRE_NOTHROW(NodeArray<int>{connectivity, 3});
        REQUIRE_NOTHROW(EdgeArray<int>{connectivity, 3});
        REQUIRE_NOTHROW(FaceArray<int>{connectivity, 3});
        REQUIRE_NOTHROW(CellArray<int>{connectivity, 3});

        REQUIRE(NodeArray<int>{connectivity, 3}.isBuilt());
        REQUIRE(EdgeArray<int>{connectivity, 3}.isBuilt());
        REQUIRE(FaceArray<int>{connectivity, 3}.isBuilt());
        REQUIRE(CellArray<int>{connectivity, 3}.isBuilt());

        NodeArray<int> node_value{connectivity, 3};
        EdgeArray<int> edge_value{connectivity, 3};
        FaceArray<int> face_value{connectivity, 3};
        CellArray<int> cell_value{connectivity, 3};

        REQUIRE(edge_value.numberOfItems() == node_value.numberOfItems());
        REQUIRE(face_value.numberOfItems() == node_value.numberOfItems());
        REQUIRE(cell_value.numberOfItems() + 1 == node_value.numberOfItems());

        REQUIRE(node_value.sizeOfArrays() == 3);
        REQUIRE(edge_value.sizeOfArrays() == 3);
        REQUIRE(face_value.sizeOfArrays() == 3);
        REQUIRE(cell_value.sizeOfArrays() == 3);
      }
    }
  }

  SECTION("2D")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_2d_v = named_mesh.mesh();
        auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

        const Connectivity<2>& connectivity = mesh_2d->connectivity();

        REQUIRE_NOTHROW(NodeArray<int>{connectivity, 2});
        REQUIRE_NOTHROW(EdgeArray<int>{connectivity, 2});
        REQUIRE_NOTHROW(FaceArray<int>{connectivity, 2});
        REQUIRE_NOTHROW(CellArray<int>{connectivity, 2});

        REQUIRE(NodeArray<int>{connectivity, 2}.isBuilt());
        REQUIRE(EdgeArray<int>{connectivity, 2}.isBuilt());
        REQUIRE(FaceArray<int>{connectivity, 2}.isBuilt());
        REQUIRE(CellArray<int>{connectivity, 2}.isBuilt());

        NodeArray<int> node_value{connectivity, 2};
        EdgeArray<int> edge_value{connectivity, 2};
        FaceArray<int> face_value{connectivity, 2};
        CellArray<int> cell_value{connectivity, 2};

        REQUIRE(edge_value.numberOfItems() == face_value.numberOfItems());

        REQUIRE(node_value.sizeOfArrays() == 2);
        REQUIRE(edge_value.sizeOfArrays() == 2);
        REQUIRE(face_value.sizeOfArrays() == 2);
        REQUIRE(cell_value.sizeOfArrays() == 2);
      }
    }
  }

  SECTION("3D")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_3d_v = named_mesh.mesh();
        auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

        const Connectivity<3>& connectivity = mesh_3d->connectivity();

        REQUIRE_NOTHROW(NodeArray<int>{connectivity, 3});
        REQUIRE_NOTHROW(EdgeArray<int>{connectivity, 3});
        REQUIRE_NOTHROW(FaceArray<int>{connectivity, 3});
        REQUIRE_NOTHROW(CellArray<int>{connectivity, 3});

        REQUIRE(NodeArray<int>{connectivity, 3}.isBuilt());
        REQUIRE(EdgeArray<int>{connectivity, 3}.isBuilt());
        REQUIRE(FaceArray<int>{connectivity, 3}.isBuilt());
        REQUIRE(CellArray<int>{connectivity, 3}.isBuilt());

        NodeArray<int> node_value{connectivity, 3};
        EdgeArray<int> edge_value{connectivity, 3};
        FaceArray<int> face_value{connectivity, 3};
        CellArray<int> cell_value{connectivity, 3};

        REQUIRE(node_value.sizeOfArrays() == 3);
        REQUIRE(edge_value.sizeOfArrays() == 3);
        REQUIRE(face_value.sizeOfArrays() == 3);
        REQUIRE(cell_value.sizeOfArrays() == 3);
      }
    }
  }

  SECTION("set values from array")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_3d_v = named_mesh.mesh();
        auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

        const Connectivity<3>& connectivity = mesh_3d->connectivity();

        Table<size_t> table{connectivity.numberOfCells(), 3};
        {
          size_t k = 0;
          for (size_t i = 0; i < table.numberOfRows(); ++i) {
            for (size_t j = 0; j < table.numberOfColumns(); ++j) {
              table(i, j) = k++;
            }
          }
        }

        CellArray<size_t> cell_array{connectivity, table};

        auto is_same = [](const CellArray<size_t>& a_cell_array, const Table<size_t>& a_table) {
          bool same = true;
          for (CellId cell_id = 0; cell_id < a_cell_array.numberOfItems(); ++cell_id) {
            auto sub_array = a_cell_array[cell_id];
            for (size_t i = 0; i < sub_array.size(); ++i) {
              same &= (sub_array[i] == a_table(cell_id, i));
            }
          }
          return same;
        };

        REQUIRE(is_same(cell_array, table));
        REQUIRE(&(cell_array[CellId{0}][0]) == &(table(0, 0)));
      }
    }
  }

  SECTION("copy")
  {
    auto is_same = [](const auto& cell_array, int value) {
      bool same = true;
      for (CellId cell_id = 0; cell_id < cell_array.numberOfItems(); ++cell_id) {
        auto sub_array = cell_array[cell_id];
        for (size_t i = 0; i < sub_array.size(); ++i) {
          same &= (sub_array[i] == value);
        }
      }
      return same;
    };

    std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_3d_v = named_mesh.mesh();
        auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

        const Connectivity<3>& connectivity = mesh_3d->connectivity();

        CellArray<int> cell_array{connectivity, 4};
        cell_array.fill(parallel::rank());

        CellArray<const int> cell_array_const_view{cell_array};
        REQUIRE(cell_array.numberOfItems() == cell_array_const_view.numberOfItems());
        REQUIRE(cell_array.sizeOfArrays() == cell_array_const_view.sizeOfArrays());
        REQUIRE(is_same(cell_array_const_view, static_cast<std::int64_t>(parallel::rank())));

        CellArray<const int> const_cell_array;
        const_cell_array = copy(cell_array);

        CellArray<int> duplicated_cell_array{connectivity, cell_array.sizeOfArrays()};
        copy_to(const_cell_array, duplicated_cell_array);

        cell_array.fill(0);

        REQUIRE(is_same(cell_array, 0));
        REQUIRE(is_same(cell_array_const_view, 0));
        REQUIRE(is_same(const_cell_array, static_cast<std::int64_t>(parallel::rank())));
        REQUIRE(is_same(duplicated_cell_array, static_cast<std::int64_t>(parallel::rank())));
      }
    }
  }

  SECTION("WeakItemArray")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_2d_v = named_mesh.mesh();
        auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

        const Connectivity<2>& connectivity = mesh_2d->connectivity();

        WeakFaceArray<int> weak_face_array{connectivity, 5};
        for (FaceId face_id = 0; face_id < mesh_2d->numberOfFaces(); ++face_id) {
          for (size_t i = 0; i < weak_face_array.sizeOfArrays(); ++i) {
            weak_face_array[face_id][i] = 2 * face_id + 3 * i;
          }
        }

        FaceArray<const int> face_array{weak_face_array};

        REQUIRE(face_array.connectivity_ptr() == weak_face_array.connectivity_ptr());

        FaceArray<int> copied_face_array = copy(weak_face_array);
        REQUIRE(copied_face_array.connectivity_ptr() == weak_face_array.connectivity_ptr());
        REQUIRE(weak_face_array.sizeOfArrays() == copied_face_array.sizeOfArrays());

        weak_face_array.fill(0);

        {
          bool is_same = true;
          for (FaceId face_id = 0; face_id < mesh_2d->numberOfFaces(); ++face_id) {
            for (size_t i = 0; i < face_array.sizeOfArrays(); ++i) {
              is_same &= (face_array[face_id][i] == 0);
            }
          }
          REQUIRE(is_same);
        }

        {
          bool is_same = true;
          for (FaceId face_id = 0; face_id < mesh_2d->numberOfFaces(); ++face_id) {
            for (size_t i = 0; i < copied_face_array.sizeOfArrays(); ++i) {
              is_same &= (copied_face_array[face_id][i] == static_cast<int>(2 * face_id + 3 * i));
            }
          }
          REQUIRE(is_same);
        }
      }
    }
  }

  SECTION("output")
  {
    auto mesh_v = MeshDataBaseForTests::get().unordered1DMesh();
    auto mesh   = mesh_v->get<Mesh<1>>();

    Table<int> table{mesh->numberOfCells(), 3};
    for (size_t i = 0; i < table.numberOfRows(); ++i) {
      for (size_t j = 0; j < table.numberOfColumns(); ++j) {
        table(i, j) = 2 * i + 1 + 3 * j;
      }
    }

    CellArray<int> cell_array{mesh->connectivity(), table};

    std::ostringstream table_ost;
    table_ost << table;
    std::ostringstream cell_array_ost;
    cell_array_ost << cell_array;
    REQUIRE(table_ost.str() == cell_array_ost.str());
  }

#ifndef NDEBUG
  SECTION("error")
  {
    SECTION("checking for build ItemArray")
    {
      CellArray<int> cell_array;
      REQUIRE_THROWS_WITH(cell_array[CellId{0}], "ItemArray is not built");

      FaceArray<int> face_array;
      REQUIRE_THROWS_WITH(face_array[FaceId{0}], "ItemArray is not built");

      EdgeArray<int> edge_array;
      REQUIRE_THROWS_WITH(edge_array[EdgeId{0}], "ItemArray is not built");

      NodeArray<int> node_array;
      REQUIRE_THROWS_WITH(node_array[NodeId{0}], "ItemArray is not built");
    }

    SECTION("checking for bounds violation")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          CellArray<int> cell_array{connectivity, 1};
          CellId invalid_cell_id = connectivity.numberOfCells();
          REQUIRE_THROWS_WITH(cell_array[invalid_cell_id], "invalid item_id");

          FaceArray<int> face_array{connectivity, 2};
          FaceId invalid_face_id = connectivity.numberOfFaces();
          REQUIRE_THROWS_WITH(face_array[invalid_face_id], "invalid item_id");

          EdgeArray<int> edge_array{connectivity, 1};
          EdgeId invalid_edge_id = connectivity.numberOfEdges();
          REQUIRE_THROWS_WITH(edge_array[invalid_edge_id], "invalid item_id");

          NodeArray<int> node_array{connectivity, 0};
          NodeId invalid_node_id = connectivity.numberOfNodes();
          REQUIRE_THROWS_WITH(node_array[invalid_node_id], "invalid item_id");
        }
      }
    }

    SECTION("set values from invalid array size")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          Table<size_t> values{connectivity.numberOfCells() + 3, 3};
          REQUIRE_THROWS_WITH(CellArray<size_t>(connectivity, values), "invalid table, wrong number of rows");
        }
      }
    }

    SECTION("invalid copy_to")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity_2d = mesh_2d->connectivity();

          std::array mesh_3d_list = MeshDataBaseForTests::get().all3DMeshes();

          for (const auto& named_mesh_3d : mesh_3d_list) {
            SECTION(named_mesh_3d.name())
            {
              auto mesh_3d_v = named_mesh_3d.mesh();
              auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

              const Connectivity<3>& connectivity_3d = mesh_3d->connectivity();

              CellArray<int> cell_2d_array{connectivity_2d, 3};
              CellArray<int> cell_3d_array{connectivity_3d, 3};
              REQUIRE_THROWS_WITH(copy_to(cell_2d_array, cell_3d_array), "different connectivities");

              CellArray<int> cell_2d_array2{connectivity_2d, 2};
              REQUIRE_THROWS_WITH(copy_to(cell_2d_array, cell_2d_array2), "incompatible size of arrays");
            }
          }
        }
      }
    }
  }
#endif   // NDEBUG
}
