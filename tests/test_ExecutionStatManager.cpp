#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/ExecutionStatManager.hpp>
#include <utils/Stringify.hpp>
#include <utils/pugs_config.hpp>

#include <sstream>

class ExecutionStatManagerTester
{
 public:
  double
  getPreviousCumulativeElapseTime() const
  {
    return ExecutionStatManager::getInstance().m_previous_cumulative_elapse_time;
  }

  double
  getPreviousCumulativeTotalCpuTime() const
  {
    return ExecutionStatManager::getInstance().m_previous_cumulative_total_cpu_time;
  }

  void
  setElapseTime(const Timer& timer) const
  {
    ExecutionStatManager::getInstance().m_elapse_time = timer;
  }

  Timer&
  getElapseTime() const
  {
    return ExecutionStatManager::getInstance().m_elapse_time;
  }

  ExecutionStatManagerTester()  = default;
  ~ExecutionStatManagerTester() = default;
};

// clazy:excludeall=non-pod-global-static

TEST_CASE("ExecutionStatManager", "[utils]")
{
  REQUIRE_NOTHROW(ExecutionStatManager::create());
  REQUIRE_THROWS_WITH(ExecutionStatManager::create(), "unexpected error: ExecutionStatManager already created");

  ExecutionStatManagerTester esm_tester;

  REQUIRE(ExecutionStatManager::getInstance().runNumber() == 1);
  REQUIRE(ExecutionStatManager::getInstance().doPrint() == true);
  REQUIRE(ExecutionStatManager::getInstance().exitCode() == 0);

  REQUIRE(esm_tester.getPreviousCumulativeElapseTime() == 0);
  REQUIRE(esm_tester.getPreviousCumulativeTotalCpuTime() == 0);

  REQUIRE_NOTHROW(ExecutionStatManager::getInstance().setRunNumber(2));
  REQUIRE(ExecutionStatManager::getInstance().runNumber() == 2);

  REQUIRE_NOTHROW(ExecutionStatManager::getInstance().setPreviousCumulativeElapseTime(100));
  REQUIRE_NOTHROW(ExecutionStatManager::getInstance().setPreviousCumulativeTotalCPUTime(200));

  REQUIRE(esm_tester.getPreviousCumulativeElapseTime() == 100);
  REQUIRE(esm_tester.getPreviousCumulativeTotalCpuTime() == 200);

  using namespace std::chrono_literals;
  Timer t{std::chrono::high_resolution_clock::now() - (2 * 21h + 267s)};
  t.pause();
  esm_tester.setElapseTime(t);

  REQUIRE(ExecutionStatManager::getInstance().getElapseTime() == t.seconds());
  REQUIRE(ExecutionStatManager::getInstance().getCumulativeElapseTime() == 100 + t.seconds());
  REQUIRE(ExecutionStatManager::getInstance().getCumulativeTotalCPUTime() >= 200);

  std::ostringstream os;
  // One just call the function not test is performed. It is quite
  // boring to check the result and not that useful.
  ExecutionStatManager::getInstance().printInfo(os);

  ExecutionStatManager::getInstance().setPrint(false);
  REQUIRE(ExecutionStatManager::getInstance().doPrint() == false);

  ExecutionStatManager::getInstance().setExitCode(1);
  REQUIRE(ExecutionStatManager::getInstance().exitCode() == 1);

  REQUIRE_NOTHROW(ExecutionStatManager::destroy());
  // One allows multiple destruction to handle unexpected code exit
  REQUIRE_NOTHROW(ExecutionStatManager::destroy());
}
