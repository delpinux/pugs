#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeJumpPlacementChecker.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ParseError.hpp>

#include <pegtl/string_input.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeJumpPlacementChecker", "[language]")
{
  SECTION("break")
  {
    SECTION("in for loop")
    {
      std::string_view data = R"(
for(;;) {
  break;
}
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      ASTSymbolTableBuilder{*ast};
      ASTNodeDataTypeBuilder{*ast};

      REQUIRE_NOTHROW(ASTNodeJumpPlacementChecker{*ast});
      ast->m_symbol_table->clearValues();
    }

    SECTION("in while loop")
    {
      std::string_view data = R"(
while(true) {
  break;
}
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      ASTSymbolTableBuilder{*ast};
      ASTNodeDataTypeBuilder{*ast};

      REQUIRE_NOTHROW(ASTNodeJumpPlacementChecker{*ast});
      ast->m_symbol_table->clearValues();
    }

    SECTION("in do while loop")
    {
      std::string_view data = R"(
do {
  break;
} while(true);
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      ASTSymbolTableBuilder{*ast};
      ASTNodeDataTypeBuilder{*ast};

      REQUIRE_NOTHROW(ASTNodeJumpPlacementChecker{*ast});
      ast->m_symbol_table->clearValues();
    }

    SECTION("misplaced")
    {
      std::string_view data = R"(
{
  break;
}
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      ASTSymbolTableBuilder{*ast};
      ASTNodeDataTypeBuilder{*ast};

      ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::undefined_t>();

      REQUIRE_THROWS_AS(ASTNodeJumpPlacementChecker{*ast}, ParseError);
      ast->m_symbol_table->clearValues();
    }
  }

  SECTION("continue")
  {
    SECTION("in for loop")
    {
      std::string_view data = R"(
for(;;) {
  continue;
}
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      ASTSymbolTableBuilder{*ast};
      ASTNodeDataTypeBuilder{*ast};

      REQUIRE_NOTHROW(ASTNodeJumpPlacementChecker{*ast});
      ast->m_symbol_table->clearValues();
    }

    SECTION("in while loop")
    {
      std::string_view data = R"(
while(true) {
  continue;
}
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      ASTSymbolTableBuilder{*ast};
      ASTNodeDataTypeBuilder{*ast};

      REQUIRE_NOTHROW(ASTNodeJumpPlacementChecker{*ast});
      ast->m_symbol_table->clearValues();
    }

    SECTION("in do while loop")
    {
      std::string_view data = R"(
do {
  continue;
} while(true);
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      ASTSymbolTableBuilder{*ast};
      ASTNodeDataTypeBuilder{*ast};

      REQUIRE_NOTHROW(ASTNodeJumpPlacementChecker{*ast});
      ast->m_symbol_table->clearValues();
    }

    SECTION("misplaced")
    {
      std::string_view data = R"(
{
  continue;
}
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);
      ASTSymbolTableBuilder{*ast};
      ASTNodeDataTypeBuilder{*ast};

      ast->children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::undefined_t>();

      REQUIRE_THROWS_AS(ASTNodeJumpPlacementChecker{*ast}, ParseError);
      ast->m_symbol_table->clearValues();
    }
  }
}
