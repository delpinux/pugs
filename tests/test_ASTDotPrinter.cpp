#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/utils/ASTDotPrinter.hpp>
#include <language/utils/SymbolTable.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_DOT(data, expected_output)                                                      \
  {                                                                                           \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);            \
    static_assert(std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view>); \
                                                                                              \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                \
    auto ast = ASTBuilder::build(input);                                                      \
                                                                                              \
    std::stringstream ast_output;                                                             \
    ast_output << '\n' << ASTDotPrinter{*ast};                                                \
                                                                                              \
    REQUIRE(ast_output.str() == expected_output);                                             \
    ast->m_symbol_table->clearValues();                                                       \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTDotPrinter", "[language]")
{
  rang::setControlMode(rang::control::Off);

  std::string_view data = R"(
let n:N, n = 2 + 3;
)";

  std::string_view result = R"(
digraph parse_tree
{
  x0 [ label="root \nundefined" ]
  x0 -> { x1 }
  x1 [ label="language::var_declaration\nlet n:N, n = 2 + 3\nundefined" ]
  x1 -> { x2, x3, x4, x5 }
  x2 [ label="language::name\nn\nundefined" ]
  x3 [ label="language::N_set\nN\nundefined" ]
  x4 [ label="language::name\nn\nundefined" ]
  x5 [ label="language::plus_op\nundefined" ]
  x5 -> { x6, x7 }
  x6 [ label="language::integer\n2\nundefined" ]
  x7 [ label="language::integer\n3\nundefined" ]
}
)";
  CHECK_DOT(data, result);
}
