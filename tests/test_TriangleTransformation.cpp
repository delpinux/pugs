#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/TriangleTransformation.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("TriangleTransformation", "[geometry]")
{
  SECTION("2D")
  {
    using R2 = TinyVector<2>;

    const R2 a{1, 2};
    const R2 b{3, 1};
    const R2 c{2, 5};

    const TriangleTransformation<2> t(a, b, c);

    REQUIRE(t(R2{0, 0})[0] == Catch::Approx(1));
    REQUIRE(t(R2{0, 0})[1] == Catch::Approx(2));

    REQUIRE(t(R2{1, 0})[0] == Catch::Approx(3));
    REQUIRE(t(R2{1, 0})[1] == Catch::Approx(1));

    REQUIRE(t(R2{0, 1})[0] == Catch::Approx(2));
    REQUIRE(t(R2{0, 1})[1] == Catch::Approx(5));

    REQUIRE(t(R2{1. / 3, 1. / 3})[0] == Catch::Approx(2));
    REQUIRE(t(R2{1. / 3, 1. / 3})[1] == Catch::Approx(8. / 3));

    REQUIRE(t.jacobianDeterminant() == Catch::Approx(7));

    SECTION("Polynomial integral")
    {
      auto p = [](const R2& X) {
        const double x = X[0];
        const double y = X[1];
        return 2 * x * x + 3 * x * y + y * y + 3 * y + 1;
      };

      QuadratureFormula<2> qf = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(2));

      double sum = 0;
      for (size_t i = 0; i < qf.numberOfPoints(); ++i) {
        sum += qf.weight(i) * t.jacobianDeterminant() * p(t(qf.point(i)));
      }

      REQUIRE(sum == Catch::Approx(3437. / 24));
    }
  }

  SECTION("3D")
  {
    SECTION("Data")
    {
      using R2 = TinyVector<2>;
      using R3 = TinyVector<3>;

      const R3 a{1, 2, 2};
      const R3 b{4, 1, 3};
      const R3 c{2, 5, 1};

      const TriangleTransformation<3> t(a, b, c);

      REQUIRE(t(R2{0, 0})[0] == Catch::Approx(1));
      REQUIRE(t(R2{0, 0})[1] == Catch::Approx(2));
      REQUIRE(t(R2{0, 0})[2] == Catch::Approx(2));

      REQUIRE(t(R2{1, 0})[0] == Catch::Approx(4));
      REQUIRE(t(R2{1, 0})[1] == Catch::Approx(1));
      REQUIRE(t(R2{1, 0})[2] == Catch::Approx(3));

      REQUIRE(t(R2{0, 1})[0] == Catch::Approx(2));
      REQUIRE(t(R2{0, 1})[1] == Catch::Approx(5));
      REQUIRE(t(R2{0, 1})[2] == Catch::Approx(1));

      REQUIRE(t(R2{1. / 3, 1. / 3})[0] == Catch::Approx(7. / 3));
      REQUIRE(t(R2{1. / 3, 1. / 3})[1] == Catch::Approx(8. / 3));
      REQUIRE(t(R2{1. / 3, 1. / 3})[2] == Catch::Approx(2));

      REQUIRE(t.areaVariationNorm() == Catch::Approx(2 * std::sqrt(30)));
    }

    SECTION("Area modulus")
    {
      using R3 = TinyVector<3>;

      const R3 a_hat{0, 0, 0};
      const R3 b_hat{1, 0, 0};
      const R3 c_hat{0, 1, 0};

      const double theta = 2.3;
      TinyMatrix<3> r_theta(1, 0, 0,                               //
                            0, std::cos(theta), std::sin(theta),   //
                            0, -std::sin(theta), std::cos(theta));

      const double phi = 0.7;
      TinyMatrix<3> r_phi(std::cos(phi), std::sin(phi), 0,    //
                          -std::sin(phi), std::cos(phi), 0,   //
                          0, 0, 1);

      const R3 a = r_phi * r_theta * a_hat;
      const R3 b = r_phi * r_theta * b_hat;
      const R3 c = r_phi * r_theta * c_hat;

      const TriangleTransformation<3> t(a, b, c);

      // The triangle (a,b,c) is just a rotation of the initial one
      REQUIRE(t.areaVariationNorm() == Catch::Approx(1));
    }

    SECTION("Polynomial integral")
    {
      using R3 = TinyVector<3>;

      const R3 a{1, 2, 2};
      const R3 b{4, 1, 3};
      const R3 c{2, 5, 1};

      const TriangleTransformation<3> t(a, b, c);

      auto p = [](const R3& X) {
        const double x = X[0];
        const double y = X[1];
        const double z = X[2];
        return 2 * x * x + 3 * x - 3 * y * y + y + 2 * z * z - 0.5 * z + 2;
      };

      QuadratureFormula<2> qf = QuadratureManager::instance().getTriangleFormula(GaussQuadratureDescriptor(2));

      double sum = 0;
      for (size_t i = 0; i < qf.numberOfPoints(); ++i) {
        sum += qf.weight(i) * t.areaVariationNorm() * p(t(qf.point(i)));
      }

      REQUIRE(sum == Catch::Approx(39.2534499545369));
    }
  }
}
