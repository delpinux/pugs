#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeAffectationExpressionBuilder.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_AFFECTATION_RESULT(data, variable_name, expected_value)         \
  {                                                                           \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                \
    auto ast = ASTBuilder::build(input);                                      \
                                                                              \
    ASTModulesImporter{*ast};                                                 \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                   \
                                                                              \
    ASTSymbolTableBuilder{*ast};                                              \
    ASTNodeDataTypeBuilder{*ast};                                             \
                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                      \
                                                                              \
    ASTNodeExpressionBuilder{*ast};                                           \
    ExecutionPolicy exec_policy;                                              \
    ASTExecutionStack::create();                                              \
    ast->execute(exec_policy);                                                \
    ASTExecutionStack::destroy();                                             \
                                                                              \
    auto symbol_table = ast->m_symbol_table;                                  \
                                                                              \
    using namespace TAO_PEGTL_NAMESPACE;                                      \
    position use_position{10000, 1000, 10, "fixture"};                        \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);   \
                                                                              \
    auto attributes = symbol->attributes();                                   \
    auto value      = std::get<decltype(expected_value)>(attributes.value()); \
                                                                              \
    REQUIRE(value == expected_value);                                         \
    ast->m_symbol_table->clearValues();                                       \
  }

#define CHECK_AFFECTATION_EXEC_THROWS_WITH(data, error_message)    \
  {                                                                \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};     \
    auto ast = ASTBuilder::build(input);                           \
                                                                   \
    ASTModulesImporter{*ast};                                      \
                                                                   \
    ASTSymbolTableBuilder{*ast};                                   \
    ASTNodeDataTypeBuilder{*ast};                                  \
                                                                   \
    ASTNodeDeclarationToAffectationConverter{*ast};                \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};           \
                                                                   \
    ASTNodeExpressionBuilder{*ast};                                \
    ExecutionPolicy exec_policy;                                   \
                                                                   \
    ASTExecutionStack::create();                                   \
    REQUIRE_THROWS_WITH(ast->execute(exec_policy), error_message); \
    ASTExecutionStack::destroy();                                  \
    ast->m_symbol_table->clearValues();                            \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("AffectationToTupleProcessor", "[language]")
{
  auto stringify = [](auto&& value) {
    std::ostringstream os;
    os << std::boolalpha << value;
    return os.str();
  };

  SECTION("Affectations from value")
  {
    CHECK_AFFECTATION_RESULT(R"(
let s :(R); s = 2.;
)",
                             "s", (std::vector<double>{2.}));

    CHECK_AFFECTATION_RESULT(R"(
let s :(R); s = 2;
)",
                             "s", (std::vector<double>{2}));

    CHECK_AFFECTATION_RESULT(R"(
let s :(string); s = 2.;
)",
                             "s", (std::vector<std::string>{stringify(2.)}));

    CHECK_AFFECTATION_RESULT(R"(
let x :R^3, x = [1,2,3];
let s :(string); s = x;
)",
                             "s", (std::vector<std::string>{stringify(TinyVector<3, double>{1, 2, 3})}));

    CHECK_AFFECTATION_RESULT(R"(
let s :(R^1); s = 1.3;
)",
                             "s", (std::vector<TinyVector<1>>{TinyVector<1>{1.3}}));

    CHECK_AFFECTATION_RESULT(R"(
let A :R^2x2, A = [[1,2],[3,4]];
let s :(string); s = A;
)",
                             "s", (std::vector<std::string>{stringify(TinyMatrix<2>{1, 2, 3, 4})}));

    CHECK_AFFECTATION_RESULT(R"(
let s :(R^1x1); s = 1.3;
)",
                             "s", (std::vector<TinyMatrix<1>>{TinyMatrix<1>{1.3}}));
  }

  SECTION("Affectations from list")
  {
    CHECK_AFFECTATION_RESULT(R"(
let t :(R); t = (2.,3);
)",
                             "t", (std::vector<double>{2., 3}));

    CHECK_AFFECTATION_RESULT(R"(
let s :(string); s = (2.,3);
)",
                             "s", (std::vector<std::string>{stringify(2.), stringify(3)}));

    CHECK_AFFECTATION_RESULT(R"(
let s :(string); s = (2.,3,"foo");
)",
                             "s", (std::vector<std::string>{stringify(2.), stringify(3), std::string("foo")}));

    CHECK_AFFECTATION_RESULT(R"(
let x : R^2, x = [1,2];
let s : (string); s = (2.,3, x);
)",
                             "s",
                             (std::vector<std::string>{stringify(2.), stringify(3),
                                                       stringify(TinyVector<2, double>{1, 2})}));

    CHECK_AFFECTATION_RESULT(R"(
let x : R^2, x = [1,2];
let t :(R^2); t = (x,0);
)",
                             "t", (std::vector<TinyVector<2>>{TinyVector<2>{1, 2}, TinyVector<2>{0, 0}}));

    CHECK_AFFECTATION_RESULT(R"(
let t :(R^2); t = ([1,2],0);
)",
                             "t", (std::vector<TinyVector<2>>{TinyVector<2>{1, 2}, TinyVector<2>{0, 0}}));

    CHECK_AFFECTATION_RESULT(R"(
let t :(R^2); t = (0);
)",
                             "t", (std::vector<TinyVector<2>>{TinyVector<2>{0, 0}}));

    CHECK_AFFECTATION_RESULT(R"(
let t :(R^3); t = (0);
)",
                             "t", (std::vector<TinyVector<3>>{TinyVector<3>{0, 0, 0}}));

    CHECK_AFFECTATION_RESULT(R"(
let x : R^1, x = 1;
let t :(R^1); t = (x,2);
)",
                             "t", (std::vector<TinyVector<1>>{TinyVector<1>{1}, TinyVector<1>{2}}));

    CHECK_AFFECTATION_RESULT(R"(
let A : R^2x2, A = [[1,2],[3,4]];
let s : (string); s = (2.,3, A);
)",
                             "s",
                             (std::vector<std::string>{stringify(2.), stringify(3),
                                                       stringify(TinyMatrix<2>{1, 2, 3, 4})}));

    CHECK_AFFECTATION_RESULT(R"(
let A : R^2x2, A = [[1,2],[3,4]];
let t :(R^2x2); t = (A,0);
)",
                             "t", (std::vector<TinyMatrix<2>>{TinyMatrix<2>{1, 2, 3, 4}, TinyMatrix<2>{0, 0, 0, 0}}));

    CHECK_AFFECTATION_RESULT(R"(
let t :(R^2x2); t = ([[1,2],[3,4]],0);
)",
                             "t", (std::vector<TinyMatrix<2>>{TinyMatrix<2>{1, 2, 3, 4}, TinyMatrix<2>{0, 0, 0, 0}}));

    CHECK_AFFECTATION_RESULT(R"(
let t :(R^2x2); t = (0);
)",
                             "t", (std::vector<TinyMatrix<2>>{TinyMatrix<2>{0, 0, 0, 0}}));

    CHECK_AFFECTATION_RESULT(R"(
let t :(R^3x3); t = 0;
)",
                             "t", (std::vector<TinyMatrix<3>>{TinyMatrix<3>{0, 0, 0, 0, 0, 0, 0, 0, 0}}));

    CHECK_AFFECTATION_RESULT(R"(
let x : R^1x1, x = 1;
let t :(R^1x1); t = (x,2);
)",
                             "t", (std::vector<TinyMatrix<1>>{TinyMatrix<1>{1}, TinyMatrix<1>{2}}));
  }

  SECTION("Affectations from tuple")
  {
    SECTION(" -> (B)")
    {
      CHECK_AFFECTATION_RESULT(R"(
let a:(B), a = (true, false, true);
let b:(B), b = a;
)",
                               "b", (std::vector<bool>{true, false, true}));
    }

    SECTION(" -> (N)")
    {
      CHECK_AFFECTATION_RESULT(R"(
let a:(B), a = (true, false, true);
let b:(N), b = a;
)",
                               "b", (std::vector<uint64_t>{true, false, true}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(N), a = (1, 2, 3);
let b:(N), b = a;
)",
                               "b", (std::vector<uint64_t>{1, 2, 3}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(Z), a = (5, 6, 2);
let b:(N), b = a;
)",
                               "b", (std::vector<uint64_t>{5, 6, 2}));
    }

    SECTION(" -> (Z)")
    {
      CHECK_AFFECTATION_RESULT(R"(
let a:(B), a = (true, false, true);
let b:(Z), b = a;
)",
                               "b", (std::vector<int64_t>{true, false, true}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(N), a = (1, 2, 3);
let b:(Z), b = a;
)",
                               "b", (std::vector<int64_t>{1, 2, 3}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(Z), a = (5, -6, 2);
let b:(Z), b = a;
)",
                               "b", (std::vector<int64_t>{5, -6, 2}));
    }

    SECTION(" -> (R)")
    {
      CHECK_AFFECTATION_RESULT(R"(
let a:(B), a = (true, false, true);
let b:(R), b = a;
)",
                               "b", (std::vector<double>{true, false, true}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(N), a = (1, 2, 3);
let b:(R), b = a;
)",
                               "b", (std::vector<double>{1, 2, 3}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(Z), a = (5, -6, 2);
let b:(R), b = a;
)",
                               "b", (std::vector<double>{5, -6, 2}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(R), a = (5.2, -6.3, 2.1);
let b:(R), b = a;
)",
                               "b", (std::vector<double>{5.2, -6.3, 2.1}));
    }

    SECTION(" -> (R^1)")
    {
      using R1 = TinyVector<1>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(B), a = (true, false, true);
let b:(R^1), b = a;
)",
                               "b", (std::vector<R1>{R1{true}, R1{false}, R1{true}}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(N), a = (1, 2, 3);
let b:(R^1), b = a;
)",
                               "b", (std::vector<R1>{R1{1}, R1{2}, R1{3}}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(Z), a = (5, -6, 2);
let b:(R^1), b = a;
)",
                               "b", (std::vector<R1>{R1{5}, R1{-6}, R1{2}}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(R), a = (5.2, -6.3, 2.1);
let b:(R^1), b = a;
)",
                               "b", (std::vector<R1>{R1{5.2}, R1{-6.3}, R1{2.1}}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(R^1), a = (5.2, -6.3, 2.1);
let b:(R^1), b = a;
)",
                               "b", (std::vector<R1>{R1{5.2}, R1{-6.3}, R1{2.1}}));
    }

    SECTION(" -> (R^2)")
    {
      using R2 = TinyVector<2>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(R^2), a = ([5.2, -6.3], [2.1, 0]);
let b:(R^2), b = a;
)",
                               "b", (std::vector<R2>{R2{5.2, -6.3}, R2{2.1, 0}}));
    }

    SECTION(" -> (R^3)")
    {
      using R3 = TinyVector<3>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(R^3), a = ([5.2, -6.3, 2], [2.1, 0, -1.1]);
let b:(R^3), b = a;
)",
                               "b", (std::vector<R3>{R3{5.2, -6.3, 2}, R3{2.1, 0, -1.1}}));
    }

    SECTION(" -> (R^1x1)")
    {
      using R1x1 = TinyMatrix<1>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(B), a = (true, false, true);
let b:(R^1x1), b = a;
)",
                               "b", (std::vector<R1x1>{R1x1{true}, R1x1{false}, R1x1{true}}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(N), a = (1, 2, 3);
let b:(R^1x1), b = a;
)",
                               "b", (std::vector<R1x1>{R1x1{1}, R1x1{2}, R1x1{3}}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(Z), a = (5, -6, 2);
let b:(R^1x1), b = a;
)",
                               "b", (std::vector<R1x1>{R1x1{5}, R1x1{-6}, R1x1{2}}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(R), a = (5.2, -6.3, 2.1);
let b:(R^1x1), b = a;
)",
                               "b", (std::vector<R1x1>{R1x1{5.2}, R1x1{-6.3}, R1x1{2.1}}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(R^1x1), a = (5.2, -6.3, 2.1);
let b:(R^1x1), b = a;
)",
                               "b", (std::vector<R1x1>{R1x1{5.2}, R1x1{-6.3}, R1x1{2.1}}));
    }

    SECTION(" -> (R^2x2)")
    {
      using R2x2 = TinyMatrix<2>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(R^2x2), a = ([[5.2, -6.3], [2.1, 0]], 0);
let b:(R^2x2), b = a;
)",
                               "b", (std::vector<R2x2>{R2x2{5.2, -6.3, 2.1, 0}, R2x2{zero}}));
    }

    SECTION(" -> (R^3x3)")
    {
      using R3x3 = TinyMatrix<3>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(R^3x3), a = ([[5.2, -6.3, 2], [2.1, 0, -1.1], [1,-1.2,-0.7]] , 0);
let b:(R^3x3), b = a;
)",
                               "b", (std::vector<R3x3>{R3x3{5.2, -6.3, 2, 2.1, 0, -1.1, 1, -1.2, -0.7}, R3x3{zero}}));
    }

    SECTION(" -> (string)")
    {
      CHECK_AFFECTATION_RESULT(R"(
let a:(B), a = (true, false, true);
let b:(string), b = a;
)",
                               "b", (std::vector<std::string>{stringify(true), stringify(false), stringify(true)}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(N), a = (1, 2, 3);
let b:(string), b = a;
)",
                               "b", (std::vector<std::string>{stringify(1ul), stringify(2ul), stringify(3ul)}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(Z), a = (5, -6, 2);
let b:(string), b = a;
)",
                               "b", (std::vector<std::string>{stringify(5l), stringify(-6l), stringify(2l)}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(R), a = (5.2, -6.3, 2.1);
let b:(string), b = a;
)",
                               "b",
                               (std::vector<std::string>{stringify(double{5.2}), stringify(double{-6.3}),
                                                         stringify(double{2.1})}));

      using R1 = TinyVector<1>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(R^1), a = ([5.2], [-6.3], [2.1]);
let b:(string), b = a;
)",
                               "b",
                               (std::vector<std::string>{stringify(R1{5.2}), stringify(R1{-6.3}), stringify(R1{2.1})}));

      using R2 = TinyVector<2>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(R^2), a = ([5.2, 1.2], [-6.3, 4], [2.1, -1]);
let b:(string), b = a;
)",
                               "b",
                               (std::vector<std::string>{stringify(R2{5.2, 1.2}), stringify(R2{-6.3, 4}),
                                                         stringify(R2{2.1, -1})}));

      using R3 = TinyVector<3>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(R^3), a = ([5.2, 1.2, 2.1], [-6.3, 4, 0], [2.1, -1, 3]);
let b:(string), b = a;
)",
                               "b",
                               (std::vector<std::string>{stringify(R3{5.2, 1.2, 2.1}), stringify(R3{-6.3, 4, 0}),
                                                         stringify(R3{2.1, -1, 3})}));

      using R1x1 = TinyMatrix<1>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(R^1x1), a = ([[5.2]], [[-6.3]], [[2.1]]);
let b:(string), b = a;
)",
                               "b",
                               (std::vector<std::string>{stringify(R1x1{5.2}), stringify(R1x1{-6.3}),
                                                         stringify(R1x1{2.1})}));

      using R2x2 = TinyMatrix<2>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(R^2x2), a = ([[5.2, 1.2], [-6.3, 4]], [[2.1, -1],[1,3]], 0);
let b:(string), b = a;
)",
                               "b",
                               (std::vector<std::string>{stringify(R2x2{5.2, 1.2, -6.3, 4}),
                                                         stringify(R2x2{2.1, -1, 1, 3}), stringify(R2x2{zero})}));

      using R3x3 = TinyMatrix<3>;
      CHECK_AFFECTATION_RESULT(R"(
let a:(R^3x3), a = ([[5.2, 1.2, 2.1], [-6.3, 4, 0], [2.1, -1, 3]], 0);
let b:(string), b = a;
)",
                               "b",
                               (std::vector<std::string>{stringify(R3x3{5.2, 1.2, 2.1, -6.3, 4, 0, 2.1, -1, 3}),
                                                         stringify(R3x3{zero})}));

      CHECK_AFFECTATION_RESULT(R"(
let a:(string), a = ("foo", "bar", "foobar");
let b:(string), b = a;
)",
                               "b", (std::vector<std::string>{"foo", "bar", "foobar"}));
    }
  }

  SECTION("errors")
  {
    SECTION("affect negative values to (N)")
    {
      CHECK_AFFECTATION_EXEC_THROWS_WITH(R"(
let n : (N), n = -3;
)",
                                         "trying to affect negative value (-3)");

      CHECK_AFFECTATION_EXEC_THROWS_WITH(R"(
let n : (N), n = (2,3,-6);
)",
                                         "trying to affect negative value (-6)");

      CHECK_AFFECTATION_EXEC_THROWS_WITH(R"(
let z : (Z), z = (2,-5,6);
let n : (N), n = z;
)",
                                         "trying to affect negative value (-5)");

      CHECK_AFFECTATION_EXEC_THROWS_WITH(R"(
let z : (Z), z = -3;
let n : N, n = z;
)",
                                         "trying to affect negative value (-3)");
    }

    SECTION("affect negative values to (N) in lists")
    {
      CHECK_AFFECTATION_EXEC_THROWS_WITH(R"(
let (z,n) : Z*(N), (z,n) = (-7,-4);
)",
                                         "trying to affect negative value (-4)");

      CHECK_AFFECTATION_EXEC_THROWS_WITH(R"(
let (z,n) : Z*(N), (z,n) = (-5,(2,3,-3));
)",
                                         "trying to affect negative value (-3)");

      CHECK_AFFECTATION_EXEC_THROWS_WITH(R"(
let z : (Z), z = (2,-2,6);
let (r,n) : R*(N), (r,n) = (0.4, z);
)",
                                         "trying to affect negative value ((2, -2, 6))");
    }
  }
}
