#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <algebra/TinyMatrix.hpp>

#include <analysis/CubeGaussQuadrature.hpp>
#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("CubeGaussQuadrature", "[analysis]")
{
  auto integrate = [](auto f, auto quadrature_formula) {
    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    auto value = weight_list[0] * f(point_list[0]);
    for (size_t i = 1; i < weight_list.size(); ++i) {
      value += weight_list[i] * f(point_list[i]);
    }

    return value;
  };

  auto integrate_on_brick = [](auto f, auto quadrature_formula, const std::array<TinyVector<3>, 4>& tetrahedron) {
    const auto& A = tetrahedron[0];
    const auto& B = tetrahedron[1];
    const auto& C = tetrahedron[2];
    const auto& D = tetrahedron[3];

    TinyMatrix<3> J;
    for (size_t i = 0; i < 3; ++i) {
      J(i, 0) = 0.5 * (B[i] - A[i]);
      J(i, 1) = 0.5 * (C[i] - A[i]);
      J(i, 2) = 0.5 * (D[i] - A[i]);
    }
    TinyVector s = 0.5 * (B + C + D - A);

    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    auto value = weight_list[0] * f(J * (point_list[0]) + s);
    for (size_t i = 1; i < weight_list.size(); ++i) {
      value += weight_list[i] * f(J * (point_list[i]) + s);
    }

    return det(J) * value;
  };

  auto get_order = [&integrate, &integrate_on_brick](auto f, auto quadrature_formula, const double exact_value) {
    using R3 = TinyVector<3>;

    const double int_K_hat = integrate(f, quadrature_formula);
    const double int_refined   //
      = integrate_on_brick(f, quadrature_formula, {R3{-1, -1, -1}, R3{+0, -1, -1}, R3{-1, +0, -1}, R3{-1, -1, +0}}) +
        integrate_on_brick(f, quadrature_formula, {R3{+0, -1, -1}, R3{+1, -1, -1}, R3{+0, +0, -1}, R3{+0, -1, +0}}) +
        integrate_on_brick(f, quadrature_formula, {R3{-1, +0, -1}, R3{+0, +0, -1}, R3{-1, +1, -1}, R3{-1, +0, +0}}) +
        integrate_on_brick(f, quadrature_formula, {R3{+0, +0, -1}, R3{+1, +0, -1}, R3{+0, +1, -1}, R3{+0, +0, +0}}) +
        integrate_on_brick(f, quadrature_formula, {R3{-1, -1, +0}, R3{+0, -1, +0}, R3{-1, +0, +0}, R3{-1, -1, +1}}) +
        integrate_on_brick(f, quadrature_formula, {R3{+0, -1, +0}, R3{+1, -1, +0}, R3{+0, +0, +0}, R3{+0, -1, +1}}) +
        integrate_on_brick(f, quadrature_formula, {R3{-1, +0, +0}, R3{+0, +0, +0}, R3{-1, +1, +0}, R3{-1, +0, +1}}) +
        integrate_on_brick(f, quadrature_formula, {R3{+0, +0, +0}, R3{+1, +0, +0}, R3{+0, +1, +0}, R3{+0, +0, +1}});

    return -std::log((int_refined - exact_value) / (int_K_hat - exact_value)) / std::log(2);
  };

  auto p0 = [](const TinyVector<3>&) { return 4; };
  auto p1 = [](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return 2 * x + 3 * y + z - 1;
  };
  auto p2 = [&p1](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p1(X) * (2.5 * x - 3 * y + z + 3);
  };
  auto p3 = [&p2](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p2(X) * (3 * x + 2 * y - 3 * z - 1);
  };
  auto p4 = [&p3](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p3(X) * (2 * x - 0.5 * y - 1.3 * z + 1);
  };
  auto p5 = [&p4](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p4(X) * (-0.1 * x + 1.3 * y - 3 * z + 1);
  };
  auto p6 = [&p5](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p5(X) * 7875. / 143443 * (2 * x - y + 4 * z + 1);
  };
  auto p7 = [&p6](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p6(X) * (0.7 * x - 2.7 * y + 1.3 * z - 2);
  };
  auto p8 = [&p7](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p7(X) * (0.3 * x + 1.2 * y - 0.7 * z + 0.2);
  };
  auto p9 = [&p8](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p8(X) * (-0.2 * x - 1.7 * y + 0.4 * z - 0.4);
  };
  auto p10 = [&p9](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p9(X) * (0.8 * x + 0.1 * y - 0.7 * z + 0.2);
  };
  auto p11 = [&p10](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p10(X) * (-0.6 * x - 0.5 * y + 0.3 * z - 0.1);
  };
  auto p12 = [&p11](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p11(X) * (0.4 * x - 0.7 * y - 0.6 * z + 0.7);
  };
  auto p13 = [&p12](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p12(X) * (-0.9 * x + 0.3 * y + 0.3 * z - 0.3);
  };
  auto p14 = [&p13](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p13(X) * (0.2 * x - 0.7 * y + 0.6 * z + 0.1);
  };
  auto p15 = [&p14](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p14(X) * (-0.5 * x - 0.3 * y + 0.7 * z - 0.4);
  };
  auto p16 = [&p15](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p15(X) * (0.7 * x + 0.6 * y - 0.1 * z + 0.6);
  };
  auto p17 = [&p16](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p16(X) * (-0.6 * x + 0.3 * y + 0.7 * z + 0.8);
  };
  auto p18 = [&p17](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p17(X) * (0.1 * x + 0.9 * y - 0.4 * z - 0.3);
  };
  auto p19 = [&p18](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p18(X) * (-0.8 * x - 0.3 * y + 0.9 * z + 0.8);
  };
  auto p20 = [&p19](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p19(X) * (0.3 * x - 0.7 * y - 0.8 * z + 0.7);
  };
  auto p21 = [&p20](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p20(X) * (-0.9 * x + 0.2 * y + 0.5 * z - 0.6);
  };
  auto p22 = [&p21](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p21(X) * (0.3 * x - 0.6 * y - 0.7 * z + 0.2);
  };

  SECTION("degree 0 and 1")
  {
    const QuadratureFormula<3>& l1 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(1));

    REQUIRE(l1.numberOfPoints() == 1);

    REQUIRE(integrate(p0, l1) == Catch::Approx(32));
    REQUIRE(integrate(p1, l1) == Catch::Approx(-8));
    REQUIRE(integrate(p2, l1) != Catch::Approx(-32));

    REQUIRE(get_order(p2, l1, -32) == Catch::Approx(2));
  }

  SECTION("degree 2 and 3")
  {
    const QuadratureFormula<3>& l2 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(2));
    const QuadratureFormula<3>& l3 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(3));

    REQUIRE(&l2 == &l3);

    REQUIRE(l3.numberOfPoints() == 6);

    REQUIRE(integrate(p0, l3) == Catch::Approx(32));
    REQUIRE(integrate(p1, l3) == Catch::Approx(-8));
    REQUIRE(integrate(p2, l3) == Catch::Approx(-32));
    REQUIRE(integrate(p3, l3) == Catch::Approx(108));
    REQUIRE(integrate(p4, l3) != Catch::Approx(868. / 75));

    REQUIRE(get_order(p4, l3, 868. / 75) == Catch::Approx(4));
  }

  SECTION("degree 4 and 5")
  {
    const QuadratureFormula<3>& l4 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(4));
    const QuadratureFormula<3>& l5 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(5));

    REQUIRE(&l4 == &l5);

    REQUIRE(l5.numberOfPoints() == 14);

    REQUIRE(integrate(p0, l5) == Catch::Approx(32));
    REQUIRE(integrate(p1, l5) == Catch::Approx(-8));
    REQUIRE(integrate(p2, l5) == Catch::Approx(-32));
    REQUIRE(integrate(p3, l5) == Catch::Approx(108));
    REQUIRE(integrate(p4, l5) == Catch::Approx(868. / 75));
    REQUIRE(integrate(p5, l5) == Catch::Approx(11176. / 225));
    REQUIRE(integrate(p6, l5) != Catch::Approx(-34781. / 430329));

    REQUIRE(get_order(p6, l5, -34781. / 430329) == Catch::Approx(6));
  }

  SECTION("degree 6 and 7")
  {
    const QuadratureFormula<3>& l6 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(6));
    const QuadratureFormula<3>& l7 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(7));

    REQUIRE(&l6 == &l7);

    REQUIRE(l7.numberOfPoints() == 34);

    REQUIRE(integrate(p0, l7) == Catch::Approx(32));
    REQUIRE(integrate(p1, l7) == Catch::Approx(-8));
    REQUIRE(integrate(p2, l7) == Catch::Approx(-32));
    REQUIRE(integrate(p3, l7) == Catch::Approx(108));
    REQUIRE(integrate(p4, l7) == Catch::Approx(868. / 75));
    REQUIRE(integrate(p5, l7) == Catch::Approx(11176. / 225));
    REQUIRE(integrate(p6, l7) == Catch::Approx(-34781. / 430329));
    REQUIRE(integrate(p7, l7) == Catch::Approx(-37338109. / 4303290));
    REQUIRE(integrate(p8, l7) != Catch::Approx(422437099. / 21516450));

    REQUIRE(get_order(p8, l7, 422437099. / 21516450) == Catch::Approx(8));
  }

  SECTION("degree 8 and 9")
  {
    const QuadratureFormula<3>& l8 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(8));
    const QuadratureFormula<3>& l9 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(9));

    REQUIRE(&l8 == &l9);

    REQUIRE(l9.numberOfPoints() == 58);

    REQUIRE(integrate(p0, l9) == Catch::Approx(32));
    REQUIRE(integrate(p1, l9) == Catch::Approx(-8));
    REQUIRE(integrate(p2, l9) == Catch::Approx(-32));
    REQUIRE(integrate(p3, l9) == Catch::Approx(108));
    REQUIRE(integrate(p4, l9) == Catch::Approx(868. / 75));
    REQUIRE(integrate(p5, l9) == Catch::Approx(11176. / 225));
    REQUIRE(integrate(p6, l9) == Catch::Approx(-34781. / 430329));
    REQUIRE(integrate(p7, l9) == Catch::Approx(-37338109. / 4303290));
    REQUIRE(integrate(p8, l9) == Catch::Approx(422437099. / 21516450));
    REQUIRE(integrate(p9, l9) == Catch::Approx(-7745999747. / 358607500));
    REQUIRE(integrate(p10, l9) != Catch::Approx(-564286973089. / 14792559375));

    REQUIRE(get_order(p10, l9, -564286973089. / 14792559375) == Catch::Approx(10));
  }

  SECTION("degree 10 and 11")
  {
    const QuadratureFormula<3>& l10 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(10));
    const QuadratureFormula<3>& l11 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(11));

    REQUIRE(&l10 == &l11);

    REQUIRE(l11.numberOfPoints() == 90);

    REQUIRE(integrate(p0, l11) == Catch::Approx(32));
    REQUIRE(integrate(p1, l11) == Catch::Approx(-8));
    REQUIRE(integrate(p2, l11) == Catch::Approx(-32));
    REQUIRE(integrate(p3, l11) == Catch::Approx(108));
    REQUIRE(integrate(p4, l11) == Catch::Approx(868. / 75));
    REQUIRE(integrate(p5, l11) == Catch::Approx(11176. / 225));
    REQUIRE(integrate(p6, l11) == Catch::Approx(-34781. / 430329));
    REQUIRE(integrate(p7, l11) == Catch::Approx(-37338109. / 4303290));
    REQUIRE(integrate(p8, l11) == Catch::Approx(422437099. / 21516450));
    REQUIRE(integrate(p9, l11) == Catch::Approx(-7745999747. / 358607500));
    REQUIRE(integrate(p10, l11) == Catch::Approx(-564286973089. / 14792559375));
    REQUIRE(integrate(p11, l11) == Catch::Approx(5047102242313. / 59170237500));
    REQUIRE(integrate(p12, l11) != Catch::Approx(41226980237154884. / 504796088671875));

    REQUIRE(get_order(p12, l11, 41226980237154884. / 504796088671875) == Catch::Approx(12));
  }

  SECTION("degree 12 and 13")
  {
    const QuadratureFormula<3>& l12 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(12));
    const QuadratureFormula<3>& l13 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(13));

    REQUIRE(&l12 == &l13);

    REQUIRE(l13.numberOfPoints() == 154);

    REQUIRE(integrate(p0, l13) == Catch::Approx(32));
    REQUIRE(integrate(p1, l13) == Catch::Approx(-8));
    REQUIRE(integrate(p2, l13) == Catch::Approx(-32));
    REQUIRE(integrate(p3, l13) == Catch::Approx(108));
    REQUIRE(integrate(p4, l13) == Catch::Approx(868. / 75));
    REQUIRE(integrate(p5, l13) == Catch::Approx(11176. / 225));
    REQUIRE(integrate(p6, l13) == Catch::Approx(-34781. / 430329));
    REQUIRE(integrate(p7, l13) == Catch::Approx(-37338109. / 4303290));
    REQUIRE(integrate(p8, l13) == Catch::Approx(422437099. / 21516450));
    REQUIRE(integrate(p9, l13) == Catch::Approx(-7745999747. / 358607500));
    REQUIRE(integrate(p10, l13) == Catch::Approx(-564286973089. / 14792559375));
    REQUIRE(integrate(p11, l13) == Catch::Approx(5047102242313. / 59170237500));
    REQUIRE(integrate(p12, l13) == Catch::Approx(41226980237154884. / 504796088671875));
    REQUIRE(integrate(p13, l13) == Catch::Approx(-2061220959094693133. / 26922458062500000));
    REQUIRE(integrate(p14, l13) != Catch::Approx(9658346476244058223. / 134612290312500000));

    REQUIRE(get_order(p14, l13, 9658346476244058223. / 134612290312500000) == Catch::Approx(14));
  }

  SECTION("degree 14 and 15")
  {
    const QuadratureFormula<3>& l14 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(14));
    const QuadratureFormula<3>& l15 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(15));

    REQUIRE(&l14 == &l15);

    REQUIRE(l15.numberOfPoints() == 256);

    REQUIRE(integrate(p0, l15) == Catch::Approx(32));
    REQUIRE(integrate(p1, l15) == Catch::Approx(-8));
    REQUIRE(integrate(p2, l15) == Catch::Approx(-32));
    REQUIRE(integrate(p3, l15) == Catch::Approx(108));
    REQUIRE(integrate(p4, l15) == Catch::Approx(868. / 75));
    REQUIRE(integrate(p5, l15) == Catch::Approx(11176. / 225));
    REQUIRE(integrate(p6, l15) == Catch::Approx(-34781. / 430329));
    REQUIRE(integrate(p7, l15) == Catch::Approx(-37338109. / 4303290));
    REQUIRE(integrate(p8, l15) == Catch::Approx(422437099. / 21516450));
    REQUIRE(integrate(p9, l15) == Catch::Approx(-7745999747. / 358607500));
    REQUIRE(integrate(p10, l15) == Catch::Approx(-564286973089. / 14792559375));
    REQUIRE(integrate(p11, l15) == Catch::Approx(5047102242313. / 59170237500));
    REQUIRE(integrate(p12, l15) == Catch::Approx(41226980237154884. / 504796088671875));
    REQUIRE(integrate(p13, l15) == Catch::Approx(-2061220959094693133. / 26922458062500000));
    REQUIRE(integrate(p14, l15) == Catch::Approx(9658346476244058223. / 134612290312500000));
    REQUIRE(integrate(p15, l15) == Catch::Approx(-74949066496612419191. / 673061451562500000.));
    REQUIRE(integrate(p16, l15) != Catch::Approx(-46230170725718969828017. / 228840893531250000000.));

    REQUIRE(get_order(p16, l15, -46230170725718969828017. / 228840893531250000000.) == Catch::Approx(16));
  }

  SECTION("degree 16 and 17")
  {
    const QuadratureFormula<3>& l16 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(16));
    const QuadratureFormula<3>& l17 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(17));

    REQUIRE(&l16 == &l17);

    REQUIRE(l17.numberOfPoints() == 346);

    REQUIRE(integrate(p0, l17) == Catch::Approx(32));
    REQUIRE(integrate(p1, l17) == Catch::Approx(-8));
    REQUIRE(integrate(p2, l17) == Catch::Approx(-32));
    REQUIRE(integrate(p3, l17) == Catch::Approx(108));
    REQUIRE(integrate(p4, l17) == Catch::Approx(868. / 75));
    REQUIRE(integrate(p5, l17) == Catch::Approx(11176. / 225));
    REQUIRE(integrate(p6, l17) == Catch::Approx(-34781. / 430329));
    REQUIRE(integrate(p7, l17) == Catch::Approx(-37338109. / 4303290));
    REQUIRE(integrate(p8, l17) == Catch::Approx(422437099. / 21516450));
    REQUIRE(integrate(p9, l17) == Catch::Approx(-7745999747. / 358607500));
    REQUIRE(integrate(p10, l17) == Catch::Approx(-564286973089. / 14792559375));
    REQUIRE(integrate(p11, l17) == Catch::Approx(5047102242313. / 59170237500));
    REQUIRE(integrate(p12, l17) == Catch::Approx(41226980237154884. / 504796088671875));
    REQUIRE(integrate(p13, l17) == Catch::Approx(-2061220959094693133. / 26922458062500000));
    REQUIRE(integrate(p14, l17) == Catch::Approx(9658346476244058223. / 134612290312500000));
    REQUIRE(integrate(p15, l17) == Catch::Approx(-74949066496612419191. / 673061451562500000.));
    REQUIRE(integrate(p16, l17) == Catch::Approx(-46230170725718969828017. / 228840893531250000000.));
    REQUIRE(integrate(p17, l17) == Catch::Approx(2946902633453348474221. / 190700744609375000000.));
    REQUIRE(integrate(p18, l17) != Catch::Approx(904723313909284441962799. / 47555998186962890625000.));

    REQUIRE(get_order(p18, l17, 904723313909284441962799. / 47555998186962890625000.) == Catch::Approx(18));
  }

  SECTION("degree 18 and 19")
  {
    const QuadratureFormula<3>& l18 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(18));
    const QuadratureFormula<3>& l19 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(19));

    REQUIRE(&l18 == &l19);

    REQUIRE(l19.numberOfPoints() == 454);

    REQUIRE(integrate(p0, l19) == Catch::Approx(32));
    REQUIRE(integrate(p1, l19) == Catch::Approx(-8));
    REQUIRE(integrate(p2, l19) == Catch::Approx(-32));
    REQUIRE(integrate(p3, l19) == Catch::Approx(108));
    REQUIRE(integrate(p4, l19) == Catch::Approx(868. / 75));
    REQUIRE(integrate(p5, l19) == Catch::Approx(11176. / 225));
    REQUIRE(integrate(p6, l19) == Catch::Approx(-34781. / 430329));
    REQUIRE(integrate(p7, l19) == Catch::Approx(-37338109. / 4303290));
    REQUIRE(integrate(p8, l19) == Catch::Approx(422437099. / 21516450));
    REQUIRE(integrate(p9, l19) == Catch::Approx(-7745999747. / 358607500));
    REQUIRE(integrate(p10, l19) == Catch::Approx(-564286973089. / 14792559375));
    REQUIRE(integrate(p11, l19) == Catch::Approx(5047102242313. / 59170237500));
    REQUIRE(integrate(p12, l19) == Catch::Approx(41226980237154884. / 504796088671875));
    REQUIRE(integrate(p13, l19) == Catch::Approx(-2061220959094693133. / 26922458062500000));
    REQUIRE(integrate(p14, l19) == Catch::Approx(9658346476244058223. / 134612290312500000));
    REQUIRE(integrate(p15, l19) == Catch::Approx(-74949066496612419191. / 673061451562500000.));
    REQUIRE(integrate(p16, l19) == Catch::Approx(-46230170725718969828017. / 228840893531250000000.));
    REQUIRE(integrate(p17, l19) == Catch::Approx(2946902633453348474221. / 190700744609375000000.));
    REQUIRE(integrate(p18, l19) == Catch::Approx(904723313909284441962799. / 47555998186962890625000.));
    REQUIRE(integrate(p19, l19) == Catch::Approx(-91477977618647751170958517. / 22826879129742187500000000.));
    REQUIRE(integrate(p20, l19) != Catch::Approx(-11898262429946164522483495921. / 836985568090546875000000000.));

    REQUIRE(get_order(p20, l19, -11898262429946164522483495921. / 836985568090546875000000000.) == Catch::Approx(20));
  }

  SECTION("degree 20 and 21")
  {
    const QuadratureFormula<3>& l20 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(20));
    const QuadratureFormula<3>& l21 = QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(21));

    REQUIRE(&l20 == &l21);

    REQUIRE(l21.numberOfPoints() == 580);

    REQUIRE(integrate(p0, l21) == Catch::Approx(32));
    REQUIRE(integrate(p1, l21) == Catch::Approx(-8));
    REQUIRE(integrate(p2, l21) == Catch::Approx(-32));
    REQUIRE(integrate(p3, l21) == Catch::Approx(108));
    REQUIRE(integrate(p4, l21) == Catch::Approx(868. / 75));
    REQUIRE(integrate(p5, l21) == Catch::Approx(11176. / 225));
    REQUIRE(integrate(p6, l21) == Catch::Approx(-34781. / 430329));
    REQUIRE(integrate(p7, l21) == Catch::Approx(-37338109. / 4303290));
    REQUIRE(integrate(p8, l21) == Catch::Approx(422437099. / 21516450));
    REQUIRE(integrate(p9, l21) == Catch::Approx(-7745999747. / 358607500));
    REQUIRE(integrate(p10, l21) == Catch::Approx(-564286973089. / 14792559375));
    REQUIRE(integrate(p11, l21) == Catch::Approx(5047102242313. / 59170237500));
    REQUIRE(integrate(p12, l21) == Catch::Approx(41226980237154884. / 504796088671875));
    REQUIRE(integrate(p13, l21) == Catch::Approx(-2061220959094693133. / 26922458062500000));
    REQUIRE(integrate(p14, l21) == Catch::Approx(9658346476244058223. / 134612290312500000));
    REQUIRE(integrate(p15, l21) == Catch::Approx(-74949066496612419191. / 673061451562500000.));
    REQUIRE(integrate(p16, l21) == Catch::Approx(-46230170725718969828017. / 228840893531250000000.));
    REQUIRE(integrate(p17, l21) == Catch::Approx(2946902633453348474221. / 190700744609375000000.));
    REQUIRE(integrate(p18, l21) == Catch::Approx(904723313909284441962799. / 47555998186962890625000.));
    REQUIRE(integrate(p19, l21) == Catch::Approx(-91477977618647751170958517. / 22826879129742187500000000.));
    REQUIRE(integrate(p20, l21) == Catch::Approx(-11898262429946164522483495921. / 836985568090546875000000000.));
    REQUIRE(integrate(p21, l21) == Catch::Approx(7694483338683700814691225463. / 224192562881396484375000000.));
    REQUIRE(integrate(p22, l21) != Catch::Approx(10761048146311587678467825954981. / 481266701652064453125000000000.));

    REQUIRE(get_order(p22, l21, 10761048146311587678467825954981. / 481266701652064453125000000000.) ==
            Catch::Approx(22));
  }

  SECTION("max implemented degree")
  {
    REQUIRE(QuadratureManager::instance().maxCubeDegree(QuadratureType::Gauss) == CubeGaussQuadrature::max_degree);
    REQUIRE_THROWS_WITH(QuadratureManager::instance().getCubeFormula(
                          GaussQuadratureDescriptor(CubeGaussQuadrature ::max_degree + 1)),
                        "error: Gauss quadrature formulae handle degrees up to " +
                          stringify(CubeGaussQuadrature ::max_degree) + " on cubes");
  }

  SECTION("Access functions")
  {
    const QuadratureFormula<3>& quadrature_formula =
      QuadratureManager::instance().getCubeFormula(GaussQuadratureDescriptor(7));

    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    REQUIRE(point_list.size() == quadrature_formula.numberOfPoints());
    REQUIRE(weight_list.size() == quadrature_formula.numberOfPoints());

    for (size_t i = 0; i < quadrature_formula.numberOfPoints(); ++i) {
      REQUIRE(&point_list[i] == &quadrature_formula.point(i));
      REQUIRE(&weight_list[i] == &quadrature_formula.weight(i));
    }
  }
}
