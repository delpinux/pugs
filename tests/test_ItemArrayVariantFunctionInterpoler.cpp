#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeFunctionEvaluationExpressionBuilder.hpp>
#include <language/ast/ASTNodeFunctionExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/PugsFunctionAdapter.hpp>
#include <language/utils/SymbolTable.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>

#include <language/utils/ItemArrayVariantFunctionInterpoler.hpp>

#include <pegtl/string_input.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ItemArrayVariantFunctionInterpoler", "[scheme]")
{
  const bool stack_details = ASTNode::getStackDetails();
  ASTNode::setStackDetails(false);

  auto same_item_array = [](auto f, auto g) -> bool {
    using ItemIdType = typename decltype(f)::index_type;
    if (f.sizeOfArrays() != g.sizeOfArrays()) {
      return false;
    }

    for (ItemIdType item_id = 0; item_id < f.numberOfItems(); ++item_id) {
      for (size_t i = 0; i < f.sizeOfArrays(); ++i) {
        if (f[item_id][i] != g[item_id][i]) {
          return false;
        }
      }
    }

    return true;
  };

  SECTION("1D")
  {
    constexpr size_t Dimension = 1;

    std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_1d_v = named_mesh.mesh();
        auto mesh_1d   = mesh_1d_v->get<Mesh<1>>();

        auto xj = MeshDataManager::instance().getMeshData(*mesh_1d).xj();
        auto xr = mesh_1d->xr();

        std::string_view data = R"(
import math;
let B_scalar_non_linear1_1d: R^1 -> B, x -> (exp(2 * x[0]) + 3 > 4);
let B_scalar_non_linear2_1d: R^1 -> B, x -> (exp(2 * x[0]) + 3 < 4);

let N_scalar_non_linear1_1d: R^1 -> N, x -> floor(3 * x[0] * x[0] + 2);
let N_scalar_non_linear2_1d: R^1 -> N, x -> floor(2 * x[0] * x[0]);

let Z_scalar_non_linear1_1d: R^1 -> Z, x -> floor(exp(2 * x[0]) - 1);
let Z_scalar_non_linear2_1d: R^1 -> Z, x -> floor(cos(2 * x[0]) + 0.5);

let R_scalar_non_linear1_1d: R^1 -> R, x -> 2 * exp(x[0]) + 3;
let R_scalar_non_linear2_1d: R^1 -> R, x -> 2 * sin(x[0]) + 1;
let R_scalar_non_linear3_1d: R^1 -> R, x -> x[0] * sin(x[0]);

let R1_non_linear1_1d: R^1 -> R^1, x -> 2 * exp(x[0]);
let R1_non_linear2_1d: R^1 -> R^1, x -> 2 * exp(x[0])*x[0];

let R2_non_linear_1d: R^1 -> R^2, x -> [2 * exp(x[0]), -3*x[0]];

let R3_non_linear_1d: R^1 -> R^3, x -> [2 * exp(x[0]) + 3, x[0] - 2, 3];

let R1x1_non_linear_1d: R^1 -> R^1x1, x -> (2 * exp(x[0]) * sin(x[0]) + 3);

let R2x2_non_linear_1d: R^1 -> R^2x2, x -> [[2 * exp(x[0]) * sin(x[0]) + 3, sin(x[0] - 2 * x[0])], [3, x[0] * x[0]]];

let R3x3_non_linear_1d: R^1 -> R^3x3, x -> [[2 * exp(x[0]) * sin(x[0]) + 3, sin(x[0] - 2 * x[0]), 3], [x[0] * x[0], -4*x[0], 2*x[0]+1], [3, -6*x[0], exp(x[0])]];
)";
        TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};

        auto ast = ASTBuilder::build(input);

        ASTModulesImporter{*ast};
        ASTNodeTypeCleaner<language::import_instruction>{*ast};

        ASTSymbolTableBuilder{*ast};
        ASTNodeDataTypeBuilder{*ast};

        ASTNodeTypeCleaner<language::var_declaration>{*ast};
        ASTNodeTypeCleaner<language::fct_declaration>{*ast};
        ASTNodeExpressionBuilder{*ast};

        std::shared_ptr<SymbolTable> symbol_table = ast->m_symbol_table;

        // ensure that variables are declared at this point
        TAO_PEGTL_NAMESPACE::position position{data.size(), 1, 1, "fixture"};

        SECTION("B_scalar_non_linear_1d")
        {
          auto [i_symbol_f1, found_f1] = symbol_table->find("B_scalar_non_linear1_1d", position);
          REQUIRE(found_f1);
          REQUIRE(i_symbol_f1->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function1_symbol_id(std::get<uint64_t>(i_symbol_f1->attributes().value()), symbol_table);

          auto [i_symbol_f2, found_f2] = symbol_table->find("B_scalar_non_linear2_1d", position);
          REQUIRE(found_f2);
          REQUIRE(i_symbol_f2->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function2_symbol_id(std::get<uint64_t>(i_symbol_f2->attributes().value()), symbol_table);

          CellArray<bool> cell_array{mesh_1d->connectivity(), 2};
          parallel_for(
            cell_array.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
              const TinyVector<Dimension>& x = xj[cell_id];
              cell_array[cell_id][0]         = std::exp(2 * x[0]) + 3 > 4;
              cell_array[cell_id][1]         = std::exp(2 * x[0]) + 3 < 4;
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_1d_v, ItemType::cell,
                                                        {function1_symbol_id, function2_symbol_id});
          std::shared_ptr item_data_variant = interpoler.interpolate();

          REQUIRE(same_item_array(cell_array, item_data_variant->get<CellArray<bool>>()));
        }

        SECTION("N_scalar_non_linear_1d")
        {
          auto [i_symbol_f1, found_f1] = symbol_table->find("N_scalar_non_linear1_1d", position);
          REQUIRE(found_f1);
          REQUIRE(i_symbol_f1->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function1_symbol_id(std::get<uint64_t>(i_symbol_f1->attributes().value()), symbol_table);

          auto [i_symbol_f2, found_f2] = symbol_table->find("N_scalar_non_linear2_1d", position);
          REQUIRE(found_f2);
          REQUIRE(i_symbol_f2->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function2_symbol_id(std::get<uint64_t>(i_symbol_f2->attributes().value()), symbol_table);

          NodeArray<uint64_t> node_array{mesh_1d->connectivity(), 2};
          parallel_for(
            node_array.numberOfItems(), PUGS_LAMBDA(const NodeId node_id) {
              const TinyVector<Dimension>& x = xr[node_id];
              node_array[node_id][0]         = std::floor(3 * x[0] * x[0] + 2);
              node_array[node_id][1]         = std::floor(2 * x[0] * x[0]);
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_1d_v, ItemType::node,
                                                        {function1_symbol_id, function2_symbol_id});
          std::shared_ptr item_data_variant = interpoler.interpolate();

          REQUIRE(same_item_array(node_array, item_data_variant->get<NodeArray<uint64_t>>()));
        }

        SECTION("Z_scalar_non_linear_1d")
        {
          auto [i_symbol_f1, found_f1] = symbol_table->find("Z_scalar_non_linear1_1d", position);
          REQUIRE(found_f1);
          REQUIRE(i_symbol_f1->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function1_symbol_id(std::get<uint64_t>(i_symbol_f1->attributes().value()), symbol_table);

          auto [i_symbol_f2, found_f2] = symbol_table->find("Z_scalar_non_linear2_1d", position);
          REQUIRE(found_f2);
          REQUIRE(i_symbol_f2->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function2_symbol_id(std::get<uint64_t>(i_symbol_f2->attributes().value()), symbol_table);

          CellArray<int64_t> cell_array{mesh_1d->connectivity(), 2};
          parallel_for(
            cell_array.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
              const TinyVector<Dimension>& x = xj[cell_id];
              cell_array[cell_id][0]         = std::floor(std::exp(2 * x[0]) - 1);
              cell_array[cell_id][1]         = std::floor(cos(2 * x[0]) + 0.5);
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_1d_v, ItemType::cell,
                                                        {function1_symbol_id, function2_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(cell_array, item_array_variant->get<CellArray<int64_t>>()));
        }

        SECTION("R_scalar_non_linear_1d")
        {
          auto [i_symbol_f1, found_f1] = symbol_table->find("R_scalar_non_linear1_1d", position);
          REQUIRE(found_f1);
          REQUIRE(i_symbol_f1->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function1_symbol_id(std::get<uint64_t>(i_symbol_f1->attributes().value()), symbol_table);

          auto [i_symbol_f2, found_f2] = symbol_table->find("R_scalar_non_linear2_1d", position);
          REQUIRE(found_f2);
          REQUIRE(i_symbol_f2->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function2_symbol_id(std::get<uint64_t>(i_symbol_f2->attributes().value()), symbol_table);

          auto [i_symbol_f3, found_f3] = symbol_table->find("R_scalar_non_linear3_1d", position);
          REQUIRE(found_f3);
          REQUIRE(i_symbol_f3->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function3_symbol_id(std::get<uint64_t>(i_symbol_f3->attributes().value()), symbol_table);

          CellArray<double> cell_array{mesh_1d->connectivity(), 3};
          parallel_for(
            cell_array.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
              const TinyVector<Dimension>& x = xj[cell_id];

              cell_array[cell_id][0] = 2 * std::exp(x[0]) + 3;
              cell_array[cell_id][1] = 2 * std::sin(x[0]) + 1;
              cell_array[cell_id][2] = x[0] * std::sin(x[0]);
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_1d_v, ItemType::cell,
                                                        {function1_symbol_id, function2_symbol_id,
                                                         function3_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(cell_array, item_array_variant->get<CellArray<double>>()));
        }

        SECTION("R1_non_linear_1d")
        {
          using DataType = TinyVector<1>;

          auto [i_symbol_f1, found_f1] = symbol_table->find("R1_non_linear1_1d", position);
          REQUIRE(found_f1);
          REQUIRE(i_symbol_f1->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function1_symbol_id(std::get<uint64_t>(i_symbol_f1->attributes().value()), symbol_table);

          auto [i_symbol_f2, found_f2] = symbol_table->find("R1_non_linear2_1d", position);
          REQUIRE(found_f2);
          REQUIRE(i_symbol_f2->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function2_symbol_id(std::get<uint64_t>(i_symbol_f2->attributes().value()), symbol_table);

          CellArray<DataType> cell_array{mesh_1d->connectivity(), 2};
          parallel_for(
            cell_array.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
              const TinyVector<Dimension>& x = xj[cell_id];

              cell_array[cell_id][0] = DataType{2 * std::exp(x[0])};
              cell_array[cell_id][1] = DataType{2 * std::exp(x[0]) * x[0]};
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_1d_v, ItemType::cell,
                                                        {function1_symbol_id, function2_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(cell_array, item_array_variant->get<CellArray<DataType>>()));
        }

        SECTION("R2_non_linear_1d")
        {
          using DataType = TinyVector<2>;

          auto [i_symbol, found] = symbol_table->find("R2_non_linear_1d", position);
          REQUIRE(found);
          REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

          CellArray<DataType> cell_array{mesh_1d->connectivity(), 1};
          parallel_for(
            cell_array.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
              const TinyVector<Dimension>& x = xj[cell_id];

              cell_array[cell_id][0] = DataType{2 * std::exp(x[0]), -3 * x[0]};
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_1d_v, ItemType::cell, {function_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(cell_array, item_array_variant->get<CellArray<DataType>>()));
        }

        SECTION("R3_non_linear_1d")
        {
          using DataType = TinyVector<3>;

          auto [i_symbol, found] = symbol_table->find("R3_non_linear_1d", position);
          REQUIRE(found);
          REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

          CellArray<DataType> cell_array{mesh_1d->connectivity(), 1};
          parallel_for(
            cell_array.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
              const TinyVector<Dimension>& x = xj[cell_id];
              cell_array[cell_id][0]         = DataType{2 * std::exp(x[0]) + 3, x[0] - 2, 3};
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_1d_v, ItemType::cell, {function_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(cell_array, item_array_variant->get<CellArray<DataType>>()));
        }

        SECTION("R1x1_non_linear_1d")
        {
          using DataType = TinyMatrix<1>;

          auto [i_symbol, found] = symbol_table->find("R1x1_non_linear_1d", position);
          REQUIRE(found);
          REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

          CellArray<DataType> cell_array{mesh_1d->connectivity(), 1};
          parallel_for(
            cell_array.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
              const TinyVector<Dimension>& x = xj[cell_id];
              cell_array[cell_id][0]         = DataType{2 * std::exp(x[0]) * std::sin(x[0]) + 3};
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_1d_v, ItemType::cell, {function_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(cell_array, item_array_variant->get<CellArray<DataType>>()));
        }

        SECTION("R2x2_non_linear_1d")
        {
          using DataType = TinyMatrix<2>;

          auto [i_symbol, found] = symbol_table->find("R2x2_non_linear_1d", position);
          REQUIRE(found);
          REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

          CellArray<DataType> cell_array{mesh_1d->connectivity(), 1};
          parallel_for(
            cell_array.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
              const TinyVector<Dimension>& x = xj[cell_id];
              cell_array[cell_id][0] =
                DataType{2 * std::exp(x[0]) * std::sin(x[0]) + 3, std::sin(x[0] - 2 * x[0]), 3, x[0] * x[0]};
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_1d_v, ItemType::cell, {function_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(cell_array, item_array_variant->get<CellArray<DataType>>()));
        }

        SECTION("R3x3_non_linear_1d")
        {
          using DataType = TinyMatrix<3>;

          auto [i_symbol, found] = symbol_table->find("R3x3_non_linear_1d", position);
          REQUIRE(found);
          REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

          CellArray<DataType> cell_array{mesh_1d->connectivity(), 1};
          parallel_for(
            cell_array.numberOfItems(), PUGS_LAMBDA(const CellId cell_id) {
              const TinyVector<Dimension>& x = xj[cell_id];

              cell_array[cell_id][0] = DataType{2 * exp(x[0]) * std::sin(x[0]) + 3,
                                                std::sin(x[0] - 2 * x[0]),
                                                3,
                                                x[0] * x[0],
                                                -4 * x[0],
                                                2 * x[0] + 1,
                                                3,
                                                -6 * x[0],
                                                std::exp(x[0])};
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_1d_v, ItemType::cell, {function_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(cell_array, item_array_variant->get<CellArray<DataType>>()));
        }

        SECTION("different types")
        {
          auto [i_symbol_f1, found_f1] = symbol_table->find("R_scalar_non_linear1_1d", position);
          REQUIRE(found_f1);
          REQUIRE(i_symbol_f1->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function1_symbol_id(std::get<uint64_t>(i_symbol_f1->attributes().value()), symbol_table);

          auto [i_symbol_f2, found_f2] = symbol_table->find("Z_scalar_non_linear2_1d", position);
          REQUIRE(found_f2);
          REQUIRE(i_symbol_f2->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function2_symbol_id(std::get<uint64_t>(i_symbol_f2->attributes().value()), symbol_table);

          auto [i_symbol_f3, found_f3] = symbol_table->find("R_scalar_non_linear3_1d", position);
          REQUIRE(found_f3);
          REQUIRE(i_symbol_f3->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function3_symbol_id(std::get<uint64_t>(i_symbol_f3->attributes().value()), symbol_table);

          ItemArrayVariantFunctionInterpoler interpoler(mesh_1d_v, ItemType::cell,
                                                        {function1_symbol_id, function2_symbol_id,
                                                         function3_symbol_id});

          REQUIRE_THROWS_WITH(interpoler.interpolate(), "error: functions must have the same type");
        }
        ast->m_symbol_table->clearValues();
      }
    }
  }

  SECTION("2D")
  {
    constexpr size_t Dimension = 2;

    std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_2d_v = named_mesh.mesh();
        auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

        auto xl = MeshDataManager::instance().getMeshData(*mesh_2d).xl();

        std::string_view data = R"(
import math;
let B_scalar_non_linear1_2d: R^2 -> B, x -> (exp(2 * x[0])< 2*x[1]);
let B_scalar_non_linear2_2d: R^2 -> B, x -> (sin(2 * x[0])< x[1]);

let R2_non_linear_2d: R^2 -> R^2, x -> [2 * exp(x[0]), -3*x[1]];

let R3x3_non_linear_2d: R^2 -> R^3x3, x -> [[2 * exp(x[0]) * sin(x[1]) + 3, sin(x[1] - 2 * x[0]), 3],
                                           [x[1] * x[0], -4*x[1], 2*x[0]+1],
                                           [3, -6*x[0], exp(x[1])]];
  )";
        TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};

        auto ast = ASTBuilder::build(input);

        ASTModulesImporter{*ast};
        ASTNodeTypeCleaner<language::import_instruction>{*ast};

        ASTSymbolTableBuilder{*ast};
        ASTNodeDataTypeBuilder{*ast};

        ASTNodeTypeCleaner<language::var_declaration>{*ast};
        ASTNodeTypeCleaner<language::fct_declaration>{*ast};
        ASTNodeExpressionBuilder{*ast};

        std::shared_ptr<SymbolTable> symbol_table = ast->m_symbol_table;

        // ensure that variables are declared at this point
        TAO_PEGTL_NAMESPACE::position position{data.size(), 1, 1, "fixture"};

        SECTION("B_scalar_non_linear_2d")
        {
          using DataType = bool;

          auto [i_symbol_f1, found_f1] = symbol_table->find("B_scalar_non_linear1_2d", position);
          REQUIRE(found_f1);
          REQUIRE(i_symbol_f1->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function1_symbol_id(std::get<uint64_t>(i_symbol_f1->attributes().value()), symbol_table);

          auto [i_symbol_f2, found_f2] = symbol_table->find("B_scalar_non_linear2_2d", position);
          REQUIRE(found_f2);
          REQUIRE(i_symbol_f2->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function2_symbol_id(std::get<uint64_t>(i_symbol_f2->attributes().value()), symbol_table);

          FaceArray<DataType> face_array{mesh_2d->connectivity(), 2};
          parallel_for(
            face_array.numberOfItems(), PUGS_LAMBDA(const FaceId face_id) {
              const TinyVector<Dimension>& x = xl[face_id];
              face_array[face_id][0]         = std::exp(2 * x[0]) < 2 * x[1];
              face_array[face_id][1]         = std::sin(2 * x[0]) < x[1];
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_2d_v, ItemType::face,
                                                        {function1_symbol_id, function2_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(face_array, item_array_variant->get<FaceArray<DataType>>()));
        }

        SECTION("R2_non_linear_2d")
        {
          using DataType = TinyVector<2>;

          auto [i_symbol, found] = symbol_table->find("R2_non_linear_2d", position);
          REQUIRE(found);
          REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

          FaceArray<DataType> face_array{mesh_2d->connectivity(), 1};
          parallel_for(
            face_array.numberOfItems(), PUGS_LAMBDA(const FaceId face_id) {
              const TinyVector<Dimension>& x = xl[face_id];

              face_array[face_id][0] = DataType{2 * std::exp(x[0]), -3 * x[1]};
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_2d_v, ItemType::face, {function_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(face_array, item_array_variant->get<FaceArray<DataType>>()));
        }

        SECTION("R3x3_non_linear_2d")
        {
          using DataType = TinyMatrix<3>;

          auto [i_symbol, found] = symbol_table->find("R3x3_non_linear_2d", position);
          REQUIRE(found);
          REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

          FaceArray<DataType> face_array{mesh_2d->connectivity(), 1};
          parallel_for(
            face_array.numberOfItems(), PUGS_LAMBDA(const FaceId face_id) {
              const TinyVector<Dimension>& x = xl[face_id];

              face_array[face_id][0] = DataType{2 * std::exp(x[0]) * std::sin(x[1]) + 3,
                                                std::sin(x[1] - 2 * x[0]),
                                                3,
                                                x[1] * x[0],
                                                -4 * x[1],
                                                2 * x[0] + 1,
                                                3,
                                                -6 * x[0],
                                                std::exp(x[1])};
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_2d_v, ItemType::face, {function_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(face_array, item_array_variant->get<FaceArray<DataType>>()));
        }
        ast->m_symbol_table->clearValues();
      }
    }
  }

  SECTION("3D")
  {
    constexpr size_t Dimension = 3;

    std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_3d_v = named_mesh.mesh();
        auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

        auto xe = MeshDataManager::instance().getMeshData(*mesh_3d).xe();

        std::string_view data = R"(
import math;
let R_scalar_non_linear1_3d: R^3 -> R, x -> 2 * exp(x[0]+x[2]) + 3 * x[1];
let R_scalar_non_linear2_3d: R^3 -> R, x -> 3 * sin(x[0]+x[2]) + 2 * x[1];

let R3_non_linear_3d: R^3 -> R^3, x -> [2 * exp(x[0]) + 3, x[1] - 2, 3 * x[2]];
let R2x2_non_linear_3d: R^3 -> R^2x2,
                          x -> [[2 * exp(x[0]) * sin(x[1]) + 3, sin(x[2] - 2 * x[0])],
                                [3, x[1] * x[0] - x[2]]];
  )";
        TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};

        auto ast = ASTBuilder::build(input);

        ASTModulesImporter{*ast};
        ASTNodeTypeCleaner<language::import_instruction>{*ast};

        ASTSymbolTableBuilder{*ast};
        ASTNodeDataTypeBuilder{*ast};

        ASTNodeTypeCleaner<language::var_declaration>{*ast};
        ASTNodeTypeCleaner<language::fct_declaration>{*ast};
        ASTNodeExpressionBuilder{*ast};

        std::shared_ptr<SymbolTable> symbol_table = ast->m_symbol_table;

        // ensure that variables are declared at this point
        TAO_PEGTL_NAMESPACE::position position{data.size(), 1, 1, "fixture"};

        SECTION("R_scalar_non_linear_3d")
        {
          auto [i_symbol_f1, found_f1] = symbol_table->find("R_scalar_non_linear1_3d", position);
          REQUIRE(found_f1);
          REQUIRE(i_symbol_f1->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function1_symbol_id(std::get<uint64_t>(i_symbol_f1->attributes().value()), symbol_table);

          auto [i_symbol_f2, found_f2] = symbol_table->find("R_scalar_non_linear2_3d", position);
          REQUIRE(found_f2);
          REQUIRE(i_symbol_f2->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function2_symbol_id(std::get<uint64_t>(i_symbol_f2->attributes().value()), symbol_table);

          EdgeArray<double> edge_array{mesh_3d->connectivity(), 2};
          parallel_for(
            edge_array.numberOfItems(), PUGS_LAMBDA(const EdgeId edge_id) {
              const TinyVector<Dimension>& x = xe[edge_id];

              edge_array[edge_id][0] = 2 * std::exp(x[0] + x[2]) + 3 * x[1];
              edge_array[edge_id][1] = 3 * std::sin(x[0] + x[2]) + 2 * x[1];
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_3d_v, ItemType::edge,
                                                        {function1_symbol_id, function2_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(edge_array, item_array_variant->get<EdgeArray<double>>()));
        }

        SECTION("R3_non_linear_3d")
        {
          using DataType = TinyVector<3>;

          auto [i_symbol, found] = symbol_table->find("R3_non_linear_3d", position);
          REQUIRE(found);
          REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

          EdgeArray<DataType> edge_array{mesh_3d->connectivity(), 1};
          parallel_for(
            edge_array.numberOfItems(), PUGS_LAMBDA(const EdgeId edge_id) {
              const TinyVector<Dimension>& x = xe[edge_id];

              edge_array[edge_id][0] = DataType{2 * std::exp(x[0]) + 3, x[1] - 2, 3 * x[2]};
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_3d_v, ItemType::edge, {function_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(edge_array, item_array_variant->get<EdgeArray<DataType>>()));
        }

        SECTION("R2x2_non_linear_3d")
        {
          using DataType = TinyMatrix<2>;

          auto [i_symbol, found] = symbol_table->find("R2x2_non_linear_3d", position);
          REQUIRE(found);
          REQUIRE(i_symbol->attributes().dataType() == ASTNodeDataType::function_t);

          FunctionSymbolId function_symbol_id(std::get<uint64_t>(i_symbol->attributes().value()), symbol_table);

          EdgeArray<DataType> edge_array{mesh_3d->connectivity(), 1};
          parallel_for(
            edge_array.numberOfItems(), PUGS_LAMBDA(const EdgeId edge_id) {
              const TinyVector<Dimension>& x = xe[edge_id];
              edge_array[edge_id][0] =
                DataType{2 * std::exp(x[0]) * std::sin(x[1]) + 3, std::sin(x[2] - 2 * x[0]), 3, x[1] * x[0] - x[2]};
            });

          ItemArrayVariantFunctionInterpoler interpoler(mesh_3d_v, ItemType::edge, {function_symbol_id});
          std::shared_ptr item_array_variant = interpoler.interpolate();

          REQUIRE(same_item_array(edge_array, item_array_variant->get<EdgeArray<DataType>>()));
        }
        ast->m_symbol_table->clearValues();
      }
    }
  }

  ASTNode::setStackDetails(stack_details);
}
