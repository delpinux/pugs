#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeEmptyBlockCleaner.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#define CHECK_AST(data, expected_output)                                                            \
  {                                                                                                 \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);                  \
    static_assert((std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view>) or    \
                  (std::is_same_v<std::decay_t<decltype(expected_output)>, std::string>));          \
                                                                                                    \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                      \
    auto ast = ASTBuilder::build(input);                                                            \
                                                                                                    \
    ASTModulesImporter{*ast};                                                                       \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                         \
                                                                                                    \
    ASTSymbolTableBuilder{*ast};                                                                    \
    ASTNodeDataTypeBuilder{*ast};                                                                   \
                                                                                                    \
    ASTNodeDeclarationToAffectationConverter{*ast};                                                 \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                            \
                                                                                                    \
    ASTNodeEmptyBlockCleaner{*ast};                                                                 \
                                                                                                    \
    ASTNodeExpressionBuilder{*ast};                                                                 \
    std::stringstream ast_output;                                                                   \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::exec_type}}; \
                                                                                                    \
    REQUIRE(ast_output.str() == expected_output);                                                   \
    ast->m_symbol_table->clearValues();                                                             \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeEmptyBlockCleaner", "[language]")
{
  SECTION("empty file")
  {
    std::string_view data = R"(
)";

    std::string_view result = R"(
(root:ASTNodeListProcessor)
)";

    CHECK_AST(data, result);
  }

  SECTION("keep non-empty block")
  {
    std::string_view data = R"(
{
  let x:R, x = 3;
}
)";

    std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::block:ASTNodeListProcessor)
     `-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
         +-(language::name:x:NameProcessor)
         `-(language::integer:3:ValueProcessor)
)";

    CHECK_AST(data, result);
  }

  SECTION("remove empty block")
  {
    std::string_view data = R"(
{
  let x:R;
}
)";

    std::string_view result = R"(
(root:ASTNodeListProcessor)
)";

    CHECK_AST(data, result);
  }

  SECTION("remove nested empty blocks")
  {
    std::string_view data = R"(
{
  let x:R;
  {
    let y :R;
  }
  {
    let z: R;
    {
      let w : R;
    }
  }
}
)";

    std::string_view result = R"(
(root:ASTNodeListProcessor)
)";

    CHECK_AST(data, result);
  }

  SECTION("remove nested empty blocks")
  {
    std::string_view data = R"(
{
  let x:R;
  {
    let y:R;
  }
  {
    let z:R;
    {
      let w:R, w = 4;
    }
  }
}
)";

    std::string_view result = R"(
(root:ASTNodeListProcessor)
 `-(language::block:ASTNodeListProcessor)
     `-(language::block:ASTNodeListProcessor)
         `-(language::block:ASTNodeListProcessor)
             `-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
                 +-(language::name:w:NameProcessor)
                 `-(language::integer:4:ValueProcessor)
)";

    CHECK_AST(data, result);
  }
}
