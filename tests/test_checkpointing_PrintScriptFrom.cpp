#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/HighFivePugsUtils.hpp>
#include <utils/Messenger.hpp>
#include <utils/checkpointing/PrintScriptFrom.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_PrintScriptFrom", "[utils/checkpointing]")
{
#ifdef PUGS_HAS_HDF5

  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    const std::string data_file0 = R"(Un tiens vaut mieux que deux tu l'auras,
Un tiens vaut mieux que deux tu l'auras,...)";
    const std::string data_file1 = R"(All work and no play makes Jack a dull boy,
All work and no play makes Jack a dull boy,...)";
    const std::string data_file2 = R"(solo trabajo y nada de juego hacen de Jack un chico aburrido,
solo trabajo y nada de juego hacen de Jack un chico aburrido,...)";

    {
      HighFive::FileAccessProps fapl;
      fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
      fapl.add(HighFive::MPIOCollectiveMetadata{});
      HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

      file.createGroup("/checkpoint_0").createAttribute("data.pgs", data_file0);
      file.createGroup("/checkpoint_1").createAttribute("data.pgs", data_file1);
      file.createGroup("/checkpoint_2").createAttribute("data.pgs", data_file2);
    }

    {
      std::ostringstream os;
      printScriptFrom(filename, 0, os);
      REQUIRE(os.str() == data_file0);
    }

    {
      std::ostringstream os;
      printScriptFrom(filename, 1, os);
      REQUIRE(os.str() == data_file1);
    }

    {
      std::ostringstream os;
      printScriptFrom(filename, 2, os);
      REQUIRE(os.str() == data_file2);
    }

    {
      std::ostringstream error_msg;
      error_msg << "error: cannot find checkpoint " << 12 << " in " << filename;
      REQUIRE_THROWS_WITH(printScriptFrom(filename, 12), error_msg.str());
    }

    {
      const std::string malformed_filename = path / "malformed.h5";

      {
        HighFive::FileAccessProps fapl;
        fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
        fapl.add(HighFive::MPIOCollectiveMetadata{});
        HighFive::File file = HighFive::File(malformed_filename, HighFive::File::Truncate, fapl);

        file.createGroup("/checkpoint_0");
      }
      REQUIRE_THROWS_WITH(printScriptFrom(malformed_filename, 0),
                          "error: Unable to open the attribute \"data.pgs\": (Attribute) Object not found");
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }

#else   // PUGS_HAS_HDF5

  if (parallel::rank() == 0) {
    std::cerr.setstate(std::ios::badbit);
  }

  std::ostringstream os;
  REQUIRE_NOTHROW(printScriptFrom("foo.h5", 0, os));

  if (parallel::rank() == 0) {
    std::cerr.clear();
  }

  REQUIRE(os.str() == "");

#endif   // PUGS_HAS_HDF5
}
