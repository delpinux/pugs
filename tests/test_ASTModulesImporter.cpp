#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/utils/ASTExecutionInfo.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <language/utils/SymbolTable.hpp>

#include <pegtl/string_input.hpp>

inline void
test_ASTExecutionInfo(const ASTNode& root_node, const ModuleRepository& module_repository)
{
  ASTExecutionInfo execution_info{root_node, module_repository};
  REQUIRE(&root_node == &execution_info.rootNode());
  REQUIRE(&module_repository == &execution_info.moduleRepository());

  REQUIRE(&ASTExecutionInfo::getInstance() == &execution_info);
}

#define CHECK_AST(data, expected_output)                                                            \
  {                                                                                                 \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);                  \
    static_assert(std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view>);       \
                                                                                                    \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                      \
    auto ast = ASTBuilder::build(input);                                                            \
                                                                                                    \
    ASTModulesImporter importer{*ast};                                                              \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                         \
                                                                                                    \
    ASTNodeExpressionBuilder{*ast};                                                                 \
    const auto& module_repository = importer.moduleRepository();                                    \
    test_ASTExecutionInfo(*ast, module_repository);                                                 \
                                                                                                    \
    ExecutionPolicy exec_policy;                                                                    \
    ASTExecutionStack::create();                                                                    \
    ast->execute(exec_policy);                                                                      \
    ASTExecutionStack::destroy();                                                                   \
                                                                                                    \
    std::stringstream ast_output;                                                                   \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::data_type}}; \
                                                                                                    \
    REQUIRE(ast_output.str() == expected_output);                                                   \
    ast->m_symbol_table->clearValues();                                                             \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTModulesImporter", "[language]")
{
  SECTION("no module")
  {
    std::string_view data = R"(
)";

    std::string_view result = R"(
(root:undefined)
)";

    CHECK_AST(data, result);
  }

  SECTION("module instruction removal")
  {
    std::string_view data = R"(
import math;
)";

    std::string_view result = R"(
(root:undefined)
)";

    CHECK_AST(data, result);
  }

  SECTION("module multiple import")
  {
    std::string_view data = R"(
import math;
import math;
)";

    std::string_view result = R"(
(root:undefined)
)";

    CHECK_AST(data, result);
  }

  SECTION("error")
  {
    SECTION("unknown module")
    {
      std::string_view data = R"(
import unknown_module;
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);

      REQUIRE_THROWS_AS(ASTModulesImporter{*ast}, ParseError);
      ast->m_symbol_table->clearValues();
    }

    SECTION("symbol already defined (same builtin function)")
    {
      std::string_view data = R"(
import math;
)";

      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
      auto ast = ASTBuilder::build(input);

      ast->m_symbol_table->add("sin:R", ast->begin());

      REQUIRE_THROWS_AS(ASTModulesImporter{*ast}, ParseError);
      ast->m_symbol_table->clearValues();
    }
  }
}
