#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/SourceLocation.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("SourceLocation", "[utils]")
{
  SECTION("provided")
  {
    SourceLocation source_location("filename", 3, 2, "function_name");

    REQUIRE(source_location.filename() == "filename");
    REQUIRE(source_location.line() == 3);
    REQUIRE(source_location.column() == 2);
    REQUIRE(source_location.function() == "function_name");
  }

  SECTION("from std::source_location")
  {
    auto std_source_location = std::experimental::source_location::current();
    SourceLocation source_location(std_source_location);

    REQUIRE(source_location.filename() == std_source_location.file_name());
    REQUIRE(source_location.line() == std_source_location.line());
    REQUIRE(source_location.column() == std_source_location.column());
    REQUIRE(source_location.function() == std_source_location.function_name());
  }
}
