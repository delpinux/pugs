#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <analysis/GaussLobattoQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <analysis/TensorialGaussLobattoQuadrature.hpp>
#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("TensorialGaussLobattoQuadrature", "[analysis]")
{
  SECTION("1D")
  {
    auto is_symmetric_formula = [](auto quadrature_formula) {
      auto point_list  = quadrature_formula.pointList();
      auto weight_list = quadrature_formula.weightList();

      bool is_symmetric = true;

      const size_t middle_index = point_list.size() / 2;
      for (size_t i = 0; i <= middle_index; ++i) {
        if (point_list[i] != -point_list[point_list.size() - 1 - i]) {
          return false;
        }
      }
      for (size_t i = 0; i <= middle_index; ++i) {
        if (weight_list[i] != weight_list[point_list.size() - 1 - i]) {
          return false;
        }
      }

      return is_symmetric;
    };

    auto integrate = [](auto f, auto quadrature_formula, const double a, const double b) {
      auto point_list  = quadrature_formula.pointList();
      auto weight_list = quadrature_formula.weightList();

      double alpha = 0.5 * (b - a);
      double beta  = 0.5 * (a + b);

      auto x = [&alpha, &beta](auto x_hat) { return TinyVector<1>{alpha * x_hat[0] + beta}; };

      auto value = weight_list[0] * f(x(point_list[0]));
      for (size_t i = 1; i < weight_list.size(); ++i) {
        value += weight_list[i] * f(x(point_list[i]));
      }

      return alpha * value;
    };

    auto get_order = [&integrate](auto f, auto quadrature_formula, const double a, const double b,
                                  const double exact_value) {
      double int_ab          = integrate(f, quadrature_formula, a, b);
      double int_first_half  = integrate(f, quadrature_formula, a, 0.5 * (a + b));
      double int_second_half = integrate(f, quadrature_formula, 0.5 * (a + b), b);

      return -std::log((int_first_half + int_second_half - exact_value) / (int_ab - exact_value)) / std::log(2);
    };

    auto p0 = [](const TinyVector<1>&) { return 1; };
    auto p1 = [&p0](const TinyVector<1>& X) {
      const double x = X[0];
      return 2 * x + p0(X);
    };
    auto p2 = [&p1](const TinyVector<1>& X) {
      const double x = X[0];
      return 3 * x * x + p1(X);
    };
    auto p3 = [&p2](const TinyVector<1>& X) {
      const double x = X[0];
      return 4 * std::pow(x, 3) + p2(X);
    };
    auto p4 = [&p3](const TinyVector<1>& X) {
      const double x = X[0];
      return 5 * std::pow(x, 4) + p3(X);
    };
    auto p5 = [&p4](const TinyVector<1>& X) {
      const double x = X[0];
      return 6 * std::pow(x, 5) + p4(X);
    };
    auto p6 = [&p5](const TinyVector<1>& X) {
      const double x = X[0];
      return 7 * std::pow(x, 6) + p5(X);
    };
    auto p7 = [&p6](const TinyVector<1>& X) {
      const double x = X[0];
      return 8 * std::pow(x, 7) + p6(X);
    };
    auto p8 = [&p7](const TinyVector<1>& X) {
      const double x = X[0];
      return 9 * std::pow(x, 8) + p7(X);
    };
    auto p9 = [&p8](const TinyVector<1>& X) {
      const double x = X[0];
      return 10 * std::pow(x, 9) + p8(X);
    };
    auto p10 = [&p9](const TinyVector<1>& X) {
      const double x = X[0];
      return 11 * std::pow(x, 10) + p9(X);
    };
    auto p11 = [&p10](const TinyVector<1>& X) {
      const double x = X[0];
      return 12 * std::pow(x, 11) + p10(X);
    };
    auto p12 = [&p11](const TinyVector<1>& X) {
      const double x = X[0];
      return 13 * std::pow(x, 12) + p11(X);
    };
    auto p13 = [&p12](const TinyVector<1>& X) {
      const double x = X[0];
      return 14 * std::pow(x, 13) + p12(X);
    };
    auto p14 = [&p13](const TinyVector<1>& X) {
      const double x = X[0];
      return 15 * std::pow(x, 14) + p13(X);
    };

    SECTION("degree 1")
    {
      const QuadratureFormula<1>& l1 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(1));

      REQUIRE(is_symmetric_formula(l1));

      REQUIRE(l1.numberOfPoints() == 2);

      REQUIRE(integrate(p0, l1, 0.5, 1) == Catch::Approx(0.5));
      REQUIRE(integrate(p1, l1, 0, 1) == Catch::Approx(2));
      REQUIRE(integrate(p2, l1, 0, 1) != Catch::Approx(3));

      REQUIRE(get_order(p2, l1, -1, 1, 4) == Catch::Approx(2));
    }

    SECTION("degree 2 and 3")
    {
      const QuadratureFormula<1>& l2 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(2));
      const QuadratureFormula<1>& l3 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(3));

      REQUIRE(&l2 == &l3);
      REQUIRE(is_symmetric_formula(l3));

      REQUIRE(l3.numberOfPoints() == 3);

      REQUIRE(integrate(p0, l3, 0.5, 1) == Catch::Approx(0.5));
      REQUIRE(integrate(p1, l3, 0, 1) == Catch::Approx(2));
      REQUIRE(integrate(p2, l3, 0, 1) == Catch::Approx(3));
      REQUIRE(integrate(p3, l3, 0, 1) == Catch::Approx(4));
      REQUIRE(integrate(p4, l3, 0, 1) != Catch::Approx(5));

      REQUIRE(get_order(p4, l3, -1, 1, 6) == Catch::Approx(4));
    }

    SECTION("degree 4 and 5")
    {
      const QuadratureFormula<1>& l4 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(4));
      const QuadratureFormula<1>& l5 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(5));

      REQUIRE(&l4 == &l5);
      REQUIRE(is_symmetric_formula(l5));

      REQUIRE(l5.numberOfPoints() == 4);

      REQUIRE(integrate(p0, l5, 0.5, 1) == Catch::Approx(0.5));
      REQUIRE(integrate(p1, l5, 0, 1) == Catch::Approx(2));
      REQUIRE(integrate(p2, l5, 0, 1) == Catch::Approx(3));
      REQUIRE(integrate(p3, l5, 0, 1) == Catch::Approx(4));
      REQUIRE(integrate(p4, l5, 0, 1) == Catch::Approx(5));
      REQUIRE(integrate(p5, l5, 0, 1) == Catch::Approx(6));
      REQUIRE(integrate(p6, l5, 0, 1) != Catch::Approx(7));

      REQUIRE(get_order(p6, l5, -1, 1, 8) == Catch::Approx(6));
    }

    SECTION("degree 6 and 7")
    {
      const QuadratureFormula<1>& l6 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(6));
      const QuadratureFormula<1>& l7 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(7));

      REQUIRE(&l6 == &l7);
      REQUIRE(is_symmetric_formula(l7));

      REQUIRE(l7.numberOfPoints() == 5);

      REQUIRE(integrate(p0, l7, 0.5, 1) == Catch::Approx(0.5));
      REQUIRE(integrate(p1, l7, 0, 1) == Catch::Approx(2));
      REQUIRE(integrate(p2, l7, 0, 1) == Catch::Approx(3));
      REQUIRE(integrate(p3, l7, 0, 1) == Catch::Approx(4));
      REQUIRE(integrate(p4, l7, 0, 1) == Catch::Approx(5));
      REQUIRE(integrate(p5, l7, 0, 1) == Catch::Approx(6));
      REQUIRE(integrate(p6, l7, 0, 1) == Catch::Approx(7));
      REQUIRE(integrate(p7, l7, 0, 1) == Catch::Approx(8));
      REQUIRE(integrate(p8, l7, 0, 1) != Catch::Approx(9));

      REQUIRE(get_order(p8, l7, -1, 1, 10) == Catch::Approx(8));
    }

    SECTION("degree 8 and 9")
    {
      const QuadratureFormula<1>& l8 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(8));
      const QuadratureFormula<1>& l9 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(9));

      REQUIRE(&l8 == &l9);
      REQUIRE(is_symmetric_formula(l9));

      REQUIRE(l9.numberOfPoints() == 6);

      REQUIRE(integrate(p0, l9, 0.5, 1) == Catch::Approx(0.5));
      REQUIRE(integrate(p1, l9, 0, 1) == Catch::Approx(2));
      REQUIRE(integrate(p2, l9, 0, 1) == Catch::Approx(3));
      REQUIRE(integrate(p3, l9, 0, 1) == Catch::Approx(4));
      REQUIRE(integrate(p4, l9, 0, 1) == Catch::Approx(5));
      REQUIRE(integrate(p5, l9, 0, 1) == Catch::Approx(6));
      REQUIRE(integrate(p6, l9, 0, 1) == Catch::Approx(7));
      REQUIRE(integrate(p7, l9, 0, 1) == Catch::Approx(8));
      REQUIRE(integrate(p8, l9, 0, 1) == Catch::Approx(9));
      REQUIRE(integrate(p9, l9, 0, 1) == Catch::Approx(10));
      REQUIRE(integrate(p10, l9, 0, 1) != Catch::Approx(11).epsilon(1E-13));

      REQUIRE(get_order(p10, l9, -1, 1, 12) == Catch::Approx(10));
    }

    SECTION("degree 10 and 11")
    {
      const QuadratureFormula<1>& l10 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(10));
      const QuadratureFormula<1>& l11 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(11));

      REQUIRE(&l10 == &l11);
      REQUIRE(is_symmetric_formula(l11));

      REQUIRE(l11.numberOfPoints() == 7);

      REQUIRE(integrate(p0, l11, 0.5, 1) == Catch::Approx(0.5));
      REQUIRE(integrate(p1, l11, 0, 1) == Catch::Approx(2));
      REQUIRE(integrate(p2, l11, 0, 1) == Catch::Approx(3));
      REQUIRE(integrate(p3, l11, 0, 1) == Catch::Approx(4));
      REQUIRE(integrate(p4, l11, 0, 1) == Catch::Approx(5));
      REQUIRE(integrate(p5, l11, 0, 1) == Catch::Approx(6));
      REQUIRE(integrate(p6, l11, 0, 1) == Catch::Approx(7));
      REQUIRE(integrate(p7, l11, 0, 1) == Catch::Approx(8));
      REQUIRE(integrate(p8, l11, 0, 1) == Catch::Approx(9));
      REQUIRE(integrate(p9, l11, 0, 1) == Catch::Approx(10));
      REQUIRE(integrate(p10, l11, 0, 1) == Catch::Approx(11));
      REQUIRE(integrate(p11, l11, 0, 1) == Catch::Approx(12));
      REQUIRE(integrate(p12, l11, 0, 1) != Catch::Approx(13).epsilon(1E-13));

      REQUIRE(get_order(p12, l11, -1, 1, 14) == Catch::Approx(12));
    }

    SECTION("degree 12 and 13")
    {
      const QuadratureFormula<1>& l12 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(12));
      const QuadratureFormula<1>& l13 =
        QuadratureManager::instance().getLineFormula(GaussLobattoQuadratureDescriptor(13));

      REQUIRE(&l12 == &l13);
      REQUIRE(is_symmetric_formula(l13));

      REQUIRE(l13.numberOfPoints() == 8);

      REQUIRE(integrate(p0, l13, 0.5, 1) == Catch::Approx(0.5));
      REQUIRE(integrate(p1, l13, 0, 1) == Catch::Approx(2));
      REQUIRE(integrate(p2, l13, 0, 1) == Catch::Approx(3));
      REQUIRE(integrate(p3, l13, 0, 1) == Catch::Approx(4));
      REQUIRE(integrate(p4, l13, 0, 1) == Catch::Approx(5));
      REQUIRE(integrate(p5, l13, 0, 1) == Catch::Approx(6));
      REQUIRE(integrate(p6, l13, 0, 1) == Catch::Approx(7));
      REQUIRE(integrate(p7, l13, 0, 1) == Catch::Approx(8));
      REQUIRE(integrate(p8, l13, 0, 1) == Catch::Approx(9));
      REQUIRE(integrate(p9, l13, 0, 1) == Catch::Approx(10));
      REQUIRE(integrate(p10, l13, 0, 1) == Catch::Approx(11));
      REQUIRE(integrate(p11, l13, 0, 1) == Catch::Approx(12));
      REQUIRE(integrate(p12, l13, 0, 1) == Catch::Approx(13));
      REQUIRE(integrate(p13, l13, 0, 1) == Catch::Approx(14));
      REQUIRE(integrate(p14, l13, 0, 1) != Catch::Approx(15).epsilon(1E-13));

      REQUIRE(get_order(p14, l13, -1, 1, 16) == Catch::Approx(14));
    }

    SECTION("max implemented degree")
    {
      REQUIRE(QuadratureManager::instance().maxLineDegree(QuadratureType::GaussLobatto) ==
              TensorialGaussLobattoQuadrature<1>::max_degree);
      REQUIRE_THROWS_WITH(QuadratureManager::instance().getLineFormula(
                            GaussLobattoQuadratureDescriptor(TensorialGaussLobattoQuadrature<1>::max_degree + 1)),
                          "error: Gauss-Lobatto quadrature formulae handle degrees up to " +
                            stringify(TensorialGaussLobattoQuadrature<1>::max_degree) + " on lines");
    }
  }

  SECTION("2D")
  {
    auto integrate = [](auto f, auto quadrature_formula, const double xa, const double xb, const double ya,
                        const double yb) {
      auto point_list  = quadrature_formula.pointList();
      auto weight_list = quadrature_formula.weightList();

      const double alphax = 0.5 * (xb - xa);
      const double betax  = 0.5 * (xa + xb);

      const double alphay = 0.5 * (yb - ya);
      const double betay  = 0.5 * (ya + yb);

      auto x = [&alphax, &betax, &alphay, &betay](auto x_hat) {
        return TinyVector<2>{alphax * x_hat[0] + betax, alphay * x_hat[1] + betay};
      };

      auto value = weight_list[0] * f(x(point_list[0]));
      for (size_t i = 1; i < weight_list.size(); ++i) {
        value += weight_list[i] * f(x(point_list[i]));
      }

      return alphax * alphay * value;
    };

    auto p6 = [](const double x) { return 7 * std::pow(x, 6); };
    auto p7 = [](const double x) { return 8 * std::pow(x, 7); };

    auto px7y6 = [&p6, &p7](const TinyVector<2>& X) {
      const double x = X[0];
      const double y = X[1];
      return p7(x) * p6(y);
    };

    SECTION("degree 6 and 7")
    {
      const QuadratureFormula<2>& l6 =
        QuadratureManager::instance().getSquareFormula(GaussLobattoQuadratureDescriptor(6));
      const QuadratureFormula<2>& l7 =
        QuadratureManager::instance().getSquareFormula(GaussLobattoQuadratureDescriptor(7));

      REQUIRE(&l6 == &l7);

      REQUIRE(l7.numberOfPoints() == 5 * 5);

      REQUIRE(integrate(px7y6, l7, 0, 1, 0.2, 0.8) == Catch::Approx(std::pow(0.8, 7) - std::pow(0.2, 7)));
    }

    SECTION("max implemented degree")
    {
      REQUIRE(QuadratureManager::instance().maxSquareDegree(QuadratureType::GaussLobatto) ==
              TensorialGaussLobattoQuadrature<2>::max_degree);
      REQUIRE_THROWS_WITH(QuadratureManager::instance().getSquareFormula(
                            GaussLobattoQuadratureDescriptor(TensorialGaussLobattoQuadrature<2>::max_degree + 1)),
                          "error: Gauss-Lobatto quadrature formulae handle degrees up to " +
                            stringify(TensorialGaussLobattoQuadrature<2>::max_degree) + " on squares");
    }
  }

  SECTION("3D")
  {
    auto integrate = [](auto f, auto quadrature_formula, const double xa, const double xb, const double ya,
                        const double yb, const double za, const double zb) {
      auto point_list  = quadrature_formula.pointList();
      auto weight_list = quadrature_formula.weightList();

      const double alphax = 0.5 * (xb - xa);
      const double betax  = 0.5 * (xa + xb);

      const double alphay = 0.5 * (yb - ya);
      const double betay  = 0.5 * (ya + yb);

      const double alphaz = 0.5 * (zb - za);
      const double betaz  = 0.5 * (za + zb);

      auto x = [&alphax, &betax, &alphay, &betay, &alphaz, &betaz](auto x_hat) {
        return TinyVector<3>{alphax * x_hat[0] + betax, alphay * x_hat[1] + betay, alphaz * x_hat[2] + betaz};
      };

      auto value = weight_list[0] * f(x(point_list[0]));
      for (size_t i = 1; i < weight_list.size(); ++i) {
        value += weight_list[i] * f(x(point_list[i]));
      }

      return alphax * alphay * alphaz * value;
    };

    auto p6 = [](const double x) { return 7 * std::pow(x, 6); };
    auto p7 = [](const double x) { return 8 * std::pow(x, 7); };

    auto px7y6 = [&p6, &p7](const TinyVector<3>& X) {
      const double x = X[0];
      const double y = X[1];
      const double z = X[2];
      return p7(x) * p6(y) + p7(z);
    };

    SECTION("degree 6 and 7")
    {
      const QuadratureFormula<3>& l6 =
        QuadratureManager::instance().getCubeFormula(GaussLobattoQuadratureDescriptor(6));
      const QuadratureFormula<3>& l7 =
        QuadratureManager::instance().getCubeFormula(GaussLobattoQuadratureDescriptor(7));

      REQUIRE(&l6 == &l7);

      REQUIRE(l7.numberOfPoints() == 5 * 5 * 5);

      REQUIRE(integrate(px7y6, l7, 0, 1, 0.2, 0.8, -0.1, 0.7) ==
              Catch::Approx((std::pow(0.8, 7) - std::pow(0.2, 7)) * (0.7 - -0.1) +
                            (0.8 - 0.2) * (std::pow(0.7, 8) - std::pow(-0.1, 8))));
    }

    SECTION("max implemented degree")
    {
      REQUIRE(QuadratureManager::instance().maxCubeDegree(QuadratureType::GaussLobatto) ==
              TensorialGaussLobattoQuadrature<3>::max_degree);
      REQUIRE_THROWS_WITH(QuadratureManager::instance().getCubeFormula(
                            GaussLobattoQuadratureDescriptor(TensorialGaussLobattoQuadrature<3>::max_degree + 1)),
                          "error: Gauss-Lobatto quadrature formulae handle degrees up to " +
                            stringify(TensorialGaussLobattoQuadrature<3>::max_degree) + " on cubes");
    }
  }

  SECTION("Access functions")
  {
    const QuadratureFormula<3>& quadrature_formula =
      QuadratureManager::instance().getCubeFormula(GaussLobattoQuadratureDescriptor(7));

    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    REQUIRE(point_list.size() == quadrature_formula.numberOfPoints());
    REQUIRE(weight_list.size() == quadrature_formula.numberOfPoints());

    for (size_t i = 0; i < quadrature_formula.numberOfPoints(); ++i) {
      REQUIRE(&point_list[i] == &quadrature_formula.point(i));
      REQUIRE(&weight_list[i] == &quadrature_formula.weight(i));
    }
  }
}
