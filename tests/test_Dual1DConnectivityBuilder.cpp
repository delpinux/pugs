#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/DualConnectivityManager.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityUtils.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>

template <ItemType item_type, typename ConnectivityType>
inline auto
get_item_ref_ids(const ConnectivityType& connectivity)
{
  std::map<std::string, size_t> ref_id_set;
  for (size_t i = 0; i < connectivity.template numberOfRefItemList<item_type>(); ++i) {
    const auto& ref_id_list = connectivity.template refItemList<item_type>(i);
    std::ostringstream os;
    os << ref_id_list.refId();
    ItemValue<size_t, item_type> item_tag{connectivity};
    item_tag.fill(0);
    auto& list = ref_id_list.list();
    for (size_t i_item = 0; i_item < ref_id_list.list().size(); ++i_item) {
      item_tag[list[i_item]] = 1;
    }

    ref_id_set[os.str()] = sum(item_tag);
  }
  return ref_id_set;
}

// clazy:excludeall=non-pod-global-static

TEST_CASE("Dual1DConnectivityBuilder", "[mesh]")
{
  constexpr static size_t Dimension = 1;

  using MeshType         = Mesh<Dimension>;
  using ConnectivityType = typename MeshType::Connectivity;

  std::shared_ptr<const MeshType> mesh        = MeshDataBaseForTests::get().unordered1DMesh()->get<MeshType>();
  const ConnectivityType& primal_connectivity = mesh->connectivity();

  REQUIRE(primal_connectivity.numberOfNodes() == 35);
  REQUIRE(primal_connectivity.numberOfCells() == 34);

  std::shared_ptr p_dual_1d_connectivity =
    DualConnectivityManager::instance().getDual1DConnectivity(primal_connectivity);
  const ConnectivityType& dual_connectivity = *p_dual_1d_connectivity;

  REQUIRE(checkConnectivityOrdering(dual_connectivity));

  REQUIRE(dual_connectivity.numberOfNodes() == 36);
  REQUIRE(dual_connectivity.numberOfCells() == 35);

  SECTION("ref node list")
  {
    REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::node>() == 3);
    REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::node>() == 2);

    const auto primal_ref_node_list = get_item_ref_ids<ItemType::node>(primal_connectivity);
    REQUIRE(primal_ref_node_list.size() == 3);

    REQUIRE(primal_ref_node_list.count("INTERFACE(3)") == 1);
    REQUIRE(primal_ref_node_list.count("XMAX(2)") == 1);
    REQUIRE(primal_ref_node_list.count("XMIN(1)") == 1);

    REQUIRE(primal_ref_node_list.at("INTERFACE(3)") == 1);
    REQUIRE(primal_ref_node_list.at("XMAX(2)") == 1);
    REQUIRE(primal_ref_node_list.at("XMIN(1)") == 1);

    const auto dual_ref_node_list = get_item_ref_ids<ItemType::node>(dual_connectivity);
    REQUIRE(dual_ref_node_list.size() == 2);

    REQUIRE(dual_ref_node_list.count("XMAX(2)") == 1);
    REQUIRE(dual_ref_node_list.count("XMIN(1)") == 1);

    REQUIRE(dual_ref_node_list.at("XMAX(2)") == 1);
    REQUIRE(dual_ref_node_list.at("XMIN(1)") == 1);
  }

  SECTION("ref cell list")
  {
    REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::cell>() == 2);
    // dual 1d cell references are not computed
    REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::cell>() == 0);

    const auto primal_ref_cell_list = get_item_ref_ids<ItemType::cell>(primal_connectivity);
    REQUIRE(primal_ref_cell_list.size() == 2);

    REQUIRE(primal_ref_cell_list.count("LEFT(4)") == 1);
    REQUIRE(primal_ref_cell_list.count("RIGHT(5)") == 1);

    REQUIRE(primal_ref_cell_list.at("LEFT(4)") == 26);
    REQUIRE(primal_ref_cell_list.at("RIGHT(5)") == 8);

    const auto dual_ref_cell_list = get_item_ref_ids<ItemType::cell>(dual_connectivity);
    REQUIRE(dual_ref_cell_list.size() == 0);
  }
}
