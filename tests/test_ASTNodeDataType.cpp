#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>
#include <language/utils/ASTNodeDataType.hpp>

namespace language
{
struct integer;
struct real;
struct vector_type;
struct matrix_type;
}   // namespace language

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeDataType", "[language]")
{
  const ASTNodeDataType undefined_dt        = ASTNodeDataType{};
  const ASTNodeDataType bool_dt             = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  const ASTNodeDataType unsigned_int_dt     = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  const ASTNodeDataType int_dt              = ASTNodeDataType::build<ASTNodeDataType::int_t>();
  const ASTNodeDataType double_dt           = ASTNodeDataType::build<ASTNodeDataType::double_t>();
  const ASTNodeDataType string_dt           = ASTNodeDataType::build<ASTNodeDataType::string_t>();
  const ASTNodeDataType void_dt             = ASTNodeDataType::build<ASTNodeDataType::void_t>();
  const ASTNodeDataType function_dt         = ASTNodeDataType::build<ASTNodeDataType::function_t>();
  const ASTNodeDataType builtin_function_dt = ASTNodeDataType::build<ASTNodeDataType::builtin_function_t>();

  std::vector<std::shared_ptr<const ASTNodeDataType>> type_list;
  type_list.push_back(std::make_shared<const ASTNodeDataType>(double_dt));
  type_list.push_back(std::make_shared<const ASTNodeDataType>(int_dt));

  const ASTNodeDataType list_dt = ASTNodeDataType::build<ASTNodeDataType::list_t>(type_list);

  const ASTNodeDataType empty_list_dt =
    ASTNodeDataType::build<ASTNodeDataType::list_t>(std::vector<std::shared_ptr<const ASTNodeDataType>>{});

  SECTION("dataTypeName")
  {
    REQUIRE(dataTypeName(undefined_dt) == "undefined");
    REQUIRE(dataTypeName(bool_dt) == "B");
    REQUIRE(dataTypeName(unsigned_int_dt) == "N");
    REQUIRE(dataTypeName(int_dt) == "Z");
    REQUIRE(dataTypeName(double_dt) == "R");
    REQUIRE(dataTypeName(string_dt) == "string");
    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::typename_t>(double_dt)) == "R");
    REQUIRE(dataTypeName(void_dt) == "void");
    REQUIRE(dataTypeName(function_dt) == "function");
    REQUIRE(dataTypeName(builtin_function_dt) == "builtin_function");
    REQUIRE(dataTypeName(list_dt) == "R*Z");
    REQUIRE(dataTypeName(empty_list_dt) == "void");
    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(bool_dt)) == "(B)");
    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(unsigned_int_dt)) == "(N)");
    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(int_dt)) == "(Z)");
    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(double_dt)) == "(R)");
    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(int_dt)) == "(Z)");

    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::type_name_id_t>()) == "type_name_id");

    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::type_id_t>("user_type")) == "user_type");

    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::vector_t>(1)) == "R^1");
    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::vector_t>(2)) == "R^2");
    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::vector_t>(3)) == "R^3");

    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::vector_t>(7)) == "R^7");

    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1)) == "R^1x1");
    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2)) == "R^2x2");
    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3)) == "R^3x3");

    REQUIRE(dataTypeName(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(7, 3)) == "R^7x3");

    REQUIRE(dataTypeName(std::vector<ASTNodeDataType>{}) == "void");
    REQUIRE(dataTypeName(std::vector<ASTNodeDataType>{bool_dt}) == "B");
    REQUIRE(dataTypeName(std::vector<ASTNodeDataType>{bool_dt, unsigned_int_dt}) == "B*N");
  }

  SECTION("promotion")
  {
    const ASTNodeDataType vector_dt = ASTNodeDataType::build<ASTNodeDataType::vector_t>(5);

    REQUIRE(dataTypePromotion(undefined_dt, undefined_dt) == undefined_dt);
    REQUIRE(dataTypePromotion(void_dt, double_dt) == undefined_dt);
    REQUIRE(dataTypePromotion(int_dt, undefined_dt) == undefined_dt);
    REQUIRE(dataTypePromotion(double_dt, bool_dt) == double_dt);
    REQUIRE(dataTypePromotion(double_dt, unsigned_int_dt) == double_dt);
    REQUIRE(dataTypePromotion(double_dt, int_dt) == double_dt);
    REQUIRE(dataTypePromotion(int_dt, unsigned_int_dt) == unsigned_int_dt);
    REQUIRE(dataTypePromotion(int_dt, bool_dt) == int_dt);
    REQUIRE(dataTypePromotion(unsigned_int_dt, bool_dt) == unsigned_int_dt);

    REQUIRE(dataTypePromotion(string_dt, bool_dt) == string_dt);
    REQUIRE(dataTypePromotion(string_dt, int_dt) == string_dt);
    REQUIRE(dataTypePromotion(string_dt, unsigned_int_dt) == string_dt);
    REQUIRE(dataTypePromotion(string_dt, double_dt) == string_dt);

    REQUIRE(dataTypePromotion(bool_dt, string_dt) == undefined_dt);
    REQUIRE(dataTypePromotion(int_dt, string_dt) == undefined_dt);
    REQUIRE(dataTypePromotion(unsigned_int_dt, string_dt) == undefined_dt);
    REQUIRE(dataTypePromotion(double_dt, string_dt) == undefined_dt);
    REQUIRE(dataTypePromotion(bool_dt, vector_dt) == vector_dt);
    REQUIRE(dataTypePromotion(double_dt, vector_dt) == vector_dt);
  }

  SECTION("getVectorDataType")
  {
    std::unique_ptr type_node = std::make_unique<ASTNode>();
    type_node->set_type<language::vector_type>();

    type_node->emplace_back(std::make_unique<ASTNode>());

    {
      std::unique_ptr dimension_node = std::make_unique<ASTNode>();
      dimension_node->set_type<language::integer>();
      dimension_node->source      = "3";
      const char* const beginning = &dimension_node->source[0];
      dimension_node->m_begin     = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
      dimension_node->m_end       = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};
      type_node->emplace_back(std::move(dimension_node));
    }

    SECTION("good node")
    {
      REQUIRE(getVectorDataType(*type_node) == ASTNodeDataType::build<ASTNodeDataType::vector_t>(3));
      REQUIRE(getVectorDataType(*type_node).dimension() == 3);
    }

    SECTION("bad node type")
    {
      type_node->set_type<language::integer>();
      REQUIRE_THROWS_WITH(getVectorDataType(*type_node), "unexpected node type");
    }

    SECTION("bad children size 1")
    {
      type_node->children.clear();
      REQUIRE_THROWS_WITH(getVectorDataType(*type_node), "unexpected node type");
    }

    SECTION("bad children size 1")
    {
      type_node->children.emplace_back(std::unique_ptr<ASTNode>());
      REQUIRE_THROWS_WITH(getVectorDataType(*type_node), "unexpected node type");
    }

    SECTION("bad dimension type")
    {
      type_node->children[1]->set_type<language::real>();
      REQUIRE_THROWS_WITH(getVectorDataType(*type_node), "unexpected non integer constant dimension");
    }

    SECTION("bad dimension value")
    {
      {
        type_node->children[1]->source  = "0";
        const char* const beginning     = &type_node->children[1]->source[0];
        type_node->children[1]->m_begin = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
        type_node->children[1]->m_end   = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};

        REQUIRE_THROWS_WITH(getVectorDataType(*type_node), "invalid dimension (must be 1, 2 or 3)");
      }

      {
        type_node->children[1]->source  = "1";
        const char* const beginning     = &type_node->children[1]->source[0];
        type_node->children[1]->m_begin = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
        type_node->children[1]->m_end   = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};

        REQUIRE_NOTHROW(getVectorDataType(*type_node));
      }

      {
        type_node->children[1]->source  = "4";
        const char* const beginning     = &type_node->children[1]->source[0];
        type_node->children[1]->m_begin = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
        type_node->children[1]->m_end   = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};

        REQUIRE_THROWS_WITH(getVectorDataType(*type_node), "invalid dimension (must be 1, 2 or 3)");
      }
    }
  }

  SECTION("getVectorExpressionType")
  {
    std::unique_ptr vector_expression_node = std::make_unique<ASTNode>();
    vector_expression_node->set_type<language::vector_expression>();
    vector_expression_node->emplace_back(std::make_unique<ASTNode>());

    SECTION("good nodes")
    {
      vector_expression_node->children.resize(1);
      for (size_t i = 0; i < vector_expression_node->children.size(); ++i) {
        vector_expression_node->children[i]              = std::make_unique<ASTNode>();
        vector_expression_node->children[i]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
      }
      REQUIRE(getVectorExpressionType(*vector_expression_node) == ASTNodeDataType::build<ASTNodeDataType::vector_t>(1));
      REQUIRE(getVectorExpressionType(*vector_expression_node).dimension() == 1);

      vector_expression_node->children.resize(2);
      for (size_t i = 0; i < vector_expression_node->children.size(); ++i) {
        vector_expression_node->children[i]              = std::make_unique<ASTNode>();
        vector_expression_node->children[i]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::double_t>();
      }
      REQUIRE(getVectorExpressionType(*vector_expression_node) == ASTNodeDataType::build<ASTNodeDataType::vector_t>(2));
      REQUIRE(getVectorExpressionType(*vector_expression_node).dimension() == 2);

      vector_expression_node->children.resize(3);
      for (size_t i = 0; i < vector_expression_node->children.size(); ++i) {
        vector_expression_node->children[i]              = std::make_unique<ASTNode>();
        vector_expression_node->children[i]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
      }
      REQUIRE(getVectorExpressionType(*vector_expression_node) == ASTNodeDataType::build<ASTNodeDataType::vector_t>(3));
      REQUIRE(getVectorExpressionType(*vector_expression_node).dimension() == 3);
    }

    SECTION("bad content type")
    {
      vector_expression_node->children.resize(3);
      for (size_t i = 0; i < vector_expression_node->children.size(); ++i) {
        vector_expression_node->children[i] = std::make_unique<ASTNode>();
      }
      REQUIRE_THROWS_WITH(getVectorExpressionType(*vector_expression_node),
                          "unexpected error: invalid implicit conversion: undefined -> R");
    }

    SECTION("bad node type")
    {
      vector_expression_node->set_type<language::real>();
      REQUIRE_THROWS_WITH(getVectorExpressionType(*vector_expression_node), "unexpected node type");
    }

    SECTION("bad children size 2")
    {
      vector_expression_node->children.resize(4);
      REQUIRE_THROWS_WITH(getVectorExpressionType(*vector_expression_node), "invalid dimension (must be 1, 2 or 3)");
    }

    SECTION("bad children size 2")
    {
      vector_expression_node->children.clear();
      REQUIRE_THROWS_WITH(getVectorExpressionType(*vector_expression_node), "unexpected node type");
    }
  }

  SECTION("getMatrixDataType")
  {
    std::unique_ptr type_node = std::make_unique<ASTNode>();
    type_node->set_type<language::matrix_type>();

    type_node->emplace_back(std::make_unique<ASTNode>());

    {
      {
        std::unique_ptr dimension0_node = std::make_unique<ASTNode>();
        dimension0_node->set_type<language::integer>();
        dimension0_node->source     = "3";
        const char* const beginning = &dimension0_node->source[0];
        dimension0_node->m_begin    = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
        dimension0_node->m_end      = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};
        type_node->emplace_back(std::move(dimension0_node));
      }
      {
        std::unique_ptr dimension1_node = std::make_unique<ASTNode>();
        dimension1_node->set_type<language::integer>();
        dimension1_node->source     = "3";
        const char* const beginning = &dimension1_node->source[0];
        dimension1_node->m_begin    = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
        dimension1_node->m_end      = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};
        type_node->emplace_back(std::move(dimension1_node));
      }
    }

    SECTION("good node")
    {
      REQUIRE(getMatrixDataType(*type_node) == ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3));
      REQUIRE(getMatrixDataType(*type_node).numberOfRows() == 3);
      REQUIRE(getMatrixDataType(*type_node).numberOfColumns() == 3);
    }

    SECTION("bad node type")
    {
      type_node->set_type<language::integer>();
      REQUIRE_THROWS_WITH(getMatrixDataType(*type_node), "unexpected node type");
    }

    SECTION("bad children size 1")
    {
      type_node->children.clear();
      REQUIRE_THROWS_WITH(getMatrixDataType(*type_node), "unexpected node type");
    }

    SECTION("bad children size 2")
    {
      type_node->children.emplace_back(std::unique_ptr<ASTNode>());
      REQUIRE_THROWS_WITH(getMatrixDataType(*type_node), "unexpected node type");
    }

    SECTION("bad dimension 0 type")
    {
      type_node->children[1]->set_type<language::real>();
      REQUIRE_THROWS_WITH(getMatrixDataType(*type_node), "unexpected non integer constant dimension");
    }

    SECTION("bad dimension 1 type")
    {
      type_node->children[2]->set_type<language::real>();
      REQUIRE_THROWS_WITH(getMatrixDataType(*type_node), "unexpected non integer constant dimension");
    }

    SECTION("bad nb rows value")
    {
      {
        type_node->children[1]->source  = "0";
        const char* const beginning     = &type_node->children[1]->source[0];
        type_node->children[1]->m_begin = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
        type_node->children[1]->m_end   = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};
      }
      {
        type_node->children[2]->source  = "0";
        const char* const beginning     = &type_node->children[2]->source[0];
        type_node->children[2]->m_begin = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
        type_node->children[2]->m_end   = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};
      }

      REQUIRE_THROWS_WITH(getMatrixDataType(*type_node), "invalid dimension (must be 1, 2 or 3)");

      {
        type_node->children[1]->source  = "4";
        const char* const beginning     = &type_node->children[1]->source[0];
        type_node->children[1]->m_begin = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
        type_node->children[1]->m_end   = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};
      }
      {
        type_node->children[2]->source  = "4";
        const char* const beginning     = &type_node->children[2]->source[0];
        type_node->children[2]->m_begin = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
        type_node->children[2]->m_end   = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};
      }

      REQUIRE_THROWS_WITH(getMatrixDataType(*type_node), "invalid dimension (must be 1, 2 or 3)");
    }

    SECTION("none square matrices")
    {
      {
        type_node->children[1]->source  = "1";
        const char* const beginning     = &type_node->children[1]->source[0];
        type_node->children[1]->m_begin = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
        type_node->children[1]->m_end   = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};
      }
      {
        type_node->children[2]->source  = "2";
        const char* const beginning     = &type_node->children[2]->source[0];
        type_node->children[2]->m_begin = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning};
        type_node->children[2]->m_end   = TAO_PEGTL_NAMESPACE::internal::inputerator{beginning + 1};
      }
      REQUIRE_THROWS_WITH(getMatrixDataType(*type_node), "only square matrices are supported");
    }
  }

  SECTION("getMatrixExpressionType")
  {
    std::unique_ptr matrix_expression_node = std::make_unique<ASTNode>();
    matrix_expression_node->set_type<language::matrix_expression>();
    matrix_expression_node->emplace_back(std::make_unique<ASTNode>());

    SECTION("good nodes")
    {
      {
        const size_t dimension = 1;
        matrix_expression_node->children.clear();
        for (size_t i = 0; i < dimension; ++i) {
          matrix_expression_node->children.emplace_back(std::make_unique<ASTNode>());
          matrix_expression_node->children[i]->set_type<language::row_expression>();
          for (size_t j = 0; j < dimension; ++j) {
            matrix_expression_node->children[i]->children.emplace_back(std::make_unique<ASTNode>());
            matrix_expression_node->children[i]->children[j]->m_data_type =
              ASTNodeDataType::build<ASTNodeDataType::int_t>();
          }
        }
        REQUIRE(getMatrixExpressionType(*matrix_expression_node) ==
                ASTNodeDataType::build<ASTNodeDataType::matrix_t>(dimension, dimension));
        REQUIRE(getMatrixExpressionType(*matrix_expression_node).numberOfRows() == dimension);
        REQUIRE(getMatrixExpressionType(*matrix_expression_node).numberOfColumns() == dimension);
      }

      {
        const size_t dimension = 2;
        matrix_expression_node->children.clear();
        for (size_t i = 0; i < dimension; ++i) {
          matrix_expression_node->children.emplace_back(std::make_unique<ASTNode>());
          matrix_expression_node->children[i]->set_type<language::row_expression>();
          for (size_t j = 0; j < dimension; ++j) {
            matrix_expression_node->children[i]->children.emplace_back(std::make_unique<ASTNode>());
            matrix_expression_node->children[i]->children[j]->m_data_type =
              ASTNodeDataType::build<ASTNodeDataType::int_t>();
          }
        }
        REQUIRE(getMatrixExpressionType(*matrix_expression_node) ==
                ASTNodeDataType::build<ASTNodeDataType::matrix_t>(dimension, dimension));
        REQUIRE(getMatrixExpressionType(*matrix_expression_node).numberOfRows() == dimension);
        REQUIRE(getMatrixExpressionType(*matrix_expression_node).numberOfColumns() == dimension);
      }

      {
        const size_t dimension = 3;
        matrix_expression_node->children.clear();
        for (size_t i = 0; i < dimension; ++i) {
          matrix_expression_node->children.emplace_back(std::make_unique<ASTNode>());
          matrix_expression_node->children[i]->set_type<language::row_expression>();
          for (size_t j = 0; j < dimension; ++j) {
            matrix_expression_node->children[i]->children.emplace_back(std::make_unique<ASTNode>());
            matrix_expression_node->children[i]->children[j]->m_data_type =
              ASTNodeDataType::build<ASTNodeDataType::int_t>();
          }
        }
        REQUIRE(getMatrixExpressionType(*matrix_expression_node) ==
                ASTNodeDataType::build<ASTNodeDataType::matrix_t>(dimension, dimension));
        REQUIRE(getMatrixExpressionType(*matrix_expression_node).numberOfRows() == dimension);
        REQUIRE(getMatrixExpressionType(*matrix_expression_node).numberOfColumns() == dimension);
      }
    }

    SECTION("bad content type")
    {
      const size_t dimension = 3;
      matrix_expression_node->children.clear();
      for (size_t i = 0; i < dimension; ++i) {
        matrix_expression_node->children.emplace_back(std::make_unique<ASTNode>());
        matrix_expression_node->children[i]->set_type<language::row_expression>();
        for (size_t j = 0; j < dimension; ++j) {
          matrix_expression_node->children[i]->children.emplace_back(std::make_unique<ASTNode>());
        }
      }
      REQUIRE_THROWS_WITH(getMatrixExpressionType(*matrix_expression_node),
                          "unexpected error: invalid implicit conversion: undefined -> R");
    }

    SECTION("bad node type")
    {
      matrix_expression_node->set_type<language::real>();
      REQUIRE_THROWS_WITH(getMatrixExpressionType(*matrix_expression_node), "unexpected node type");
    }

    SECTION("bad content type")
    {
      const size_t dimension = 3;
      matrix_expression_node->children.clear();
      for (size_t i = 0; i < dimension; ++i) {
        matrix_expression_node->children.emplace_back(std::make_unique<ASTNode>());
      }
      REQUIRE_THROWS_WITH(getMatrixExpressionType(*matrix_expression_node), "expecting row expression");
    }

    SECTION("bad children size 1")
    {
      matrix_expression_node->children.resize(4);
      REQUIRE_THROWS_WITH(getMatrixExpressionType(*matrix_expression_node), "invalid dimension (must be 1, 2 or 3)");
    }

    SECTION("bad children size 2")
    {
      const size_t dimension = 2;
      matrix_expression_node->children.clear();
      for (size_t i = 0; i < dimension; ++i) {
        matrix_expression_node->children.emplace_back(std::make_unique<ASTNode>());
        matrix_expression_node->children[i]->set_type<language::row_expression>();
        for (size_t j = 0; j < dimension + 1; ++j) {
          matrix_expression_node->children[i]->children.emplace_back(std::make_unique<ASTNode>());
        }
      }
      REQUIRE_THROWS_WITH(getMatrixExpressionType(*matrix_expression_node), "only square matrices are supported");
    }

    SECTION("bad children size 3")
    {
      const size_t dimension = 2;
      matrix_expression_node->children.clear();
      for (size_t i = 0; i < dimension; ++i) {
        matrix_expression_node->children.emplace_back(std::make_unique<ASTNode>());
        matrix_expression_node->children[i]->set_type<language::row_expression>();
        for (size_t j = 0; j < dimension; ++j) {
          matrix_expression_node->children[i]->children.emplace_back(std::make_unique<ASTNode>());
        }
      }
      matrix_expression_node->children[1]->children.emplace_back(std::make_unique<ASTNode>());

      REQUIRE_THROWS_WITH(getMatrixExpressionType(*matrix_expression_node), "row must have same sizes");
    }
  }

  SECTION("isNaturalConversion")
  {
    SECTION("-> B")
    {
      REQUIRE(isNaturalConversion(bool_dt, bool_dt));
      REQUIRE(not isNaturalConversion(unsigned_int_dt, bool_dt));
      REQUIRE(not isNaturalConversion(int_dt, bool_dt));
      REQUIRE(not isNaturalConversion(double_dt, bool_dt));
      REQUIRE(not isNaturalConversion(string_dt, bool_dt));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(bool_dt), bool_dt));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(1), bool_dt));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1), bool_dt));
    }

    SECTION("-> N")
    {
      REQUIRE(isNaturalConversion(bool_dt, unsigned_int_dt));
      REQUIRE(isNaturalConversion(unsigned_int_dt, unsigned_int_dt));
      REQUIRE(isNaturalConversion(int_dt, unsigned_int_dt));
      REQUIRE(not isNaturalConversion(double_dt, unsigned_int_dt));
      REQUIRE(not isNaturalConversion(string_dt, unsigned_int_dt));
      REQUIRE(
        not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(unsigned_int_dt), unsigned_int_dt));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(1), unsigned_int_dt));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1), unsigned_int_dt));
    }

    SECTION("-> Z")
    {
      REQUIRE(isNaturalConversion(bool_dt, int_dt));
      REQUIRE(isNaturalConversion(unsigned_int_dt, int_dt));
      REQUIRE(isNaturalConversion(int_dt, int_dt));
      REQUIRE(not isNaturalConversion(double_dt, int_dt));
      REQUIRE(not isNaturalConversion(string_dt, int_dt));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(int_dt), int_dt));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(1), int_dt));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1), int_dt));
    }

    SECTION("-> R")
    {
      REQUIRE(isNaturalConversion(bool_dt, double_dt));
      REQUIRE(isNaturalConversion(unsigned_int_dt, double_dt));
      REQUIRE(isNaturalConversion(int_dt, double_dt));
      REQUIRE(isNaturalConversion(double_dt, double_dt));
      REQUIRE(not isNaturalConversion(string_dt, double_dt));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(double_dt), double_dt));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(1), double_dt));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1), double_dt));
    }

    SECTION("-> string")
    {
      REQUIRE(isNaturalConversion(bool_dt, string_dt));
      REQUIRE(isNaturalConversion(unsigned_int_dt, string_dt));
      REQUIRE(isNaturalConversion(int_dt, string_dt));
      REQUIRE(isNaturalConversion(double_dt, string_dt));
      REQUIRE(isNaturalConversion(string_dt, string_dt));
      REQUIRE(isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_dt), string_dt));
      REQUIRE(isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(1), string_dt));
      REQUIRE(isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1), string_dt));
    }

    SECTION("-> tuple")
    {
      REQUIRE(isNaturalConversion(bool_dt, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(bool_dt)));
      REQUIRE(isNaturalConversion(bool_dt, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(unsigned_int_dt)));
      REQUIRE(not isNaturalConversion(int_dt, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(bool_dt)));

      REQUIRE(isNaturalConversion(unsigned_int_dt, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(unsigned_int_dt)));
      REQUIRE(isNaturalConversion(unsigned_int_dt, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(int_dt)));
      REQUIRE(not isNaturalConversion(double_dt, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(unsigned_int_dt)));

      REQUIRE(isNaturalConversion(bool_dt, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(double_dt)));
      REQUIRE(isNaturalConversion(int_dt, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(double_dt)));
      REQUIRE(isNaturalConversion(unsigned_int_dt, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(double_dt)));
      REQUIRE(isNaturalConversion(double_dt, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(double_dt)));
      REQUIRE(not isNaturalConversion(string_dt, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(double_dt)));
    }

    SECTION("-> vector")
    {
      REQUIRE(not isNaturalConversion(bool_dt, ASTNodeDataType::build<ASTNodeDataType::vector_t>(1)));
      REQUIRE(not isNaturalConversion(unsigned_int_dt, ASTNodeDataType::build<ASTNodeDataType::vector_t>(3)));
      REQUIRE(not isNaturalConversion(int_dt, ASTNodeDataType::build<ASTNodeDataType::vector_t>(2)));
      REQUIRE(not isNaturalConversion(double_dt, ASTNodeDataType::build<ASTNodeDataType::vector_t>(2)));
      REQUIRE(not isNaturalConversion(string_dt, ASTNodeDataType::build<ASTNodeDataType::vector_t>(3)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(double_dt),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(1)));

      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(1)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(2)));

      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(2)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(4)));

      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(3)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(9)));

      REQUIRE(isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(1),
                                  ASTNodeDataType::build<ASTNodeDataType::vector_t>(1)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(2),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(1)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(3),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(1)));

      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(1),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(2)));
      REQUIRE(isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(2),
                                  ASTNodeDataType::build<ASTNodeDataType::vector_t>(2)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(3),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(2)));

      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(1),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(3)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(2),
                                      ASTNodeDataType::build<ASTNodeDataType::vector_t>(3)));
      REQUIRE(isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(3),
                                  ASTNodeDataType::build<ASTNodeDataType::vector_t>(3)));
    }

    SECTION("-> matrix")
    {
      REQUIRE(not isNaturalConversion(bool_dt, ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1)));
      REQUIRE(not isNaturalConversion(unsigned_int_dt, ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3)));
      REQUIRE(not isNaturalConversion(int_dt, ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2)));
      REQUIRE(not isNaturalConversion(double_dt, ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2)));
      REQUIRE(not isNaturalConversion(string_dt, ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(double_dt),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1)));

      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(1),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(2),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1)));

      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(2),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(4),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2)));

      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(3),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(9),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3)));

      REQUIRE(isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1),
                                  ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1)));

      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2)));
      REQUIRE(isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2),
                                  ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2)));

      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3)));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2),
                                      ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 2)));
      REQUIRE(isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3),
                                  ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3)));
    }

    SECTION("-> type_id")
    {
      REQUIRE(not isNaturalConversion(bool_dt, ASTNodeDataType::build<ASTNodeDataType::type_id_t>("foo")));
      REQUIRE(not isNaturalConversion(unsigned_int_dt, ASTNodeDataType::build<ASTNodeDataType::type_id_t>("foo")));
      REQUIRE(not isNaturalConversion(int_dt, ASTNodeDataType::build<ASTNodeDataType::type_id_t>("foo")));
      REQUIRE(not isNaturalConversion(double_dt, ASTNodeDataType::build<ASTNodeDataType::type_id_t>("foo")));
      REQUIRE(not isNaturalConversion(string_dt, ASTNodeDataType::build<ASTNodeDataType::type_id_t>("foo")));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::vector_t>(1),
                                      ASTNodeDataType::build<ASTNodeDataType::type_id_t>("foo")));
      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(int_dt),
                                      ASTNodeDataType::build<ASTNodeDataType::type_id_t>("foo")));

      REQUIRE(isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::type_id_t>("foo"),
                                  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("foo")));

      REQUIRE(not isNaturalConversion(ASTNodeDataType::build<ASTNodeDataType::type_id_t>("foo"),
                                      ASTNodeDataType::build<ASTNodeDataType::type_id_t>("bar")));
    }
  }

#ifndef NDEBUG
  SECTION("errors")
  {
    REQUIRE_THROWS_AS(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(undefined_dt), AssertError);
  }
#endif   // NDEBUG
}
