#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/OStream.hpp>

#include <sstream>

// clazy:excludeall=non-pod-global-static

TEST_CASE("OStream", "[language]")
{
  SECTION("null ostream")
  {
    std::shared_ptr os = std::make_shared<OStream>(OStream::Type::std_ostream);

    REQUIRE_NOTHROW(os << "foo" << 3 << " bar");
  }

  SECTION("valid ostream")
  {
    std::stringstream sstr;

    std::shared_ptr os = std::make_shared<OStream>(sstr, OStream::Type::std_ofstream);
    os << "foo" << 3 << " bar";

    REQUIRE(sstr.str() == "foo3 bar");
  }

#ifndef NDEBUG
  SECTION("non allocated stream")
  {
    std::shared_ptr<OStream> bad_os;
    REQUIRE_THROWS_WITH((bad_os << 2), "non allocated stream");
  }
#endif   // NDEBUG
}
