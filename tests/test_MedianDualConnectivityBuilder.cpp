#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/DualConnectivityManager.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityUtils.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>

template <ItemType item_type, typename ConnectivityType>
inline auto
get_item_ref_ids(const ConnectivityType& connectivity)
{
  std::map<std::string, size_t> ref_id_set;
  for (size_t i = 0; i < connectivity.template numberOfRefItemList<item_type>(); ++i) {
    const auto& ref_id_list = connectivity.template refItemList<item_type>(i);
    std::ostringstream os;
    os << ref_id_list.refId();
    ItemValue<size_t, item_type> item_tag{connectivity};
    item_tag.fill(0);
    for (size_t i_item = 0; i_item < ref_id_list.list().size(); ++i_item) {
      item_tag[ref_id_list.list()[i_item]] = 1;
    }

    ref_id_set[os.str()] = sum(item_tag);
  }
  return ref_id_set;
}

// clazy:excludeall=non-pod-global-static

TEST_CASE("MedianDualConnectivityBuilder", "[mesh]")
{
  SECTION("2D")
  {
    constexpr static size_t Dimension = 2;

    using MeshType         = Mesh<Dimension>;
    using ConnectivityType = typename MeshType::Connectivity;

    std::shared_ptr<const MeshType> mesh        = MeshDataBaseForTests::get().hybrid2DMesh()->get<MeshType>();
    const ConnectivityType& primal_connectivity = mesh->connectivity();

    REQUIRE(primal_connectivity.numberOfNodes() == 53);
    REQUIRE(primal_connectivity.numberOfFaces() == 110);
    REQUIRE(primal_connectivity.numberOfCells() == 58);

    std::shared_ptr p_median_dual_connectivity =
      DualConnectivityManager::instance().getMedianDualConnectivity(primal_connectivity);
    const ConnectivityType& dual_connectivity = *p_median_dual_connectivity;

    REQUIRE(checkConnectivityOrdering(dual_connectivity));

    REQUIRE(dual_connectivity.numberOfNodes() == 192);
    REQUIRE(dual_connectivity.numberOfFaces() == 244);
    REQUIRE(dual_connectivity.numberOfCells() == 53);

    SECTION("ref node list")
    {
      REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::node>() == 4);
      REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::node>() == 4);

      const auto primal_ref_node_list = get_item_ref_ids<ItemType::node>(primal_connectivity);
      REQUIRE(primal_ref_node_list.size() == 4);

      REQUIRE(primal_ref_node_list.count("XMAXYMAX(11)") == 1);
      REQUIRE(primal_ref_node_list.count("XMAXYMIN(10)") == 1);
      REQUIRE(primal_ref_node_list.count("XMINYMAX(9)") == 1);
      REQUIRE(primal_ref_node_list.count("XMINYMIN(8)") == 1);

      REQUIRE(primal_ref_node_list.at("XMAXYMAX(11)") == 1);
      REQUIRE(primal_ref_node_list.at("XMAXYMIN(10)") == 1);
      REQUIRE(primal_ref_node_list.at("XMINYMAX(9)") == 1);
      REQUIRE(primal_ref_node_list.at("XMINYMIN(8)") == 1);

      const auto dual_ref_node_list = get_item_ref_ids<ItemType::node>(dual_connectivity);
      REQUIRE(dual_ref_node_list.size() == 4);

      REQUIRE(dual_ref_node_list.count("XMAXYMAX(11)") == 1);
      REQUIRE(dual_ref_node_list.count("XMAXYMIN(10)") == 1);
      REQUIRE(dual_ref_node_list.count("XMINYMAX(9)") == 1);
      REQUIRE(dual_ref_node_list.count("XMINYMIN(8)") == 1);

      REQUIRE(dual_ref_node_list.at("XMAXYMAX(11)") == 1);
      REQUIRE(dual_ref_node_list.at("XMAXYMIN(10)") == 1);
      REQUIRE(dual_ref_node_list.at("XMINYMAX(9)") == 1);
      REQUIRE(dual_ref_node_list.at("XMINYMIN(8)") == 1);
    }

    SECTION("ref face list")
    {
      REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::face>() == 5);
      REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::face>() == 4);

      const auto primal_ref_face_list = get_item_ref_ids<ItemType::face>(primal_connectivity);
      REQUIRE(primal_ref_face_list.size() == 5);

      REQUIRE(primal_ref_face_list.count("INTERFACE(5)") == 1);
      REQUIRE(primal_ref_face_list.count("XMAX(2)") == 1);
      REQUIRE(primal_ref_face_list.count("XMIN(1)") == 1);
      REQUIRE(primal_ref_face_list.count("YMAX(3)") == 1);
      REQUIRE(primal_ref_face_list.count("YMIN(4)") == 1);

      REQUIRE(primal_ref_face_list.at("INTERFACE(5)") == 4);
      REQUIRE(primal_ref_face_list.at("XMAX(2)") == 4);
      REQUIRE(primal_ref_face_list.at("XMIN(1)") == 4);
      REQUIRE(primal_ref_face_list.at("YMAX(3)") == 8);
      REQUIRE(primal_ref_face_list.at("YMIN(4)") == 8);

      const auto dual_ref_face_list = get_item_ref_ids<ItemType::face>(dual_connectivity);
      REQUIRE(dual_ref_face_list.size() == 4);

      REQUIRE(dual_ref_face_list.count("XMAX(2)") == 1);
      REQUIRE(dual_ref_face_list.count("XMIN(1)") == 1);
      REQUIRE(dual_ref_face_list.count("YMAX(3)") == 1);
      REQUIRE(dual_ref_face_list.count("YMIN(4)") == 1);

      REQUIRE(dual_ref_face_list.at("XMAX(2)") == 8);
      REQUIRE(dual_ref_face_list.at("XMIN(1)") == 8);
      REQUIRE(dual_ref_face_list.at("YMAX(3)") == 16);
      REQUIRE(dual_ref_face_list.at("YMIN(4)") == 16);
    }

    SECTION("ref cell list")
    {
      REQUIRE(primal_connectivity.numberOfRefItemList<ItemType::cell>() == 2);
      // Median dual cell references are not computed
      REQUIRE(dual_connectivity.numberOfRefItemList<ItemType::cell>() == 0);

      const auto primal_ref_cell_list = get_item_ref_ids<ItemType::cell>(primal_connectivity);
      REQUIRE(primal_ref_cell_list.size() == 2);

      REQUIRE(primal_ref_cell_list.count("LEFT(6)") == 1);
      REQUIRE(primal_ref_cell_list.count("RIGHT(7)") == 1);

      REQUIRE(primal_ref_cell_list.at("LEFT(6)") == 22);
      REQUIRE(primal_ref_cell_list.at("RIGHT(7)") == 36);

      const auto dual_ref_cell_list = get_item_ref_ids<ItemType::cell>(dual_connectivity);
      REQUIRE(dual_ref_cell_list.size() == 0);
    }
  }
}
