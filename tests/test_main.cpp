#include <catch2/catch_all.hpp>

#include <Kokkos_Core.hpp>

#include <analysis/QuadratureManager.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <mesh/DualConnectivityManager.hpp>
#include <mesh/DualMeshManager.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/SynchronizerManager.hpp>
#include <utils/GlobalVariableManager.hpp>
#include <utils/Messenger.hpp>
#include <utils/PETScWrapper.hpp>
#include <utils/RandomEngine.hpp>
#include <utils/SLEPcWrapper.hpp>
#include <utils/checkpointing/ResumingManager.hpp>

#include <MeshDataBaseForTests.hpp>

#include <thread>

int
main(int argc, char* argv[])
{
  parallel::Messenger::create(argc, argv);
  int nb_threads = std::min(std::max(std::thread::hardware_concurrency() / 2, 1u), 8u);

  Catch::Session session;

  auto cli = session.cli() | Catch::Clara::Opt(nb_threads, "number of threads")["--threads"](
                               "number of threads (default: max logical threads)");

  session.cli(cli);

  int result = session.applyCommandLine(argc, argv);

  {
    Kokkos::InitializationSettings args;
    args.set_num_threads(nb_threads);
    args.set_device_id(-1);
    args.set_disable_warnings(true);

    Kokkos::initialize(args);
  }

  // disable file locking to avoid mess in tests
  setenv("HDF5_USE_FILE_LOCKING", "FALSE", 1);

  PETScWrapper::initialize(argc, argv);
  SLEPcWrapper::initialize(argc, argv);

  if (result == 0) {
    const auto& config = session.config();
    if (config.listReporters() or config.listTags() or config.listTests()) {
      result = session.run();
    } else {
      std::cout << "Using " << nb_threads << " threads\n";

      // Disable outputs from tested classes to the standard output
      std::cout.setstate(std::ios::badbit);

      ResumingManager::create();

      SynchronizerManager::create();
      RandomEngine::create();
      QuadratureManager::create();
      MeshDataManager::create();
      DualConnectivityManager::create();
      DualMeshManager::create();
      GlobalVariableManager::create();

      MeshDataBaseForTests::create();

      OperatorRepository::create();

      result = session.run();

      OperatorRepository::destroy();

      MeshDataBaseForTests::destroy();

      GlobalVariableManager::destroy();
      DualMeshManager::destroy();
      DualConnectivityManager::destroy();
      MeshDataManager::destroy();
      QuadratureManager::destroy();
      RandomEngine::destroy();
      SynchronizerManager::destroy();

      ResumingManager::destroy();
    }
  }

  SLEPcWrapper::finalize();
  PETScWrapper::finalize();

  Kokkos::finalize();
  parallel::Messenger::destroy();

  return result;
}
