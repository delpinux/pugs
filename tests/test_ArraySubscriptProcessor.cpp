#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeAffectationExpressionBuilder.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#include <sstream>

#define CHECK_EVALUATION_RESULT(data, variable_name, expected_value)          \
  {                                                                           \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                \
    auto ast = ASTBuilder::build(input);                                      \
                                                                              \
    ASTModulesImporter{*ast};                                                 \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                   \
                                                                              \
    ASTSymbolTableBuilder{*ast};                                              \
    ASTNodeDataTypeBuilder{*ast};                                             \
                                                                              \
    ASTNodeDeclarationToAffectationConverter{*ast};                           \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                      \
    ASTNodeTypeCleaner<language::fct_declaration>{*ast};                      \
                                                                              \
    ASTNodeExpressionBuilder{*ast};                                           \
    ExecutionPolicy exec_policy;                                              \
    ASTExecutionStack::create();                                              \
    ast->execute(exec_policy);                                                \
    ASTExecutionStack::destroy();                                             \
                                                                              \
    auto symbol_table = ast->m_symbol_table;                                  \
                                                                              \
    using namespace TAO_PEGTL_NAMESPACE;                                      \
    position use_position{10000, 1000, 10, "fixture"};                        \
    auto [symbol, found] = symbol_table->find(variable_name, use_position);   \
                                                                              \
    auto attributes = symbol->attributes();                                   \
    auto value      = std::get<decltype(expected_value)>(attributes.value()); \
                                                                              \
    REQUIRE(value == expected_value);                                         \
    ast->m_symbol_table->clearValues();                                       \
  }

#define CHECK_EVALUATION_THROWS_WITH(data, error_message)        \
  {                                                              \
    auto eval = [&] {                                            \
      TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"}; \
      auto ast = ASTBuilder::build(input);                       \
                                                                 \
      ASTModulesImporter{*ast};                                  \
      ASTNodeTypeCleaner<language::import_instruction>{*ast};    \
                                                                 \
      ASTSymbolTableBuilder{*ast};                               \
      ASTNodeDataTypeBuilder{*ast};                              \
                                                                 \
      ASTNodeDeclarationToAffectationConverter{*ast};            \
      ASTNodeTypeCleaner<language::var_declaration>{*ast};       \
      ASTNodeTypeCleaner<language::fct_declaration>{*ast};       \
                                                                 \
      ASTNodeExpressionBuilder{*ast};                            \
      ExecutionPolicy exec_policy;                               \
      ASTExecutionStack::create();                               \
      ast->execute(exec_policy);                                 \
      ASTExecutionStack::destroy();                              \
      ast->m_symbol_table->clearValues();                        \
    };                                                           \
                                                                 \
    REQUIRE_THROWS_WITH(eval(), error_message);                  \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ArraySubscriptProcessor", "[language]")
{
  SECTION("R^1 component access")
  {
    std::string_view data = R"(
let x : R^1, x = 1;
let x0: R, x0 = x[0];
)";
    CHECK_EVALUATION_RESULT(data, "x0", double{1});
  }

  SECTION("R^2 component access")
  {
    std::string_view data = R"(
let x : R^2, x = [1,2];
let x0: R, x0 = x[0];
let x1: R, x1 = x[1];
)";
    CHECK_EVALUATION_RESULT(data, "x0", double{1});
    CHECK_EVALUATION_RESULT(data, "x1", double{2});
  }

  SECTION("R^3 component access")
  {
    std::string_view data = R"(
let x : R^3, x = [1,2,3];
let x0 : R, x0 = x[0];
let x1 : R, x1 = x[1];
let x2 : R, x2 = x[2];
)";
    CHECK_EVALUATION_RESULT(data, "x0", double{1});
    CHECK_EVALUATION_RESULT(data, "x1", double{2});
    CHECK_EVALUATION_RESULT(data, "x2", double{3});
  }

  SECTION("R^1x1 component access")
  {
    std::string_view data = R"(
let x : R^1x1, x = 1;
let x00: R, x00 = x[0,0];
)";
    CHECK_EVALUATION_RESULT(data, "x00", double{1});
  }

  SECTION("R^2x2 component access")
  {
    std::string_view data = R"(
let x : R^2x2, x = [[1,2],[3,4]];
let x00: R, x00 = x[0,0];
let x01: R, x01 = x[0,1];
let x10: R, x10 = x[1,0];
let x11: R, x11 = x[1,1];
)";
    CHECK_EVALUATION_RESULT(data, "x00", double{1});
    CHECK_EVALUATION_RESULT(data, "x01", double{2});
    CHECK_EVALUATION_RESULT(data, "x10", double{3});
    CHECK_EVALUATION_RESULT(data, "x11", double{4});
  }

  SECTION("R^3x3 component access")
  {
    std::string_view data = R"(
let x : R^3x3, x = [[1,2,3],[4,5,6],[7,8,9]];
let x00 : R, x00 = x[0,0];
let x01 : R, x01 = x[0,1];
let x02 : R, x02 = x[0,2];
let x10 : R, x10 = x[1,0];
let x11 : R, x11 = x[1,1];
let x12 : R, x12 = x[1,2];
let x20 : R, x20 = x[2,0];
let x21 : R, x21 = x[2,1];
let x22 : R, x22 = x[2,2];
)";
    CHECK_EVALUATION_RESULT(data, "x00", double{1});
    CHECK_EVALUATION_RESULT(data, "x01", double{2});
    CHECK_EVALUATION_RESULT(data, "x02", double{3});
    CHECK_EVALUATION_RESULT(data, "x10", double{4});
    CHECK_EVALUATION_RESULT(data, "x11", double{5});
    CHECK_EVALUATION_RESULT(data, "x12", double{6});
    CHECK_EVALUATION_RESULT(data, "x20", double{7});
    CHECK_EVALUATION_RESULT(data, "x21", double{8});
    CHECK_EVALUATION_RESULT(data, "x22", double{9});
  }

  SECTION("R^d component access from integer expression")
  {
    std::string_view data = R"(
let x : R^3, x = [1,2,3];
let x0: R,  x0 = x[3-2-1];

let y : R^2, y = [2,7];
let y1: R,  y1 = y[2/2];

let z : R^1, z = 8;
let z0: R,  z0 = z[(2-2)*1];
)";
    CHECK_EVALUATION_RESULT(data, "x0", double{1});
    CHECK_EVALUATION_RESULT(data, "y1", double{7});
    CHECK_EVALUATION_RESULT(data, "z0", double{8});
  }

  SECTION("R^dxd component access from integer expression")
  {
    std::string_view data = R"(
let x : R^3x3, x = [[1,2,3],[4,5,6],[7,8,9]];
let x01: R,  x01 = x[3-2-1,2+3-4];

let y : R^2x2, y = [[2,7],[6,-2]];
let y11: R,  y11 = y[2/2, 3/1-2];

let z : R^1x1, z = 8;
let z00: R,  z00 = z[(2-2)*1, (3-1)*2-4];
)";
    CHECK_EVALUATION_RESULT(data, "x01", double{2});
    CHECK_EVALUATION_RESULT(data, "y11", double{-2});
    CHECK_EVALUATION_RESULT(data, "z00", double{8});
  }

  SECTION("error invalid index type")
  {
    SECTION("R index type")
    {
      std::string_view data = R"(
let x : R^3, x = [1,2,3];
let x0: R,  x0 = x[2.3];
)";

      CHECK_EVALUATION_THROWS_WITH(data, std::string{"invalid implicit conversion: R -> Z"});
    }

    SECTION("string index type")
    {
      std::string_view data = R"(
let x : R^3, x = [1,2,3];
let x0: R,  x0 = x["foo"];
)";

      CHECK_EVALUATION_THROWS_WITH(data, std::string{"invalid implicit conversion: string -> Z"});
    }

    SECTION("R^d index type")
    {
      std::string_view data = R"(
let x : R^3, x = [1,2,3];
let x0: R,  x0 = x[x];
)";

      CHECK_EVALUATION_THROWS_WITH(data, std::string{"invalid implicit conversion: R^3 -> Z"});
    }
  }
}
