#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Messenger.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshVariant.hpp>
#include <utils/GlobalVariableManager.hpp>
#include <utils/checkpointing/ReadMesh.hpp>
#include <utils/checkpointing/ResumingData.hpp>
#include <utils/checkpointing/WriteMesh.hpp>

#include <MeshDataBaseForTests.hpp>
#include <checkpointing_Mesh_utilities.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_Mesh", "[utils/checkpointing]")
{
  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});
    HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

    const size_t initial_connectivity_id = GlobalVariableManager::instance().getConnectivityId();
    const size_t initial_mesh_id         = GlobalVariableManager::instance().getMeshId();

    SECTION("Mesh")
    {
      HighFive::Group checkpoint_group_0 = file.createGroup("checkpoint_0");
      HighFive::Group symbol_table_0     = checkpoint_group_0.createGroup("symbol_table");

      HighFive::Group checkpoint_group_1 = file.createGroup("checkpoint_1");
      HighFive::Group symbol_table_1     = checkpoint_group_1.createGroup("symbol_table");

      {   // Write
        auto mesh_1d = MeshDataBaseForTests::get().unordered1DMesh();

        // creates artificially holes in numbering
        test_only::duplicateMesh(mesh_1d);

        auto new_mesh_1d = test_only::duplicateMesh(mesh_1d);

        using DataHandlerT = DataHandler<const MeshVariant>;

        checkpointing::writeMesh("mesh_1d", EmbeddedData{std::make_shared<DataHandlerT>(new_mesh_1d)}, file,
                                 checkpoint_group_0, symbol_table_0);
        checkpointing::writeMesh("mesh_1d", EmbeddedData{std::make_shared<DataHandlerT>(new_mesh_1d)}, file,
                                 checkpoint_group_1, symbol_table_1);

        auto mesh_2d = MeshDataBaseForTests::get().hybrid2DMesh();

        // creates artificially holes in numbering
        test_only::duplicateMesh(mesh_2d);
        auto new_mesh_2d = test_only::duplicateMesh(mesh_2d);

        checkpointing::writeMesh("mesh_2d", EmbeddedData{std::make_shared<DataHandlerT>(new_mesh_2d)}, file,
                                 checkpoint_group_0, symbol_table_0);
        checkpointing::writeMesh("mesh_2d", EmbeddedData{std::make_shared<DataHandlerT>(new_mesh_2d)}, file,
                                 checkpoint_group_1, symbol_table_1);

        HighFive::Group global_variables_group_0 = checkpoint_group_0.createGroup("singleton/global_variables");
        global_variables_group_0.createAttribute("connectivity_id",
                                                 GlobalVariableManager::instance().getConnectivityId());
        global_variables_group_0.createAttribute("mesh_id", GlobalVariableManager::instance().getMeshId());

        auto mesh_3d = MeshDataBaseForTests::get().hybrid3DMesh();

        // creates artificially holes in numbering
        test_only::duplicateMesh(mesh_3d);

        auto new_mesh_3d = test_only::duplicateMesh(mesh_3d);
        checkpointing::writeMesh("mesh_3d", EmbeddedData{std::make_shared<DataHandlerT>(new_mesh_3d)}, file,
                                 checkpoint_group_1, symbol_table_1);

        // creates artificially holes in numbering
        test_only::duplicateMesh(mesh_3d);
        test_only::duplicateMesh(mesh_3d);
        test_only::duplicateMesh(mesh_3d);

        HighFive::Group global_variables_group_1 = checkpoint_group_1.createGroup("singleton/global_variables");
        global_variables_group_1.createAttribute("connectivity_id",
                                                 GlobalVariableManager::instance().getConnectivityId());
        global_variables_group_1.createAttribute("mesh_id", GlobalVariableManager::instance().getMeshId());
      }

      // reset to reuse after resuming
      GlobalVariableManager::instance().setConnectivityId(initial_connectivity_id);
      GlobalVariableManager::instance().setMeshId(initial_mesh_id);

      file.flush();

      checkpointing::ResumingData::create();
      checkpointing::ResumingData::instance().readData(checkpoint_group_1, nullptr);

      GlobalVariableManager::instance().setConnectivityId(initial_connectivity_id);
      GlobalVariableManager::instance().setMeshId(initial_mesh_id);
      {   // Read

        auto mesh_1d = MeshDataBaseForTests::get().unordered1DMesh();

        EmbeddedData e_mesh_1d = checkpointing::readMesh("mesh_1d", symbol_table_1);

        REQUIRE_NOTHROW(dynamic_cast<const DataHandler<const MeshVariant>&>(e_mesh_1d.get()));

        std::shared_ptr<const MeshVariant> read_mesh_1d =
          dynamic_cast<const DataHandler<const MeshVariant>&>(e_mesh_1d.get()).data_ptr();

        REQUIRE(test_only::isSameMesh(mesh_1d, read_mesh_1d));

        auto mesh_2d = MeshDataBaseForTests::get().hybrid2DMesh();

        EmbeddedData e_mesh_2d = checkpointing::readMesh("mesh_2d", symbol_table_1);

        REQUIRE_NOTHROW(dynamic_cast<const DataHandler<const MeshVariant>&>(e_mesh_2d.get()));

        std::shared_ptr<const MeshVariant> read_mesh_2d =
          dynamic_cast<const DataHandler<const MeshVariant>&>(e_mesh_2d.get()).data_ptr();

        REQUIRE(test_only::isSameMesh(mesh_2d, read_mesh_2d));

        auto mesh_3d = MeshDataBaseForTests::get().hybrid3DMesh();

        EmbeddedData e_mesh_3d = checkpointing::readMesh("mesh_3d", symbol_table_1);

        REQUIRE_NOTHROW(dynamic_cast<const DataHandler<const MeshVariant>&>(e_mesh_3d.get()));

        std::shared_ptr<const MeshVariant> read_mesh_3d =
          dynamic_cast<const DataHandler<const MeshVariant>&>(e_mesh_3d.get()).data_ptr();

        REQUIRE(test_only::isSameMesh(mesh_3d, read_mesh_3d));
      }
      checkpointing::ResumingData::destroy();
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }
}
