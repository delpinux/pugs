#include <catch2/catch_all.hpp>
#include <catch2/catch_test_macros.hpp>

#include <utils/PartitionerOptions.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("PartitionerOptions", "[utils]")
{
  SECTION("name")
  {
    REQUIRE(name(PartitionerLibrary::parmetis) == "ParMETIS");
    REQUIRE(name(PartitionerLibrary::ptscotch) == "PTScotch");
    REQUIRE_THROWS_WITH(name(PartitionerLibrary::PT__end),
                        "unexpected error: Partitioner library name is not defined!");
  }

  SECTION("type from name")
  {
    REQUIRE(getPartitionerEnumFromName<PartitionerLibrary>("ParMETIS") == PartitionerLibrary::parmetis);
    REQUIRE(getPartitionerEnumFromName<PartitionerLibrary>("PTScotch") == PartitionerLibrary::ptscotch);

    REQUIRE_THROWS_WITH(getPartitionerEnumFromName<PartitionerLibrary>("foobar"),
                        "error: could not find 'foobar' associate type!");
  }

  SECTION("output")
  {
    {
      PartitionerOptions options;
      options.library() = PartitionerLibrary::parmetis;
      std::stringstream os;
      os << options;
      REQUIRE(os.str() == "  library: ParMETIS\n");
    }

    {
      PartitionerOptions options;
      options.library() = PartitionerLibrary::ptscotch;
      std::stringstream os;
      os << options;
      REQUIRE(os.str() == "  library: PTScotch\n");
    }
  }
}
