#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/SubItemArrayPerItem.hpp>
#include <mesh/SubItemArrayPerItemUtils.hpp>
#include <utils/Messenger.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("SubItemArrayPerItemUtils", "[mesh]")
{
  SECTION("Synchronize")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

        const Connectivity<2>& connectivity = mesh_2d->connectivity();

        NodeArrayPerFace<int> weak_node_array_per_face{connectivity, 3};

        for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
          auto face_table = weak_node_array_per_face.itemTable(face_id);
          for (size_t i_node = 0; i_node < face_table.numberOfRows(); ++i_node) {
            for (size_t l = 0; l < face_table.numberOfColumns(); ++l) {
              face_table(i_node, l) = parallel::rank() + 2 * i_node + l;
            }
          }
        }

        NodeArrayPerFace<const int> node_array_per_face{weak_node_array_per_face};

        REQUIRE(node_array_per_face.connectivity_ptr() == weak_node_array_per_face.connectivity_ptr());

        if (parallel::size() > 1) {
          // before synchronization
          auto face_owner = connectivity.faceOwner();

          bool is_synchronized = true;
          for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
            auto face_table = node_array_per_face.itemTable(face_id);
            for (size_t i_node = 0; i_node < face_table.numberOfRows(); ++i_node) {
              for (size_t l = 0; l < face_table.numberOfColumns(); ++l) {
                if (face_table(i_node, l) != static_cast<int>(face_owner[face_id] + 2 * i_node + l)) {
                  is_synchronized = false;
                  break;
                }
              }
            }
          }

          REQUIRE(not is_synchronized);
          REQUIRE(not isSynchronized(node_array_per_face));
        }

        synchronize(weak_node_array_per_face);

        {   // after synchronization
          auto face_owner = connectivity.faceOwner();

          bool is_synchronized = true;
          for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
            auto face_table = node_array_per_face.itemTable(face_id);
            for (size_t i_node = 0; i_node < face_table.numberOfRows(); ++i_node) {
              for (size_t l = 0; l < face_table.numberOfColumns(); ++l) {
                if (face_table(i_node, l) != static_cast<int>(face_owner[face_id] + 2 * i_node + l)) {
                  is_synchronized = false;
                }
              }
            }
          }
          REQUIRE(is_synchronized);
        }
        REQUIRE(isSynchronized(node_array_per_face));
      }
    }
  }

  SECTION("min")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d = named_mesh.mesh()->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          NodeArrayPerCell<int> node_array_per_cell{connectivity, 3};
          node_array_per_cell.fill(-1);

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            connectivity.numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                auto cell_table = node_array_per_cell.itemTable(cell_id);
                for (size_t i = 0; i < cell_table.numberOfRows(); ++i) {
                  for (size_t j = 0; j < cell_table.numberOfColumns(); ++j) {
                    cell_table(i, j) = 10 + parallel::rank() + i + 2 * j;
                  }
                }
              }
            });

          REQUIRE(min(node_array_per_cell) == 10);
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          FaceArrayPerCell<int> face_array_per_cell{connectivity, 3};
          face_array_per_cell.fill(-1);

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            connectivity.numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                auto cell_table = face_array_per_cell.itemTable(cell_id);
                for (size_t i = 0; i < cell_table.numberOfRows(); ++i) {
                  for (size_t j = 0; j < cell_table.numberOfColumns(); ++j) {
                    cell_table(i, j) = 10 + parallel::rank() + i + j;
                  }
                }
              }
            });

          REQUIRE(min(face_array_per_cell) == 10);
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          EdgeArrayPerFace<int> edge_array_per_face{connectivity, 3};
          edge_array_per_face.fill(-1);

          auto face_is_owned = connectivity.faceIsOwned();
          parallel_for(
            connectivity.numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
              if (face_is_owned[face_id]) {
                auto face_table = edge_array_per_face.itemTable(face_id);
                for (size_t i = 0; i < face_table.numberOfRows(); ++i) {
                  for (size_t j = 0; j < face_table.numberOfColumns(); ++j) {
                    face_table(i, j) = 10 + parallel::rank() + i + j;
                  }
                }
              }
            });

          REQUIRE(min(edge_array_per_face) == 10);
        }
      }
    }
  }

  SECTION("max")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d = named_mesh.mesh()->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          EdgeArrayPerCell<size_t> edge_array_per_cell{connectivity, 3};
          edge_array_per_cell.fill(std::numeric_limits<size_t>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            connectivity.numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                auto cell_table = edge_array_per_cell.itemTable(cell_id);
                for (size_t i = 0; i < cell_table.numberOfRows(); ++i) {
                  for (size_t j = 0; j < cell_table.numberOfColumns(); ++j) {
                    cell_table(i, j) = 10 + parallel::rank() - i - j;
                  }
                }
              }
            });

          REQUIRE(max(edge_array_per_cell) == 9 + parallel::size());
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          EdgeArrayPerCell<size_t> edge_array_per_cell{connectivity, 3};
          edge_array_per_cell.fill(std::numeric_limits<size_t>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            connectivity.numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                auto cell_table = edge_array_per_cell.itemTable(cell_id);
                for (size_t i = 0; i < cell_table.numberOfRows(); ++i) {
                  for (size_t j = 0; j < cell_table.numberOfColumns(); ++j) {
                    cell_table(i, j) = 10 + parallel::rank() - i - j;
                  }
                }
              }
            });

          REQUIRE(max(edge_array_per_cell) == 9 + parallel::size());
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          NodeArrayPerEdge<size_t> node_array_per_edge{connectivity, 3};
          node_array_per_edge.fill(std::numeric_limits<size_t>::max());

          auto edge_is_owned = connectivity.edgeIsOwned();

          parallel_for(
            connectivity.numberOfEdges(), PUGS_LAMBDA(EdgeId edge_id) {
              if (edge_is_owned[edge_id]) {
                auto edge_table = node_array_per_edge.itemTable(edge_id);
                for (size_t i = 0; i < edge_table.numberOfRows(); ++i) {
                  for (size_t j = 0; j < edge_table.numberOfColumns(); ++j) {
                    edge_table(i, j) = 10 + parallel::rank() - i - j;
                  }
                }
              }
            });

          REQUIRE(max(node_array_per_edge) == 9 + parallel::size());
        }
      }
    }

    SECTION("max of only negative values")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          NodeArrayPerEdge<double> node_array_per_edge{connectivity, 3};
          node_array_per_edge.fill(std::numeric_limits<double>::max());

          auto edge_is_owned = connectivity.edgeIsOwned();

          parallel_for(
            connectivity.numberOfEdges(), PUGS_LAMBDA(EdgeId edge_id) {
              if (edge_is_owned[edge_id]) {
                auto edge_table = node_array_per_edge.itemTable(edge_id);
                for (size_t i = 0; i < edge_table.numberOfRows(); ++i) {
                  for (size_t j = 0; j < edge_table.numberOfColumns(); ++j) {
                    edge_table(i, j) = -10. - parallel::size() + parallel::rank() - i - j;
                  }
                }
              }
            });

          REQUIRE(max(node_array_per_edge) == -11.);
        }
      }
    }
  }

  SECTION("sum")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d = named_mesh.mesh()->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          NodeArrayPerCell<size_t> node_array_per_cell{connectivity, 3};
          node_array_per_cell.fill(5);

          auto cell_is_owned = connectivity.cellIsOwned();

          const size_t global_number_of_nodes_per_cells = [&] {
            size_t number_of_nodes_per_cells = 0;
            for (CellId cell_id = 0; cell_id < cell_is_owned.numberOfItems(); ++cell_id) {
              number_of_nodes_per_cells += cell_is_owned[cell_id] * node_array_per_cell.numberOfSubArrays(cell_id);
            }
            return parallel::allReduceSum(number_of_nodes_per_cells);
          }();

          REQUIRE(sum(node_array_per_cell) ==
                  5 * global_number_of_nodes_per_cells * node_array_per_cell.sizeOfArrays());
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name() + " for size_t data")
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          NodeArrayPerFace<size_t> node_array_per_face{connectivity, 3};
          node_array_per_face.fill(2);

          auto face_is_owned = connectivity.faceIsOwned();

          const size_t global_number_of_nodes_per_faces = [&] {
            size_t number_of_nodes_per_faces = 0;
            for (FaceId face_id = 0; face_id < face_is_owned.numberOfItems(); ++face_id) {
              number_of_nodes_per_faces += face_is_owned[face_id] * node_array_per_face.numberOfSubArrays(face_id);
            }
            return parallel::allReduceSum(number_of_nodes_per_faces);
          }();

          REQUIRE(sum(node_array_per_face) ==
                  2 * global_number_of_nodes_per_faces * node_array_per_face.sizeOfArrays());
        }

        SECTION(named_mesh.name() + " for N^2 data")
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          using N2 = TinyVector<2, size_t>;
          NodeArrayPerFace<N2> node_array_per_face{connectivity, 3};

          const N2 data(3, 2);
          node_array_per_face.fill(data);

          auto face_is_owned = connectivity.faceIsOwned();

          const size_t global_number_of_nodes_per_faces = [&] {
            size_t number_of_nodes_per_faces = 0;
            for (FaceId face_id = 0; face_id < face_is_owned.numberOfItems(); ++face_id) {
              number_of_nodes_per_faces += face_is_owned[face_id] * node_array_per_face.numberOfSubArrays(face_id);
            }
            return parallel::allReduceSum(number_of_nodes_per_faces);
          }();

          REQUIRE(sum(node_array_per_face) ==
                  global_number_of_nodes_per_faces * node_array_per_face.sizeOfArrays() * data);
        }

        SECTION(named_mesh.name() + " for float data")
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          EdgeArrayPerCell<float> edge_array_per_cell{connectivity, 3};
          edge_array_per_cell.fill(1E10);

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            connectivity.numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                auto cell_table = edge_array_per_cell.itemTable(cell_id);
                for (size_t i = 0; i < cell_table.numberOfRows(); ++i) {
                  for (size_t j = 0; j < cell_table.numberOfColumns(); ++j) {
                    cell_table(i, j) = 10 + parallel::rank() - i - j + 1.5;
                  }
                }
              }
            });

          CellValue<float> cell_sum(connectivity);
          cell_sum.fill(0);
          parallel_for(
            connectivity.numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              auto cell_table = edge_array_per_cell.itemTable(cell_id);
              for (size_t i = 0; i < cell_table.numberOfRows(); ++i) {
                for (size_t j = 0; j < cell_table.numberOfColumns(); ++j) {
                  cell_sum[cell_id] += cell_table(i, j);
                }
              }
            });

          REQUIRE(sum(edge_array_per_cell) == sum(cell_sum));
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name() + " for size_t data")
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          EdgeArrayPerFace<size_t> edge_array_per_face{connectivity, 3};
          edge_array_per_face.fill(3);

          auto face_is_owned = connectivity.faceIsOwned();

          const size_t global_number_of_edges_per_faces = [&] {
            size_t number_of_edges_per_faces = 0;
            for (FaceId face_id = 0; face_id < face_is_owned.numberOfItems(); ++face_id) {
              number_of_edges_per_faces += face_is_owned[face_id] * edge_array_per_face.numberOfSubArrays(face_id);
            }
            return parallel::allReduceSum(number_of_edges_per_faces);
          }();

          REQUIRE(sum(edge_array_per_face) ==
                  3 * global_number_of_edges_per_faces * edge_array_per_face.sizeOfArrays());
        }

        SECTION(named_mesh.name() + " for N^2x3 data")
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          using R2x3 = TinyMatrix<2, 3, size_t>;
          EdgeArrayPerFace<R2x3> edge_array_per_face{connectivity, 3};

          const R2x3 data(1, 3, 4, 6, 2, 5);
          edge_array_per_face.fill(data);

          auto face_is_owned = connectivity.faceIsOwned();

          const size_t global_number_of_edges_per_faces = [&] {
            size_t number_of_edges_per_faces = 0;
            for (FaceId face_id = 0; face_id < face_is_owned.numberOfItems(); ++face_id) {
              number_of_edges_per_faces += face_is_owned[face_id] * edge_array_per_face.numberOfSubArrays(face_id);
            }
            return parallel::allReduceSum(number_of_edges_per_faces);
          }();

          REQUIRE(sum(edge_array_per_face) ==
                  global_number_of_edges_per_faces * edge_array_per_face.sizeOfArrays() * data);
        }

        SECTION(named_mesh.name() + " for R^2x3 data")
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          using R2x3 = TinyMatrix<2, 3, double>;
          EdgeArrayPerFace<R2x3> edge_array_per_face{connectivity, 3};

          auto Nl = MeshDataManager::instance().getMeshData(*mesh_3d).Nl();

          parallel_for(
            mesh_3d->numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
              for (size_t i_edge = 0; i_edge < edge_array_per_face.numberOfSubArrays(face_id); ++i_edge) {
                for (size_t i = 0; i < edge_array_per_face[face_id][i_edge].size(); ++i) {
                  R2x3 value((2.3 * i_edge + 1.7) * Nl[face_id][0],                    //
                             (3. - i_edge) * Nl[face_id][1],                           //
                             (2 * i_edge + 1.3) * Nl[face_id][2],                      //
                             (i_edge + 1.7) * Nl[face_id][0] + 3.2 * Nl[face_id][1],   //
                             Nl[face_id][2] + 3 * i_edge,                              //
                             7.13 * i_edge * i_edge * l2Norm(Nl[face_id]));
                  edge_array_per_face[face_id][i_edge][i] = (i + 1.3) * value;
                }
              }
            });

          FaceValue<R2x3> face_value{connectivity};
          face_value.fill(zero);
          parallel_for(
            mesh_3d->numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
              for (size_t i_edge = 0; i_edge < edge_array_per_face.numberOfSubArrays(face_id); ++i_edge) {
                for (size_t i = 0; i < edge_array_per_face[face_id][i_edge].size(); ++i) {
                  face_value[face_id] += edge_array_per_face[face_id][i_edge][i];
                }
              }
            });

          REQUIRE(sum(edge_array_per_face) == sum(face_value));
        }
      }
    }
  }
}
