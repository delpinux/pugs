#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

#define CHECK_AST(data, expected_output)                                                            \
  {                                                                                                 \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);                  \
    static_assert((std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view>) or    \
                  (std::is_same_v<std::decay_t<decltype(expected_output)>, std::string>));          \
                                                                                                    \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                      \
    auto ast = ASTBuilder::build(input);                                                            \
                                                                                                    \
    ASTModulesImporter{*ast};                                                                       \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                         \
                                                                                                    \
    ASTSymbolTableBuilder{*ast};                                                                    \
    ASTNodeDataTypeBuilder{*ast};                                                                   \
                                                                                                    \
    ASTNodeDeclarationToAffectationConverter{*ast};                                                 \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                            \
                                                                                                    \
    ASTNodeExpressionBuilder{*ast};                                                                 \
    std::stringstream ast_output;                                                                   \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::exec_type}}; \
                                                                                                    \
    REQUIRE(ast_output.str() == expected_output);                                                   \
    ast->m_symbol_table->clearValues();                                                             \
  }

#define CHECK_AST_THROWS_WITH(data, error_message)                    \
  {                                                                   \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};        \
    auto ast = ASTBuilder::build(input);                              \
                                                                      \
    ASTModulesImporter{*ast};                                         \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};           \
                                                                      \
    ASTSymbolTableBuilder{*ast};                                      \
                                                                      \
    REQUIRE_THROWS_WITH(ASTNodeDataTypeBuilder{*ast}, error_message); \
    ast->m_symbol_table->clearValues();                               \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeExpressionBuilder", "[language]")
{
  SECTION("empty file")
  {
    std::string_view data = R"(
)";

    std::string_view result = R"(
(root:ASTNodeListProcessor)
)";

    CHECK_AST(data, result);
  }

  SECTION("values")
  {
    SECTION("integer")
    {
      std::string_view data = R"(
3;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::integer:3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("real")
    {
      std::string_view data = R"(
2.3e-5;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::real:2.3e-5:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("literal")
    {
      std::string_view data = R"(
"foo";
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::literal:"foo":ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("true")
    {
      std::string_view data = R"(
true;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("false")
    {
      std::string_view data = R"(
false;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::false_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("block")
  {
    std::string_view data = R"(
{
 1;
}
)";

    std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::block:ASTNodeListProcessor)
     `-(language::integer:1:ValueProcessor)
)";

    CHECK_AST(data, result);
  }

  SECTION("name")
  {
    std::string_view data = R"(
let i:N;
i;
)";

    std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::name:i:NameProcessor)
)";

    CHECK_AST(data, result);
  }

  SECTION("affectations")
  {
    SECTION("operator=")
    {
      std::string_view data = R"(
let i:N, i = 1;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
     +-(language::name:i:NameProcessor)
     `-(language::integer:1:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("operator*=")
    {
      std::string_view data = R"(
let i:N, i = 1;
i *= 3;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:i:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::multiplyeq_op:AffectationProcessor<language::multiplyeq_op, unsigned long, long>)
     +-(language::name:i:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("operator/=")
    {
      std::string_view data = R"(
let x:R, x = 1;
x /= 2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, double, long>)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::divideeq_op:AffectationProcessor<language::divideeq_op, double, long>)
     +-(language::name:x:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("operator+=")
    {
      std::string_view data = R"(
let i:N, i = 1;
i += 3;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:i:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::pluseq_op:AffectationProcessor<language::pluseq_op, unsigned long, long>)
     +-(language::name:i:NameProcessor)
     `-(language::integer:3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("operator-=")
    {
      std::string_view data = R"(
let z:Z, z = 1;
z -= 2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, long, long>)
 |   +-(language::name:z:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::minuseq_op:AffectationProcessor<language::minuseq_op, long, long>)
     +-(language::name:z:NameProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("tuple -> R^3")
    {
      std::string_view data = R"(
let  (t,x): R*R^3, (t,x) = (0,[1,2,3]);
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:ListAffectationProcessor<language::eq_op>)
     +-(language::name_list:FakeProcessor)
     |   +-(language::name:t:NameProcessor)
     |   `-(language::name:x:NameProcessor)
     `-(language::expression_list:ASTNodeExpressionListProcessor)
         +-(language::integer:0:ValueProcessor)
         `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
             +-(language::integer:1:ValueProcessor)
             +-(language::integer:2:ValueProcessor)
             `-(language::integer:3:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("tuple -> R^2")
    {
      std::string_view data = R"(
let (t,x): R*R^2, (t,x) = (0,[1,2]);
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eq_op:ListAffectationProcessor<language::eq_op>)
     +-(language::name_list:FakeProcessor)
     |   +-(language::name:t:NameProcessor)
     |   `-(language::name:x:NameProcessor)
     `-(language::expression_list:ASTNodeExpressionListProcessor)
         +-(language::integer:0:ValueProcessor)
         `-(language::vector_expression:TinyVectorExpressionProcessor<2ul>)
             +-(language::integer:1:ValueProcessor)
             `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("unary operators")
  {
    SECTION("unary minus for B")
    {
      std::string_view data = R"(
-true;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, bool>)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("unary minus for N")
    {
      std::string_view data = R"(
let n:N;
-n;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, unsigned long>)
     `-(language::name:n:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("unary minus for Z")
    {
      std::string_view data = R"(
-1;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, long, long>)
     `-(language::integer:1:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("unary minus for R")
    {
      std::string_view data = R"(
-1.2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::unary_minus:UnaryExpressionProcessor<language::unary_minus, double, double>)
     `-(language::real:1.2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("unary not")
    {
      std::string error_message = R"(undefined unary operator
note: unexpected operand type Z)";

      CHECK_AST_THROWS_WITH(R"(not 1;)", error_message);
    }

    SECTION("pre-increment operator")
    {
      std::string_view data = R"(
let a:Z, a=1;
++a;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, long, long>)
 |   +-(language::name:a:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::unary_plusplus:IncDecExpressionProcessor<language::unary_plusplus, long>)
     `-(language::name:a:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("pre-decrement operator")
    {
      std::string_view data = R"(
let a:Z, a=1;
--a;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, long, long>)
 |   +-(language::name:a:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::unary_minusminus:IncDecExpressionProcessor<language::unary_minusminus, long>)
     `-(language::name:a:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("post-increment operator")
    {
      std::string_view data = R"(
let a:Z, a=1;
a++;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, long, long>)
 |   +-(language::name:a:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::post_plusplus:IncDecExpressionProcessor<language::post_plusplus, long>)
     `-(language::name:a:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("post-decrement operator")
    {
      std::string_view data = R"(
let a:Z, a=1;
a--;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, long, long>)
 |   +-(language::name:a:NameProcessor)
 |   `-(language::integer:1:ValueProcessor)
 `-(language::post_minusminus:IncDecExpressionProcessor<language::post_minusminus, long>)
     `-(language::name:a:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("array subscript")
    {
      std::string_view data = R"(
let x: R^3, x = [1, 2, 3];
let y: R, y = x[2];
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, TinyVector<3ul, double>, TinyVector<3ul, double> >)
 |   +-(language::name:x:NameProcessor)
 |   `-(language::vector_expression:TinyVectorExpressionProcessor<3ul>)
 |       +-(language::integer:1:ValueProcessor)
 |       +-(language::integer:2:ValueProcessor)
 |       `-(language::integer:3:ValueProcessor)
 `-(language::eq_op:AffectationProcessor<language::eq_op, double, double>)
     +-(language::name:y:NameProcessor)
     `-(language::subscript_expression:ArraySubscriptProcessor<TinyVector<3ul, double> >)
         +-(language::name:x:NameProcessor)
         `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("binary operators")
  {
    SECTION("multiply")
    {
      std::string_view data = R"(
1*2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::multiply_op:BinaryExpressionProcessor<language::multiply_op, long, long, long>)
     +-(language::integer:1:ValueProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("divide")
    {
      std::string_view data = R"(
1/2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::divide_op:BinaryExpressionProcessor<language::divide_op, long, long, long>)
     +-(language::integer:1:ValueProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("plus")
    {
      std::string_view data = R"(
1+2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::plus_op:BinaryExpressionProcessor<language::plus_op, long, long, long>)
     +-(language::integer:1:ValueProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("minus")
    {
      std::string_view data = R"(
1-2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::minus_op:BinaryExpressionProcessor<language::minus_op, long, long, long>)
     +-(language::integer:1:ValueProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("or")
    {
      const std::string error_message = R"(undefined binary operator
note: incompatible operand types Z and Z)";

      CHECK_AST_THROWS_WITH(R"(1 or 2;)", error_message);
    }

    SECTION("and")
    {
      const std::string error_message = R"(undefined binary operator
note: incompatible operand types Z and Z)";

      CHECK_AST_THROWS_WITH(R"(1 and 2;)", error_message);
    }

    SECTION("xor")
    {
      const std::string error_message = R"(undefined binary operator
note: incompatible operand types Z and Z)";

      CHECK_AST_THROWS_WITH(R"(1 xor 2;)", error_message);
    }

    SECTION("lesser")
    {
      std::string_view data = R"(
1 < 2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::lesser_op:BinaryExpressionProcessor<language::lesser_op, bool, long, long>)
     +-(language::integer:1:ValueProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("lesser or equal")
    {
      std::string_view data = R"(
1 <= 2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::lesser_or_eq_op:BinaryExpressionProcessor<language::lesser_or_eq_op, bool, long, long>)
     +-(language::integer:1:ValueProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("greater")
    {
      std::string_view data = R"(
1 > 2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::greater_op:BinaryExpressionProcessor<language::greater_op, bool, long, long>)
     +-(language::integer:1:ValueProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("greater or equal")
    {
      std::string_view data = R"(
1 >= 2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::greater_or_eq_op:BinaryExpressionProcessor<language::greater_or_eq_op, bool, long, long>)
     +-(language::integer:1:ValueProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("equal")
    {
      std::string_view data = R"(
1 == 2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::eqeq_op:BinaryExpressionProcessor<language::eqeq_op, bool, long, long>)
     +-(language::integer:1:ValueProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("different")
    {
      std::string_view data = R"(
1 != 2;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::not_eq_op:BinaryExpressionProcessor<language::not_eq_op, bool, long, long>)
     +-(language::integer:1:ValueProcessor)
     `-(language::integer:2:ValueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("ostream objects")
  {
    SECTION("cout")
    {
      std::string_view data = R"(
cout;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::name:cout:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("cerr")
    {
      std::string_view data = R"(
cerr;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::name:cerr:NameProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("clog")
    {
      std::string_view data = R"(
clog;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::name:clog:NameProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("statements")
  {
    SECTION("if")
    {
      std::string_view data = R"(
if(true);
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::if_statement:IfProcessor)
     +-(language::true_kw:ValueProcessor)
     `-(language::statement_block:ASTNodeListProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("do while")
    {
      std::string_view data = R"(
do; while(true);
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::do_while_statement:DoWhileProcessor)
     +-(language::statement_block:ASTNodeListProcessor)
     `-(language::true_kw:ValueProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("while")
    {
      std::string_view data = R"(
while(true);
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::while_statement:WhileProcessor)
     +-(language::true_kw:ValueProcessor)
     `-(language::statement_block:ASTNodeListProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("for")
    {
      SECTION("for_statement")
      {
        std::string_view data = R"(
for(let i:N, i=0; i<10; ++i);
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::for_statement:ForProcessor)
     +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
     |   +-(language::name:i:NameProcessor)
     |   `-(language::integer:0:ValueProcessor)
     +-(language::lesser_op:BinaryExpressionProcessor<language::lesser_op, bool, unsigned long, long>)
     |   +-(language::name:i:NameProcessor)
     |   `-(language::integer:10:ValueProcessor)
     +-(language::unary_plusplus:IncDecExpressionProcessor<language::unary_plusplus, unsigned long>)
     |   `-(language::name:i:NameProcessor)
     `-(language::for_statement_block:ASTNodeListProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("no init")
      {
        std::string_view data = R"(
let i:N, i=0;
for(; i<10; ++i);
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
 |   +-(language::name:i:NameProcessor)
 |   `-(language::integer:0:ValueProcessor)
 `-(language::for_statement:ForProcessor)
     +-(language::for_init:FakeProcessor)
     +-(language::lesser_op:BinaryExpressionProcessor<language::lesser_op, bool, unsigned long, long>)
     |   +-(language::name:i:NameProcessor)
     |   `-(language::integer:10:ValueProcessor)
     +-(language::unary_plusplus:IncDecExpressionProcessor<language::unary_plusplus, unsigned long>)
     |   `-(language::name:i:NameProcessor)
     `-(language::for_statement_block:ASTNodeListProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("no test")
      {
        std::string_view data = R"(
for(let i:N, i=0; ; ++i);
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::for_statement:ForProcessor)
     +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
     |   +-(language::name:i:NameProcessor)
     |   `-(language::integer:0:ValueProcessor)
     +-(language::for_test:ValueProcessor)
     +-(language::unary_plusplus:IncDecExpressionProcessor<language::unary_plusplus, unsigned long>)
     |   `-(language::name:i:NameProcessor)
     `-(language::for_statement_block:ASTNodeListProcessor)
)";

        CHECK_AST(data, result);
      }

      SECTION("no post instruction")
      {
        std::string_view data = R"(
for(let i:N, i=0; i<10;);
)";

        std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::for_statement:ForProcessor)
     +-(language::eq_op:AffectationProcessor<language::eq_op, unsigned long, long>)
     |   +-(language::name:i:NameProcessor)
     |   `-(language::integer:0:ValueProcessor)
     +-(language::lesser_op:BinaryExpressionProcessor<language::lesser_op, bool, unsigned long, long>)
     |   +-(language::name:i:NameProcessor)
     |   `-(language::integer:10:ValueProcessor)
     +-(language::for_post:FakeProcessor)
     `-(language::for_statement_block:ASTNodeListProcessor)
)";

        CHECK_AST(data, result);
      }
    }
  }

  SECTION("jumps instruction")
  {
    SECTION("break")
    {
      std::string_view data = R"(
break;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::break_kw:BreakProcessor)
)";

      CHECK_AST(data, result);
    }

    SECTION("continue")
    {
      std::string_view data = R"(
continue;
)";

      std::string result = R"(
(root:ASTNodeListProcessor)
 `-(language::continue_kw:ContinueProcessor)
)";

      CHECK_AST(data, result);
    }
  }

  SECTION("unexpected node type")
  {
    std::string_view data = R"(
1;
)";

    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
    auto ast = ASTBuilder::build(input);

    ASTModulesImporter{*ast};
    ASTNodeTypeCleaner<language::import_instruction>{*ast};

    ASTSymbolTableBuilder{*ast};
    ASTNodeDataTypeBuilder{*ast};

    // One is sure that language::ignored is not treated so its a good candidate
    // for this test
    ast->children[0]->set_type<language::ignored>();
    REQUIRE_THROWS_AS(ASTNodeExpressionBuilder{*ast}, ParseError);
    ast->m_symbol_table->clearValues();
  }
}
