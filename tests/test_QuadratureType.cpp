#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <analysis/QuadratureType.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("QuadratureType", "[analysis]")
{
  SECTION("name")
  {
    REQUIRE(name(QuadratureType::Gauss) == "Gauss");
    REQUIRE(name(QuadratureType::GaussLegendre) == "Gauss-Legendre");
    REQUIRE(name(QuadratureType::GaussLobatto) == "Gauss-Lobatto");
  }
}
