#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/node_processor/FunctionArgumentConverter.hpp>
#include <language/utils/SymbolTable.hpp>
#include <utils/Stringify.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("FunctionArgumentConverter", "[language]")
{
  ExecutionPolicy::Context context(0, std::make_shared<ExecutionPolicy::Context::Values>(3));
  ExecutionPolicy execution_policy(ExecutionPolicy{}, context);

  SECTION("FunctionArgumentToStringConverter")
  {
    const std::string s{"foo"};
    FunctionArgumentToStringConverter converter0{0};
    converter0.convert(execution_policy, s);

    const TinyVector<3> X{1, 3.2, 4};
    FunctionArgumentToStringConverter converter1{1};
    converter1.convert(execution_policy, X);
    std::ostringstream os_X;
    os_X << X;

    const double x = 3.2;
    FunctionArgumentToStringConverter converter2{2};
    converter2.convert(execution_policy, x);

    REQUIRE(std::get<std::string>(execution_policy.currentContext()[0]) == s);
    REQUIRE(std::get<std::string>(execution_policy.currentContext()[1]) == os_X.str());
    REQUIRE(std::get<std::string>(execution_policy.currentContext()[2]) == stringify(x));
  }

  SECTION("FunctionArgumentConverter")
  {
    const double double_value = 1.7;
    FunctionArgumentConverter<double, double> converter0{0};
    converter0.convert(execution_policy, double{double_value});

    const uint64_t uint64_value = 3;
    FunctionArgumentConverter<double, uint64_t> converter1{1};
    converter1.convert(execution_policy, uint64_value);

    const bool bool_value = false;
    FunctionArgumentConverter<uint64_t, bool> converter2{2};
    converter2.convert(execution_policy, bool_value);

    REQUIRE(std::get<double>(execution_policy.currentContext()[0]) == double_value);
    REQUIRE(std::get<double>(execution_policy.currentContext()[1]) == static_cast<double>(uint64_value));
    REQUIRE(std::get<uint64_t>(execution_policy.currentContext()[2]) == static_cast<uint64_t>(bool_value));
  }

  SECTION("FunctionTinyVectorArgumentConverter")
  {
    const TinyVector<3> x3{1.7, 2.9, -3};
    FunctionTinyVectorArgumentConverter<TinyVector<3>, TinyVector<3>> converter0{0};
    converter0.convert(execution_policy, TinyVector{x3});

    const double x1 = 6.3;
    FunctionTinyVectorArgumentConverter<TinyVector<1>, double> converter1{1};
    converter1.convert(execution_policy, double{x1});

    AggregateDataVariant values{std::vector<DataVariant>{6.3, 3.2, 4ul}};
    FunctionTinyVectorArgumentConverter<TinyVector<3>, TinyVector<3>> converter2{2};
    converter2.convert(execution_policy, values);

    REQUIRE(std::get<TinyVector<3>>(execution_policy.currentContext()[0]) == x3);
    REQUIRE(std::get<TinyVector<1>>(execution_policy.currentContext()[1]) == TinyVector<1>{x1});
    REQUIRE(std::get<TinyVector<3>>(execution_policy.currentContext()[2]) == TinyVector<3>{6.3, 3.2, 4ul});

    AggregateDataVariant bad_values{std::vector<DataVariant>{6.3, 3.2, std::string{"bar"}}};

    REQUIRE_THROWS_WITH(converter2.convert(execution_policy, bad_values), std::string{"unexpected error: "} +
                                                                            demangle<std::string>() +
                                                                            " unexpected aggregate value type");
  }

  SECTION("FunctionTinyMatrixArgumentConverter")
  {
    const TinyMatrix<3> x3{1.7, 2.9, -3, 4, 5.2, 6.1, -7, 8.3, 9.05};
    FunctionTinyMatrixArgumentConverter<TinyMatrix<3>, TinyMatrix<3>> converter0{0};
    converter0.convert(execution_policy, TinyMatrix{x3});

    const double x1 = 6.3;
    FunctionTinyMatrixArgumentConverter<TinyMatrix<1>, double> converter1{1};
    converter1.convert(execution_policy, double{x1});

    AggregateDataVariant values{std::vector<DataVariant>{6.3, 3.2, 4ul, 2.3, -3.1, 6.7, 3.6, 2ul, 1.1}};
    FunctionTinyMatrixArgumentConverter<TinyMatrix<3>, TinyMatrix<3>> converter2{2};
    converter2.convert(execution_policy, values);

    REQUIRE(std::get<TinyMatrix<3>>(execution_policy.currentContext()[0]) == x3);
    REQUIRE(std::get<TinyMatrix<1>>(execution_policy.currentContext()[1]) == TinyMatrix<1>{x1});
    REQUIRE(std::get<TinyMatrix<3>>(execution_policy.currentContext()[2]) ==
            TinyMatrix<3>{6.3, 3.2, 4ul, 2.3, -3.1, 6.7, 3.6, 2ul, 1.1});

    AggregateDataVariant bad_values{std::vector<DataVariant>{6.3, 3.2, std::string{"bar"}, true}};

    REQUIRE_THROWS_WITH(converter2.convert(execution_policy, bad_values), std::string{"unexpected error: "} +
                                                                            demangle<std::string>() +
                                                                            " unexpected aggregate value type");
  }

  SECTION("FunctionTupleArgumentConverter")
  {
    const TinyVector<3> x3{1.7, 2.9, -3};
    FunctionTupleArgumentConverter<TinyVector<3>, TinyVector<3>> converter0{0};
    converter0.convert(execution_policy, TinyVector{x3});

    const double a = 1.2;
    const double b = -3.5;
    const double c = 2.6;
    FunctionTupleArgumentConverter<double, double> converter1{1};
    converter1.convert(execution_policy, std::vector{a, b, c});

    const uint64_t i = 1;
    const uint64_t j = 3;
    const uint64_t k = 6;
    FunctionTupleArgumentConverter<double, uint64_t> converter2{2};
    converter2.convert(execution_policy, std::vector<uint64_t>{i, j, k});

    REQUIRE(std::get<std::vector<TinyVector<3>>>(execution_policy.currentContext()[0]) ==
            std::vector<TinyVector<3>>{x3});
    REQUIRE(std::get<std::vector<double>>(execution_policy.currentContext()[1]) == std::vector<double>{a, b, c});
    REQUIRE(std::get<std::vector<double>>(execution_policy.currentContext()[2]) == std::vector<double>{i, j, k});

    converter1.convert(execution_policy, a);
    REQUIRE(std::get<std::vector<double>>(execution_policy.currentContext()[1]) == std::vector<double>{a});

    converter1.convert(execution_policy, j);
    REQUIRE(std::get<std::vector<double>>(execution_policy.currentContext()[1]) == std::vector<double>{j});

    // Errors
    REQUIRE_THROWS_WITH(converter0.convert(execution_policy, j),
                        "unexpected error: cannot convert 'unsigned long' to 'TinyVector<3ul, double>'");
  }

  SECTION("FunctionTupleArgumentConverter (string tuple)")
  {
    const TinyVector<3> x3{1.7, 2.9, -3};
    FunctionTupleArgumentConverter<std::string, TinyVector<3>> converter0{0};
    converter0.convert(execution_policy, TinyVector{x3});

    FunctionTupleArgumentConverter<std::string, std::vector<std::string>> converter1{1};
    converter1.convert(execution_policy, std::vector<std::string>{"foo"});

    FunctionTupleArgumentConverter<std::string, std::vector<TinyVector<3>>> converter2{2};
    converter2.convert(execution_policy, std::vector<TinyVector<3>>{TinyVector<3>{1, 2, 3}});

    REQUIRE(std::get<std::vector<std::string>>(execution_policy.currentContext()[0]) ==
            std::vector<std::string>{[](auto x) {
              std::ostringstream os;
              os << x;
              return os.str();
            }(x3)});
    REQUIRE(std::get<std::vector<std::string>>(execution_policy.currentContext()[1]) ==
            std::vector<std::string>{"foo"});

    REQUIRE(std::get<std::vector<std::string>>(execution_policy.currentContext()[2]) == std::vector<std::string>{[]() {
              std::ostringstream os;
              os << TinyVector<3>{1, 2, 3};
              return os.str();
            }()});
  }

  SECTION("FunctionListArgumentConverter")
  {
    const uint64_t i = 3;
    FunctionListArgumentConverter<double, double> converter0{0};
    converter0.convert(execution_policy, i);

    const double a = 6.3;
    const double b = -1.3;
    const double c = 3.6;
    FunctionListArgumentConverter<double, double> converter1{1};
    converter1.convert(execution_policy, std::vector<double>{a, b, c});

    AggregateDataVariant v{std::vector<DataVariant>{1ul, 2.3, -3l}};
    FunctionListArgumentConverter<double, double> converter2{2};
    converter2.convert(execution_policy, v);

    REQUIRE(std::get<std::vector<double>>(execution_policy.currentContext()[0]) == std::vector<double>{i});
    REQUIRE(std::get<std::vector<double>>(execution_policy.currentContext()[1]) == std::vector<double>{a, b, c});
    REQUIRE(std::get<std::vector<double>>(execution_policy.currentContext()[2]) == std::vector<double>{1ul, 2.3, -3l});

    FunctionListArgumentConverter<TinyVector<2>, TinyVector<2>> converterR2_0{0};
    converterR2_0.convert(execution_policy, TinyVector<2>{1, 3.2});

    FunctionListArgumentConverter<TinyVector<2>, TinyVector<2>> converterR2_1{1};
    converterR2_1.convert(execution_policy, std::vector{TinyVector<2>{1, 3.2}, TinyVector<2>{-1, 0.2}});

    AggregateDataVariant v_R2{std::vector<DataVariant>{TinyVector<2>{-3, 12.2}, TinyVector<2>{2, 1.2}}};
    FunctionListArgumentConverter<TinyVector<2>, TinyVector<2>> converterR2_2{2};
    converterR2_2.convert(execution_policy, v_R2);

    REQUIRE(std::get<std::vector<TinyVector<2>>>(execution_policy.currentContext()[0]) ==
            std::vector<TinyVector<2>>{TinyVector<2>{1, 3.2}});
    REQUIRE(std::get<std::vector<TinyVector<2>>>(execution_policy.currentContext()[1]) ==
            std::vector<TinyVector<2>>{TinyVector<2>{1, 3.2}, TinyVector<2>{-1, 0.2}});
    REQUIRE(std::get<std::vector<TinyVector<2>>>(execution_policy.currentContext()[2]) ==
            std::vector<TinyVector<2>>{TinyVector<2>{-3, 12.2}, TinyVector<2>{2, 1.2}});

    std::shared_ptr symbol_table = std::make_shared<SymbolTable>();
    AggregateDataVariant v_fid{std::vector<DataVariant>{uint64_t{3}, uint64_t{2}, uint64_t{7}}};

    FunctionListArgumentConverter<FunctionSymbolId, FunctionSymbolId> converterFid{0, symbol_table};
    converterFid.convert(execution_policy, v_fid);

    auto&& fid_tuple = std::get<std::vector<FunctionSymbolId>>(execution_policy.currentContext()[0]);

    REQUIRE(fid_tuple[0].id() == 3);
    REQUIRE(fid_tuple[1].id() == 2);
    REQUIRE(fid_tuple[2].id() == 7);
  }

  SECTION("FunctionArgumentToFunctionSymbolIdConverter")
  {
    std::shared_ptr symbol_table = std::make_shared<SymbolTable>();

    const uint64_t f_id = 3;
    FunctionArgumentToFunctionSymbolIdConverter converter0{0, symbol_table};
    converter0.convert(execution_policy, f_id);

    REQUIRE(std::get<FunctionSymbolId>(execution_policy.currentContext()[0]).id() == f_id);
  }

  SECTION("FunctionArgumentToTupleFunctionSymbolIdConverter")
  {
    std::shared_ptr symbol_table = std::make_shared<SymbolTable>();

    const uint64_t f_id = 3;
    FunctionArgumentToTupleFunctionSymbolIdConverter converter0{0, symbol_table};
    converter0.convert(execution_policy, f_id);
    auto&& tuple = std::get<std::vector<FunctionSymbolId>>(execution_policy.currentContext()[0]);
    REQUIRE(tuple.size() == 1);
    REQUIRE(tuple[0].id() == f_id);
  }

  SECTION("error")
  {
    SECTION("FunctionArgumentConverter negative value to N")
    {
      const int64_t negative_value = -2;
      FunctionArgumentConverter<uint64_t, int64_t> converter{0};
      REQUIRE_THROWS_WITH(converter.convert(execution_policy, negative_value), "trying to convert negative value (-2)");
    }

    SECTION("FunctionTupleOfNArgumentConverter negative value to N")
    {
      const int64_t i = 1;
      const int64_t j = 3;
      const int64_t k = -6;
      FunctionTupleArgumentConverter<uint64_t, int64_t> converter{0};
      REQUIRE_THROWS_WITH(converter.convert(execution_policy, std::vector<int64_t>{i, j, k}),
                          "trying to convert negative value (-6)");
    }

    SECTION("FunctionListArgumentConverter")
    {
      AggregateDataVariant v{std::vector<DataVariant>{1ul, -3l}};
      FunctionListArgumentConverter<uint64_t, uint64_t> converter{2};
      REQUIRE_THROWS_WITH(converter.convert(execution_policy, v), "trying to convert negative value (-3)");
    }
  }
}
