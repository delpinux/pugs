#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Messenger.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <utils/checkpointing/ReadTable.hpp>
#include <utils/checkpointing/WriteTable.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_Table", "[utils/checkpointing]")
{
  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});
    HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

    SECTION("Table")
    {
      HighFive::Group checkpoint_group = file.createGroup("checkpoint_group");
      HighFive::Group useless_group;

      Table<CellType> cell_type_table{19 + 3 * parallel::rank(), 3};
      cell_type_table.fill(CellType::Line);
      for (size_t i = 0; i < 10; ++i) {
        for (size_t j = 0; j < cell_type_table.numberOfColumns(); ++j) {
          cell_type_table[std::rand() / (RAND_MAX / cell_type_table.numberOfRows())][j] = CellType::Line;
          cell_type_table[std::rand() / (RAND_MAX / cell_type_table.numberOfRows())][j] = CellType::Triangle;
          cell_type_table[std::rand() / (RAND_MAX / cell_type_table.numberOfRows())][j] = CellType::Quadrangle;
          cell_type_table[std::rand() / (RAND_MAX / cell_type_table.numberOfRows())][j] = CellType::Polygon;
          cell_type_table[std::rand() / (RAND_MAX / cell_type_table.numberOfRows())][j] = CellType::Tetrahedron;
          cell_type_table[std::rand() / (RAND_MAX / cell_type_table.numberOfRows())][j] = CellType::Diamond;
          cell_type_table[std::rand() / (RAND_MAX / cell_type_table.numberOfRows())][j] = CellType::Hexahedron;
          cell_type_table[std::rand() / (RAND_MAX / cell_type_table.numberOfRows())][j] = CellType::Prism;
          cell_type_table[std::rand() / (RAND_MAX / cell_type_table.numberOfRows())][j] = CellType::Pyramid;
        }
      }
      checkpointing::write(checkpoint_group, "cell_type_table", cell_type_table);

      Table<CellId> cell_id_table{27 + 2 * parallel::rank(), 3};
      cell_id_table.fill(0);
      for (size_t i = 0; i < 20; ++i) {
        for (size_t j = 0; j < cell_id_table.numberOfColumns(); ++j) {
          cell_id_table[std::rand() / (RAND_MAX / cell_id_table.numberOfRows())][j] =
            std::rand() / (RAND_MAX / cell_id_table.numberOfRows());
        }
      }
      checkpointing::write(checkpoint_group, "cell_id_table", cell_id_table);

      Table<FaceId> face_id_table{29 + 2 * parallel::rank(), 2};
      face_id_table.fill(0);
      for (size_t i = 0; i < 20; ++i) {
        for (size_t j = 0; j < face_id_table.numberOfColumns(); ++j) {
          face_id_table[std::rand() / (RAND_MAX / face_id_table.numberOfRows())][j] =
            std::rand() / (RAND_MAX / face_id_table.numberOfRows());
        }
      }
      checkpointing::write(checkpoint_group, "face_id_table", face_id_table);

      Table<EdgeId> edge_id_table{13 + 2 * parallel::rank(), 4};
      edge_id_table.fill(0);
      for (size_t i = 0; i < 20; ++i) {
        for (size_t j = 0; j < edge_id_table.numberOfColumns(); ++j) {
          edge_id_table[std::rand() / (RAND_MAX / edge_id_table.numberOfRows())][j] =
            std::rand() / (RAND_MAX / edge_id_table.numberOfRows());
        }
      }
      checkpointing::write(checkpoint_group, "edge_id_table", edge_id_table);

      Table<NodeId> node_id_table{22 + 2 * parallel::rank(), 3};
      node_id_table.fill(0);
      for (size_t i = 0; i < 20; ++i) {
        for (size_t j = 0; j < node_id_table.numberOfColumns(); ++j) {
          node_id_table[std::rand() / (RAND_MAX / node_id_table.numberOfRows())][j] =
            std::rand() / (RAND_MAX / node_id_table.numberOfRows());
        }
      }
      checkpointing::write(checkpoint_group, "node_id_table", node_id_table);

      Table<double> double_table{16 + 3 * parallel::rank(), 5};
      double_table.fill(0);
      for (size_t i = 0; i < 20; ++i) {
        for (size_t j = 0; j < double_table.numberOfColumns(); ++j) {
          double_table[std::rand() / (RAND_MAX / double_table.numberOfRows())][j] =
            (1. * std::rand()) / (1. * RAND_MAX / double_table.numberOfRows());
        }
      }
      checkpointing::write(checkpoint_group, "double_table", double_table);

      file.flush();

      auto is_same = [](const auto& a, const auto& b) {
        bool same = true;
        for (size_t i = 0; i < a.numberOfRows(); ++i) {
          for (size_t j = 0; j < a.numberOfColumns(); ++j) {
            if (a(i, j) != b(i, j)) {
              same = false;
            }
          }
        }
        return parallel::allReduceAnd(same);
      };

      Table read_cell_type_table = checkpointing::readTable<CellType>(checkpoint_group, "cell_type_table");
      REQUIRE(is_same(cell_type_table, read_cell_type_table));

      Table read_cell_id_table = checkpointing::readTable<CellId>(checkpoint_group, "cell_id_table");
      REQUIRE(is_same(cell_id_table, read_cell_id_table));

      Table read_face_id_table = checkpointing::readTable<FaceId>(checkpoint_group, "face_id_table");
      REQUIRE(is_same(face_id_table, read_face_id_table));

      Table read_edge_id_table = checkpointing::readTable<EdgeId>(checkpoint_group, "edge_id_table");
      REQUIRE(is_same(edge_id_table, read_edge_id_table));

      Table read_node_id_table = checkpointing::readTable<NodeId>(checkpoint_group, "node_id_table");
      REQUIRE(is_same(node_id_table, read_node_id_table));

      Table read_double_table = checkpointing::readTable<double>(checkpoint_group, "double_table");
      REQUIRE(is_same(double_table, read_double_table));
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }
}
