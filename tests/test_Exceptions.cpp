#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Exceptions.hpp>
#include <utils/SourceLocation.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("Exceptions", "[utils]")
{
  SECTION("exceptions message")
  {
    RawError raw_error{"a raw error"};
    REQUIRE(std::string{raw_error.what()} == "a raw error");

    UnexpectedError unexpected_error{"an unexpected error"};
    REQUIRE(std::string{unexpected_error.what()} == "unexpected error: an unexpected error");
    REQUIRE(std::string{unexpected_error.sourceLocation().file_name()} == SourceLocation{}.filename());

    NotImplementedError not_implemented_error{"not implemented error"};
    REQUIRE(std::string{not_implemented_error.what()} == "not implemented yet: not implemented error");
    REQUIRE(std::string{not_implemented_error.sourceLocation().file_name()} == SourceLocation{}.filename());
  }
}
