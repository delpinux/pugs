#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <analysis/GaussLegendreQuadratureDescriptor.hpp>
#include <analysis/GaussLobattoQuadratureDescriptor.hpp>
#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/SquareTransformation.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("SquareTransformation", "[geometry]")
{
  SECTION("2D")
  {
    using R2 = TinyVector<2>;

    const R2 a_hat{-1, -1};
    const R2 b_hat{+1, -1};
    const R2 c_hat{+1, +1};
    const R2 d_hat{-1, +1};

    const R2 m_hat = zero;

    const R2 a{0, 0};
    const R2 b{8, -2};
    const R2 c{12, 7};
    const R2 d{3, 7};

    const R2 m = 0.25 * (a + b + c + d);

    const SquareTransformation<2> t(a, b, c, d);

    SECTION("values")
    {
      REQUIRE(t(a_hat)[0] == Catch::Approx(a[0]));
      REQUIRE(t(a_hat)[1] == Catch::Approx(a[1]));

      REQUIRE(t(b_hat)[0] == Catch::Approx(b[0]));
      REQUIRE(t(b_hat)[1] == Catch::Approx(b[1]));

      REQUIRE(t(c_hat)[0] == Catch::Approx(c[0]));
      REQUIRE(t(c_hat)[1] == Catch::Approx(c[1]));

      REQUIRE(t(d_hat)[0] == Catch::Approx(d[0]));
      REQUIRE(t(d_hat)[1] == Catch::Approx(d[1]));

      REQUIRE(t(m_hat)[0] == Catch::Approx(m[0]));
      REQUIRE(t(m_hat)[1] == Catch::Approx(m[1]));
    }

    SECTION("Jacobian determinant")
    {
      SECTION("at points")
      {
        auto detJ = [](const R2& X) {
          const double x = X[0];
          const double y = X[1];

          return 0.25 * ((0.5 * x + 4) * (y + 17) - 0.5 * (x + 7) * (y - 1));
        };

        REQUIRE(t.jacobianDeterminant(a_hat) == Catch::Approx(detJ(a_hat)));
        REQUIRE(t.jacobianDeterminant(b_hat) == Catch::Approx(detJ(b_hat)));
        REQUIRE(t.jacobianDeterminant(c_hat) == Catch::Approx(detJ(c_hat)));
        REQUIRE(t.jacobianDeterminant(d_hat) == Catch::Approx(detJ(d_hat)));

        REQUIRE(t.jacobianDeterminant(m_hat) == Catch::Approx(detJ(m_hat)));
      }

      SECTION("Gauss order 1")
      {
        // One point is enough in 2d
        const QuadratureFormula<2>& gauss =
          QuadratureManager::instance().getSquareFormula(GaussLegendreQuadratureDescriptor(1));

        double surface = 0;
        for (size_t i = 0; i < gauss.numberOfPoints(); ++i) {
          surface += gauss.weight(i) * t.jacobianDeterminant(gauss.point(i));
        }

        // 71.5 is actually the surface of the quadrangle
        REQUIRE(surface == Catch::Approx(71.5));
      }

      SECTION("Gauss order 3")
      {
        auto p = [](const R2& X) {
          const double& x = X[0];
          const double& y = X[1];

          return (2 * x + 3 * y + 2) * (x - 2 * y - 1);
        };

        // Jacbian determinant is a degree 1 polynomial, so the
        // following formula is required to reach exactness
        const QuadratureFormula<2>& gauss =
          QuadratureManager::instance().getSquareFormula(GaussLegendreQuadratureDescriptor(3));

        double integral = 0;
        for (size_t i = 0; i < gauss.numberOfPoints(); ++i) {
          integral += gauss.weight(i) * t.jacobianDeterminant(gauss.point(i)) * p(t(gauss.point(i)));
        }

        REQUIRE(integral == Catch::Approx(-76277. / 24));
      }
    }
  }

  SECTION("degenerate 2D ")
  {
    SECTION("2D")
    {
      using R2 = TinyVector<2>;

      const R2 a{1, 2};
      const R2 b{3, 1};
      const R2 c{2, 5};

      const SquareTransformation<2> t(a, b, c, c);

      auto p = [](const R2& X) {
        const double x = X[0];
        const double y = X[1];
        return 2 * x * x + 3 * x * y + y * y + 3 * y + 1;
      };

      SECTION("Gauss")
      {
        QuadratureFormula<2> qf = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(2));

        double sum = 0;
        for (size_t i = 0; i < qf.numberOfPoints(); ++i) {
          R2 xi = qf.point(i);
          sum += qf.weight(i) * t.jacobianDeterminant(xi) * p(t(xi));
        }

        REQUIRE(sum == Catch::Approx(3437. / 24));
      }

      SECTION("Gauss Legendre")
      {
        QuadratureFormula<2> qf = QuadratureManager::instance().getSquareFormula(GaussLegendreQuadratureDescriptor(2));

        double sum = 0;
        for (size_t i = 0; i < qf.numberOfPoints(); ++i) {
          R2 xi = qf.point(i);
          sum += qf.weight(i) * t.jacobianDeterminant(xi) * p(t(xi));
        }

        REQUIRE(sum == Catch::Approx(3437. / 24));
      }

      SECTION("Gauss Lobatto")
      {
        QuadratureFormula<2> qf = QuadratureManager::instance().getSquareFormula(GaussLobattoQuadratureDescriptor(2));

        double sum = 0;
        for (size_t i = 0; i < qf.numberOfPoints(); ++i) {
          R2 xi = qf.point(i);
          sum += qf.weight(i) * t.jacobianDeterminant(xi) * p(t(xi));
        }

        REQUIRE(sum == Catch::Approx(3437. / 24));
      }
    }
  }

  SECTION("3D")
  {
    using R2 = TinyVector<2>;

    const R2 a_hat{-1, -1};
    const R2 b_hat{+1, -1};
    const R2 c_hat{+1, +1};
    const R2 d_hat{-1, +1};

    const R2 m_hat = zero;

    using R3 = TinyVector<3>;

    const R3 a{0, 0, -1};
    const R3 b{8, -2, 3};
    const R3 c{12, 7, 2};
    const R3 d{3, 7, 1};

    const R3 m = 0.25 * (a + b + c + d);

    const SquareTransformation<3> t(a, b, c, d);

    SECTION("values")
    {
      REQUIRE(t(a_hat)[0] == Catch::Approx(a[0]));
      REQUIRE(t(a_hat)[1] == Catch::Approx(a[1]));
      REQUIRE(t(a_hat)[2] == Catch::Approx(a[2]));

      REQUIRE(t(b_hat)[0] == Catch::Approx(b[0]));
      REQUIRE(t(b_hat)[1] == Catch::Approx(b[1]));
      REQUIRE(t(b_hat)[2] == Catch::Approx(b[2]));

      REQUIRE(t(c_hat)[0] == Catch::Approx(c[0]));
      REQUIRE(t(c_hat)[1] == Catch::Approx(c[1]));
      REQUIRE(t(c_hat)[2] == Catch::Approx(c[2]));

      REQUIRE(t(d_hat)[0] == Catch::Approx(d[0]));
      REQUIRE(t(d_hat)[1] == Catch::Approx(d[1]));
      REQUIRE(t(d_hat)[2] == Catch::Approx(d[2]));

      REQUIRE(t(m_hat)[0] == Catch::Approx(m[0]));
      REQUIRE(t(m_hat)[1] == Catch::Approx(m[1]));
      REQUIRE(t(m_hat)[2] == Catch::Approx(m[2]));
    }

    SECTION("Area variation norm")
    {
      auto area_variation_norm = [&](const R2& X) {
        const double x = X[0];
        const double y = X[1];

        const R3 J1 = 0.25 * (-a + b + c - d);
        const R3 J2 = 0.25 * (-a - b + c + d);
        const R3 J3 = 0.25 * (a - b + c - d);

        return l2Norm(crossProduct(J1 + y * J3, J2 + x * J3));
      };

      SECTION("at points")
      {
        REQUIRE(t.areaVariationNorm(a_hat) == Catch::Approx(area_variation_norm(a_hat)));
        REQUIRE(t.areaVariationNorm(b_hat) == Catch::Approx(area_variation_norm(b_hat)));
        REQUIRE(t.areaVariationNorm(c_hat) == Catch::Approx(area_variation_norm(c_hat)));
        REQUIRE(t.areaVariationNorm(d_hat) == Catch::Approx(area_variation_norm(d_hat)));

        REQUIRE(t.areaVariationNorm(m_hat) == Catch::Approx(area_variation_norm(m_hat)));
      }

      auto p = [](const R3& X) {
        const double x = X[0];
        const double y = X[1];
        const double z = X[2];
        return 2 * x * x + 3 * x - 3 * y * y + y + 2 * z * z - 0.5 * z + 2;
      };

      SECTION("Gauss order 3")
      {
        // Due to the area variation term (square root of a
        // polynomial), Gauss-like quadrature cannot be exact for
        // general 3d quadrangle.
        //
        // Moreover, even the surface of general 3d quadrangle cannot
        // be computed exactly.
        //
        // So for this test we integrate the following function:
        // f(x,y,z) := p(x,y,z) * |dA(t^-1(x,y,z))|
        //
        // dA denotes the area change on the reference element. This
        // function can be exactly integrated since computing the
        // integral on the reference quadrangle, one gets a dA^2,
        // which is a polynomial.
        const QuadratureFormula<2>& gauss =
          QuadratureManager::instance().getSquareFormula(GaussLegendreQuadratureDescriptor(4));

        double integral = 0;
        for (size_t i = 0; i < gauss.numberOfPoints(); ++i) {
          const double fX = (t.areaVariationNorm(gauss.point(i)) * p(t(gauss.point(i))));
          integral += gauss.weight(i) * t.areaVariationNorm(gauss.point(i)) * fX;
        }

        REQUIRE(integral == Catch::Approx(60519715. / 576));
      }
    }
  }
}
