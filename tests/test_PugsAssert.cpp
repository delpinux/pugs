#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/PugsAssert.hpp>

#include <string>

// clazy:excludeall=non-pod-global-static

TEST_CASE("PugsAssert", "[utils]")
{
  SECTION("checking for assert error")
  {
    const std::string filename = "filename";
    const size_t line          = 10;
    const std::string function = "function";

    AssertError assert_error(filename, line, function, std::make_tuple("test"), "test");

    REQUIRE(Catch::Detail::stringify(assert_error) == "\n---------- Assertion error -----------\n at filename:10\n in "
                                                      "function\n assertion (test) "
                                                      "failed!\n--------------------------------------\n");
  }

  SECTION("checking for assert error with message")
  {
    const std::string filename = "filename";
    const int line             = 10;
    const std::string function = "function";

    AssertError assert_error(filename, line, function, std::make_tuple(false, "message"), "test, message  ");

    REQUIRE(Catch::Detail::stringify(assert_error) == "\n---------- Assertion error -----------\n at filename:10\n in "
                                                      "function\n assertion (test) failed!\n "
                                                      "message\n--------------------------------------\n");
  }
}
