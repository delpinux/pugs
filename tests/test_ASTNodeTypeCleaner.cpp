#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/utils/ASTPrinter.hpp>

#define CHECK_AST(data, expected_output)                                                       \
  {                                                                                            \
    static_assert(std::is_same_v<std::decay_t<decltype(data)>, std::string_view>);             \
    static_assert(std::is_same_v<std::decay_t<decltype(expected_output)>, std::string_view>);  \
                                                                                               \
    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};                                 \
    auto ast = ASTBuilder::build(input);                                                       \
                                                                                               \
    ASTModulesImporter{*ast};                                                                  \
    ASTNodeTypeCleaner<language::import_instruction>{*ast};                                    \
                                                                                               \
    ASTSymbolTableBuilder{*ast};                                                               \
    ASTNodeDataTypeBuilder{*ast};                                                              \
                                                                                               \
    ASTNodeTypeCleaner<language::var_declaration>{*ast};                                       \
                                                                                               \
    std::stringstream ast_output;                                                              \
    ast_output << '\n' << ASTPrinter{*ast, ASTPrinter::Format::raw, {ASTPrinter::Info::none}}; \
                                                                                               \
    REQUIRE(ast_output.str() == expected_output);                                              \
    ast->m_symbol_table->clearValues();                                                        \
  }

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTNodeTypeCleaner", "[language]")
{
  SECTION("no declaration")
  {
    std::string_view data = R"(
cout << "two=" << 2 << "\n";
)";

    std::string_view result = R"(
(root)
 `-(language::shift_left_op)
     +-(language::shift_left_op)
     |   +-(language::shift_left_op)
     |   |   +-(language::name:cout)
     |   |   `-(language::literal:"two=")
     |   `-(language::integer:2)
     `-(language::literal:"\n")
)";

    CHECK_AST(data, result);
  }

  SECTION("single declaration")
  {
    std::string_view data = R"(
let z:R;
z = 1;
)";

    std::string_view result = R"(
(root)
 `-(language::eq_op)
     +-(language::name:z)
     `-(language::integer:1)
)";

    CHECK_AST(data, result);
  }

  SECTION("multiple declaration")
  {
    std::string_view data = R"(
let z:Z;
z = 1;
if (true) {
  let x:R;
  x = 0.5 *z;
}
)";

    std::string_view result = R"(
(root)
 +-(language::eq_op)
 |   +-(language::name:z)
 |   `-(language::integer:1)
 `-(language::if_statement)
     +-(language::true_kw)
     `-(language::block)
         `-(language::eq_op)
             +-(language::name:x)
             `-(language::multiply_op)
                 +-(language::real:0.5)
                 `-(language::name:z)
)";

    CHECK_AST(data, result);
  }
}
