#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/Messenger.hpp>
#include <utils/checkpointing/PrintCheckpointInfo.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("PrintCheckpointInfo", "[utils/checkpointing]")
{
#ifdef PUGS_HAS_HDF5

  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    const std::string data_file0 = R"(Un tiens vaut mieux que deux tu l'auras,
Un tiens vaut mieux que deux tu l'auras,...)";
    const std::string data_file1 = R"(All work and no play makes Jack a dull boy,
All work and no play makes Jack a dull boy,...)";
    const std::string data_file2 = R"(solo trabajo y nada de juego hacen de Jack un chico aburrido,
solo trabajo y nada de juego hacen de Jack un chico aburrido,...)";

    std::string info = R"( * checkpoint_0 [Today]
   X1 ...................... [2.7]
   X2 ...................... [-2.3,4.1]
   a ....................... 1.3
   b ....................... true
   n ....................... 12
   z ....................... -3
   mesh0 ................... mesh
 * checkpoint_1 [Tomorrow]
   X3 ...................... [1.2,1.4,-1]
   m ....................... 8
   z ....................... -6
   tv ...................... ([1,2], [-1.2,3])
 * checkpoint_2 [Yesterday]
   M1 ...................... [[2.7]]
   M2 ...................... [[-2.3,4.1],[-1.8,1.5]]
   M3 ...................... [[-2.3,4.1,3.2],[-1.8,1.5,1.7],[-1.2,0.7,1.2]]
   s ....................... "foo"
   ts ...................... ("foo", "bar")
-------------------------------------------------------
 * resuming_checkpoint -> checkpoint_1
 * last_checkpoint -> checkpoint_2
)";

    {
      HighFive::FileAccessProps fapl;
      fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
      fapl.add(HighFive::MPIOCollectiveMetadata{});
      HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

      {
        HighFive::Group cp = file.createGroup("/checkpoint_0");
        cp.createAttribute("data.pgs", data_file0);
        cp.createAttribute("id", 0ul);
        cp.createAttribute("creation_date", std::string{"Today"});
        cp.createAttribute("checkpoint_number", 0ul);
        cp.createAttribute("name", std::string{"checkpoint_0"});
        HighFive::Group symbol_table = cp.createGroup("symbol table");
        symbol_table.createAttribute("a", 1.3);
        symbol_table.createAttribute("n", uint64_t{12});
        symbol_table.createAttribute("z", int64_t{-3});
        symbol_table.createAttribute("X1", TinyVector<1>{2.7});
        symbol_table.createAttribute("X2", TinyVector<2>{-2.3, 4.1});
        symbol_table.createAttribute("b", true);
        HighFive::Group embedded = symbol_table.createGroup("embedded");
        HighFive::Group mesh0    = embedded.createGroup("mesh0");
        mesh0.createAttribute("type", std::string{"mesh"});
      }

      {
        HighFive::Group cp = file.createGroup("/checkpoint_1");
        cp.createAttribute("data.pgs", data_file1);
        cp.createAttribute("id", 1ul);
        cp.createAttribute("creation_date", std::string{"Tomorrow"});
        cp.createAttribute("checkpoint_number", 1ul);
        cp.createAttribute("name", std::string{"checkpoint_1"});
        HighFive::Group symbol_table = cp.createGroup("symbol table");
        symbol_table.createAttribute("m", uint64_t{8});
        symbol_table.createAttribute("z", int64_t{-6});
        symbol_table.createAttribute("X3", TinyVector<3>{1.2, 1.4, -1});

        HighFive::Group sub_symbol_table = symbol_table.createGroup("symbol table");

        sub_symbol_table.createAttribute("tv", std::vector{TinyVector<2>{1, 2}, TinyVector<2>{-1.2, 3}});

        file.createHardLink("resuming_checkpoint", cp);
      }

      {
        HighFive::Group cp = file.createGroup("/checkpoint_2");
        cp.createAttribute("data.pgs", data_file2);
        cp.createAttribute("id", 2ul);
        cp.createAttribute("creation_date", std::string{"Yesterday"});
        cp.createAttribute("checkpoint_number", 2ul);
        cp.createAttribute("name", std::string{"checkpoint_2"});
        HighFive::Group symbol_table = cp.createGroup("symbol table");
        symbol_table.createAttribute("s", std::string{"foo"});
        symbol_table.createAttribute("M1", TinyMatrix<1>{2.7});
        symbol_table.createAttribute("M2", TinyMatrix<2>{-2.3, 4.1,   //
                                                         -1.8, 1.5});
        symbol_table.createAttribute("M3", TinyMatrix<3>{-2.3, 4.1, 3.2,   //
                                                         -1.8, 1.5, 1.7,   //
                                                         -1.2, 0.7, 1.2});

        symbol_table.createAttribute("ts", std::vector<std::string>{"foo", "bar"});

        file.createHardLink("last_checkpoint", cp);
      }
    }

    {
      std::ostringstream os;
      printCheckpointInfo(filename, os);

      if (parallel::rank() == 0) {
        REQUIRE(os.str() == info);
      } else {
        REQUIRE(os.str() == "");
      }
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }

#else   // PUGS_HAS_HDF5

  if (parallel::rank() == 0) {
    std::cerr.setstate(std::ios::badbit);
  }

  std::ostringstream os;
  REQUIRE_NOTHROW(printCheckpointInfo("foo.h5", os));

  if (parallel::rank() == 0) {
    std::cerr.clear();
  }

  REQUIRE(os.str() == "");

#endif   // PUGS_HAS_HDF5
}
