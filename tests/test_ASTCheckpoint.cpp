#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTNode.hpp>
#include <language/utils/ASTCheckpoint.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ASTCheckpoint", "[language]")
{
  ASTNode ast_node;

  std::vector<size_t> location = {1, 2, 4};
  ASTCheckpoint cp{location, &ast_node};

  REQUIRE(&cp.node() == &ast_node);
  REQUIRE(cp.getASTLocation() == location);

  ASTCheckpoint cp_copy = cp;

  REQUIRE(&cp_copy.node() == &ast_node);
  REQUIRE(cp_copy.getASTLocation() == location);

  ASTCheckpoint cp_move = std::move(cp);

  REQUIRE(&cp_move.node() == &ast_node);
  REQUIRE(cp_move.getASTLocation() == location);
}
