#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <analysis/GaussLobattoQuadratureDescriptor.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("GaussLobattoQuadratureDescriptor", "[analysis]")
{
  GaussLobattoQuadratureDescriptor quadrature_descriptor(3);

  REQUIRE(quadrature_descriptor.isTensorial() == true);
  REQUIRE(quadrature_descriptor.type() == QuadratureType::GaussLobatto);
  REQUIRE(quadrature_descriptor.degree() == 3);
  REQUIRE(quadrature_descriptor.name() == ::name(QuadratureType::GaussLobatto) + "(3)");
}
