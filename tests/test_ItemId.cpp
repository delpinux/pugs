#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

#include <mesh/ItemId.hpp>

// Instantiate to ensure full coverage is performed
template class ItemIdT<ItemType::node>;
template class ItemIdT<ItemType::edge>;
template class ItemIdT<ItemType::face>;
template class ItemIdT<ItemType::cell>;

// clazy:excludeall=non-pod-global-static

TEST_CASE("ItemId", "[mesh]")
{
  SECTION("NodeId")
  {
    NodeId node_id0(13);
    REQUIRE(node_id0 == 13);

    NodeId node_id1 = 0;
    REQUIRE(node_id1 == 0);

    REQUIRE(++node_id1 == 1);

    REQUIRE(node_id1++ == 1);
    REQUIRE(node_id1 == 2);

    NodeId node_id2{node_id1};
    REQUIRE(node_id2 == 2);

    node_id2 = 17;
    REQUIRE(node_id2 == 17);

    node_id2 = node_id1;
    REQUIRE(node_id2 == 2);

    NodeId node_id3;
    {
      NodeId tmp_node_id{12};
      NodeId node_id4{std::move(tmp_node_id)};
      REQUIRE(node_id4 == 12);
      node_id3 = std::move(node_id4);
    }
    REQUIRE(node_id3 == 12);
  }

  SECTION("EdgeId")
  {
    EdgeId edge_id0(13);
    REQUIRE(edge_id0 == 13);

    EdgeId edge_id1 = 0;
    REQUIRE(edge_id1 == 0);

    REQUIRE(++edge_id1 == 1);

    REQUIRE(edge_id1++ == 1);
    REQUIRE(edge_id1 == 2);

    EdgeId edge_id2{edge_id1};
    REQUIRE(edge_id2 == 2);

    edge_id2 = 17;
    REQUIRE(edge_id2 == 17);

    edge_id2 = edge_id1;
    REQUIRE(edge_id2 == 2);

    EdgeId edge_id3;
    {
      EdgeId tmp_edge_id{12};
      EdgeId edge_id4{std::move(tmp_edge_id)};
      REQUIRE(edge_id4 == 12);
      edge_id3 = std::move(edge_id4);
    }
    REQUIRE(edge_id3 == 12);
  }

  SECTION("FaceId")
  {
    FaceId face_id0(13);
    REQUIRE(face_id0 == 13);

    FaceId face_id1 = 0;
    REQUIRE(face_id1 == 0);

    REQUIRE(++face_id1 == 1);

    REQUIRE(face_id1++ == 1);
    REQUIRE(face_id1 == 2);

    FaceId face_id2{face_id1};
    REQUIRE(face_id2 == 2);

    face_id2 = 17;
    REQUIRE(face_id2 == 17);

    face_id2 = face_id1;
    REQUIRE(face_id2 == 2);

    FaceId face_id3;
    {
      FaceId tmp_face_id{12};
      FaceId face_id4{std::move(tmp_face_id)};
      REQUIRE(face_id4 == 12);
      face_id3 = std::move(face_id4);
    }
    REQUIRE(face_id3 == 12);
  }

  SECTION("CellId")
  {
    CellId cell_id0(13);
    REQUIRE(cell_id0 == 13);

    CellId cell_id1 = 0;
    REQUIRE(cell_id1 == 0);

    REQUIRE(++cell_id1 == 1);

    REQUIRE(cell_id1++ == 1);
    REQUIRE(cell_id1 == 2);

    CellId cell_id2{cell_id1};
    REQUIRE(cell_id2 == 2);

    cell_id2 = 17;
    REQUIRE(cell_id2 == 17);

    cell_id2 = cell_id1;
    REQUIRE(cell_id2 == 2);

    CellId cell_id3;
    {
      CellId tmp_cell_id{12};
      CellId cell_id4{std::move(tmp_cell_id)};
      REQUIRE(cell_id4 == 12);
      cell_id3 = std::move(cell_id4);
    }
    REQUIRE(cell_id3 == 12);
  }
}
