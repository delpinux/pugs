#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <analysis/GaussLegendreQuadratureDescriptor.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("GaussLegendreQuadratureDescriptor", "[analysis]")
{
  GaussLegendreQuadratureDescriptor quadrature_descriptor(3);

  REQUIRE(quadrature_descriptor.isTensorial() == true);
  REQUIRE(quadrature_descriptor.type() == QuadratureType::GaussLegendre);
  REQUIRE(quadrature_descriptor.degree() == 3);
  REQUIRE(quadrature_descriptor.name() == ::name(QuadratureType::GaussLegendre) + "(3)");
}
