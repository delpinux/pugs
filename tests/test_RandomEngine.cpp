#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Messenger.hpp>
#include <utils/RandomEngine.hpp>

#include <utils/pugs_config.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("RandomEngine", "[random]")
{
  SECTION("current state")
  {
    RandomEngine& random_engine = RandomEngine::instance();
    REQUIRE(isSynchronized(random_engine));
  }

  SECTION("set seed")
  {
    RandomEngine& random_engine = RandomEngine::instance();
    random_engine.setRandomSeed(1402339680);

    REQUIRE(isSynchronized(random_engine));

    REQUIRE(random_engine.getCurrentSeed() == 1402339680);
  }

  SECTION("reset seed")
  {
    RandomEngine& random_engine = RandomEngine::instance();
    random_engine.resetRandomSeed();

    REQUIRE(isSynchronized(random_engine));
  }

  SECTION("de-synchronize seeds")
  {
    RandomEngine& random_engine = RandomEngine::instance();
    random_engine.resetRandomSeed();
    auto& engine = random_engine.engine();

    for (size_t i = 0; i < parallel::rank(); ++i) {
      engine();
    }

    REQUIRE(((parallel::size() == 1) or not isSynchronized(random_engine)));

    random_engine.resetRandomSeed();
    REQUIRE(isSynchronized(random_engine));
  }
}
