#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/SubItemArrayPerItem.hpp>
#include <utils/Messenger.hpp>

// Instantiate to ensure full coverage is performed
template class SubItemArrayPerItem<size_t, NodeOfCell>;

// clazy:excludeall=non-pod-global-static

TEST_CASE("SubItemArrayPerItem", "[mesh]")
{
  SECTION("default constructors")
  {
    REQUIRE_NOTHROW(NodeArrayPerEdge<int>{});
    REQUIRE_NOTHROW(NodeArrayPerFace<int>{});
    REQUIRE_NOTHROW(NodeArrayPerCell<int>{});

    REQUIRE_NOTHROW(EdgeArrayPerNode<int>{});
    REQUIRE_NOTHROW(EdgeArrayPerFace<int>{});
    REQUIRE_NOTHROW(EdgeArrayPerCell<int>{});

    REQUIRE_NOTHROW(FaceArrayPerNode<int>{});
    REQUIRE_NOTHROW(FaceArrayPerEdge<int>{});
    REQUIRE_NOTHROW(FaceArrayPerCell<int>{});

    REQUIRE_NOTHROW(CellArrayPerNode<int>{});
    REQUIRE_NOTHROW(CellArrayPerEdge<int>{});
    REQUIRE_NOTHROW(CellArrayPerFace<int>{});

    REQUIRE(not NodeArrayPerEdge<int>{}.isBuilt());
    REQUIRE(not NodeArrayPerFace<int>{}.isBuilt());
    REQUIRE(not NodeArrayPerCell<int>{}.isBuilt());

    REQUIRE(not EdgeArrayPerNode<int>{}.isBuilt());
    REQUIRE(not EdgeArrayPerFace<int>{}.isBuilt());
    REQUIRE(not EdgeArrayPerCell<int>{}.isBuilt());

    REQUIRE(not FaceArrayPerNode<int>{}.isBuilt());
    REQUIRE(not FaceArrayPerEdge<int>{}.isBuilt());
    REQUIRE(not FaceArrayPerCell<int>{}.isBuilt());

    REQUIRE(not CellArrayPerNode<int>{}.isBuilt());
    REQUIRE(not CellArrayPerEdge<int>{}.isBuilt());
    REQUIRE(not CellArrayPerFace<int>{}.isBuilt());
  }

  SECTION("dimensions")
  {
    auto number_of_arrays = [](const auto& sub_item_array_per_item) -> size_t {
      using SubItemArrayPerItemType = std::decay_t<decltype(sub_item_array_per_item)>;
      using ItemId                  = typename SubItemArrayPerItemType::ItemId;

      size_t number = 0;
      for (ItemId item_id = 0; item_id < sub_item_array_per_item.numberOfItems(); ++item_id) {
        number += sub_item_array_per_item.numberOfSubArrays(item_id);
      }
      return number;
    };

    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d = named_mesh.mesh()->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          SECTION("per cell")
          {
            NodeArrayPerCell<int> node_array_per_cell{connectivity, 3};
            REQUIRE(node_array_per_cell.numberOfItems() == connectivity.numberOfCells());
            REQUIRE(node_array_per_cell.numberOfArrays() == number_of_arrays(node_array_per_cell));
            REQUIRE(node_array_per_cell.sizeOfArrays() == 3);

            auto cell_to_node_matrix = connectivity.cellToNodeMatrix();
            {
              bool is_correct = true;
              for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
                is_correct &=
                  (cell_to_node_matrix[cell_id].size() == node_array_per_cell.numberOfSubArrays(cell_id)) and
                  (node_array_per_cell.itemTable(cell_id).numberOfRows() ==
                   node_array_per_cell.numberOfSubArrays(cell_id));
              }
              REQUIRE(is_correct);
            }

            const NodeArrayPerCell<const int> const_node_array_per_cell = node_array_per_cell;
            {
              bool is_correct = true;
              for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
                is_correct &= (const_node_array_per_cell.itemTable(cell_id).numberOfRows() ==
                               node_array_per_cell.numberOfSubArrays(cell_id));
              }
              REQUIRE(is_correct);
            }

            EdgeArrayPerCell<int> edge_array_per_cell{connectivity, 4};
            REQUIRE(edge_array_per_cell.numberOfItems() == connectivity.numberOfCells());
            REQUIRE(edge_array_per_cell.numberOfArrays() == number_of_arrays(edge_array_per_cell));
            REQUIRE(edge_array_per_cell.sizeOfArrays() == 4);

            auto cell_to_edge_matrix = connectivity.cellToEdgeMatrix();
            {
              bool is_correct = true;
              for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
                is_correct &= (cell_to_edge_matrix[cell_id].size() == edge_array_per_cell.numberOfSubArrays(cell_id));
              }
              REQUIRE(is_correct);
            }

            FaceArrayPerCell<int> face_array_per_cell{connectivity, 2};
            REQUIRE(face_array_per_cell.numberOfItems() == connectivity.numberOfCells());
            REQUIRE(face_array_per_cell.numberOfArrays() == number_of_arrays(face_array_per_cell));
            REQUIRE(face_array_per_cell.sizeOfArrays() == 2);

            auto cell_to_face_matrix = connectivity.cellToFaceMatrix();
            {
              bool is_correct = true;
              for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
                is_correct &= (cell_to_face_matrix[cell_id].size() == face_array_per_cell.numberOfSubArrays(cell_id));
              }
              REQUIRE(is_correct);
            }
          }

          SECTION("per face")
          {
            CellArrayPerFace<int> cell_array_per_face{connectivity, 2};
            REQUIRE(cell_array_per_face.numberOfItems() == connectivity.numberOfFaces());
            REQUIRE(cell_array_per_face.numberOfArrays() == number_of_arrays(cell_array_per_face));
            REQUIRE(cell_array_per_face.sizeOfArrays() == 2);

            auto face_to_cell_matrix = connectivity.faceToCellMatrix();
            {
              bool is_correct = true;
              for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
                is_correct &= (face_to_cell_matrix[face_id].size() == cell_array_per_face.numberOfSubArrays(face_id));
              }
              REQUIRE(is_correct);
            }
          }

          SECTION("per edge")
          {
            CellArrayPerEdge<int> cell_array_per_edge{connectivity, 3};
            REQUIRE(cell_array_per_edge.numberOfItems() == connectivity.numberOfEdges());
            REQUIRE(cell_array_per_edge.numberOfArrays() == number_of_arrays(cell_array_per_edge));
            REQUIRE(cell_array_per_edge.sizeOfArrays() == 3);

            auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();
            {
              bool is_correct = true;
              for (EdgeId edge_id = 0; edge_id < connectivity.numberOfEdges(); ++edge_id) {
                is_correct &= (edge_to_cell_matrix[edge_id].size() == cell_array_per_edge.numberOfSubArrays(edge_id));
              }
              REQUIRE(is_correct);
            }
          }

          SECTION("per node")
          {
            CellArrayPerNode<int> cell_array_per_node{connectivity, 4};
            REQUIRE(cell_array_per_node.numberOfItems() == connectivity.numberOfNodes());
            REQUIRE(cell_array_per_node.numberOfArrays() == number_of_arrays(cell_array_per_node));
            REQUIRE(cell_array_per_node.sizeOfArrays() == 4);

            auto node_to_cell_matrix = connectivity.nodeToCellMatrix();
            {
              bool is_correct = true;
              for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
                is_correct &= (node_to_cell_matrix[node_id].size() == cell_array_per_node.numberOfSubArrays(node_id));
              }
              REQUIRE(is_correct);
            }
          }
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          SECTION("per cell")
          {
            NodeArrayPerCell<int> node_array_per_cell{connectivity, 5};
            REQUIRE(node_array_per_cell.numberOfItems() == connectivity.numberOfCells());
            REQUIRE(node_array_per_cell.numberOfArrays() == number_of_arrays(node_array_per_cell));
            REQUIRE(node_array_per_cell.sizeOfArrays() == 5);

            auto cell_to_node_matrix = connectivity.cellToNodeMatrix();
            {
              bool is_correct = true;
              for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
                is_correct &= (cell_to_node_matrix[cell_id].size() == node_array_per_cell.numberOfSubArrays(cell_id));
              }
              REQUIRE(is_correct);
            }

            EdgeArrayPerCell<int> edge_array_per_cell{connectivity, 4};
            REQUIRE(edge_array_per_cell.numberOfItems() == connectivity.numberOfCells());
            REQUIRE(edge_array_per_cell.numberOfArrays() == number_of_arrays(edge_array_per_cell));
            REQUIRE(edge_array_per_cell.sizeOfArrays() == 4);

            auto cell_to_edge_matrix = connectivity.cellToEdgeMatrix();
            {
              bool is_correct = true;
              for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
                is_correct &= (cell_to_edge_matrix[cell_id].size() == edge_array_per_cell.numberOfSubArrays(cell_id));
              }
              REQUIRE(is_correct);
            }

            FaceArrayPerCell<int> face_array_per_cell{connectivity, 3};
            REQUIRE(face_array_per_cell.numberOfItems() == connectivity.numberOfCells());
            REQUIRE(face_array_per_cell.numberOfArrays() == number_of_arrays(face_array_per_cell));
            REQUIRE(face_array_per_cell.sizeOfArrays() == 3);

            auto cell_to_face_matrix = connectivity.cellToFaceMatrix();
            {
              bool is_correct = true;
              for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
                is_correct &= (cell_to_face_matrix[cell_id].size() == face_array_per_cell.numberOfSubArrays(cell_id));
              }
              REQUIRE(is_correct);
            }
          }

          SECTION("per face")
          {
            CellArrayPerFace<int> cell_array_per_face{connectivity, 3};
            REQUIRE(cell_array_per_face.numberOfItems() == connectivity.numberOfFaces());
            REQUIRE(cell_array_per_face.numberOfArrays() == number_of_arrays(cell_array_per_face));
            REQUIRE(cell_array_per_face.sizeOfArrays() == 3);

            auto face_to_cell_matrix = connectivity.faceToCellMatrix();
            {
              bool is_correct = true;
              for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
                is_correct &= (face_to_cell_matrix[face_id].size() == cell_array_per_face.numberOfSubArrays(face_id));
              }
              REQUIRE(is_correct);
            }

            NodeArrayPerFace<int> node_array_per_face{connectivity, 2};
            REQUIRE(node_array_per_face.numberOfItems() == connectivity.numberOfFaces());
            REQUIRE(node_array_per_face.numberOfArrays() == number_of_arrays(node_array_per_face));
            REQUIRE(node_array_per_face.sizeOfArrays() == 2);

            auto face_to_node_matrix = connectivity.faceToNodeMatrix();
            {
              bool is_correct = true;
              for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
                is_correct &= (face_to_node_matrix[face_id].size() == node_array_per_face.numberOfSubArrays(face_id));
              }
              REQUIRE(is_correct);
            }
          }

          SECTION("per edge")
          {
            CellArrayPerEdge<int> cell_array_per_edge{connectivity, 3};
            REQUIRE(cell_array_per_edge.numberOfItems() == connectivity.numberOfEdges());
            REQUIRE(cell_array_per_edge.numberOfArrays() == number_of_arrays(cell_array_per_edge));
            REQUIRE(cell_array_per_edge.sizeOfArrays() == 3);

            auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();
            {
              bool is_correct = true;
              for (EdgeId edge_id = 0; edge_id < connectivity.numberOfEdges(); ++edge_id) {
                is_correct &= (edge_to_cell_matrix[edge_id].size() == cell_array_per_edge.numberOfSubArrays(edge_id));
              }
              REQUIRE(is_correct);
            }

            NodeArrayPerEdge<int> node_array_per_edge{connectivity, 5};
            REQUIRE(node_array_per_edge.numberOfItems() == connectivity.numberOfEdges());
            REQUIRE(node_array_per_edge.numberOfArrays() == number_of_arrays(node_array_per_edge));
            REQUIRE(node_array_per_edge.sizeOfArrays() == 5);

            auto edge_to_node_matrix = connectivity.edgeToNodeMatrix();
            {
              bool is_correct = true;
              for (EdgeId edge_id = 0; edge_id < connectivity.numberOfEdges(); ++edge_id) {
                is_correct &= (edge_to_node_matrix[edge_id].size() == node_array_per_edge.numberOfSubArrays(edge_id));
              }
              REQUIRE(is_correct);
            }
          }

          SECTION("per node")
          {
            EdgeArrayPerNode<int> edge_array_per_node{connectivity, 4};
            REQUIRE(edge_array_per_node.numberOfItems() == connectivity.numberOfNodes());
            REQUIRE(edge_array_per_node.numberOfArrays() == number_of_arrays(edge_array_per_node));
            REQUIRE(edge_array_per_node.sizeOfArrays() == 4);

            auto node_to_edge_matrix = connectivity.nodeToEdgeMatrix();
            {
              bool is_correct = true;
              for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
                is_correct &= (node_to_edge_matrix[node_id].size() == edge_array_per_node.numberOfSubArrays(node_id));
              }
              REQUIRE(is_correct);
            }

            FaceArrayPerNode<int> face_array_per_node{connectivity, 3};
            REQUIRE(face_array_per_node.numberOfItems() == connectivity.numberOfNodes());
            REQUIRE(face_array_per_node.numberOfArrays() == number_of_arrays(face_array_per_node));
            REQUIRE(face_array_per_node.sizeOfArrays() == 3);

            auto node_to_face_matrix = connectivity.nodeToFaceMatrix();
            {
              bool is_correct = true;
              for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
                is_correct &= (node_to_face_matrix[node_id].size() == face_array_per_node.numberOfSubArrays(node_id));
              }
              REQUIRE(is_correct);
            }

            CellArrayPerNode<int> cell_array_per_node{connectivity, 2};
            REQUIRE(cell_array_per_node.numberOfItems() == connectivity.numberOfNodes());
            REQUIRE(cell_array_per_node.numberOfArrays() == number_of_arrays(cell_array_per_node));
            REQUIRE(cell_array_per_node.sizeOfArrays() == 2);

            auto node_to_cell_matrix = connectivity.nodeToCellMatrix();
            {
              bool is_correct = true;
              for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
                is_correct &= (node_to_cell_matrix[node_id].size() == cell_array_per_node.numberOfSubArrays(node_id));
              }
              REQUIRE(is_correct);
            }
          }
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          SECTION("per cell")
          {
            NodeArrayPerCell<int> node_array_per_cell{connectivity, 3};
            REQUIRE(node_array_per_cell.numberOfItems() == connectivity.numberOfCells());
            REQUIRE(node_array_per_cell.numberOfArrays() == number_of_arrays(node_array_per_cell));
            REQUIRE(node_array_per_cell.sizeOfArrays() == 3);

            auto cell_to_node_matrix = connectivity.cellToNodeMatrix();
            {
              bool is_correct = true;
              for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
                is_correct &= (cell_to_node_matrix[cell_id].size() == node_array_per_cell.numberOfSubArrays(cell_id));
              }
              REQUIRE(is_correct);
            }

            EdgeArrayPerCell<int> edge_array_per_cell{connectivity, 4};
            REQUIRE(edge_array_per_cell.numberOfItems() == connectivity.numberOfCells());
            REQUIRE(edge_array_per_cell.numberOfArrays() == number_of_arrays(edge_array_per_cell));
            REQUIRE(edge_array_per_cell.sizeOfArrays() == 4);

            auto cell_to_edge_matrix = connectivity.cellToEdgeMatrix();
            {
              bool is_correct = true;
              for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
                is_correct &= (cell_to_edge_matrix[cell_id].size() == edge_array_per_cell.numberOfSubArrays(cell_id));
              }
              REQUIRE(is_correct);
            }

            FaceArrayPerCell<int> face_array_per_cell{connectivity, 3};
            REQUIRE(face_array_per_cell.numberOfItems() == connectivity.numberOfCells());
            REQUIRE(face_array_per_cell.numberOfArrays() == number_of_arrays(face_array_per_cell));
            REQUIRE(face_array_per_cell.sizeOfArrays() == 3);

            auto cell_to_face_matrix = connectivity.cellToFaceMatrix();
            {
              bool is_correct = true;
              for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
                is_correct &= (cell_to_face_matrix[cell_id].size() == face_array_per_cell.numberOfSubArrays(cell_id));
              }
              REQUIRE(is_correct);
            }
          }

          SECTION("per face")
          {
            CellArrayPerFace<int> cell_array_per_face{connectivity, 5};
            REQUIRE(cell_array_per_face.numberOfItems() == connectivity.numberOfFaces());
            REQUIRE(cell_array_per_face.numberOfArrays() == number_of_arrays(cell_array_per_face));
            REQUIRE(cell_array_per_face.sizeOfArrays() == 5);

            auto face_to_cell_matrix = connectivity.faceToCellMatrix();
            {
              bool is_correct = true;
              for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
                is_correct &= (face_to_cell_matrix[face_id].size() == cell_array_per_face.numberOfSubArrays(face_id));
              }
              REQUIRE(is_correct);
            }

            EdgeArrayPerFace<int> edge_array_per_face{connectivity, 3};
            REQUIRE(edge_array_per_face.numberOfItems() == connectivity.numberOfFaces());
            REQUIRE(edge_array_per_face.numberOfArrays() == number_of_arrays(edge_array_per_face));
            REQUIRE(edge_array_per_face.sizeOfArrays() == 3);

            auto face_to_edge_matrix = connectivity.faceToEdgeMatrix();
            {
              bool is_correct = true;
              for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
                is_correct &= (face_to_edge_matrix[face_id].size() == edge_array_per_face.numberOfSubArrays(face_id));
              }
              REQUIRE(is_correct);
            }

            NodeArrayPerFace<int> node_array_per_face{connectivity, 2};
            REQUIRE(node_array_per_face.numberOfItems() == connectivity.numberOfFaces());
            REQUIRE(node_array_per_face.numberOfArrays() == number_of_arrays(node_array_per_face));
            REQUIRE(node_array_per_face.sizeOfArrays() == 2);

            auto face_to_node_matrix = connectivity.faceToNodeMatrix();
            {
              bool is_correct = true;
              for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
                is_correct &= (face_to_node_matrix[face_id].size() == node_array_per_face.numberOfSubArrays(face_id));
              }
              REQUIRE(is_correct);
            }
          }

          SECTION("per edge")
          {
            CellArrayPerEdge<int> cell_array_per_edge{connectivity, 3};
            REQUIRE(cell_array_per_edge.numberOfItems() == connectivity.numberOfEdges());
            REQUIRE(cell_array_per_edge.numberOfArrays() == number_of_arrays(cell_array_per_edge));
            REQUIRE(cell_array_per_edge.sizeOfArrays() == 3);

            auto edge_to_cell_matrix = connectivity.edgeToCellMatrix();
            {
              bool is_correct = true;
              for (EdgeId edge_id = 0; edge_id < connectivity.numberOfEdges(); ++edge_id) {
                is_correct &= (edge_to_cell_matrix[edge_id].size() == cell_array_per_edge.numberOfSubArrays(edge_id));
              }
              REQUIRE(is_correct);
            }

            FaceArrayPerEdge<int> face_array_per_edge{connectivity, 5};
            REQUIRE(face_array_per_edge.numberOfItems() == connectivity.numberOfEdges());
            REQUIRE(face_array_per_edge.numberOfArrays() == number_of_arrays(face_array_per_edge));
            REQUIRE(face_array_per_edge.sizeOfArrays() == 5);

            auto edge_to_face_matrix = connectivity.edgeToFaceMatrix();
            {
              bool is_correct = true;
              for (EdgeId edge_id = 0; edge_id < connectivity.numberOfEdges(); ++edge_id) {
                is_correct &= (edge_to_face_matrix[edge_id].size() == face_array_per_edge.numberOfSubArrays(edge_id));
              }
              REQUIRE(is_correct);
            }

            NodeArrayPerEdge<int> node_array_per_edge{connectivity, 3};
            REQUIRE(node_array_per_edge.numberOfItems() == connectivity.numberOfEdges());
            REQUIRE(node_array_per_edge.numberOfArrays() == number_of_arrays(node_array_per_edge));
            REQUIRE(node_array_per_edge.sizeOfArrays() == 3);

            auto edge_to_node_matrix = connectivity.edgeToNodeMatrix();
            {
              bool is_correct = true;
              for (EdgeId edge_id = 0; edge_id < connectivity.numberOfEdges(); ++edge_id) {
                is_correct &= (edge_to_node_matrix[edge_id].size() == node_array_per_edge.numberOfSubArrays(edge_id));
              }
              REQUIRE(is_correct);
            }
          }

          SECTION("per node")
          {
            EdgeArrayPerNode<int> edge_array_per_node{connectivity, 3};
            REQUIRE(edge_array_per_node.numberOfItems() == connectivity.numberOfNodes());
            REQUIRE(edge_array_per_node.numberOfArrays() == number_of_arrays(edge_array_per_node));
            REQUIRE(edge_array_per_node.sizeOfArrays() == 3);

            auto node_to_edge_matrix = connectivity.nodeToEdgeMatrix();
            {
              bool is_correct = true;
              for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
                is_correct &= (node_to_edge_matrix[node_id].size() == edge_array_per_node.numberOfSubArrays(node_id));
              }
              REQUIRE(is_correct);
            }

            FaceArrayPerNode<int> face_array_per_node{connectivity, 4};
            REQUIRE(face_array_per_node.numberOfItems() == connectivity.numberOfNodes());
            REQUIRE(face_array_per_node.numberOfArrays() == number_of_arrays(face_array_per_node));
            REQUIRE(face_array_per_node.sizeOfArrays() == 4);

            auto node_to_face_matrix = connectivity.nodeToFaceMatrix();
            {
              bool is_correct = true;
              for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
                is_correct &= (node_to_face_matrix[node_id].size() == face_array_per_node.numberOfSubArrays(node_id));
              }
              REQUIRE(is_correct);
            }

            CellArrayPerNode<int> cell_array_per_node{connectivity, 5};
            REQUIRE(cell_array_per_node.numberOfItems() == connectivity.numberOfNodes());
            REQUIRE(cell_array_per_node.numberOfArrays() == number_of_arrays(cell_array_per_node));
            REQUIRE(cell_array_per_node.sizeOfArrays() == 5);

            auto node_to_cell_matrix = connectivity.nodeToCellMatrix();
            {
              bool is_correct = true;
              for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
                is_correct &= (node_to_cell_matrix[node_id].size() == cell_array_per_node.numberOfSubArrays(node_id));
              }
              REQUIRE(is_correct);
            }
          }
        }
      }
    }
  }

  SECTION("array view")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d = named_mesh.mesh()->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          EdgeArrayPerCell<size_t> edge_arrays_per_cell{connectivity, 3};
          {
            size_t array = 0;
            for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
              for (size_t i_edge = 0; i_edge < edge_arrays_per_cell.numberOfSubArrays(cell_id); ++i_edge) {
                for (size_t i = 0; i < edge_arrays_per_cell(cell_id, i_edge).size(); ++i) {
                  edge_arrays_per_cell(cell_id, i_edge)[i] = array++;
                }
              }
            }
          }
          {
            bool is_same = true;
            size_t k     = 0;
            for (size_t i = 0; i < edge_arrays_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < edge_arrays_per_cell.sizeOfArrays(); ++j, ++k) {
                is_same &= (edge_arrays_per_cell[i][j] == k);
              }
            }
            REQUIRE(is_same);
          }

          {
            size_t k = 0;
            for (size_t i = 0; i < edge_arrays_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < edge_arrays_per_cell.sizeOfArrays(); ++j, ++k) {
                edge_arrays_per_cell[i][j] = k * k + 1;
              }
            }
          }
          {
            bool is_same = true;
            size_t i     = 0;
            for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
              for (size_t i_edge = 0; i_edge < edge_arrays_per_cell.numberOfSubArrays(cell_id); ++i_edge) {
                for (size_t l = 0; l < edge_arrays_per_cell(cell_id, i_edge).size(); ++l, ++i) {
                  is_same &= (edge_arrays_per_cell(cell_id, i_edge)[l] == i * i + 1);
                }
              }
            }
            REQUIRE(is_same);
          }

          const EdgeArrayPerCell<const size_t> const_edge_arrays_per_cell = edge_arrays_per_cell;
          {
            bool is_same = true;
            size_t i     = 0;
            for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
              const auto& cell_table = const_edge_arrays_per_cell.itemTable(cell_id);
              for (size_t i_edge = 0; i_edge < cell_table.numberOfRows(); ++i_edge) {
                const auto& array = cell_table[i_edge];
                for (size_t l = 0; l < array.size(); ++l, ++i) {
                  is_same &= (array[l] == i * i + 1);
                }
              }
            }
            REQUIRE(is_same);
          }
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          CellArrayPerFace<size_t> cell_arrays_per_face{connectivity, 3};
          {
            size_t array = 0;
            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              for (size_t i_cell = 0; i_cell < cell_arrays_per_face.numberOfSubArrays(face_id); ++i_cell) {
                for (size_t i = 0; i < cell_arrays_per_face(face_id, i_cell).size(); ++i) {
                  cell_arrays_per_face(face_id, i_cell)[i] = array++;
                }
              }
            }
          }
          {
            bool is_same = true;
            size_t k     = 0;
            for (size_t i = 0; i < cell_arrays_per_face.numberOfArrays(); ++i) {
              for (size_t j = 0; j < cell_arrays_per_face.sizeOfArrays(); ++j, ++k) {
                is_same &= (cell_arrays_per_face[i][j] == k);
              }
            }
            REQUIRE(is_same);
          }
          {
            size_t k = 0;
            for (size_t i = 0; i < cell_arrays_per_face.numberOfArrays(); ++i) {
              for (size_t j = 0; j < cell_arrays_per_face.sizeOfArrays(); ++j, ++k) {
                cell_arrays_per_face[i][j] = 3 * k + 1;
              }
            }
          }
          {
            bool is_same = true;
            size_t i     = 0;
            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              for (size_t i_cell = 0; i_cell < cell_arrays_per_face.numberOfSubArrays(face_id); ++i_cell) {
                for (size_t l = 0; l < cell_arrays_per_face(face_id, i_cell).size(); ++l, ++i) {
                  is_same &= (cell_arrays_per_face(face_id, i_cell)[l] == 3 * i + 1);
                }
              }
            }
            REQUIRE(is_same);
          }

          const CellArrayPerFace<const size_t> const_cell_arrays_per_face = cell_arrays_per_face;
          {
            bool is_same = true;
            size_t i     = 0;
            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              const auto& face_table = const_cell_arrays_per_face.itemTable(face_id);
              for (size_t i_cell = 0; i_cell < face_table.numberOfRows(); ++i_cell) {
                const auto& array = face_table[i_cell];
                for (size_t l = 0; l < array.size(); ++l, ++i) {
                  is_same &= (array[l] == 3 * i + 1);
                }
              }
            }
            REQUIRE(is_same);
          }
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          FaceArrayPerNode<size_t> face_arrays_per_node{connectivity, 3};
          {
            size_t array = 0;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              for (size_t i_face = 0; i_face < face_arrays_per_node.numberOfSubArrays(node_id); ++i_face) {
                for (size_t i = 0; i < face_arrays_per_node(node_id, i_face).size(); ++i)
                  face_arrays_per_node(node_id, i_face)[i] = array++;
              }
            }
          }
          {
            bool is_same = true;
            size_t k     = 0;
            for (size_t i = 0; i < face_arrays_per_node.numberOfArrays(); ++i) {
              for (size_t j = 0; j < face_arrays_per_node.sizeOfArrays(); ++j, ++k) {
                is_same &= (face_arrays_per_node[i][j] == k);
              }
              REQUIRE(is_same);
            }
          }
          {
            size_t k = 0;
            for (size_t i = 0; i < face_arrays_per_node.numberOfArrays(); ++i) {
              for (size_t j = 0; j < face_arrays_per_node.sizeOfArrays(); ++j, ++k) {
                face_arrays_per_node[i][j] = 3 + k * k;
              }
            }
          }
          {
            bool is_same = true;
            size_t i     = 0;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              for (size_t i_face = 0; i_face < face_arrays_per_node.numberOfSubArrays(node_id); ++i_face) {
                for (size_t l = 0; l < face_arrays_per_node(node_id, i_face).size(); ++l, ++i) {
                  is_same &= (face_arrays_per_node(node_id, i_face)[l] == 3 + i * i);
                }
              }
            }
            REQUIRE(is_same);
          }

          const FaceArrayPerNode<const size_t> const_face_arrays_per_node = face_arrays_per_node;
          {
            bool is_same = true;
            size_t i     = 0;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              const auto& node_table = const_face_arrays_per_node.itemTable(node_id);
              for (size_t i_face = 0; i_face < node_table.numberOfRows(); ++i_face) {
                const auto& array = node_table[i_face];
                for (size_t l = 0; l < array.size(); ++l, ++i) {
                  is_same &= (array[l] == 3 + i * i);
                }
              }
            }
            REQUIRE(is_same);
          }
        }
      }
    }
  }

  SECTION("copy")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

        const Connectivity<3>& connectivity = mesh_3d->connectivity();

        SECTION("classic")
        {
          NodeArrayPerCell<size_t> node_array_per_cell{connectivity, 3};
          {
            size_t k = 0;
            for (size_t i = 0; i < node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < node_array_per_cell.sizeOfArrays(); ++j, ++k) {
                node_array_per_cell[i][j] = k;
              }
            }
          }
          NodeArrayPerCell<size_t> copy_node_array_per_cell = copy(node_array_per_cell);
          {
            bool is_same = true;
            for (size_t i = 0; i < copy_node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < node_array_per_cell.sizeOfArrays(); ++j) {
                is_same &= (copy_node_array_per_cell[i][j] == node_array_per_cell[i][j]);
              }
            }

            REQUIRE(is_same);
          }

          {
            for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
              auto cell_table = node_array_per_cell.itemTable(cell_id);
              for (size_t i_node = 0; i_node < node_array_per_cell.numberOfSubArrays(cell_id); ++i_node) {
                auto node_array = cell_table[i_node];
                for (size_t i = 0; i < node_array.size(); ++i) {
                  node_array[i] = cell_id + i_node + i;
                }
              }
            }
          }

          {
            bool is_same = true;
            for (size_t i = 0; i < copy_node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < copy_node_array_per_cell.sizeOfArrays(); ++j) {
                is_same &= (copy_node_array_per_cell[i][j] == node_array_per_cell[i][j]);
              }
            }

            REQUIRE(not is_same);
          }
        }

        SECTION("from weak")
        {
          WeakNodeArrayPerCell<size_t> node_array_per_cell{connectivity, 3};
          {
            size_t k = 0;
            for (size_t i = 0; i < node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < node_array_per_cell.sizeOfArrays(); ++j, ++k) {
                node_array_per_cell[i][j] = k;
              }
            }
          }

          WeakNodeArrayPerCell<const size_t> node_const_array_per_cell = node_array_per_cell;

          NodeArrayPerCell<size_t> copy_node_array_per_cell = copy(node_const_array_per_cell);
          {
            bool is_same = true;
            for (size_t i = 0; i < copy_node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < node_array_per_cell.sizeOfArrays(); ++j) {
                is_same &= (copy_node_array_per_cell[i][j] == node_array_per_cell[i][j]);
              }
            }

            REQUIRE(is_same);
          }

          {
            for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
              auto cell_table = node_array_per_cell.itemTable(cell_id);
              for (size_t i_node = 0; i_node < node_array_per_cell.numberOfSubArrays(cell_id); ++i_node) {
                auto node_array = cell_table[i_node];
                for (size_t i = 0; i < node_array.size(); ++i) {
                  node_array[i] = cell_id + i_node + i;
                }
              }
            }
          }

          {
            bool is_same = true;
            for (size_t i = 0; i < copy_node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < node_array_per_cell.sizeOfArrays(); ++j) {
                is_same &= (copy_node_array_per_cell[i][j] == node_array_per_cell[i][j]);
              }
            }

            REQUIRE(not is_same);
          }
        }
      }
    }
  }

  SECTION("fill")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

        const Connectivity<3>& connectivity = mesh_3d->connectivity();

        SECTION("classic")
        {
          NodeArrayPerCell<size_t> node_array_per_cell{connectivity, 3};
          {
            size_t k = 0;
            for (size_t i = 0; i < node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < node_array_per_cell.sizeOfArrays(); ++j, ++k) {
                node_array_per_cell[i][j] = k;
              }
            }
          }
          NodeArrayPerCell<size_t> copy_node_array_per_cell = copy(node_array_per_cell);
          {
            bool is_same = true;
            for (size_t i = 0; i < copy_node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < node_array_per_cell.sizeOfArrays(); ++j) {
                is_same &= (copy_node_array_per_cell[i][j] == node_array_per_cell[i][j]);
              }
            }

            REQUIRE(is_same);
          }

          node_array_per_cell.fill(11);

          {
            bool is_same = true;
            for (size_t i = 0; i < copy_node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < copy_node_array_per_cell.sizeOfArrays(); ++j) {
                is_same &= (copy_node_array_per_cell[i][j] == node_array_per_cell[i][j]);
              }
            }

            REQUIRE(not is_same);
          }

          {
            bool is_filled_with_11 = true;
            for (size_t i = 0; i < copy_node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < copy_node_array_per_cell.sizeOfArrays(); ++j) {
                is_filled_with_11 &= (node_array_per_cell[i][j] == 11);
              }
            }

            REQUIRE(is_filled_with_11);
          }
        }

        SECTION("from weak")
        {
          WeakNodeArrayPerCell<size_t> node_array_per_cell{connectivity, 3};
          {
            size_t k = 0;
            for (size_t i = 0; i < node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < node_array_per_cell.sizeOfArrays(); ++j, ++k) {
                node_array_per_cell[i][j] = k;
              }
            }
          }

          NodeArrayPerCell<size_t> copy_node_array_per_cell = copy(node_array_per_cell);
          {
            bool is_same = true;
            for (size_t i = 0; i < copy_node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < node_array_per_cell.sizeOfArrays(); ++j) {
                is_same &= (copy_node_array_per_cell[i][j] == node_array_per_cell[i][j]);
              }
            }

            REQUIRE(is_same);
          }

          node_array_per_cell.fill(13);

          {
            bool is_same = true;
            for (size_t i = 0; i < copy_node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < node_array_per_cell.sizeOfArrays(); ++j) {
                is_same &= (copy_node_array_per_cell[i][j] == node_array_per_cell[i][j]);
              }
            }

            REQUIRE(not is_same);
          }

          {
            bool is_filled_with_13 = true;
            for (size_t i = 0; i < copy_node_array_per_cell.numberOfArrays(); ++i) {
              for (size_t j = 0; j < copy_node_array_per_cell.sizeOfArrays(); ++j) {
                is_filled_with_13 &= (node_array_per_cell[i][j] == 13);
              }
            }

            REQUIRE(is_filled_with_13);
          }
        }
      }
    }
  }

  SECTION("WeakSubItemArrayPerItem")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_2d = named_mesh.mesh()->get<Mesh<2>>();

        const Connectivity<2>& connectivity = mesh_2d->connectivity();

        WeakFaceArrayPerCell<int> weak_face_array_per_cell{connectivity, 3};
        {
          size_t k = 0;
          for (size_t i = 0; i < weak_face_array_per_cell.numberOfArrays(); ++i) {
            for (size_t j = 0; j < weak_face_array_per_cell.sizeOfArrays(); ++j, ++k) {
              weak_face_array_per_cell[i][j] = k;
            }
          }
        }

        FaceArrayPerCell<const int> face_array_per_cell{weak_face_array_per_cell};

        REQUIRE(face_array_per_cell.connectivity_ptr() == weak_face_array_per_cell.connectivity_ptr());
        REQUIRE(face_array_per_cell.sizeOfArrays() == weak_face_array_per_cell.sizeOfArrays());

        bool is_same = true;
        for (size_t i = 0; i < weak_face_array_per_cell.numberOfArrays(); ++i) {
          for (size_t j = 0; j < weak_face_array_per_cell.sizeOfArrays(); ++j) {
            is_same &= (face_array_per_cell[i][j] == weak_face_array_per_cell[i][j]);
          }
        }
        REQUIRE(is_same);
      }
    }
  }

#ifndef NDEBUG
  SECTION("error")
  {
    SECTION("checking for build SubItemArrayPerItem")
    {
      CellArrayPerNode<int> cell_array_per_node;
      REQUIRE_THROWS_WITH(cell_array_per_node[0], "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_node.itemTable(NodeId{0}), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_node(NodeId{0}, 0), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_node.sizeOfArrays(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_node.numberOfArrays(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_node.numberOfItems(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_node.numberOfSubArrays(NodeId{0}), "SubItemArrayPerItem is not built");

      FaceArrayPerCell<int> face_array_per_cell;
      REQUIRE_THROWS_WITH(face_array_per_cell[0], "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(face_array_per_cell.itemTable(CellId{0}), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(face_array_per_cell(CellId{0}, 0), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(face_array_per_cell.sizeOfArrays(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(face_array_per_cell.numberOfArrays(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(face_array_per_cell.numberOfItems(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(face_array_per_cell.numberOfSubArrays(CellId{0}), "SubItemArrayPerItem is not built");

      CellArrayPerEdge<int> cell_array_per_edge;
      REQUIRE_THROWS_WITH(cell_array_per_edge[0], "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_edge.itemTable(EdgeId{0}), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_edge(EdgeId{0}, 0), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_edge.sizeOfArrays(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_edge.numberOfArrays(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_edge.numberOfItems(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(cell_array_per_edge.numberOfSubArrays(EdgeId{0}), "SubItemArrayPerItem is not built");

      NodeArrayPerFace<int> node_array_per_face;
      REQUIRE_THROWS_WITH(node_array_per_face[0], "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(node_array_per_face.itemTable(FaceId{0}), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(node_array_per_face(FaceId{0}, 0), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(node_array_per_face.sizeOfArrays(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(node_array_per_face.numberOfArrays(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(node_array_per_face.numberOfItems(), "SubItemArrayPerItem is not built");
      REQUIRE_THROWS_WITH(node_array_per_face.numberOfSubArrays(FaceId{0}), "SubItemArrayPerItem is not built");
    }

    SECTION("checking invalid table size in constructor")
    {
      auto mesh_3d = MeshDataBaseForTests::get().hybrid3DMesh()->get<Mesh<3>>();

      const Connectivity<3>& connectivity = mesh_3d->connectivity();

      {
        Table<int> table{connectivity.getMatrix(ItemType::node, ItemType::cell).numberOfValues() + 3, 2};
        REQUIRE_THROWS_WITH(CellArrayPerNode<int>(connectivity, table), "invalid size of provided table");
      }

      {
        Table<int> table{connectivity.getMatrix(ItemType::face, ItemType::edge).numberOfValues() + 3, 2};
        REQUIRE_THROWS_WITH(EdgeArrayPerFace<int>(connectivity, table), "invalid size of provided table");
      }
    }

    SECTION("checking for bounds violation")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d = named_mesh.mesh()->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          CellArrayPerFace<int> cell_array_per_face{connectivity, 3};
          {
            FaceId invalid_face_id = connectivity.numberOfFaces();
            REQUIRE_THROWS_WITH(cell_array_per_face(invalid_face_id, 0), "invalid item_id");
            REQUIRE_THROWS_WITH(cell_array_per_face[invalid_face_id], "invalid item_id");
          }
          if (connectivity.numberOfFaces() > 0) {
            FaceId face_id         = 0;
            const auto& face_table = cell_array_per_face.itemTable(face_id);
            REQUIRE_THROWS_WITH(cell_array_per_face(face_id, face_table.numberOfRows()), "invalid index");
            REQUIRE_THROWS_WITH(face_table[face_table.numberOfRows()], "invalid index");
            REQUIRE_THROWS_WITH(cell_array_per_face.itemTable(face_id)[face_table.numberOfRows()], "invalid index");
            REQUIRE_THROWS_WITH(cell_array_per_face.itemTable(face_id)[0][cell_array_per_face.sizeOfArrays()],
                                "invalid index");
            REQUIRE_THROWS_WITH(cell_array_per_face.itemTable(face_id)[0][cell_array_per_face.sizeOfArrays()] = 2,
                                "invalid index");
          }

          FaceArrayPerNode<int> face_array_per_node{connectivity, 5};
          {
            NodeId invalid_node_id = connectivity.numberOfNodes();
            REQUIRE_THROWS_WITH(face_array_per_node(invalid_node_id, 0), "invalid item_id");
            REQUIRE_THROWS_WITH(face_array_per_node[invalid_node_id], "invalid item_id");
          }
          if (connectivity.numberOfNodes() > 0) {
            NodeId node_id         = 0;
            const auto& node_table = face_array_per_node.itemTable(node_id);
            REQUIRE_THROWS_WITH(face_array_per_node(node_id, node_table.numberOfRows()), "invalid index");
            REQUIRE_THROWS_WITH(node_table[node_table.numberOfRows()], "invalid index");
            REQUIRE_THROWS_WITH(face_array_per_node.itemTable(node_id)[node_table.numberOfRows()], "invalid index");
            REQUIRE_THROWS_WITH(face_array_per_node.itemTable(node_id)[0][face_array_per_node.sizeOfArrays()],
                                "invalid index");
            REQUIRE_THROWS_WITH(face_array_per_node.itemTable(node_id)[0][face_array_per_node.sizeOfArrays()] = 2,
                                "invalid index");
          }

          EdgeArrayPerCell<int> edge_array_per_cell{connectivity, 3};
          {
            CellId invalid_cell_id = connectivity.numberOfCells();
            REQUIRE_THROWS_WITH(edge_array_per_cell(invalid_cell_id, 0), "invalid item_id");
            REQUIRE_THROWS_WITH(edge_array_per_cell[invalid_cell_id], "invalid item_id");
          }
          if (connectivity.numberOfCells() > 0) {
            CellId cell_id         = 0;
            const auto& cell_table = edge_array_per_cell.itemTable(cell_id);
            REQUIRE_THROWS_WITH(edge_array_per_cell(cell_id, cell_table.numberOfRows()), "invalid index");
            REQUIRE_THROWS_WITH(cell_table[cell_table.numberOfRows()], "invalid index");
            REQUIRE_THROWS_WITH(edge_array_per_cell.itemTable(cell_id)[cell_table.numberOfRows()], "invalid index");
            REQUIRE_THROWS_WITH(edge_array_per_cell.itemTable(cell_id)[0][edge_array_per_cell.sizeOfArrays()],
                                "invalid index");
            REQUIRE_THROWS_WITH(edge_array_per_cell.itemTable(cell_id)[0][edge_array_per_cell.sizeOfArrays()] == 2,
                                "invalid index");
          }

          NodeArrayPerEdge<int> node_array_per_edge{connectivity, 3};
          {
            EdgeId invalid_edge_id = connectivity.numberOfEdges();
            REQUIRE_THROWS_WITH(node_array_per_edge(invalid_edge_id, 0), "invalid item_id");
            REQUIRE_THROWS_WITH(node_array_per_edge[invalid_edge_id], "invalid item_id");
          }
          if (connectivity.numberOfEdges() > 0) {
            EdgeId edge_id         = 0;
            const auto& edge_table = node_array_per_edge.itemTable(edge_id);
            REQUIRE_THROWS_WITH(node_array_per_edge(edge_id, edge_table.numberOfRows()), "invalid index");
            REQUIRE_THROWS_WITH(edge_table[edge_table.numberOfRows()], "invalid index");
            REQUIRE_THROWS_WITH(node_array_per_edge.itemTable(edge_id)[edge_table.numberOfRows()], "invalid index");
            REQUIRE_THROWS_WITH(node_array_per_edge.itemTable(edge_id)[0][node_array_per_edge.sizeOfArrays()],
                                "invalid index");
            REQUIRE_THROWS_WITH(node_array_per_edge.itemTable(edge_id)[0][node_array_per_edge.sizeOfArrays()] = 2,
                                "invalid index");
          }
        }
      }
    }
  }
#endif   // NDEBUG
}
