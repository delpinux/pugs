#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <analysis/GaussQuadratureDescriptor.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("GaussQuadratureDescriptor", "[analysis]")
{
  GaussQuadratureDescriptor quadrature_descriptor(3);

  REQUIRE(quadrature_descriptor.isTensorial() == false);
  REQUIRE(quadrature_descriptor.type() == QuadratureType::Gauss);
  REQUIRE(quadrature_descriptor.degree() == 3);
  REQUIRE(quadrature_descriptor.name() == ::name(QuadratureType::Gauss) + "(3)");
}
