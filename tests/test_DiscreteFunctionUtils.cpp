#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <scheme/DiscreteFunctionP0.hpp>
#include <scheme/DiscreteFunctionP0Vector.hpp>
#include <scheme/DiscreteFunctionUtils.hpp>

#include <mesh/CartesianMeshBuilder.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("DiscreteFunctionUtils", "[scheme]")
{
  SECTION("1D")
  {
    constexpr size_t Dimension = 1;

    std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_v = named_mesh.mesh();
        auto mesh   = mesh_v->get<Mesh<Dimension>>();

        std::shared_ptr mesh_copy =
          std::make_shared<const std::decay_t<decltype(*mesh)>>(mesh->shared_connectivity(), mesh->xr());
        std::shared_ptr mesh_copy_v = std::make_shared<const MeshVariant>(mesh_copy);

        SECTION("common mesh")
        {
          DiscreteFunctionP0<double> uh(mesh);
          DiscreteFunctionP0<double> vh(mesh);
          DiscreteFunctionP0<TinyVector<2>> wh(mesh);

          DiscreteFunctionP0<double> qh(mesh_copy);

          std::shared_ptr uh_v = std::make_shared<DiscreteFunctionVariant>(uh);
          std::shared_ptr vh_v = std::make_shared<DiscreteFunctionVariant>(vh);
          std::shared_ptr wh_v = std::make_shared<DiscreteFunctionVariant>(wh);
          std::shared_ptr qh_v = std::make_shared<DiscreteFunctionVariant>(qh);

          REQUIRE(getCommonMesh({uh_v, vh_v, wh_v})->id() == mesh->id());
          REQUIRE(getCommonMesh({uh_v, vh_v, wh_v, qh_v}).use_count() == 0);
        }

        SECTION("check discretization type")
        {
          DiscreteFunctionP0<double> uh(mesh);
          DiscreteFunctionP0<double> vh(mesh);
          DiscreteFunctionP0<double> qh(mesh_copy);

          DiscreteFunctionP0Vector<double> Uh(mesh, 3);
          DiscreteFunctionP0Vector<double> Vh(mesh, 3);

          auto uh_v = std::make_shared<DiscreteFunctionVariant>(uh);
          auto vh_v = std::make_shared<DiscreteFunctionVariant>(vh);
          auto qh_v = std::make_shared<DiscreteFunctionVariant>(qh);
          auto Uh_v = std::make_shared<DiscreteFunctionVariant>(Uh);
          auto Vh_v = std::make_shared<DiscreteFunctionVariant>(Vh);

          REQUIRE(checkDiscretizationType({uh_v}, DiscreteFunctionType::P0));
          REQUIRE(checkDiscretizationType({uh_v, vh_v, qh_v}, DiscreteFunctionType::P0));
          REQUIRE(not checkDiscretizationType({uh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(not checkDiscretizationType({uh_v, vh_v, qh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(checkDiscretizationType({Uh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(checkDiscretizationType({Uh_v, Vh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(not checkDiscretizationType({Uh_v, Vh_v}, DiscreteFunctionType::P0));
          REQUIRE(not checkDiscretizationType({Uh_v}, DiscreteFunctionType::P0));
        }

        SECTION("scalar function shallow copy")
        {
          using DiscreteFunctionT = DiscreteFunctionP0<const double>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<double>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^1 function shallow copy")
        {
          using DataType          = TinyVector<1>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^2 function shallow copy")
        {
          using DataType          = TinyVector<2>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^3 function shallow copy")
        {
          using DataType          = TinyVector<3>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^1x1 function shallow copy")
        {
          using DataType          = TinyMatrix<1>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^2x2 function shallow copy")
        {
          using DataType          = TinyMatrix<2>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^3x3 function shallow copy")
        {
          using DataType          = TinyMatrix<3>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("P0Vector function shallow copy")
        {
          using DiscreteFunctionT = DiscreteFunctionP0Vector<const double>;
          std::shared_ptr uh = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0Vector<double>(mesh, 2));
          std::shared_ptr vh = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellArrays()[CellId{0}][0]) ==
                  &(wh->get<DiscreteFunctionT>().cellArrays()[CellId{0}][0]));
        }
      }
    }
  }

  SECTION("2D")
  {
    constexpr size_t Dimension = 2;

    std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_v = named_mesh.mesh();
        auto mesh   = mesh_v->get<Mesh<Dimension>>();
        std::shared_ptr mesh_copy =
          std::make_shared<const std::decay_t<decltype(*mesh)>>(mesh->shared_connectivity(), mesh->xr());
        std::shared_ptr mesh_copy_v = std::make_shared<const MeshVariant>(mesh_copy);

        SECTION("common mesh")
        {
          DiscreteFunctionP0<double> uh(mesh);
          DiscreteFunctionP0<double> vh(mesh);
          DiscreteFunctionP0<TinyVector<2>> wh(mesh);

          DiscreteFunctionP0<double> qh(mesh_copy);

          std::shared_ptr uh_v = std::make_shared<DiscreteFunctionVariant>(uh);
          std::shared_ptr vh_v = std::make_shared<DiscreteFunctionVariant>(vh);
          std::shared_ptr wh_v = std::make_shared<DiscreteFunctionVariant>(wh);
          std::shared_ptr qh_v = std::make_shared<DiscreteFunctionVariant>(qh);

          REQUIRE(getCommonMesh({uh_v, vh_v, wh_v})->id() == mesh->id());
          REQUIRE(getCommonMesh({uh_v, vh_v, wh_v, qh_v}).use_count() == 0);
        }

        SECTION("check discretization type")
        {
          DiscreteFunctionP0<double> uh(mesh);
          DiscreteFunctionP0<double> vh(mesh);
          DiscreteFunctionP0<double> qh(mesh_copy);

          DiscreteFunctionP0Vector<double> Uh(mesh, 3);
          DiscreteFunctionP0Vector<double> Vh(mesh, 3);

          auto uh_v = std::make_shared<DiscreteFunctionVariant>(uh);
          auto vh_v = std::make_shared<DiscreteFunctionVariant>(vh);
          auto qh_v = std::make_shared<DiscreteFunctionVariant>(qh);
          auto Uh_v = std::make_shared<DiscreteFunctionVariant>(Uh);
          auto Vh_v = std::make_shared<DiscreteFunctionVariant>(Vh);

          REQUIRE(checkDiscretizationType({uh_v}, DiscreteFunctionType::P0));
          REQUIRE(checkDiscretizationType({uh_v, vh_v, qh_v}, DiscreteFunctionType::P0));
          REQUIRE(not checkDiscretizationType({uh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(not checkDiscretizationType({uh_v, vh_v, qh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(checkDiscretizationType({Uh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(checkDiscretizationType({Uh_v, Vh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(not checkDiscretizationType({Uh_v, Vh_v}, DiscreteFunctionType::P0));
          REQUIRE(not checkDiscretizationType({Uh_v}, DiscreteFunctionType::P0));
        }

        SECTION("scalar function shallow copy")
        {
          using DiscreteFunctionT = DiscreteFunctionP0<const double>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<double>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^1 function shallow copy")
        {
          using DataType          = TinyVector<1>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^2 function shallow copy")
        {
          using DataType          = TinyVector<2>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^3 function shallow copy")
        {
          using DataType          = TinyVector<3>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^1x1 function shallow copy")
        {
          using DataType          = TinyMatrix<1>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^2x2 function shallow copy")
        {
          using DataType          = TinyMatrix<2>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^3x3 function shallow copy")
        {
          using DataType          = TinyMatrix<3>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("P0Vector function shallow copy")
        {
          using DiscreteFunctionT = DiscreteFunctionP0Vector<const double>;
          std::shared_ptr uh = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0Vector<double>(mesh, 2));
          std::shared_ptr vh = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellArrays()[CellId{0}][0]) ==
                  &(wh->get<DiscreteFunctionT>().cellArrays()[CellId{0}][0]));
        }
      }
    }
  }

  SECTION("3D")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_v = named_mesh.mesh();
        auto mesh   = mesh_v->get<Mesh<3>>();

        std::shared_ptr mesh_copy =
          std::make_shared<const std::decay_t<decltype(*mesh)>>(mesh->shared_connectivity(), mesh->xr());
        std::shared_ptr mesh_copy_v = std::make_shared<const MeshVariant>(mesh_copy);

        SECTION("common mesh")
        {
          DiscreteFunctionP0<double> uh(mesh);
          DiscreteFunctionP0<double> vh(mesh);
          DiscreteFunctionP0<TinyVector<2>> wh(mesh);

          DiscreteFunctionP0<double> qh(mesh_copy);

          std::shared_ptr uh_v = std::make_shared<DiscreteFunctionVariant>(uh);
          std::shared_ptr vh_v = std::make_shared<DiscreteFunctionVariant>(vh);
          std::shared_ptr wh_v = std::make_shared<DiscreteFunctionVariant>(wh);
          std::shared_ptr qh_v = std::make_shared<DiscreteFunctionVariant>(qh);

          REQUIRE(getCommonMesh({uh_v, vh_v, wh_v})->id() == mesh->id());
          REQUIRE(getCommonMesh({uh_v, vh_v, wh_v, qh_v}).use_count() == 0);
        }

        SECTION("check discretization type")
        {
          DiscreteFunctionP0<double> uh(mesh);
          DiscreteFunctionP0<double> vh(mesh);
          DiscreteFunctionP0<double> qh(mesh_copy);

          DiscreteFunctionP0Vector<double> Uh(mesh, 3);
          DiscreteFunctionP0Vector<double> Vh(mesh, 3);

          auto uh_v = std::make_shared<DiscreteFunctionVariant>(uh);
          auto vh_v = std::make_shared<DiscreteFunctionVariant>(vh);
          auto qh_v = std::make_shared<DiscreteFunctionVariant>(qh);
          auto Uh_v = std::make_shared<DiscreteFunctionVariant>(Uh);
          auto Vh_v = std::make_shared<DiscreteFunctionVariant>(Vh);

          REQUIRE(checkDiscretizationType({uh_v}, DiscreteFunctionType::P0));
          REQUIRE(checkDiscretizationType({uh_v, vh_v, qh_v}, DiscreteFunctionType::P0));
          REQUIRE(not checkDiscretizationType({uh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(not checkDiscretizationType({uh_v, vh_v, qh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(checkDiscretizationType({Uh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(checkDiscretizationType({Uh_v, Vh_v}, DiscreteFunctionType::P0Vector));
          REQUIRE(not checkDiscretizationType({Uh_v, Vh_v}, DiscreteFunctionType::P0));
          REQUIRE(not checkDiscretizationType({Uh_v}, DiscreteFunctionType::P0));
        }

        SECTION("scalar function shallow copy")
        {
          using DiscreteFunctionT = DiscreteFunctionP0<const double>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<double>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^1 function shallow copy")
        {
          using DataType          = TinyVector<1>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^2 function shallow copy")
        {
          using DataType          = TinyVector<2>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^3 function shallow copy")
        {
          using DataType          = TinyVector<3>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^1x1 function shallow copy")
        {
          using DataType          = TinyMatrix<1>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^2x2 function shallow copy")
        {
          using DataType          = TinyMatrix<2>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("R^3x3 function shallow copy")
        {
          using DataType          = TinyMatrix<3>;
          using DiscreteFunctionT = DiscreteFunctionP0<const DataType>;
          std::shared_ptr uh      = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<DataType>(mesh));
          std::shared_ptr vh      = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellValues()[CellId{0}]) ==
                  &(wh->get<DiscreteFunctionT>().cellValues()[CellId{0}]));
        }

        SECTION("P0Vector function shallow copy")
        {
          using DiscreteFunctionT = DiscreteFunctionP0Vector<const double>;
          std::shared_ptr uh = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0Vector<double>(mesh, 2));
          std::shared_ptr vh = shallowCopy(mesh_v, uh);

          REQUIRE(uh == vh);

          std::shared_ptr wh = shallowCopy(mesh_copy_v, uh);

          REQUIRE(uh != wh);
          REQUIRE(&(uh->get<DiscreteFunctionT>().cellArrays()[CellId{0}][0]) ==
                  &(wh->get<DiscreteFunctionT>().cellArrays()[CellId{0}][0]));
        }
      }
    }
  }

  SECTION("errors")
  {
    SECTION("different connectivities")
    {
      constexpr size_t Dimension = 1;

      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh = named_mesh.mesh()->get<Mesh<Dimension>>();

          std::shared_ptr other_mesh =
            CartesianMeshBuilder{TinyVector<1>{-1}, TinyVector<1>{3}, TinyVector<1, size_t>{19}}.mesh()->get<Mesh<1>>();
          std::shared_ptr other_mesh_v = std::make_shared<const MeshVariant>(other_mesh);

          std::shared_ptr uh = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<double>(mesh));

          REQUIRE_THROWS_WITH(shallowCopy(other_mesh_v, uh), "error: cannot shallow copy when connectivity changes");
        }
      }
    }

    SECTION("incompatible mesh dimension")
    {
      std::shared_ptr mesh_1d = MeshDataBaseForTests::get().cartesian1DMesh()->get<Mesh<1>>();
      std::shared_ptr mesh_2d_v =
        std::make_shared<const MeshVariant>(MeshDataBaseForTests::get().cartesian2DMesh()->get<Mesh<2>>());

      std::shared_ptr uh = std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionP0<double>(mesh_1d));

      REQUIRE_THROWS_WITH(shallowCopy(mesh_2d_v, uh), "error: incompatible mesh dimensions");
    }
  }
}
