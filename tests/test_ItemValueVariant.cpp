#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemValueVariant.hpp>
#include <mesh/Mesh.hpp>
#include <utils/Messenger.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ItemValueVariant", "[mesh]")
{
  auto mesh_2d_v = MeshDataBaseForTests::get().hybrid2DMesh();
  auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

  const Connectivity<2>& connectivity = *mesh_2d->shared_connectivity();

  using R1 = TinyVector<1>;
  using R2 = TinyVector<2>;
  using R3 = TinyVector<3>;

  using R1x1 = TinyMatrix<1>;
  using R2x2 = TinyMatrix<2>;
  using R3x3 = TinyMatrix<3>;

  SECTION("NodeValue<double>")
  {
    NodeValue<double> node_value{connectivity};
    ItemValueVariant v(node_value);
    REQUIRE_NOTHROW(v.get<NodeValue<const double>>());
    REQUIRE_THROWS_WITH(v.get<NodeValue<int64_t>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<NodeValue<uint64_t>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<NodeValue<const R1>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<NodeValue<const R2>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<NodeValue<const R3>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<NodeValue<const R1x1>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<NodeValue<const R2x2>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<NodeValue<const R3x3>>(), "error: invalid ItemValue type");

    REQUIRE_THROWS_WITH(v.get<EdgeValue<const double>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<FaceValue<const double>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<CellValue<const double>>(), "error: invalid ItemValue type");
  }

  SECTION("EdgeValue<R3>")
  {
    EdgeValue<R3> node_value{connectivity};
    ItemValueVariant v(node_value);
    REQUIRE_THROWS_WITH(v.get<EdgeValue<int64_t>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<EdgeValue<uint64_t>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<EdgeValue<double>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<EdgeValue<const R1>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<EdgeValue<const R2>>(), "error: invalid ItemValue type");
    REQUIRE_NOTHROW(v.get<EdgeValue<const R3>>());
    REQUIRE_THROWS_WITH(v.get<EdgeValue<const R1x1>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<EdgeValue<const R2x2>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<EdgeValue<const R3x3>>(), "error: invalid ItemValue type");

    REQUIRE_THROWS_WITH(v.get<NodeValue<const R3>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<FaceValue<const R3>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<CellValue<const R3>>(), "error: invalid ItemValue type");
  }

  SECTION("FaceValue<R3x3>")
  {
    FaceValue<R3x3> node_value{connectivity};
    ItemValueVariant v(node_value);
    REQUIRE_THROWS_WITH(v.get<FaceValue<int64_t>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<FaceValue<uint64_t>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<FaceValue<double>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<FaceValue<const R1>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<FaceValue<const R2>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<FaceValue<const R3>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<FaceValue<const R1x1>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<FaceValue<const R2x2>>(), "error: invalid ItemValue type");
    REQUIRE_NOTHROW(v.get<FaceValue<const R3x3>>());

    REQUIRE_THROWS_WITH(v.get<NodeValue<const R3x3>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<EdgeValue<const R3x3>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<CellValue<const R3x3>>(), "error: invalid ItemValue type");
  }

  SECTION("CellValue<int64_t>")
  {
    CellValue<int64_t> node_value{connectivity};
    ItemValueVariant v(node_value);
    REQUIRE_NOTHROW(v.get<CellValue<const int64_t>>());
    REQUIRE_THROWS_WITH(v.get<CellValue<const uint64_t>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<CellValue<const double>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<CellValue<const R1>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<CellValue<const R2>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<CellValue<const R3>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<CellValue<const R1x1>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<CellValue<const R2x2>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<CellValue<const R3x3>>(), "error: invalid ItemValue type");

    REQUIRE_THROWS_WITH(v.get<NodeValue<const int64_t>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<EdgeValue<const int64_t>>(), "error: invalid ItemValue type");
    REQUIRE_THROWS_WITH(v.get<FaceValue<const int64_t>>(), "error: invalid ItemValue type");
  }
}
