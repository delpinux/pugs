#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Messenger.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshVariant.hpp>
#include <utils/checkpointing/ReadItemValue.hpp>
#include <utils/checkpointing/WriteItemValue.hpp>

#include <MeshDataBaseForTests.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_ItemValue", "[utils/checkpointing]")
{
  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});
    HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

    SECTION("ItemValue")
    {
      HighFive::Group checkpoint_group = file.createGroup("checkpoint_group");
      HighFive::Group useless_group;

      auto mesh_2d = MeshDataBaseForTests::get().hybrid2DMesh()->get<Mesh<2>>();

      auto xr = mesh_2d->xr();
      checkpointing::write(checkpoint_group, "xr", xr);

      file.flush();

      auto is_same = [](const auto& a, const auto& b) {
        bool same = true;
        for (size_t i = 0; i < a.size(); ++i) {
          if (a[i] != b[i]) {
            same = false;
          }
        }
        return parallel::allReduceAnd(same);
      };

      NodeValue<TinyVector<2>> read_xr =
        checkpointing::readItemValue<TinyVector<2>, ItemType::node>(checkpoint_group, "xr", mesh_2d->connectivity());
      REQUIRE(is_same(xr.arrayView(), read_xr.arrayView()));
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }
}
