#ifndef CHECKPOINTING_CONNECTIVITY_UTILITIES_HPP
#define CHECKPOINTING_CONNECTIVITY_UTILITIES_HPP

#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityDescriptor.hpp>
#include <utils/Messenger.hpp>
#include <utils/PugsMacros.hpp>

namespace test_only
{

PUGS_INLINE
std::shared_ptr<const IConnectivity>
duplicateConnectivity(const IConnectivity& i_connectivity)
{
  auto setRefItemLists = [](const auto& connectivity, ConnectivityDescriptor& descriptor) {
    for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::cell>(); ++i) {
      descriptor.addRefItemList(connectivity.template refItemList<ItemType::cell>(i));
    }
    for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::face>(); ++i) {
      descriptor.addRefItemList(connectivity.template refItemList<ItemType::face>(i));
    }
    for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::edge>(); ++i) {
      descriptor.addRefItemList(connectivity.template refItemList<ItemType::edge>(i));
    }
    for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::node>(); ++i) {
      descriptor.addRefItemList(connectivity.template refItemList<ItemType::node>(i));
    }
  };

  ConnectivityDescriptor descriptor;
  switch (i_connectivity.dimension()) {
  case 1: {
    using ConnectivityType = Connectivity<1>;

    const ConnectivityType& connectivity = dynamic_cast<const ConnectivityType&>(i_connectivity);
    descriptor.setCellNumberVector(connectivity.cellNumber().arrayView());
    descriptor.setNodeNumberVector(connectivity.nodeNumber().arrayView());

    descriptor.setCellTypeVector(connectivity.cellType().arrayView());

    descriptor.setCellOwnerVector(connectivity.cellOwner().arrayView());
    descriptor.setNodeOwnerVector(connectivity.nodeOwner().arrayView());

    descriptor.setCellToNodeMatrix(connectivity.getMatrix(ItemType::cell, ItemType::node));

    setRefItemLists(connectivity, descriptor);

    return ConnectivityType::build(descriptor);
  }
  case 2: {
    using ConnectivityType = Connectivity<2>;

    const ConnectivityType& connectivity = dynamic_cast<const ConnectivityType&>(i_connectivity);
    descriptor.setCellNumberVector(connectivity.cellNumber().arrayView());
    descriptor.setFaceNumberVector(connectivity.faceNumber().arrayView());
    descriptor.setNodeNumberVector(connectivity.nodeNumber().arrayView());

    descriptor.setCellTypeVector(connectivity.cellType().arrayView());

    descriptor.setCellFaceIsReversed(connectivity.cellFaceIsReversed().arrayView());

    descriptor.setCellOwnerVector(connectivity.cellOwner().arrayView());
    descriptor.setFaceOwnerVector(connectivity.faceOwner().arrayView());
    descriptor.setNodeOwnerVector(connectivity.nodeOwner().arrayView());

    descriptor.setCellToFaceMatrix(connectivity.getMatrix(ItemType::cell, ItemType::face));
    descriptor.setCellToNodeMatrix(connectivity.getMatrix(ItemType::cell, ItemType::node));
    descriptor.setFaceToNodeMatrix(connectivity.getMatrix(ItemType::face, ItemType::node));

    setRefItemLists(connectivity, descriptor);

    return ConnectivityType::build(descriptor);
  }
  case 3: {
    using ConnectivityType = Connectivity<3>;

    const ConnectivityType& connectivity = dynamic_cast<const ConnectivityType&>(i_connectivity);
    descriptor.setCellNumberVector(connectivity.cellNumber().arrayView());
    descriptor.setFaceNumberVector(connectivity.faceNumber().arrayView());
    descriptor.setEdgeNumberVector(connectivity.edgeNumber().arrayView());
    descriptor.setNodeNumberVector(connectivity.nodeNumber().arrayView());

    descriptor.setCellTypeVector(connectivity.cellType().arrayView());

    descriptor.setCellFaceIsReversed(connectivity.cellFaceIsReversed().arrayView());
    descriptor.setFaceEdgeIsReversed(connectivity.faceEdgeIsReversed().arrayView());

    descriptor.setCellOwnerVector(connectivity.cellOwner().arrayView());
    descriptor.setFaceOwnerVector(connectivity.faceOwner().arrayView());
    descriptor.setEdgeOwnerVector(connectivity.edgeOwner().arrayView());
    descriptor.setNodeOwnerVector(connectivity.nodeOwner().arrayView());

    descriptor.setCellToFaceMatrix(connectivity.getMatrix(ItemType::cell, ItemType::face));
    descriptor.setCellToEdgeMatrix(connectivity.getMatrix(ItemType::cell, ItemType::edge));
    descriptor.setCellToNodeMatrix(connectivity.getMatrix(ItemType::cell, ItemType::node));
    descriptor.setFaceToEdgeMatrix(connectivity.getMatrix(ItemType::face, ItemType::edge));
    descriptor.setFaceToNodeMatrix(connectivity.getMatrix(ItemType::face, ItemType::node));
    descriptor.setEdgeToNodeMatrix(connectivity.getMatrix(ItemType::edge, ItemType::node));

    setRefItemLists(connectivity, descriptor);

    return ConnectivityType::build(descriptor);
  }
  default: {
    throw UnexpectedError("invalid connectivity");
  }
  }
}

template <size_t Dimension>
PUGS_INLINE bool
isSameConnectivity(const Connectivity<Dimension>& connectivity, const Connectivity<Dimension>& read_connectivity)
{
  using ConnectivityType = std::decay_t<decltype(connectivity)>;

  auto same_value = [](const auto& a, const auto& b) -> bool {
    bool same = true;
    for (size_t i = 0; i < a.size(); ++i) {
      same &= (a[i] == b[i]);
    }
    return parallel::allReduceAnd(same);
  };

  auto same_ref_item_list = [&same_value](const auto& a, const auto& b) -> bool {
    REQUIRE(a.type() == b.type());
    REQUIRE(a.refId() == b.refId());

    return same_value(a.list(), b.list());
  };

  bool same = true;
  same &= same_value(connectivity.cellNumber().arrayView(), read_connectivity.cellNumber().arrayView());
  same &= same_value(connectivity.nodeNumber().arrayView(), read_connectivity.nodeNumber().arrayView());

  same &= same_value(connectivity.cellType().arrayView(), read_connectivity.cellType().arrayView());

  same &= same_value(connectivity.cellOwner().arrayView(), read_connectivity.cellOwner().arrayView());
  same &= same_value(connectivity.nodeOwner().arrayView(), read_connectivity.nodeOwner().arrayView());

  same &= same_value(connectivity.cellToNodeMatrix().values(), read_connectivity.cellToNodeMatrix().values());

  if constexpr (ConnectivityType::Dimension >= 2) {
    same &= same_value(connectivity.faceNumber().arrayView(), read_connectivity.faceNumber().arrayView());
    same &= same_value(connectivity.faceOwner().arrayView(), read_connectivity.faceOwner().arrayView());
    same &=
      same_value(connectivity.cellFaceIsReversed().arrayView(), read_connectivity.cellFaceIsReversed().arrayView());
    same &= same_value(connectivity.cellToFaceMatrix().values(), read_connectivity.cellToFaceMatrix().values());
    same &= same_value(connectivity.faceToNodeMatrix().values(), read_connectivity.faceToNodeMatrix().values());
  }

  if constexpr (ConnectivityType::Dimension == 3) {
    same &= same_value(connectivity.edgeNumber().arrayView(), read_connectivity.edgeNumber().arrayView());
    same &= same_value(connectivity.edgeOwner().arrayView(), read_connectivity.edgeOwner().arrayView());
    same &=
      same_value(connectivity.faceEdgeIsReversed().arrayView(), read_connectivity.faceEdgeIsReversed().arrayView());
    same &= same_value(connectivity.cellToEdgeMatrix().values(), read_connectivity.cellToEdgeMatrix().values());
    same &= same_value(connectivity.faceToEdgeMatrix().values(), read_connectivity.faceToEdgeMatrix().values());
    same &= same_value(connectivity.edgeToNodeMatrix().values(), read_connectivity.edgeToNodeMatrix().values());
  }

  REQUIRE(connectivity.template numberOfRefItemList<ItemType::cell>() ==
          read_connectivity.template numberOfRefItemList<ItemType::cell>());
  for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::cell>(); ++i) {
    same &= same_ref_item_list(connectivity.template refItemList<ItemType::cell>(i),
                               read_connectivity.template refItemList<ItemType::cell>(i));
  }
  REQUIRE(connectivity.template numberOfRefItemList<ItemType::face>() ==
          read_connectivity.template numberOfRefItemList<ItemType::face>());
  for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::face>(); ++i) {
    same &= same_ref_item_list(connectivity.template refItemList<ItemType::face>(i),
                               read_connectivity.template refItemList<ItemType::face>(i));
  }
  REQUIRE(connectivity.template numberOfRefItemList<ItemType::edge>() ==
          read_connectivity.template numberOfRefItemList<ItemType::edge>());
  for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::edge>(); ++i) {
    same &= same_ref_item_list(connectivity.template refItemList<ItemType::edge>(i),
                               read_connectivity.template refItemList<ItemType::edge>(i));
  }
  REQUIRE(connectivity.template numberOfRefItemList<ItemType::node>() ==
          read_connectivity.template numberOfRefItemList<ItemType::node>());
  for (size_t i = 0; i < connectivity.template numberOfRefItemList<ItemType::node>(); ++i) {
    same &= same_ref_item_list(connectivity.template refItemList<ItemType::node>(i),
                               read_connectivity.template refItemList<ItemType::node>(i));
  }

  return same;
}

PUGS_INLINE bool
isSameConnectivity(const IConnectivity& i_connectivity, const IConnectivity& read_i_connectivity)
{
  if (i_connectivity.dimension() != read_i_connectivity.dimension()) {
    return false;
  } else {
    switch (i_connectivity.dimension()) {
    case 1: {
      constexpr size_t Dimension = 1;

      const Connectivity<Dimension>& connectivity = dynamic_cast<const Connectivity<Dimension>&>(i_connectivity);
      const Connectivity<Dimension>& read_connectivity =
        dynamic_cast<const Connectivity<Dimension>&>(read_i_connectivity);

      return isSameConnectivity(connectivity, read_connectivity);
    }
    case 2: {
      constexpr size_t Dimension = 2;

      const Connectivity<Dimension>& connectivity = dynamic_cast<const Connectivity<Dimension>&>(i_connectivity);
      const Connectivity<Dimension>& read_connectivity =
        dynamic_cast<const Connectivity<Dimension>&>(read_i_connectivity);

      return isSameConnectivity(connectivity, read_connectivity);
    }
    case 3: {
      constexpr size_t Dimension = 3;

      const Connectivity<Dimension>& connectivity = dynamic_cast<const Connectivity<Dimension>&>(i_connectivity);
      const Connectivity<Dimension>& read_connectivity =
        dynamic_cast<const Connectivity<Dimension>&>(read_i_connectivity);

      return isSameConnectivity(connectivity, read_connectivity);
    }
    default: {
      return false;
    }
    }
  }
}

}   // namespace test_only

#endif   // CHECKPOINTING_CONNECTIVITY_UTILITIES_HPP
