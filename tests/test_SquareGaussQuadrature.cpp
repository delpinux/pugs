#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <algebra/TinyMatrix.hpp>

#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <analysis/SquareGaussQuadrature.hpp>
#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("SquareGaussQuadrature", "[analysis]")
{
  auto integrate = [](auto f, auto quadrature_formula) {
    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    auto value = weight_list[0] * f(point_list[0]);
    for (size_t i = 1; i < weight_list.size(); ++i) {
      value += weight_list[i] * f(point_list[i]);
    }

    return value;
  };

  auto integrate_on_rectangle = [](auto f, auto quadrature_formula, const std::array<TinyVector<2>, 3>& triangle) {
    const auto& A = triangle[0];
    const auto& B = triangle[1];
    const auto& C = triangle[2];

    TinyMatrix<2> J;
    for (size_t i = 0; i < 2; ++i) {
      J(i, 0) = 0.5 * (B[i] - A[i]);
      J(i, 1) = 0.5 * (C[i] - A[i]);
    }
    TinyVector s = 0.5 * (B + C);

    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    auto value = weight_list[0] * f(J * (point_list[0]) + s);
    for (size_t i = 1; i < weight_list.size(); ++i) {
      value += weight_list[i] * f(J * (point_list[i]) + s);
    }

    return det(J) * value;
  };

  auto get_order = [&integrate, &integrate_on_rectangle](auto f, auto quadrature_formula, const double exact_value) {
    using R2               = TinyVector<2>;
    const double int_K_hat = integrate(f, quadrature_formula);
    const double int_refined   //
      = integrate_on_rectangle(f, quadrature_formula, {R2{-1, -1}, R2{0, -1}, R2{-1, 0}}) +
        integrate_on_rectangle(f, quadrature_formula, {R2{0, -1}, R2{1, -1}, R2{0, 0}}) +
        integrate_on_rectangle(f, quadrature_formula, {R2{-1, 0}, R2{0, 0}, R2{-1, 1}}) +
        integrate_on_rectangle(f, quadrature_formula, {R2{0, 0}, R2{1, 0}, R2{0, 1}});

    return -std::log((int_refined - exact_value) / (int_K_hat - exact_value)) / std::log(2);
  };

  auto p0 = [](const TinyVector<2>&) { return 2; };
  auto p1 = [](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return 2 * x + 3 * y + 1;
  };
  auto p2 = [&p1](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p1(X) * (2.5 * x - 3 * y + 3);
  };
  auto p3 = [&p2](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p2(X) * (-1.5 * x + 3 * y - 3);
  };
  auto p4 = [&p3](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p3(X) * (x + y + 1);
  };
  auto p5 = [&p4](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p4(X) * (-0.2 * x - 1.3 * y - 0.7);
  };
  auto p6 = [&p5](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p5(X) * (3 * x - 2 * y + 3);
  };
  auto p7 = [&p6](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p6(X) * (-2 * x + 4 * y - 7);
  };
  auto p8 = [&p7](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p7(X) * (2 * x - 3 * y - 3);
  };
  auto p9 = [&p8](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p8(X) * (x + 2 * y - 1.7);
  };
  auto p10 = [&p9](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p9(X) * (-1.3 * x - 1.7 * y + 1.3);
  };
  auto p11 = [&p10](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p10(X) * (0.8 * x - 3.1 * y + 0.6);
  };
  auto p12 = [&p11](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p11(X) * (1.8 * x + 1.3 * y - 0.3);
  };
  auto p13 = [&p12](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p12(X) * (-0.9 * x + 1.1 * y - 0.6);
  };
  auto p14 = [&p13](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p13(X) * (0.6 * x + 0.3 * y + 1.1);
  };
  auto p15 = [&p14](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p14(X) * (0.5 * x - 0.4 * y - 1.1);
  };
  auto p16 = [&p15](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p15(X) * (-0.3 * x - 0.3 * y - 0.2);
  };
  auto p17 = [&p16](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p16(X) * (-0.1 * x - 0.4 * y - 0.3);
  };
  auto p18 = [&p17](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p17(X) * (0.2 * x + 0.3 * y + 0.3);
  };
  auto p19 = [&p18](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p18(X) * (2.1 * x + 3.3 * y - 0.3);
  };
  auto p20 = [&p19](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p19(X) * (1.2 * x - 2.1 * y + 0.6);
  };
  auto p21 = [&p20](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p20(X) * (-1.3 * x - 1.2 * y + 0.7);
  };
  auto p22 = [&p21](const TinyVector<2>& X) {
    const double x = X[0];
    const double y = X[1];
    return p21(X) * (1.1 * x - 2.2 * y - 0.3);
  };

  SECTION("degree 0 and 1")
  {
    const QuadratureFormula<2>& l1 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(1));

    REQUIRE(l1.numberOfPoints() == 1);

    REQUIRE(integrate(p0, l1) == Catch::Approx(8));
    REQUIRE(integrate(p1, l1) == Catch::Approx(4));
    REQUIRE(integrate(p2, l1) != Catch::Approx(20. / 3));

    REQUIRE(get_order(p2, l1, 20. / 3) == Catch::Approx(2));
  }

  SECTION("degree 2 and 3")
  {
    const QuadratureFormula<2>& l2 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(2));
    const QuadratureFormula<2>& l3 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(3));

    REQUIRE(&l2 == &l3);

    REQUIRE(l3.numberOfPoints() == 4);

    REQUIRE(integrate(p0, l3) == Catch::Approx(8));
    REQUIRE(integrate(p1, l3) == Catch::Approx(4));
    REQUIRE(integrate(p2, l3) == Catch::Approx(20. / 3));
    REQUIRE(integrate(p3, l3) == Catch::Approx(-13));
    REQUIRE(integrate(p4, l3) != Catch::Approx(-1184. / 15));

    REQUIRE(get_order(p4, l3, -1184. / 15) == Catch::Approx(4));
  }

  SECTION("degree 4 and 5")
  {
    const QuadratureFormula<2>& l4 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(4));
    const QuadratureFormula<2>& l5 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(5));

    REQUIRE(&l4 == &l5);

    REQUIRE(l5.numberOfPoints() == 8);

    REQUIRE(integrate(p0, l5) == Catch::Approx(8));
    REQUIRE(integrate(p1, l5) == Catch::Approx(4));
    REQUIRE(integrate(p2, l5) == Catch::Approx(20. / 3));
    REQUIRE(integrate(p3, l5) == Catch::Approx(-13));
    REQUIRE(integrate(p4, l5) == Catch::Approx(-1184. / 15));
    REQUIRE(integrate(p5, l5) == Catch::Approx(1971. / 25));
    REQUIRE(integrate(p6, l5) != Catch::Approx(60441. / 175));

    REQUIRE(get_order(p6, l5, 60441. / 175) == Catch::Approx(6));
  }

  SECTION("degree 6 and 7")
  {
    const QuadratureFormula<2>& l6 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(6));
    const QuadratureFormula<2>& l7 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(7));

    REQUIRE(&l6 == &l7);

    REQUIRE(l7.numberOfPoints() == 12);

    REQUIRE(integrate(p0, l7) == Catch::Approx(8));
    REQUIRE(integrate(p1, l7) == Catch::Approx(4));
    REQUIRE(integrate(p2, l7) == Catch::Approx(20. / 3));
    REQUIRE(integrate(p3, l7) == Catch::Approx(-13));
    REQUIRE(integrate(p4, l7) == Catch::Approx(-1184. / 15));
    REQUIRE(integrate(p5, l7) == Catch::Approx(1971. / 25));
    REQUIRE(integrate(p6, l7) == Catch::Approx(60441. / 175));
    REQUIRE(integrate(p7, l7) == Catch::Approx(-1307119. / 525));
    REQUIRE(integrate(p8, l7) != Catch::Approx(957697. / 175));

    REQUIRE(get_order(p8, l7, 957697. / 175) == Catch::Approx(8));
  }

  SECTION("degree 8 and 9")
  {
    const QuadratureFormula<2>& l8 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(8));
    const QuadratureFormula<2>& l9 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(9));

    REQUIRE(&l8 == &l9);

    REQUIRE(l9.numberOfPoints() == 20);

    REQUIRE(integrate(p0, l9) == Catch::Approx(8));
    REQUIRE(integrate(p1, l9) == Catch::Approx(4));
    REQUIRE(integrate(p2, l9) == Catch::Approx(20. / 3));
    REQUIRE(integrate(p3, l9) == Catch::Approx(-13));
    REQUIRE(integrate(p4, l9) == Catch::Approx(-1184. / 15));
    REQUIRE(integrate(p5, l9) == Catch::Approx(1971. / 25));
    REQUIRE(integrate(p6, l9) == Catch::Approx(60441. / 175));
    REQUIRE(integrate(p7, l9) == Catch::Approx(-1307119. / 525));
    REQUIRE(integrate(p8, l9) == Catch::Approx(957697. / 175));
    REQUIRE(integrate(p9, l9) == Catch::Approx(-196981. / 5250));
    REQUIRE(integrate(p10, l9) != Catch::Approx(78447601. / 577500));

    REQUIRE(get_order(p10, l9, 78447601. / 577500) == Catch::Approx(10));
  }

  SECTION("degree 10 and 11")
  {
    const QuadratureFormula<2>& l10 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(10));
    const QuadratureFormula<2>& l11 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(11));

    REQUIRE(&l10 == &l11);

    REQUIRE(l11.numberOfPoints() == 28);

    REQUIRE(integrate(p0, l11) == Catch::Approx(8));
    REQUIRE(integrate(p1, l11) == Catch::Approx(4));
    REQUIRE(integrate(p2, l11) == Catch::Approx(20. / 3));
    REQUIRE(integrate(p3, l11) == Catch::Approx(-13));
    REQUIRE(integrate(p4, l11) == Catch::Approx(-1184. / 15));
    REQUIRE(integrate(p5, l11) == Catch::Approx(1971. / 25));
    REQUIRE(integrate(p6, l11) == Catch::Approx(60441. / 175));
    REQUIRE(integrate(p7, l11) == Catch::Approx(-1307119. / 525));
    REQUIRE(integrate(p8, l11) == Catch::Approx(957697. / 175));
    REQUIRE(integrate(p9, l11) == Catch::Approx(-196981. / 5250));
    REQUIRE(integrate(p10, l11) == Catch::Approx(78447601. / 577500));
    REQUIRE(integrate(p11, l11) == Catch::Approx(673235482069. / 86625000));
    REQUIRE(integrate(p12, l11) != Catch::Approx(-4092600398251. / 303187500));

    REQUIRE(get_order(p12, l11, -4092600398251. / 303187500) == Catch::Approx(12));
  }

  SECTION("degree 12 and 13")
  {
    const QuadratureFormula<2>& l12 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(12));
    const QuadratureFormula<2>& l13 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(13));

    REQUIRE(&l12 == &l13);

    REQUIRE(l13.numberOfPoints() == 37);

    REQUIRE(integrate(p0, l13) == Catch::Approx(8));
    REQUIRE(integrate(p1, l13) == Catch::Approx(4));
    REQUIRE(integrate(p2, l13) == Catch::Approx(20. / 3));
    REQUIRE(integrate(p3, l13) == Catch::Approx(-13));
    REQUIRE(integrate(p4, l13) == Catch::Approx(-1184. / 15));
    REQUIRE(integrate(p5, l13) == Catch::Approx(1971. / 25));
    REQUIRE(integrate(p6, l13) == Catch::Approx(60441. / 175));
    REQUIRE(integrate(p7, l13) == Catch::Approx(-1307119. / 525));
    REQUIRE(integrate(p8, l13) == Catch::Approx(957697. / 175));
    REQUIRE(integrate(p9, l13) == Catch::Approx(-196981. / 5250));
    REQUIRE(integrate(p10, l13) == Catch::Approx(78447601. / 577500));
    REQUIRE(integrate(p11, l13) == Catch::Approx(673235482069. / 86625000));
    REQUIRE(integrate(p12, l13) == Catch::Approx(-4092600398251. / 303187500));
    REQUIRE(integrate(p13, l13) == Catch::Approx(77231697272647. / 5053125000));
    REQUIRE(integrate(p14, l13) != Catch::Approx(46574962939049. / 9384375000));

    REQUIRE(get_order(p14, l13, 46574962939049. / 9384375000) == Catch::Approx(14));
  }

  SECTION("degree 14 and 15")
  {
    const QuadratureFormula<2>& l14 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(14));
    const QuadratureFormula<2>& l15 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(15));

    REQUIRE(&l14 == &l15);

    REQUIRE(l15.numberOfPoints() == 48);

    REQUIRE(integrate(p0, l15) == Catch::Approx(8));
    REQUIRE(integrate(p1, l15) == Catch::Approx(4));
    REQUIRE(integrate(p2, l15) == Catch::Approx(20. / 3));
    REQUIRE(integrate(p3, l15) == Catch::Approx(-13));
    REQUIRE(integrate(p4, l15) == Catch::Approx(-1184. / 15));
    REQUIRE(integrate(p5, l15) == Catch::Approx(1971. / 25));
    REQUIRE(integrate(p6, l15) == Catch::Approx(60441. / 175));
    REQUIRE(integrate(p7, l15) == Catch::Approx(-1307119. / 525));
    REQUIRE(integrate(p8, l15) == Catch::Approx(957697. / 175));
    REQUIRE(integrate(p9, l15) == Catch::Approx(-196981. / 5250));
    REQUIRE(integrate(p10, l15) == Catch::Approx(78447601. / 577500));
    REQUIRE(integrate(p11, l15) == Catch::Approx(673235482069. / 86625000));
    REQUIRE(integrate(p12, l15) == Catch::Approx(-4092600398251. / 303187500));
    REQUIRE(integrate(p13, l15) == Catch::Approx(77231697272647. / 5053125000));
    REQUIRE(integrate(p14, l15) == Catch::Approx(46574962939049. / 9384375000));
    REQUIRE(integrate(p15, l15) == Catch::Approx(-4818864487842259. / 1094843750000));
    REQUIRE(integrate(p16, l15) != Catch::Approx(-89885697514686141. / 23265429687500));

    REQUIRE(get_order(p16, l15, -89885697514686141. / 23265429687500) == Catch::Approx(16));
  }

  SECTION("degree 16 and 17")
  {
    const QuadratureFormula<2>& l16 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(16));
    const QuadratureFormula<2>& l17 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(17));

    REQUIRE(&l16 == &l17);

    REQUIRE(l17.numberOfPoints() == 60);

    REQUIRE(integrate(p0, l17) == Catch::Approx(8));
    REQUIRE(integrate(p1, l17) == Catch::Approx(4));
    REQUIRE(integrate(p2, l17) == Catch::Approx(20. / 3));
    REQUIRE(integrate(p3, l17) == Catch::Approx(-13));
    REQUIRE(integrate(p4, l17) == Catch::Approx(-1184. / 15));
    REQUIRE(integrate(p5, l17) == Catch::Approx(1971. / 25));
    REQUIRE(integrate(p6, l17) == Catch::Approx(60441. / 175));
    REQUIRE(integrate(p7, l17) == Catch::Approx(-1307119. / 525));
    REQUIRE(integrate(p8, l17) == Catch::Approx(957697. / 175));
    REQUIRE(integrate(p9, l17) == Catch::Approx(-196981. / 5250));
    REQUIRE(integrate(p10, l17) == Catch::Approx(78447601. / 577500));
    REQUIRE(integrate(p11, l17) == Catch::Approx(673235482069. / 86625000));
    REQUIRE(integrate(p12, l17) == Catch::Approx(-4092600398251. / 303187500));
    REQUIRE(integrate(p13, l17) == Catch::Approx(77231697272647. / 5053125000));
    REQUIRE(integrate(p14, l17) == Catch::Approx(46574962939049. / 9384375000));
    REQUIRE(integrate(p15, l17) == Catch::Approx(-4818864487842259. / 1094843750000));
    REQUIRE(integrate(p16, l17) == Catch::Approx(-89885697514686141. / 23265429687500));
    REQUIRE(integrate(p17, l17) == Catch::Approx(16706355156097391. / 12085937500000));
    REQUIRE(integrate(p18, l17) != Catch::Approx(495866230514635109957. / 397838847656250000.).epsilon(1E-8));

    REQUIRE(get_order(p18, l17, 495866230514635109957. / 397838847656250000.) == Catch::Approx(18).margin(0.001));
  }

  SECTION("degree 18 and 19")
  {
    const QuadratureFormula<2>& l18 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(18));
    const QuadratureFormula<2>& l19 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(19));

    REQUIRE(&l18 == &l19);

    REQUIRE(l19.numberOfPoints() == 72);

    REQUIRE(integrate(p0, l19) == Catch::Approx(8));
    REQUIRE(integrate(p1, l19) == Catch::Approx(4));
    REQUIRE(integrate(p2, l19) == Catch::Approx(20. / 3));
    REQUIRE(integrate(p3, l19) == Catch::Approx(-13));
    REQUIRE(integrate(p4, l19) == Catch::Approx(-1184. / 15));
    REQUIRE(integrate(p5, l19) == Catch::Approx(1971. / 25));
    REQUIRE(integrate(p6, l19) == Catch::Approx(60441. / 175));
    REQUIRE(integrate(p7, l19) == Catch::Approx(-1307119. / 525));
    REQUIRE(integrate(p8, l19) == Catch::Approx(957697. / 175));
    REQUIRE(integrate(p9, l19) == Catch::Approx(-196981. / 5250));
    REQUIRE(integrate(p10, l19) == Catch::Approx(78447601. / 577500));
    REQUIRE(integrate(p11, l19) == Catch::Approx(673235482069. / 86625000));
    REQUIRE(integrate(p12, l19) == Catch::Approx(-4092600398251. / 303187500));
    REQUIRE(integrate(p13, l19) == Catch::Approx(77231697272647. / 5053125000));
    REQUIRE(integrate(p14, l19) == Catch::Approx(46574962939049. / 9384375000));
    REQUIRE(integrate(p15, l19) == Catch::Approx(-4818864487842259. / 1094843750000));
    REQUIRE(integrate(p16, l19) == Catch::Approx(-89885697514686141. / 23265429687500));
    REQUIRE(integrate(p17, l19) == Catch::Approx(16706355156097391. / 12085937500000));
    REQUIRE(integrate(p18, l19) == Catch::Approx(495866230514635109957. / 397838847656250000.));
    REQUIRE(integrate(p19, l19) == Catch::Approx(703712939580204375319. / 132612949218750000.));
    REQUIRE(integrate(p20, l19) != Catch::Approx(-891851528496270127477. / 884086328125000000.).epsilon(1E-8));

    REQUIRE(get_order(p20, l19, -891851528496270127477. / 884086328125000000.) == Catch::Approx(20).margin(0.001));
  }

  SECTION("degree 20 and 21")
  {
    const QuadratureFormula<2>& l20 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(20));
    const QuadratureFormula<2>& l21 = QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(21));

    REQUIRE(&l20 == &l21);

    REQUIRE(l21.numberOfPoints() == 85);

    REQUIRE(integrate(p0, l21) == Catch::Approx(8));
    REQUIRE(integrate(p1, l21) == Catch::Approx(4));
    REQUIRE(integrate(p2, l21) == Catch::Approx(20. / 3));
    REQUIRE(integrate(p3, l21) == Catch::Approx(-13));
    REQUIRE(integrate(p4, l21) == Catch::Approx(-1184. / 15));
    REQUIRE(integrate(p5, l21) == Catch::Approx(1971. / 25));
    REQUIRE(integrate(p6, l21) == Catch::Approx(60441. / 175));
    REQUIRE(integrate(p7, l21) == Catch::Approx(-1307119. / 525));
    REQUIRE(integrate(p8, l21) == Catch::Approx(957697. / 175));
    REQUIRE(integrate(p9, l21) == Catch::Approx(-196981. / 5250));
    REQUIRE(integrate(p10, l21) == Catch::Approx(78447601. / 577500));
    REQUIRE(integrate(p11, l21) == Catch::Approx(673235482069. / 86625000));
    REQUIRE(integrate(p12, l21) == Catch::Approx(-4092600398251. / 303187500));
    REQUIRE(integrate(p13, l21) == Catch::Approx(77231697272647. / 5053125000));
    REQUIRE(integrate(p14, l21) == Catch::Approx(46574962939049. / 9384375000));
    REQUIRE(integrate(p15, l21) == Catch::Approx(-4818864487842259. / 1094843750000));
    REQUIRE(integrate(p16, l21) == Catch::Approx(-89885697514686141. / 23265429687500));
    REQUIRE(integrate(p17, l21) == Catch::Approx(16706355156097391. / 12085937500000));
    REQUIRE(integrate(p18, l21) == Catch::Approx(495866230514635109957. / 397838847656250000.));
    REQUIRE(integrate(p19, l21) == Catch::Approx(703712939580204375319. / 132612949218750000.));
    REQUIRE(integrate(p20, l21) == Catch::Approx(-891851528496270127477. / 884086328125000000.));
    REQUIRE(integrate(p21, l21) == Catch::Approx(31710268999580650802107. / 66306474609375000000.));
    REQUIRE(integrate(p22, l21) != Catch::Approx(-1827205780869627586647799. / 726213769531250000000.).epsilon(1E-8));

    REQUIRE(get_order(p22, l21, -1827205780869627586647799. / 726213769531250000000.) ==
            Catch::Approx(22).margin(0.01));
  }

  SECTION("max implemented degree")
  {
    REQUIRE(QuadratureManager::instance().maxSquareDegree(QuadratureType::Gauss) == SquareGaussQuadrature::max_degree);
    REQUIRE_THROWS_WITH(QuadratureManager::instance().getSquareFormula(
                          GaussQuadratureDescriptor(SquareGaussQuadrature ::max_degree + 1)),
                        "error: Gauss quadrature formulae handle degrees up to " +
                          stringify(SquareGaussQuadrature ::max_degree) + " on squares");
  }

  SECTION("Access functions")
  {
    const QuadratureFormula<2>& quadrature_formula =
      QuadratureManager::instance().getSquareFormula(GaussQuadratureDescriptor(7));

    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    REQUIRE(point_list.size() == quadrature_formula.numberOfPoints());
    REQUIRE(weight_list.size() == quadrature_formula.numberOfPoints());

    for (size_t i = 0; i < quadrature_formula.numberOfPoints(); ++i) {
      REQUIRE(&point_list[i] == &quadrature_formula.point(i));
      REQUIRE(&weight_list[i] == &quadrature_formula.weight(i));
    }
  }
}
