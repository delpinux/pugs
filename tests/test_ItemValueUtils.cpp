#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <MeshDataBaseForTests.hpp>
#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemValue.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <utils/Messenger.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("ItemValueUtils", "[mesh]")
{
  SECTION("Synchronize")
  {
    std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

    for (const auto& named_mesh : mesh_list) {
      SECTION(named_mesh.name())
      {
        auto mesh_2d_v = named_mesh.mesh();
        auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

        const Connectivity<2>& connectivity = mesh_2d->connectivity();

        WeakFaceValue<int> weak_face_value{connectivity};

        weak_face_value.fill(parallel::rank());

        FaceValue<const int> face_value{weak_face_value};

        REQUIRE(face_value.connectivity_ptr() == weak_face_value.connectivity_ptr());

        {   // before synchronization
          auto face_owner    = connectivity.faceOwner();
          auto face_is_owned = connectivity.faceIsOwned();

          for (FaceId i_face = 0; i_face < mesh_2d->numberOfFaces(); ++i_face) {
            if (face_is_owned[i_face]) {
              REQUIRE(face_owner[i_face] == face_value[i_face]);
            } else {
              REQUIRE(face_owner[i_face] != face_value[i_face]);
            }
          }
        }

        if (parallel::size() > 1) {
          REQUIRE(not isSynchronized(face_value));
        }

        synchronize(weak_face_value);

        {   // after synchronization
          auto face_owner    = connectivity.faceOwner();
          auto face_is_owned = connectivity.faceIsOwned();

          for (FaceId i_face = 0; i_face < mesh_2d->numberOfFaces(); ++i_face) {
            REQUIRE(face_owner[i_face] == face_value[i_face]);
          }
        }
        REQUIRE(isSynchronized(face_value));
      }
    }
  }

  SECTION("min")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d_v = named_mesh.mesh();
          auto mesh_1d   = mesh_1d_v->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          CellValue<int> cell_value{connectivity};
          cell_value.fill(-1);

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_1d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                cell_value[cell_id] = 10 + parallel::rank();
              }
            });

          REQUIRE(min(cell_value) == 10);
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          CellValue<int> cell_value{connectivity};
          cell_value.fill(-1);

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_2d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                cell_value[cell_id] = 10 + parallel::rank();
              }
            });

          REQUIRE(min(cell_value) == 10);
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          CellValue<int> cell_value{connectivity};
          cell_value.fill(-1);

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_3d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                cell_value[cell_id] = 10 + parallel::rank();
              }
            });

          REQUIRE(min(cell_value) == 10);
        }
      }
    }
  }

  SECTION("max")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_1d_v = named_mesh.mesh();
          auto mesh_1d   = mesh_1d_v->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          CellValue<size_t> cell_value{connectivity};
          cell_value.fill(std::numeric_limits<size_t>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_1d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                cell_value[cell_id] = parallel::rank() + 1;
              }
            });

          REQUIRE(max(cell_value) == parallel::size());
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          CellValue<size_t> cell_value{connectivity};
          cell_value.fill(std::numeric_limits<size_t>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_2d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                cell_value[cell_id] = parallel::rank() + 1;
              }
            });

          REQUIRE(max(cell_value) == parallel::size());
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          CellValue<size_t> cell_value{connectivity};
          cell_value.fill(std::numeric_limits<size_t>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_3d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                cell_value[cell_id] = parallel::rank() + 1;
              }
            });

          REQUIRE(max(cell_value) == parallel::size());
        }
      }
    }

    SECTION("max of all negative values")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name())
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          CellValue<float> cell_value{connectivity};
          cell_value.fill(std::numeric_limits<float>::max());

          auto cell_is_owned = connectivity.cellIsOwned();
          parallel_for(
            mesh_3d->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
              if (cell_is_owned[cell_id]) {
                cell_value[cell_id] = -2 * parallel::size() + parallel::rank() + 1;
              }
            });

          REQUIRE(max(cell_value) == -parallel::size());
        }
      }
    }
  }

  SECTION("sum")
  {
    SECTION("1D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all1DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name() + " for size_t data")
        {
          auto mesh_1d_v = named_mesh.mesh();
          auto mesh_1d   = mesh_1d_v->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          auto Vj = MeshDataManager::instance().getMeshData(*mesh_1d).Vj();

          CellValue<size_t> cell_value{connectivity};

          parallel_for(
            connectivity.numberOfCells(),
            PUGS_LAMBDA(CellId cell_id) { cell_value[cell_id] = std::ceil(100 * Vj[cell_id]); });

          auto cell_is_owned = connectivity.cellIsOwned();

          const size_t global_sum = [&] {
            size_t local_dum = 0;
            for (CellId cell_id = 0; cell_id < cell_is_owned.numberOfItems(); ++cell_id) {
              local_dum += cell_is_owned[cell_id] * cell_value[cell_id];
            }
            return parallel::allReduceSum(local_dum);
          }();

          REQUIRE(sum(cell_value) == global_sum);
        }

        SECTION(named_mesh.name() + " for double data")
        {
          auto mesh_1d_v = named_mesh.mesh();
          auto mesh_1d   = mesh_1d_v->get<Mesh<1>>();

          const Connectivity<1>& connectivity = mesh_1d->connectivity();

          auto Vj = MeshDataManager::instance().getMeshData(*mesh_1d).Vj();

          auto cell_is_owned = connectivity.cellIsOwned();

          size_t nb_owned_cells = 0;
          for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
            nb_owned_cells += cell_is_owned[cell_id];
          }

          Array<double> local_Vj(nb_owned_cells);
          {
            size_t index = 0;
            for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
              if (cell_is_owned[cell_id]) {
                local_Vj[index++] = Vj[cell_id];
              }
            }
          }
          Array<double> global_Vj = parallel::allGatherVariable(local_Vj);

          REQUIRE(sum(Vj) == sum(global_Vj));
        }
      }
    }

    SECTION("2D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all2DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name() + "for size_t data")
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          auto ll = MeshDataManager::instance().getMeshData(*mesh_2d).ll();

          FaceValue<size_t> face_value{connectivity};

          parallel_for(
            connectivity.numberOfFaces(),
            PUGS_LAMBDA(FaceId face_id) { face_value[face_id] = std::ceil(100 * ll[face_id]); });

          auto face_is_owned = connectivity.faceIsOwned();

          const size_t global_sum = [&] {
            size_t local_sum = 0;
            for (FaceId face_id = 0; face_id < face_is_owned.numberOfItems(); ++face_id) {
              local_sum += face_is_owned[face_id] * face_value[face_id];
            }
            return parallel::allReduceSum(local_sum);
          }();

          REQUIRE(sum(face_value) == global_sum);
        }

        SECTION(named_mesh.name() + "for double data")
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          auto Vj = MeshDataManager::instance().getMeshData(*mesh_2d).Vj();

          auto cell_is_owned = connectivity.cellIsOwned();

          size_t nb_owned_cells = 0;
          for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
            nb_owned_cells += cell_is_owned[cell_id];
          }

          Array<double> local_Vj(nb_owned_cells);
          {
            size_t index = 0;
            for (CellId cell_id = 0; cell_id < connectivity.numberOfCells(); ++cell_id) {
              if (cell_is_owned[cell_id]) {
                local_Vj[index++] = Vj[cell_id];
              }
            }
          }
          Array<double> global_Vj = parallel::allGatherVariable(local_Vj);

          REQUIRE(sum(Vj) == sum(global_Vj));
        }

        SECTION(named_mesh.name() + "for N^3 data")
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          auto ll = MeshDataManager::instance().getMeshData(*mesh_2d).ll();

          using N3 = TinyVector<3, size_t>;
          FaceValue<N3> face_value{connectivity};

          parallel_for(
            connectivity.numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
              face_value[face_id] = N3(std::ceil(100 * ll[face_id]),        //
                                       std::ceil(200 * ll[face_id] - 40),   //
                                       std::ceil(17.3 * ll[face_id] + 12.9));
            });

          auto face_is_owned = connectivity.faceIsOwned();

          const N3 global_sum = [=] {
            N3 local_sum = zero;
            for (FaceId face_id = 0; face_id < face_is_owned.numberOfItems(); ++face_id) {
              local_sum += face_is_owned[face_id] * face_value[face_id];
            }
            return parallel::allReduceSum(local_sum);
          }();

          REQUIRE(sum(face_value) == global_sum);
        }

        SECTION(named_mesh.name() + "for R2 data")
        {
          auto mesh_2d_v = named_mesh.mesh();
          auto mesh_2d   = mesh_2d_v->get<Mesh<2>>();

          const Connectivity<2>& connectivity = mesh_2d->connectivity();

          auto ll = MeshDataManager::instance().getMeshData(*mesh_2d).ll();

          using R2x3 = TinyVector<2, double>;
          FaceValue<R2x3> face_value{connectivity};

          auto face_is_owned = connectivity.faceIsOwned();

          size_t nb_owned_faces = 0;
          for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
            nb_owned_faces += face_is_owned[face_id];
          }

          parallel_for(
            connectivity.numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
              face_value[face_id] = R2x3{ll[face_id], 1. / ll[face_id]};
            });

          Array<R2x3> local_array(nb_owned_faces);
          {
            size_t index = 0;
            for (FaceId face_id = 0; face_id < connectivity.numberOfFaces(); ++face_id) {
              if (face_is_owned[face_id]) {
                local_array[index++] = face_value[face_id];
              }
            }
          }
          Array global_array = parallel::allGatherVariable(local_array);

          REQUIRE(sum(face_value) == sum(global_array));
        }
      }
    }

    SECTION("3D")
    {
      std::array mesh_list = MeshDataBaseForTests::get().all3DMeshes();

      for (const auto& named_mesh : mesh_list) {
        SECTION(named_mesh.name() + " for size_t data")
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          NodeValue<size_t> node_value{connectivity};
          node_value.fill(3);

          auto node_is_owned = connectivity.nodeIsOwned();

          const size_t global_number_of_nodes = [&] {
            size_t number_of_nodes = 0;
            for (NodeId node_id = 0; node_id < node_is_owned.numberOfItems(); ++node_id) {
              number_of_nodes += node_is_owned[node_id];
            }
            return parallel::allReduceSum(number_of_nodes);
          }();

          REQUIRE(sum(node_value) == 3 * global_number_of_nodes);
        }

        SECTION(named_mesh.name() + " for N^3x2 data")
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          using N3x2 = TinyMatrix<3, 2, size_t>;

          NodeValue<N3x2> node_value{connectivity};
          const N3x2 data(3, 6, 1, 4, 5, 7);

          node_value.fill(data);

          auto node_is_owned = connectivity.nodeIsOwned();

          const size_t global_number_of_nodes = [&] {
            size_t number_of_nodes = 0;
            for (NodeId node_id = 0; node_id < node_is_owned.numberOfItems(); ++node_id) {
              number_of_nodes += node_is_owned[node_id];
            }
            return parallel::allReduceSum(number_of_nodes);
          }();

          REQUIRE(sum(node_value) == global_number_of_nodes * data);
        }

        SECTION(named_mesh.name() + "for R^2x3 data")
        {
          auto mesh_3d_v = named_mesh.mesh();
          auto mesh_3d   = mesh_3d_v->get<Mesh<3>>();

          const Connectivity<3>& connectivity = mesh_3d->connectivity();

          auto xr = mesh_3d->xr();

          using R2x3 = TinyMatrix<2, 3, double>;
          NodeValue<R2x3> node_value{connectivity};

          auto node_is_owned = connectivity.nodeIsOwned();

          size_t nb_owned_nodes = 0;
          for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
            nb_owned_nodes += node_is_owned[node_id];
          }

          parallel_for(
            connectivity.numberOfNodes(), PUGS_LAMBDA(NodeId node_id) {
              node_value[node_id] = R2x3{xr[node_id][0],
                                         l2Norm(xr[node_id]),
                                         3.178 * xr[node_id][0] - 3 * xr[node_id][2],
                                         xr[node_id][1],
                                         xr[node_id][2] + xr[node_id][0],
                                         xr[node_id][0] * xr[node_id][1] * xr[node_id][2]};
            });

          Array<R2x3> local_array(nb_owned_nodes);
          {
            size_t index = 0;
            for (NodeId node_id = 0; node_id < connectivity.numberOfNodes(); ++node_id) {
              if (node_is_owned[node_id]) {
                local_array[index++] = node_value[node_id];
              }
            }
          }
          Array global_array = parallel::allGatherVariable(local_array);

          REQUIRE(sum(node_value) == sum(global_array));
        }
      }
    }
  }
}
