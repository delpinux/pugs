#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/ast/ASTBuilder.hpp>
#include <language/ast/ASTExecutionStack.hpp>
#include <language/ast/ASTModulesImporter.hpp>
#include <language/ast/ASTNodeDataTypeBuilder.hpp>
#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>
#include <language/ast/ASTNodeExpressionBuilder.hpp>
#include <language/ast/ASTNodeTypeCleaner.hpp>
#include <language/ast/ASTSymbolTableBuilder.hpp>
#include <language/node_processor/NameProcessor.hpp>
#include <utils/Demangle.hpp>

#include <pegtl/string_input.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("NameProcessor", "[language]")
{
  rang::setControlMode(rang::control::Off);

  SECTION("basic use")
  {
    std::string_view data = R"(
let n:N, n=3;
let m:N, m = n;
n = 2;
)";

    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
    auto ast = ASTBuilder::build(input);

    ASTExecutionStack::create();

    ASTModulesImporter{*ast};
    ASTNodeTypeCleaner<language::import_instruction>{*ast};

    ASTSymbolTableBuilder{*ast};
    ASTNodeDataTypeBuilder{*ast};

    ASTNodeDeclarationToAffectationConverter{*ast};
    ASTNodeTypeCleaner<language::var_declaration>{*ast};

    ASTNodeExpressionBuilder{*ast};
    ExecutionPolicy exec_policy;
    ast->execute(exec_policy);

    auto symbol_table = ast->m_symbol_table;

    TAO_PEGTL_NAMESPACE::position use_position{100, 1, 1, "fixture"};
    auto symbol_n = symbol_table->find("n", use_position).first->attributes();
    auto value_n  = std::get<long unsigned int>(symbol_n.value());

    ASTExecutionStack::destroy();

    REQUIRE(value_n == 2);

    auto symbol_m = symbol_table->find("m", use_position).first->attributes();
    auto value_m  = std::get<long unsigned int>(symbol_m.value());

    REQUIRE(value_m == 3);

    REQUIRE(ast->children[0]->children[0]->m_node_processor->typeIdName() == demangle<NameProcessor>());
    REQUIRE(ast->children[1]->children[0]->m_node_processor->typeIdName() == demangle<NameProcessor>());
    REQUIRE(ast->children[1]->children[1]->m_node_processor->typeIdName() == demangle<NameProcessor>());
    REQUIRE(ast->children[2]->children[0]->m_node_processor->typeIdName() == demangle<NameProcessor>());
    ast->m_symbol_table->clearValues();
  }

  SECTION("error")
  {
    std::string_view data = R"(
let n:N;
if (false) {
  n = 3;
}
let m:N, m = n;
)";

    TAO_PEGTL_NAMESPACE::string_input input{data, "test.pgs"};
    auto ast = ASTBuilder::build(input);

    ASTExecutionStack::create();

    ASTModulesImporter{*ast};
    ASTNodeTypeCleaner<language::import_instruction>{*ast};

    ASTSymbolTableBuilder{*ast};
    ASTNodeDataTypeBuilder{*ast};

    ASTNodeDeclarationToAffectationConverter{*ast};
    ASTNodeTypeCleaner<language::var_declaration>{*ast};

    ASTNodeExpressionBuilder{*ast};
    ExecutionPolicy exec_policy;
    REQUIRE_THROWS_WITH(ast->execute(exec_policy), "trying to use uninitialized symbol 'n'");

    ASTExecutionStack::destroy();
    ast->m_symbol_table->clearValues();
  }
}
