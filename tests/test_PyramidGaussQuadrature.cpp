#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/PyramidGaussQuadrature.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/TetrahedronTransformation.hpp>
#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

// clazy:excludeall=non-pod-global-static

TEST_CASE("PyramidGaussQuadrature", "[analysis]")
{
  auto integrate = [](auto f, auto quadrature_formula) {
    auto point_list  = quadrature_formula.pointList();
    auto weight_list = quadrature_formula.weightList();

    auto value = weight_list[0] * f(point_list[0]);
    for (size_t i = 1; i < weight_list.size(); ++i) {
      value += weight_list[i] * f(point_list[i]);
    }

    return value;
  };

  auto p0 = [](const TinyVector<3>&) { return 4; };
  auto p1 = [](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return 2 * x + 3 * y + z - 1;
  };
  auto p2 = [&p1](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p1(X) * (2.5 * x - 3 * y + z + 3);
  };
  auto p3 = [&p2](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p2(X) * (3 * x + 2 * y - 3 * z - 1);
  };
  auto p4 = [&p3](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p3(X) * (2 * x - 0.5 * y - 1.3 * z + 1);
  };
  auto p5 = [&p4](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p4(X) * (-0.1 * x + 1.3 * y - 3 * z + 1);
  };
  auto p6 = [&p5](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p5(X) * 7875. / 143443 * (2 * x - y + 4 * z + 1);
  };
  auto p7 = [&p6](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p6(X) * (0.7 * x - 2.7 * y + 1.3 * z - 2);
  };
  auto p8 = [&p7](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p7(X) * (0.3 * x + 1.2 * y - 0.7 * z + 0.2);
  };
  auto p9 = [&p8](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p8(X) * (-0.2 * x + 1.1 * y - 0.5 * z + 0.6);
  };
  auto p10 = [&p9](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p9(X) * (0.7 * x - 0.6 * y - 0.7 * z - 0.2);
  };
  auto p11 = [&p10](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p10(X) * (-1.3 * x + 0.6 * y - 1.3 * z + 0.7);
  };
  auto p12 = [&p11](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p11(X) * (0.3 * x - 0.7 * y + 0.3 * z + 0.7);
  };
  auto p13 = [&p12](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p12(X) * (0.9 * x + 0.2 * y - 0.4 * z + 0.5);
  };
  auto p14 = [&p13](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p13(X) * (0.6 * x - 1.2 * y + 0.7 * z - 0.4);
  };
  auto p15 = [&p14](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p14(X) * (-0.2 * x - 0.7 * y + 0.9 * z + 0.8);
  };
  auto p16 = [&p15](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p15(X) * (0.7 * x + 0.2 * y - 0.6 * z + 0.4);
  };
  auto p17 = [&p16](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p16(X) * (-0.1 * x + 0.8 * y + 0.3 * z - 0.2);
  };
  auto p18 = [&p17](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p17(X) * (0.7 * x - 0.2 * y - 0.3 * z + 0.8);
  };
  auto p19 = [&p18](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p18(X) * (-0.7 * x + 1.2 * y + 1.3 * z + 0.8);
  };
  auto p20 = [&p19](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p19(X) * (0.7 * x - 1.2 * y + 0.3 * z - 0.6);
  };
  auto p21 = [&p20](const TinyVector<3>& X) {
    const double x = X[0];
    const double y = X[1];
    const double z = X[2];
    return p20(X) * (0.7 * x - 1.2 * y + 0.3 * z - 0.6);
  };

  SECTION("degree 1")
  {
    const QuadratureFormula<3>& l1 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(1));

    REQUIRE(l1.numberOfPoints() == 1);

    REQUIRE(integrate(p0, l1) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l1) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l1) != Catch::Approx(-64. / 15));
  }

  SECTION("degree 2")
  {
    const QuadratureFormula<3>& l2 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(2));

    REQUIRE(l2.numberOfPoints() == 5);

    REQUIRE(integrate(p0, l2) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l2) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l2) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l2) != Catch::Approx(83. / 5));
  }

  SECTION("degree 3")
  {
    const QuadratureFormula<3>& l3 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(3));

    REQUIRE(l3.numberOfPoints() == 6);

    REQUIRE(integrate(p0, l3) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l3) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l3) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l3) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l3) != Catch::Approx(26809. / 3150));
  }

  SECTION("degree 4")
  {
    const QuadratureFormula<3>& l4 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(4));

    REQUIRE(l4.numberOfPoints() == 10);

    REQUIRE(integrate(p0, l4) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l4) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l4) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l4) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l4) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l4) != Catch::Approx(42881. / 63000));
  }

  SECTION("degree 5")
  {
    const QuadratureFormula<3>& l5 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(5));

    REQUIRE(l5.numberOfPoints() == 15);

    REQUIRE(integrate(p0, l5) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l5) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l5) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l5) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l5) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l5) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l5) != Catch::Approx(-59509. / 1290987));
  }

  SECTION("degree 6")
  {
    const QuadratureFormula<3>& l6 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(6));

    REQUIRE(l6.numberOfPoints() == 23);

    REQUIRE(integrate(p0, l6) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l6) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l6) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l6) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l6) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l6) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l6) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l6) != Catch::Approx(-79258447. / 64549350));
  }

  SECTION("degree 7")
  {
    const QuadratureFormula<3>& l7 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(7));

    REQUIRE(l7.numberOfPoints() == 31);

    REQUIRE(integrate(p0, l7) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l7) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l7) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l7) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l7) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l7) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l7) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l7) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l7) != Catch::Approx(-64936890181. / 56803428000));
  }

  SECTION("degree 8")
  {
    const QuadratureFormula<3>& l8 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(8));

    REQUIRE(l8.numberOfPoints() == 47);

    REQUIRE(integrate(p0, l8) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l8) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l8) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l8) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l8) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l8) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l8) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l8) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l8) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l8) != Catch::Approx(-46104457917. / 31557460000));
  }

  SECTION("degree 9")
  {
    const QuadratureFormula<3>& l9 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(9));

    REQUIRE(l9.numberOfPoints() == 62);

    REQUIRE(integrate(p0, l9) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l9) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l9) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l9) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l9) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l9) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l9) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l9) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l9) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l9) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l9) != Catch::Approx(14564160020837. / 73844456400000));
  }

  SECTION("degree 10")
  {
    const QuadratureFormula<3>& l10 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(10));

    REQUIRE(l10.numberOfPoints() == 80);

    REQUIRE(integrate(p0, l10) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l10) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l10) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l10) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l10) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l10) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l10) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l10) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l10) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l10) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l10) == Catch::Approx(14564160020837. / 73844456400000));
    REQUIRE(integrate(p11, l10) != Catch::Approx(70717900459291. / 1723037316000000));
  }

  SECTION("degree 11")
  {
    const QuadratureFormula<3>& l11 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(11));

    REQUIRE(l11.numberOfPoints() == 103);

    REQUIRE(integrate(p0, l11) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l11) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l11) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l11) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l11) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l11) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l11) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l11) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l11) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l11) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l11) == Catch::Approx(14564160020837. / 73844456400000));
    REQUIRE(integrate(p11, l11) == Catch::Approx(70717900459291. / 1723037316000000));
    REQUIRE(integrate(p12, l11) != Catch::Approx(4088535221940569. / 129227798700000000));
  }

  SECTION("degree 12")
  {
    const QuadratureFormula<3>& l12 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(12));

    REQUIRE(l12.numberOfPoints() == 127);

    REQUIRE(integrate(p0, l12) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l12) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l12) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l12) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l12) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l12) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l12) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l12) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l12) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l12) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l12) == Catch::Approx(14564160020837. / 73844456400000));
    REQUIRE(integrate(p11, l12) == Catch::Approx(70717900459291. / 1723037316000000));
    REQUIRE(integrate(p12, l12) == Catch::Approx(4088535221940569. / 129227798700000000));
    REQUIRE(integrate(p13, l12) != Catch::Approx(4202215015498883. / 129227798700000000));
  }

  SECTION("degree 13")
  {
    const QuadratureFormula<3>& l13 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(13));

    REQUIRE(l13.numberOfPoints() == 152);

    REQUIRE(integrate(p0, l13) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l13) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l13) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l13) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l13) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l13) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l13) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l13) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l13) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l13) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l13) == Catch::Approx(14564160020837. / 73844456400000));
    REQUIRE(integrate(p11, l13) == Catch::Approx(70717900459291. / 1723037316000000));
    REQUIRE(integrate(p12, l13) == Catch::Approx(4088535221940569. / 129227798700000000));
    REQUIRE(integrate(p13, l13) == Catch::Approx(4202215015498883. / 129227798700000000));
    REQUIRE(integrate(p14, l13) != Catch::Approx(-13139133580740403. / 4992892222500000000.));
  }

  SECTION("degree 14")
  {
    const QuadratureFormula<3>& l14 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(14));

    REQUIRE(l14.numberOfPoints() == 184);

    REQUIRE(integrate(p0, l14) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l14) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l14) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l14) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l14) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l14) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l14) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l14) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l14) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l14) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l14) == Catch::Approx(14564160020837. / 73844456400000));
    REQUIRE(integrate(p11, l14) == Catch::Approx(70717900459291. / 1723037316000000));
    REQUIRE(integrate(p12, l14) == Catch::Approx(4088535221940569. / 129227798700000000));
    REQUIRE(integrate(p13, l14) == Catch::Approx(4202215015498883. / 129227798700000000));
    REQUIRE(integrate(p14, l14) == Catch::Approx(-13139133580740403. / 4992892222500000000.));
    REQUIRE(integrate(p15, l14) != Catch::Approx(50695835504084747233. / 3295308866850000000000.));
  }

  SECTION("degree 15")
  {
    const QuadratureFormula<3>& l15 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(15));

    REQUIRE(l15.numberOfPoints() == 234);

    REQUIRE(integrate(p0, l15) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l15) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l15) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l15) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l15) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l15) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l15) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l15) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l15) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l15) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l15) == Catch::Approx(14564160020837. / 73844456400000));
    REQUIRE(integrate(p11, l15) == Catch::Approx(70717900459291. / 1723037316000000));
    REQUIRE(integrate(p12, l15) == Catch::Approx(4088535221940569. / 129227798700000000));
    REQUIRE(integrate(p13, l15) == Catch::Approx(4202215015498883. / 129227798700000000));
    REQUIRE(integrate(p14, l15) == Catch::Approx(-13139133580740403. / 4992892222500000000.));
    REQUIRE(integrate(p15, l15) == Catch::Approx(50695835504084747233. / 3295308866850000000000.));
    REQUIRE(integrate(p16, l15) != Catch::Approx(7438848232461834482681. / 834811579602000000000000.));
  }

  SECTION("degree 16")
  {
    const QuadratureFormula<3>& l16 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(16));

    REQUIRE(l16.numberOfPoints() == 285);

    REQUIRE(integrate(p0, l16) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l16) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l16) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l16) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l16) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l16) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l16) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l16) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l16) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l16) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l16) == Catch::Approx(14564160020837. / 73844456400000));
    REQUIRE(integrate(p11, l16) == Catch::Approx(70717900459291. / 1723037316000000));
    REQUIRE(integrate(p12, l16) == Catch::Approx(4088535221940569. / 129227798700000000));
    REQUIRE(integrate(p13, l16) == Catch::Approx(4202215015498883. / 129227798700000000));
    REQUIRE(integrate(p14, l16) == Catch::Approx(-13139133580740403. / 4992892222500000000.));
    REQUIRE(integrate(p15, l16) == Catch::Approx(50695835504084747233. / 3295308866850000000000.));
    REQUIRE(integrate(p16, l16) == Catch::Approx(7438848232461834482681. / 834811579602000000000000.));
    REQUIRE(integrate(p17, l16) != Catch::Approx(-49370451351776632471. / 4384514598750000000000.));
  }

  SECTION("degree 17")
  {
    const QuadratureFormula<3>& l17 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(17));

    REQUIRE(l17.numberOfPoints() == 319);

    REQUIRE(integrate(p0, l17) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l17) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l17) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l17) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l17) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l17) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l17) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l17) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l17) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l17) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l17) == Catch::Approx(14564160020837. / 73844456400000));
    REQUIRE(integrate(p11, l17) == Catch::Approx(70717900459291. / 1723037316000000));
    REQUIRE(integrate(p12, l17) == Catch::Approx(4088535221940569. / 129227798700000000));
    REQUIRE(integrate(p13, l17) == Catch::Approx(4202215015498883. / 129227798700000000));
    REQUIRE(integrate(p14, l17) == Catch::Approx(-13139133580740403. / 4992892222500000000.));
    REQUIRE(integrate(p15, l17) == Catch::Approx(50695835504084747233. / 3295308866850000000000.));
    REQUIRE(integrate(p16, l17) == Catch::Approx(7438848232461834482681. / 834811579602000000000000.));
    REQUIRE(integrate(p17, l17) == Catch::Approx(-49370451351776632471. / 4384514598750000000000.));
    REQUIRE(integrate(p18, l17) != Catch::Approx(-3041981344499113218848083. / 194789368573800000000000000.));
  }

  SECTION("degree 18")
  {
    const QuadratureFormula<3>& l18 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(18));

    REQUIRE(l18.numberOfPoints() == 357);

    REQUIRE(integrate(p0, l18) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l18) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l18) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l18) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l18) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l18) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l18) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l18) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l18) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l18) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l18) == Catch::Approx(14564160020837. / 73844456400000));
    REQUIRE(integrate(p11, l18) == Catch::Approx(70717900459291. / 1723037316000000));
    REQUIRE(integrate(p12, l18) == Catch::Approx(4088535221940569. / 129227798700000000));
    REQUIRE(integrate(p13, l18) == Catch::Approx(4202215015498883. / 129227798700000000));
    REQUIRE(integrate(p14, l18) == Catch::Approx(-13139133580740403. / 4992892222500000000.));
    REQUIRE(integrate(p15, l18) == Catch::Approx(50695835504084747233. / 3295308866850000000000.));
    REQUIRE(integrate(p16, l18) == Catch::Approx(7438848232461834482681. / 834811579602000000000000.));
    REQUIRE(integrate(p17, l18) == Catch::Approx(-49370451351776632471. / 4384514598750000000000.));
    REQUIRE(integrate(p18, l18) == Catch::Approx(-3041981344499113218848083. / 194789368573800000000000000.));
    REQUIRE(integrate(p19, l18) != Catch::Approx(6741839335620301740899793. / 892784605963250000000000000.));
  }

  SECTION("degree 19")
  {
    const QuadratureFormula<3>& l19 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(19));

    REQUIRE(l19.numberOfPoints() == 418);

    REQUIRE(integrate(p0, l19) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l19) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l19) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l19) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l19) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l19) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l19) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l19) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l19) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l19) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l19) == Catch::Approx(14564160020837. / 73844456400000));
    REQUIRE(integrate(p11, l19) == Catch::Approx(70717900459291. / 1723037316000000));
    REQUIRE(integrate(p12, l19) == Catch::Approx(4088535221940569. / 129227798700000000));
    REQUIRE(integrate(p13, l19) == Catch::Approx(4202215015498883. / 129227798700000000));
    REQUIRE(integrate(p14, l19) == Catch::Approx(-13139133580740403. / 4992892222500000000.));
    REQUIRE(integrate(p15, l19) == Catch::Approx(50695835504084747233. / 3295308866850000000000.));
    REQUIRE(integrate(p16, l19) == Catch::Approx(7438848232461834482681. / 834811579602000000000000.));
    REQUIRE(integrate(p17, l19) == Catch::Approx(-49370451351776632471. / 4384514598750000000000.));
    REQUIRE(integrate(p18, l19) == Catch::Approx(-3041981344499113218848083. / 194789368573800000000000000.));
    REQUIRE(integrate(p19, l19) == Catch::Approx(6741839335620301740899793. / 892784605963250000000000000.));
    REQUIRE(integrate(p20, l19) != Catch::Approx(50574805739660969727328017511. / 4928171024917140000000000000000.));
  }

  SECTION("degree 20")
  {
    const QuadratureFormula<3>& l20 = QuadratureManager::instance().getPyramidFormula(GaussQuadratureDescriptor(20));

    REQUIRE(l20.numberOfPoints() == 489);

    REQUIRE(integrate(p0, l20) == Catch::Approx(16. / 3));
    REQUIRE(integrate(p1, l20) == Catch::Approx(-1));
    REQUIRE(integrate(p2, l20) == Catch::Approx(-64. / 15));
    REQUIRE(integrate(p3, l20) == Catch::Approx(83. / 5));
    REQUIRE(integrate(p4, l20) == Catch::Approx(26809. / 3150));
    REQUIRE(integrate(p5, l20) == Catch::Approx(42881. / 63000));
    REQUIRE(integrate(p6, l20) == Catch::Approx(-59509. / 1290987));
    REQUIRE(integrate(p7, l20) == Catch::Approx(-79258447. / 64549350));
    REQUIRE(integrate(p8, l20) == Catch::Approx(-64936890181. / 56803428000));
    REQUIRE(integrate(p9, l20) == Catch::Approx(-46104457917. / 31557460000));
    REQUIRE(integrate(p10, l20) == Catch::Approx(14564160020837. / 73844456400000));
    REQUIRE(integrate(p11, l20) == Catch::Approx(70717900459291. / 1723037316000000));
    REQUIRE(integrate(p12, l20) == Catch::Approx(4088535221940569. / 129227798700000000));
    REQUIRE(integrate(p13, l20) == Catch::Approx(4202215015498883. / 129227798700000000));
    REQUIRE(integrate(p14, l20) == Catch::Approx(-13139133580740403. / 4992892222500000000.));
    REQUIRE(integrate(p15, l20) == Catch::Approx(50695835504084747233. / 3295308866850000000000.));
    REQUIRE(integrate(p16, l20) == Catch::Approx(7438848232461834482681. / 834811579602000000000000.));
    REQUIRE(integrate(p17, l20) == Catch::Approx(-49370451351776632471. / 4384514598750000000000.));
    REQUIRE(integrate(p18, l20) == Catch::Approx(-3041981344499113218848083. / 194789368573800000000000000.));
    REQUIRE(integrate(p19, l20) == Catch::Approx(6741839335620301740899793. / 892784605963250000000000000.));
    REQUIRE(integrate(p20, l20) == Catch::Approx(50574805739660969727328017511. / 4928171024917140000000000000000.));
    REQUIRE(integrate(p21, l20) != Catch::Approx(796248143552124247176376796357. / 110883848060635650000000000000000.));
  }

  SECTION("max implemented degree")
  {
    REQUIRE(QuadratureManager::instance().maxPyramidDegree(QuadratureType::Gauss) ==
            PyramidGaussQuadrature::max_degree);
    REQUIRE_THROWS_WITH(QuadratureManager::instance().getPyramidFormula(
                          GaussQuadratureDescriptor(PyramidGaussQuadrature ::max_degree + 1)),
                        "error: Gauss quadrature formulae handle degrees up to " +
                          stringify(PyramidGaussQuadrature ::max_degree) + " on pyramids");
  }
}
