#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/Messenger.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <language/utils/OFStream.hpp>
#include <utils/checkpointing/ReadOStream.hpp>
#include <utils/checkpointing/WriteOStream.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_OStream", "[utils/checkpointing]")
{
  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});
    HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

    SECTION("OStream")
    {
      HighFive::Group symbol_table_group = file.createGroup("symbol_table");
      HighFive::Group useless_group;

      auto p_ofstream = std::make_shared<OFStream>("output_name");
      checkpointing::writeOStream("ofstream", EmbeddedData{std::make_shared<DataHandler<const OStream>>(p_ofstream)},
                                  file, useless_group, symbol_table_group);

      auto p_ostream = std::make_shared<OStream>(std::cout);
      REQUIRE_THROWS_WITH(checkpointing::writeOStream("ostream",
                                                      EmbeddedData{
                                                        std::make_shared<DataHandler<const OStream>>(p_ostream)},
                                                      file, useless_group, symbol_table_group),
                          "not implemented yet: std::ostream checkpoint");

      file.flush();

      EmbeddedData read_ofstream = checkpointing::readOStream("ofstream", symbol_table_group);

      auto get_value = [](const EmbeddedData& embedded_data) -> const OStream& {
        return *dynamic_cast<const DataHandler<const OStream>&>(embedded_data.get()).data_ptr();
      };

      REQUIRE_NOTHROW(get_value(read_ofstream));

      REQUIRE_NOTHROW(dynamic_cast<const OFStream&>(get_value(read_ofstream)));

      REQUIRE(get_value(read_ofstream).type() == OStream::Type::std_ofstream);
      REQUIRE(dynamic_cast<const OFStream&>(get_value(read_ofstream)).filename() == "output_name");
    }
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }
}
