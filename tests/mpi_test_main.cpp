#include <catch2/catch_all.hpp>

#include <Kokkos_Core.hpp>

#include <analysis/QuadratureManager.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <mesh/DualConnectivityManager.hpp>
#include <mesh/DualMeshManager.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/SynchronizerManager.hpp>
#include <utils/GlobalVariableManager.hpp>
#include <utils/Messenger.hpp>
#include <utils/PETScWrapper.hpp>
#include <utils/RandomEngine.hpp>
#include <utils/Stringify.hpp>
#include <utils/checkpointing/ResumingManager.hpp>
#include <utils/pugs_config.hpp>

#include <MeshDataBaseForTests.hpp>

#include <cstdlib>
#include <filesystem>
#include <thread>

int
main(int argc, char* argv[])
{
  parallel::Messenger::create(argc, argv);

  const int nb_max_threads = std::max(std::thread::hardware_concurrency(), 1u);
  int nb_threads           = std::min(std::max(nb_max_threads / parallel::Messenger::getInstance().size(), 1ul), 8ul);

  Catch::Session session;

  auto cli = session.cli() | Catch::Clara::Opt(nb_threads, "number of threads")["--threads"](
                               "number of threads (default: max logical threads)");

  session.cli(cli);

  int result = session.applyCommandLine(argc, argv);

  {
    Kokkos::InitializationSettings args;
    args.set_num_threads(nb_threads);
    args.set_device_id(-1);
    args.set_disable_warnings(true);

    Kokkos::initialize(args);
  }

  PETScWrapper::initialize(argc, argv);

  const std::string output_base_name{"mpi_test_rank_"};

  std::filesystem::path parallel_output = std::filesystem::path{PUGS_BINARY_DIR}.append("tests");

  std::filesystem::path gcov_prefix = [&]() -> std::filesystem::path {
    std::string template_temp_dir = std::filesystem::temp_directory_path() / "pugs_gcov_XXXXXX";
    if constexpr (std::string_view{PUGS_BUILD_TYPE} == "Coverage") {
      return std::filesystem::path{mkdtemp(&template_temp_dir[0])};
    } else {
      return "";
    }
  }();

  // disable file locking to avoid mess in tests
  setenv("HDF5_USE_FILE_LOCKING", "FALSE", 1);

  if (result == 0) {
    const auto& config = session.config();
    if (config.listReporters() or config.listTags() or config.listTests()) {
      if (parallel::rank() == 0) {
        session.run();
      }
    } else {
      if (parallel::rank() != 0) {
        // Disable outputs for ranks != 0
        setenv("GCOV_PREFIX", gcov_prefix.string().c_str(), 1);
        parallel_output /= output_base_name + stringify(parallel::rank());

        Catch::ConfigData data{session.configData()};
        data.defaultOutputFilename = parallel_output.string();
        session.useConfigData(data);
      }

      std::cout << "Using " << nb_threads << " threads per process [" << nb_threads << "x"
                << parallel::Messenger::getInstance().size() << "]\n";

      if (parallel::rank() == 0) {
        if (parallel::size() > 1) {
          std::cout << rang::fgB::green << "Other rank outputs are stored in corresponding files" << rang::style::reset
                    << '\n';

          for (size_t i_rank = 1; i_rank < parallel::size(); ++i_rank) {
            std::filesystem::path other_rank_output = std::filesystem::path{PUGS_BINARY_DIR}.append("tests");
            other_rank_output /= output_base_name + stringify(i_rank);
            std::cout << " - " << rang::fg::green << other_rank_output.parent_path().string()
                      << other_rank_output.preferred_separator << rang::style::reset << rang::fgB::green
                      << other_rank_output.filename().string() << rang::style::reset << '\n';
          }
        }
      }

      // Disable outputs from tested classes to the standard output
      std::cout.setstate(std::ios::badbit);

      ResumingManager::create();

      SynchronizerManager::create();
      RandomEngine::create();
      QuadratureManager::create();
      MeshDataManager::create();
      DualConnectivityManager::create();
      DualMeshManager::create();
      GlobalVariableManager::create();

      MeshDataBaseForTests::create();

      OperatorRepository::create();

      result = session.run();

      OperatorRepository::destroy();

      MeshDataBaseForTests::destroy();

      GlobalVariableManager::destroy();
      DualMeshManager::destroy();
      DualConnectivityManager::destroy();
      MeshDataManager::destroy();
      QuadratureManager::destroy();
      RandomEngine::destroy();
      SynchronizerManager::destroy();

      ResumingManager::destroy();
    }
  }

  PETScWrapper::finalize();

  Kokkos::finalize();
  parallel::Messenger::destroy();

  return result;
}
