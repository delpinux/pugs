#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/HighFivePugsUtils.hpp>
#include <utils/Messenger.hpp>
#include <utils/checkpointing/ResumingManager.hpp>

#include <filesystem>

// clazy:excludeall=non-pod-global-static

TEST_CASE("checkpointing_ResumingManager", "[utils/checkpointing]")
{
  // Need to destroy the instance created in the main for tests
  REQUIRE_NOTHROW(ResumingManager::destroy());

#ifndef NDEBUG
  REQUIRE_THROWS_WITH(ResumingManager::destroy(), "Resuming manager was not created");
  REQUIRE_THROWS_WITH(ResumingManager::getInstance(), "instance was not created");
#endif   // NDEBUG

  REQUIRE_NOTHROW(ResumingManager::create());

#ifndef NDEBUG
  REQUIRE_THROWS_WITH(ResumingManager::create(), "Resuming manager was already created");
#endif   // NDEBUG

#ifdef PUGS_HAS_HDF5
  std::string tmp_dirname;
  {
    {
      if (parallel::rank() == 0) {
        tmp_dirname = [&]() -> std::string {
          std::string temp_filename = std::filesystem::temp_directory_path() / "pugs_checkpointing_XXXXXX";
          return std::string{mkdtemp(&temp_filename[0])};
        }();
      }
      parallel::broadcast(tmp_dirname, 0);
    }
    std::filesystem::path path = tmp_dirname;
    const std::string filename = path / "checkpoint.h5";

    {
      HighFive::FileAccessProps fapl;
      fapl.add(HighFive::MPIOFileAccess{MPI_COMM_WORLD, MPI_INFO_NULL});
      fapl.add(HighFive::MPIOCollectiveMetadata{});
      HighFive::File file = HighFive::File(filename, HighFive::File::Truncate, fapl);

      {
        HighFive::Group cp = file.createGroup("/resuming_checkpoint");
        cp.createAttribute("id", 13ul);
      }
    }

    parallel::barrier();
    REQUIRE_NOTHROW(ResumingManager::getInstance().setFilename(filename));
    REQUIRE(ResumingManager::getInstance().filename() == filename);

    REQUIRE(ResumingManager::getInstance().checkpointId() == 13);
  }

  parallel::barrier();
  if (parallel::rank() == 0) {
    std::filesystem::remove_all(std::filesystem::path{tmp_dirname});
  }

#else   // PUGS_HAS_HDF5

  ResumingManager::getInstance().setFilename("useless");
  REQUIRE(ResumingManager::getInstance().filename() == "useless");
  REQUIRE(ResumingManager::getInstance().checkpointId() == 0);

#endif   // PUGS_HAS_HDF5

  ResumingManager::getInstance().currentASTLevel()  = 3;
  ResumingManager::getInstance().checkpointNumber() = 7;
  ResumingManager::getInstance().setIsResuming(false);

  REQUIRE(ResumingManager::getInstance().currentASTLevel() == 3);
  REQUIRE(ResumingManager::getInstance().checkpointNumber() == 7);
  REQUIRE(not ResumingManager::getInstance().isResuming());

  ResumingManager::getInstance().currentASTLevel()  = 1;
  ResumingManager::getInstance().checkpointNumber() = 5;
  ResumingManager::getInstance().setIsResuming(true);

  REQUIRE(ResumingManager::getInstance().currentASTLevel() == 1);
  REQUIRE(ResumingManager::getInstance().checkpointNumber() == 5);
  REQUIRE(ResumingManager::getInstance().isResuming());

  REQUIRE_NOTHROW(ResumingManager::destroy());

  // Recreate ResumingManager for remaining tests
  REQUIRE_NOTHROW(ResumingManager::create());
}
