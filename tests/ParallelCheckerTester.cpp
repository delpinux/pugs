#include <ParallelCheckerTester.hpp>

bool
ParallelCheckerTester::isCreated() const
{
  return ParallelChecker::m_instance != nullptr;
}

std::string
ParallelCheckerTester::getFilename() const
{
  return ParallelChecker::instance().m_filename;
}

ParallelChecker::Mode
ParallelCheckerTester::getMode() const
{
  return ParallelChecker::instance().m_mode;
}

size_t
ParallelCheckerTester::getTag() const
{
  return ParallelChecker::instance().m_tag;
}

void
ParallelCheckerTester::setFilename(const std::string& filename) const
{
  ParallelChecker::instance().m_filename = filename;
}

void
ParallelCheckerTester::setMode(ParallelChecker::Mode mode) const
{
  ParallelChecker::instance().m_mode = mode;
}

void
ParallelCheckerTester::setTag(size_t tag) const
{
  ParallelChecker::instance().m_tag = tag;
}

void
ParallelCheckerTester::setTagWithCheck(size_t tag) const
{
  ParallelChecker::instance().setTag(tag);
}
