#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/NaNHelper.hpp>

#include <sstream>

// clazy:excludeall=non-pod-global-static

TEST_CASE("NaNHelper", "[utils]")
{
  SECTION("signaling NaN")
  {
    std::ostringstream ost_nan_helper;
    ost_nan_helper << NaNHelper(std::numeric_limits<double>::signaling_NaN());

    REQUIRE(ost_nan_helper.str() == "nan");
  }

  SECTION("valid double")
  {
    std::ostringstream ost_nan_helper;
    ost_nan_helper << NaNHelper(double{3.2});

    std::ostringstream ost;
    ost << double{3.2};
    REQUIRE(ost_nan_helper.str() == ost.str());
  }

  SECTION("non arithmetic type")
  {
    std::string s = "foo";
    std::ostringstream ost_nan_helper;
    ost_nan_helper << NaNHelper(s);

    REQUIRE(ost_nan_helper.str() == s);
  }
}
