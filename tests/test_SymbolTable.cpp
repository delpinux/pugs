#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/SymbolTable.hpp>
#include <language/utils/TypeDescriptor.hpp>

#include <pegtl/internal/inputerator.hpp>

#include <sstream>

// clazy:excludeall=non-pod-global-static

TEST_CASE("SymbolTable", "[language]")
{
  SECTION("Simple Symbol Table")
  {
    std::shared_ptr root_st = std::make_shared<SymbolTable>();

    using namespace TAO_PEGTL_NAMESPACE;
    position begin_position{internal::inputerator{"fixture"}, "fixture"};
    begin_position.byte = 2;

    auto [i_symbol_a, created_a] = root_st->add("a", begin_position);
    REQUIRE(created_a);

    REQUIRE(i_symbol_a->attributes().position().byte == 2);

    // Check that one cannot build another "a" in this table
    REQUIRE(not root_st->add("a", begin_position).second);

    position use_position{internal::inputerator{"fixture"}, "fixture"};
    use_position.byte = 3;   // after declarative position

    auto [i_search_a, found_a] = root_st->find("a", use_position);
    REQUIRE(found_a);
    REQUIRE(i_search_a == i_symbol_a);

    SECTION("Output uninitialized")
    {
      std::stringstream st_output;
      st_output << '\n' << *root_st;

      std::stringstream expected_output;
      expected_output << '\n'
                      << "-- Symbol table state -- parent : " << static_cast<SymbolTable*>(nullptr) << '\n'
                      << " a: undefined:--\n"
                      << "------------------------\n";

      REQUIRE(st_output.str() == expected_output.str());
    }

    SECTION("Attributes")
    {
      // undefined data
      auto& attributes_a = i_search_a->attributes();
      REQUIRE(attributes_a.dataType() == ASTNodeDataType::undefined_t);
      REQUIRE(not attributes_a.isInitialized());

      REQUIRE(std::holds_alternative<std::monostate>(attributes_a.value()));

      {
        std::stringstream value_output;
        value_output << attributes_a;
        REQUIRE(value_output.str() == "undefined:--");
      }

      // defining data
      attributes_a.setIsInitialized();
      REQUIRE(attributes_a.isInitialized());

      attributes_a.setDataType(ASTNodeDataType::build<ASTNodeDataType::double_t>());
      REQUIRE(attributes_a.dataType() == ASTNodeDataType::build<ASTNodeDataType::double_t>());

      attributes_a.value() = 2.3;

      REQUIRE(std::holds_alternative<double>(attributes_a.value()));

      const auto const_attribute_a = attributes_a;
      REQUIRE(std::get<double>(const_attribute_a.value()) == 2.3);

      {
        std::stringstream value_output;
        value_output << attributes_a;
        REQUIRE(value_output.str() == "R:2.3");
      }

      SECTION("Output initialized")
      {
        std::stringstream st_output;
        st_output << '\n' << *root_st;

        std::stringstream expected_output;
        expected_output << '\n'
                        << "-- Symbol table state -- parent : " << static_cast<SymbolTable*>(nullptr) << '\n'
                        << " a: R:2.3\n"
                        << "------------------------\n";

        REQUIRE(st_output.str() == expected_output.str());
      }
    }
  }

  SECTION("Hierarchy Symbol Table")
  {
    std::shared_ptr root_st = std::make_shared<SymbolTable>();

    using namespace TAO_PEGTL_NAMESPACE;
    position begin_position{internal::inputerator{"fixture"}, "fixture"};
    position end_declaration{internal::inputerator{"fixture"}, "fixture"};

    auto [i_root_symbol_a, created_root_a] = root_st->add("a", begin_position);
    REQUIRE(created_root_a);

    std::shared_ptr nested_st = std::make_shared<SymbolTable>(root_st);

    position use_position{internal::inputerator{"fixture"}, "fixture"};
    auto [i_search_a, found_a] = nested_st->find("a", use_position);
    REQUIRE(found_a);
    // symbol "a" is the one defined in root_st
    REQUIRE(i_root_symbol_a == i_search_a);

    auto [i_nested_symbol_a, created_nested_a] = nested_st->add("a", begin_position);
    REQUIRE(created_nested_a);

    auto [i_search_nested_a, found_nested_a] = nested_st->find("a", use_position);

    REQUIRE(found_nested_a);
    // found the symbol created in nested symbol table
    REQUIRE(&(i_search_nested_a->attributes()) != &(i_root_symbol_a->attributes()));
    REQUIRE(&(i_search_nested_a->attributes()) == &(i_nested_symbol_a->attributes()));

    auto [i_search_b, found_b] = nested_st->find("b", use_position);
    REQUIRE(not found_b);   // "b" is not defined in any symbol table
  }

  SECTION("Output of function_id")
  {
    std::shared_ptr root_st = std::make_shared<SymbolTable>();

    using namespace TAO_PEGTL_NAMESPACE;
    position begin_position{internal::inputerator{"fixture"}, "fixture"};
    begin_position.byte = 2;

    auto [i_symbol_a, created_a] = root_st->add("a", begin_position);
    REQUIRE(i_symbol_a->attributes().position().byte == 2);

    position use_position{internal::inputerator{"fixture"}, "fixture"};
    use_position.byte = 3;   // after declarative position

    auto [i_search_a, found_a] = root_st->find("a", use_position);
    REQUIRE(found_a);
    REQUIRE(i_search_a == i_symbol_a);

    auto& attributes_a = i_search_a->attributes();
    REQUIRE(attributes_a.dataType() == ASTNodeDataType::undefined_t);
    REQUIRE(not attributes_a.isInitialized());

    REQUIRE(std::holds_alternative<std::monostate>(attributes_a.value()));

    {
      std::stringstream value_output;
      value_output << attributes_a;
      REQUIRE(value_output.str() == "undefined:--");
    }

    // defining function data
    attributes_a.setIsInitialized();
    REQUIRE(attributes_a.isInitialized());

    attributes_a.setDataType(ASTNodeDataType::build<ASTNodeDataType::function_t>());
    REQUIRE(attributes_a.dataType() == ASTNodeDataType::build<ASTNodeDataType::function_t>());

    attributes_a.value() = static_cast<uint64_t>(2);

    REQUIRE(std::holds_alternative<uint64_t>(attributes_a.value()));

    {
      std::stringstream value_output;
      value_output << attributes_a;
      REQUIRE(value_output.str() == "function_id:2");
    }

    {
      const SymbolTable::Symbol symbol{i_symbol_a->name(), i_symbol_a->attributes()};
      std::stringstream value_output;
      value_output << symbol.attributes();
      REQUIRE(value_output.str() == "function_id:2");
    }

    {
      const SymbolTable::Symbol symbol_decl{i_symbol_a->name(), i_symbol_a->attributes()};
      SymbolTable::Attributes attributes_b = attributes_a;
      SymbolTable::Symbol symbol("foo", attributes_b);

      symbol = symbol_decl;

      std::stringstream value_output;
      value_output << symbol.attributes();
      REQUIRE(value_output.str() == "function_id:2");
    }

    {
      SymbolTable::Attributes attributes_b = attributes_a;
      SymbolTable::Symbol symbol("foo", attributes_b);

      symbol = SymbolTable::Symbol{i_symbol_a->name(), i_symbol_a->attributes()};

      std::stringstream value_output;
      value_output << symbol.attributes();
      REQUIRE(value_output.str() == "function_id:2");
    }
  }

  SECTION("FunctionTable")
  {
    std::shared_ptr root_st = std::make_shared<SymbolTable>();

    auto& function_table = root_st->functionTable();
    REQUIRE(function_table.size() == 0);

    const auto& const_function_table = static_cast<const SymbolTable&>(*root_st).functionTable();
    REQUIRE(const_function_table.size() == 0);

    function_table.add(FunctionDescriptor{});

    REQUIRE(function_table.size() == 1);
    REQUIRE(const_function_table.size() == 1);
  }

  SECTION("BuiltinFunctionEmbedderTable")
  {
    std::shared_ptr root_st = std::make_shared<SymbolTable>();

    auto& builtin_function_table = root_st->builtinFunctionEmbedderTable();
    REQUIRE(builtin_function_table.size() == 0);

    const auto& const_builtin_function_table = static_cast<const SymbolTable&>(*root_st).builtinFunctionEmbedderTable();
    REQUIRE(const_builtin_function_table.size() == 0);

    builtin_function_table.add(
      std::make_shared<BuiltinFunctionEmbedder<double(double)>>([](double) -> double { return 0; }));

    REQUIRE(builtin_function_table.size() == 1);
    REQUIRE(const_builtin_function_table.size() == 1);
  }

  SECTION("TypeEmbedderTable")
  {
    std::shared_ptr root_st = std::make_shared<SymbolTable>();

    auto& type_table = root_st->typeEmbedderTable();
    REQUIRE(type_table.size() == 0);

    const auto& const_type_table = static_cast<const SymbolTable&>(*root_st).typeEmbedderTable();
    REQUIRE(const_type_table.size() == 0);

    type_table.add(std::make_shared<TypeDescriptor>("a_type"));

    REQUIRE(type_table.size() == 1);
    REQUIRE(const_type_table.size() == 1);
  }
}
