#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>

#include <utils/CommunicatorManager.hpp>

// clazy:excludeall=non-pod-global-static

inline void
resetCommunicationManagerForTests()
{
  CommunicatorManager::s_split_color.reset();
}

TEST_CASE("CommunicatorManager", "[utils]")
{
  resetCommunicationManagerForTests();

  REQUIRE(not CommunicatorManager::hasSplitColor());
#ifndef NDEBUG
  REQUIRE_THROWS_WITH(CommunicatorManager::splitColor(), "split color has not been defined");
#endif   // NDEBUG

  REQUIRE_NOTHROW(CommunicatorManager::setSplitColor(2));
  REQUIRE(CommunicatorManager::splitColor() == 2);

#ifndef NDEBUG
  REQUIRE_THROWS_WITH(CommunicatorManager::setSplitColor(2), "split color has already been defined");
#endif   // NDEBUG
}
