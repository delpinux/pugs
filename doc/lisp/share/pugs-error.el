;;; pugs-error -- simple major mode for pgs files used to document errors
;;; Commentary:
;;; inherits pugs mode
;;; Code:

(define-derived-mode pugs-error-mode pugs-mode "pugs-error"
  "pugs mode is a major mode for editing pugs files"
  ;; define pugs keywords
  )

(provide 'pugs-error-mode)

;;; (provide 'pugs-error-mode)
;;; pugs-error.el ends here
