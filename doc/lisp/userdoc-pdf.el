;;; userdoc-pdf --- builds userdoc (pdf)
;;; Commentary:
;;;
;;; Code:

(load-library "${HOME}/build-doc-config.el")

(with-current-buffer
    (find-file-noselect (substitute-in-file-name "${PUGS_SOURCE_DIR}/doc/userdoc.org"))
  (org-latex-export-to-pdf))

;;; Local Variables:
;;; byte-compile-warnings: (not free-vars)
;;; End: (provide 'userdoc-pdf)
;;; userdoc-pdf.el ends here
