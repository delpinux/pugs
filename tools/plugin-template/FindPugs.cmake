# Finds for the pugs installation directory

find_path(PUGS_PREFIX_PATH include/utils/pugs_version.hpp
  HINTS
  $ENV{PUGS_INSTALL_DIR}
  /usr/local/pugs
  NO_DEFAULT_PATH
)

find_package_handle_standard_args(Pugs REQUIRED_VARS PUGS_PREFIX_PATH )
