#! /usr/bin/env bash

arguments=()

while [[ "$#" -ne 0 ]]; do
  case "$1" in
  -l)
    arguments+=("$1")

    shift
    lexer=$1

    if [[ "$lexer" == "Pugs" ]]; then
      arguments+=("${PUGS_SOURCE_DIR}/tools/pgs-pygments.py:PugsLexer")
      arguments+=("-x")
    else
      arguments+=("${lexer}")
    fi

    ;;
  *)
    arguments+=("$1")
    ;;
  esac
  shift
done

pygmentize -Ostyle=colorful "${arguments[@]}"
