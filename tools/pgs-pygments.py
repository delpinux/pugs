from pygments.lexer import *
from pygments.token import *

class PugsLexer(RegexLexer):
    name = 'Pugs'
    aliases = ['pgs']
    filenames = ['*.pgs']

    tokens = {
        'root': [
            (r'([Ll]?)(")', bygroups(String.Affix, String.Double), 'string'),
            (r'([Ll]?)(\')(\\[^\']+)(\')', bygroups(String.Affix, String.Char, String.Escape, String.Char)),
            (r'\s+', Whitespace),
            (r'/\*', Comment.Multiline, 'comment'),
            (r'//.*?$', Comment.Singleline),
            (r'/', Text),
            (words(('B', 'N', 'Z', 'R', 'R^1', 'R^2','R^3', 'R^1x1', 'R^2x2', 'R^3x3', 'string'), suffix=r'\b'), Keyword.Type),
            (words(('let', 'import', 'do', 'while', 'for', 'if', 'else' 'break'), suffix=r'\b'), Keyword.Reserved),
            (words(('and', 'or', 'xor', 'not', 'true', 'false'), suffix=r'\b'), Keyword.Reserved),
            (words(('cout', 'cerr', 'clog'), suffix=r'\b'), Name.Variable),
            (r'[,~!%&*+=|?:<>/-]', Operator),
            (r'[+-]?\d+(\.\d*)?[Ee][+-]?\d+', Number.Float),
            (r'[+-]?(\d+\.\d*)|(\d*\.\d+)([Ee][+-]?\d+)?', Number.Float),
            (r'\d+[LlUu]*', Number.Integer),
            (r'[\(\)\[\];.]', Punctuation),
            (r'[{}]', Punctuation),
            (r'\w+', Name),
        ],
        'comment': [
            (r'[^*/]+', Comment.Multiline),
            (r'/\*', Comment.Multiline, '#push'),
            (r'\*/', Comment.Multiline, '#pop'),
            (r'[*/]', Comment.Multiline)
        ],
        'string': [
            (r'"', String, '#pop'),
            (r'\\([\\abfnrtv"\']|x[a-fA-F0-9]{2,4}|[0-7]{1,3})', String.Escape),
            (r'\\\n', String.Escape),  # line continuation
            (r'[^\\"]+', String),  # all other characters
            (r'\\', String),  # stray backslash
        ],
    }
