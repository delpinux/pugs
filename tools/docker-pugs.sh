#! /bin/sh

DOCKER=$(command -v docker 2>/dev/null)

if [ "${DOCKER}" = "" ]
then
    echo Could not find Docker on your system. Check your installation.
    exit 1
fi

echo "Using docker: ${DOCKER}"

USABLE_DOCKER=$(${DOCKER} info >/dev/null 2>&1 && echo yes)

if [ "${USABLE_DOCKER}" != "yes" ]
then
   echo "################### ABORTING ######################"
   echo "Cannot use Docker!"
   echo " - check that Docker server is running"
   echo " - check that you have permissions to use it"
   echo "   (usually user must belong to the 'docker' group)"
   echo "###################################################"

   exit 1
fi

USER=$(id -un)
USER_ID=$(id -u)
USER_GID=$(id -g)
DOCKER_HOSTNAME="$(hostname)-docker"

DOCKERFILE_DIR=/tmp/${USER}/pugs.docker
mkdir -p ${DOCKERFILE_DIR}

DOCKERFILE="${DOCKERFILE_DIR}/Dockerfile"

cat > ${DOCKERFILE} <<EOF
FROM ubuntu:jammy

ARG DEBIAN_FRONTEND=noninteractive
ENV USER="${USER}" USER_ID="${USER_ID}" USER_GID="${USER_GID}" HOSTNAME="${DOCKER_HOSTNAME}"
ENV TZ=Europe/Paris
RUN echo "${DOCKER_HOSTNAME}" > /etc/hostname
RUN groupadd --gid "${USER_GID}" "${USER}"
RUN useradd --uid "${USER_ID}" --gid "${USER_GID}" --create-home --shell /bin/bash "${USER}"

RUN apt-get update && apt-get -y upgrade && apt-get -y remove g++ gcc && apt-get -y install apt-utils gnupg gnupg2 gnupg1 wget cmake git make lcov bc pkg-config sudo doxygen

RUN apt-get -y install clang-11 clang-format-11

RUN apt-get -y install libparmetis-dev petsc-dev slepc-dev
RUN apt-get clean

RUN rm /usr/bin/cc
RUN echo "${USER} ALL=(ALL:ALL) NOPASSWD:ALL" > "/etc/sudoers.d/${USER}"

RUN ln -s /usr/bin/clang-format-11 /usr/bin/clang-format

ENV CC="clang-11" CXX="clang++-11"

EOF

if [ -e "${DOCKERFILE}" ]
then
    echo "Successfully built: ${DOCKERFILE}"
else
    echo "Aborting: unable to build ${DOCKERFILE}"
    exit 1
fi

IMAGE_NAME="pugs-docker-${USER}"
${DOCKER} build -t "${IMAGE_NAME}:latest" ${DOCKERFILE_DIR}

${DOCKER} run --volume=${HOME}:${HOME} -w $(pwd) --user ${USER_ID}:${USER_GID} -ti --entrypoint /bin/bash --hostname="${DOCKER_HOSTNAME}" "${IMAGE_NAME}"
