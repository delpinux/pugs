# Contributing to `pugs`

----
## Branches

`develop` and `master` branches are protected. This means that one
cannot `push` on them, so **before** to do any code change one has to
create a working branch. The following conventions are greatly
encouraged:

* `feature/my_outstanding_feature`
* `issue/issue-number` (if no number is related to the issue please
  consider [opening an
  issue](https://gitlab.delpinux.fr/code/pugs/issues) and assign it to
  yourself)

----
## Tests and coverage

### Running tests
Tests are built automatically and are run typing
> `make; make test`

**all tests should be running correctly before any merge request**

### Unit tests
Unit tests are defined in the `tests` directory. New unit tests should
be written or updated when needed.

### Coverage
Preferably, one should install [gcovr](http://www.gcovr.com) and build
`pugs` specifying the `Coverage` build type > `cmake
-DCMAKE_BUILD_TYPE=Coverage [...]`

However coverage is computed at each `push` by the `gitlab-ci`.

----
## Up to date build environment using [Docker](http://www.docker.com)
This is the easiest way to keep your environment up to date in order
to build `pugs`.

Running the [docker-pugs.sh](tools/docker-pugs.sh) script creates the
image and runs it in interactive mode. The image will runs the user's
permissions and his home directory is mounted.

**keep in mind that the produced executable will only run inside docker**

----
## Coding

### `packages` directory
Do not make **any** change in the `packages` directory. This directory
contains *third party libraries* that are are mandatory to minimal
builds or `pugs`.

For any changes in this directory (bug report, suggestion of new
library, library upgrades or downgrades) **open an issue**.

### Warnings
Try to avoid publishing sources that generates compilation warnings.

Also, avoid the use of `#warning` directives and prefer opening an
issue.

`C++` `[[deprecated]]` directive should also be avoid as much as
possible.

### Comments

**Do not** comment deprecated code. It is `git`'s job to keep track of
old versions.

Avoid commenting functions bodies:

* comments are likely to be deprecates
* if the code is not clear by itself comments will not help

`Doxygen` is to be used to comment functions in the header.

### Code formatting

`clang-format` is used in `pugs`, so install it and run
> `make clang-format` before any commit

A better solution is to configure your favored editor to perform
formatting automatically. For instance `emacs` users should copy-past
the following code to their `.emacs.el` init file.

```lisp
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Clang-format
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; clang-format can be triggered using C-c C-f
;; Create clang-format file using google style
;; clang-format -style=google -dump-config > .clang-format
(use-package clang-format
  :ensure t
  :bind (("C-c C-f" . clang-format-region))
  )
;; sets clang-format to be used instead of emacs indentation
(defun clang-format-c-mode-common-hook ()
  (fset 'c-indent-region 'clang-format-region)
  (define-key c-mode-base-map (kbd "<tab>") 'clang-format-region)
  )
(add-hook 'c-mode-common-hook 'clang-format-c-mode-common-hook)
;; sets clang-format to be run on save for the whole file
(defun clang-format-buffer-on-save ()
  "Add auto-save hook for clang-format-buffer-smart."
  (add-hook 'before-save-hook 'clang-format-buffer nil t))
(add-hook 'c-mode-common-hook 'clang-format-buffer-on-save)
```
