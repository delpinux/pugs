#include <algebra/LinearSolver.hpp>
#include <utils/pugs_config.hpp>

#include <algebra/BiCGStab.hpp>
#include <algebra/CG.hpp>
#include <algebra/Eigen3Utils.hpp>
#include <algebra/PETScUtils.hpp>

#ifdef PUGS_HAS_EIGEN3
#include <eigen3/unsupported/Eigen/IterativeSolvers>
#endif   // PUGS_HAS_EIGEN3

#ifdef PUGS_HAS_PETSC
#include <petsc.h>
#endif   // PUGS_HAS_PETSC

struct LinearSolver::Internals
{
  static bool
  hasLibrary(const LSLibrary library)
  {
    switch (library) {
    case LSLibrary::builtin: {
      return true;
    }
    case LSLibrary::eigen3: {
#ifdef PUGS_HAS_EIGEN3
      return true;
#else
      return false;
#endif
    }
    case LSLibrary::petsc: {
#ifdef PUGS_HAS_PETSC
      return true;
#else
      return false;
#endif
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("Linear system library (" + ::name(library) + ") was not set!");
    }
      // LCOV_EXCL_STOP
    }
  }

  static void
  checkHasLibrary(const LSLibrary library)
  {
    if (not hasLibrary(library)) {
      // LCOV_EXCL_START
      throw NormalError(::name(library) + " is not linked to pugs. Cannot use it!");
      // LCOV_EXCL_STOP
    }
  }

  static void
  checkBuiltinMethod(const LSMethod method)
  {
    switch (method) {
    case LSMethod::cg:
    case LSMethod::bicgstab: {
      break;
    }
    default: {
      throw NormalError(name(method) + " is not a builtin linear solver!");
    }
    }
  }

  static void
  checkEigen3Method(const LSMethod method)
  {
    switch (method) {
    case LSMethod::cg:
    case LSMethod::bicgstab:
    case LSMethod::gmres:
    case LSMethod::lu:
    case LSMethod::cholesky: {
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw NormalError(name(method) + " is not an Eigen3 linear solver!");
    }
      // LCOV_EXCL_STOP
    }
  }

  static void
  checkPETScMethod(const LSMethod method)
  {
    switch (method) {
    case LSMethod::cg:
    case LSMethod::bicgstab:
    case LSMethod::bicgstab2:
    case LSMethod::gmres:
    case LSMethod::lu:
    case LSMethod::cholesky: {
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw NormalError(name(method) + " is not a builtin linear solver!");
    }
      // LCOV_EXCL_STOP
    }
  }

  static void
  checkBuiltinPrecond(const LSPrecond precond)
  {
    switch (precond) {
    case LSPrecond::none: {
      break;
    }
    default: {
      throw NormalError(name(precond) + " is not a builtin preconditioner!");
    }
    }
  }

  static void
  checkEigen3Precond(const LSPrecond precond)
  {
    switch (precond) {
    case LSPrecond::none:
    case LSPrecond::diagonal:
    case LSPrecond::incomplete_cholesky:
    case LSPrecond::incomplete_LU: {
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw NormalError(name(precond) + " is not an Eigen3 preconditioner!");
    }
      // LCOV_EXCL_STOP
    }
  }

  static void
  checkPETScPrecond(const LSPrecond precond)
  {
    switch (precond) {
    case LSPrecond::none:
    case LSPrecond::amg:
    case LSPrecond::diagonal:
    case LSPrecond::incomplete_cholesky:
    case LSPrecond::incomplete_LU: {
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw NormalError(name(precond) + " is not a PETSc preconditioner!");
    }
      // LCOV_EXCL_STOP
    }
  }

  static void
  checkOptions(const LinearSolverOptions& options)
  {
    switch (options.library()) {
    case LSLibrary::builtin: {
      checkBuiltinMethod(options.method());
      checkBuiltinPrecond(options.precond());
      break;
    }
    case LSLibrary::eigen3: {
      checkEigen3Method(options.method());
      checkEigen3Precond(options.precond());
      break;
    }
    case LSLibrary::petsc: {
      checkPETScMethod(options.method());
      checkPETScPrecond(options.precond());
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("undefined options compatibility for this library (" + ::name(options.library()) + ")!");
    }
      // LCOV_EXCL_STOP
    }
  }

  template <typename MatrixType, typename SolutionVectorType, typename RHSVectorType>
  static void
  _builtinSolveLocalSystem(const MatrixType& A,
                           SolutionVectorType& x,
                           const RHSVectorType& b,
                           const LinearSolverOptions& options)
  {
    if (options.precond() != LSPrecond::none) {
      // LCOV_EXCL_START
      throw UnexpectedError("builtin linear solver do not allow any preconditioner!");
      // LCOV_EXCL_STOP
    }
    switch (options.method()) {
    case LSMethod::cg: {
      CG{A, x, b, options.epsilon(), options.maximumIteration(), options.verbose()};
      break;
    }
    case LSMethod::bicgstab: {
      BiCGStab{A, x, b, options.epsilon(), options.maximumIteration(), options.verbose()};
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw NotImplementedError("undefined builtin method: " + name(options.method()));
    }
      // LCOV_EXCL_STOP
    }
  }

#ifdef PUGS_HAS_EIGEN3
  template <typename PreconditionerType, typename Eigen3MatrixEmbedderType>
  static void
  _preconditionedEigen3SolveLocalSystem(const Eigen3MatrixEmbedderType& eigen3_A,
                                        VectorWrapper<double>& x,
                                        const VectorWrapper<const double>& b,
                                        const LinearSolverOptions& options)
  {
    Eigen::Map<Eigen::VectorX<double>> eigen3_x{x.ptr(), static_cast<int>(x.size())};
    Eigen::Map<const Eigen::VectorX<double>> eigen3_b{b.ptr(), static_cast<int>(b.size())};

    using Eigen3MatrixType = typename Eigen3MatrixEmbedderType::Eigen3MatrixType;

    switch (options.method()) {
    case LSMethod::bicgstab: {
      Eigen::BiCGSTAB<Eigen3MatrixType, PreconditionerType> solver;
      solver.compute(eigen3_A.matrix());
      solver.setMaxIterations(options.maximumIteration());
      solver.setTolerance(options.epsilon());
      eigen3_x = solver.solve(eigen3_b);
      break;
    }
    case LSMethod::cg: {
      Eigen::ConjugateGradient<Eigen3MatrixType, Eigen::Lower | Eigen::Upper, PreconditionerType> solver;
      solver.compute(eigen3_A.matrix());
      solver.setMaxIterations(options.maximumIteration());
      solver.setTolerance(options.epsilon());
      eigen3_x = solver.solve(eigen3_b);
      break;
    }
    case LSMethod::gmres: {
      Eigen::GMRES<Eigen3MatrixType, PreconditionerType> solver;
      solver.compute(eigen3_A.matrix());
      solver.setMaxIterations(options.maximumIteration());
      solver.setTolerance(options.epsilon());
      eigen3_x = solver.solve(eigen3_b);
      break;
    }
    case LSMethod::lu: {
      if constexpr (std::is_same_v<Eigen3MatrixEmbedderType, Eigen3DenseMatrixEmbedder>) {
        Eigen::FullPivLU<Eigen3MatrixType> solver;
        solver.compute(eigen3_A.matrix());
        eigen3_x = solver.solve(eigen3_b);
      } else {
        Eigen::SparseLU<Eigen3MatrixType> solver;
        solver.compute(eigen3_A.matrix());
        eigen3_x = solver.solve(eigen3_b);
      }
      break;
    }
    case LSMethod::cholesky: {
      if constexpr (std::is_same_v<Eigen3MatrixEmbedderType, Eigen3DenseMatrixEmbedder>) {
        Eigen::LLT<Eigen3MatrixType> solver;
        solver.compute(eigen3_A.matrix());
        eigen3_x = solver.solve(eigen3_b);
      } else {
        Eigen::SimplicialLLT<Eigen3MatrixType> solver;
        solver.compute(eigen3_A.matrix());
        eigen3_x = solver.solve(eigen3_b);
      }
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("unexpected method: " + name(options.method()));
    }
      // LCOV_EXCL_STOP
    }
  }

  template <typename Eigen3MatrixEmbedderType>
  static void
  _eigen3SolveLocalSystem(const Eigen3MatrixEmbedderType& eigen3_A,
                          VectorWrapper<double>& x,
                          const VectorWrapper<const double>& b,
                          const LinearSolverOptions& options)
  {
    switch (options.precond()) {
    case LSPrecond::none: {
      _preconditionedEigen3SolveLocalSystem<Eigen::IdentityPreconditioner, Eigen3MatrixEmbedderType>(eigen3_A, x, b,
                                                                                                     options);
      break;
    }
    case LSPrecond::diagonal: {
      _preconditionedEigen3SolveLocalSystem<Eigen::DiagonalPreconditioner<double>, Eigen3MatrixEmbedderType>(eigen3_A,
                                                                                                             x, b,
                                                                                                             options);
      break;
    }
    case LSPrecond::incomplete_cholesky: {
      if constexpr (std::is_same_v<Eigen3SparseMatrixEmbedder, Eigen3MatrixEmbedderType>) {
        _preconditionedEigen3SolveLocalSystem<Eigen::IncompleteCholesky<double>, Eigen3MatrixEmbedderType>(eigen3_A, x,
                                                                                                           b, options);
      } else {
        throw NormalError("incomplete cholesky is not available for dense matrices in Eigen3");
      }
      break;
    }
    case LSPrecond::incomplete_LU: {
      if constexpr (std::is_same_v<Eigen3SparseMatrixEmbedder, Eigen3MatrixEmbedderType>) {
        _preconditionedEigen3SolveLocalSystem<Eigen::IncompleteLUT<double>, Eigen3MatrixEmbedderType>(eigen3_A, x, b,
                                                                                                      options);
      } else {
        throw NormalError("incomplete LU is not available for dense matrices in Eigen3");
      }
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("unexpected preconditioner: " + name(options.precond()));
    }
      // LCOV_EXCL_STOP
    }
  }
#else   // PUGS_HAS_EIGEN3

  // LCOV_EXCL_START
  template <typename MatrixType, typename SolutionVectorType, typename RHSVectorType>
  static void
  _eigen3SolveLocalSystem(const MatrixType&, SolutionVectorType&, const RHSVectorType&, const LinearSolverOptions&)
  {
    checkHasLibrary(LSLibrary::eigen3);
    throw UnexpectedError("unexpected situation should not reach this point!");
  }
  // LCOV_EXCL_STOP

#endif   // PUGS_HAS_EIGEN3

#ifdef PUGS_HAS_PETSC
  static int
  petscMonitor(KSP, int i, double residu, void*)
  {
    std::cout << "  - iteration: " << std::setw(6) << i << " residu: " << std::scientific << residu << '\n';
    return 0;
  }

  template <typename MatrixType>
  static void
  _petscSolveLocalSystem(const MatrixType& A,
                         VectorWrapper<double>& x,
                         const VectorWrapper<const double>& b,
                         const LinearSolverOptions& options)
  {
    Vec petscB;
    VecCreateMPIWithArray(PETSC_COMM_SELF, 1, b.size(), b.size(), b.ptr(), &petscB);
    Vec petscX;
    VecCreateMPIWithArray(PETSC_COMM_SELF, 1, x.size(), x.size(), x.ptr(), &petscX);

    PETScAijMatrixEmbedder petscA(A);

    KSP ksp;
    KSPCreate(PETSC_COMM_SELF, &ksp);
    KSPSetTolerances(ksp, options.epsilon(), 1E-100, 1E5, options.maximumIteration());

    KSPSetOperators(ksp, petscA, petscA);

    PC pc;
    KSPGetPC(ksp, &pc);

    bool direct_solver = false;

    switch (options.method()) {
    case LSMethod::bicgstab: {
      KSPSetType(ksp, KSPBCGS);
      break;
    }
    case LSMethod::bicgstab2: {
      KSPSetType(ksp, KSPBCGSL);
      KSPBCGSLSetEll(ksp, 2);
      break;
    }
    case LSMethod::cg: {
      KSPSetType(ksp, KSPCG);
      break;
    }
    case LSMethod::gmres: {
      KSPSetType(ksp, KSPGMRES);

      break;
    }
    case LSMethod::lu: {
      KSPSetType(ksp, KSPPREONLY);
      PCSetType(pc, PCLU);
      PCFactorSetShiftType(pc, MAT_SHIFT_NONZERO);
      direct_solver = true;
      break;
    }
    case LSMethod::cholesky: {
      KSPSetType(ksp, KSPPREONLY);
      PCSetType(pc, PCCHOLESKY);
      PCFactorSetShiftType(pc, MAT_SHIFT_NONZERO);
      direct_solver = true;
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("unexpected method: " + name(options.method()));
    }
      // LCOV_EXCL_STOP
    }

    if (not direct_solver) {
      switch (options.precond()) {
      case LSPrecond::amg: {
        PCSetType(pc, PCGAMG);
        break;
      }
      case LSPrecond::diagonal: {
        PCSetType(pc, PCJACOBI);
        break;
      }
      case LSPrecond::incomplete_LU: {
        PCSetType(pc, PCILU);
        break;
      }
      case LSPrecond::incomplete_cholesky: {
        PCSetType(pc, PCICC);
        break;
      }
      case LSPrecond::none: {
        PCSetType(pc, PCNONE);
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("unexpected preconditioner: " + name(options.precond()));
      }
        // LCOV_EXCL_STOP
      }
    }
    if (options.verbose()) {
      KSPMonitorSet(ksp, petscMonitor, 0, 0);
    }

    KSPSolve(ksp, petscB, petscX);

    VecDestroy(&petscB);
    VecDestroy(&petscX);
    KSPDestroy(&ksp);
  }

#else   // PUGS_HAS_PETSC

  // LCOV_EXCL_START
  template <typename MatrixType, typename SolutionVectorType, typename RHSVectorType>
  static void
  _petscSolveLocalSystem(const MatrixType&, SolutionVectorType&, const RHSVectorType&, const LinearSolverOptions&)
  {
    checkHasLibrary(LSLibrary::petsc);
    throw UnexpectedError("unexpected situation should not reach this point!");
  }
  // LCOV_EXCL_STOP

#endif   // PUGS_HAS_PETSC
};

bool
LinearSolver::hasLibrary(LSLibrary library) const
{
  return Internals::hasLibrary(library);
}

void
LinearSolver::checkOptions(const LinearSolverOptions& options) const
{
  Internals::checkOptions(options);
}

void
LinearSolver::_builtinSolveLocalSystem(const CRSMatrix<double, int>& A,
                                       Vector<double>& x,
                                       const Vector<const double>& b)
{
  Internals::_builtinSolveLocalSystem(A, x, b, m_options);
}

void
LinearSolver::_builtinSolveLocalSystem(const SmallMatrix<double>& A,
                                       SmallVector<double>& x,
                                       const SmallVector<const double>& b)
{
  Internals::_builtinSolveLocalSystem(A, x, b, m_options);
}

void
LinearSolver::_eigen3SolveLocalSystem(const Eigen3SparseMatrixEmbedder& A,
                                      VectorWrapper<double> x,
                                      const VectorWrapper<const double>& b)
{
  Internals::_eigen3SolveLocalSystem(A, x, b, m_options);
}

void
LinearSolver::_eigen3SolveLocalSystem(const DenseMatrixWrapper<double>& A,
                                      VectorWrapper<double> x,
                                      const VectorWrapper<const double>& b)
{
  Eigen3DenseMatrixEmbedder A_wrapper{A};
  Internals::_eigen3SolveLocalSystem(A_wrapper, x, b, m_options);
}

void
LinearSolver::_petscSolveLocalSystem(const CRSMatrix<double, int>& A,
                                     VectorWrapper<double> x,
                                     const VectorWrapper<const double>& b)
{
  Internals::_petscSolveLocalSystem(A, x, b, m_options);
}

void
LinearSolver::_petscSolveLocalSystem(const DenseMatrixWrapper<double>& A,
                                     VectorWrapper<double> x,
                                     const VectorWrapper<const double>& b)
{
  Internals::_petscSolveLocalSystem(A, x, b, m_options);
}

LinearSolver::LinearSolver(const LinearSolverOptions& options) : m_options{options}
{
  Internals::checkHasLibrary(m_options.library());
  Internals::checkOptions(options);
}

LinearSolver::LinearSolver() : LinearSolver{LinearSolverOptions::default_options} {}
