#ifndef CRS_MATRIX_HPP
#define CRS_MATRIX_HPP

#include <algebra/Vector.hpp>
#include <utils/Array.hpp>
#include <utils/NaNHelper.hpp>
#include <utils/PugsAssert.hpp>

#include <iostream>

template <typename DataType, typename IndexType>
class CRSMatrixDescriptor;

template <typename DataType = double, typename IndexType = int>
class CRSMatrix
{
 public:
  using MutableDataType = std::remove_const_t<DataType>;
  using index_type      = IndexType;
  using data_type       = DataType;

 private:
  const IndexType m_nb_rows;
  const IndexType m_nb_columns;

  Array<const IndexType> m_row_map;
  Array<const DataType> m_values;
  Array<const IndexType> m_column_indices;

  template <typename DataType2, typename BinOp>
  CRSMatrix
  _binOp(const CRSMatrix& A, const CRSMatrix<DataType2, IndexType>& B) const
  {
    static_assert(std::is_same<std::remove_const_t<DataType>, std::remove_const_t<DataType2>>(),
                  "Cannot add matrices of different types");
    Assert(A.numberOfRows() == B.numberOfRows() and A.numberOfColumns() == B.numberOfColumns(),
           "cannot apply inner binary operator on matrices of different sizes");

    Array<int> non_zeros(A.m_nb_rows);
    for (IndexType i_row = 0; i_row < A.m_nb_rows; ++i_row) {
      const auto A_row_begin = A.m_row_map[i_row];
      const auto A_row_end   = A.m_row_map[i_row + 1];
      const auto B_row_begin = B.m_row_map[i_row];
      const auto B_row_end   = B.m_row_map[i_row + 1];
      IndexType i_row_A      = A_row_begin;
      IndexType i_row_B      = B_row_begin;

      int row_nb_columns = 0;

      while (i_row_A < A_row_end or i_row_B < B_row_end) {
        const IndexType A_column_idx = [&] {
          if (i_row_A < A_row_end) {
            return A.m_column_indices[i_row_A];
          } else {
            return std::numeric_limits<IndexType>::max();
          }
        }();

        const IndexType B_column_idx = [&] {
          if (i_row_B < B_row_end) {
            return B.m_column_indices[i_row_B];
          } else {
            return std::numeric_limits<IndexType>::max();
          }
        }();

        if (A_column_idx == B_column_idx) {
          ++row_nb_columns;
          ++i_row_A;
          ++i_row_B;
        } else if (A_column_idx < B_column_idx) {
          ++row_nb_columns;
          ++i_row_A;
        } else {
          Assert(B_column_idx < A_column_idx);
          ++row_nb_columns;
          ++i_row_B;
        }
      }
      non_zeros[i_row] = row_nb_columns;
    }

    Array<IndexType> row_map(A.m_nb_rows + 1);
    row_map[0] = 0;
    for (IndexType i = 0; i < A.m_nb_rows; ++i) {
      row_map[i + 1] = row_map[i] + non_zeros[i];
    }

    const IndexType nb_values = row_map[row_map.size() - 1];
    Array<DataType> values(nb_values);
    Array<IndexType> column_indices(nb_values);

    IndexType i_value = 0;
    for (IndexType i_row = 0; i_row < A.m_nb_rows; ++i_row) {
      const auto A_row_begin = A.m_row_map[i_row];
      const auto A_row_end   = A.m_row_map[i_row + 1];
      const auto B_row_begin = B.m_row_map[i_row];
      const auto B_row_end   = B.m_row_map[i_row + 1];
      IndexType i_row_A      = A_row_begin;
      IndexType i_row_B      = B_row_begin;

      while (i_row_A < A_row_end or i_row_B < B_row_end) {
        const IndexType A_column_idx = [&] {
          if (i_row_A < A_row_end) {
            return A.m_column_indices[i_row_A];
          } else {
            return std::numeric_limits<IndexType>::max();
          }
        }();

        const IndexType B_column_idx = [&] {
          if (i_row_B < B_row_end) {
            return B.m_column_indices[i_row_B];
          } else {
            return std::numeric_limits<IndexType>::max();
          }
        }();

        if (A_column_idx == B_column_idx) {
          column_indices[i_value] = A_column_idx;
          values[i_value]         = BinOp()(A.m_values[i_row_A], B.m_values[i_row_B]);
          ++i_row_A;
          ++i_row_B;
        } else if (A_column_idx < B_column_idx) {
          column_indices[i_value] = A_column_idx;
          values[i_value]         = BinOp()(A.m_values[i_row_A], 0);
          ++i_row_A;
        } else {
          Assert(B_column_idx < A_column_idx);
          column_indices[i_value] = B_column_idx;
          values[i_value]         = BinOp()(0, B.m_values[i_row_B]);
          ++i_row_B;
        }
        ++i_value;
      }
    }

    return CRSMatrix(A.m_nb_rows, A.m_nb_columns, row_map, values, column_indices);
  }

 public:
  PUGS_INLINE
  bool
  isSquare() const noexcept
  {
    return m_nb_rows == m_nb_columns;
  }

  PUGS_INLINE
  IndexType
  numberOfRows() const
  {
    return m_nb_rows;
  }

  PUGS_INLINE
  IndexType
  numberOfColumns() const
  {
    return m_nb_columns;
  }

  auto
  values() const
  {
    return m_values;
  }

  auto
  rowMap() const
  {
    return m_row_map;
  }

  auto
  columnIndices() const
  {
    return m_column_indices;
  }

  template <typename DataType2>
  Vector<MutableDataType>
  operator*(const Vector<DataType2>& x) const
  {
    static_assert(std::is_same<std::remove_const_t<DataType>, std::remove_const_t<DataType2>>(),
                  "Cannot multiply matrix and vector of different types");

    Assert(x.size() - m_nb_columns == 0, "cannot compute product: incompatible matrix and vector sizes");

    Vector<MutableDataType> Ax(m_nb_rows);

    parallel_for(
      m_nb_rows, PUGS_CLASS_LAMBDA(const IndexType& i_row) {
        const auto row_begin = m_row_map[i_row];
        const auto row_end   = m_row_map[i_row + 1];
        MutableDataType sum{0};
        for (IndexType j = row_begin; j < row_end; ++j) {
          sum += m_values[j] * x[m_column_indices[j]];
        }
        Ax[i_row] = sum;
      });

    return Ax;
  }

  template <typename DataType2>
  CRSMatrix<DataType>
  operator+(const CRSMatrix<DataType2>& B) const
  {
    return this->_binOp<DataType2, std::plus<DataType>>(*this, B);
  }

  template <typename DataType2>
  CRSMatrix<DataType>
  operator-(const CRSMatrix<DataType2>& B) const
  {
    return this->_binOp<DataType2, std::minus<DataType>>(*this, B);
  }

  friend PUGS_INLINE std::ostream&
  operator<<(std::ostream& os, const CRSMatrix& A)
  {
    for (IndexType i = 0; i < A.m_nb_rows; ++i) {
      const auto row_begin = A.m_row_map[i];
      const auto row_end   = A.m_row_map[i + 1];
      os << i << "|";
      for (IndexType j = row_begin; j < row_end; ++j) {
        os << ' ' << A.m_column_indices[j] << ':' << NaNHelper(A.m_values[j]);
      }
      os << '\n';
    }
    return os;
  }

 private:
  friend class CRSMatrixDescriptor<DataType, IndexType>;

  CRSMatrix(const IndexType nb_rows,
            const IndexType nb_columns,
            const Array<const IndexType>& row_map,
            const Array<const DataType>& values,
            const Array<const IndexType>& column_indices)
    : m_nb_rows{nb_rows},
      m_nb_columns{nb_columns},
      m_row_map{row_map},
      m_values{values},
      m_column_indices{column_indices}
  {}

 public:
  ~CRSMatrix() = default;
};

#endif   // CRS_MATRIX_HPP
