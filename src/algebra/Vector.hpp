#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <utils/Array.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>
#include <utils/PugsUtils.hpp>
#include <utils/Types.hpp>

template <typename DataType>
class Vector   // LCOV_EXCL_LINE
{
 public:
  using data_type  = DataType;
  using index_type = size_t;

 private:
  Array<DataType> m_values;
  const bool m_deep_copies;

  static_assert(std::is_same_v<typename decltype(m_values)::index_type, index_type>);
  static_assert(std::is_arithmetic_v<DataType>, "Vector is only defined for arithmetic values");

  // Allows const version to access our data
  friend Vector<std::add_const_t<DataType>>;

 public:
  friend std::ostream&
  operator<<(std::ostream& os, const Vector& x)
  {
    return os << x.m_values;
  }

  [[nodiscard]] friend std::remove_const_t<DataType>
  min(const Vector& x)
  {
    return min(x.m_values);
  }

  [[nodiscard]] friend std::remove_const_t<DataType>
  max(const Vector& x)
  {
    return max(x.m_values);
  }

  friend Vector<std::remove_const_t<DataType>>
  copy(const Vector& x)
  {
    auto values = copy(x.m_values);

    Vector<std::remove_const_t<DataType>> x_copy{0};
    x_copy.m_values = values;
    return x_copy;
  }

  PUGS_INLINE
  Vector<std::remove_const_t<DataType>>
  operator-() const
  {
    Vector<std::remove_const_t<DataType>> opposite(this->size());
    parallel_for(
      opposite.size(), PUGS_CLASS_LAMBDA(index_type i) { opposite.m_values[i] = -m_values[i]; });
    return opposite;
  }

  friend Vector
  operator*(const DataType& a, const Vector& x)
  {
    Vector<std::remove_const_t<DataType>> y = copy(x);
    return y *= a;
  }

  template <typename DataType2>
  PUGS_INLINE friend auto
  dot(const Vector& x, const Vector<DataType2>& y)
  {
    Assert(x.size() == y.size(), "cannot compute dot product: incompatible vector sizes");
    decltype(std::declval<DataType>() * std::declval<DataType2>()) sum = 0;

    // Does not use parallel_for to preserve sum order
    for (index_type i = 0; i < x.size(); ++i) {
      sum += x[i] * y[i];
    }

    return sum;
  }

  template <typename DataType2>
  PUGS_INLINE Vector&
  operator/=(const DataType2& a)
  {
    const auto inv_a = 1. / a;
    return (*this) *= inv_a;
  }

  template <typename DataType2>
  PUGS_INLINE Vector&
  operator*=(const DataType2& a)
  {
    parallel_for(
      this->size(), PUGS_CLASS_LAMBDA(index_type i) { m_values[i] *= a; });
    return *this;
  }

  template <typename DataType2>
  PUGS_INLINE Vector&
  operator-=(const Vector<DataType2>& y)
  {
    Assert(this->size() == y.size(), "cannot substract vector: incompatible sizes");

    parallel_for(
      this->size(), PUGS_CLASS_LAMBDA(index_type i) { m_values[i] -= y[i]; });

    return *this;
  }

  template <typename DataType2>
  PUGS_INLINE Vector&
  operator+=(const Vector<DataType2>& y)
  {
    Assert(this->size() == y.size(), "cannot add vector: incompatible sizes");

    parallel_for(
      this->size(), PUGS_CLASS_LAMBDA(index_type i) { m_values[i] += y[i]; });

    return *this;
  }

  template <typename DataType2>
  PUGS_INLINE Vector<std::remove_const_t<DataType>>
  operator+(const Vector<DataType2>& y) const
  {
    Assert(this->size() == y.size(), "cannot compute vector sum: incompatible sizes");
    Vector<std::remove_const_t<DataType>> sum{y.size()};

    parallel_for(
      this->size(), PUGS_CLASS_LAMBDA(index_type i) { sum.m_values[i] = m_values[i] + y[i]; });

    return sum;
  }

  template <typename DataType2>
  PUGS_INLINE Vector<std::remove_const_t<DataType>>
  operator-(const Vector<DataType2>& y) const
  {
    Assert(this->size() == y.size(), "cannot compute vector difference: incompatible sizes");
    Vector<std::remove_const_t<DataType>> sum{y.size()};

    parallel_for(
      this->size(), PUGS_CLASS_LAMBDA(index_type i) { sum.m_values[i] = m_values[i] - y[i]; });

    return sum;
  }

  PUGS_INLINE
  DataType&
  operator[](index_type i) const noexcept(NO_ASSERT)
  {
    return m_values[i];
  }

  PUGS_INLINE
  size_t
  size() const noexcept
  {
    return m_values.size();
  }

  PUGS_INLINE Vector&
  fill(const DataType& value) noexcept
  {
    m_values.fill(value);
    return *this;
  }

  PUGS_INLINE Vector& operator=(const DataType&) = delete;

  PUGS_INLINE Vector&
  operator=(const ZeroType&) noexcept
  {
    m_values.fill(0);
    return *this;
  }

  template <typename DataType2>
  PUGS_INLINE Vector&
  operator=(const Vector<DataType2>& x)
  {
    // ensures that DataType is the same as source DataType2
    static_assert(std::is_same<std::remove_const_t<DataType>, std::remove_const_t<DataType2>>(),
                  "Cannot assign Vector of different type");
    // ensures that const is not lost through copy. If this point is
    // reached data types only differ through their constness
    static_assert((not std::is_const_v<DataType2>), "Cannot assign Vector of const to Vector of non-const");

    Assert(not m_deep_copies, "cannot assign value to a Vector of const that required deep copies");
    m_values = x.m_values;
    return *this;
  }

  PUGS_INLINE
  Vector&
  operator=(const Vector& x)
  {
    if (m_deep_copies) {
      Assert(not std::is_const_v<DataType>, "cannot assign value to a Vector of const that required deep copies");
      if constexpr (not std::is_const_v<DataType>) {
        copy_to(x.m_values, m_values);
      }
    } else {
      m_values = x.m_values;
    }
    return *this;
  }

  PUGS_INLINE
  Vector&
  operator=(Vector&& x)
  {
    if (m_deep_copies) {
      copy_to(x.m_values, m_values);
    } else {
      m_values = x.m_values;
    }
    return *this;
  }

  template <typename DataType2>
  Vector(const Vector<DataType2>& x) : m_deep_copies{x.m_deep_copies}
  {
    // ensures that DataType is the same as source DataType2
    static_assert(std::is_same<std::remove_const_t<DataType>, std::remove_const_t<DataType2>>(),
                  "Cannot assign Vector of different type");
    // ensures that const is not lost through copy
    static_assert(((std::is_const_v<DataType2> and std::is_const_v<DataType>) or not std::is_const_v<DataType2>),
                  "Cannot assign Vector of const to Vector of non-const");

    m_values = x.m_values;
  }

  explicit Vector(const Array<DataType>& values) : m_values{values}, m_deep_copies{true} {}

  Vector(const Vector&) = default;
  Vector(Vector&&)      = default;

  Vector(size_t size) : m_values{size}, m_deep_copies{false} {}
  Vector() : m_deep_copies{false} {}

  ~Vector() = default;
};

#endif   // VECTOR_HPP
