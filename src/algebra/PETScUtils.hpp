#ifndef PETSC_UTILS_HPP
#define PETSC_UTILS_HPP

#include <utils/pugs_config.hpp>

#ifdef PUGS_HAS_PETSC

#include <algebra/CRSMatrix.hpp>
#include <algebra/DenseMatrixWrapper.hpp>
#include <algebra/SmallMatrix.hpp>
#include <algebra/TinyMatrix.hpp>

#include <petsc.h>

class PETScAijMatrixEmbedder
{
 private:
  Mat m_petscMat;
  Array<const PetscScalar> m_values;
  Array<const PetscInt> m_row_indices;
  Array<const PetscInt> m_column_indices;
  SmallArray<const PetscInt> m_small_row_indices;
  SmallArray<const PetscInt> m_small_column_indices;
  const size_t m_nb_rows;
  const size_t m_nb_columns;

  PETScAijMatrixEmbedder(const size_t nb_rows, const size_t nb_columns, const double* A);

 public:
  PUGS_INLINE
  size_t
  numberOfRows() const
  {
    return m_nb_rows;
  }

  PUGS_INLINE
  size_t
  numberOfColumns() const
  {
    return m_nb_columns;
  }

  PUGS_INLINE
  operator Mat&()
  {
    return m_petscMat;
  }

  PUGS_INLINE
  operator const Mat&() const
  {
    return m_petscMat;
  }

  PETScAijMatrixEmbedder(const DenseMatrixWrapper<double>& A)
    : PETScAijMatrixEmbedder{A.numberOfRows(), A.numberOfColumns(), A.ptr()}
  {}

  PETScAijMatrixEmbedder(const CRSMatrix<double, int>& A);

  ~PETScAijMatrixEmbedder();
};

#endif   // PUGS_HAS_PETSC

#endif   // PETSC_UTILS_HPP
