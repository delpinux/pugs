#ifndef VECTOR_WRAPPER_HPP
#define VECTOR_WRAPPER_HPP

#include <algebra/SmallVector.hpp>
#include <algebra/TinyVector.hpp>
#include <algebra/Vector.hpp>

template <typename DataType>
class VectorWrapper
{
 private:
  const size_t m_size;
  DataType* const m_vector_ptr;

 public:
  PUGS_INLINE
  size_t
  size() const
  {
    return m_size;
  }

  PUGS_INLINE
  DataType* const&
  ptr() const
  {
    return m_vector_ptr;
  }

  VectorWrapper(const Vector<DataType>& x) : m_size{x.size()}, m_vector_ptr{(m_size > 0) ? (&x[0]) : nullptr} {}
  VectorWrapper(Vector<DataType>& x) : m_size{x.size()}, m_vector_ptr{(m_size > 0) ? (&x[0]) : nullptr} {}

  VectorWrapper(const SmallVector<DataType>& x) : m_size{x.size()}, m_vector_ptr{(m_size > 0) ? (&x[0]) : nullptr} {}

  template <size_t N>
  VectorWrapper(const TinyVector<N, DataType>& x) : m_size{N}, m_vector_ptr{(N > 0) ? (&x[0]) : nullptr}
  {}

  template <size_t N>
  VectorWrapper(TinyVector<N, DataType>& x) : m_size{N}, m_vector_ptr{(N > 0) ? (&x[0]) : nullptr}
  {}

  VectorWrapper(const VectorWrapper&) = delete;
  VectorWrapper(VectorWrapper&&)      = delete;
};

#endif   // VECTOR_WRAPPER_HPP
