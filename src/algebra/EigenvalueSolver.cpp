#include <algebra/EigenvalueSolver.hpp>
#include <utils/pugs_config.hpp>

#ifdef PUGS_HAS_SLEPC
#include <algebra/PETScUtils.hpp>
#include <slepc.h>
#endif   // PUGS_HAS_SLEPC

#ifdef PUGS_HAS_EIGEN3
#include <algebra/Eigen3Utils.hpp>
#endif   // PUGS_HAS_EIGEN3

struct EigenvalueSolver::Internals
{
#ifdef PUGS_HAS_SLEPC
  static PetscReal
  computeSmallestRealEigenvalueOfSymmetricMatrix(EPS& eps)
  {
    EPSSetWhichEigenpairs(eps, EPS_SMALLEST_REAL);
    EPSSolve(eps);

    PetscReal smallest_eigenvalue;
    EPSGetEigenpair(eps, 0, &smallest_eigenvalue, nullptr, nullptr, nullptr);
    return smallest_eigenvalue;
  }

  static PetscReal
  computeLargestRealEigenvalueOfSymmetricMatrix(EPS& eps)
  {
    EPSSetWhichEigenpairs(eps, EPS_LARGEST_REAL);
    EPSSolve(eps);

    PetscReal largest_eigenvalue;
    EPSGetEigenpair(eps, 0, &largest_eigenvalue, nullptr, nullptr, nullptr);
    return largest_eigenvalue;
  }

  static void
  computeAllEigenvaluesOfSymmetricMatrixInInterval(EPS& eps, const PetscReal left_bound, const PetscReal right_bound)
  {
    Assert(left_bound < right_bound);
    EPSSetType(eps, EPSKRYLOVSCHUR);
    EPSSetWhichEigenpairs(eps, EPS_ALL);
    EPSSetInterval(eps, left_bound - 0.01 * std::abs(left_bound), right_bound + 0.01 * std::abs(right_bound));

    ST st;
    EPSGetST(eps, &st);
    STSetType(st, STSINVERT);

    KSP ksp;
#if (SLEPC_VERSION_MAJOR >= 3) && (SLEPC_VERSION_MINOR >= 15)
    EPSKrylovSchurGetKSP(eps, &ksp);
#else
    STGetKSP(st, &ksp);
#endif
    KSPSetType(ksp, KSPPREONLY);

    PC pc;
    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCCHOLESKY);
    EPSSetFromOptions(eps);

    EPSSolve(eps);
  }

  static void
  computeAllEigenvaluesOfSymmetricMatrix(EPS& eps)
  {
    const PetscReal left_bound  = computeSmallestRealEigenvalueOfSymmetricMatrix(eps);
    const PetscReal right_bound = computeLargestRealEigenvalueOfSymmetricMatrix(eps);

    computeAllEigenvaluesOfSymmetricMatrixInInterval(eps, left_bound - 0.01 * std::abs(left_bound),
                                                     right_bound + 0.01 * std::abs(right_bound));
  }

  static void
  slepscComputeForSymmetricMatrix(const PETScAijMatrixEmbedder& A, SmallArray<double>& eigenvalues)
  {
    EPS eps;

    EPSCreate(PETSC_COMM_SELF, &eps);
    EPSSetOperators(eps, A, nullptr);
    EPSSetProblemType(eps, EPS_HEP);

    Internals::computeAllEigenvaluesOfSymmetricMatrix(eps);

    PetscInt nb_eigenvalues;
    EPSGetDimensions(eps, &nb_eigenvalues, nullptr, nullptr);

    eigenvalues = SmallArray<double>(nb_eigenvalues);
    for (PetscInt i = 0; i < nb_eigenvalues; ++i) {
      EPSGetEigenpair(eps, i, &(eigenvalues[i]), nullptr, nullptr, nullptr);
    }

    EPSDestroy(&eps);
  }

  static void
  slepscComputeForSymmetricMatrix(const PETScAijMatrixEmbedder& A,
                                  SmallArray<double>& eigenvalues,
                                  std::vector<SmallVector<double>>& eigenvector_list)
  {
    EPS eps;

    EPSCreate(PETSC_COMM_SELF, &eps);
    EPSSetOperators(eps, A, nullptr);
    EPSSetProblemType(eps, EPS_HEP);

    Internals::computeAllEigenvaluesOfSymmetricMatrix(eps);

    PetscInt nb_eigenvalues;
    EPSGetDimensions(eps, &nb_eigenvalues, nullptr, nullptr);

    eigenvalues = SmallArray<double>(nb_eigenvalues);
    eigenvector_list.reserve(nb_eigenvalues);
    for (PetscInt i = 0; i < nb_eigenvalues; ++i) {
      Vec Vr;
      SmallVector<double> eigenvector{A.numberOfRows()};
      VecCreateSeqWithArray(PETSC_COMM_SELF, 1, A.numberOfRows(), &(eigenvector[0]), &Vr);
      EPSGetEigenpair(eps, i, &(eigenvalues[i]), nullptr, Vr, nullptr);
      VecDestroy(&Vr);
      eigenvector_list.push_back(eigenvector);
    }

    EPSDestroy(&eps);
  }

  static void
  slepscComputeForSymmetricMatrix(const PETScAijMatrixEmbedder& A,
                                  SmallArray<double>& eigenvalues,
                                  SmallMatrix<double>& P)
  {
    EPS eps;

    EPSCreate(PETSC_COMM_SELF, &eps);
    EPSSetOperators(eps, A, nullptr);
    EPSSetProblemType(eps, EPS_HEP);

    Internals::computeAllEigenvaluesOfSymmetricMatrix(eps);

    PetscInt nb_eigenvalues;
    EPSGetDimensions(eps, &nb_eigenvalues, nullptr, nullptr);

    eigenvalues = SmallArray<double>(nb_eigenvalues);
    P           = SmallMatrix<double>(nb_eigenvalues, nb_eigenvalues);

    Array<double> eigenvector(nb_eigenvalues);
    for (PetscInt i = 0; i < nb_eigenvalues; ++i) {
      Vec Vr;
      VecCreateSeqWithArray(PETSC_COMM_SELF, 1, A.numberOfRows(), &(eigenvector[0]), &Vr);
      EPSGetEigenpair(eps, i, &(eigenvalues[i]), nullptr, Vr, nullptr);
      VecDestroy(&Vr);
      for (size_t j = 0; j < eigenvector.size(); ++j) {
        P(j, i) = eigenvector[j];
      }
    }

    EPSDestroy(&eps);
  }
#endif   // PUGS_HAS_SLEPC

#ifdef PUGS_HAS_EIGEN3

  template <typename Eigen3MatrixEmbedderType>
  static void
  eigen3ComputeForSymmetricMatrix(const Eigen3MatrixEmbedderType& A, SmallArray<double>& eigenvalues)
  {
    using Eigen3MatrixType = typename Eigen3MatrixEmbedderType::Eigen3MatrixType;
    Eigen::SelfAdjointEigenSolver<Eigen3MatrixType> eigen_solver;

    eigen_solver.compute(A.matrix(), Eigen::EigenvaluesOnly);

    eigenvalues = SmallArray<double>(A.numberOfRows());
    for (size_t i = 0; i < eigenvalues.size(); ++i) {
      eigenvalues[i] = eigen_solver.eigenvalues()[i];
    }
  }

  template <typename Eigen3MatrixEmbedderType>
  static void
  eigen3ComputeForSymmetricMatrix(const Eigen3MatrixEmbedderType& A,
                                  SmallArray<double>& eigenvalues,
                                  std::vector<SmallVector<double>>& eigenvectors)
  {
    using Eigen3MatrixType = typename Eigen3MatrixEmbedderType::Eigen3MatrixType;
    Eigen::SelfAdjointEigenSolver<Eigen3MatrixType> eigen_solver;

    eigen_solver.compute(A.matrix(), Eigen::ComputeEigenvectors);

    eigenvalues = SmallArray<double>(A.numberOfRows());
    for (size_t i = 0; i < eigenvalues.size(); ++i) {
      eigenvalues[i] = eigen_solver.eigenvalues()[i];
    }

    eigenvectors.resize(eigenvalues.size());
    for (size_t i = 0; i < eigenvalues.size(); ++i) {
      eigenvectors[i] = SmallVector<double>{eigenvalues.size()};
      for (size_t j = 0; j < eigenvalues.size(); ++j) {
        eigenvectors[i][j] = eigen_solver.eigenvectors().coeff(j, i);
      }
    }
  }

  template <typename Eigen3MatrixEmbedderType>
  static void
  eigen3ComputeForSymmetricMatrix(const Eigen3MatrixEmbedderType& A,
                                  SmallArray<double>& eigenvalues,
                                  SmallMatrix<double>& P)
  {
    using Eigen3MatrixType = typename Eigen3MatrixEmbedderType::Eigen3MatrixType;
    Eigen::SelfAdjointEigenSolver<Eigen3MatrixType> eigen_solver;

    eigen_solver.compute(A.matrix(), Eigen::ComputeEigenvectors);

    eigenvalues = SmallArray<double>(A.numberOfRows());
    for (size_t i = 0; i < eigenvalues.size(); ++i) {
      eigenvalues[i] = eigen_solver.eigenvalues()[i];
    }

    P = SmallMatrix<double>(eigenvalues.size(), eigenvalues.size());
    for (size_t i = 0; i < eigenvalues.size(); ++i) {
      for (size_t j = 0; j < eigenvalues.size(); ++j) {
        P(i, j) = eigen_solver.eigenvectors().coeff(i, j);
      }
    }
  }
#endif   // PUGS_HAS_EIGEN3
};

#ifdef PUGS_HAS_SLEPC
void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const CRSMatrix<double>& A, SmallArray<double>& eigenvalues)
{
  Internals::slepscComputeForSymmetricMatrix(A, eigenvalues);
}

void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const DenseMatrixWrapper<double>& A, SmallArray<double>& eigenvalues)
{
  Internals::slepscComputeForSymmetricMatrix(A, eigenvalues);
}

void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const CRSMatrix<double>& A,
                                                   SmallArray<double>& eigenvalues,
                                                   std::vector<SmallVector<double>>& eigenvectors)
{
  Internals::slepscComputeForSymmetricMatrix(A, eigenvalues, eigenvectors);
}

void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const DenseMatrixWrapper<double>& A,
                                                   SmallArray<double>& eigenvalues,
                                                   std::vector<SmallVector<double>>& eigenvectors)
{
  Internals::slepscComputeForSymmetricMatrix(A, eigenvalues, eigenvectors);
}

void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const CRSMatrix<double>& A,
                                                   SmallArray<double>& eigenvalues,
                                                   SmallMatrix<double>& P)
{
  Internals::slepscComputeForSymmetricMatrix(A, eigenvalues, P);
}

void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const DenseMatrixWrapper<double>& A,
                                                   SmallArray<double>& eigenvalues,
                                                   SmallMatrix<double>& P)
{
  Internals::slepscComputeForSymmetricMatrix(A, eigenvalues, P);
}

#else   // PUGS_HAS_SLEPC

void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const CRSMatrix<double>&, SmallArray<double>&)
{}

void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const DenseMatrixWrapper<double>&, SmallArray<double>&)
{}

void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const CRSMatrix<double>&,
                                                   SmallArray<double>&,
                                                   std::vector<SmallVector<double>>&)
{}

void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const DenseMatrixWrapper<double>&,
                                                   SmallArray<double>&,
                                                   std::vector<SmallVector<double>>&)
{}

void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const CRSMatrix<double>&, SmallArray<double>&, SmallMatrix<double>&)
{}

void
EigenvalueSolver::_slepscComputeForSymmetricMatrix(const DenseMatrixWrapper<double>&,
                                                   SmallArray<double>&,
                                                   SmallMatrix<double>&)
{}

#endif   // PUGS_HAS_SLEPC

#ifdef PUGS_HAS_EIGEN3
void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const CRSMatrix<double>& A, SmallArray<double>& eigenvalues)
{
  Eigen3SparseMatrixEmbedder embedded_A{A};
  Internals::eigen3ComputeForSymmetricMatrix(embedded_A, eigenvalues);
}

void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const DenseMatrixWrapper<double>& A, SmallArray<double>& eigenvalues)
{
  Internals::eigen3ComputeForSymmetricMatrix(Eigen3DenseMatrixEmbedder{A}, eigenvalues);
}

void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const CRSMatrix<double>& A,
                                                   SmallArray<double>& eigenvalues,
                                                   std::vector<SmallVector<double>>& eigenvectors)
{
  Internals::eigen3ComputeForSymmetricMatrix(Eigen3SparseMatrixEmbedder{A}, eigenvalues, eigenvectors);
}

void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const DenseMatrixWrapper<double>& A,
                                                   SmallArray<double>& eigenvalues,
                                                   std::vector<SmallVector<double>>& eigenvectors)
{
  Internals::eigen3ComputeForSymmetricMatrix(Eigen3DenseMatrixEmbedder{A}, eigenvalues, eigenvectors);
}

void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const CRSMatrix<double>& A,
                                                   SmallArray<double>& eigenvalues,
                                                   SmallMatrix<double>& P)
{
  Internals::eigen3ComputeForSymmetricMatrix(Eigen3SparseMatrixEmbedder{A}, eigenvalues, P);
}

void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const DenseMatrixWrapper<double>& A,
                                                   SmallArray<double>& eigenvalues,
                                                   SmallMatrix<double>& P)
{
  Internals::eigen3ComputeForSymmetricMatrix(Eigen3DenseMatrixEmbedder{A}, eigenvalues, P);
}

#else   // PUGS_HAS_EIGEN3

void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const CRSMatrix<double>&, SmallArray<double>&)
{}

void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const DenseMatrixWrapper<double>&, SmallArray<double>&)
{}

void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const CRSMatrix<double>&,
                                                   SmallArray<double>&,
                                                   std::vector<SmallVector<double>>&)
{}

void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const DenseMatrixWrapper<double>&,
                                                   SmallArray<double>&,
                                                   std::vector<SmallVector<double>>&)
{}

void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const CRSMatrix<double>&, SmallArray<double>&, SmallMatrix<double>&)
{}

void
EigenvalueSolver::_eigen3ComputeForSymmetricMatrix(const DenseMatrixWrapper<double>&,
                                                   SmallArray<double>&,
                                                   SmallMatrix<double>&)
{}

#endif   // PUGS_HAS_EIGEN3

bool
EigenvalueSolver::hasLibrary(const ESLibrary library)
{
  switch (library) {
  case ESLibrary::eigen3: {
#ifdef PUGS_HAS_EIGEN3
    return true;
#else
    return false;
#endif
  }
  case ESLibrary::slepsc: {
#ifdef PUGS_HAS_SLEPC
    return true;
#else
    return false;
#endif
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("Eigenvalue  library (" + ::name(library) + ") was not set!");
  }
    // LCOV_EXCL_STOP
  }
}

void
EigenvalueSolver::checkHasLibrary(const ESLibrary library)
{
  if (not hasLibrary(library)) {
    // LCOV_EXCL_START
    throw NormalError(::name(library) + " is not linked to pugs. Cannot use it!");
    // LCOV_EXCL_STOP
  }
}

EigenvalueSolver::EigenvalueSolver(const EigenvalueSolverOptions& options) : m_options{options}
{
  checkHasLibrary(options.library());
}
