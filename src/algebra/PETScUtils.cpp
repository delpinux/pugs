#include <algebra/PETScUtils.hpp>

#ifdef PUGS_HAS_PETSC

PETScAijMatrixEmbedder::PETScAijMatrixEmbedder(const size_t nb_rows, const size_t nb_columns, const double* A)
  : m_nb_rows{nb_rows}, m_nb_columns{nb_columns}
{
  MatCreate(PETSC_COMM_SELF, &m_petscMat);
  MatSetSizes(m_petscMat, PETSC_DECIDE, PETSC_DECIDE, nb_rows, nb_columns);
  MatSetFromOptions(m_petscMat);
  MatSetType(m_petscMat, MATAIJ);
  MatSetUp(m_petscMat);

  {
    SmallArray<PetscInt> row_indices(nb_rows);
    for (size_t i = 0; i < nb_rows; ++i) {
      row_indices[i] = i;
    }
    m_small_row_indices = row_indices;
  }

  if (nb_rows == nb_columns) {
    m_small_column_indices = m_small_row_indices;
  } else {
    SmallArray<PetscInt> column_indices(nb_columns);
    for (size_t i = 0; i < nb_columns; ++i) {
      column_indices[i] = i;
    }
    m_small_column_indices = column_indices;
  }

  MatSetValuesBlocked(m_petscMat, nb_rows, &(m_small_row_indices[0]), nb_columns, &(m_small_column_indices[0]), A,
                      INSERT_VALUES);

  MatAssemblyBegin(m_petscMat, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(m_petscMat, MAT_FINAL_ASSEMBLY);
}

PETScAijMatrixEmbedder::PETScAijMatrixEmbedder(const CRSMatrix<double, int>& A)
  : m_nb_rows(A.numberOfRows()), m_nb_columns(A.numberOfColumns())
{
  static_assert(std::is_same_v<PetscInt, typename std::decay_t<decltype(A)>::index_type>,
                "index type conversion is not implemented yet");
  static_assert(std::is_same_v<PetscScalar, typename std::decay_t<decltype(A)>::data_type>,
                "value type conversion is not implemented yet");

  m_values         = A.values();
  m_row_indices    = A.rowMap();
  m_column_indices = A.columnIndices();

  MatCreateSeqAIJWithArrays(PETSC_COMM_SELF, A.numberOfRows(), A.numberOfColumns(),
                            const_cast<PetscInt*>(&m_row_indices[0]),      // These ugly const_cast
                            const_cast<PetscInt*>(&m_column_indices[0]),   // permit CRSMatrix to
                            const_cast<PetscScalar*>(&m_values[0]),        // be non-mutable
                            &m_petscMat);

  MatAssemblyBegin(m_petscMat, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(m_petscMat, MAT_FINAL_ASSEMBLY);
}

PETScAijMatrixEmbedder::~PETScAijMatrixEmbedder()
{
  MatDestroy(&m_petscMat);
}

#endif   // PUGS_HAS_PETSC
