#include <algebra/LinearSolverOptions.hpp>

#include <rang.hpp>

std::ostream&
operator<<(std::ostream& os, const LinearSolverOptions& options)
{
  os << "  library: " << rang::style::bold << name(options.library()) << rang::style::reset << '\n';
  os << "  method : " << rang::style::bold << name(options.method()) << rang::style::reset << '\n';
  os << "  precond: " << rang::style::bold << name(options.precond()) << rang::style::reset << '\n';
  os << "  epsilon: " << rang::style::bold << options.epsilon() << rang::style::reset << '\n';
  os << "  maxiter: " << rang::style::bold << options.maximumIteration() << rang::style::reset << '\n';
  os << "  verbose: " << rang::style::bold << std::boolalpha << options.verbose() << rang::style::reset << '\n';
  return os;
}
