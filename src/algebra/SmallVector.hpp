#ifndef SMALL_VECTOR_HPP
#define SMALL_VECTOR_HPP

#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>
#include <utils/PugsUtils.hpp>
#include <utils/SmallArray.hpp>
#include <utils/Types.hpp>

template <typename DataType>
class SmallVector   // LCOV_EXCL_LINE
{
 public:
  using data_type  = DataType;
  using index_type = size_t;

 private:
  SmallArray<DataType> m_values;

  static_assert(std::is_same_v<typename decltype(m_values)::index_type, index_type>);
  static_assert(std::is_arithmetic_v<DataType>, "SmallVector is only defined for arithmetic values");

  // Allows const version to access our data
  friend SmallVector<std::add_const_t<DataType>>;

 public:
  friend std::ostream&
  operator<<(std::ostream& os, const SmallVector& x)
  {
    return os << x.m_values;
  }

  [[nodiscard]] friend std::remove_const_t<DataType>
  min(const SmallVector& x)
  {
    return min(x.m_values);
  }

  [[nodiscard]] friend std::remove_const_t<DataType>
  max(const SmallVector& x)
  {
    return max(x.m_values);
  }

  friend SmallVector<std::remove_const_t<DataType>>
  copy(const SmallVector& x)
  {
    auto values = copy(x.m_values);

    SmallVector<std::remove_const_t<DataType>> x_copy{0};
    x_copy.m_values = values;
    return x_copy;
  }

  PUGS_INLINE
  SmallVector<std::remove_const_t<DataType>>
  operator-() const
  {
    SmallVector<std::remove_const_t<DataType>> opposite(this->size());
    parallel_for(
      opposite.size(), PUGS_CLASS_LAMBDA(index_type i) { opposite.m_values[i] = -m_values[i]; });
    return opposite;
  }

  friend SmallVector
  operator*(const DataType& a, const SmallVector& x)
  {
    SmallVector<std::remove_const_t<DataType>> y = copy(x);
    return y *= a;
  }

  template <typename DataType2>
  PUGS_INLINE friend auto
  dot(const SmallVector& x, const SmallVector<DataType2>& y)
  {
    Assert(x.size() == y.size(), "cannot compute dot product: incompatible vector sizes");

    decltype(std::declval<DataType>() * std::declval<DataType2>()) sum = 0;

    // Does not use parallel_for to preserve sum order
    for (index_type i = 0; i < x.size(); ++i) {
      sum += x[i] * y[i];
    }

    return sum;
  }

  template <typename DataType2>
  PUGS_INLINE SmallVector&
  operator/=(const DataType2& a)
  {
    const auto inv_a = 1. / a;
    return (*this) *= inv_a;
  }

  template <typename DataType2>
  PUGS_INLINE SmallVector&
  operator*=(const DataType2& a)
  {
    parallel_for(
      this->size(), PUGS_CLASS_LAMBDA(index_type i) { m_values[i] *= a; });
    return *this;
  }

  template <typename DataType2>
  PUGS_INLINE SmallVector&
  operator-=(const SmallVector<DataType2>& y)
  {
    Assert(this->size() == y.size(), "cannot substract vector: incompatible sizes");

    parallel_for(
      this->size(), PUGS_CLASS_LAMBDA(index_type i) { m_values[i] -= y[i]; });

    return *this;
  }

  template <typename DataType2>
  PUGS_INLINE SmallVector&
  operator+=(const SmallVector<DataType2>& y)
  {
    Assert(this->size() == y.size(), "cannot add vector: incompatible sizes");

    parallel_for(
      this->size(), PUGS_CLASS_LAMBDA(index_type i) { m_values[i] += y[i]; });

    return *this;
  }

  template <typename DataType2>
  PUGS_INLINE SmallVector<std::remove_const_t<DataType>>
  operator+(const SmallVector<DataType2>& y) const
  {
    Assert(this->size() == y.size(), "cannot compute vector sum: incompatible sizes");
    SmallVector<std::remove_const_t<DataType>> sum{y.size()};

    parallel_for(
      this->size(), PUGS_CLASS_LAMBDA(index_type i) { sum.m_values[i] = m_values[i] + y[i]; });

    return sum;
  }

  template <typename DataType2>
  PUGS_INLINE SmallVector<std::remove_const_t<DataType>>
  operator-(const SmallVector<DataType2>& y) const
  {
    Assert(this->size() == y.size(), "cannot compute vector difference: incompatible sizes");
    SmallVector<std::remove_const_t<DataType>> sum{y.size()};

    parallel_for(
      this->size(), PUGS_CLASS_LAMBDA(index_type i) { sum.m_values[i] = m_values[i] - y[i]; });

    return sum;
  }

  PUGS_INLINE
  DataType&
  operator[](index_type i) const noexcept(NO_ASSERT)
  {
    return m_values[i];
  }

  PUGS_INLINE
  size_t
  size() const noexcept
  {
    return m_values.size();
  }

  PUGS_INLINE SmallVector&
  fill(const DataType& value) noexcept
  {
    m_values.fill(value);
    return *this;
  }

  PUGS_INLINE SmallVector& operator=(const DataType&) = delete;

  PUGS_INLINE SmallVector&
  operator=(const ZeroType&) noexcept
  {
    m_values.fill(0);
    return *this;
  }

  template <typename DataType2>
  PUGS_INLINE SmallVector&
  operator=(const SmallVector<DataType2>& x)
  {
    // ensures that DataType is the same as source DataType2
    static_assert(std::is_same<std::remove_const_t<DataType>, std::remove_const_t<DataType2>>(),
                  "Cannot assign SmallVector of different type");
    // ensures that const is not lost through copy. If this point is
    // reached data types only differ through their constness
    static_assert((not std::is_const_v<DataType2>), "Cannot assign SmallVector of const to SmallVector of non-const");

    m_values = x.m_values;
    return *this;
  }

  PUGS_INLINE
  SmallVector& operator=(const SmallVector&) = default;

  PUGS_INLINE
  SmallVector& operator=(SmallVector&& x) = default;

  template <typename DataType2>
  SmallVector(const SmallVector<DataType2>& x)
  {
    // ensures that DataType is the same as source DataType2
    static_assert(std::is_same<std::remove_const_t<DataType>, std::remove_const_t<DataType2>>(),
                  "Cannot assign SmallVector of different type");
    // ensures that const is not lost through copy
    static_assert(((std::is_const_v<DataType2> and std::is_const_v<DataType>) or not std::is_const_v<DataType2>),
                  "Cannot assign SmallVector of const to SmallVector of non-const");

    m_values = x.m_values;
  }

  template <size_t N>
  explicit SmallVector(const TinyVector<N, DataType>& x) : m_values{N}
  {
    std::copy(&x[0], &x[0] + N, &m_values[0]);
  }

  SmallVector(const SmallVector&) = default;
  SmallVector(SmallVector&&)      = default;

  SmallVector(size_t size) : m_values{size} {}

  SmallVector()  = default;
  ~SmallVector() = default;
};

#endif   // SMALL_VECTOR_HPP
