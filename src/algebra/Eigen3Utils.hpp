#ifndef EIGEN3_UTILS_HPP
#define EIGEN3_UTILS_HPP

#include <utils/pugs_config.hpp>

#ifdef PUGS_HAS_EIGEN3

#include <algebra/CRSMatrix.hpp>
#include <algebra/DenseMatrixWrapper.hpp>

#include <eigen3/Eigen/Eigen>

class Eigen3DenseMatrixEmbedder
{
 public:
  using Eigen3MatrixType    = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor>;
  using Eigen3MapMatrixType = Eigen::Map<const Eigen3MatrixType>;

 private:
  Eigen3MapMatrixType m_eigen_matrix;

  Eigen3DenseMatrixEmbedder(const size_t nb_rows, const size_t nb_columns, const double* A)
    : m_eigen_matrix{A, int(nb_rows), int(nb_columns)}
  {}

 public:
  PUGS_INLINE
  size_t
  isSquare() const
  {
    return (m_eigen_matrix.rows() == m_eigen_matrix.cols());
  }

  PUGS_INLINE
  size_t
  numberOfRows() const
  {
    return m_eigen_matrix.rows();
  }

  PUGS_INLINE
  size_t
  numberOfColumns() const
  {
    return m_eigen_matrix.cols();
  }

  PUGS_INLINE
  Eigen3MapMatrixType&
  matrix()
  {
    return m_eigen_matrix;
  }

  PUGS_INLINE
  const Eigen3MapMatrixType&
  matrix() const
  {
    return m_eigen_matrix;
  }

  Eigen3DenseMatrixEmbedder(const DenseMatrixWrapper<double>& A)
    : Eigen3DenseMatrixEmbedder{A.numberOfRows(), A.numberOfColumns(), A.ptr()}
  {}

  ~Eigen3DenseMatrixEmbedder() = default;
};

class Eigen3SparseMatrixEmbedder
{
 public:
  using Eigen3MatrixType    = Eigen::SparseMatrix<double, Eigen::RowMajor>;
  using Eigen3MapMatrixType = Eigen::Map<const Eigen3MatrixType>;

 private:
  Eigen3MapMatrixType m_eigen_matrix;

 public:
  PUGS_INLINE
  size_t
  numberOfRows() const
  {
    return m_eigen_matrix.rows();
  }

  PUGS_INLINE
  size_t
  numberOfColumns() const
  {
    return m_eigen_matrix.cols();
  }

  PUGS_INLINE
  size_t
  isSquare() const
  {
    return (m_eigen_matrix.rows() == m_eigen_matrix.cols());
  }

  PUGS_INLINE
  Eigen3MapMatrixType&
  matrix()
  {
    return m_eigen_matrix;
  }

  PUGS_INLINE
  const Eigen3MapMatrixType&
  matrix() const
  {
    return m_eigen_matrix;
  }

  Eigen3SparseMatrixEmbedder(const CRSMatrix<double>& A)
    : m_eigen_matrix{A.numberOfRows(),
                     A.numberOfColumns(),
                     static_cast<int>(A.values().size()),                                  //
                     (A.rowMap().size() > 0) ? &(A.rowMap()[0]) : nullptr,                 //
                     (A.columnIndices().size() > 0) ? &(A.columnIndices()[0]) : nullptr,   //
                     (A.values().size() > 0) ? &(A.values()[0]) : nullptr}
  {}
  ~Eigen3SparseMatrixEmbedder() = default;
};

#else   // PUGS_HAS_EIGEN3

class Eigen3DenseMatrixEmbedder
{
 public:
  Eigen3DenseMatrixEmbedder(const DenseMatrixWrapper<double>&) {}

  ~Eigen3DenseMatrixEmbedder() = default;
};

class Eigen3SparseMatrixEmbedder
{
 public:
  Eigen3SparseMatrixEmbedder(const CRSMatrix<double>&) {}

  ~Eigen3SparseMatrixEmbedder() = default;
};

#endif   // PUGS_HAS_EIGEN3

#endif   // EIGEN3_UTILS_HPP
