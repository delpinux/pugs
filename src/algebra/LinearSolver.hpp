#ifndef LINEAR_SOLVER_HPP
#define LINEAR_SOLVER_HPP

#include <algebra/CRSMatrix.hpp>
#include <algebra/DenseMatrixWrapper.hpp>
#include <algebra/Eigen3Utils.hpp>
#include <algebra/LinearSolverOptions.hpp>
#include <algebra/SmallMatrix.hpp>
#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <algebra/Vector.hpp>
#include <algebra/VectorWrapper.hpp>
#include <utils/Exceptions.hpp>

class LinearSolver
{
 private:
  struct Internals;

  const LinearSolverOptions m_options;

  void _builtinSolveLocalSystem(const CRSMatrix<double, int>& A, Vector<double>& x, const Vector<const double>& b);

  void _builtinSolveLocalSystem(const SmallMatrix<double>& A,
                                SmallVector<double>& x,
                                const SmallVector<const double>& b);

  void _eigen3SolveLocalSystem(const Eigen3SparseMatrixEmbedder& A,
                               VectorWrapper<double> x,
                               const VectorWrapper<const double>& b);

  void _eigen3SolveLocalSystem(const DenseMatrixWrapper<double>& A,
                               VectorWrapper<double> x,
                               const VectorWrapper<const double>& b);

  void _petscSolveLocalSystem(const CRSMatrix<double, int>& A,
                              VectorWrapper<double> x,
                              const VectorWrapper<const double>& b);

  void _petscSolveLocalSystem(const DenseMatrixWrapper<double>& A,
                              VectorWrapper<double> x,
                              const VectorWrapper<const double>& b);

 public:
  bool hasLibrary(LSLibrary library) const;
  void checkOptions(const LinearSolverOptions& options) const;

  void
  solveLocalSystem(const CRSMatrix<double, int>& A, Vector<double>& x, const Vector<const double>& b)
  {
    Assert((x.size() == b.size()) and (x.size() - A.numberOfColumns() == 0) and A.isSquare());
    switch (m_options.library()) {
    case LSLibrary::builtin: {
      _builtinSolveLocalSystem(A, x, b);
      break;
    }
      // LCOV_EXCL_START
    case LSLibrary::eigen3: {
      // not covered since if Eigen3 is not linked this point is
      // unreachable: LinearSolver throws an exception at construction
      // in this case.
      _eigen3SolveLocalSystem(A, x, b);
      break;
    }
    case LSLibrary::petsc: {
      // not covered since if PETSc is not linked this point is
      // unreachable: LinearSolver throws an exception at construction
      // in this case.
      _petscSolveLocalSystem(A, x, b);
      break;
    }
    default: {
      throw UnexpectedError(::name(m_options.library()) + " cannot solve local systems");
    }
      // LCOV_EXCL_STOP
    }
  }

  void
  solveLocalSystem(const SmallMatrix<double>& A, SmallVector<double>& x, const SmallVector<const double>& b)
  {
    Assert((x.size() == b.size()) and (x.size() - A.numberOfColumns() == 0) and A.isSquare());
    switch (m_options.library()) {
    case LSLibrary::builtin: {
      _builtinSolveLocalSystem(A, x, b);
      break;
    }
      // LCOV_EXCL_START
    case LSLibrary::eigen3: {
      // not covered since if Eigen3 is not linked this point is
      // unreachable: LinearSolver throws an exception at construction
      // in this case.
      _eigen3SolveLocalSystem(A, x, b);
      break;
    }
    case LSLibrary::petsc: {
      // not covered since if PETSc is not linked this point is
      // unreachable: LinearSolver throws an exception at construction
      // in this case.
      _petscSolveLocalSystem(A, x, b);
      break;
    }
    default: {
      throw UnexpectedError(::name(m_options.library()) + " cannot solve local systems");
    }
      // LCOV_EXCL_STOP
    }
  }

  template <size_t N>
  void
  solveLocalSystem(const TinyMatrix<N>& A, TinyVector<N>& x, const TinyVector<N>& b)
  {
    Assert((x.size() == b.size()) and (x.size() - A.numberOfColumns() == 0) and A.isSquare());
    switch (m_options.library()) {
    case LSLibrary::builtin: {
      // Probably expensive but builtin is not the preferred way to
      // solve linear systems
      SmallMatrix A_dense{A};

      SmallVector x_vector{x};
      SmallVector<const double> b_vector{b};

      this->solveLocalSystem(A_dense, x_vector, b_vector);

      for (size_t i = 0; i < N; ++i) {
        x[i] = x_vector[i];
      }

      _builtinSolveLocalSystem(A, x, b);
      break;
    }
      // LCOV_EXCL_START
    case LSLibrary::eigen3: {
      // not covered since if Eigen3 is not linked this point is
      // unreachable: LinearSolver throws an exception at construction
      // in this case.
      _eigen3SolveLocalSystem(A, x, b);
      break;
    }
    case LSLibrary::petsc: {
      // not covered since if PETSc is not linked this point is
      // unreachable: LinearSolver throws an exception at construction
      // in this case.
      _petscSolveLocalSystem(A, x, b);
      break;
    }
    default: {
      throw UnexpectedError(::name(m_options.library()) + " cannot solve local systems");
    }
      // LCOV_EXCL_STOP
    }
  }

  LinearSolver();
  LinearSolver(const LinearSolverOptions& options);
};

#endif   // LINEAR_SOLVER_HPP
