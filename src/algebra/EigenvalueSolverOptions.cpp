#include <algebra/EigenvalueSolverOptions.hpp>

#include <rang.hpp>

std::ostream&
operator<<(std::ostream& os, const EigenvalueSolverOptions& options)
{
  os << "  library: " << rang::style::bold << name(options.library()) << rang::style::reset << '\n';
  return os;
}
