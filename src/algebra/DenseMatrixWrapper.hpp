#ifndef DENSE_MATRIX_WRAPPER_HPP
#define DENSE_MATRIX_WRAPPER_HPP

#include <algebra/SmallMatrix.hpp>
#include <algebra/TinyMatrix.hpp>

template <typename DataType>
class DenseMatrixWrapper
{
 private:
  const size_t m_number_of_rows;
  const size_t m_number_of_columns;
  const DataType* const m_matrix_ptr;

 public:
  PUGS_INLINE
  bool
  isSquare() const
  {
    return (m_number_of_rows == m_number_of_columns);
  }

  PUGS_INLINE
  size_t
  numberOfRows() const
  {
    return m_number_of_rows;
  }

  PUGS_INLINE
  size_t
  numberOfColumns() const
  {
    return m_number_of_columns;
  }

  PUGS_INLINE
  const DataType* const&
  ptr() const
  {
    return m_matrix_ptr;
  }

  DenseMatrixWrapper(const SmallMatrix<DataType>& A)
    : m_number_of_rows{A.numberOfRows()},
      m_number_of_columns{A.numberOfColumns()},
      m_matrix_ptr{(m_number_of_columns * m_number_of_rows > 0) ? (&A(0, 0)) : nullptr}
  {}

  template <size_t M, size_t N>
  DenseMatrixWrapper(const TinyMatrix<M, N, DataType>& A)
    : m_number_of_rows{M}, m_number_of_columns{N}, m_matrix_ptr{(M * N > 0) ? (&A(0, 0)) : nullptr}
  {}

  DenseMatrixWrapper(const DenseMatrixWrapper&) = delete;
  DenseMatrixWrapper(DenseMatrixWrapper&&)      = delete;
};

#endif   // DENSE_MATRIX_WRAPPER_HPP
