#include <analysis/QuadratureManager.hpp>
#include <dev/ParallelChecker.hpp>
#include <language/PugsParser.hpp>
#include <language/modules/ModuleRepository.hpp>
#include <mesh/DualConnectivityManager.hpp>
#include <mesh/DualMeshManager.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/SynchronizerManager.hpp>
#include <utils/ExecutionStatManager.hpp>
#include <utils/GlobalVariableManager.hpp>
#include <utils/PugsUtils.hpp>
#include <utils/RandomEngine.hpp>
#include <utils/checkpointing/ResumingManager.hpp>

#include <utils/PluginsLoader.hpp>

int
main(int argc, char* argv[])
{
  ExecutionStatManager::create();
  ResumingManager::create();
  ParallelChecker::create();

  std::string filename = initialize(argc, argv);

  PluginsLoader plugins_loader;

  SynchronizerManager::create();
  RandomEngine::create();
  QuadratureManager::create();
  MeshDataManager::create();
  DualConnectivityManager::create();
  DualMeshManager::create();

  GlobalVariableManager::create();

  parser(filename);
  ExecutionStatManager::printInfo();

  ModuleRepository::destroy();

  GlobalVariableManager::destroy();

  DualMeshManager::destroy();
  DualConnectivityManager::destroy();
  MeshDataManager::destroy();
  QuadratureManager::destroy();
  RandomEngine::destroy();
  SynchronizerManager::destroy();

  finalize();

  ParallelChecker::destroy();
  ResumingManager::destroy();

  int return_code = ExecutionStatManager::getInstance().exitCode();
  ExecutionStatManager::destroy();

  return return_code;
}
