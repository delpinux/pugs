#ifndef I_QUADRATURE_DESCRIPTOR_HPP
#define I_QUADRATURE_DESCRIPTOR_HPP

#include <analysis/QuadratureType.hpp>

#include <string>

class IQuadratureDescriptor
{
 public:
  virtual QuadratureType type() const = 0;
  virtual bool isTensorial() const    = 0;
  virtual size_t degree() const       = 0;
  virtual std::string name() const    = 0;

  IQuadratureDescriptor() noexcept = default;

  IQuadratureDescriptor(const IQuadratureDescriptor&) = default;

  IQuadratureDescriptor(IQuadratureDescriptor&&) noexcept = default;

  virtual ~IQuadratureDescriptor() noexcept = default;
};

#endif   // I_QUADRATURE_DESCRIPTOR_HPP
