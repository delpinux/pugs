#ifndef GAUSS_LOBATTO_QUADRATURE_DESCRIPTOR_HPP
#define GAUSS_LOBATTO_QUADRATURE_DESCRIPTOR_HPP

#include <analysis/IQuadratureDescriptor.hpp>

#include <sstream>

class GaussLobattoQuadratureDescriptor final : public IQuadratureDescriptor
{
 private:
  size_t m_degree;

 public:
  bool
  isTensorial() const
  {
    return true;
  }

  QuadratureType
  type() const
  {
    return QuadratureType::GaussLobatto;
  }

  size_t
  degree() const
  {
    return m_degree;
  }

  std::string
  name() const
  {
    std::ostringstream os;
    os << ::name(this->type()) << "(" << m_degree << ")";
    return os.str();
  }

  GaussLobattoQuadratureDescriptor(size_t degree) : m_degree{degree} {}
  GaussLobattoQuadratureDescriptor() noexcept = delete;

  GaussLobattoQuadratureDescriptor(const GaussLobattoQuadratureDescriptor&) = default;

  GaussLobattoQuadratureDescriptor(GaussLobattoQuadratureDescriptor&&) noexcept = default;

  virtual ~GaussLobattoQuadratureDescriptor() noexcept = default;
};

#endif   // GAUSS_LOBATTO_QUADRATURE_DESCRIPTOR_HPP
