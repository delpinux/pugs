#ifndef TETRAHEDRON_GAUSS_QUADRATURE_HPP
#define TETRAHEDRON_GAUSS_QUADRATURE_HPP

#include <analysis/QuadratureFormula.hpp>

/**
 * Defines Gauss quadrature on the P1 reference tetrahedron element
 *
 * \note formulae are provided by
 *
 * 'High-order symmetric cubature rules for tetrahedra and pyramids'
 * Jan Jaśkowiec & N. Sukumar (2020).
 */
class TetrahedronGaussQuadrature final : public QuadratureFormula<3>
{
 public:
  constexpr static size_t max_degree = 20;

 private:
  void _buildPointAndWeightLists(const size_t degree);

 public:
  TetrahedronGaussQuadrature(TetrahedronGaussQuadrature&&)      = default;
  TetrahedronGaussQuadrature(const TetrahedronGaussQuadrature&) = default;

 private:
  friend class QuadratureManager;

  explicit TetrahedronGaussQuadrature(const size_t degree) : QuadratureFormula<3>(QuadratureType::Gauss)
  {
    this->_buildPointAndWeightLists(degree);
  }

  TetrahedronGaussQuadrature()  = delete;
  ~TetrahedronGaussQuadrature() = default;
};

#endif   // TETRAHEDRON_GAUSS_QUADRATURE_HPP
