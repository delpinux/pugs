#ifndef TENSORIAL_GAUSS_LOBATTO_QUADRATURE_HPP
#define TENSORIAL_GAUSS_LOBATTO_QUADRATURE_HPP

#include <analysis/QuadratureFormula.hpp>

/**
 * Defines Gauss Lobatto quadrature on the reference element
 * \f$]-1,1[^d\f$, where \f$d\f$ denotes the \a Dimension.
 *
 * \note formulae are extracted from High-order Finite Element Method [2004 -  Chapman & Hall]
 */
template <size_t Dimension>
class TensorialGaussLobattoQuadrature final : public QuadratureFormula<Dimension>
{
 public:
  constexpr static size_t max_degree = 13;

 private:
  void _buildPointAndWeightLists(const size_t degree);

 public:
  TensorialGaussLobattoQuadrature(TensorialGaussLobattoQuadrature&&)      = default;
  TensorialGaussLobattoQuadrature(const TensorialGaussLobattoQuadrature&) = default;

 private:
  friend class QuadratureManager;

  template <size_t D>
  friend class TensorialGaussLobattoQuadrature;

  explicit TensorialGaussLobattoQuadrature(const size_t degree) : QuadratureFormula<Dimension>(QuadratureType::Gauss)
  {
    this->_buildPointAndWeightLists(degree);
  }

  TensorialGaussLobattoQuadrature()  = delete;
  ~TensorialGaussLobattoQuadrature() = default;
};

#endif   // TENSORIAL_GAUSS_LOBATTO_QUADRATURE_HPP
