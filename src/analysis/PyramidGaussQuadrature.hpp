#ifndef PYRAMID_GAUSS_QUADRATURE_HPP
#define PYRAMID_GAUSS_QUADRATURE_HPP

#include <analysis/QuadratureFormula.hpp>

/**
 * Defines Gauss quadrature on the reference pyramid element
 *
 * \note formulae are provided by
 *
 * 'High-order symmetric cubature rules for tetrahedra and pyramids'
 * Jan Jaśkowiec & N. Sukumar (2020).
 */
class PyramidGaussQuadrature final : public QuadratureFormula<3>
{
 public:
  constexpr static size_t max_degree = 20;

 private:
  void _buildPointAndWeightLists(const size_t degree);

 public:
  PyramidGaussQuadrature(PyramidGaussQuadrature&&)      = default;
  PyramidGaussQuadrature(const PyramidGaussQuadrature&) = default;

 private:
  friend class QuadratureManager;

  explicit PyramidGaussQuadrature(const size_t degree) : QuadratureFormula<3>(QuadratureType::Gauss)
  {
    this->_buildPointAndWeightLists(degree);
  }

  PyramidGaussQuadrature()  = delete;
  ~PyramidGaussQuadrature() = default;
};

#endif   // PYRAMID_GAUSS_QUADRATURE_HPP
