#include <analysis/TriangleGaussQuadrature.hpp>

#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

void
TriangleGaussQuadrature::_buildPointAndWeightLists(const size_t degree)
{
  using R2 = TinyVector<2>;

  struct Descriptor
  {
    int id;
    double weight;
    std::vector<double> lambda_list;
  };

  auto fill_quadrature_points = [](auto descriptor_list, auto& point_list, auto& weight_list) {
    Assert(point_list.size() == weight_list.size());

    const R2 A{0, 0};
    const R2 B{1, 0};
    const R2 C{0, 1};

    size_t k = 0;
    for (size_t i = 0; i < descriptor_list.size(); ++i) {
      const auto [id, w, position_list] = descriptor_list[i];

      switch (id) {
      case 1: {
        Assert(position_list.size() == 0);

        point_list[k]  = (1. / 3) * (A + B + C);
        weight_list[k] = w;

        k += 1;
        break;
      }
      case 2: {
        Assert(position_list.size() == 1);
        const double& l0 = position_list[0];
        const double& l1 = 1 - 2 * l0;

        point_list[k + 0] = l0 * A + l0 * B + l1 * C;
        point_list[k + 1] = l0 * A + l1 * B + l0 * C;
        point_list[k + 2] = l1 * A + l0 * B + l0 * C;

        for (size_t l = 0; l < 3; ++l) {
          weight_list[k + l] = w;
        }

        k += 3;
        break;
      }
      case 3: {
        Assert(position_list.size() == 2);
        const double& l0 = position_list[0];
        const double& l1 = position_list[1];
        const double& l2 = 1 - l0 - l1;

        point_list[k + 0] = l0 * A + l1 * B + l2 * C;
        point_list[k + 1] = l0 * A + l2 * B + l1 * C;
        point_list[k + 2] = l1 * A + l0 * B + l2 * C;
        point_list[k + 3] = l1 * A + l2 * B + l0 * C;
        point_list[k + 4] = l2 * A + l0 * B + l1 * C;
        point_list[k + 5] = l2 * A + l1 * B + l0 * C;

        for (size_t l = 0; l < 6; ++l) {
          weight_list[k + l] = w;
        }

        k += 6;
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("invalid quadrature id");
      }
        // LCOV_EXCL_STOP
      }
    }

    Assert(k == point_list.size(), "invalid number of quadrature points");
  };

  switch (degree) {
  case 0:
  case 1: {
    constexpr size_t nb_points = 1;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 5.000000000000000e-01, {}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 2: {
    constexpr size_t nb_points = 3;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.666666666666667e-01, {1.666666666666667e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 3:
  case 4: {
    constexpr size_t nb_points = 6;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.116907948390057e-01, {4.459484909159649e-01}},
       Descriptor{2, 5.497587182766094e-02, {9.157621350977074e-02}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);
    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 5: {
    constexpr size_t nb_points = 7;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 1.125000000000000e-01, {}}, Descriptor{2, 6.296959027241357e-02, {1.012865073234563e-01}},
       Descriptor{2, 6.619707639425310e-02, {4.701420641051151e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 6: {
    constexpr size_t nb_points = 12;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 2.542245318510341e-02, {6.308901449150223e-02}},
       Descriptor{2, 5.839313786318968e-02, {2.492867451709104e-01}},
       Descriptor{3, 4.142553780918679e-02, {3.103524510337844e-01, 5.314504984481695e-02}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 7: {
    constexpr size_t nb_points = 15;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 8.272525055396066e-03, {3.373064855458785e-02}},
       Descriptor{2, 6.397208561507779e-02, {2.415773825954036e-01}},
       Descriptor{2, 3.854332309299303e-02, {4.743096925047182e-01}},
       Descriptor{3, 2.793936645159989e-02, {1.986833147973516e-01, 4.703664465259523e-02}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 8: {
    constexpr size_t nb_points = 16;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 7.215780383889359e-02, {}}, Descriptor{2, 4.754581713364231e-02, {4.592925882927232e-01}},
       Descriptor{2, 5.160868526735912e-02, {1.705693077517602e-01}},
       Descriptor{2, 1.622924881159904e-02, {5.054722831703098e-02}},
       Descriptor{3, 1.361515708721750e-02, {2.631128296346381e-01, 8.394777409957605e-03}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 9: {
    constexpr size_t nb_points = 19;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 4.856789814139942e-02, {}},
       Descriptor{2, 3.891377050238714e-02, {4.370895914929366e-01}},
       Descriptor{2, 3.982386946360512e-02, {1.882035356190327e-01}},
       Descriptor{2, 1.566735011356954e-02, {4.896825191987376e-01}},
       Descriptor{2, 1.278883782934902e-02, {4.472951339445271e-02}},
       Descriptor{3, 2.164176968864469e-02, {2.219629891607657e-01, 3.683841205473629e-02}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 10: {
    constexpr size_t nb_points = 25;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 4.087166457314299e-02, {}},
       Descriptor{2, 6.676484406574783e-03, {3.205537321694351e-02}},
       Descriptor{2, 2.297898180237237e-02, {1.421611010565644e-01}},
       Descriptor{3, 3.195245319821202e-02, {1.481328857838206e-01, 3.218129952888354e-01}},
       Descriptor{3, 1.709232408147971e-02, {3.691467818278110e-01, 2.961988948872977e-02}},
       Descriptor{3, 1.264887885364419e-02, {1.637017337371825e-01, 2.836766533993844e-02}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 11: {
    constexpr size_t nb_points = 28;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 4.288058986611211e-02, {}},
       Descriptor{2, 5.215935256447348e-03, {2.848541761437191e-02}},
       Descriptor{2, 3.525784205585829e-02, {2.102199567031783e-01}},
       Descriptor{2, 1.931537961850966e-02, {1.026354827122464e-01}},
       Descriptor{2, 8.303136527292684e-03, {4.958919009658909e-01}},
       Descriptor{2, 3.365807703973415e-02, {4.384659267643522e-01}},
       Descriptor{3, 5.145144786476639e-03, {7.325427686064452e-03, 1.493247886520824e-01}},
       Descriptor{3, 2.016623832025028e-02, {2.895811256377058e-01, 4.601050016542996e-02}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 12: {
    constexpr size_t nb_points = 33;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.213341904072602e-02, {4.882037509455415e-01}},
       Descriptor{2, 1.424302603443877e-02, {1.092578276593543e-01}},
       Descriptor{2, 3.127060659795138e-02, {2.714625070149261e-01}},
       Descriptor{2, 3.965821254986819e-03, {2.464636343633559e-02}},
       Descriptor{2, 2.495916746403047e-02, {4.401116486585931e-01}},
       Descriptor{3, 1.089179251930378e-02, {2.303415635526714e-02, 2.916556797383409e-01}},
       Descriptor{3, 2.161368182970710e-02, {2.554542286385174e-01, 1.162960196779266e-01}},
       Descriptor{3, 7.541838788255719e-03, {2.138249025617059e-02, 8.513377925102400e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 13: {
    constexpr size_t nb_points = 37;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 3.398001829341582e-02, {}},
       Descriptor{2, 1.199720096444737e-02, {4.890769464525394e-01}},
       Descriptor{2, 2.913924255959999e-02, {2.213722862918329e-01}},
       Descriptor{2, 2.780098376522666e-02, {4.269414142598004e-01}},
       Descriptor{2, 3.026168551769586e-03, {2.150968110884318e-02}},
       Descriptor{3, 1.208951990579691e-02, {1.635974010678505e-01, 8.789548303219732e-02}},
       Descriptor{3, 7.482700552582834e-03, {2.437018690109383e-02, 1.109220428034634e-01}},
       Descriptor{3, 1.732063807042419e-02, {6.801224355420665e-02, 3.084417608921178e-01}},
       Descriptor{3, 4.795340501771632e-03, {2.725158177734296e-01, 5.126389102382369e-03}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 14: {
    constexpr size_t nb_points = 42;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 2.108129436849651e-02, {1.772055324125434e-01}},
       Descriptor{2, 1.639417677206267e-02, {4.176447193404539e-01}},
       Descriptor{2, 7.216849834888334e-03, {6.179988309087260e-02}},
       Descriptor{2, 1.094179068471444e-02, {4.889639103621786e-01}},
       Descriptor{2, 2.588705225364579e-02, {2.734775283088386e-01}},
       Descriptor{2, 2.461701801200041e-03, {1.939096124870105e-02}},
       Descriptor{3, 7.218154056766920e-03, {1.464695005565441e-02, 2.983728821362577e-01}},
       Descriptor{3, 1.233287660628184e-02, {1.722666878213556e-01, 5.712475740364794e-02}},
       Descriptor{3, 1.928575539353034e-02, {9.291624935697182e-02, 3.368614597963450e-01}},
       Descriptor{3, 2.505114419250336e-03, {1.189744976969569e-01, 1.268330932872025e-03}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 15: {
    constexpr size_t nb_points = 49;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 2.216769369109204e-02, {}},
       Descriptor{2, 2.135689078573028e-02, {4.053622141339755e-01}},
       Descriptor{2, 8.222368781312581e-03, {7.017355289998602e-02}},
       Descriptor{2, 8.698074000381707e-03, {4.741706814380198e-01}},
       Descriptor{2, 2.339168086435481e-02, {2.263787134203496e-01}},
       Descriptor{2, 4.786923091230043e-03, {4.949969567691262e-01}},
       Descriptor{2, 1.480387318952688e-03, {1.581172625098864e-02}},
       Descriptor{3, 7.801286415287982e-03, {3.146482428124508e-01, 1.837611238568109e-02}},
       Descriptor{3, 2.014926686009050e-03, {7.094860523645553e-02, 9.139237037308396e-03}},
       Descriptor{3, 1.436029346260067e-02, {9.424205359215536e-02, 1.905355894763939e-01}},
       Descriptor{3, 5.836310590787923e-03, {1.863871372816638e-02, 1.680686452224144e-01}},
       Descriptor{3, 1.565773814248464e-02, {9.579672364760859e-02, 3.389506114752772e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 16: {
    constexpr size_t nb_points = 55;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 2.263228303690940e-02, {}},
       Descriptor{2, 2.054646157184948e-02, {2.459900704671417e-01}},
       Descriptor{2, 2.035591665621268e-02, {4.155848968854205e-01}},
       Descriptor{2, 7.390817345112202e-03, {8.535556658670032e-02}},
       Descriptor{2, 1.470920484949405e-02, {1.619186441912712e-01}},
       Descriptor{2, 2.209273156075285e-03, {5.000000000000000e-01}},
       Descriptor{2, 1.298716664913858e-02, {4.752807275459421e-01}},
       Descriptor{3, 9.469136232207850e-03, {1.910747636405291e-01, 5.475517491470312e-02}},
       Descriptor{3, 8.272333574175241e-04, {8.552204200227611e-03, 2.320342776881371e-02}},
       Descriptor{3, 7.504300892142903e-03, {3.317645234741477e-01, 1.893177828040591e-02}},
       Descriptor{3, 3.973796966696249e-03, {8.069616698587292e-02, 1.903012974369745e-02}},
       Descriptor{3, 1.599180503968503e-02, {3.082449691963540e-01, 1.026061902393981e-01}},
       Descriptor{3, 2.695593558424406e-03, {1.874417824837821e-01, 5.936350016822270e-03}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 17: {
    constexpr size_t nb_points = 60;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.365546326405105e-02, {4.171034443615992e-01}},
       Descriptor{2, 1.386943788818821e-03, {1.475549166075395e-02}},
       Descriptor{2, 1.250972547524868e-02, {4.655978716188903e-01}},
       Descriptor{2, 1.315631529400899e-02, {1.803581162663706e-01}},
       Descriptor{2, 6.229500401152721e-03, {6.665406347959693e-02}},
       Descriptor{2, 1.885811857639764e-02, {2.857065024365866e-01}},
       Descriptor{3, 3.989150102964797e-03, {1.591922874727927e-01, 1.601764236211930e-02}},
       Descriptor{3, 1.124388627334553e-02, {6.734937786736120e-02, 3.062815917461865e-01}},
       Descriptor{3, 5.199219977919768e-03, {4.154754592952291e-01, 1.322967276008689e-02}},
       Descriptor{3, 1.027894916022726e-02, {1.687225134952595e-01, 7.804234056828242e-02}},
       Descriptor{3, 4.346107250500596e-03, {2.717918700553548e-01, 1.313587083400269e-02}},
       Descriptor{3, 2.292174200867934e-03, {7.250547079900242e-02, 1.157517590318062e-02}},
       Descriptor{3, 1.308581296766849e-02, {2.992189424769703e-01, 1.575054779268699e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 18: {
    constexpr size_t nb_points = 67;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 1.817786765071333e-02, {}},
       Descriptor{2, 1.665223501669507e-02, {3.999556280675762e-01}},
       Descriptor{2, 6.023323816999855e-03, {4.875803015748695e-01}},
       Descriptor{2, 9.474585753389433e-03, {4.618095064064492e-01}},
       Descriptor{2, 1.823754470447182e-02, {2.422647025142720e-01}},
       Descriptor{2, 3.564663009859485e-03, {3.883025608868559e-02}},
       Descriptor{2, 8.279579976001624e-03, {9.194774212164319e-02}},
       Descriptor{3, 6.879808117471103e-03, {1.838227079254640e-01, 4.580491585986078e-02}},
       Descriptor{3, 1.189095545007642e-02, {1.226967573719275e-01, 2.063492574338379e-01}},
       Descriptor{3, 2.265267251128533e-03, {3.956834343322697e-01, 3.897611033473383e-03}},
       Descriptor{3, 3.420055059803591e-03, {1.081957937910333e-01, 1.346201674144499e-02}},
       Descriptor{3, 8.873744551010202e-03, {3.197516245253774e-01, 4.026028346990806e-02}},
       Descriptor{3, 2.505330437289861e-03, {2.357721849581917e-01, 5.298335186609765e-03}},
       Descriptor{3, 6.114740634805449e-04, {2.709091099516201e-02, 5.483600420423190e-04}},
       Descriptor{3, 1.274108765591222e-02, {3.334935294498808e-01, 1.205876951639246e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 19: {
    constexpr size_t nb_points = 73;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 1.723469885200617e-02, {}},
       Descriptor{2, 3.554628298899065e-03, {5.252389035120897e-02}},
       Descriptor{2, 5.160877571472141e-03, {4.925126750413369e-01}},
       Descriptor{2, 7.617175546509150e-03, {1.114488733230214e-01}},
       Descriptor{2, 1.149179501337080e-02, {4.591942010395437e-01}},
       Descriptor{2, 1.576876744657749e-02, {4.039697225519012e-01}},
       Descriptor{2, 1.232595742409543e-02, {1.781701047817643e-01}},
       Descriptor{2, 8.826613882214238e-04, {1.163946118378945e-02}},
       Descriptor{2, 1.587650968300154e-02, {2.551616329136077e-01}},
       Descriptor{3, 4.847742243427523e-03, {3.914585933169222e-02, 1.306976762680324e-01}},
       Descriptor{3, 1.317316098869537e-02, {1.293125644701578e-01, 3.113176298095413e-01}},
       Descriptor{3, 1.641038275917910e-03, {3.646177809746111e-01, 2.068925896604807e-03}},
       Descriptor{3, 9.053972465606226e-03, {2.214348854323312e-01, 7.456029460162668e-02}},
       Descriptor{3, 1.463157551735100e-03, {1.424257573657563e-01, 5.007288257354491e-03}},
       Descriptor{3, 8.051081382012054e-03, {3.540280097352752e-01, 4.088801119601688e-02}},
       Descriptor{3, 4.227943749768248e-03, {1.492405208198407e-02, 2.418945789605796e-01}},
       Descriptor{3, 1.663600681429694e-03, {9.776025800888155e-03, 6.008627532230670e-02}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 20: {
    constexpr size_t nb_points = 79;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 1.391011070145312e-02, {}},
       Descriptor{2, 1.408320130752025e-02, {2.545792676733391e-01}},
       Descriptor{2, 7.988407910666199e-04, {1.097614102839776e-02}},
       Descriptor{2, 7.830230776074533e-03, {1.093835967117146e-01}},
       Descriptor{2, 9.173462974252915e-03, {1.862949977445409e-01}},
       Descriptor{2, 9.452399933232448e-03, {4.455510569559248e-01}},
       Descriptor{2, 2.161275410665577e-03, {3.731088059888470e-02}},
       Descriptor{2, 1.378805062907046e-02, {3.934253478170999e-01}},
       Descriptor{2, 7.101825303408441e-03, {4.762456115404990e-01}},
       Descriptor{3, 2.202897418558497e-03, {1.591337076570672e-01, 7.570780504696529e-03}},
       Descriptor{3, 5.986398578954690e-03, {1.985181322287882e-01, 4.656036490766432e-02}},
       Descriptor{3, 1.129869602125866e-03, {4.854937607623754e-03, 6.409058560843406e-02}},
       Descriptor{3, 8.667225567219333e-03, {3.331348173095875e-01, 5.498747914298681e-02}},
       Descriptor{3, 4.145711527613858e-03, {3.836368477537459e-02, 9.995229628813866e-02}},
       Descriptor{3, 7.722607822099230e-03, {2.156070573900944e-01, 1.062272047202700e-01}},
       Descriptor{3, 3.695681500255298e-03, {9.831548292802561e-03, 4.200237588162241e-01}},
       Descriptor{3, 1.169174573182774e-02, {1.398080719917999e-01, 3.178601238357720e-01}},
       Descriptor{3, 3.578200238457685e-03, {2.805814114236652e-01, 1.073721285601109e-02}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("Gauss quadrature formulae handle degrees up to " +
                          stringify(TriangleGaussQuadrature::max_degree) + " on triangles");
  }
    // LCOV_EXCL_STOP
  }
}
