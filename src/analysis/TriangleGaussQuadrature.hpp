#ifndef TRIANGLE_GAUSS_QUADRATURE_HPP
#define TRIANGLE_GAUSS_QUADRATURE_HPP

#include <analysis/QuadratureFormula.hpp>

/**
 * Defines Gauss quadrature on the P1 reference triangle
 *
 * \note formulae are provided by
 *
 * ‘On the identification of symmetric quadrature rules for finite
 * element methods‘ by F.D. Witherden & P.E. Vincent (2015).
 */
class TriangleGaussQuadrature final : public QuadratureFormula<2>
{
 public:
  constexpr static size_t max_degree = 20;

 private:
  void _buildPointAndWeightLists(const size_t degree);

 public:
  TriangleGaussQuadrature(TriangleGaussQuadrature&&)      = default;
  TriangleGaussQuadrature(const TriangleGaussQuadrature&) = default;

 private:
  friend class QuadratureManager;
  explicit TriangleGaussQuadrature(const size_t degree) : QuadratureFormula<2>(QuadratureType::Gauss)
  {
    this->_buildPointAndWeightLists(degree);
  }

  TriangleGaussQuadrature()  = delete;
  ~TriangleGaussQuadrature() = default;
};

#endif   // TRIANGLE_GAUSS_QUADRATURE_HPP
