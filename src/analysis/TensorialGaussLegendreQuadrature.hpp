#ifndef TENSORIAL_GAUSS_LEGENDRE_QUADRATURE_HPP
#define TENSORIAL_GAUSS_LEGENDRE_QUADRATURE_HPP

#include <analysis/QuadratureFormula.hpp>

/**
 * Defines Gauss Legendre quadrature on the reference element
 * \f$]-1,1[^d\f$, where \f$d\f$ denotes the \a Dimension.
 *
 * \note formulae are extracted from High-order Finite Element Method [2004 -  Chapman & Hall]
 */
template <size_t Dimension>
class TensorialGaussLegendreQuadrature final : public QuadratureFormula<Dimension>
{
 public:
  constexpr static size_t max_degree = 23;

 private:
  void _buildPointAndWeightLists(const size_t degree);

 public:
  TensorialGaussLegendreQuadrature(TensorialGaussLegendreQuadrature&&)      = default;
  TensorialGaussLegendreQuadrature(const TensorialGaussLegendreQuadrature&) = default;

 private:
  friend class QuadratureManager;

  template <size_t D>
  friend class TensorialGaussLegendreQuadrature;

  explicit TensorialGaussLegendreQuadrature(const size_t degree) : QuadratureFormula<Dimension>(QuadratureType::Gauss)
  {
    this->_buildPointAndWeightLists(degree);
  }

  TensorialGaussLegendreQuadrature()  = delete;
  ~TensorialGaussLegendreQuadrature() = default;
};

#endif   // TENSORIAL_GAUSS_LEGENDRE_QUADRATURE_HPP
