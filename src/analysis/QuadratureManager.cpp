#include <analysis/QuadratureManager.hpp>

#include <analysis/CubeGaussQuadrature.hpp>
#include <analysis/PrismGaussQuadrature.hpp>
#include <analysis/PyramidGaussQuadrature.hpp>
#include <analysis/SquareGaussQuadrature.hpp>
#include <analysis/TensorialGaussLegendreQuadrature.hpp>
#include <analysis/TensorialGaussLobattoQuadrature.hpp>
#include <analysis/TetrahedronGaussQuadrature.hpp>
#include <analysis/TriangleGaussQuadrature.hpp>

QuadratureManager* QuadratureManager::s_instance{nullptr};

void
QuadratureManager::create()
{
  Assert(s_instance == nullptr, "QuadratureManager is already created");
  s_instance = new QuadratureManager;
}

void
QuadratureManager::destroy()
{
  Assert(s_instance != nullptr, "QuadratureManager was not created!");

  delete s_instance;
  s_instance = nullptr;
}

Array<const QuadratureFormula<1>>
QuadratureManager::_buildLineGaussLegendreFormulaList()
{
  Array<QuadratureFormula<1>> line_gauss_legendre_formula_list(TensorialGaussLegendreQuadrature<1>::max_degree / 2 + 1);
  for (size_t i = 0; i < TensorialGaussLegendreQuadrature<1>::max_degree / 2 + 1; ++i) {
    line_gauss_legendre_formula_list[i] = TensorialGaussLegendreQuadrature<1>(2 * i + 1);
  }

  return line_gauss_legendre_formula_list;
}

Array<const QuadratureFormula<1>>
QuadratureManager::_buildLineGaussLobattoFormulaList()
{
  Array<QuadratureFormula<1>> line_gauss_lobatto_formula_list(TensorialGaussLobattoQuadrature<1>::max_degree / 2 + 1);
  for (size_t i = 0; i < TensorialGaussLobattoQuadrature<1>::max_degree / 2 + 1; ++i) {
    line_gauss_lobatto_formula_list[i] = TensorialGaussLobattoQuadrature<1>(2 * i + 1);
  }

  return line_gauss_lobatto_formula_list;
}

Array<const QuadratureFormula<2>>
QuadratureManager::_buildSquareGaussFormulaList()
{
  Array<QuadratureFormula<2>> square_gauss_formula_list(SquareGaussQuadrature::max_degree / 2 + 1);
  for (size_t i = 0; i < SquareGaussQuadrature::max_degree / 2 + 1; ++i) {
    square_gauss_formula_list[i] = SquareGaussQuadrature(2 * i + 1);
  }

  return square_gauss_formula_list;
}

Array<const QuadratureFormula<2>>
QuadratureManager::_buildSquareGaussLegendreFormulaList()
{
  Array<QuadratureFormula<2>> square_gauss_legendre_formula_list(TensorialGaussLegendreQuadrature<2>::max_degree / 2 +
                                                                 1);
  for (size_t i = 0; i < TensorialGaussLegendreQuadrature<2>::max_degree / 2 + 1; ++i) {
    square_gauss_legendre_formula_list[i] = TensorialGaussLegendreQuadrature<2>(2 * i + 1);
  }

  return square_gauss_legendre_formula_list;
}

Array<const QuadratureFormula<2>>
QuadratureManager::_buildSquareGaussLobattoFormulaList()
{
  Array<QuadratureFormula<2>> square_gauss_lobatto_formula_list(TensorialGaussLobattoQuadrature<2>::max_degree / 2 + 1);
  for (size_t i = 0; i < TensorialGaussLobattoQuadrature<2>::max_degree / 2 + 1; ++i) {
    square_gauss_lobatto_formula_list[i] = TensorialGaussLobattoQuadrature<2>(2 * i + 1);
  }

  return square_gauss_lobatto_formula_list;
}

Array<const QuadratureFormula<2>>
QuadratureManager::_buildTriangleGaussFormulaList()
{
  Array<QuadratureFormula<2>> triangle_gauss_formula_list(TriangleGaussQuadrature::max_degree);
  for (size_t i = 0; i < TriangleGaussQuadrature::max_degree; ++i) {
    triangle_gauss_formula_list[i] = TriangleGaussQuadrature(i + 1);
  }

  return triangle_gauss_formula_list;
}

Array<const QuadratureFormula<3>>
QuadratureManager::_buildCubeGaussFormulaList()
{
  Array<QuadratureFormula<3>> cube_gauss_formula_list(CubeGaussQuadrature::max_degree / 2 + 1);
  for (size_t i = 0; i < CubeGaussQuadrature::max_degree / 2 + 1; ++i) {
    cube_gauss_formula_list[i] = CubeGaussQuadrature(2 * i + 1);
  }

  return cube_gauss_formula_list;
}

Array<const QuadratureFormula<3>>
QuadratureManager::_buildCubeGaussLegendreFormulaList()
{
  Array<QuadratureFormula<3>> cube_gauss_legendre_formula_list(TensorialGaussLegendreQuadrature<3>::max_degree / 2 + 1);
  for (size_t i = 0; i < TensorialGaussLegendreQuadrature<3>::max_degree / 2 + 1; ++i) {
    cube_gauss_legendre_formula_list[i] = TensorialGaussLegendreQuadrature<3>(2 * i + 1);
  }

  return cube_gauss_legendre_formula_list;
}

Array<const QuadratureFormula<3>>
QuadratureManager::_buildCubeGaussLobattoFormulaList()
{
  Array<QuadratureFormula<3>> cube_gauss_lobatto_formula_list(TensorialGaussLobattoQuadrature<3>::max_degree / 2 + 1);
  for (size_t i = 0; i < TensorialGaussLobattoQuadrature<3>::max_degree / 2 + 1; ++i) {
    cube_gauss_lobatto_formula_list[i] = TensorialGaussLobattoQuadrature<3>(2 * i + 1);
  }

  return cube_gauss_lobatto_formula_list;
}

Array<const QuadratureFormula<3>>
QuadratureManager::_buildPrismGaussFormulaList()
{
  Array<QuadratureFormula<3>> prism_gauss_formula_list(PrismGaussQuadrature::max_degree);
  for (size_t i = 0; i < PrismGaussQuadrature::max_degree; ++i) {
    prism_gauss_formula_list[i] = PrismGaussQuadrature(i + 1);
  }

  return prism_gauss_formula_list;
}

Array<const QuadratureFormula<3>>
QuadratureManager::_buildPyramidGaussFormulaList()
{
  Array<QuadratureFormula<3>> pyramid_gauss_formula_list(PyramidGaussQuadrature::max_degree);
  for (size_t i = 0; i < PyramidGaussQuadrature::max_degree; ++i) {
    pyramid_gauss_formula_list[i] = PyramidGaussQuadrature(i + 1);
  }

  return pyramid_gauss_formula_list;
}

Array<const QuadratureFormula<3>>
QuadratureManager::_buildTetrahedronGaussFormulaList()
{
  Array<QuadratureFormula<3>> tetrahedron_gauss_formula_list(TetrahedronGaussQuadrature::max_degree);
  for (size_t i = 0; i < TetrahedronGaussQuadrature::max_degree; ++i) {
    tetrahedron_gauss_formula_list[i] = TetrahedronGaussQuadrature(i + 1);
  }

  return tetrahedron_gauss_formula_list;
}

QuadratureManager::QuadratureManager()
  : m_line_gauss_legendre_formula_list{this->_buildLineGaussLegendreFormulaList()},
    m_line_gauss_lobatto_formula_list{this->_buildLineGaussLobattoFormulaList()},
    //
    m_square_gauss_formula_list{this->_buildSquareGaussFormulaList()},
    m_square_gauss_legendre_formula_list{this->_buildSquareGaussLegendreFormulaList()},
    m_square_gauss_lobatto_formula_list{this->_buildSquareGaussLobattoFormulaList()},
    m_triangle_gauss_formula_list{this->_buildTriangleGaussFormulaList()},
    //
    m_cube_gauss_formula_list{this->_buildCubeGaussFormulaList()},
    m_cube_gauss_legendre_formula_list{this->_buildCubeGaussLegendreFormulaList()},
    m_cube_gauss_lobatto_formula_list{this->_buildCubeGaussLobattoFormulaList()},
    m_prism_gauss_formula_list{this->_buildPrismGaussFormulaList()},
    m_pyramid_gauss_formula_list{this->_buildPyramidGaussFormulaList()},
    m_tetrahedron_gauss_formula_list{this->_buildTetrahedronGaussFormulaList()}
{
  Assert(m_line_gauss_legendre_formula_list.size() * 2 - 1 == TensorialGaussLegendreQuadrature<1>::max_degree);
  Assert(m_square_gauss_legendre_formula_list.size() * 2 - 1 == TensorialGaussLegendreQuadrature<2>::max_degree);
  Assert(m_cube_gauss_legendre_formula_list.size() * 2 - 1 == TensorialGaussLegendreQuadrature<3>::max_degree);

  Assert(m_line_gauss_lobatto_formula_list.size() * 2 - 1 == TensorialGaussLobattoQuadrature<1>::max_degree);
  Assert(m_square_gauss_lobatto_formula_list.size() * 2 - 1 == TensorialGaussLobattoQuadrature<2>::max_degree);
  Assert(m_cube_gauss_lobatto_formula_list.size() * 2 - 1 == TensorialGaussLobattoQuadrature<3>::max_degree);

  Assert(m_square_gauss_formula_list.size() * 2 - 1 == SquareGaussQuadrature::max_degree);
  Assert(m_triangle_gauss_formula_list.size() == TriangleGaussQuadrature::max_degree);

  Assert(m_cube_gauss_formula_list.size() * 2 - 1 == CubeGaussQuadrature::max_degree);
  Assert(m_prism_gauss_formula_list.size() == PrismGaussQuadrature::max_degree);
  Assert(m_pyramid_gauss_formula_list.size() == PyramidGaussQuadrature::max_degree);
  Assert(m_tetrahedron_gauss_formula_list.size() == TetrahedronGaussQuadrature::max_degree);
}
