#ifndef QUADRATURE_TYPE_HPP
#define QUADRATURE_TYPE_HPP

#include <utils/Exceptions.hpp>
#include <utils/PugsMacros.hpp>

#include <string>

enum class QuadratureType
{
  Gauss         = 0,
  GaussLegendre = 1,
  GaussLobatto  = 2,
};

PUGS_INLINE
std::string
name(QuadratureType type)
{
  switch (type) {
  case QuadratureType::Gauss: {
    return "Gauss";
  }
  case QuadratureType::GaussLegendre: {
    return "Gauss-Legendre";
  }
  case QuadratureType::GaussLobatto: {
    return "Gauss-Lobatto";
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("unknown quadrature type name");
  }
    // LCOV_EXCL_STOP
  }
}

#endif   // QUADRATURE_TYPE_HPP
