#ifndef SQUARE_GAUSS_QUADRATURE_HPP
#define SQUARE_GAUSS_QUADRATURE_HPP

#include <analysis/QuadratureFormula.hpp>

/**
 * Defines Square Gauss quadrature on the reference element
 * \f$]-1,1[^2\f$
 *
 * \note formulae are provided by
 *
 * ‘On the identification of symmetric quadrature rules for finite
 * element methods‘ by F.D. Witherden & P.E. Vincent (2015).
 */
class SquareGaussQuadrature final : public QuadratureFormula<2>
{
 public:
  constexpr static size_t max_degree = 21;

 private:
  void _buildPointAndWeightLists(const size_t degree);

 public:
  SquareGaussQuadrature(SquareGaussQuadrature&&)      = default;
  SquareGaussQuadrature(const SquareGaussQuadrature&) = default;

 private:
  friend class QuadratureManager;

  explicit SquareGaussQuadrature(const size_t degree) : QuadratureFormula<2>(QuadratureType::Gauss)
  {
    this->_buildPointAndWeightLists(degree);
  }

  SquareGaussQuadrature()  = delete;
  ~SquareGaussQuadrature() = default;
};

#endif   // SQUARE_GAUSS_QUADRATURE_HPP
