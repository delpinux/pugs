#include <analysis/CubeGaussQuadrature.hpp>

#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

void
CubeGaussQuadrature::_buildPointAndWeightLists(const size_t degree)
{
  using R3 = TinyVector<3>;

  struct Descriptor
  {
    int id;
    double weight;
    std::vector<double> lambda_list;
  };

  auto fill_quadrature_points = [](auto descriptor_list, auto& point_list, auto& weight_list) {
    Assert(point_list.size() == weight_list.size());

    size_t k = 0;
    for (size_t i = 0; i < descriptor_list.size(); ++i) {
      const auto [id, unit_weight, position_list] = descriptor_list[i];

      const double w = 8. * unit_weight;

      switch (id) {
      case 1: {
        Assert(position_list.size() == 0);
        point_list[k]  = R3{0, 0, 0};
        weight_list[k] = w;
        ++k;
        break;
      }
      case 2: {
        Assert(position_list.size() == 1);
        const double a = position_list[0];

        point_list[k + 0] = R3{+a, 0, 0};
        point_list[k + 1] = R3{-a, 0, 0};
        point_list[k + 2] = R3{0, +a, 0};
        point_list[k + 3] = R3{0, -a, 0};
        point_list[k + 4] = R3{0, 0, +a};
        point_list[k + 5] = R3{0, 0, -a};

        for (size_t l = 0; l < 6; ++l) {
          weight_list[k + l] = w;
        }

        k += 6;
        break;
      }
      case 3: {
        Assert(position_list.size() == 1);
        const double a = position_list[0];

        point_list[k + 0] = R3{+a, +a, +a};
        point_list[k + 1] = R3{+a, +a, -a};
        point_list[k + 2] = R3{+a, -a, +a};
        point_list[k + 3] = R3{+a, -a, -a};
        point_list[k + 4] = R3{-a, +a, +a};
        point_list[k + 5] = R3{-a, +a, -a};
        point_list[k + 6] = R3{-a, -a, +a};
        point_list[k + 7] = R3{-a, -a, -a};

        for (size_t l = 0; l < 8; ++l) {
          weight_list[k + l] = w;
        }

        k += 8;
        break;
      }
      case 4: {
        Assert(position_list.size() == 1);
        const double a = position_list[0];

        point_list[k + 0]  = R3{+a, +a, 0};
        point_list[k + 1]  = R3{+a, -a, 0};
        point_list[k + 2]  = R3{-a, +a, 0};
        point_list[k + 3]  = R3{-a, -a, 0};
        point_list[k + 4]  = R3{+a, 0, +a};
        point_list[k + 5]  = R3{+a, 0, -a};
        point_list[k + 6]  = R3{-a, 0, +a};
        point_list[k + 7]  = R3{-a, 0, -a};
        point_list[k + 8]  = R3{0, +a, +a};
        point_list[k + 9]  = R3{0, +a, -a};
        point_list[k + 10] = R3{0, -a, +a};
        point_list[k + 11] = R3{0, -a, -a};

        for (size_t l = 0; l < 12; ++l) {
          weight_list[k + l] = w;
        }

        k += 12;
        break;
      }
      case 5: {
        Assert(position_list.size() == 2);
        const double a = position_list[0];
        const double b = position_list[1];

        point_list[k + 0]  = R3{+a, +b, 0};
        point_list[k + 1]  = R3{+a, -b, 0};
        point_list[k + 2]  = R3{-a, +b, 0};
        point_list[k + 3]  = R3{-a, -b, 0};
        point_list[k + 4]  = R3{+b, +a, 0};
        point_list[k + 5]  = R3{-b, +a, 0};
        point_list[k + 6]  = R3{+b, -a, 0};
        point_list[k + 7]  = R3{-b, -a, 0};
        point_list[k + 8]  = R3{+a, 0, +b};
        point_list[k + 9]  = R3{+a, 0, -b};
        point_list[k + 10] = R3{-a, 0, +b};
        point_list[k + 11] = R3{-a, 0, -b};
        point_list[k + 12] = R3{+b, 0, +a};
        point_list[k + 13] = R3{-b, 0, +a};
        point_list[k + 14] = R3{+b, 0, -a};
        point_list[k + 15] = R3{-b, 0, -a};
        point_list[k + 16] = R3{0, +a, +b};
        point_list[k + 17] = R3{0, +a, -b};
        point_list[k + 18] = R3{0, -a, +b};
        point_list[k + 19] = R3{0, -a, -b};
        point_list[k + 20] = R3{0, +b, +a};
        point_list[k + 21] = R3{0, -b, +a};
        point_list[k + 22] = R3{0, +b, -a};
        point_list[k + 23] = R3{0, -b, -a};

        for (size_t l = 0; l < 24; ++l) {
          weight_list[k + l] = w;
        }

        k += 24;
        break;
      }
      case 6: {
        Assert(position_list.size() == 2);
        const double a = position_list[0];
        const double b = position_list[1];

        point_list[k + 0]  = R3{+a, +a, +b};
        point_list[k + 1]  = R3{+a, +a, -b};
        point_list[k + 2]  = R3{+a, -a, +b};
        point_list[k + 3]  = R3{+a, -a, -b};
        point_list[k + 4]  = R3{-a, +a, +b};
        point_list[k + 5]  = R3{-a, +a, -b};
        point_list[k + 6]  = R3{-a, -a, +b};
        point_list[k + 7]  = R3{-a, -a, -b};
        point_list[k + 8]  = R3{+a, +b, +a};
        point_list[k + 9]  = R3{+a, -b, +a};
        point_list[k + 10] = R3{+a, +b, -a};
        point_list[k + 11] = R3{+a, -b, -a};
        point_list[k + 12] = R3{-a, +b, +a};
        point_list[k + 13] = R3{-a, -b, +a};
        point_list[k + 14] = R3{-a, +b, -a};
        point_list[k + 15] = R3{-a, -b, -a};
        point_list[k + 16] = R3{+b, +a, +a};
        point_list[k + 17] = R3{-b, +a, +a};
        point_list[k + 18] = R3{+b, +a, -a};
        point_list[k + 19] = R3{-b, +a, -a};
        point_list[k + 20] = R3{+b, -a, +a};
        point_list[k + 21] = R3{-b, -a, +a};
        point_list[k + 22] = R3{+b, -a, -a};
        point_list[k + 23] = R3{-b, -a, -a};

        for (size_t l = 0; l < 24; ++l) {
          weight_list[k + l] = w;
        }

        k += 24;
        break;
      }
      case 7: {
        Assert(position_list.size() == 3);
        const double a = position_list[0];
        const double b = position_list[1];
        const double c = position_list[2];

        point_list[k + 0]  = R3{+a, +b, +c};
        point_list[k + 1]  = R3{+a, +b, -c};
        point_list[k + 2]  = R3{+a, -b, +c};
        point_list[k + 3]  = R3{+a, -b, -c};
        point_list[k + 4]  = R3{-a, +b, +c};
        point_list[k + 5]  = R3{-a, +b, -c};
        point_list[k + 6]  = R3{-a, -b, +c};
        point_list[k + 7]  = R3{-a, -b, -c};
        point_list[k + 8]  = R3{+a, +c, +b};
        point_list[k + 9]  = R3{+a, -c, +b};
        point_list[k + 10] = R3{+a, +c, -b};
        point_list[k + 11] = R3{+a, -c, -b};
        point_list[k + 12] = R3{-a, +c, +b};
        point_list[k + 13] = R3{-a, -c, +b};
        point_list[k + 14] = R3{-a, +c, -b};
        point_list[k + 15] = R3{-a, -c, -b};
        point_list[k + 16] = R3{+b, +a, +c};
        point_list[k + 17] = R3{+b, +a, -c};
        point_list[k + 18] = R3{-b, +a, +c};
        point_list[k + 19] = R3{-b, +a, -c};
        point_list[k + 20] = R3{+b, -a, +c};
        point_list[k + 21] = R3{+b, -a, -c};
        point_list[k + 22] = R3{-b, -a, +c};
        point_list[k + 23] = R3{-b, -a, -c};
        point_list[k + 24] = R3{+b, +c, +a};
        point_list[k + 25] = R3{+b, -c, +a};
        point_list[k + 26] = R3{-b, +c, +a};
        point_list[k + 27] = R3{-b, -c, +a};
        point_list[k + 28] = R3{+b, +c, -a};
        point_list[k + 29] = R3{+b, -c, -a};
        point_list[k + 30] = R3{-b, +c, -a};
        point_list[k + 31] = R3{-b, -c, -a};
        point_list[k + 32] = R3{+c, +a, +b};
        point_list[k + 33] = R3{-c, +a, +b};
        point_list[k + 34] = R3{+c, +a, -b};
        point_list[k + 35] = R3{-c, +a, -b};
        point_list[k + 36] = R3{+c, -a, +b};
        point_list[k + 37] = R3{-c, -a, +b};
        point_list[k + 38] = R3{+c, -a, -b};
        point_list[k + 39] = R3{-c, -a, -b};
        point_list[k + 40] = R3{+c, +b, +a};
        point_list[k + 41] = R3{-c, +b, +a};
        point_list[k + 42] = R3{+c, -b, +a};
        point_list[k + 43] = R3{-c, -b, +a};
        point_list[k + 44] = R3{+c, +b, -a};
        point_list[k + 45] = R3{-c, +b, -a};
        point_list[k + 46] = R3{+c, -b, -a};
        point_list[k + 47] = R3{-c, -b, -a};

        for (size_t l = 0; l < 48; ++l) {
          weight_list[k + l] = w;
        }

        k += 48;
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("invalid quadrature id");
      }
        // LCOV_EXCL_STOP
      }
    }
  };

  switch (degree) {
  case 0:
  case 1: {
    constexpr size_t nb_points = 1;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 1.000000000000000e+00, {}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 2:
  case 3: {
    constexpr size_t nb_points = 6;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.666666666666667e-01, {+1.000000000000000e+00}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 4:
  case 5: {
    constexpr size_t nb_points = 14;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.108033240997230e-01, {-7.958224257542215e-01}},
       Descriptor{3, 4.189750692520776e-02, {+7.587869106393281e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 6:
  case 7: {
    constexpr size_t nb_points = 34;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 3.400458474980031e-02, {-9.388053721060176e-01}},
       Descriptor{3, 2.529672440135186e-02, {+7.463336100128160e-01}},
       Descriptor{3, 5.417063533485642e-02, {+4.101308983320144e-01}},
       Descriptor{4, 1.335280113429432e-02, {+9.064606901228371e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 8:
  case 9: {
    constexpr size_t nb_points = 58;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 5.415937446870682e-02, {-6.136814695917090e-01}},
       Descriptor{3, 6.268599412418628e-03, {+8.700997846619759e-01}},
       Descriptor{3, 2.485747976800294e-02, {+5.641108070200300e-01}},
       Descriptor{4, 1.147372576702221e-02, {+8.776871232576783e-01}},
       Descriptor{6, 1.201460043917167e-02, {+4.322679026308622e-01, +9.385304218646717e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 10:
  case 11: {
    constexpr size_t nb_points = 90;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 2.961692452402056e-02, {+7.221330388744185e-01}},
       Descriptor{3, 7.751183787523073e-03, {+8.094882019630989e-01}},
       Descriptor{3, 2.222100810905048e-02, {-5.336540088804971e-01}},
       Descriptor{3, 1.698870214455200e-02, {+2.807725866512744e-01}},
       Descriptor{4, 1.602728177693560e-02, {+8.039334672152845e-01}},
       Descriptor{6, 1.618821190032323e-03, {+9.800994910090715e-01, -5.307838311938264e-01}},
       Descriptor{6, 8.976342110119554e-03, {+4.056859801950964e-01, +9.545832189295661e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 12:
  case 13: {
    constexpr size_t nb_points = 154;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.739128248747042e-02, {-6.344099893707986e-01}},
       Descriptor{3, 1.838657780061070e-03, {+9.078540479628353e-01}},
       Descriptor{3, 1.129102483142921e-02, {-2.362052584516462e-01}},
       Descriptor{4, 5.975759635289482e-03, {+7.375746343132366e-01}},
       Descriptor{5, 8.691231524417118e-03, {+4.607161476964481e-01, +9.333452030108366e-01}},
       Descriptor{6, 1.061455871034256e-02, {+3.733841515799997e-01, +6.642163895931538e-01}},
       Descriptor{6, 1.690358916666084e-03, {+9.495110814642814e-01, +2.576763878569212e-01}},
       Descriptor{6, 2.284240552839863e-03, {+6.656694827601780e-01, +9.948700428018972e-01}},
       Descriptor{6, 6.674015652391932e-03, {-8.051009105032320e-01, -4.521405059548300e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 14:
  case 15: {
    constexpr size_t nb_points = 256;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 3.340305010694517e-03, {+9.239155149504112e-01}},
       Descriptor{2, 5.018552337877279e-03, {-2.173948613666498e-01}},
       Descriptor{3, 9.082758817224784e-03, {+3.384731869494254e-01}},
       Descriptor{3, 2.936408738387597e-03, {+7.930891310393379e-01}},
       Descriptor{4, 9.318238647048784e-03, {+6.965519027266717e-01}},
       Descriptor{5, 6.990786235751096e-03, {+2.642667592628472e-01, +5.849899486873243e-01}},
       Descriptor{5, 2.841005747490632e-03, {+5.529505913370675e-01, +9.828048727801773e-01}},
       Descriptor{6, 5.309574072330840e-03, {+2.720325890860906e-01, +8.780894076733160e-01}},
       Descriptor{6, 3.570894536567292e-03, {+6.186478600855522e-01, +9.399813353075436e-01}},
       Descriptor{6, 3.505872179585672e-03, {+8.631544625530380e-01, +3.042225951752605e-01}},
       Descriptor{6, 6.665199051138311e-03, {+4.616057726468051e-01, +7.371361183496182e-01}},
       Descriptor{6, 7.781518386120085e-04, {+9.603141805416257e-01, +7.670146080341008e-01}},
       Descriptor{7, 6.249800796596737e-04, {+2.802518397917547e-01, +8.885938329579797e-01, +9.942601945848643e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 16:
  case 17: {
    constexpr size_t nb_points = 346;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 9.614592128929721e-03, {+4.853150302302675e-01}},
       Descriptor{3, 4.393827602159037e-03, {+7.406788158740985e-01}},
       Descriptor{3, 4.964725194523337e-05, {+9.999995835822517e-01}},
       Descriptor{4, 2.506284464832531e-03, {+7.546268052131762e-01}},
       Descriptor{4, 6.133232482070283e-03, {+5.496818141686911e-01}},
       Descriptor{4, 3.052513689971173e-04, {+9.996685037422811e-01}},
       Descriptor{5, 2.825437733033075e-03, {+6.157290991684734e-01, +9.530419407049950e-01}},
       Descriptor{5, 4.817704598799924e-03, {+2.756812731993550e-01, +8.171240681070916e-01}},
       Descriptor{6, 2.283142338953617e-03, {+2.553429540372821e-01, +9.697740446619386e-01}},
       Descriptor{6, 1.177437487278679e-03, {+9.352363848980443e-01, +7.467233074183380e-01}},
       Descriptor{6, 6.184060750627916e-03, {+3.430591461388653e-01, +6.186639578085585e-01}},
       Descriptor{6, 2.674655837593652e-03, {+2.555841601150721e-01, -1.540702494043778e-01}},
       Descriptor{6, 3.419177677578799e-03, {+5.416211416468879e-01, +8.956569517854811e-01}},
       Descriptor{6, 2.334617556615003e-03, {+8.874561091440486e-01, +2.414649778934788e-01}},
       Descriptor{7, 1.052790164887146e-03, {+8.014733742030056e-01, +9.887847181449660e-01, +4.683593080344387e-01}},
       Descriptor{7, 2.743830940763945e-03, {+6.099140660050306e-01, +7.868230999431114e-01, +3.205108203811766e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 18:
  case 19: {
    constexpr size_t nb_points = 454;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 2.057371005731085e-03, {+2.357950388975374e-01}},
       Descriptor{3, 9.660877915728369e-04, {+8.884950400440639e-01}},
       Descriptor{3, 1.635901446799172e-03, {+6.691818469930052e-01}},
       Descriptor{5, 8.499912813105116e-04, {+7.123100397018546e-01, +9.982977792850621e-01}},
       Descriptor{5, 3.595003721736422e-03, {+3.997141162742279e-01, +1.720241294475643e-01}},
       Descriptor{5, 3.836063429622649e-03, {+8.765805264721680e-01, -4.319090088935915e-01}},
       Descriptor{6, 5.044128485798918e-03, {-3.101731974045544e-01, +5.411043641351523e-01}},
       Descriptor{6, 1.951830532879285e-04, {+9.841852549757261e-01, +8.206649948892372e-01}},
       Descriptor{6, 1.758454840661237e-03, {+8.443872335315854e-01, -1.473766326322086e-01}},
       Descriptor{6, 3.887320262932200e-03, {+4.875993998852213e-01, +7.674914745033637e-01}},
       Descriptor{6, 1.887401083675721e-03, {+7.641318497967444e-01, +3.863910365012072e-01}},
       Descriptor{6, 1.699165183521457e-03, {+7.112537388224306e-01, +9.032012949929131e-01}},
       Descriptor{6, 7.588378288377641e-04, {+9.551489495939087e-01, +2.226160644570115e-01}},
       Descriptor{6, 1.455762954103218e-03, {+1.696919803869971e-01, +9.651628675592381e-01}},
       Descriptor{6, 4.007905075757146e-03, {+6.085335258834057e-01, +9.412726803541549e-02}},
       Descriptor{6, 1.399186075584291e-03, {+4.265517787687217e-01, +9.784781131096041e-01}},
       Descriptor{6, 4.099280788926685e-03, {+1.836920702608942e-01, +7.457754226757105e-01}},
       Descriptor{7, 1.951861257384991e-03, {+6.535330886596633e-01, +9.133523126050157e-01, +2.888629020786040e-01}},
       Descriptor{7, 9.537937942918817e-04, {+8.475635090984786e-01, +9.748482139426561e-01, +5.534277540813458e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 20:
  case 21: {
    constexpr size_t nb_points = 580;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 3.007917561622623e-03, {-6.174957125232848e-01}},
       Descriptor{2, 6.157551855986320e-03, {-2.913675401437903e-01}},
       Descriptor{3, 7.451864938264940e-04, {+8.622583182294242e-01}},
       Descriptor{3, 3.473649875824585e-03, {+5.666798116095041e-01}},
       Descriptor{5, 5.231065457280251e-04, {+7.962288576876380e-01, +9.999999871114571e-01}},
       Descriptor{5, 4.721261422431887e-03, {+6.935661772320008e-01, +3.463806774517028e-01}},
       Descriptor{5, 2.337222373763527e-03, {+8.718362020420203e-01, -6.252215000307171e-01}},
       Descriptor{6, 3.841754898565957e-03, {-4.164545409594995e-01, +8.053481425029153e-01}},
       Descriptor{6, 1.507767719634259e-04, {+9.738519593091541e-01, +8.745003988374457e-01}},
       Descriptor{6, 3.214079409518292e-03, {+7.290234505608810e-01, +2.451675433295293e-01}},
       Descriptor{6, 2.245446622613030e-03, {+6.496117102813809e-01, +8.441031264282526e-01}},
       Descriptor{6, 1.041367673151780e-03, {+8.919707516952021e-01, +1.301935885566839e-01}},
       Descriptor{6, 5.920000965024342e-04, {+7.833271437290619e-01, +9.532187072534166e-01}},
       Descriptor{6, 4.100172337698149e-04, {+9.699411873288849e-01, +2.710159328732420e-01}},
       Descriptor{6, 7.465850926621290e-04, {+1.720339772207737e-01, +9.847690037797490e-01}},
       Descriptor{6, 3.743289683970358e-03, {+5.380985926191851e-01, -2.207631346215846e-01}},
       Descriptor{6, 4.534357504253366e-04, {+5.536849035893876e-01, +9.989728978231427e-01}},
       Descriptor{6, 4.182867578178122e-03, {+2.235580139510551e-01, +4.418577456047065e-01}},
       Descriptor{6, 7.389797381822837e-04, {+8.700625815926416e-01, +4.988147208937697e-01}},
       Descriptor{7, 1.371282553387760e-03, {+7.501439986013229e-01, +9.439853549264192e-01, +3.972114022576276e-01}},
       Descriptor{7, 3.675472168789246e-04, {+9.025858122958752e-01, +9.846237882037482e-01, +6.307126444914427e-01}},
       Descriptor{7, 1.258019483515844e-03, {+2.217794596478607e-01, +9.822849317288322e-02, +8.721988578583585e-01}},
       Descriptor{7, 1.516565561694638e-03, {+9.568422563542535e-01, +2.081392891496346e-01, +4.958409266028638e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("Gauss quadrature formulae handle degrees up to " +
                          stringify(CubeGaussQuadrature::max_degree) + " on cubes");
  }
    // LCOV_EXCL_STOP
  }
}
