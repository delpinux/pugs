#include <analysis/TetrahedronGaussQuadrature.hpp>

#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

void
TetrahedronGaussQuadrature::_buildPointAndWeightLists(const size_t degree)
{
  using R3 = TinyVector<3>;

  struct Descriptor
  {
    int id;
    double weight;
    std::vector<double> lambda_list;
  };

  auto fill_quadrature_points = [](auto descriptor_list, auto& point_list, auto& weight_list) {
    Assert(point_list.size() == weight_list.size());

    const R3 A{0, 0, 0};
    const R3 B{1, 0, 0};
    const R3 C{0, 1, 0};
    const R3 D{0, 0, 1};

    size_t k = 0;
    for (size_t i = 0; i < descriptor_list.size(); ++i) {
      const auto [id, unit_weight, lambda_list] = descriptor_list[i];

      const double w = (1. / 6) * unit_weight;

      switch (id) {
      case 1: {
        Assert(lambda_list.size() == 0);
        point_list[k]  = (1. / 4) * (A + B + C + D);
        weight_list[k] = w;
        ++k;
        break;
      }
      case 2: {
        Assert(lambda_list.size() == 1);
        const double l0 = lambda_list[0];
        const double l1 = 1 - 3 * l0;

        point_list[k + 0] = l0 * A + l0 * B + l0 * C + l1 * D;
        point_list[k + 1] = l0 * A + l0 * B + l1 * C + l0 * D;
        point_list[k + 2] = l0 * A + l1 * B + l0 * C + l0 * D;
        point_list[k + 3] = l1 * A + l0 * B + l0 * C + l0 * D;

        for (size_t l = 0; l < 4; ++l) {
          weight_list[k + l] = w;
        }

        k += 4;
        break;
      }
      case 3: {
        Assert(lambda_list.size() == 1);
        const double l0 = lambda_list[0];
        const double l1 = 0.5 - l0;

        point_list[k + 0] = l0 * A + l0 * B + l1 * C + l1 * D;
        point_list[k + 1] = l0 * A + l1 * B + l0 * C + l1 * D;
        point_list[k + 2] = l0 * A + l1 * B + l1 * C + l0 * D;
        point_list[k + 3] = l1 * A + l0 * B + l0 * C + l1 * D;
        point_list[k + 4] = l1 * A + l0 * B + l1 * C + l0 * D;
        point_list[k + 5] = l1 * A + l1 * B + l0 * C + l0 * D;

        for (size_t l = 0; l < 6; ++l) {
          weight_list[k + l] = w;
        }

        k += 6;
        break;
      }
      case 4: {
        Assert(lambda_list.size() == 2);
        const double l0 = lambda_list[0];
        const double l1 = lambda_list[1];
        const double l2 = 1 - 2 * l0 - l1;

        point_list[k + 0]  = l0 * A + l0 * B + l1 * C + l2 * D;
        point_list[k + 1]  = l0 * A + l0 * B + l2 * C + l1 * D;
        point_list[k + 2]  = l0 * A + l1 * B + l0 * C + l2 * D;
        point_list[k + 3]  = l0 * A + l1 * B + l2 * C + l0 * D;
        point_list[k + 4]  = l0 * A + l2 * B + l0 * C + l1 * D;
        point_list[k + 5]  = l0 * A + l2 * B + l1 * C + l0 * D;
        point_list[k + 6]  = l1 * A + l0 * B + l0 * C + l2 * D;
        point_list[k + 7]  = l1 * A + l0 * B + l2 * C + l0 * D;
        point_list[k + 8]  = l1 * A + l2 * B + l0 * C + l0 * D;
        point_list[k + 9]  = l2 * A + l0 * B + l0 * C + l1 * D;
        point_list[k + 10] = l2 * A + l0 * B + l1 * C + l0 * D;
        point_list[k + 11] = l2 * A + l1 * B + l0 * C + l0 * D;

        for (size_t l = 0; l < 12; ++l) {
          weight_list[k + l] = w;
        }

        k += 12;
        break;
      }
      case 5: {
        Assert(lambda_list.size() == 3);
        const double l0 = lambda_list[0];
        const double l1 = lambda_list[1];
        const double l2 = lambda_list[2];
        const double l3 = 1 - l0 - l1 - l2;

        point_list[k + 0]  = l0 * A + l1 * B + l2 * C + l3 * D;
        point_list[k + 1]  = l0 * A + l1 * B + l3 * C + l2 * D;
        point_list[k + 2]  = l0 * A + l2 * B + l1 * C + l3 * D;
        point_list[k + 3]  = l0 * A + l2 * B + l3 * C + l1 * D;
        point_list[k + 4]  = l0 * A + l3 * B + l1 * C + l2 * D;
        point_list[k + 5]  = l0 * A + l3 * B + l2 * C + l1 * D;
        point_list[k + 6]  = l1 * A + l0 * B + l2 * C + l3 * D;
        point_list[k + 7]  = l1 * A + l0 * B + l3 * C + l2 * D;
        point_list[k + 8]  = l1 * A + l2 * B + l0 * C + l3 * D;
        point_list[k + 9]  = l1 * A + l2 * B + l3 * C + l0 * D;
        point_list[k + 10] = l1 * A + l3 * B + l0 * C + l2 * D;
        point_list[k + 11] = l1 * A + l3 * B + l2 * C + l0 * D;
        point_list[k + 12] = l2 * A + l0 * B + l1 * C + l3 * D;
        point_list[k + 13] = l2 * A + l0 * B + l3 * C + l1 * D;
        point_list[k + 14] = l2 * A + l1 * B + l0 * C + l3 * D;
        point_list[k + 15] = l2 * A + l1 * B + l3 * C + l0 * D;
        point_list[k + 16] = l2 * A + l3 * B + l0 * C + l1 * D;
        point_list[k + 17] = l2 * A + l3 * B + l1 * C + l0 * D;
        point_list[k + 18] = l3 * A + l0 * B + l1 * C + l2 * D;
        point_list[k + 19] = l3 * A + l0 * B + l2 * C + l1 * D;
        point_list[k + 20] = l3 * A + l1 * B + l0 * C + l2 * D;
        point_list[k + 21] = l3 * A + l1 * B + l2 * C + l0 * D;
        point_list[k + 22] = l3 * A + l2 * B + l0 * C + l1 * D;
        point_list[k + 23] = l3 * A + l2 * B + l1 * C + l0 * D;

        for (size_t l = 0; l < 24; ++l) {
          weight_list[k + l] = w;
        }

        k += 24;
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("invalid quadrature id");
      }
        // LCOV_EXCL_STOP
      }
    }
  };

  switch (degree) {
  case 0:
  case 1: {
    constexpr size_t nb_points = 1;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 1.000000000000000e+00, {}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 2: {
    constexpr size_t nb_points = 4;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 2.500000000000000e-01, {1.381966011250105e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 3: {
    constexpr size_t nb_points = 8;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.274913115575064e-01, {3.286811466653490e-01}},
       Descriptor{2, 1.225086884424936e-01, {1.119207275092916e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 4: {
    constexpr size_t nb_points = 14;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 6.540916363445218e-02, {3.095821653179519e-01}},
       Descriptor{2, 5.935526423100475e-02, {8.344277230397758e-02}},
       Descriptor{3, 8.349038142302871e-02, {4.227790459299413e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 5: {
    constexpr size_t nb_points = 14;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.126879257180158e-01, {3.108859192633006e-01}},
       Descriptor{2, 7.349304311636196e-02, {9.273525031089122e-02}},
       Descriptor{3, 4.254602077708147e-02, {4.544962958743504e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 6: {
    constexpr size_t nb_points = 24;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.007721105532064e-02, {4.067395853461135e-02}},
       Descriptor{2, 5.535718154365472e-02, {3.223378901422755e-01}},
       Descriptor{2, 3.992275025816749e-02, {2.146028712591520e-01}},
       Descriptor{4, 4.821428571428572e-02, {6.366100187501753e-02, 6.030056647916492e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 7: {
    constexpr size_t nb_points = 35;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 9.548528946413085e-02, {}}, Descriptor{2, 4.232958120996703e-02, {3.157011497782028e-01}},
       Descriptor{3, 3.189692783285758e-02, {4.495101774016036e-01}},
       Descriptor{4, 8.110770829903342e-03, {2.126547254148325e-02, 8.108302410985485e-01}},
       Descriptor{4, 3.720713072833462e-02, {1.888338310260010e-01, 5.751716375870001e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 8: {
    constexpr size_t nb_points = 46;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 4.781298803430666e-02, {1.868127582707210e-01}},
       Descriptor{2, 2.972785408679422e-02, {1.144624067612305e-01}},
       Descriptor{2, 4.352732421897527e-02, {3.138065922724016e-01}},
       Descriptor{2, 8.632736020814975e-03, {4.457737944884625e-02}},
       Descriptor{3, 3.689054126986636e-02, {6.548349054384239e-02}},
       Descriptor{4, 1.449702671085950e-02, {2.038931746621103e-01, 5.073320750421590e-03}},
       Descriptor{4, 7.157401867243608e-03, {2.124777966957167e-02, 7.146769263933049e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 9: {
    constexpr size_t nb_points = 59;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 5.686662425355173e-02, {}},
       Descriptor{2, 2.569427668952318e-02, {1.679066052367428e-01}},
       Descriptor{2, 2.298923353028370e-03, {9.143627051407625e-02}},
       Descriptor{2, 3.045603866759101e-02, {3.218556648176533e-01}},
       Descriptor{2, 7.123722340238881e-03, {4.183769590036560e-02}},
       Descriptor{3, 3.684365548094388e-02, {1.067294385748464e-01}},
       Descriptor{4, 1.032205678220985e-02, {3.395716818308889e-02, 7.183930939814244e-01}},
       Descriptor{4, 7.634887153980855e-03, {4.606581810547776e-01, 7.868363447668793e-02}},
       Descriptor{4, 2.035802261874757e-02, {1.843879435000152e-01, 5.972107227618499e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 10: {
    constexpr size_t nb_points = 81;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 5.165089225609747e-02, {}},
       Descriptor{2, 1.346797267166026e-02, {7.255175741743780e-02}},
       Descriptor{2, 2.646691924968953e-02, {3.078959478669229e-01}},
       Descriptor{3, 5.107047828143525e-03, {2.371913960722093e-02}},
       Descriptor{3, 2.364999615824560e-02, {4.034202269308927e-01}},
       Descriptor{4, 2.531736665096822e-02, {2.017386068476799e-01, 7.385418101848620e-02}},
       Descriptor{4, 5.416263407447926e-03, {1.548397007438618e-01, 6.875048558830396e-01}},
       Descriptor{4, 8.153659012054226e-03, {4.125991504993639e-01, 1.741437164939321e-01}},
       Descriptor{4, 1.112597175961278e-02, {3.889072755780651e-02, 2.657575065627199e-01}},
       Descriptor{4, 1.325678848264236e-03, {5.604847951021338e-03, 9.324134903525841e-02}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 11: {
    constexpr size_t nb_points = 110;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.468934358613075e-02, {2.993130923993425e-01}},
       Descriptor{2, 3.380869530621681e-03, {3.263284396518180e-02}},
       Descriptor{3, 1.676676867722563e-02, {1.742044154846892e-01}},
       Descriptor{3, 1.932364034775851e-02, {4.012153758583749e-01}},
       Descriptor{3, 1.653253610375797e-03, {4.942980948772996e-01}},
       Descriptor{4, 9.799971247032874e-03, {1.231206549747224e-01, 7.273303463463835e-01}},
       Descriptor{4, 2.036278491304826e-03, {1.300387048559024e-02, 1.702280734892642e-01}},
       Descriptor{4, 8.207613752916718e-03, {4.293540117192574e-02, 6.182598097891158e-01}},
       Descriptor{4, 1.510498736666349e-02, {1.844240320470778e-01, 5.399001168557016e-01}},
       Descriptor{4, 1.274239266610639e-02, {2.591694074642826e-01, 4.554353196219595e-01}},
       Descriptor{5, 5.273427059689129e-03, {5.351420009314595e-01, 1.097757236596031e-02, 3.414518661958190e-01}}

      };

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 12: {
    constexpr size_t nb_points = 168;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.761491553503257e-02, {2.135679944533018e-01}},
       Descriptor{2, 8.351235933432746e-03, {8.080469951147343e-02}},
       Descriptor{2, 1.777571920669439e-02, {1.460894685275485e-01}},
       Descriptor{3, 1.016802439757214e-02, {4.359346229622011e-01}},
       Descriptor{3, 1.574040002395383e-02, {3.723816950753983e-01}},
       Descriptor{4, 1.511281145864287e-03, {1.481472606744865e-02, 6.940351772721454e-01}},
       Descriptor{4, 8.726569887485608e-04, {4.406791967562980e-02, 5.787943875724942e-04}},
       Descriptor{4, 3.712882661457528e-03, {2.900481455515819e-02, 7.927838364729656e-01}},
       Descriptor{4, 2.358314255727376e-03, {1.384125788015036e-01, 7.217318354185758e-01}},
       Descriptor{5, 4.490847036271700e-03, {1.155183527100142e-02, 2.002685156661767e-01, 4.459556015690982e-01}},
       Descriptor{5, 7.549616048899519e-03, {7.295863195082626e-02, 2.545920450251540e-01, 4.229332866644536e-01}},
       Descriptor{5, 1.850514589067692e-03, {5.001810761518710e-02, 3.937338659984053e-01, 5.538497716858680e-01}},
       Descriptor{5, 9.780703581954099e-03, {2.518367824271116e-01, 3.608735666657864e-02, 5.985486290544021e-01}}

      };

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 13: {
    constexpr size_t nb_points = 172;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 8.606417789141471e-04, {2.023816786180974e-02}},
       Descriptor{2, 2.260732681773922e-02, {2.890147352435263e-01}},
       Descriptor{2, 9.680037326838918e-03, {9.402870008212708e-02}},
       Descriptor{2, 7.170940325572750e-03, {1.976498544437255e-01}},
       Descriptor{3, 7.784870979280401e-03, {4.046189676018364e-02}},
       Descriptor{3, 5.120271481716123e-04, {4.999998048519694e-01}},
       Descriptor{4, 8.880272171422971e-03, {6.752926495280243e-02, 6.118971302042935e-01}},
       Descriptor{4, 1.062144779049374e-02, {1.360105145132029e-01, 4.868308338798159e-01}},
       Descriptor{4, 5.873089331995857e-03, {2.780783459563702e-01, 4.273786080349000e-01}},
       Descriptor{4, 9.235535397556015e-03, {1.967550197192861e-01, 5.667528680967985e-01}},
       Descriptor{4, 1.456732207916522e-02, {3.862498629203497e-01, 6.201970724197543e-02}},
       Descriptor{4, 2.658739862173390e-03, {2.353551905465277e-02, 1.086607820833199e-01}},
       Descriptor{5, 3.221852775001763e-03, {3.362108930747719e-01, 5.446964977019162e-01, 1.155925493534321e-01}},
       Descriptor{5, 1.464206741103990e-03, {6.859816301235152e-01, 2.886160494075550e-02, 2.745833302490004e-01}},
       Descriptor{5, 2.268354927450138e-03, {7.356772467818598e-01, 1.545866468273804e-01, 1.103371356908743e-02}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 14: {
    constexpr size_t nb_points = 204;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 3.445433050323388e-03, {3.298151517846193e-01}},
       Descriptor{2, 2.621255271870497e-03, {5.753828268975920e-02}},
       Descriptor{2, 1.107383241893503e-04, {5.856168613783504e-03}},
       Descriptor{2, 1.002858537247380e-02, {1.605554758479567e-01}},
       Descriptor{2, 6.790408457809412e-03, {9.873964607404909e-02}},
       Descriptor{2, 7.118148341899285e-03, {2.080531961597265e-01}},
       Descriptor{3, 1.060052294564051e-03, {4.902479711877765e-01}},
       Descriptor{3, 1.366983099498089e-02, {1.029394001155326e-01}},
       Descriptor{3, 2.690741094676155e-03, {3.791346346121514e-02}},
       Descriptor{3, 1.016460122584274e-02, {3.185735761838604e-01}},
       Descriptor{4, 1.354439963772740e-02, {2.499092188634998e-01, 4.189387788376669e-01}},
       Descriptor{4, 4.623340047669791e-03, {2.132633780618757e-01, 5.628766802497838e-01}},
       Descriptor{4, 8.331498452473301e-04, {4.918128494015905e-02, 8.954689727140162e-01}},
       Descriptor{4, 7.240296760857249e-03, {3.928626179700601e-01, 2.066357892967347e-02}},
       Descriptor{4, 8.751807809978078e-04, {1.261760553257071e-02, 1.269022024074958e-01}},
       Descriptor{5, 7.172536518098438e-03, {1.216187843486990e-01, 6.214000311762153e-02, 2.395005950785847e-01}},
       Descriptor{5, 4.286856342790781e-03, {3.489022960470246e-01, 5.517941321184247e-01, 8.378145261134291e-02}},
       Descriptor{5, 9.757527641506894e-04, {6.822383201682893e-01, 1.784164064765456e-02, 2.844407418708836e-01}},
       Descriptor{5, 3.757936299766722e-03, {7.339982986585810e-01, 1.695395507001622e-01, 7.868554787111390e-02}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 15: {
    constexpr size_t nb_points = 264;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 5.112029771454935e-04, {1.692642158547220e-02}},
       Descriptor{2, 1.620740592022658e-02, {2.836779254595722e-01}},
       Descriptor{2, 1.552578602041314e-02, {1.821984139975859e-01}},
       Descriptor{3, 1.219010259975421e-03, {1.450800515218459e-02}},
       Descriptor{3, 8.472862853125917e-03, {1.427441437658564e-01}},
       Descriptor{4, 1.797534305405733e-03, {7.321727256195354e-02, 7.766357493733004e-01}},
       Descriptor{4, 8.198015220760061e-04, {1.056558410489699e-02, 2.225606554924344e-01}},
       Descriptor{4, 7.681775272473012e-03, {2.637972626688146e-01, 5.575022240565973e-02}},
       Descriptor{4, 4.479850385774781e-03, {4.354902702993817e-01, 1.112909267055329e-01}},
       Descriptor{4, 5.615298608384355e-03, {5.347259364185193e-02, 5.448865322975603e-01}},
       Descriptor{4, 1.769703685797290e-03, {1.105308939580100e-01, 7.694703779405406e-01}},
       Descriptor{5, 1.131138329187022e-03, {4.104670514991076e-02, 6.030936440770986e-01, 2.328924415481402e-03}},
       Descriptor{5, 8.404993407577487e-04, {9.201667327695258e-02, 2.939319332917082e-02, 8.672646051241815e-01}},
       Descriptor{5, 3.011968607402684e-03, {6.822627868000052e-02, 7.077814873155971e-01, 2.200643755861662e-02}},
       Descriptor{5, 5.084429327522668e-03, {1.890015556386176e-01, 1.295809597674534e-01, 6.807532978800637e-02}},
       Descriptor{5, 7.139383728003996e-03, {3.054799663906012e-01, 4.695333003662676e-01, 7.251484323369149e-02}},
       Descriptor{5, 3.378550568229294e-03, {1.509145500825105e-01, 5.778346261482753e-01, 1.112774031280626e-02}},
       Descriptor{5, 2.201680777701462e-03, {3.368450658547645e-01, 1.082194753682293e-02, 4.176069528643034e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 16: {
    constexpr size_t nb_points = 304;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 4.034878937238870e-03, {3.296714738440606e-01}},
       Descriptor{2, 5.263428008047628e-03, {1.120421044173788e-01}},
       Descriptor{2, 1.078639106857764e-02, {2.804460259110929e-01}},
       Descriptor{2, 1.892340029030997e-03, {3.942164444076166e-02}},
       Descriptor{3, 6.014511003000459e-03, {7.491741856476755e-02}},
       Descriptor{3, 8.692191232873023e-03, {3.356931029556346e-01}},
       Descriptor{4, 3.507623285784993e-03, {4.904898759556675e-02, 7.646870675801803e-01}},
       Descriptor{4, 1.188662273319198e-03, {1.412609568309253e-02, 2.328268045894251e-01}},
       Descriptor{4, 4.345225314466667e-03, {6.239652058154325e-02, 2.832417683077947e-01}},
       Descriptor{4, 2.660686570203166e-03, {1.890959275696560e-01, 1.283187405611824e-02}},
       Descriptor{4, 5.553174302124013e-03, {2.750176001295444e-01, 3.872709603194903e-01}},
       Descriptor{4, 1.531431405395808e-04, {5.944898252569946e-03, 3.723805935523542e-02}},
       Descriptor{4, 2.231204136715679e-03, {1.183058071099944e-01, 7.482941078308859e-01}},
       Descriptor{5, 2.472377156249970e-03, {8.011846127872502e-02, 5.146357887888395e-01, 3.908021114187921e-01}},
       Descriptor{5, 6.596122891817925e-03, {3.102585498627273e-01, 1.645739468379099e-01, 6.995093322963369e-02}},
       Descriptor{5, 6.004349067697421e-04, {1.085240801928985e-01, 3.435867950145696e-02, 8.557156992205752e-01}},
       Descriptor{5, 2.242330996876527e-03, {2.483824987814955e-01, 6.625317544850510e-01, 1.098323448764900e-02}},
       Descriptor{5, 8.244422362471837e-04, {3.960091211067035e-01, 1.226898678006519e-02, 1.878187449597510e-02}},
       Descriptor{5, 4.388015835267911e-03, {6.367516197137306e-02, 2.054604991324105e-01, 1.362449508885895e-01}},
       Descriptor{5, 3.454278425740962e-03, {1.757650466139104e-01, 4.610678860796995e-01, 1.387535709612253e-01}},
       Descriptor{5, 3.929289473335571e-03, {4.779942532006705e-01, 1.344788610299629e-02, 3.221398306538996e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 17: {
    constexpr size_t nb_points = 364;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 2.943411054682655e-03, {1.092594110391400e-01}},
       Descriptor{2, 4.821254562130973e-04, {1.706905335350991e-02}},
       Descriptor{2, 1.633411882335971e-03, {6.030276440208671e-02}},
       Descriptor{2, 8.937260544733967e-03, {1.802936778752487e-01}},
       Descriptor{3, 3.539281041131444e-03, {2.075642494319923e-01}},
       Descriptor{3, 5.127876775122913e-03, {6.350561875036179e-02}},
       Descriptor{3, 8.505961705776655e-03, {1.425740948569834e-01}},
       Descriptor{3, 1.362001059565090e-03, {1.525498961135818e-02}},
       Descriptor{4, 1.952703594699083e-03, {2.817717468661823e-01, 6.605628545936354e-03}},
       Descriptor{4, 3.552482332702156e-03, {2.532792896616394e-01, 3.473076755269789e-01}},
       Descriptor{4, 4.735037872087881e-03, {1.194446836887992e-01, 5.129631932135305e-01}},
       Descriptor{4, 4.076204052523780e-03, {2.796975892179978e-01, 4.040135150131379e-01}},
       Descriptor{4, 4.397889616038591e-03, {5.914965001755916e-02, 2.771505768037979e-01}},
       Descriptor{4, 8.159915418039479e-04, {1.156645687972039e-02, 3.372693231713962e-01}},
       Descriptor{4, 6.436927403356441e-03, {2.546200946118619e-01, 8.899213511967391e-02}},
       Descriptor{4, 2.290937029978224e-04, {6.672914677865158e-03, 8.335377000797282e-02}},
       Descriptor{4, 3.188746694204980e-03, {5.954996366173630e-02, 1.488738514609226e-01}},
       Descriptor{5, 1.991266259969879e-03, {9.563829425828949e-02, 1.308047471312760e-02, 1.683493064182302e-01}},
       Descriptor{5, 1.209946636868781e-03, {6.035751534893870e-02, 6.376551993416253e-01, 1.062246299591423e-02}},
       Descriptor{5, 2.943417982183902e-03, {5.720629662753272e-01, 1.575062824084584e-01, 1.289545879798304e-02}},
       Descriptor{5, 3.454094554810564e-03, {5.982871618499548e-01, 1.530963612221368e-01, 6.489065131906320e-02}},
       Descriptor{5, 8.241135414777571e-04, {8.721077921772252e-02, 1.165500297046612e-02, 4.707766223753580e-02}},
       Descriptor{5, 1.383542378170185e-03, {4.137799680661914e-01, 4.085328910163256e-01, 1.048009320013415e-02}},
       Descriptor{5, 1.781445235926538e-03, {3.904989583719425e-01, 1.167932299246964e-02, 7.360595669274642e-02}},
       Descriptor{5, 6.969316351001401e-04, {7.735017403534717e-01, 8.163680371232981e-03, 2.545430962429988e-02}},
       Descriptor{5, 5.722888401891606e-03, {5.603440494408158e-02, 1.529886055031752e-01, 3.226986469260043e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 18: {
    constexpr size_t nb_points = 436;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.505388406750596e-03, {2.063721818681210e-01}},
       Descriptor{2, 7.201441531528436e-03, {1.625387945176406e-01}},
       Descriptor{2, 5.801801233694815e-03, {3.116266728255643e-01}},
       Descriptor{2, 2.465088392849791e-03, {3.307279160890313e-01}},
       Descriptor{2, 8.235271059110021e-04, {3.374408201299506e-02}},
       Descriptor{2, 2.396153395824959e-03, {6.568138278425954e-02}},
       Descriptor{2, 1.662026538062453e-04, {1.169072420259711e-02}},
       Descriptor{3, 4.747626606883917e-05, {3.051729141214794e-01}},
       Descriptor{3, 5.654960404175805e-04, {2.498171653415399e-01}},
       Descriptor{3, 1.126644847587598e-03, {1.336652291605620e-02}},
       Descriptor{3, 1.348246151880201e-03, {2.503423435758855e-01}},
       Descriptor{3, 3.657069096677683e-04, {3.324517722542840e-01}},
       Descriptor{3, 5.853171992565114e-03, {3.328935974705466e-01}},
       Descriptor{3, 1.956691034821430e-03, {1.053889982941293e-01}},
       Descriptor{3, 1.673244252179039e-03, {4.317721941366494e-02}},
       Descriptor{4, 1.684409973125634e-03, {3.310306761504429e-02, 1.352754021303201e-01}},
       Descriptor{4, 9.489733030971150e-04, {1.946185784467675e-01, 1.171879047047253e-03}},
       Descriptor{4, 1.039083135931653e-03, {1.321252126131118e-02, 3.278960098149503e-01}},
       Descriptor{4, 2.868558324240348e-03, {1.229320721770598e-01, 2.028399843152170e-02}},
       Descriptor{4, 3.672202784572132e-03, {6.199506095168380e-02, 3.498961219623653e-01}},
       Descriptor{4, 4.747155546596363e-03, {8.407187546991698e-02, 1.666830796952924e-01}},
       Descriptor{4, 4.150884078212972e-04, {7.867048874453891e-03, 1.859179591228751e-01}},
       Descriptor{4, 2.504990122247683e-03, {4.217582498871236e-02, 2.482707411503117e-01}},
       Descriptor{4, 2.629919627710238e-04, {8.683491872138840e-03, 6.698102226768764e-02}},
       Descriptor{4, 5.237948818645674e-03, {1.111185367576208e-01, 2.918417077981162e-01}},
       Descriptor{5, 9.530517345502799e-04, {3.685463845606022e-03, 6.377078964518405e-02, 2.161982818015302e-01}},
       Descriptor{5, 4.424079498028044e-04, {9.147563979493070e-02, 4.024098103223798e-03, 4.866034767929566e-02}},
       Descriptor{5, 1.756179008422938e-03, {1.326563740184036e-01, 4.168774597385260e-01, 2.814320586294362e-02}},
       Descriptor{5, 2.434266881750153e-03, {4.439198669278809e-01, 2.464330224801677e-01, 2.919443405619611e-02}},
       Descriptor{5, 3.936511965938193e-03, {2.395450885895190e-01, 1.271207488070096e-01, 2.457574497117561e-01}},
       Descriptor{5, 3.199791269106457e-03, {1.657130887168779e-01, 2.137049061861850e-01, 7.023964653694080e-02}},
       Descriptor{5, 3.797610312158748e-03, {1.788776686026366e-01, 3.290353014877974e-01, 6.661063766146717e-02}},
       Descriptor{5, 1.495938562630981e-03, {3.340176116726106e-01, 4.950315956949369e-03, 1.887999372824127e-01}},
       Descriptor{5, 1.703996181224725e-03, {5.458773009126385e-01, 7.846288361681254e-03, 7.104964990519175e-02}},
       Descriptor{5, 3.628775117699059e-03, {2.510439859007851e-02, 1.293557534662013e-01, 2.529882400374321e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 19: {
    constexpr size_t nb_points = 487;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 9.477424964421555e-03, {}},
       Descriptor{2, 2.309652046579032e-03, {9.623346794170115e-02}},
       Descriptor{2, 4.091492762240734e-03, {2.840710971185555e-01}},
       Descriptor{2, 1.173654667448287e-03, {4.201218049762310e-02}},
       Descriptor{3, 5.797759302818667e-03, {1.293925012179526e-01}},
       Descriptor{4, 2.028992639106921e-03, {5.339312125330306e-02, 7.679699947393867e-01}},
       Descriptor{4, 2.005858485106433e-04, {5.250553906919603e-03, 1.660339253205299e-01}},
       Descriptor{4, 2.908059972456899e-03, {2.128993323694659e-01, 1.034045995093328e-01}},
       Descriptor{4, 2.532078463287134e-03, {4.034135325977229e-01, 1.333356346432022e-01}},
       Descriptor{4, 7.343695736316139e-04, {1.188811930811271e-02, 5.532379678070896e-01}},
       Descriptor{4, 1.076183574539400e-03, {8.903116551206627e-02, 8.080695151909102e-01}},
       Descriptor{4, 1.986415677131464e-03, {5.151678695899160e-02, 4.873280365294901e-01}},
       Descriptor{4, 5.046317364293947e-03, {1.179992852297671e-01, 2.466149943644083e-01}},
       Descriptor{4, 2.692282460531053e-03, {4.608512334928590e-02, 2.781414318856127e-01}},
       Descriptor{4, 1.330636288503801e-03, {1.410186708097898e-01, 8.174649948389451e-03}},
       Descriptor{4, 1.782928900852855e-03, {1.411034408699267e-01, 5.855868767598970e-01}},
       Descriptor{4, 1.263015826184454e-04, {7.911862204821108e-03, 3.035883809061935e-02}},
       Descriptor{4, 3.244260943819173e-03, {1.761012114703174e-01, 2.461239587816849e-01}},
       Descriptor{5, 1.048498185712524e-03, {4.531137441520981e-02, 7.694124041149815e-01, 1.162671237541303e-02}},
       Descriptor{5, 3.605740745734604e-03, {1.902469770309511e-01, 1.139907343389768e-01, 4.902896398807803e-02}},
       Descriptor{5, 2.561558407869844e-03, {2.025882107314621e-01, 5.045224197189542e-01, 5.200356315054191e-02}},
       Descriptor{5, 1.498918467834416e-03, {6.280459865457699e-02, 3.899342137938394e-01, 9.651021927014570e-03}},
       Descriptor{5, 1.814324572327483e-03, {1.689034611068301e-01, 1.025101977116908e-02, 2.542260337897459e-01}},
       Descriptor{5, 2.883938073996170e-03, {1.282802298826546e-01, 3.714709314621824e-01, 2.826241348940526e-01}},
       Descriptor{5, 3.191411305221261e-03, {3.753164302248334e-01, 2.323738231259893e-01, 5.648574604597608e-02}},
       Descriptor{5, 3.955282336986757e-04, {8.176634693298535e-02, 2.823323910477210e-02, 8.840321921276471e-01}},
       Descriptor{5, 1.642211231915762e-03, {3.727215008878210e-01, 4.691373442926859e-01, 1.052014400647361e-02}},
       Descriptor{5, 4.640920897459689e-04, {2.881077012617455e-01, 6.241150434253934e-03, 2.040787180394686e-02}},
       Descriptor{5, 1.210865339994028e-03, {8.483677030488673e-03, 2.548571009364550e-01, 8.369886684600136e-02}},
       Descriptor{5, 3.653283017278065e-03, {1.193128036212033e-01, 5.081225906571617e-01, 5.013138003585081e-02}},
       Descriptor{5, 1.744791238762615e-03, {4.198369201417426e-01, 1.094996106379021e-02, 3.155028856836414e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 20: {
    constexpr size_t nb_points = 552;
    SmallArray<R3> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 5.418454790084648e-03, {2.967759596934131e-01}},
       Descriptor{2, 3.820897988248627e-03, {1.209047972112185e-01}},
       Descriptor{2, 4.057458152334048e-03, {3.177959071881044e-01}},
       Descriptor{2, 4.147670297750325e-03, {2.012655712414790e-01}},
       Descriptor{2, 5.289566609934361e-03, {1.678838969272885e-01}},
       Descriptor{2, 9.720418198623532e-04, {3.621493960968947e-02}},
       Descriptor{3, 2.988176136780452e-03, {5.260244961748377e-02}},
       Descriptor{3, 7.061746923528361e-03, {3.045785563313042e-01}},
       Descriptor{4, 1.138014164865969e-03, {3.228230549839516e-02, 8.181532733687802e-01}},
       Descriptor{4, 3.284328089439585e-04, {8.126514470519638e-03, 1.671963931828212e-01}},
       Descriptor{4, 4.517305074533165e-03, {1.851459522994347e-01, 6.611011876662430e-02}},
       Descriptor{4, 2.743957098139693e-03, {4.110775382017770e-01, 1.281831845252744e-01}},
       Descriptor{4, 5.345104029866354e-04, {1.005979740685446e-02, 5.566366248307155e-01}},
       Descriptor{4, 8.527602773365834e-04, {8.230161503723588e-02, 8.242515811428504e-01}},
       Descriptor{4, 1.608673320950611e-03, {3.801282860266467e-02, 6.072922714391159e-01}},
       Descriptor{4, 2.164021789623034e-03, {8.297816905794758e-02, 1.927591204214162e-01}},
       Descriptor{4, 1.298964551990821e-03, {3.232830149045374e-02, 2.290899793113822e-01}},
       Descriptor{4, 7.540221084980848e-04, {1.406521940177762e-01, 4.062459178585371e-03}},
       Descriptor{4, 3.936392177197076e-03, {1.139719439112915e-01, 5.168656572693773e-01}},
       Descriptor{4, 8.260437254311268e-05, {7.453571377174895e-03, 2.517856316811188e-02}},
       Descriptor{4, 2.202593581265821e-03, {1.110171267302689e-01, 3.907070674791312e-01}},
       Descriptor{5, 1.142912288312416e-03, {5.313810503913767e-02, 5.333767142737392e-01, 9.768492667580009e-03}},
       Descriptor{5, 8.037341179884985e-04, {9.439207428728114e-02, 9.450517146275739e-02, 7.609480923624367e-01}},
       Descriptor{5, 6.441055085633393e-04, {5.505445164272958e-02, 7.653955692270024e-01, 5.660747947619223e-03}},
       Descriptor{5, 2.284803061568028e-03, {1.798637301234305e-01, 9.751720382220438e-02, 3.466187077541209e-02}},
       Descriptor{5, 1.768215173403588e-03, {2.121920547752147e-01, 4.095036586170944e-01, 4.613078410418590e-02}},
       Descriptor{5, 1.364093116604035e-03, {1.290161512351960e-01, 4.765557207500070e-01, 9.956030373782836e-03}},
       Descriptor{5, 1.409513020242023e-03, {1.614873223066114e-01, 1.376084748677020e-02, 2.264813194055542e-01}},
       Descriptor{5, 5.352196172820926e-03, {1.059283078939860e-01, 4.143500741402470e-01, 1.944202417194029e-01}},
       Descriptor{5, 2.357251753798956e-03, {2.046229328456809e-01, 2.864083155254953e-01, 3.607794123452725e-02}},
       Descriptor{5, 2.335500281519085e-04, {7.441796869877018e-02, 2.361583233015298e-02, 8.995240292632410e-01}},
       Descriptor{5, 8.299191110448388e-04, {2.938352359686784e-01, 5.078745759286403e-01, 4.379772363790332e-03}},
       Descriptor{5, 3.480327275620159e-04, {2.899004919931303e-01, 2.388614940511808e-03, 2.095834844884252e-02}},
       Descriptor{5, 1.081306497459309e-03, {6.920395527608953e-03, 2.780090233488638e-01, 8.666224435610116e-02}},
       Descriptor{5, 3.363592992643566e-03, {1.053191068972152e-01, 5.431130574905816e-01, 4.625008859127536e-02}},
       Descriptor{5, 1.138819523953005e-03, {3.387615700292962e-01, 8.792325219258773e-03, 4.002505388571689e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("Gauss quadrature formulae handle degrees up to " +
                          stringify(TetrahedronGaussQuadrature::max_degree) + " on tetrahedra");
  }
    // LCOV_EXCL_STOP
  }
}
