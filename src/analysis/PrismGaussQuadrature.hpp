#ifndef PRISM_GAUSS_QUADRATURE_HPP
#define PRISM_GAUSS_QUADRATURE_HPP

#include <analysis/QuadratureFormula.hpp>

/**
 * Defines Gauss quadrature on the reference prism element
 *
 * \note formulae are provided by
 *
 * 'High-order symmetric cubature rules for tetrahedra and pyramids'
 * Jan Jaśkowiec & N. Sukumar (2020).
 */
class PrismGaussQuadrature final : public QuadratureFormula<3>
{
 public:
  constexpr static size_t max_degree = 20;

 private:
  void _buildPointAndWeightLists(const size_t degree);

 public:
  PrismGaussQuadrature(PrismGaussQuadrature&&)      = default;
  PrismGaussQuadrature(const PrismGaussQuadrature&) = default;

 private:
  friend class QuadratureManager;

  explicit PrismGaussQuadrature(const size_t degree) : QuadratureFormula<3>(QuadratureType::Gauss)
  {
    this->_buildPointAndWeightLists(degree);
  }

  PrismGaussQuadrature()  = delete;
  ~PrismGaussQuadrature() = default;
};

#endif   // PRISM_GAUSS_QUADRATURE_HPP
