#ifndef GAUSS_QUADRATURE_DESCRIPTOR_HPP
#define GAUSS_QUADRATURE_DESCRIPTOR_HPP

#include <analysis/IQuadratureDescriptor.hpp>

#include <sstream>

class GaussQuadratureDescriptor final : public IQuadratureDescriptor
{
 private:
  size_t m_degree;

 public:
  bool
  isTensorial() const
  {
    return false;
  }

  QuadratureType
  type() const
  {
    return QuadratureType::Gauss;
  }

  size_t
  degree() const
  {
    return m_degree;
  }

  std::string
  name() const
  {
    std::ostringstream os;
    os << ::name(this->type()) << "(" << m_degree << ")";
    return os.str();
  }

  GaussQuadratureDescriptor(size_t degree) : m_degree{degree} {}
  GaussQuadratureDescriptor() noexcept = delete;

  GaussQuadratureDescriptor(const GaussQuadratureDescriptor&) = default;

  GaussQuadratureDescriptor(GaussQuadratureDescriptor&&) noexcept = default;

  virtual ~GaussQuadratureDescriptor() noexcept = default;
};

#endif   // GAUSS_QUADRATURE_DESCRIPTOR_HPP
