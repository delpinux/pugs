#include <analysis/SquareGaussQuadrature.hpp>

#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

void
SquareGaussQuadrature::_buildPointAndWeightLists(const size_t degree)
{
  using R2 = TinyVector<2>;

  struct Descriptor
  {
    int id;
    double weight;
    std::vector<double> lambda_list;
  };

  auto fill_quadrature_points = [](auto descriptor_list, auto& point_list, auto& weight_list) {
    Assert(point_list.size() == weight_list.size());

    size_t k = 0;
    for (size_t i = 0; i < descriptor_list.size(); ++i) {
      const auto [id, w, position_list] = descriptor_list[i];

      switch (id) {
      case 1: {
        Assert(position_list.size() == 0);

        point_list[k]  = R2{0, 0};
        weight_list[k] = w;

        k += 1;
        break;
      }
      case 2: {
        Assert(position_list.size() == 1);
        const double& a = position_list[0];

        point_list[k + 0] = R2{+a, 0};
        point_list[k + 1] = R2{-a, 0};
        point_list[k + 2] = R2{0, +a};
        point_list[k + 3] = R2{0, -a};

        for (size_t l = 0; l < 4; ++l) {
          weight_list[k + l] = w;
        }

        k += 4;
        break;
      }
      case 3: {
        Assert(position_list.size() == 1);
        const double& a = position_list[0];

        point_list[k + 0] = R2{+a, +a};
        point_list[k + 1] = R2{+a, -a};
        point_list[k + 2] = R2{-a, +a};
        point_list[k + 3] = R2{-a, -a};

        for (size_t l = 0; l < 4; ++l) {
          weight_list[k + l] = w;
        }

        k += 4;
        break;
      }
      case 4: {
        Assert(position_list.size() == 2);
        const double& a = position_list[0];
        const double& b = position_list[1];

        point_list[k + 0] = R2{+a, +b};
        point_list[k + 1] = R2{+a, -b};
        point_list[k + 2] = R2{-a, +b};
        point_list[k + 3] = R2{-a, -b};
        point_list[k + 4] = R2{+b, +a};
        point_list[k + 5] = R2{+b, -a};
        point_list[k + 6] = R2{-b, +a};
        point_list[k + 7] = R2{-b, -a};

        for (size_t l = 0; l < 8; ++l) {
          weight_list[k + l] = w;
        }

        k += 8;
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("invalid quadrature id");
      }
        // LCOV_EXCL_STOP
      }
    }

    Assert(k == point_list.size(), "invalid number of quadrature points");
  };

  switch (degree) {
  case 0:
  case 1: {
    constexpr size_t nb_points = 1;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 4.000000000000000e+00, {}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 2:
  case 3: {
    constexpr size_t nb_points = 4;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{3, 1.000000000000000e+00, {5.773502691896257e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 4:
  case 5: {
    constexpr size_t nb_points = 8;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 8.163265306122449e-01, {6.831300510639732e-01}},
       Descriptor{3, 1.836734693877551e-01, {8.819171036881969e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 6:
  case 7: {
    constexpr size_t nb_points = 12;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 2.419753086419753e-01, {9.258200997725514e-01}},
       Descriptor{3, 2.374317746906302e-01, {8.059797829185987e-01}},
       Descriptor{3, 5.205929166673945e-01, {3.805544332083157e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 8:
  case 9: {
    constexpr size_t nb_points = 20;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 4.541639606867490e-01, {4.889268569743691e-01}},
       Descriptor{3, 4.273123186577577e-02, {9.396552580968377e-01}},
       Descriptor{3, 2.142003609268616e-01, {6.908805504863439e-01}},
       Descriptor{4, 1.444522232603068e-01, {9.186204410567222e-01, 3.448720253644036e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 10:
  case 11: {
    constexpr size_t nb_points = 28;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 2.174004398687120e-01, {7.146178296646060e-01}},
       Descriptor{3, 2.772741029838511e-01, {2.736572101714596e-01}},
       Descriptor{3, 2.139336378782481e-01, {6.366039322123010e-01}},
       Descriptor{4, 4.407456911498309e-02, {9.516303887840335e-01, 8.155654336896384e-01}},
       Descriptor{4, 1.016213405196113e-01, {3.462072000476454e-01, 9.355678714875911e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 12:
  case 13: {
    constexpr size_t nb_points = 37;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 2.999933443589096e-01, {}},
       Descriptor{2, 3.812862534985274e-02, {9.834613326132455e-01}},
       Descriptor{2, 1.845354689698072e-01, {6.398614183671097e-01}},
       Descriptor{3, 3.950714064745244e-02, {9.187784880797114e-01}},
       Descriptor{3, 2.313985194006854e-01, {3.796285051867438e-01}},
       Descriptor{3, 1.372420967130035e-01, {6.995542165133511e-01}},
       Descriptor{4, 3.351978005038143e-02, {6.435973749966181e-01, 9.750688839059836e-01}},
       Descriptor{4, 1.135751263643542e-01, {3.335398811647831e-01, 8.644276092670619e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 14:
  case 15: {
    constexpr size_t nb_points = 48;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.149329272635254e-01, {7.980989878970071e-01}},
       Descriptor{2, 1.816857589672718e-01, {3.038530263984597e-01}},
       Descriptor{3, 4.123848378876581e-02, {8.824222701068853e-01}},
       Descriptor{3, 5.933746485839221e-03, {9.777897995399027e-01}},
       Descriptor{4, 1.011491434774324e-01, {8.087121358195035e-01, 5.672135733965907e-01}},
       Descriptor{4, 1.478367216328812e-01, {3.046547990370526e-01, 5.787361940358066e-01}},
       Descriptor{4, 2.327319414467321e-02, {9.805048437245319e-01, 6.974636319909671e-01}},
       Descriptor{4, 5.584548249231202e-02, {2.648441558723162e-01, 9.557425198095117e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 16:
  case 17: {
    constexpr size_t nb_points = 60;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 1.388043776872199e-01, {5.450429872091483e-01}},
       Descriptor{2, 6.534782380746491e-02, {9.174971882191532e-01}},
       Descriptor{3, 1.428564196221996e-01, {1.925542426140469e-01}},
       Descriptor{3, 3.671590848525151e-02, {8.713286846954432e-01}},
       Descriptor{3, 8.625569963814381e-02, {6.943880900895507e-01}},
       Descriptor{3, 1.345562732119169e-01, {4.576829928831555e-01}},
       Descriptor{3, 7.655633414909936e-03, {9.692042560915087e-01}},
       Descriptor{4, 1.031767247849354e-01, {7.620882760801982e-01, 2.794777667151921e-01}},
       Descriptor{4, 5.482640023729144e-02, {9.073943521982236e-01, 5.468986704143101e-01}},
       Descriptor{4, 1.511981038825686e-02, {7.612486664390827e-01, 9.837879715015495e-01}},
       Descriptor{4, 2.078099665596304e-02, {9.870947070447679e-01, 3.028074817888614e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 18:
  case 19: {
    constexpr size_t nb_points = 72;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{2, 9.773736469282875e-02, {7.143442595727942e-01}},
       Descriptor{2, 1.393076129224823e-01, {2.656720521209637e-01}},
       Descriptor{2, 3.486958349188791e-02, {9.644342692579673e-01}},
       Descriptor{3, 4.780454716279579e-02, {8.033797294676850e-01}},
       Descriptor{3, 1.617715177911761e-03, {9.921654058971348e-01}},
       Descriptor{3, 1.744104803435576e-02, {9.294496027996094e-01}},
       Descriptor{4, 1.177258328400561e-01, {5.102782573693434e-01, 2.666403145945622e-01}},
       Descriptor{4, 1.617957761165785e-02, {3.907342057752498e-01, 9.878929331417353e-01}},
       Descriptor{4, 8.284661898340490e-02, {7.171679213097452e-01, 5.124918772160977e-01}},
       Descriptor{4, 6.498908149259605e-02, {2.654400078112960e-01, 8.689024341545042e-01}},
       Descriptor{4, 3.898055239641054e-02, {6.200353986932564e-01, 9.263029558071293e-01}},
       Descriptor{4, 9.889400934743484e-03, {8.016715847185969e-01, 9.884465306839737e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
  case 20:
  case 21: {
    constexpr size_t nb_points = 85;
    SmallArray<R2> point_list(nb_points);
    SmallArray<double> weight_list(nb_points);

    std::array descriptor_list =   //
      {Descriptor{1, 1.351928403561428e-01, {}},
       Descriptor{2, 1.067941587859404e-01, {4.733510743582242e-01}},
       Descriptor{2, 6.625720800494439e-02, {8.352784297558683e-01}},
       Descriptor{3, 1.198844836463405e-01, {2.573719006072290e-01}},
       Descriptor{3, 8.229461289220009e-03, {9.633378321156234e-01}},
       Descriptor{3, 3.060364092877565e-02, {8.624519253796515e-01}},
       Descriptor{3, 9.679179189359521e-02, {4.968979625193457e-01}},
       Descriptor{3, 6.151634148131656e-02, {7.043321751954006e-01}},
       Descriptor{4, 8.672849951320823e-02, {2.418781854767020e-01, 6.741462199553178e-01}},
       Descriptor{4, 5.587911740412735e-02, {4.801569663127951e-01, 8.246473752709207e-01}},
       Descriptor{4, 3.712882744091382e-02, {9.322419359217540e-01, 2.706991841016649e-01}},
       Descriptor{4, 2.877560085102053e-02, {9.349023258240106e-01, 6.750395674370753e-01}},
       Descriptor{4, 1.150952044249317e-02, {9.904554675295242e-01, 4.889162511771669e-01}},
       Descriptor{4, 7.528482130401646e-03, {8.311352459759014e-01, 9.889606113865724e-01}},
       Descriptor{4, 1.051230415825108e-02, {9.146960092229130e-02, 9.840171484895990e-01}}};

    fill_quadrature_points(descriptor_list, point_list, weight_list);

    m_point_list  = point_list;
    m_weight_list = weight_list;
    break;
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("Gauss quadrature formulae handle degrees up to " +
                          stringify(SquareGaussQuadrature::max_degree) + " on squares");
  }
    // LCOV_EXCL_STOP
  }
}
