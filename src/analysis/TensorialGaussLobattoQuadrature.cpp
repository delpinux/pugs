#include <analysis/TensorialGaussLobattoQuadrature.hpp>
#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

template <>
void
TensorialGaussLobattoQuadrature<1>::_buildPointAndWeightLists(const size_t degree)
{
  using R1 = TinyVector<1>;

  const size_t nb_points = degree / 2 + 2;

  SmallArray<TinyVector<1>> point_list(nb_points);
  SmallArray<double> weight_list(nb_points);

  switch (degree) {
  case 0:
  case 1: {
    point_list[0] = R1{-1};
    point_list[1] = R1{+1};

    weight_list[0] = 1;
    weight_list[1] = 1;
    break;
  }
  case 2:
  case 3: {
    point_list[0] = R1{-1};
    point_list[1] = R1{0};
    point_list[2] = R1{+1};

    weight_list[0] = 0.3333333333333333333333333;
    weight_list[1] = 1.3333333333333333333333333;
    weight_list[2] = 0.3333333333333333333333333;
    break;
  }
  case 4:
  case 5: {
    point_list[0] = R1{-1};
    point_list[1] = R1{-0.4472135954999579392818347};
    point_list[2] = R1{+0.4472135954999579392818347};
    point_list[3] = R1{+1};

    weight_list[0] = 0.1666666666666666666666667;
    weight_list[1] = 0.8333333333333333333333333;
    weight_list[2] = 0.8333333333333333333333333;
    weight_list[3] = 0.1666666666666666666666667;
    break;
  }
  case 6:
  case 7: {
    point_list[0] = R1{-1};
    point_list[1] = R1{-0.6546536707079771437982925};
    point_list[2] = R1{0};
    point_list[3] = R1{+0.6546536707079771437982925};
    point_list[4] = R1{+1};

    weight_list[0] = 0.1;
    weight_list[1] = 0.5444444444444444444444444;
    weight_list[2] = 0.7111111111111111111111111;
    weight_list[3] = 0.5444444444444444444444444;
    weight_list[4] = 0.1;
    break;
  }
  case 8:
  case 9: {
    point_list[0] = R1{-1};
    point_list[1] = R1{-0.7650553239294646928510030};
    point_list[2] = R1{-0.2852315164806450963141510};
    point_list[3] = R1{+0.2852315164806450963141510};
    point_list[4] = R1{+0.7650553239294646928510030};
    point_list[5] = R1{+1};

    weight_list[0] = 0.0666666666666666666666667;
    weight_list[1] = 0.3784749562978469803166128;
    weight_list[2] = 0.5548583770354863530167205;
    weight_list[3] = 0.5548583770354863530167205;
    weight_list[4] = 0.3784749562978469803166128;
    weight_list[5] = 0.0666666666666666666666667;
    break;
  }
  case 10:
  case 11: {
    point_list[0] = R1{-1};
    point_list[1] = R1{-0.8302238962785669298720322};
    point_list[2] = R1{-0.4688487934707142138037719};
    point_list[3] = R1{0};
    point_list[4] = R1{+0.4688487934707142138037719};
    point_list[5] = R1{+0.8302238962785669298720322};
    point_list[6] = R1{+1};

    weight_list[0] = 0.0476190476190476190476190;
    weight_list[1] = 0.2768260473615659480107004;
    weight_list[2] = 0.4317453812098626234178710;
    weight_list[3] = 0.4876190476190476190476190;
    weight_list[4] = 0.4317453812098626234178710;
    weight_list[5] = 0.2768260473615659480107004;
    weight_list[6] = 0.0476190476190476190476190;
    break;
  }
  case 12:
  case 13: {
    point_list[0] = R1{-1};
    point_list[1] = R1{-0.8717401485096066153374457};
    point_list[2] = R1{-0.5917001814331423021445107};
    point_list[3] = R1{-0.2092992179024788687686573};
    point_list[4] = R1{+0.2092992179024788687686573};
    point_list[5] = R1{+0.5917001814331423021445107};
    point_list[6] = R1{+0.8717401485096066153374457};
    point_list[7] = R1{+1};

    weight_list[0] = 0.0357142857142857142857143;
    weight_list[1] = 0.2107042271435060393829921;
    weight_list[2] = 0.3411226924835043647642407;
    weight_list[3] = 0.4124587946587038815670530;
    weight_list[4] = 0.4124587946587038815670530;
    weight_list[5] = 0.3411226924835043647642407;
    weight_list[6] = 0.2107042271435060393829921;
    weight_list[7] = 0.0357142857142857142857143;
    break;
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("Gauss-Lobatto quadrature formulae handle degrees up to " +
                          stringify(TensorialGaussLobattoQuadrature<1>::max_degree));
  }
    // LCOV_EXCL_STOP
  }

  m_point_list  = point_list;
  m_weight_list = weight_list;
}

template <>
void
TensorialGaussLobattoQuadrature<2>::_buildPointAndWeightLists(const size_t degree)
{
  const size_t nb_points_1d = degree / 2 + 2;
  const size_t nb_points    = nb_points_1d * nb_points_1d;

  SmallArray<TinyVector<2>> point_list(nb_points);
  SmallArray<double> weight_list(nb_points);

  TensorialGaussLobattoQuadrature<1> gauss_lobatto_1d(degree);
  const auto& point_list_1d  = gauss_lobatto_1d.pointList();
  const auto& weight_list_1d = gauss_lobatto_1d.weightList();

  size_t l = 0;
  for (size_t i = 0; i < nb_points_1d; ++i) {
    for (size_t j = 0; j < nb_points_1d; ++j, ++l) {
      point_list[l]  = TinyVector<2>{point_list_1d[i][0], point_list_1d[j][0]};
      weight_list[l] = weight_list_1d[i] * weight_list_1d[j];
    }
  }

  this->m_point_list  = point_list;
  this->m_weight_list = weight_list;
}

template <>
void
TensorialGaussLobattoQuadrature<3>::_buildPointAndWeightLists(const size_t degree)
{
  const size_t nb_points_1d = degree / 2 + 2;
  const size_t nb_points    = nb_points_1d * nb_points_1d * nb_points_1d;

  SmallArray<TinyVector<3>> point_list(nb_points);
  SmallArray<double> weight_list(nb_points);

  TensorialGaussLobattoQuadrature<1> gauss_lobatto_1d(degree);
  const auto& point_list_1d  = gauss_lobatto_1d.pointList();
  const auto& weight_list_1d = gauss_lobatto_1d.weightList();

  size_t l = 0;
  for (size_t i = 0; i < nb_points_1d; ++i) {
    for (size_t j = 0; j < nb_points_1d; ++j) {
      for (size_t k = 0; k < nb_points_1d; ++k, ++l) {
        point_list[l]  = TinyVector<3>{point_list_1d[i][0], point_list_1d[j][0], point_list_1d[k][0]};
        weight_list[l] = weight_list_1d[i] * weight_list_1d[j] * weight_list_1d[k];
      }
    }
  }

  this->m_point_list  = point_list;
  this->m_weight_list = weight_list;
}
