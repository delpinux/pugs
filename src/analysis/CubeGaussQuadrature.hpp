#ifndef CUBE_GAUSS_QUADRATURE_HPP
#define CUBE_GAUSS_QUADRATURE_HPP

#include <analysis/QuadratureFormula.hpp>

/**
 * Defines Cube Gauss quadrature on the reference element
 * \f$]-1,1[^3\f$.
 *
 * \note formulae are provided by
 *
 * "Addendum to the paper 'High-order symmetric cubature rules for
 * tetrahedra and pyramids'" Jan Jaśkowiec & N. Sukumar (2020).
 */
class CubeGaussQuadrature final : public QuadratureFormula<3>
{
 public:
  constexpr static size_t max_degree = 21;

 private:
  void _buildPointAndWeightLists(const size_t degree);

 public:
  CubeGaussQuadrature(CubeGaussQuadrature&&)      = default;
  CubeGaussQuadrature(const CubeGaussQuadrature&) = default;

 private:
  friend class QuadratureManager;

  explicit CubeGaussQuadrature(const size_t degree) : QuadratureFormula<3>{QuadratureType::Gauss}
  {
    this->_buildPointAndWeightLists(degree);
  }

  CubeGaussQuadrature()  = delete;
  ~CubeGaussQuadrature() = default;
};

#endif   // CUBE_GAUSS_QUADRATURE_HPP
