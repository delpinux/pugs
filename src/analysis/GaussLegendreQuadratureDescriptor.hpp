#ifndef GAUSS_LEGENDRE_QUADRATURE_DESCRIPTOR_HPP
#define GAUSS_LEGENDRE_QUADRATURE_DESCRIPTOR_HPP

#include <analysis/IQuadratureDescriptor.hpp>

#include <sstream>

class GaussLegendreQuadratureDescriptor final : public IQuadratureDescriptor
{
 private:
  size_t m_degree;

 public:
  bool
  isTensorial() const
  {
    return true;
  }

  QuadratureType
  type() const
  {
    return QuadratureType::GaussLegendre;
  }

  size_t
  degree() const
  {
    return m_degree;
  }

  std::string
  name() const
  {
    std::ostringstream os;
    os << ::name(this->type()) << "(" << m_degree << ")";
    return os.str();
  }

  GaussLegendreQuadratureDescriptor(size_t degree) : m_degree{degree} {}
  GaussLegendreQuadratureDescriptor() noexcept = delete;

  GaussLegendreQuadratureDescriptor(const GaussLegendreQuadratureDescriptor&) = default;

  GaussLegendreQuadratureDescriptor(GaussLegendreQuadratureDescriptor&&) noexcept = default;

  virtual ~GaussLegendreQuadratureDescriptor() noexcept = default;
};

#endif   // GAUSS_LEGENDRE_QUADRATURE_DESCRIPTOR_HPP
