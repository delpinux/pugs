#ifndef QUADRATURE_MANAGER_HPP
#define QUADRATURE_MANAGER_HPP

#include <analysis/IQuadratureDescriptor.hpp>
#include <analysis/QuadratureFormula.hpp>
#include <utils/Array.hpp>
#include <utils/Exceptions.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>
#include <utils/Stringify.hpp>

class QuadratureManager
{
 private:
  static QuadratureManager* s_instance;

  Array<const QuadratureFormula<1>> m_line_gauss_legendre_formula_list;
  Array<const QuadratureFormula<1>> m_line_gauss_lobatto_formula_list;

  Array<const QuadratureFormula<2>> m_square_gauss_formula_list;
  Array<const QuadratureFormula<2>> m_square_gauss_legendre_formula_list;
  Array<const QuadratureFormula<2>> m_square_gauss_lobatto_formula_list;
  Array<const QuadratureFormula<2>> m_triangle_gauss_formula_list;

  Array<const QuadratureFormula<3>> m_cube_gauss_formula_list;
  Array<const QuadratureFormula<3>> m_cube_gauss_legendre_formula_list;
  Array<const QuadratureFormula<3>> m_cube_gauss_lobatto_formula_list;
  Array<const QuadratureFormula<3>> m_prism_gauss_formula_list;
  Array<const QuadratureFormula<3>> m_pyramid_gauss_formula_list;
  Array<const QuadratureFormula<3>> m_tetrahedron_gauss_formula_list;

  Array<const QuadratureFormula<1>> _buildLineGaussLobattoFormulaList();
  Array<const QuadratureFormula<1>> _buildLineGaussLegendreFormulaList();

  Array<const QuadratureFormula<2>> _buildSquareGaussFormulaList();
  Array<const QuadratureFormula<2>> _buildSquareGaussLegendreFormulaList();
  Array<const QuadratureFormula<2>> _buildSquareGaussLobattoFormulaList();
  Array<const QuadratureFormula<2>> _buildTriangleGaussFormulaList();

  Array<const QuadratureFormula<3>> _buildCubeGaussFormulaList();
  Array<const QuadratureFormula<3>> _buildCubeGaussLegendreFormulaList();
  Array<const QuadratureFormula<3>> _buildCubeGaussLobattoFormulaList();
  Array<const QuadratureFormula<3>> _buildPrismGaussFormulaList();
  Array<const QuadratureFormula<3>> _buildPyramidGaussFormulaList();
  Array<const QuadratureFormula<3>> _buildTetrahedronGaussFormulaList();

 public:
  const QuadratureFormula<1>&
  getLineFormula(const IQuadratureDescriptor& quadrature_descriptor) const
  {
    if (quadrature_descriptor.degree() > maxLineDegree(quadrature_descriptor.type())) {
      throw NormalError(::name(quadrature_descriptor.type()) + " quadrature formulae handle degrees up to " +
                        stringify(maxLineDegree(quadrature_descriptor.type())) + " on lines");
    }
    switch (quadrature_descriptor.type()) {
    case QuadratureType::Gauss:
    case QuadratureType::GaussLegendre: {
      return m_line_gauss_legendre_formula_list[quadrature_descriptor.degree() / 2];
      break;
    }
    case QuadratureType::GaussLobatto: {
      return m_line_gauss_lobatto_formula_list[quadrature_descriptor.degree() / 2];
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError(::name(quadrature_descriptor.type()) + " is not defined on lines");
    }
      // LCOV_EXCL_STOP
    }
  }

  const QuadratureFormula<2>&
  getTriangleFormula(const IQuadratureDescriptor& quadrature_descriptor) const
  {
    if (quadrature_descriptor.degree() > maxTriangleDegree(quadrature_descriptor.type())) {
      throw NormalError(::name(quadrature_descriptor.type()) + " quadrature formulae handle degrees up to " +
                        stringify(maxTriangleDegree(quadrature_descriptor.type())) + " on triangles");
    }
    switch (quadrature_descriptor.type()) {
    case QuadratureType::Gauss: {
      return m_triangle_gauss_formula_list[quadrature_descriptor.degree() - 1];
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError(::name(quadrature_descriptor.type()) + " is not defined on triangles");
    }
      // LCOV_EXCL_STOP
    }
  }

  const QuadratureFormula<2>&
  getSquareFormula(const IQuadratureDescriptor& quadrature_descriptor) const
  {
    if (quadrature_descriptor.degree() > maxSquareDegree(quadrature_descriptor.type())) {
      throw NormalError(::name(quadrature_descriptor.type()) + " quadrature formulae handle degrees up to " +
                        stringify(maxSquareDegree(quadrature_descriptor.type())) + " on squares");
    }
    switch (quadrature_descriptor.type()) {
    case QuadratureType::Gauss: {
      return m_square_gauss_formula_list[quadrature_descriptor.degree() / 2];
    }
    case QuadratureType::GaussLegendre: {
      return m_square_gauss_legendre_formula_list[quadrature_descriptor.degree() / 2];
    }
    case QuadratureType::GaussLobatto: {
      return m_square_gauss_lobatto_formula_list[quadrature_descriptor.degree() / 2];
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError(::name(quadrature_descriptor.type()) + " is not defined on squares");
    }
      // LCOV_EXCL_STOP
    }
  }

  const QuadratureFormula<3>&
  getTetrahedronFormula(const IQuadratureDescriptor& quadrature_descriptor) const
  {
    if (quadrature_descriptor.degree() > maxTetrahedronDegree(quadrature_descriptor.type())) {
      throw NormalError(::name(quadrature_descriptor.type()) + " quadrature formulae handle degrees up to " +
                        stringify(maxTetrahedronDegree(quadrature_descriptor.type())) + " on tetrahedra");
    }
    switch (quadrature_descriptor.type()) {
    case QuadratureType::Gauss: {
      return m_tetrahedron_gauss_formula_list[quadrature_descriptor.degree() - 1];
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError(::name(quadrature_descriptor.type()) + " is not defined on tetrahedra");
    }
      // LCOV_EXCL_STOP
    }
  }

  const QuadratureFormula<3>&
  getPrismFormula(const IQuadratureDescriptor& quadrature_descriptor) const
  {
    if (quadrature_descriptor.degree() > maxPrismDegree(quadrature_descriptor.type())) {
      throw NormalError(::name(quadrature_descriptor.type()) + " quadrature formulae handle degrees up to " +
                        stringify(maxPrismDegree(quadrature_descriptor.type())) + " on prisms");
    }
    switch (quadrature_descriptor.type()) {
    case QuadratureType::Gauss: {
      return m_prism_gauss_formula_list[quadrature_descriptor.degree() - 1];
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError(::name(quadrature_descriptor.type()) + " is not defined on prisms");
    }
      // LCOV_EXCL_STOP
    }
  }

  const QuadratureFormula<3>&
  getPyramidFormula(const IQuadratureDescriptor& quadrature_descriptor) const
  {
    if (quadrature_descriptor.degree() > maxPyramidDegree(quadrature_descriptor.type())) {
      throw NormalError(::name(quadrature_descriptor.type()) + " quadrature formulae handle degrees up to " +
                        stringify(maxPyramidDegree(quadrature_descriptor.type())) + " on pyramids");
    }
    switch (quadrature_descriptor.type()) {
    case QuadratureType::Gauss: {
      return m_pyramid_gauss_formula_list[quadrature_descriptor.degree() - 1];
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError(::name(quadrature_descriptor.type()) + " is not defined on pyramid");
    }
      // LCOV_EXCL_STOP
    }
  }

  const QuadratureFormula<3>&
  getCubeFormula(const IQuadratureDescriptor& quadrature_descriptor) const
  {
    if (quadrature_descriptor.degree() > maxCubeDegree(quadrature_descriptor.type())) {
      throw NormalError(::name(quadrature_descriptor.type()) + " quadrature formulae handle degrees up to " +
                        stringify(maxCubeDegree(quadrature_descriptor.type())) + " on cubes");
    }
    switch (quadrature_descriptor.type()) {
    case QuadratureType::Gauss: {
      return m_cube_gauss_formula_list[quadrature_descriptor.degree() / 2];
    }
    case QuadratureType::GaussLegendre: {
      return m_cube_gauss_legendre_formula_list[quadrature_descriptor.degree() / 2];
      break;
    }
    case QuadratureType::GaussLobatto: {
      return m_cube_gauss_lobatto_formula_list[quadrature_descriptor.degree() / 2];
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError(::name(quadrature_descriptor.type()) + " is not defined on cubes");
    }
      // LCOV_EXCL_STOP
    }
  }

  size_t
  maxLineDegree(const QuadratureType type) const
  {
    switch (type) {
    case QuadratureType::Gauss:
    case QuadratureType::GaussLegendre: {
      return m_line_gauss_legendre_formula_list.size() * 2 - 1;
    }
    case QuadratureType::GaussLobatto: {
      return m_line_gauss_lobatto_formula_list.size() * 2 - 1;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("invalid quadrature type");
    }
      // LCOV_EXCL_STOP
    }
  }

  size_t
  maxSquareDegree(const QuadratureType type) const
  {
    switch (type) {
    case QuadratureType::Gauss: {
      return m_square_gauss_formula_list.size() * 2 - 1;
    }
    case QuadratureType::GaussLegendre: {
      return m_square_gauss_legendre_formula_list.size() * 2 - 1;
    }
    case QuadratureType::GaussLobatto: {
      return m_square_gauss_lobatto_formula_list.size() * 2 - 1;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("invalid quadrature type");
    }
      // LCOV_EXCL_STOP
    }
  }

  size_t
  maxTriangleDegree(const QuadratureType type) const
  {
    switch (type) {
    case QuadratureType::Gauss: {
      return m_triangle_gauss_formula_list.size();
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError(::name(type) + " is not defined on triangle");
    }
      // LCOV_EXCL_STOP
    }
  }

  size_t
  maxCubeDegree(const QuadratureType type) const
  {
    switch (type) {
    case QuadratureType::Gauss: {
      return m_cube_gauss_formula_list.size() * 2 - 1;
    }
    case QuadratureType::GaussLegendre: {
      return m_cube_gauss_legendre_formula_list.size() * 2 - 1;
    }
    case QuadratureType::GaussLobatto: {
      return m_cube_gauss_lobatto_formula_list.size() * 2 - 1;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("invalid quadrature type");
    }
      // LCOV_EXCL_STOP
    }
  }

  size_t
  maxPrismDegree(const QuadratureType type) const
  {
    switch (type) {
    case QuadratureType::Gauss: {
      return m_prism_gauss_formula_list.size();
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError(::name(type) + " is not defined on prism");
    }
      // LCOV_EXCL_STOP
    }
  }

  size_t
  maxPyramidDegree(const QuadratureType type) const
  {
    switch (type) {
    case QuadratureType::Gauss: {
      return m_pyramid_gauss_formula_list.size();
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError(::name(type) + " is not defined on pyramid");
    }
      // LCOV_EXCL_STOP
    }
  }

  size_t
  maxTetrahedronDegree(const QuadratureType type) const
  {
    switch (type) {
    case QuadratureType::Gauss: {
      return m_tetrahedron_gauss_formula_list.size();
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError(::name(type) + " is not defined on tetrahedron");
    }
      // LCOV_EXCL_STOP
    }
  }

  static void create();
  static void destroy();

  PUGS_INLINE
  static const QuadratureManager&
  instance()
  {
    Assert(s_instance != nullptr, "QuadratureManager was not created!");
    return *s_instance;
  }

 private:
  QuadratureManager(const QuadratureManager&) = delete;
  QuadratureManager(QuadratureManager&&)      = delete;

  QuadratureManager();
  ~QuadratureManager() = default;
};

#endif   // QUADRATURE_MANAGER_HPP
