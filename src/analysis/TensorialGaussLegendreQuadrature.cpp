#include <analysis/TensorialGaussLegendreQuadrature.hpp>
#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

template <>
void
TensorialGaussLegendreQuadrature<1>::_buildPointAndWeightLists(const size_t degree)
{
  using R1 = TinyVector<1>;

  const size_t nb_points = degree / 2 + 1;

  SmallArray<R1> point_list(nb_points);
  SmallArray<double> weight_list(nb_points);

  switch (degree) {
  case 0:
  case 1: {
    point_list[0] = R1{0};

    weight_list[0] = 2;
    break;
  }
  case 2:
  case 3: {
    point_list[0] = R1{-0.5773502691896257645091488};
    point_list[1] = R1{+0.5773502691896257645091488};

    weight_list[0] = 1;
    weight_list[1] = 1;
    break;
  }
  case 4:
  case 5: {
    point_list[0] = R1{-0.7745966692414833770358531};
    point_list[1] = R1{0};
    point_list[2] = R1{+0.7745966692414833770358531};

    weight_list[0] = 0.5555555555555555555555556;
    weight_list[1] = 0.8888888888888888888888889;
    weight_list[2] = 0.5555555555555555555555556;
    break;
  }
  case 6:
  case 7: {
    point_list[0] = R1{-0.8611363115940525752239465};
    point_list[1] = R1{-0.3399810435848562648026658};
    point_list[2] = R1{+0.3399810435848562648026658};
    point_list[3] = R1{+0.8611363115940525752239465};

    weight_list[0] = 0.3478548451374538573730639;
    weight_list[1] = 0.6521451548625461426269361;
    weight_list[2] = 0.6521451548625461426269361;
    weight_list[3] = 0.3478548451374538573730639;
    break;
  }
  case 8:
  case 9: {
    point_list[0] = R1{-0.9061798459386639927976269};
    point_list[1] = R1{-0.5384693101056830910363144};
    point_list[2] = R1{0};
    point_list[3] = R1{+0.5384693101056830910363144};
    point_list[4] = R1{+0.9061798459386639927976269};

    weight_list[0] = 0.2369268850561890875142640;
    weight_list[1] = 0.4786286704993664680412915;
    weight_list[2] = 0.5688888888888888888888889;
    weight_list[3] = 0.4786286704993664680412915;
    weight_list[4] = 0.2369268850561890875142640;
    break;
  }
  case 10:
  case 11: {
    point_list[0] = R1{-0.9324695142031520278123016};
    point_list[1] = R1{-0.6612093864662645136613996};
    point_list[2] = R1{-0.2386191860831969086305017};
    point_list[3] = R1{+0.2386191860831969086305017};
    point_list[4] = R1{+0.6612093864662645136613996};
    point_list[5] = R1{+0.9324695142031520278123016};

    weight_list[0] = 0.1713244923791703450402961;
    weight_list[1] = 0.3607615730481386075698335;
    weight_list[2] = 0.4679139345726910473898703;
    weight_list[3] = 0.4679139345726910473898703;
    weight_list[4] = 0.3607615730481386075698335;
    weight_list[5] = 0.1713244923791703450402961;
    break;
  }
  case 12:
  case 13: {
    point_list[0] = R1{-0.9491079123427585245261897};
    point_list[1] = R1{-0.7415311855993944398638648};
    point_list[2] = R1{-0.4058451513773971669066064};
    point_list[3] = R1{0};
    point_list[4] = R1{+0.4058451513773971669066064};
    point_list[5] = R1{+0.7415311855993944398638648};
    point_list[6] = R1{+0.9491079123427585245261897};

    weight_list[0] = 0.1294849661688696932706114;
    weight_list[1] = 0.2797053914892766679014678;
    weight_list[2] = 0.3818300505051189449503698;
    weight_list[3] = 0.4179591836734693877551020;
    weight_list[4] = 0.3818300505051189449503698;
    weight_list[5] = 0.2797053914892766679014678;
    weight_list[6] = 0.1294849661688696932706114;
    break;
  }
  case 14:
  case 15: {
    point_list[0] = R1{-0.9602898564975362316835609};
    point_list[1] = R1{-0.7966664774136267395915539};
    point_list[2] = R1{-0.5255324099163289858177390};
    point_list[3] = R1{-0.1834346424956498049394761};
    point_list[4] = R1{+0.1834346424956498049394761};
    point_list[5] = R1{+0.5255324099163289858177390};
    point_list[6] = R1{+0.7966664774136267395915539};
    point_list[7] = R1{+0.9602898564975362316835609};

    weight_list[0] = 0.1012285362903762591525314;
    weight_list[1] = 0.2223810344533744705443560;
    weight_list[2] = 0.3137066458778872873379622;
    weight_list[3] = 0.3626837833783619829651504;
    weight_list[4] = 0.3626837833783619829651504;
    weight_list[5] = 0.3137066458778872873379622;
    weight_list[6] = 0.2223810344533744705443560;
    weight_list[7] = 0.1012285362903762591525314;
    break;
  }
  case 16:
  case 17: {
    point_list[0] = R1{-0.9681602395076260898355762};
    point_list[1] = R1{-0.8360311073266357942994298};
    point_list[2] = R1{-0.6133714327005903973087020};
    point_list[3] = R1{-0.3242534234038089290385380};
    point_list[4] = R1{0};
    point_list[5] = R1{+0.3242534234038089290385380};
    point_list[6] = R1{+0.6133714327005903973087020};
    point_list[7] = R1{+0.8360311073266357942994298};
    point_list[8] = R1{+0.9681602395076260898355762};

    weight_list[0] = 0.0812743883615744119718922;
    weight_list[1] = 0.1806481606948574040584720;
    weight_list[2] = 0.2606106964029354623187429;
    weight_list[3] = 0.3123470770400028400686304;
    weight_list[4] = 0.3302393550012597631645251;
    weight_list[5] = 0.3123470770400028400686304;
    weight_list[6] = 0.2606106964029354623187429;
    weight_list[7] = 0.1806481606948574040584720;
    weight_list[8] = 0.0812743883615744119718922;
    break;
  }
  case 18:
  case 19: {
    point_list[0] = R1{-0.9739065285171717200779640};
    point_list[1] = R1{-0.8650633666889845107320967};
    point_list[2] = R1{-0.6794095682990244062343274};
    point_list[3] = R1{-0.4333953941292471907992659};
    point_list[4] = R1{-0.1488743389816312108848260};
    point_list[5] = R1{+0.1488743389816312108848260};
    point_list[6] = R1{+0.4333953941292471907992659};
    point_list[7] = R1{+0.6794095682990244062343274};
    point_list[8] = R1{+0.8650633666889845107320967};
    point_list[9] = R1{+0.9739065285171717200779640};

    weight_list[0] = 0.0666713443086881375935688;
    weight_list[1] = 0.1494513491505805931457763;
    weight_list[2] = 0.2190863625159820439955349;
    weight_list[3] = 0.2692667193099963550912269;
    weight_list[4] = 0.2955242247147528701738930;
    weight_list[5] = 0.2955242247147528701738930;
    weight_list[6] = 0.2692667193099963550912269;
    weight_list[7] = 0.2190863625159820439955349;
    weight_list[8] = 0.1494513491505805931457763;
    weight_list[9] = 0.0666713443086881375935688;
    break;
  }
  case 20:
  case 21: {
    point_list[0]  = R1{-0.9782286581460569928039380};
    point_list[1]  = R1{-0.8870625997680952990751578};
    point_list[2]  = R1{-0.7301520055740493240934163};
    point_list[3]  = R1{-0.5190961292068118159257257};
    point_list[4]  = R1{-0.2695431559523449723315320};
    point_list[5]  = R1{0};
    point_list[6]  = R1{+0.2695431559523449723315320};
    point_list[7]  = R1{+0.5190961292068118159257257};
    point_list[8]  = R1{+0.7301520055740493240934163};
    point_list[9]  = R1{+0.8870625997680952990751578};
    point_list[10] = R1{+0.9782286581460569928039380};

    weight_list[0]  = 0.0556685671161736664827537;
    weight_list[1]  = 0.1255803694649046246346940;
    weight_list[2]  = 0.1862902109277342514260980;
    weight_list[3]  = 0.2331937645919904799185237;
    weight_list[4]  = 0.2628045445102466621806889;
    weight_list[5]  = 0.2729250867779006307144835;
    weight_list[6]  = 0.2628045445102466621806889;
    weight_list[7]  = 0.2331937645919904799185237;
    weight_list[8]  = 0.1862902109277342514260980;
    weight_list[9]  = 0.1255803694649046246346940;
    weight_list[10] = 0.0556685671161736664827537;
    break;
  }
  case 22:
  case 23: {
    point_list[0]  = R1{-0.9815606342467192506905491};
    point_list[1]  = R1{-0.9041172563704748566784659};
    point_list[2]  = R1{-0.7699026741943046870368938};
    point_list[3]  = R1{-0.5873179542866174472967024};
    point_list[4]  = R1{-0.3678314989981801937526915};
    point_list[5]  = R1{-0.1252334085114689154724414};
    point_list[6]  = R1{+0.1252334085114689154724414};
    point_list[7]  = R1{+0.3678314989981801937526915};
    point_list[8]  = R1{+0.5873179542866174472967024};
    point_list[9]  = R1{+0.7699026741943046870368938};
    point_list[10] = R1{+0.9041172563704748566784659};
    point_list[11] = R1{+0.9815606342467192506905491};

    weight_list[0]  = 0.0471753363865118271946160;
    weight_list[1]  = 0.1069393259953184309602547;
    weight_list[2]  = 0.1600783285433462263346525;
    weight_list[3]  = 0.2031674267230659217490645;
    weight_list[4]  = 0.2334925365383548087608499;
    weight_list[5]  = 0.2491470458134027850005624;
    weight_list[6]  = 0.2491470458134027850005624;
    weight_list[7]  = 0.2334925365383548087608499;
    weight_list[8]  = 0.2031674267230659217490645;
    weight_list[9]  = 0.1600783285433462263346525;
    weight_list[10] = 0.1069393259953184309602547;
    weight_list[11] = 0.0471753363865118271946160;
    break;
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("Gauss-Legendre quadrature formulae handle degrees up to " +
                          stringify(TensorialGaussLegendreQuadrature<1>::max_degree));
  }
    // LCOV_EXCL_STOP
  }

  m_point_list  = point_list;
  m_weight_list = weight_list;
}

template <>
void
TensorialGaussLegendreQuadrature<2>::_buildPointAndWeightLists(const size_t degree)
{
  const size_t nb_points_1d = degree / 2 + 1;
  const size_t nb_points    = nb_points_1d * nb_points_1d;

  SmallArray<TinyVector<2>> point_list(nb_points);
  SmallArray<double> weight_list(nb_points);

  TensorialGaussLegendreQuadrature<1> gauss_legendre_1d(degree);
  const auto& point_list_1d  = gauss_legendre_1d.pointList();
  const auto& weight_list_1d = gauss_legendre_1d.weightList();

  size_t l = 0;
  for (size_t i = 0; i < nb_points_1d; ++i) {
    for (size_t j = 0; j < nb_points_1d; ++j, ++l) {
      point_list[l]  = TinyVector<2>{point_list_1d[i][0], point_list_1d[j][0]};
      weight_list[l] = weight_list_1d[i] * weight_list_1d[j];
    }
  }

  this->m_point_list  = point_list;
  this->m_weight_list = weight_list;
}

template <>
void
TensorialGaussLegendreQuadrature<3>::_buildPointAndWeightLists(const size_t degree)
{
  const size_t nb_points_1d = degree / 2 + 1;
  const size_t nb_points    = nb_points_1d * nb_points_1d * nb_points_1d;

  SmallArray<TinyVector<3>> point_list(nb_points);
  SmallArray<double> weight_list(nb_points);

  TensorialGaussLegendreQuadrature<1> gauss_legendre_1d(degree);
  const auto& point_list_1d  = gauss_legendre_1d.pointList();
  const auto& weight_list_1d = gauss_legendre_1d.weightList();

  size_t l = 0;
  for (size_t i = 0; i < nb_points_1d; ++i) {
    for (size_t j = 0; j < nb_points_1d; ++j) {
      for (size_t k = 0; k < nb_points_1d; ++k, ++l) {
        point_list[l]  = TinyVector<3>{point_list_1d[i][0], point_list_1d[j][0], point_list_1d[k][0]};
        weight_list[l] = weight_list_1d[i] * weight_list_1d[j] * weight_list_1d[k];
      }
    }
  }

  this->m_point_list  = point_list;
  this->m_weight_list = weight_list;
}
