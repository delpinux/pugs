#ifndef QUADRATURE_FORMULA_HPP
#define QUADRATURE_FORMULA_HPP

#include <algebra/TinyVector.hpp>
#include <analysis/QuadratureType.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>
#include <utils/SmallArray.hpp>

template <size_t Dimension>
class QuadratureFormula
{
 public:
 protected:
  QuadratureType m_type;
  SmallArray<const TinyVector<Dimension>> m_point_list;
  SmallArray<const double> m_weight_list;

 public:
  PUGS_INLINE size_t
  numberOfPoints() const
  {
    Assert(m_point_list.size() == m_weight_list.size());
    return m_point_list.size();
  }

  PUGS_INLINE
  const SmallArray<const TinyVector<Dimension>>&
  pointList() const
  {
    return m_point_list;
  }

  PUGS_INLINE
  const TinyVector<Dimension>&
  point(const size_t i) const
  {
    return m_point_list[i];
  }

  PUGS_INLINE
  const SmallArray<const double>&
  weightList() const
  {
    return m_weight_list;
  }

  PUGS_INLINE
  const double&
  weight(const size_t i) const
  {
    return m_weight_list[i];
  }

 protected:
  explicit QuadratureFormula(const QuadratureType type) : m_type{type} {}

 public:
  QuadratureFormula()          = default;
  virtual ~QuadratureFormula() = default;
};

#endif   // QUADRATURE_FORMULA_HPP
