#ifndef REF_ID_HPP
#define REF_ID_HPP

#include <utils/Stringify.hpp>

#include <iostream>
#include <string>

class RefId
{
 public:
  using TagNumberType = unsigned int;
  using TagNameType   = std::string;

 private:
  TagNumberType m_tag_number;
  TagNameType m_tag_name;

 public:
  friend std::ostream&
  operator<<(std::ostream& os, const RefId& ref_id)
  {
    if (stringify(ref_id.m_tag_number) != ref_id.m_tag_name) {
      os << ref_id.m_tag_name << '(' << ref_id.m_tag_number << ')';
    } else {
      os << ref_id.m_tag_number;
    }
    return os;
  }

  bool
  operator==(const RefId& ref_id) const
  {
    return ((m_tag_number == ref_id.m_tag_number) and (m_tag_name == ref_id.m_tag_name));
  }

  bool
  operator<(const RefId& ref_id) const
  {
    return ((m_tag_number < ref_id.m_tag_number) or
            ((m_tag_number == ref_id.m_tag_number) and (m_tag_name < ref_id.m_tag_name)));
  }

  TagNumberType
  tagNumber() const
  {
    return m_tag_number;
  }

  const TagNameType&
  tagName() const
  {
    return m_tag_name;
  }

  RefId& operator=(const RefId&) = default;
  RefId& operator=(RefId&&) = default;
  RefId()                   = default;
  RefId(const RefId&)       = default;
  RefId(RefId&&)            = default;

  explicit RefId(TagNumberType tag_number, const TagNameType& tag_name) : m_tag_number(tag_number), m_tag_name(tag_name)
  {
    ;
  }

  explicit RefId(TagNumberType tag_number) : m_tag_number(tag_number), m_tag_name(stringify(tag_number))
  {
    ;
  }

  ~RefId() = default;
};

#endif   // REF_ID_HPP
