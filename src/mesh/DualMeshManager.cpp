#include <mesh/DualMeshManager.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/DiamondDualMeshBuilder.hpp>
#include <mesh/Dual1DMeshBuilder.hpp>
#include <mesh/MedianDualMeshBuilder.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>
#include <utils/Exceptions.hpp>
#include <utils/PugsAssert.hpp>

#include <sstream>

DualMeshManager* DualMeshManager::m_instance{nullptr};

void
DualMeshManager::create()
{
  Assert(m_instance == nullptr, "DualMeshManager is already created");
  m_instance = new DualMeshManager;
}

void
DualMeshManager::destroy()
{
  Assert(m_instance != nullptr, "DualMeshManager was not created!");

  if (m_instance->m_mesh_to_dual_mesh_map.size() > 0) {
    // LCOV_EXCL_START
    std::stringstream error;
    error << ": some meshes are still registered\n";
    for (const auto& [key, parent_mesh] : m_instance->m_mesh_to_dual_mesh_map) {
      error << " - mesh " << rang::fgB::magenta << key.second << rang::style::reset << ": " << name(key.first)
            << " dual mesh of " << rang::fgB::yellow << parent_mesh.get() << rang::style::reset << '\n';
    }
    throw UnexpectedError(error.str());
    // LCOV_EXCL_STOP
  }
  delete m_instance;
  m_instance = nullptr;
}

void
DualMeshManager::deleteMesh(const size_t mesh_id)
{
  bool has_removed = false;
  do {
    has_removed = false;
    for (const auto& [key, dual_mesh] : m_mesh_to_dual_mesh_map) {
      const auto& [type, p_parent_mesh_id] = key;
      if (mesh_id == p_parent_mesh_id) {
        m_mesh_to_dual_mesh_map.erase(key);
        has_removed = true;
        break;
      }
    }
  } while (has_removed);
}

std::shared_ptr<const MeshVariant>
DualMeshManager::getDual1DMesh(const std::shared_ptr<const MeshVariant>& mesh_v)
{
  auto key = std::make_pair(DualMeshType::Dual1D, mesh_v->id());
  if (auto i_mesh_to_dual_mesh = m_mesh_to_dual_mesh_map.find(key);
      i_mesh_to_dual_mesh != m_mesh_to_dual_mesh_map.end()) {
    return i_mesh_to_dual_mesh->second;
  } else {
    Dual1DMeshBuilder builder{mesh_v};
    m_mesh_to_dual_mesh_map[key] = builder.mesh();
    return builder.mesh();
    ;
  }
}

std::shared_ptr<const MeshVariant>
DualMeshManager::getMedianDualMesh(const std::shared_ptr<const MeshVariant>& mesh_v)
{
  return std::visit(
    [&](auto&& p_mesh) {
      using MeshType = mesh_type_t<decltype(p_mesh)>;
      if constexpr (MeshType::Dimension == 1) {
        return this->getDual1DMesh(mesh_v);
      } else {
        auto key = std::make_pair(DualMeshType::Median, mesh_v->id());
        if (auto i_mesh_data = m_mesh_to_dual_mesh_map.find(key); i_mesh_data != m_mesh_to_dual_mesh_map.end()) {
          return i_mesh_data->second;
        } else {
          MedianDualMeshBuilder builder{mesh_v};

          m_mesh_to_dual_mesh_map[key] = builder.mesh();
          return builder.mesh();
        }
      }
    },
    mesh_v->variant());
}

std::shared_ptr<const MeshVariant>
DualMeshManager::getDiamondDualMesh(const std::shared_ptr<const MeshVariant>& mesh_v)
{
  return std::visit(
    [&](auto&& p_mesh) {
      using MeshType = mesh_type_t<decltype(p_mesh)>;
      if constexpr (MeshType::Dimension == 1) {
        return this->getDual1DMesh(mesh_v);
      } else {
        auto key = std::make_pair(DualMeshType::Diamond, mesh_v->id());
        if (auto i_mesh_data = m_mesh_to_dual_mesh_map.find(key); i_mesh_data != m_mesh_to_dual_mesh_map.end()) {
          return i_mesh_data->second;
        } else {
          DiamondDualMeshBuilder builder{mesh_v};

          m_mesh_to_dual_mesh_map[key] = builder.mesh();
          return builder.mesh();
        }
      }
    },
    mesh_v->variant());
}
