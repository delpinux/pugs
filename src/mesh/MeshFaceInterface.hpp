#ifndef MESH_FACE_INTERFACE_HPP
#define MESH_FACE_INTERFACE_HPP

#include <algebra/TinyVector.hpp>
#include <mesh/IInterfaceDescriptor.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/RefItemList.hpp>
#include <utils/Array.hpp>

class [[nodiscard]] MeshFaceInterface   // clazy:exclude=copyable-polymorphic
{
 protected:
  RefFaceList m_ref_face_list;

 public:
  template <MeshConcept MeshType>
  friend MeshFaceInterface getMeshFaceInterface(const MeshType& mesh, const IInterfaceDescriptor& interface_descriptor);

  MeshFaceInterface& operator=(const MeshFaceInterface&) = default;
  MeshFaceInterface& operator=(MeshFaceInterface&&) = default;

  PUGS_INLINE
  const RefFaceList& refFaceList() const
  {
    return m_ref_face_list;
  }

  PUGS_INLINE
  const Array<const FaceId>& faceList() const
  {
    return m_ref_face_list.list();
  }

 protected:
  template <MeshConcept MeshType>
  MeshFaceInterface(const MeshType& mesh, const RefFaceList& ref_face_list);

 public:
  MeshFaceInterface(const MeshFaceInterface&) = default;   // LCOV_EXCL_LINE
  MeshFaceInterface(MeshFaceInterface &&)     = default;   // LCOV_EXCL_LINE

  MeshFaceInterface()          = default;
  virtual ~MeshFaceInterface() = default;
};

template <MeshConcept MeshType>
MeshFaceInterface getMeshFaceInterface(const MeshType& mesh, const IInterfaceDescriptor& interface_descriptor);

#endif   // MESH_FACE_INTERFACE_HPP
