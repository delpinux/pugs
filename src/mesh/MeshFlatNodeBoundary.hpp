#ifndef MESH_FLAT_NODE_BOUNDARY_HPP
#define MESH_FLAT_NODE_BOUNDARY_HPP

#include <mesh/MeshNodeBoundary.hpp>
#include <mesh/MeshTraits.hpp>

template <MeshConcept MeshType>
class [[nodiscard]] MeshFlatNodeBoundary final : public MeshNodeBoundary   // clazy:exclude=copyable-polymorphic
{
 public:
  static_assert(not std::is_const_v<MeshType>, "MeshType must be non-const");

  using Rd = TinyVector<MeshType::Dimension, double>;

 private:
  const Rd m_outgoing_normal;

  Rd _getFarestNode(const MeshType& mesh, const Rd& x0, const Rd& x1);

  Rd _getNormal(const MeshType& mesh);

  void _checkBoundaryIsFlat(const TinyVector<MeshType::Dimension, double>& normal,
                            const TinyVector<MeshType::Dimension, double>& origin,
                            const double length,
                            const MeshType& mesh) const;

  Rd _getOutgoingNormal(const MeshType& mesh);

 public:
  const Rd&
  outgoingNormal() const
  {
    return m_outgoing_normal;
  }

  MeshFlatNodeBoundary& operator=(const MeshFlatNodeBoundary&) = default;
  MeshFlatNodeBoundary& operator=(MeshFlatNodeBoundary&&)      = default;

  template <typename MeshTypeT>
  friend MeshFlatNodeBoundary<MeshTypeT> getMeshFlatNodeBoundary(const MeshTypeT& mesh,
                                                                 const IBoundaryDescriptor& boundary_descriptor);

 private:
  MeshFlatNodeBoundary(const MeshType& mesh, const RefFaceList& ref_face_list)
    : MeshNodeBoundary(mesh, ref_face_list), m_outgoing_normal(_getOutgoingNormal(mesh))
  {}

  MeshFlatNodeBoundary(const MeshType& mesh, const RefNodeList& ref_node_list)
    : MeshNodeBoundary(mesh, ref_node_list), m_outgoing_normal(_getOutgoingNormal(mesh))
  {}

 public:
  MeshFlatNodeBoundary()                            = default;
  MeshFlatNodeBoundary(const MeshFlatNodeBoundary&) = default;
  MeshFlatNodeBoundary(MeshFlatNodeBoundary&&)      = default;
  ~MeshFlatNodeBoundary()                           = default;
};

template <typename MeshType>
MeshFlatNodeBoundary<MeshType> getMeshFlatNodeBoundary(const MeshType& mesh,
                                                       const IBoundaryDescriptor& boundary_descriptor);

#endif   // MESH_FLAT_NODE_BOUNDARY_HPP
