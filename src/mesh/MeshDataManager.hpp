#ifndef MESH_DATA_MANAGER_HPP
#define MESH_DATA_MANAGER_HPP

#include <mesh/MeshTraits.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>

#include <memory>
#include <unordered_map>

class MeshDataVariant;

template <MeshConcept MeshType>
class MeshData;

class MeshDataManager
{
 private:
  std::unordered_map<size_t, std::shared_ptr<MeshDataVariant>> m_mesh_id_mesh_data_map;

  static MeshDataManager* m_instance;

  MeshDataManager(const MeshDataManager&) = delete;
  MeshDataManager(MeshDataManager&&)      = delete;

  MeshDataManager()  = default;
  ~MeshDataManager() = default;

 public:
  static void create();
  static void destroy();

  PUGS_INLINE
  static MeshDataManager&
  instance()
  {
    Assert(m_instance != nullptr, "MeshDataManager was not created!");
    return *m_instance;
  }

  void deleteMeshData(const size_t mesh_id);

  template <MeshConcept MeshType>
  MeshData<MeshType>& getMeshData(const MeshType&);
};

#endif   // MESH_DATA_MANAGER_HPP
