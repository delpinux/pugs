#include <mesh/MeshLineNodeBoundary.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshNodeBoundaryUtils.hpp>
#include <utils/Messenger.hpp>

template <MeshConcept MeshType>
void
MeshLineNodeBoundary<MeshType>::_checkBoundaryIsLine(const TinyVector<MeshType::Dimension, double>& direction,
                                                     const TinyVector<MeshType::Dimension, double>& origin,
                                                     const double length,
                                                     const MeshType& mesh) const
{
  using Rdxd = TinyMatrix<MeshType::Dimension>;

  const NodeValue<const Rd>& xr = mesh.xr();

  const Rdxd P = Rdxd{identity} - tensorProduct(direction, direction);

  bool is_bad    = false;
  auto node_list = this->m_ref_node_list.list();
  parallel_for(node_list.size(), [=, &is_bad](int i_node) {
    const Rd& x    = xr[node_list[i_node]];
    const Rd delta = P * (x - origin);
    if (dot(delta, delta) > 1E-13 * length) {
      is_bad = true;
    }
  });

  if (parallel::allReduceOr(is_bad)) {
    std::ostringstream ost;
    ost << "invalid boundary \"" << rang::fgB::yellow << this->m_ref_node_list.refId() << rang::style::reset
        << "\": boundary is not a line!";
    throw NormalError(ost.str());
  }
}

template <>
TinyVector<2>
MeshLineNodeBoundary<Mesh<2>>::_getDirection(const Mesh<2>& mesh)
{
  using R2 = TinyVector<2, double>;

  std::array<R2, 2> bounds = getBounds(mesh, m_ref_node_list);

  const R2& xmin = bounds[0];
  const R2& xmax = bounds[1];

  if (xmin == xmax) {
    std::ostringstream ost;
    ost << "invalid boundary \"" << rang::fgB::yellow << m_ref_node_list.refId() << rang::style::reset
        << "\": unable to compute direction";
    throw NormalError(ost.str());
  }

  R2 direction        = xmax - xmin;
  const double length = l2Norm(direction);
  direction *= 1. / length;

  this->_checkBoundaryIsLine(direction, xmin, length, mesh);

  return direction;
}

template <>
TinyVector<3>
MeshLineNodeBoundary<Mesh<3>>::_getDirection(const Mesh<3>& mesh)
{
  using R3 = TinyVector<3, double>;

  std::array<R3, 6> bounds = getBounds(mesh, m_ref_node_list);

  const R3& xmin = bounds[0];
  const R3& ymin = bounds[1];
  const R3& zmin = bounds[2];
  const R3& xmax = bounds[3];
  const R3& ymax = bounds[4];
  const R3& zmax = bounds[5];

  const R3 u = xmax - xmin;
  const R3 v = ymax - ymin;
  const R3 w = zmax - zmin;

  R3 direction = u;
  if (dot(v, v) > dot(direction, direction)) {
    direction = v;
  }

  if (dot(w, w) > dot(direction, direction)) {
    direction = w;
  }

  const double length = l2Norm(direction);
  if (length == 0) {
    std::ostringstream ost;
    ost << "invalid boundary \"" << rang::fgB::yellow << this->m_ref_node_list.refId() << rang::style::reset
        << "\": unable to compute direction";
    throw NormalError(ost.str());
  }
  direction *= 1. / length;

  this->_checkBoundaryIsLine(direction, xmin, length, mesh);

  return direction;
}

template <typename MeshType>
MeshLineNodeBoundary<MeshType>
getMeshLineNodeBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor)
{
  for (size_t i_ref_node_list = 0; i_ref_node_list < mesh.connectivity().template numberOfRefItemList<ItemType::node>();
       ++i_ref_node_list) {
    const auto& ref_node_list = mesh.connectivity().template refItemList<ItemType::node>(i_ref_node_list);
    const RefId& ref          = ref_node_list.refId();
    if (ref == boundary_descriptor) {
      return MeshLineNodeBoundary<MeshType>{mesh, ref_node_list};
    }
  }
  for (size_t i_ref_edge_list = 0; i_ref_edge_list < mesh.connectivity().template numberOfRefItemList<ItemType::edge>();
       ++i_ref_edge_list) {
    const auto& ref_edge_list = mesh.connectivity().template refItemList<ItemType::edge>(i_ref_edge_list);
    const RefId& ref          = ref_edge_list.refId();
    if (ref == boundary_descriptor) {
      return MeshLineNodeBoundary<MeshType>{mesh, ref_edge_list};
    }
  }
  for (size_t i_ref_face_list = 0; i_ref_face_list < mesh.connectivity().template numberOfRefItemList<ItemType::face>();
       ++i_ref_face_list) {
    const auto& ref_face_list = mesh.connectivity().template refItemList<ItemType::face>(i_ref_face_list);
    const RefId& ref          = ref_face_list.refId();
    if (ref == boundary_descriptor) {
      return MeshLineNodeBoundary<MeshType>{mesh, ref_face_list};
    }
  }

  std::ostringstream ost;
  ost << "cannot find surface with name " << rang::fgB::red << boundary_descriptor << rang::style::reset;

  throw NormalError(ost.str());
}

template MeshLineNodeBoundary<Mesh<2>> getMeshLineNodeBoundary(const Mesh<2>&, const IBoundaryDescriptor&);
template MeshLineNodeBoundary<Mesh<3>> getMeshLineNodeBoundary(const Mesh<3>&, const IBoundaryDescriptor&);
