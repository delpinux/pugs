#ifndef I_INTERFACE_DESCRIPTOR_HPP
#define I_INTERFACE_DESCRIPTOR_HPP

#include <mesh/RefId.hpp>

#include <iostream>

class IInterfaceDescriptor
{
 public:
  enum class Type
  {
    named,
    numbered
  };

 protected:
  virtual std::ostream& _write(std::ostream& os) const = 0;

 public:
  friend std::ostream&
  operator<<(std::ostream& os, const IInterfaceDescriptor& interface_descriptor)
  {
    return interface_descriptor._write(os);
  }

  [[nodiscard]] virtual bool operator==(const RefId& ref_id) const = 0;

  [[nodiscard]] friend bool
  operator==(const RefId& ref_id, const IInterfaceDescriptor& interface_descriptor)
  {
    return interface_descriptor == ref_id;
  }

  [[nodiscard]] virtual Type type() const = 0;

  IInterfaceDescriptor(const IInterfaceDescriptor&) = delete;
  IInterfaceDescriptor(IInterfaceDescriptor&&)      = delete;
  IInterfaceDescriptor()                            = default;

  virtual ~IInterfaceDescriptor() = default;
};

#endif   // I_INTERFACE_DESCRIPTOR_HPP
