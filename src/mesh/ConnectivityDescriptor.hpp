#ifndef CONNECTIVITY_DESCRIPTOR_HPP
#define CONNECTIVITY_DESCRIPTOR_HPP

#include <mesh/CellType.hpp>
#include <mesh/ConnectivityMatrix.hpp>
#include <mesh/ItemOfItemType.hpp>
#include <mesh/RefItemList.hpp>
#include <utils/PugsTraits.hpp>

#include <vector>

class ConnectivityDescriptor
{
 private:
  std::vector<RefCellList> m_ref_cell_list_vector;
  std::vector<RefFaceList> m_ref_face_list_vector;
  std::vector<RefEdgeList> m_ref_edge_list_vector;
  std::vector<RefNodeList> m_ref_node_list_vector;

  ConnectivityMatrix m_cell_to_face_matrix = ConnectivityMatrix{true};
  ConnectivityMatrix m_cell_to_edge_matrix = ConnectivityMatrix{true};
  ConnectivityMatrix m_cell_to_node_matrix = ConnectivityMatrix{true};

  ConnectivityMatrix m_face_to_edge_matrix = ConnectivityMatrix{true};
  ConnectivityMatrix m_face_to_node_matrix = ConnectivityMatrix{true};

  ConnectivityMatrix m_edge_to_node_matrix = ConnectivityMatrix{true};

  ConnectivityMatrix m_node_to_face_matrix = ConnectivityMatrix{true};
  ConnectivityMatrix m_node_to_edge_matrix = ConnectivityMatrix{true};

  Array<const bool> m_cell_face_is_reversed;
  Array<const bool> m_face_edge_is_reversed;

  Array<const CellType> m_cell_type_vector;

  Array<const int> m_cell_number_vector;
  Array<const int> m_face_number_vector;
  Array<const int> m_edge_number_vector;
  Array<const int> m_node_number_vector;

  Array<const int> m_cell_owner_vector;
  Array<const int> m_face_owner_vector;
  Array<const int> m_edge_owner_vector;
  Array<const int> m_node_owner_vector;

 public:
  void
  setCellNumberVector(const Array<const int>& cell_number_vector)
  {
    // No check since it can change reading file for instance
    m_cell_number_vector = cell_number_vector;
  }

  PUGS_INLINE
  const Array<const int>&
  cellNumberVector() const
  {
    return m_cell_number_vector;
  }

  void
  setFaceNumberVector(const Array<const int>& face_number_vector)
  {
    // No check since it can change reading file for instance
    m_face_number_vector = face_number_vector;
  }

  PUGS_INLINE
  const Array<const int>&
  faceNumberVector() const
  {
    return m_face_number_vector;
  }

  void
  setEdgeNumberVector(const Array<const int>& edge_number_vector)
  {
    // No check since it can change reading file for instance
    m_edge_number_vector = edge_number_vector;
  }

  PUGS_INLINE
  const Array<const int>&
  edgeNumberVector() const
  {
    return m_edge_number_vector;
  }

  void
  setNodeNumberVector(const Array<const int>& node_number_vector)
  {
    // No check since it can change reading file for instance
    m_node_number_vector = node_number_vector;
  }

  PUGS_INLINE
  const Array<const int>&
  nodeNumberVector() const
  {
    return m_node_number_vector;
  }

  template <ItemType item_type>
  Array<const int>
  itemNumberVector() const
  {
    if constexpr (item_type == ItemType::cell) {
      return m_cell_number_vector;
    } else if constexpr (item_type == ItemType::face) {
      return m_face_number_vector;
    } else if constexpr (item_type == ItemType::edge) {
      return m_edge_number_vector;
    } else if constexpr (item_type == ItemType::node) {
      return m_node_number_vector;
    } else {
      static_assert(is_false_item_type_v<item_type>, "Unexpected item type");
    }
  }

  void
  setCellOwnerVector(const Array<const int>& cell_owner_vector)
  {
    Assert(m_cell_owner_vector.size() == 0);
    m_cell_owner_vector = cell_owner_vector;
  }

  PUGS_INLINE
  const Array<const int>&
  cellOwnerVector() const
  {
    return m_cell_owner_vector;
  }

  void
  setFaceOwnerVector(const Array<const int>& face_owner_vector)
  {
    Assert(m_face_owner_vector.size() == 0);
    m_face_owner_vector = face_owner_vector;
  }

  PUGS_INLINE
  const Array<const int>&
  faceOwnerVector() const
  {
    return m_face_owner_vector;
  }

  void
  setEdgeOwnerVector(const Array<const int>& edge_owner_vector)
  {
    Assert(m_edge_owner_vector.size() == 0);
    m_edge_owner_vector = edge_owner_vector;
  }

  PUGS_INLINE
  const Array<const int>&
  edgeOwnerVector() const
  {
    return m_edge_owner_vector;
  }

  void
  setNodeOwnerVector(const Array<const int>& node_owner_vector)
  {
    Assert(m_node_owner_vector.size() == 0);
    m_node_owner_vector = node_owner_vector;
  }

  PUGS_INLINE
  const Array<const int>&
  nodeOwnerVector() const
  {
    return m_node_owner_vector;
  }

  void
  setCellFaceIsReversed(const Array<const bool>& cell_face_is_reversed)
  {
    Assert(m_cell_face_is_reversed.size() == 0);
    m_cell_face_is_reversed = cell_face_is_reversed;
  }

  PUGS_INLINE
  const Array<const bool>&
  cellFaceIsReversed() const
  {
    return m_cell_face_is_reversed;
  }

  void
  setFaceEdgeIsReversed(const Array<const bool>& face_edge_is_reversed)
  {
    Assert(m_face_edge_is_reversed.size() == 0);
    m_face_edge_is_reversed = face_edge_is_reversed;
  }

  PUGS_INLINE
  const Array<const bool>&
  faceEdgeIsReversed() const
  {
    return m_face_edge_is_reversed;
  }

  void
  setCellTypeVector(const Array<const CellType>& cell_type_vector)
  {
    Assert(m_face_edge_is_reversed.size() == 0);
    m_cell_type_vector = cell_type_vector;
  }

  PUGS_INLINE
  const Array<const CellType>&
  cellTypeVector() const
  {
    return m_cell_type_vector;
  }

  void
  setCellToFaceMatrix(const ConnectivityMatrix& cell_to_face_matrix)
  {
    Assert(m_cell_to_face_matrix.numberOfRows() == 0);
    m_cell_to_face_matrix = cell_to_face_matrix;
  }

  void
  setCellToEdgeMatrix(const ConnectivityMatrix& cell_to_edge_matrix)
  {
    Assert(m_cell_to_edge_matrix.numberOfRows() == 0);
    m_cell_to_edge_matrix = cell_to_edge_matrix;
  }

  void
  setCellToNodeMatrix(const ConnectivityMatrix& cell_to_node_matrix)
  {
    Assert(m_cell_to_node_matrix.numberOfRows() == 0);
    m_cell_to_node_matrix = cell_to_node_matrix;
  }

  void
  setFaceToEdgeMatrix(const ConnectivityMatrix& face_to_edge_matrix)
  {
    Assert(m_face_to_edge_matrix.numberOfRows() == 0);
    m_face_to_edge_matrix = face_to_edge_matrix;
  }

  void
  setFaceToNodeMatrix(const ConnectivityMatrix& face_to_node_matrix)
  {
    Assert(m_face_to_node_matrix.numberOfRows() == 0);
    m_face_to_node_matrix = face_to_node_matrix;
  }

  void
  setEdgeToNodeMatrix(const ConnectivityMatrix& edge_to_node_matrix)
  {
    Assert(m_edge_to_node_matrix.numberOfRows() == 0);
    m_edge_to_node_matrix = edge_to_node_matrix;
  }

  void
  setNodeToFaceMatrix(const ConnectivityMatrix& node_to_face_matrix)
  {
    Assert(m_node_to_face_matrix.numberOfRows() == 0);
    m_node_to_face_matrix = node_to_face_matrix;
  }

  void
  setNodeToEdgeMatrix(const ConnectivityMatrix& node_to_edge_matrix)
  {
    Assert(m_node_to_edge_matrix.numberOfRows() == 0);
    m_node_to_edge_matrix = node_to_edge_matrix;
  }

  PUGS_INLINE
  const ConnectivityMatrix&
  cellToFaceMatrix() const
  {
    return m_cell_to_face_matrix;
  }

  PUGS_INLINE
  const ConnectivityMatrix&
  cellToEdgeMatrix() const
  {
    return m_cell_to_edge_matrix;
  }

  PUGS_INLINE
  const ConnectivityMatrix&
  cellToNodeMatrix() const
  {
    return m_cell_to_node_matrix;
  }

  PUGS_INLINE
  const ConnectivityMatrix&
  faceToEdgeMatrix() const
  {
    return m_face_to_edge_matrix;
  }

  PUGS_INLINE
  const ConnectivityMatrix&
  faceToNodeMatrix() const
  {
    return m_face_to_node_matrix;
  }

  PUGS_INLINE
  const ConnectivityMatrix&
  edgeToNodeMatrix() const
  {
    return m_edge_to_node_matrix;
  }

  PUGS_INLINE
  const ConnectivityMatrix&
  nodeToFaceMatrix() const
  {
    return m_node_to_face_matrix;
  }

  PUGS_INLINE
  const ConnectivityMatrix&
  nodeToEdgeMatrix() const
  {
    return m_node_to_edge_matrix;
  }

  template <typename ItemOfItemT>
  auto&
  itemOfItemVector()
  {
    if constexpr (std::is_same_v<ItemOfItemT, NodeOfCell>) {
      return m_cell_to_node_matrix;
    } else if constexpr (std::is_same_v<ItemOfItemT, FaceOfCell>) {
      return m_cell_to_face_matrix;
    } else if constexpr (std::is_same_v<ItemOfItemT, EdgeOfCell>) {
      return m_cell_to_edge_matrix;
    } else if constexpr (std::is_same_v<ItemOfItemT, EdgeOfFace>) {
      return m_face_to_edge_matrix;
    } else if constexpr (std::is_same_v<ItemOfItemT, NodeOfFace>) {
      return m_face_to_node_matrix;
    } else if constexpr (std::is_same_v<ItemOfItemT, NodeOfEdge>) {
      return m_edge_to_node_matrix;
    } else {
      static_assert(is_false_v<ItemOfItemT>, "Unexpected item of item type");
    }
  }

  template <ItemType item_type>
  const std::vector<RefItemList<item_type>>&
  refItemListVector() const
  {
    if constexpr (item_type == ItemType::cell) {
      return m_ref_cell_list_vector;
    } else if constexpr (item_type == ItemType::face) {
      return m_ref_face_list_vector;
    } else if constexpr (item_type == ItemType::edge) {
      return m_ref_edge_list_vector;
    } else if constexpr (item_type == ItemType::node) {
      return m_ref_node_list_vector;
    } else {
      static_assert(is_false_item_type_v<item_type>, "Unexpected item type");
    }
  }

  template <ItemType item_type>
  void
  addRefItemList(const RefItemList<item_type>& ref_item_list)
  {
    if constexpr (item_type == ItemType::cell) {
      m_ref_cell_list_vector.push_back(ref_item_list);
    } else if constexpr (item_type == ItemType::face) {
      m_ref_face_list_vector.push_back(ref_item_list);
    } else if constexpr (item_type == ItemType::edge) {
      m_ref_edge_list_vector.push_back(ref_item_list);
    } else if constexpr (item_type == ItemType::node) {
      m_ref_node_list_vector.push_back(ref_item_list);
    } else {
      static_assert(is_false_item_type_v<item_type>, "Unexpected item type");
    }
  }

  ConnectivityDescriptor& operator=(const ConnectivityDescriptor&) = delete;
  ConnectivityDescriptor& operator=(ConnectivityDescriptor&&)      = delete;

  ConnectivityDescriptor()                              = default;
  ConnectivityDescriptor(const ConnectivityDescriptor&) = default;
  ConnectivityDescriptor(ConnectivityDescriptor&&)      = delete;
  ~ConnectivityDescriptor()                             = default;
};

#endif   // CONNECTIVITY_DESCRIPTOR_HPP
