#ifndef MESH_NODE_BOUNDARY_UTILS_HPP
#define MESH_NODE_BOUNDARY_UTILS_HPP

#include <algebra/TinyVector.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/RefItemList.hpp>

#include <array>

template <MeshConcept MeshType>
std::array<TinyVector<MeshType::Dimension>, MeshType::Dimension*(MeshType::Dimension - 1)> getBounds(
  const MeshType& mesh,
  const RefNodeList& ref_node_list);

#endif   // MESH_NODE_BOUNDARY_UTILS_HPP
