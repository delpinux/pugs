#ifndef DUAL_MESH_TYPE_HPP
#define DUAL_MESH_TYPE_HPP

#include <utils/Exceptions.hpp>
#include <utils/PugsMacros.hpp>

#include <string>

enum class DualMeshType
{
  Diamond,
  Dual1D,
  Median
};

PUGS_INLINE
std::string
name(DualMeshType type)
{
  switch (type) {
  case DualMeshType::Diamond: {
    return "diamond";
  }
  case DualMeshType::Dual1D: {
    return "dual 1d";
  }
  case DualMeshType::Median: {
    return "median";
  }
  default: {
    throw UnexpectedError("unexpected dual mesh type");
  }
  }
}

#endif   // DUAL_MESH_TYPE_HPP
