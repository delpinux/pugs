#include <mesh/DiamondDualConnectivityBuilder.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityDescriptor.hpp>
#include <mesh/ConnectivityDispatcher.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/PrimalToDiamondDualConnectivityDataMapper.hpp>
#include <mesh/RefId.hpp>
#include <utils/Array.hpp>
#include <utils/Messenger.hpp>
#include <utils/Stringify.hpp>

#include <optional>
#include <vector>

template <size_t Dimension>
void
DiamondDualConnectivityBuilder::_buildDiamondConnectivityDescriptor(const Connectivity<Dimension>& primal_connectivity,
                                                                    ConnectivityDescriptor& diamond_descriptor)
{
  static_assert((Dimension == 2) or (Dimension == 3), "invalid connectivity dimension");

  const size_t primal_number_of_nodes = primal_connectivity.numberOfNodes();
  const size_t primal_number_of_faces = primal_connectivity.numberOfFaces();
  const size_t primal_number_of_cells = primal_connectivity.numberOfCells();

  const auto& primal_node_number = primal_connectivity.nodeNumber();
  const auto& primal_cell_number = primal_connectivity.cellNumber();

  const size_t diamond_number_of_nodes = primal_number_of_cells + primal_number_of_nodes;
  const size_t diamond_number_of_cells = primal_number_of_faces;

  {
    m_primal_node_to_dual_node_map = NodeIdToNodeIdMap{primal_number_of_nodes};

    NodeId diamond_node_id = 0;
    for (NodeId primal_node_id = 0; primal_node_id < primal_number_of_nodes; ++primal_node_id) {
      m_primal_node_to_dual_node_map[primal_node_id] = std::make_pair(primal_node_id, diamond_node_id++);
    }

    m_primal_cell_to_dual_node_map = CellIdToNodeIdMap{primal_number_of_cells};
    for (CellId primal_cell_id = 0; primal_cell_id < primal_number_of_cells; ++primal_cell_id) {
      m_primal_cell_to_dual_node_map[primal_cell_id] = std::make_pair(primal_cell_id, diamond_node_id++);
    }
  }

  Array<int> node_number_vector(diamond_number_of_nodes);

  parallel_for(m_primal_node_to_dual_node_map.size(), [&](size_t i) {
    const auto [primal_node_id, diamond_dual_node_id] = m_primal_node_to_dual_node_map[i];

    node_number_vector[diamond_dual_node_id] = primal_node_number[primal_node_id];
  });

  const size_t cell_number_shift = max(primal_node_number) + 1;
  parallel_for(primal_number_of_cells, [&](size_t i) {
    const auto [primal_cell_id, diamond_dual_node_id] = m_primal_cell_to_dual_node_map[i];

    node_number_vector[diamond_dual_node_id] = primal_cell_number[primal_cell_id] + cell_number_shift;
  });
  diamond_descriptor.setNodeNumberVector(node_number_vector);

  {
    m_primal_face_to_dual_cell_map = FaceIdToCellIdMap{primal_number_of_faces};
    CellId diamond_cell_id         = 0;
    for (FaceId primal_face_id = 0; primal_face_id < primal_number_of_faces; ++primal_face_id) {
      m_primal_face_to_dual_cell_map[primal_face_id] = std::make_pair(primal_face_id, diamond_cell_id++);
    }
  }

  diamond_descriptor.setCellNumberVector([&] {
    Array<int> cell_number_vector(diamond_number_of_cells);
    const auto& primal_face_number = primal_connectivity.faceNumber();
    parallel_for(diamond_number_of_cells, [&](size_t i) {
      const auto [primal_face_id, dual_cell_id] = m_primal_face_to_dual_cell_map[i];
      cell_number_vector[dual_cell_id]          = primal_face_number[primal_face_id];
    });
    return cell_number_vector;
  }());

  Array<CellType> cell_type_vector(diamond_number_of_cells);

  const auto& primal_face_to_cell_matrix = primal_connectivity.faceToCellMatrix();
  const auto& primal_face_to_node_matrix = primal_connectivity.faceToNodeMatrix();

  static_assert(Dimension > 1);
  parallel_for(primal_number_of_faces, [&](FaceId face_id) {
    const size_t i_cell               = face_id;
    const auto& primal_face_cell_list = primal_face_to_cell_matrix[face_id];

    if constexpr (Dimension == 2) {
      if (primal_face_cell_list.size() == 1) {
        cell_type_vector[i_cell] = CellType::Triangle;
      } else {
        Assert(primal_face_cell_list.size() == 2);
        cell_type_vector[i_cell] = CellType::Quadrangle;
      }
    } else if constexpr (Dimension == 3) {
      if (primal_face_cell_list.size() == 1) {
        if (primal_face_to_node_matrix[face_id].size() == 3) {
          cell_type_vector[i_cell] = CellType::Tetrahedron;
        } else {
          cell_type_vector[i_cell] = CellType::Pyramid;
        }
      } else {
        Assert(primal_face_cell_list.size() == 2);
        cell_type_vector[i_cell] = CellType::Diamond;
      }
    }
  });

  diamond_descriptor.setCellTypeVector(cell_type_vector);

  Array<const unsigned int> cell_to_node_row = [&] {
    Array<unsigned int> tmp_cell_to_node_row(primal_number_of_faces + 1);
    tmp_cell_to_node_row[0] = 0;
    for (FaceId face_id = 0; face_id < primal_number_of_faces; ++face_id) {
      tmp_cell_to_node_row[face_id + 1] = tmp_cell_to_node_row[face_id] + primal_face_to_node_matrix[face_id].size() +
                                          primal_face_to_cell_matrix[face_id].size();
    }

    return tmp_cell_to_node_row;
  }();

  Array<const unsigned int> cell_to_node_list = [&] {
    Array<unsigned int> tmp_cell_to_node_list(cell_to_node_row[cell_to_node_row.size() - 1]);
    const auto& primal_face_local_number_in_their_cells = primal_connectivity.faceLocalNumbersInTheirCells();
    const auto& cell_face_is_reversed                   = primal_connectivity.cellFaceIsReversed();
    parallel_for(primal_number_of_faces, [&](FaceId face_id) {
      const auto& primal_face_cell_list = primal_face_to_cell_matrix[face_id];
      const auto& primal_face_node_list = primal_face_to_node_matrix[face_id];
      const size_t first_node           = cell_to_node_row[face_id];
      if (primal_face_cell_list.size() == 1) {
        const CellId cell_id      = primal_face_cell_list[0];
        const auto i_face_in_cell = primal_face_local_number_in_their_cells(face_id, 0);

        for (size_t i_node = 0; i_node < primal_face_node_list.size(); ++i_node) {
          tmp_cell_to_node_list[first_node + i_node] = primal_face_node_list[i_node];
        }
        tmp_cell_to_node_list[first_node + primal_face_node_list.size()] = primal_number_of_nodes + cell_id;

        if constexpr (Dimension == 2) {
          if (cell_face_is_reversed(cell_id, i_face_in_cell)) {
            std::swap(tmp_cell_to_node_list[first_node], tmp_cell_to_node_list[first_node + 1]);
          }
        } else {
          if (not cell_face_is_reversed(cell_id, i_face_in_cell)) {
            // In 3D the basis of the pyramid is described in the
            // indirect way IF the face is not reversed. In other words
            // the "topological normal" must point to the "top" of the
            // pyramid.
            for (size_t i_node = 0; i_node < primal_face_node_list.size() / 2; ++i_node) {
              std::swap(tmp_cell_to_node_list[first_node + i_node],
                        tmp_cell_to_node_list[first_node + primal_face_node_list.size() - 1 - i_node]);
            }
          }
        }
      } else {
        Assert(primal_face_cell_list.size() == 2);

        const CellId cell0_id      = primal_face_cell_list[0];
        const CellId cell1_id      = primal_face_cell_list[1];
        const auto i_face_in_cell0 = primal_face_local_number_in_their_cells(face_id, 0);

        if constexpr (Dimension == 2) {
          Assert(primal_face_node_list.size() == 2);

          tmp_cell_to_node_list[first_node + 0] = primal_number_of_nodes + cell0_id;
          tmp_cell_to_node_list[first_node + 1] = primal_face_node_list[0];
          tmp_cell_to_node_list[first_node + 2] = primal_number_of_nodes + cell1_id;
          tmp_cell_to_node_list[first_node + 3] = primal_face_node_list[1];

          if (cell_face_is_reversed(cell0_id, i_face_in_cell0)) {
            std::swap(tmp_cell_to_node_list[first_node + 1], tmp_cell_to_node_list[first_node + 3]);
          }
        } else {
          tmp_cell_to_node_list[first_node + 0] = primal_number_of_nodes + cell0_id;
          for (size_t i_node = 0; i_node < primal_face_node_list.size(); ++i_node) {
            tmp_cell_to_node_list[first_node + i_node + 1] = primal_face_node_list[i_node];
          }
          tmp_cell_to_node_list[first_node + primal_face_node_list.size() + 1] = primal_number_of_nodes + cell1_id;

          if (cell_face_is_reversed(cell0_id, i_face_in_cell0)) {
            std::swap(tmp_cell_to_node_list[first_node],
                      tmp_cell_to_node_list[first_node + primal_face_node_list.size() + 1]);
          }
        }
      }
    });

    return tmp_cell_to_node_list;
  }();

  diamond_descriptor.setCellToNodeMatrix(ConnectivityMatrix(cell_to_node_row, cell_to_node_list));
}

template <size_t Dimension>
void
DiamondDualConnectivityBuilder::_buildDiamondConnectivityFrom(const IConnectivity& i_primal_connectivity)
{
  static_assert((Dimension == 2) or (Dimension == 3), "invalid connectivity dimension");
  using ConnectivityType = Connectivity<Dimension>;

  const ConnectivityType& primal_connectivity = dynamic_cast<const ConnectivityType&>(i_primal_connectivity);

  ConnectivityDescriptor diamond_descriptor;

  this->_buildDiamondConnectivityDescriptor(primal_connectivity, diamond_descriptor);

  ConnectivityBuilderBase::_computeCellFaceAndFaceNodeConnectivities<Dimension>(diamond_descriptor);
  if constexpr (Dimension == 3) {
    ConnectivityBuilderBase::_computeFaceEdgeAndEdgeNodeAndCellEdgeConnectivities<Dimension>(diamond_descriptor);
  }

  const auto& node_number_vector = diamond_descriptor.nodeNumberVector();
  {
    const std::unordered_map<unsigned int, NodeId> node_to_id_map = [&] {
      std::unordered_map<unsigned int, NodeId> mutable_node_to_id_map;
      for (size_t i_node = 0; i_node < node_number_vector.size(); ++i_node) {
        mutable_node_to_id_map[node_number_vector[i_node]] = i_node;
      }
      return mutable_node_to_id_map;
    }();

    for (size_t i_node_list = 0; i_node_list < primal_connectivity.template numberOfRefItemList<ItemType::node>();
         ++i_node_list) {
      const auto& primal_ref_node_list = primal_connectivity.template refItemList<ItemType::node>(i_node_list);
      const auto& primal_node_list     = primal_ref_node_list.list();

      const std::vector<NodeId> diamond_node_list = [&]() {
        std::vector<NodeId> mutable_diamond_node_list;

        for (size_t i_primal_node = 0; i_primal_node < primal_node_list.size(); ++i_primal_node) {
          auto primal_node_id       = primal_node_list[i_primal_node];
          const auto i_diamond_node = node_to_id_map.find(primal_connectivity.nodeNumber()[primal_node_id]);
          if (i_diamond_node != node_to_id_map.end()) {
            mutable_diamond_node_list.push_back(i_diamond_node->second);
          }
        }

        return mutable_diamond_node_list;
      }();

      if (parallel::allReduceOr(diamond_node_list.size() > 0)) {
        Array<NodeId> node_array(diamond_node_list.size());
        for (size_t i = 0; i < diamond_node_list.size(); ++i) {
          node_array[i] = diamond_node_list[i];
        }
        diamond_descriptor.addRefItemList(
          RefNodeList{primal_ref_node_list.refId(), node_array, primal_ref_node_list.type()});
      }
    }
  }

  {
    const auto& primal_face_to_node_matrix  = primal_connectivity.faceToNodeMatrix();
    const auto& diamond_node_to_face_matrix = diamond_descriptor.nodeToFaceMatrix();
    const auto& diamond_face_to_node_matrix = diamond_descriptor.faceToNodeMatrix();

    const auto find_face = [&](std::vector<uint32_t> node_list) -> std::optional<FaceId> {
      // The node list of already sorted correctly
      const auto& face_id_vector = diamond_node_to_face_matrix[node_list[0]];

      for (size_t i_face = 0; i_face < face_id_vector.size(); ++i_face) {
        const FaceId face_id          = face_id_vector[i_face];
        const auto& face_node_id_list = diamond_face_to_node_matrix[face_id];
        if (face_node_id_list.size() == node_list.size()) {
          bool is_same = true;
          for (size_t i_node = 1; i_node < face_node_id_list.size(); ++i_node) {
            is_same &= (face_node_id_list[i_node] == node_list[i_node]);
          }
          if (is_same) {
            return face_id;
          }
        }
      }

      return std::nullopt;
    };

    std::vector<unsigned int> face_node_list;
    for (size_t i_face_list = 0; i_face_list < primal_connectivity.template numberOfRefItemList<ItemType::face>();
         ++i_face_list) {
      const auto& primal_ref_face_list = primal_connectivity.template refItemList<ItemType::face>(i_face_list);
      const auto& primal_face_list     = primal_ref_face_list.list();

      const std::vector<FaceId> diamond_face_list = [&]() {
        std::vector<FaceId> mutablt_diamond_face_list;
        mutablt_diamond_face_list.reserve(primal_face_list.size());
        for (size_t i_primal_face = 0; i_primal_face < primal_face_list.size(); ++i_primal_face) {
          FaceId primal_face_id = primal_face_list[i_primal_face];

          const auto& primal_face_node_list = primal_face_to_node_matrix[primal_face_id];

          face_node_list.clear();

          for (size_t i_primal_face_noe = 0; i_primal_face_noe < primal_face_node_list.size(); ++i_primal_face_noe) {
            face_node_list.push_back(primal_face_node_list[i_primal_face_noe]);
          }

          auto face_id = find_face(face_node_list);
          if (face_id.has_value()) {
            mutablt_diamond_face_list.push_back(face_id.value());
          }
        }
        return mutablt_diamond_face_list;
      }();

      if (parallel::allReduceOr(diamond_face_list.size() > 0)) {
        Array<FaceId> face_array(diamond_face_list.size());
        for (size_t i_diamond_face = 0; i_diamond_face < diamond_face_list.size(); ++i_diamond_face) {
          face_array[i_diamond_face] = diamond_face_list[i_diamond_face];
        }
        diamond_descriptor.addRefItemList(
          RefFaceList{primal_ref_face_list.refId(), face_array, primal_ref_face_list.type()});
      }
    }
  }

  if constexpr (Dimension == 3) {
    const auto& primal_edge_to_node_matrix = primal_connectivity.edgeToNodeMatrix();

    {
      const size_t number_of_edges = diamond_descriptor.edgeToNodeMatrix().numberOfRows();
      diamond_descriptor.setEdgeNumberVector([&] {
        Array<int> edge_number_vector(number_of_edges);
        parallel_for(
          number_of_edges, PUGS_LAMBDA(size_t i_edge) { edge_number_vector[i_edge] = i_edge; });
        return edge_number_vector;
      }());

      // LCOV_EXCL_START
      if (parallel::size() > 1) {
        throw NotImplementedError("parallel edge numbering is undefined");
      }
      // LCOV_EXCL_STOP
    }
    const auto& diamond_node_to_edge_matrix = diamond_descriptor.nodeToEdgeMatrix();
    const auto& diamond_edge_to_node_matrix = diamond_descriptor.edgeToNodeMatrix();

    const auto find_edge = [&](uint32_t node0, uint32_t node1) -> std::optional<EdgeId> {
      if (node_number_vector[node0] > node_number_vector[node1]) {
        std::swap(node0, node1);
      }
      const auto& edge_id_vector = diamond_node_to_edge_matrix[node0];

      for (size_t i_edge = 0; i_edge < edge_id_vector.size(); ++i_edge) {
        const EdgeId edge_id = edge_id_vector[i_edge];
        if (diamond_edge_to_node_matrix[edge_id][1] == node1) {
          return edge_id;
        }
      }

      return std::nullopt;
    };

    for (size_t i_edge_list = 0; i_edge_list < primal_connectivity.template numberOfRefItemList<ItemType::edge>();
         ++i_edge_list) {
      const auto& primal_ref_edge_list = primal_connectivity.template refItemList<ItemType::edge>(i_edge_list);
      const auto& primal_edge_list     = primal_ref_edge_list.list();

      const std::vector<EdgeId> diamond_edge_list = [&]() {
        std::vector<EdgeId> mutable_diamond_edge_list;
        mutable_diamond_edge_list.reserve(primal_edge_list.size());
        for (size_t i = 0; i < primal_edge_list.size(); ++i) {
          EdgeId primal_edge_id = primal_edge_list[i];

          const auto& primal_edge_node_list = primal_edge_to_node_matrix[primal_edge_id];

          const auto diamond_edge_id = find_edge(primal_edge_node_list[0], primal_edge_node_list[1]);

          if (diamond_edge_id.has_value()) {
            mutable_diamond_edge_list.push_back(diamond_edge_id.value());
          }
        }
        return mutable_diamond_edge_list;
      }();

      if (parallel::allReduceOr(diamond_edge_list.size() > 0)) {
        Array<EdgeId> edge_array(diamond_edge_list.size());
        for (size_t i = 0; i < diamond_edge_list.size(); ++i) {
          edge_array[i] = diamond_edge_list[i];
        }
        diamond_descriptor.addRefItemList(
          RefEdgeList{primal_ref_edge_list.refId(), edge_array, primal_ref_edge_list.type()});
      }
    }
  }

  const size_t primal_number_of_nodes = primal_connectivity.numberOfNodes();
  const size_t primal_number_of_cells = primal_connectivity.numberOfCells();

  diamond_descriptor.setNodeOwnerVector([&] {
    Array<int> node_owner_vector(node_number_vector.size());
    const auto& primal_node_owner = primal_connectivity.nodeOwner();
    for (NodeId primal_node_id = 0; primal_node_id < primal_connectivity.numberOfNodes(); ++primal_node_id) {
      node_owner_vector[primal_node_id] = primal_node_owner[primal_node_id];
    }
    const auto& primal_cell_owner = primal_connectivity.cellOwner();
    for (CellId primal_cell_id = 0; primal_cell_id < primal_number_of_cells; ++primal_cell_id) {
      node_owner_vector[primal_number_of_nodes + primal_cell_id] = primal_cell_owner[primal_cell_id];
    }
    return node_owner_vector;
  }());

  diamond_descriptor.setCellOwnerVector([&] {
    Array<int> cell_owner_vector(diamond_descriptor.cellNumberVector().size());
    const size_t primal_number_of_faces = primal_connectivity.numberOfFaces();
    const auto& primal_face_owner       = primal_connectivity.faceOwner();
    parallel_for(
      primal_number_of_faces, PUGS_LAMBDA(const FaceId primal_face_id) {
        cell_owner_vector[primal_face_id] = primal_face_owner[primal_face_id];
      });
    return cell_owner_vector;
  }());

  const auto& diamond_cell_owner_vector   = diamond_descriptor.cellOwnerVector();
  const auto& diamond_cell_to_face_matrix = diamond_descriptor.cellToFaceMatrix();

  diamond_descriptor.setFaceOwnerVector([&] {
    Array<int> face_owner_vector(diamond_descriptor.faceNumberVector().size());
    face_owner_vector.fill(parallel::rank());

    for (size_t i_cell = 0; i_cell < diamond_cell_to_face_matrix.numberOfRows(); ++i_cell) {
      const auto& cell_face_list = diamond_cell_to_face_matrix[i_cell];
      for (size_t i_face = 0; i_face < cell_face_list.size(); ++i_face) {
        const size_t face_id       = cell_face_list[i_face];
        face_owner_vector[face_id] = std::min(face_owner_vector[face_id], diamond_cell_owner_vector[i_cell]);
      }
    }
    return face_owner_vector;
  }());

  if constexpr (Dimension == 3) {
    const auto& diamond_cell_to_edge_matrix = diamond_descriptor.cellToEdgeMatrix();

    diamond_descriptor.setEdgeOwnerVector([&] {
      Array<int> edge_owner_vector(diamond_descriptor.edgeNumberVector().size());
      edge_owner_vector.fill(parallel::rank());

      for (size_t i_cell = 0; i_cell < diamond_cell_to_edge_matrix.numberOfRows(); ++i_cell) {
        const auto& cell_edge_list = diamond_cell_to_edge_matrix[i_cell];
        for (size_t i_edge = 0; i_edge < cell_edge_list.size(); ++i_edge) {
          const size_t edge_id       = cell_edge_list[i_edge];
          edge_owner_vector[edge_id] = std::min(edge_owner_vector[edge_id], diamond_cell_owner_vector[i_cell]);
        }
      }

      return edge_owner_vector;
    }());
  }

  m_connectivity = ConnectivityType::build(diamond_descriptor);

  m_mapper =
    std::make_shared<PrimalToDiamondDualConnectivityDataMapper<Dimension>>(primal_connectivity,
                                                                           dynamic_cast<const ConnectivityType&>(
                                                                             *m_connectivity),
                                                                           m_primal_node_to_dual_node_map,
                                                                           m_primal_cell_to_dual_node_map,
                                                                           m_primal_face_to_dual_cell_map);
}

DiamondDualConnectivityBuilder::DiamondDualConnectivityBuilder(const IConnectivity& connectivity)
{
  // LCOV_EXCL_START
  if (parallel::size() > 1) {
    throw NotImplementedError("Construction of diamond dual mesh is not implemented in parallel");
  }
  // LCOV_EXCL_STOP
  switch (connectivity.dimension()) {
  case 2: {
    this->_buildDiamondConnectivityFrom<2>(connectivity);
    break;
  }
  case 3: {
    this->_buildDiamondConnectivityFrom<3>(connectivity);
    break;
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("invalid connectivity dimension: " + stringify(connectivity.dimension()));
  }
    // LCOV_EXCL_STOP
  }
}
