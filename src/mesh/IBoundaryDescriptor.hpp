#ifndef I_BOUNDARY_DESCRIPTOR_HPP
#define I_BOUNDARY_DESCRIPTOR_HPP

#include <mesh/RefId.hpp>

#include <iostream>

class IBoundaryDescriptor
{
 public:
  enum class Type
  {
    named,
    numbered
  };

 protected:
  virtual std::ostream& _write(std::ostream& os) const = 0;

 public:
  friend std::ostream&
  operator<<(std::ostream& os, const IBoundaryDescriptor& boundary_descriptor)
  {
    return boundary_descriptor._write(os);
  }

  [[nodiscard]] virtual bool operator==(const RefId& ref_id) const = 0;

  [[nodiscard]] friend bool
  operator==(const RefId& ref_id, const IBoundaryDescriptor& boundary_descriptor)
  {
    return boundary_descriptor == ref_id;
  }

  [[nodiscard]] virtual Type type() const = 0;

  IBoundaryDescriptor(const IBoundaryDescriptor&) = delete;
  IBoundaryDescriptor(IBoundaryDescriptor&&)      = delete;
  IBoundaryDescriptor()                           = default;

  virtual ~IBoundaryDescriptor() = default;
};

#endif   // I_BOUNDARY_DESCRIPTOR_HPP
