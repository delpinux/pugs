#ifndef MESH_DATA_VARIANT_HPP
#define MESH_DATA_VARIANT_HPP

#include <mesh/MeshData.hpp>
#include <mesh/MeshTraits.hpp>
#include <utils/Demangle.hpp>
#include <utils/Exceptions.hpp>
#include <utils/PugsMacros.hpp>

#include <rang.hpp>

#include <memory>
#include <sstream>
#include <variant>

template <size_t Dimension>
class Mesh;

class MeshDataVariant
{
 private:
  using Variant = std::variant<std::shared_ptr<MeshData<Mesh<1>>>,   //
                               std::shared_ptr<MeshData<Mesh<2>>>,   //
                               std::shared_ptr<MeshData<Mesh<3>>>>;

  Variant m_p_mesh_data_variant;

 public:
  template <MeshConcept MeshType>
  PUGS_INLINE std::shared_ptr<MeshData<MeshType>>
  get() const
  {
    if (not std::holds_alternative<std::shared_ptr<MeshData<MeshType>>>(this->m_p_mesh_data_variant)) {
      std::ostringstream error_msg;
      error_msg << "invalid mesh type type\n";
      error_msg << "- required " << rang::fgB::red << demangle<MeshData<MeshType>>() << rang::fg::reset << '\n';
      error_msg << "- contains " << rang::fgB::yellow
                << std::visit(
                     [](auto&& p_mesh_data) -> std::string {
                       using FoundMeshDataType = typename std::decay_t<decltype(p_mesh_data)>::element_type;
                       return demangle<FoundMeshDataType>();
                     },
                     this->m_p_mesh_data_variant)
                << rang::fg::reset;
      throw NormalError(error_msg.str());
    }
    return std::get<std::shared_ptr<MeshData<MeshType>>>(m_p_mesh_data_variant);
  }

  MeshDataVariant() = delete;

  template <MeshConcept MeshType>
  MeshDataVariant(const std::shared_ptr<MeshData<MeshType>>& p_mesh_data) : m_p_mesh_data_variant{p_mesh_data}
  {}

  MeshDataVariant(const MeshDataVariant&) = default;
  MeshDataVariant(MeshDataVariant&&)      = default;

  ~MeshDataVariant() = default;
};

#endif   // MESH_DATA_VARIANT_HPP
