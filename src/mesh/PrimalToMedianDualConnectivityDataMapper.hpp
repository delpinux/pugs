#ifndef PRIMAL_TO_MEDIAN_DUAL_CONNECTIVITY_DATA_MAPPER_HPP
#define PRIMAL_TO_MEDIAN_DUAL_CONNECTIVITY_DATA_MAPPER_HPP

#include <mesh/Connectivity.hpp>
#include <mesh/IPrimalToDualConnectivityDataMapper.hpp>
#include <mesh/ItemIdToItemIdMap.hpp>
#include <mesh/ItemValue.hpp>
#include <utils/Array.hpp>
#include <utils/PugsAssert.hpp>

template <size_t Dimension>
class PrimalToMedianDualConnectivityDataMapper : public IPrimalToDualConnectivityDataMapper
{
  static_assert(Dimension == 2 or Dimension == 3,
                "primal to median dual connectivity mapper is defined in dimension 2 or 3");

 private:
  const IConnectivity* m_primal_connectivity;
  const IConnectivity* m_dual_connectivity;

  ConstNodeIdToNodeIdMap m_primal_boundary_node_to_dual_node_map;
  ConstFaceIdToNodeIdMap m_primal_face_to_dual_node_map;
  ConstCellIdToNodeIdMap m_primal_cell_to_dual_node_map;
  ConstNodeIdToCellIdMap m_primal_node_to_dual_cell_map;

 public:
  template <typename OriginDataType1,
            typename OriginDataType2,
            typename OriginDataType3,
            typename DestinationDataType,
            ItemType origin_face_type>
  void
  toDualNode(const NodeValue<OriginDataType1>& primal_node_value,
             const ItemValue<OriginDataType2, origin_face_type>& primal_face_value,
             const CellValue<OriginDataType3>& primal_cell_value,
             const NodeValue<DestinationDataType>& dual_node_value) const
  {
    static_assert(not std::is_const_v<DestinationDataType>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType1>, DestinationDataType>, "incompatible types");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType2>, DestinationDataType>, "incompatible types");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType3>, DestinationDataType>, "incompatible types");

    static_assert(((Dimension == 3) and (origin_face_type == ItemType::face)) or (is_face_in_2d_v<origin_face_type>),
                  "invalid destination face type");

    Assert(m_primal_connectivity == primal_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for primal CellValue");
    Assert(m_primal_connectivity == primal_face_value.connectivity_ptr().get(),
           "unexpected connectivity for primal FaceValue");
    Assert(m_primal_connectivity == primal_node_value.connectivity_ptr().get(),
           "unexpected connectivity for primal NodeValue");
    Assert(m_dual_connectivity == dual_node_value.connectivity_ptr().get(),
           "unexpected connectivity for dual NodeValue");

    parallel_for(
      m_primal_boundary_node_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_node_id, dual_node_id] = m_primal_boundary_node_to_dual_node_map[i];

        dual_node_value[dual_node_id] = primal_node_value[primal_node_id];
      });

    using OriginFaceId = ItemIdT<origin_face_type>;

    parallel_for(
      m_primal_face_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_face_id, dual_node_id] = m_primal_face_to_dual_node_map[i];

        const OriginFaceId origin_face_id = static_cast<typename OriginFaceId::base_type>(primal_face_id);

        dual_node_value[dual_node_id] = primal_face_value[origin_face_id];
      });

    parallel_for(
      m_primal_cell_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_cell_id, dual_node_id] = m_primal_cell_to_dual_node_map[i];

        dual_node_value[dual_node_id] = primal_cell_value[primal_cell_id];
      });
  }

  template <typename OriginDataType,
            typename DestinationDataType1,
            typename DestinationDataType2,
            typename DestinationDataType3,
            ItemType destination_face_type>
  void
  fromDualNode(const NodeValue<OriginDataType>& dual_node_value,
               const NodeValue<DestinationDataType1>& primal_node_value,
               const ItemValue<DestinationDataType2, destination_face_type>& primal_face_value,
               const CellValue<DestinationDataType3>& primal_cell_value) const
  {
    static_assert(not std::is_const_v<DestinationDataType1>, "destination data type must not be constant");
    static_assert(not std::is_const_v<DestinationDataType2>, "destination data type must not be constant");
    static_assert(not std::is_const_v<DestinationDataType3>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType1>, "incompatible types");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType2>, "incompatible types");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType3>, "incompatible types");

    static_assert(((Dimension == 3) and (destination_face_type == ItemType::face)) or
                    (is_face_in_2d_v<destination_face_type>),
                  "invalid destination face type");

    Assert(m_primal_connectivity == primal_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for primal CellValue");
    Assert(m_primal_connectivity == primal_face_value.connectivity_ptr().get(),
           "unexpected connectivity for primal FaceValue");
    Assert(m_primal_connectivity == primal_node_value.connectivity_ptr().get(),
           "unexpected connectivity for primal NodeValue");
    Assert(m_dual_connectivity == dual_node_value.connectivity_ptr().get(),
           "unexpected connectivity for dual NodeValue");

    parallel_for(
      m_primal_boundary_node_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_node_id, dual_node_id] = m_primal_boundary_node_to_dual_node_map[i];

        primal_node_value[primal_node_id] = dual_node_value[dual_node_id];
      });

    using DestinationFaceId = ItemIdT<destination_face_type>;

    parallel_for(
      m_primal_face_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_face_id, dual_node_id] = m_primal_face_to_dual_node_map[i];

        const DestinationFaceId destination_face_id =
          static_cast<typename DestinationFaceId::base_type>(primal_face_id);

        primal_face_value[destination_face_id] = dual_node_value[dual_node_id];
      });

    parallel_for(
      m_primal_cell_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_cell_id, dual_node_id] = m_primal_cell_to_dual_node_map[i];

        primal_cell_value[primal_cell_id] = dual_node_value[dual_node_id];
      });
  }

  template <typename OriginDataType, typename DestinationDataType>
  void
  toDualCell(const NodeValue<OriginDataType>& primal_node_value,
             const CellValue<DestinationDataType>& dual_cell_value) const
  {
    static_assert(not std::is_const_v<DestinationDataType>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType>, "incompatible types");

    Assert(m_primal_connectivity == primal_node_value.connectivity_ptr().get(),
           "unexpected connectivity for primal NodeValue");
    Assert(m_dual_connectivity == dual_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for dual CellValue");

    parallel_for(
      m_primal_node_to_dual_cell_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_node_id, dual_cell_id] = m_primal_node_to_dual_cell_map[i];

        dual_cell_value[dual_cell_id] = primal_node_value[primal_node_id];
      });
  }

  template <typename OriginDataType, typename DestinationDataType>
  void
  fromDualCell(const CellValue<OriginDataType>& dual_cell_value,
               const NodeValue<DestinationDataType>& primal_node_value) const
  {
    static_assert(not std::is_const_v<DestinationDataType>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType>, "incompatible types");

    Assert(m_primal_connectivity == primal_node_value.connectivity_ptr().get(),
           "unexpected connectivity for primal NodeValue");
    Assert(m_dual_connectivity == dual_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for dual CellValue");

    parallel_for(
      m_primal_node_to_dual_cell_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_face_id, dual_cell_id] = m_primal_node_to_dual_cell_map[i];

        primal_node_value[primal_face_id] = dual_cell_value[dual_cell_id];
      });
  }

  PrimalToMedianDualConnectivityDataMapper(const Connectivity<Dimension>& primal_connectivity,
                                           const Connectivity<Dimension>& dual_connectivity,
                                           const ConstNodeIdToNodeIdMap& primal_boundary_to_dual_node_map,
                                           const ConstFaceIdToNodeIdMap& primal_face_to_dual_node_map,
                                           const ConstCellIdToNodeIdMap& primal_cell_to_dual_node_map,
                                           const ConstNodeIdToCellIdMap& primal_node_to_dual_cell_map)
    : m_primal_connectivity{&primal_connectivity},
      m_dual_connectivity{&dual_connectivity},
      m_primal_boundary_node_to_dual_node_map{primal_boundary_to_dual_node_map},
      m_primal_face_to_dual_node_map{primal_face_to_dual_node_map},
      m_primal_cell_to_dual_node_map{primal_cell_to_dual_node_map},
      m_primal_node_to_dual_cell_map{primal_node_to_dual_cell_map}
  {}
};

#endif   // PRIMAL_TO_MEDIAN_DUAL_CONNECTIVITY_DATA_MAPPER_HPP
