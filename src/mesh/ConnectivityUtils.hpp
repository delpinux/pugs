#ifndef CONNECTIVITY_UTILS_HPP
#define CONNECTIVITY_UTILS_HPP

#include "mesh/IConnectivity.hpp"

bool checkConnectivityOrdering(const IConnectivity&);

#endif   // CONNECTIVITY_UTILS_HPP
