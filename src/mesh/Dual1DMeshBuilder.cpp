#include <mesh/Dual1DMeshBuilder.hpp>

#include <mesh/DualConnectivityManager.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>
#include <mesh/PrimalToDiamondDualConnectivityDataMapper.hpp>
#include <mesh/PrimalToDual1DConnectivityDataMapper.hpp>
#include <utils/Stringify.hpp>

template <MeshConcept MeshType>
void
Dual1DMeshBuilder::_buildDual1DMeshFrom(const MeshType& primal_mesh)
{
  static_assert(is_polygonal_mesh_v<MeshType>);
  static_assert(MeshType::Dimension == 1);

  using ConnectivityType = typename MeshType::Connectivity;

  DualConnectivityManager& manager = DualConnectivityManager::instance();

  std::shared_ptr<const ConnectivityType> p_dual_connectivity =
    manager.getDiamondDualConnectivity(primal_mesh.connectivity());

  const ConnectivityType& dual_connectivity = *p_dual_connectivity;

  const NodeValue<const TinyVector<1>> primal_xr = primal_mesh.xr();

  MeshData<MeshType>& primal_mesh_data           = MeshDataManager::instance().getMeshData(primal_mesh);
  const CellValue<const TinyVector<1>> primal_xj = primal_mesh_data.xj();

  std::shared_ptr primal_to_dual_1d_connectivity_data_mapper =
    manager.getPrimalToDual1DConnectivityDataMapper(primal_mesh.connectivity());

  NodeValue<TinyVector<1>> dual_xr{dual_connectivity};
  primal_to_dual_1d_connectivity_data_mapper->toDualNode(primal_xr, primal_xj, dual_xr);

  m_mesh = std::make_shared<MeshVariant>(std::make_shared<const Mesh<1>>(p_dual_connectivity, dual_xr));
}

Dual1DMeshBuilder::Dual1DMeshBuilder(const std::shared_ptr<const MeshVariant>& mesh_v)
{
  std::cout << "building Dual1DMesh\n";
  this->_buildDual1DMeshFrom(*(mesh_v->get<Mesh<1>>()));
}
