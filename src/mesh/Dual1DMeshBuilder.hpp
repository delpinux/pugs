#ifndef DUAL_1D_MESH_BUILDER_HPP
#define DUAL_1D_MESH_BUILDER_HPP

#include <mesh/MeshBuilderBase.hpp>
#include <mesh/MeshTraits.hpp>

class MeshVariant;
class Dual1DMeshBuilder : public MeshBuilderBase
{
 private:
  template <MeshConcept MeshType>
  void _buildDual1DMeshFrom(const MeshType&);

  friend class DualMeshManager;
  Dual1DMeshBuilder(const std::shared_ptr<const MeshVariant>&);

 public:
  ~Dual1DMeshBuilder() = default;
};

#endif   // DUAL_1D_MESH_BUILDER_HPP
