#ifndef MESH_CELL_ZONE_HPP
#define MESH_CELL_ZONE_HPP

#include <mesh/IZoneDescriptor.hpp>
#include <mesh/RefItemList.hpp>
#include <utils/Array.hpp>

template <size_t Dimension>
class Mesh;

template <size_t Dimension>
class [[nodiscard]] MeshCellZone   // clazy:exclude=copyable-polymorphic
{
 protected:
  Array<const CellId> m_cell_list;
  std::string m_zone_name;

 public:
  template <size_t MeshDimension>
  friend MeshCellZone<MeshDimension> getMeshCellZone(const Mesh<MeshDimension>& mesh,
                                                     const IZoneDescriptor& zone_descriptor);

  MeshCellZone& operator=(const MeshCellZone&) = default;
  MeshCellZone& operator=(MeshCellZone&&)      = default;

  const Array<const CellId>&
  cellList() const
  {
    return m_cell_list;
  }

 protected:
  MeshCellZone(const Mesh<Dimension>& mesh, const RefCellList& ref_cell_list);

 public:
  MeshCellZone(const MeshCellZone&) = default;
  MeshCellZone(MeshCellZone&&)      = default;

  MeshCellZone()          = default;
  virtual ~MeshCellZone() = default;
};

template <size_t Dimension>
MeshCellZone<Dimension> getMeshCellZone(const Mesh<Dimension>& mesh, const IZoneDescriptor& zone_descriptor);

#endif   // MESH_CELL_ZONE_HPP
