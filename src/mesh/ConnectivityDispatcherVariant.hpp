#ifndef CONNECTIVITY_DISPATCHER_VARIANT_HPP
#define CONNECTIVITY_DISPATCHER_VARIANT_HPP

#include <utils/Demangle.hpp>
#include <utils/Exceptions.hpp>
#include <utils/PugsMacros.hpp>

#include <rang.hpp>

#include <memory>
#include <sstream>
#include <variant>

template <size_t Dimension>
class ConnectivityDispatcher;

class ConnectivityDispatcherVariant
{
 private:
  using Variant = std::variant<std::shared_ptr<const ConnectivityDispatcher<1>>,   //
                               std::shared_ptr<const ConnectivityDispatcher<2>>,   //
                               std::shared_ptr<const ConnectivityDispatcher<3>>>;

  Variant m_p_connectivity_dispatcher;

 public:
  template <size_t Dimension>
  PUGS_INLINE std::shared_ptr<const ConnectivityDispatcher<Dimension>>
  get() const
  {
    if (not std::holds_alternative<std::shared_ptr<const ConnectivityDispatcher<Dimension>>>(
          this->m_p_connectivity_dispatcher)) {
      std::ostringstream error_msg;
      error_msg << "invalid connectivity dispatcher type type\n";
      error_msg << "- required " << rang::fgB::red << demangle<ConnectivityDispatcher<Dimension>>() << rang::fg::reset
                << '\n';
      error_msg << "- contains " << rang::fgB::yellow
                << std::visit(
                     [](auto&& p_connectivity_dispatcher) -> std::string {
                       using CDType = std::decay_t<decltype(*p_connectivity_dispatcher)>;
                       return demangle<std::decay_t<CDType>>();
                     },
                     this->m_p_connectivity_dispatcher)
                << rang::fg::reset;
      throw NormalError(error_msg.str());
    }
    return std::get<std::shared_ptr<const ConnectivityDispatcher<Dimension>>>(m_p_connectivity_dispatcher);
  }

  PUGS_INLINE
  Variant
  variant() const
  {
    return m_p_connectivity_dispatcher;
  }

  ConnectivityDispatcherVariant() = delete;

  template <size_t Dimension>
  ConnectivityDispatcherVariant(
    const std::shared_ptr<const ConnectivityDispatcher<Dimension>>& p_connectivity_dispatcher)
    : m_p_connectivity_dispatcher{p_connectivity_dispatcher}
  {}

  ConnectivityDispatcherVariant(const ConnectivityDispatcherVariant&) = default;
  ConnectivityDispatcherVariant(ConnectivityDispatcherVariant&&)      = default;

  ~ConnectivityDispatcherVariant() = default;
};

#endif   // CONNECTIVITY_DISPATCHER_VARIANT_HPP
