#include <mesh/IConnectivity.hpp>

#include <mesh/DualConnectivityManager.hpp>
#include <mesh/SynchronizerManager.hpp>

IConnectivity::~IConnectivity()
{
  SynchronizerManager::instance().deleteConnectivitySynchronizer(this);
  DualConnectivityManager::instance().deleteConnectivity(this);
}
