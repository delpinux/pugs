#ifndef MESH_LINE_NODE_BOUNDARY_HPP
#define MESH_LINE_NODE_BOUNDARY_HPP

#include <algebra/TinyMatrix.hpp>
#include <mesh/MeshNodeBoundary.hpp>

template <MeshConcept MeshType>
class [[nodiscard]] MeshLineNodeBoundary final : public MeshNodeBoundary   // clazy:exclude=copyable-polymorphic
{
 public:
  static_assert(not std::is_const_v<MeshType>, "MeshType must be non-const");
  static_assert(MeshType::Dimension > 1, "MeshLineNodeBoundary makes only sense in dimension greater than 1");

  using Rd = TinyVector<MeshType::Dimension, double>;

 private:
  const Rd m_direction;

  TinyVector<MeshType::Dimension> _getDirection(const MeshType&);

  void _checkBoundaryIsLine(const TinyVector<MeshType::Dimension, double>& direction,
                            const TinyVector<MeshType::Dimension, double>& origin,
                            const double length,
                            const MeshType& mesh) const;

 public:
  template <typename MeshTypeT>
  friend MeshLineNodeBoundary<MeshTypeT> getMeshLineNodeBoundary(const MeshTypeT& mesh,
                                                                 const IBoundaryDescriptor& boundary_descriptor);

  PUGS_INLINE
  const Rd&
  direction() const
  {
    return m_direction;
  }

  MeshLineNodeBoundary& operator=(const MeshLineNodeBoundary&) = default;
  MeshLineNodeBoundary& operator=(MeshLineNodeBoundary&&)      = default;

 private:
  MeshLineNodeBoundary(const MeshType& mesh, const RefEdgeList& ref_edge_list)
    : MeshNodeBoundary(mesh, ref_edge_list), m_direction(_getDirection(mesh))
  {}

  MeshLineNodeBoundary(const MeshType& mesh, const RefFaceList& ref_face_list)
    : MeshNodeBoundary(mesh, ref_face_list), m_direction(_getDirection(mesh))
  {}

  MeshLineNodeBoundary(const MeshType& mesh, const RefNodeList& ref_node_list)
    : MeshNodeBoundary(mesh, ref_node_list), m_direction(_getDirection(mesh))
  {}

 public:
  MeshLineNodeBoundary()                            = default;
  MeshLineNodeBoundary(const MeshLineNodeBoundary&) = default;
  MeshLineNodeBoundary(MeshLineNodeBoundary&&)      = default;
  ~MeshLineNodeBoundary()                           = default;
};

template <typename MeshType>
MeshLineNodeBoundary<MeshType> getMeshLineNodeBoundary(const MeshType& mesh,
                                                       const IBoundaryDescriptor& boundary_descriptor);

#endif   // MESH_LINE_NODE_BOUNDARY_HPP
