#include <utils/PugsAssert.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/MeshDataVariant.hpp>
#include <utils/Exceptions.hpp>

#include <sstream>

MeshDataManager* MeshDataManager::m_instance{nullptr};

void
MeshDataManager::create()
{
  Assert(m_instance == nullptr, "MeshDataManager is already created");
  m_instance = new MeshDataManager;
}

void
MeshDataManager::destroy()
{
  Assert(m_instance != nullptr, "MeshDataManager was not created!");

  if (m_instance->m_mesh_id_mesh_data_map.size() > 0) {
    std::stringstream error;
    error << ": some mesh data is still registered\n";
    for (const auto& i_mesh_data : m_instance->m_mesh_id_mesh_data_map) {
      error << " - mesh data " << rang::fgB::magenta << i_mesh_data.first << rang::style::reset << '\n';
    }
    throw UnexpectedError(error.str());
  }
  delete m_instance;
  m_instance = nullptr;
}

void
MeshDataManager::deleteMeshData(const size_t mesh_id)
{
  m_mesh_id_mesh_data_map.erase(mesh_id);
}

template <MeshConcept MeshType>
MeshData<MeshType>&
MeshDataManager::getMeshData(const MeshType& mesh)
{
  if (auto i_mesh_data = m_mesh_id_mesh_data_map.find(mesh.id()); i_mesh_data != m_mesh_id_mesh_data_map.end()) {
    const auto& mesh_data_v = *i_mesh_data->second;
    return *mesh_data_v.template get<MeshType>();
  } else {
    // **cannot** use make_shared since MeshData constructor is **private**
    std::shared_ptr<MeshData<MeshType>> mesh_data{new MeshData<MeshType>(mesh)};

    m_mesh_id_mesh_data_map[mesh.id()] = std::make_shared<MeshDataVariant>(mesh_data);
    return *mesh_data;
  }
}

template MeshData<Mesh<1>>& MeshDataManager::getMeshData(const Mesh<1>&);
template MeshData<Mesh<2>>& MeshDataManager::getMeshData(const Mesh<2>&);
template MeshData<Mesh<3>>& MeshDataManager::getMeshData(const Mesh<3>&);
