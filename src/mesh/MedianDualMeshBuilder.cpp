#include <mesh/MedianDualMeshBuilder.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/DualConnectivityManager.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>
#include <mesh/PrimalToMedianDualConnectivityDataMapper.hpp>
#include <utils/Stringify.hpp>

template <MeshConcept MeshType>
void
MedianDualMeshBuilder::_buildMedianDualMeshFrom(const MeshType& primal_mesh)
{
  constexpr size_t Dimension = MeshType::Dimension;
  static_assert(Dimension > 1);

  using ConnectivityType = Connectivity<Dimension>;

  DualConnectivityManager& manager = DualConnectivityManager::instance();

  std::shared_ptr<const ConnectivityType> p_dual_connectivity =
    manager.getMedianDualConnectivity(primal_mesh.connectivity());

  const ConnectivityType& dual_connectivity = *p_dual_connectivity;

  const NodeValue<const TinyVector<Dimension>> primal_xr = primal_mesh.xr();

  MeshData<MeshType>& primal_mesh_data                   = MeshDataManager::instance().getMeshData(primal_mesh);
  const CellValue<const TinyVector<Dimension>> primal_xj = primal_mesh_data.xj();
  const FaceValue<const TinyVector<Dimension>> primal_xl = primal_mesh_data.xl();

  std::shared_ptr primal_to_dual_connectivity_data_mapper =
    manager.getPrimalToMedianDualConnectivityDataMapper(primal_mesh.connectivity());

  NodeValue<TinyVector<Dimension>> dual_xr{dual_connectivity};
  primal_to_dual_connectivity_data_mapper->toDualNode(primal_xr, primal_xl, primal_xj, dual_xr);

  m_mesh = std::make_shared<MeshVariant>(std::make_shared<const MeshType>(p_dual_connectivity, dual_xr));
}

MedianDualMeshBuilder::MedianDualMeshBuilder(const std::shared_ptr<const MeshVariant>& mesh_v)
{
  std::visit(
    [&](auto&& p_mesh) {
      using MeshType = mesh_type_t<decltype(p_mesh)>;
      if constexpr (is_polygonal_mesh_v<MeshType>) {
        if constexpr (MeshType::Dimension > 1) {
          this->_buildMedianDualMeshFrom(*p_mesh);
        } else {
          throw UnexpectedError("invalid polygonal mesh dimension");
        }
      } else {
        throw UnexpectedError("invalid mesh type");
      }
    },
    mesh_v->variant());
}
