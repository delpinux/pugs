#include <mesh/Mesh.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/DualMeshManager.hpp>
#include <mesh/MeshDataManager.hpp>

template <size_t Dimension>
Mesh<Dimension>::~Mesh()
{
  MeshDataManager::instance().deleteMeshData(this->id());
  DualMeshManager::instance().deleteMesh(this->id());
}

template Mesh<1>::~Mesh();
template Mesh<2>::~Mesh();
template Mesh<3>::~Mesh();
