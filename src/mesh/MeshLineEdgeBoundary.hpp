#ifndef MESH_LINE_EDGE_BOUNDARY_HPP
#define MESH_LINE_EDGE_BOUNDARY_HPP

#include <algebra/TinyMatrix.hpp>
#include <mesh/MeshEdgeBoundary.hpp>

template <MeshConcept MeshType>
class [[nodiscard]] MeshLineEdgeBoundary final : public MeshEdgeBoundary   // clazy:exclude=copyable-polymorphic
{
 public:
  static_assert(not std::is_const_v<MeshType>, "MeshType must be non-const");

  static_assert(MeshType::Dimension > 1, "MeshLineEdgeBoundary makes only sense in dimension 1");

  using Rd = TinyVector<MeshType::Dimension, double>;

 private:
  const Rd m_direction;

 public:
  template <typename MeshTypeT>
  friend MeshLineEdgeBoundary<MeshTypeT> getMeshLineEdgeBoundary(const MeshTypeT& mesh,
                                                                 const IBoundaryDescriptor& boundary_descriptor);

  PUGS_INLINE
  const Rd&
  direction() const
  {
    return m_direction;
  }

  MeshLineEdgeBoundary& operator=(const MeshLineEdgeBoundary&) = default;
  MeshLineEdgeBoundary& operator=(MeshLineEdgeBoundary&&)      = default;

 private:
  MeshLineEdgeBoundary(const MeshType& mesh, const RefEdgeList& ref_edge_list, const Rd& direction)
    : MeshEdgeBoundary(mesh, ref_edge_list), m_direction(direction)
  {}

 public:
  MeshLineEdgeBoundary()                            = default;
  MeshLineEdgeBoundary(const MeshLineEdgeBoundary&) = default;
  MeshLineEdgeBoundary(MeshLineEdgeBoundary&&)      = default;
  ~MeshLineEdgeBoundary()                           = default;
};

template <typename MeshType>
MeshLineEdgeBoundary<MeshType> getMeshLineEdgeBoundary(const MeshType& mesh,
                                                       const IBoundaryDescriptor& boundary_descriptor);

#endif   // MESH_LINE_EDGE_BOUNDARY_HPP
