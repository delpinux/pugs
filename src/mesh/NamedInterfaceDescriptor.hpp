#ifndef NAMED_INTERFACE_DESCRIPTOR_HPP
#define NAMED_INTERFACE_DESCRIPTOR_HPP

#include <mesh/IInterfaceDescriptor.hpp>

#include <iostream>
#include <string>

class NamedInterfaceDescriptor final : public IInterfaceDescriptor
{
 private:
  std::string m_name;

  std::ostream&
  _write(std::ostream& os) const final
  {
    os << '"' << m_name << '"';
    return os;
  }

 public:
  [[nodiscard]] bool
  operator==(const RefId& ref_id) const final
  {
    return m_name == ref_id.tagName();
  }

  [[nodiscard]] const std::string&
  name() const
  {
    return m_name;
  }

  [[nodiscard]] Type
  type() const final
  {
    return Type::named;
  }

  NamedInterfaceDescriptor(const NamedInterfaceDescriptor&) = delete;
  NamedInterfaceDescriptor(NamedInterfaceDescriptor&&)      = delete;
  NamedInterfaceDescriptor(const std::string& name) : m_name(name) {}
  virtual ~NamedInterfaceDescriptor() = default;
};

#endif   // NAMED_INTERFACE_DESCRIPTOR_HPP
