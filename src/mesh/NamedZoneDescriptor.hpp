#ifndef NAMED_ZONE_DESCRIPTOR_HPP
#define NAMED_ZONE_DESCRIPTOR_HPP

#include <mesh/IZoneDescriptor.hpp>

#include <iostream>
#include <string>

class NamedZoneDescriptor final : public IZoneDescriptor
{
 private:
  std::string m_name;

  std::ostream&
  _write(std::ostream& os) const final
  {
    os << '"' << m_name << '"';
    return os;
  }

 public:
  bool
  operator==(const RefId& ref_id) const final
  {
    return m_name == ref_id.tagName();
  }

  [[nodiscard]] const std::string&
  name() const
  {
    return m_name;
  }

  [[nodiscard]] Type
  type() const final
  {
    return Type::named;
  }

  NamedZoneDescriptor(const NamedZoneDescriptor&) = delete;
  NamedZoneDescriptor(NamedZoneDescriptor&&)      = delete;
  NamedZoneDescriptor(const std::string& name) : m_name(name)
  {
    ;
  }
  virtual ~NamedZoneDescriptor() = default;
};

#endif   // NAMED_ZONE_DESCRIPTOR_HPP
