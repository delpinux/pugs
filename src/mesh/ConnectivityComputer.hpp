#ifndef CONNECTIVITY_COMPUTER_HPP
#define CONNECTIVITY_COMPUTER_HPP

#include <mesh/ConnectivityMatrix.hpp>
#include <mesh/SubItemValuePerItem.hpp>

class ConnectivityComputer
{
 private:
  template <typename ConnectivityType, ItemType child_item_type>
  ConnectivityMatrix _computeInverseConnectivity(const ConnectivityType& connectivity,
                                                 const ConnectivityMatrix& item_to_child_matrix) const;

 public:
  template <typename ConnectivityType>
  ConnectivityMatrix computeInverseConnectivityMatrix(const ConnectivityType& connectivity,
                                                      ItemType item_type,
                                                      ItemType child_item_type) const;

  template <typename ItemOfItem, typename ConnectivityType>
  void computeLocalItemNumberInChildItem(const ConnectivityType& connectivity) const;

  ConnectivityComputer(const ConnectivityComputer&) = default;
  ConnectivityComputer()                            = default;
  ~ConnectivityComputer()                           = default;
};

#endif   // CONNECTIVITY_COMPUTER_HPP
