#ifndef NUMBERED_ZONE_DESCRIPTOR_HPP
#define NUMBERED_ZONE_DESCRIPTOR_HPP

#include <mesh/IZoneDescriptor.hpp>

#include <iostream>

class NumberedZoneDescriptor final : public IZoneDescriptor
{
 private:
  unsigned int m_number;

  std::ostream&
  _write(std::ostream& os) const final
  {
    os << '"' << m_number << '"';
    return os;
  }

  bool
  operator==(const RefId& ref_id) const final
  {
    return m_number == ref_id.tagNumber();
  }

 public:
  [[nodiscard]] unsigned int
  number() const
  {
    return m_number;
  }

  [[nodiscard]] Type
  type() const final
  {
    return Type::numbered;
  }

  NumberedZoneDescriptor(const NumberedZoneDescriptor&) = delete;
  NumberedZoneDescriptor(NumberedZoneDescriptor&&)      = delete;
  NumberedZoneDescriptor(unsigned int number) : m_number(number) {}
  virtual ~NumberedZoneDescriptor() = default;
};

#endif   // NUMBERED_ZONE_DESCRIPTOR_HPP
