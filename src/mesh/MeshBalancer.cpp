#include <mesh/MeshBalancer.hpp>

#include <mesh/Mesh.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>

MeshBalancer::MeshBalancer(const std::shared_ptr<const MeshVariant>& initial_mesh)
{
  m_mesh = initial_mesh;
  std::visit(
    [this](auto&& mesh) {
      using MeshType = mesh_type_t<decltype(mesh)>;

      this->_dispatch<MeshType::Dimension>(DispatchType::balance);
    },
    initial_mesh->variant());
}
