#include <mesh/GmshReader.hpp>

#include <utils/PugsMacros.hpp>

#include <mesh/CellType.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityBuilderBase.hpp>
#include <mesh/ConnectivityDispatcher.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshVariant.hpp>
#include <mesh/RefItemList.hpp>
#include <utils/Exceptions.hpp>
#include <utils/Stringify.hpp>

#include <rang.hpp>

#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <regex>
#include <set>
#include <sstream>
#include <unordered_map>

template <size_t Dimension>
class GmshConnectivityBuilder : public ConnectivityBuilderBase
{
 public:
  GmshConnectivityBuilder(const GmshReader::GmshData& gmsh_data, const size_t nb_cells);
};

template <>
GmshConnectivityBuilder<1>::GmshConnectivityBuilder(const GmshReader::GmshData& gmsh_data, const size_t nb_cells)
{
  ConnectivityDescriptor descriptor;

  descriptor.setNodeNumberVector(convert_to_array(gmsh_data.__verticesNumbers));
  Array<CellType> cell_type_vector(nb_cells);
  Array<int> cell_number_vector(nb_cells);

  Array<unsigned int> cell_to_node_row(nb_cells + 1);
  parallel_for(
    cell_to_node_row.size(), PUGS_LAMBDA(const CellId cell_id) { cell_to_node_row[cell_id] = 2 * cell_id; });

  Array<unsigned int> cell_to_node_list(cell_to_node_row[cell_to_node_row.size() - 1]);
  for (CellId cell_id = 0; cell_id < nb_cells; ++cell_id) {
    for (int i_node = 0; i_node < 2; ++i_node) {
      cell_to_node_list[2 * cell_id + i_node] = gmsh_data.__edges[cell_id][i_node];
    }
  }
  descriptor.setCellToNodeMatrix(ConnectivityMatrix(cell_to_node_row, cell_to_node_list));

  cell_type_vector.fill(CellType::Line);
  descriptor.setCellTypeVector(cell_type_vector);

  for (size_t j = 0; j < nb_cells; ++j) {
    cell_number_vector[j] = gmsh_data.__edges_number[j];
  }
  descriptor.setCellNumberVector(cell_number_vector);

  std::map<unsigned int, std::vector<unsigned int>> ref_points_map;
  for (unsigned int r = 0; r < gmsh_data.__points.size(); ++r) {
    const unsigned int point_number = gmsh_data.__points[r];
    const unsigned int& ref         = gmsh_data.__points_ref[r];
    ref_points_map[ref].push_back(point_number);
  }

  Array<size_t> node_nb_cell(descriptor.nodeNumberVector().size());
  node_nb_cell.fill(0);

  const auto& cell_to_node_matrix = descriptor.cellToNodeMatrix();
  for (size_t j = 0; j < nb_cells; ++j) {
    const auto& cell_node_list = cell_to_node_matrix[j];
    for (int r = 0; r < 2; ++r) {
      node_nb_cell[cell_node_list[r]] += 1;
    }
  }

  for (const auto& ref_point_list : ref_points_map) {
    Array<NodeId> point_list(ref_point_list.second.size());
    for (size_t j = 0; j < ref_point_list.second.size(); ++j) {
      point_list[j] = ref_point_list.second[j];
    }

    RefItemListBase::Type list_type = [&] {
      bool is_boundary = true;
      for (size_t i_node = 0; i_node < point_list.size(); ++i_node) {
        if (node_nb_cell[point_list[i_node]] > 1) {
          is_boundary = false;
          break;
        }
      }
      if (is_boundary) {
        return RefItemListBase::Type::boundary;
      }

      bool is_interface = true;
      for (size_t i_node = 0; i_node < point_list.size(); ++i_node) {
        if (node_nb_cell[point_list[i_node]] < 2) {
          is_interface = false;
          break;
        }
      }
      if (is_interface) {
        return RefItemListBase::Type::interface;
      }

      return RefItemListBase::Type::set;
    }();

    auto ref_id = [&] {
      if (auto i_physical_ref = gmsh_data.m_physical_ref_map.find(ref_point_list.first);
          i_physical_ref != gmsh_data.m_physical_ref_map.end()) {
        return i_physical_ref->second.refId();
      } else {
        return RefId{ref_point_list.first};
      }
    }();

    descriptor.addRefItemList(RefNodeList(ref_id, point_list, list_type));
  }

  std::map<unsigned int, std::vector<unsigned int>> ref_cells_map;
  for (unsigned int j = 0; j < gmsh_data.__edges_ref.size(); ++j) {
    const unsigned int elem_number = j;
    const unsigned int& ref        = gmsh_data.__edges_ref[j];
    ref_cells_map[ref].push_back(elem_number);
  }

  for (const auto& ref_cell_list : ref_cells_map) {
    Array<CellId> cell_list(ref_cell_list.second.size());
    for (size_t j = 0; j < ref_cell_list.second.size(); ++j) {
      cell_list[j] = ref_cell_list.second[j];
    }

    auto ref_id = [&] {
      if (auto i_physical_ref = gmsh_data.m_physical_ref_map.find(ref_cell_list.first);
          i_physical_ref != gmsh_data.m_physical_ref_map.end()) {
        return i_physical_ref->second.refId();
      } else {
        return RefId{ref_cell_list.first};
      }
    }();

    descriptor.addRefItemList(RefCellList(ref_id, cell_list, RefItemListBase::Type::set));
  }

  descriptor.setCellOwnerVector([&] {
    Array<int> cell_owner_vector(nb_cells);
    cell_owner_vector.fill(parallel::rank());
    return cell_owner_vector;
  }());

  descriptor.setNodeOwnerVector([&] {
    Array<int> node_owner_vector(descriptor.nodeNumberVector().size());
    node_owner_vector.fill(parallel::rank());
    return node_owner_vector;
  }());
  ;
  m_connectivity = Connectivity1D::build(descriptor);
}

template <>
GmshConnectivityBuilder<2>::GmshConnectivityBuilder(const GmshReader::GmshData& gmsh_data, const size_t nb_cells)
{
  ConnectivityDescriptor descriptor;

  descriptor.setNodeNumberVector(convert_to_array(gmsh_data.__verticesNumbers));
  Array<CellType> cell_type_vector(nb_cells);
  Array<int> cell_number_vector(nb_cells);

  Array<unsigned int> cell_to_node_row(gmsh_data.__triangles.size() + gmsh_data.__quadrangles.size() + 1);
  {
    cell_to_node_row[0] = 0;
    size_t i_cell       = 0;
    for (size_t i_triangle = 0; i_triangle < gmsh_data.__triangles.size(); ++i_triangle, ++i_cell) {
      cell_to_node_row[i_cell + 1] = cell_to_node_row[i_cell] + 3;
    }
    for (size_t i_quadrangle = 0; i_quadrangle < gmsh_data.__quadrangles.size(); ++i_quadrangle, ++i_cell) {
      cell_to_node_row[i_cell + 1] = cell_to_node_row[i_cell] + 4;
    }
  }

  Array<unsigned int> cell_to_node_list(cell_to_node_row[cell_to_node_row.size() - 1]);
  {
    size_t i_cell_node = 0;
    for (size_t i_triangle = 0; i_triangle < gmsh_data.__triangles.size(); ++i_triangle) {
      cell_to_node_list[i_cell_node++] = gmsh_data.__triangles[i_triangle][0];
      cell_to_node_list[i_cell_node++] = gmsh_data.__triangles[i_triangle][1];
      cell_to_node_list[i_cell_node++] = gmsh_data.__triangles[i_triangle][2];
    }
    for (size_t i_quadrangle = 0; i_quadrangle < gmsh_data.__quadrangles.size(); ++i_quadrangle) {
      cell_to_node_list[i_cell_node++] = gmsh_data.__quadrangles[i_quadrangle][0];
      cell_to_node_list[i_cell_node++] = gmsh_data.__quadrangles[i_quadrangle][1];
      cell_to_node_list[i_cell_node++] = gmsh_data.__quadrangles[i_quadrangle][2];
      cell_to_node_list[i_cell_node++] = gmsh_data.__quadrangles[i_quadrangle][3];
    }
  }
  descriptor.setCellToNodeMatrix(ConnectivityMatrix(cell_to_node_row, cell_to_node_list));

  const size_t nb_triangles = gmsh_data.__triangles.size();
  for (size_t i_triangle = 0; i_triangle < nb_triangles; ++i_triangle) {
    cell_type_vector[i_triangle]   = CellType::Triangle;
    cell_number_vector[i_triangle] = gmsh_data.__triangles_number[i_triangle];
  }

  const size_t nb_quadrangles = gmsh_data.__quadrangles.size();
  for (size_t i_quadrangle = 0; i_quadrangle < nb_quadrangles; ++i_quadrangle) {
    const size_t i_cell = i_quadrangle + nb_triangles;

    cell_type_vector[i_cell]   = CellType::Quadrangle;
    cell_number_vector[i_cell] = gmsh_data.__quadrangles_number[i_quadrangle];
  }

  descriptor.setCellNumberVector(cell_number_vector);
  descriptor.setCellTypeVector(cell_type_vector);

  std::map<unsigned int, std::vector<unsigned int>> ref_cells_map;
  for (unsigned int j = 0; j < gmsh_data.__triangles_ref.size(); ++j) {
    const unsigned int elem_number = j;
    const unsigned int& ref        = gmsh_data.__triangles_ref[j];
    ref_cells_map[ref].push_back(elem_number);
  }

  for (unsigned int j = 0; j < gmsh_data.__quadrangles_ref.size(); ++j) {
    const size_t elem_number = nb_triangles + j;
    const unsigned int& ref  = gmsh_data.__quadrangles_ref[j];
    ref_cells_map[ref].push_back(elem_number);
  }

  for (const auto& ref_cell_list : ref_cells_map) {
    Array<CellId> cell_list(ref_cell_list.second.size());
    for (size_t j = 0; j < ref_cell_list.second.size(); ++j) {
      cell_list[j] = ref_cell_list.second[j];
    }

    auto ref_id = [&] {
      if (auto i_physical_ref = gmsh_data.m_physical_ref_map.find(ref_cell_list.first);
          i_physical_ref != gmsh_data.m_physical_ref_map.end()) {
        return i_physical_ref->second.refId();
      } else {
        return RefId{ref_cell_list.first};
      }
    }();

    descriptor.addRefItemList(RefCellList(ref_id, cell_list, RefItemListBase::Type::set));
  }

  ConnectivityBuilderBase::_computeCellFaceAndFaceNodeConnectivities<2>(descriptor);

  std::unordered_map<int, FaceId> face_number_id_map = [&] {
    const auto& face_number_vector = descriptor.faceNumberVector();
    std::unordered_map<int, FaceId> mutable_face_number_id_map;
    for (size_t l = 0; l < face_number_vector.size(); ++l) {
      mutable_face_number_id_map[face_number_vector[l]] = l;
    }
    Assert(mutable_face_number_id_map.size() == face_number_vector.size());
    return mutable_face_number_id_map;
  }();

  const auto& node_number_vector  = descriptor.nodeNumberVector();
  const auto& node_to_face_matrix = descriptor.nodeToFaceMatrix();
  const auto& face_to_node_matrix = descriptor.faceToNodeMatrix();

  const auto find_face = [&](uint32_t node0, uint32_t node1) {
    if (node_number_vector[node0] > node_number_vector[node1]) {
      std::swap(node0, node1);
    }
    const auto& face_id_vector = node_to_face_matrix[node0];

    for (size_t i_face = 0; i_face < face_id_vector.size(); ++i_face) {
      const FaceId face_id = face_id_vector[i_face];
      if (face_to_node_matrix[face_id][1] == node1) {
        return face_id;
      }
    }

    std::stringstream error_msg;
    error_msg << "face (" << node0 << ',' << node1 << ") not found";
    throw NormalError(error_msg.str());
  };

  Array<int> face_number_vector = copy(descriptor.faceNumberVector());
  std::map<unsigned int, std::vector<unsigned int>> ref_faces_map;
  for (unsigned int e = 0; e < gmsh_data.__edges.size(); ++e) {
    const unsigned int edge_id = find_face(gmsh_data.__edges[e][0], gmsh_data.__edges[e][1]);

    const unsigned int& ref = gmsh_data.__edges_ref[e];
    ref_faces_map[ref].push_back(edge_id);

    if (face_number_vector[edge_id] != gmsh_data.__edges_number[e]) {
      if (auto i_face = face_number_id_map.find(gmsh_data.__edges_number[e]); i_face != face_number_id_map.end()) {
        const int other_edge_id = i_face->second;
        std::swap(face_number_vector[edge_id], face_number_vector[other_edge_id]);

        face_number_id_map.erase(face_number_vector[edge_id]);
        face_number_id_map.erase(face_number_vector[other_edge_id]);

        face_number_id_map[face_number_vector[edge_id]]       = edge_id;
        face_number_id_map[face_number_vector[other_edge_id]] = other_edge_id;
      } else {
        face_number_id_map.erase(face_number_vector[edge_id]);
        face_number_vector[edge_id]                     = gmsh_data.__edges_number[e];
        face_number_id_map[face_number_vector[edge_id]] = edge_id;
      }
    }
  }
  descriptor.setFaceNumberVector(face_number_vector);

  Array<size_t> face_nb_cell(descriptor.faceNumberVector().size());
  face_nb_cell.fill(0);

  const auto& cell_to_face_matrix = descriptor.cellToFaceMatrix();
  for (size_t j = 0; j < cell_to_face_matrix.numberOfRows(); ++j) {
    const auto& cell_face_list = cell_to_face_matrix[j];
    for (size_t l = 0; l < cell_face_list.size(); ++l) {
      face_nb_cell[cell_face_list[l]] += 1;
    }
  }

  for (const auto& ref_face_list : ref_faces_map) {
    Array<FaceId> face_list(ref_face_list.second.size());
    for (size_t j = 0; j < ref_face_list.second.size(); ++j) {
      face_list[j] = ref_face_list.second[j];
    }

    auto ref_id = [&] {
      if (auto i_physical_ref = gmsh_data.m_physical_ref_map.find(ref_face_list.first);
          i_physical_ref != gmsh_data.m_physical_ref_map.end()) {
        return i_physical_ref->second.refId();
      } else {
        return RefId{ref_face_list.first};
      }
    }();

    RefItemListBase::Type list_type = [&] {
      bool is_boundary = true;
      for (size_t i_face = 0; i_face < face_list.size(); ++i_face) {
        if (face_nb_cell[face_list[i_face]] > 1) {
          is_boundary = false;
          break;
        }
      }
      if (is_boundary) {
        return RefItemListBase::Type::boundary;
      }

      bool is_interface = true;
      for (size_t i_face = 0; i_face < face_list.size(); ++i_face) {
        if (face_nb_cell[face_list[i_face]] < 2) {
          is_interface = false;
          break;
        }
      }
      if (is_interface) {
        return RefItemListBase::Type::interface;
      }

      return RefItemListBase::Type::set;
    }();

    descriptor.addRefItemList(RefFaceList{ref_id, face_list, list_type});
  }

  Array<bool> is_boundary_node(node_number_vector.size());
  is_boundary_node.fill(false);

  for (size_t i_face = 0; i_face < face_nb_cell.size(); ++i_face) {
    if (face_nb_cell[i_face] == 1) {
      const auto& face_node_list = face_to_node_matrix[i_face];
      for (size_t i_node = 0; i_node < face_node_list.size(); ++i_node) {
        is_boundary_node[face_node_list[i_node]] = true;
      }
    }
  }

  std::map<unsigned int, std::vector<unsigned int>> ref_points_map;
  for (unsigned int r = 0; r < gmsh_data.__points.size(); ++r) {
    const unsigned int point_number = gmsh_data.__points[r];
    const unsigned int& ref         = gmsh_data.__points_ref[r];
    ref_points_map[ref].push_back(point_number);
  }

  for (const auto& ref_point_list : ref_points_map) {
    Array<NodeId> point_list(ref_point_list.second.size());
    for (size_t j = 0; j < ref_point_list.second.size(); ++j) {
      point_list[j] = ref_point_list.second[j];
    }

    auto ref_id = [&] {
      if (auto i_physical_ref = gmsh_data.m_physical_ref_map.find(ref_point_list.first);
          i_physical_ref != gmsh_data.m_physical_ref_map.end()) {
        return i_physical_ref->second.refId();
      } else {
        return RefId{ref_point_list.first};
      }
    }();

    RefItemListBase::Type list_type = [&] {
      bool is_boundary = true;
      for (size_t i_node = 0; i_node < point_list.size(); ++i_node) {
        if (not is_boundary_node[point_list[i_node]]) {
          is_boundary = false;
          break;
        }
      }
      if (is_boundary) {
        return RefItemListBase::Type::boundary;
      }

      return RefItemListBase::Type::set;
    }();

    descriptor.addRefItemList(RefNodeList(ref_id, point_list, list_type));
  }

  descriptor.setCellOwnerVector([&] {
    Array<int> cell_owner_vector(nb_cells);
    cell_owner_vector.fill(parallel::rank());
    return cell_owner_vector;
  }());

  descriptor.setFaceOwnerVector([&] {
    Array<int> face_owner_vector(descriptor.faceNumberVector().size());
    face_owner_vector.fill(parallel::rank());
    return face_owner_vector;
  }());

  descriptor.setNodeOwnerVector([&] {
    Array<int> node_owner_vector(node_number_vector.size());
    node_owner_vector.fill(parallel::rank());
    return node_owner_vector;
  }());

  m_connectivity = Connectivity2D::build(descriptor);
}

template <>
GmshConnectivityBuilder<3>::GmshConnectivityBuilder(const GmshReader::GmshData& gmsh_data, const size_t nb_cells)
{
  ConnectivityDescriptor descriptor;

  descriptor.setNodeNumberVector(convert_to_array(gmsh_data.__verticesNumbers));
  Array<CellType> cell_type_vector(nb_cells);
  Array<int> cell_number_vector(nb_cells);

  const size_t nb_tetrahedra = gmsh_data.__tetrahedra.size();
  const size_t nb_hexahedra  = gmsh_data.__hexahedra.size();
  const size_t nb_prisms     = gmsh_data.__prisms.size();
  const size_t nb_pyramids   = gmsh_data.__pyramids.size();

  Array<unsigned int> cell_to_node_row(nb_cells + 1);
  {
    cell_to_node_row[0] = 0;
    size_t i_cell       = 0;
    for (size_t i_tetrahedron = 0; i_tetrahedron < nb_tetrahedra; ++i_tetrahedron, ++i_cell) {
      cell_to_node_row[i_cell + 1] = cell_to_node_row[i_cell] + 4;
    }
    for (size_t i_hexahedron = 0; i_hexahedron < nb_hexahedra; ++i_hexahedron, ++i_cell) {
      cell_to_node_row[i_cell + 1] = cell_to_node_row[i_cell] + 8;
    }
    for (size_t i_prism = 0; i_prism < nb_prisms; ++i_prism, ++i_cell) {
      cell_to_node_row[i_cell + 1] = cell_to_node_row[i_cell] + 6;
    }
    for (size_t i_pyramid = 0; i_pyramid < nb_pyramids; ++i_pyramid, ++i_cell) {
      cell_to_node_row[i_cell + 1] = cell_to_node_row[i_cell] + 5;
    }
  }

  Array<unsigned int> cell_to_node_list(cell_to_node_row[cell_to_node_row.size() - 1]);
  {
    size_t i_cell_node = 0;
    for (size_t i_tetrahedron = 0; i_tetrahedron < nb_tetrahedra; ++i_tetrahedron) {
      cell_to_node_list[i_cell_node++] = gmsh_data.__tetrahedra[i_tetrahedron][0];
      cell_to_node_list[i_cell_node++] = gmsh_data.__tetrahedra[i_tetrahedron][1];
      cell_to_node_list[i_cell_node++] = gmsh_data.__tetrahedra[i_tetrahedron][2];
      cell_to_node_list[i_cell_node++] = gmsh_data.__tetrahedra[i_tetrahedron][3];
    }
    for (size_t i_hexahedron = 0; i_hexahedron < nb_hexahedra; ++i_hexahedron) {
      cell_to_node_list[i_cell_node++] = gmsh_data.__hexahedra[i_hexahedron][0];
      cell_to_node_list[i_cell_node++] = gmsh_data.__hexahedra[i_hexahedron][1];
      cell_to_node_list[i_cell_node++] = gmsh_data.__hexahedra[i_hexahedron][2];
      cell_to_node_list[i_cell_node++] = gmsh_data.__hexahedra[i_hexahedron][3];
      cell_to_node_list[i_cell_node++] = gmsh_data.__hexahedra[i_hexahedron][4];
      cell_to_node_list[i_cell_node++] = gmsh_data.__hexahedra[i_hexahedron][5];
      cell_to_node_list[i_cell_node++] = gmsh_data.__hexahedra[i_hexahedron][6];
      cell_to_node_list[i_cell_node++] = gmsh_data.__hexahedra[i_hexahedron][7];
    }
    for (size_t i_prism = 0; i_prism < nb_prisms; ++i_prism) {
      cell_to_node_list[i_cell_node++] = gmsh_data.__prisms[i_prism][0];
      cell_to_node_list[i_cell_node++] = gmsh_data.__prisms[i_prism][1];
      cell_to_node_list[i_cell_node++] = gmsh_data.__prisms[i_prism][2];
      cell_to_node_list[i_cell_node++] = gmsh_data.__prisms[i_prism][3];
      cell_to_node_list[i_cell_node++] = gmsh_data.__prisms[i_prism][4];
      cell_to_node_list[i_cell_node++] = gmsh_data.__prisms[i_prism][5];
    }
    for (size_t i_pyramid = 0; i_pyramid < nb_pyramids; ++i_pyramid) {
      cell_to_node_list[i_cell_node++] = gmsh_data.__pyramids[i_pyramid][0];
      cell_to_node_list[i_cell_node++] = gmsh_data.__pyramids[i_pyramid][1];
      cell_to_node_list[i_cell_node++] = gmsh_data.__pyramids[i_pyramid][2];
      cell_to_node_list[i_cell_node++] = gmsh_data.__pyramids[i_pyramid][3];
      cell_to_node_list[i_cell_node++] = gmsh_data.__pyramids[i_pyramid][4];
    }
  }

  for (size_t j = 0; j < nb_tetrahedra; ++j) {
    cell_type_vector[j]   = CellType::Tetrahedron;
    cell_number_vector[j] = gmsh_data.__tetrahedra_number[j];
  }

  for (size_t j = 0; j < nb_hexahedra; ++j) {
    const size_t jh = nb_tetrahedra + j;

    cell_type_vector[jh]   = CellType::Hexahedron;
    cell_number_vector[jh] = gmsh_data.__hexahedra_number[j];
  }

  for (size_t j = 0; j < nb_prisms; ++j) {
    const size_t jp = nb_tetrahedra + nb_hexahedra + j;

    cell_type_vector[jp]   = CellType::Prism;
    cell_number_vector[jp] = gmsh_data.__prisms_number[j];
  }

  for (size_t j = 0; j < nb_pyramids; ++j) {
    const size_t jh = nb_tetrahedra + nb_hexahedra + nb_prisms + j;

    cell_type_vector[jh]   = CellType::Pyramid;
    cell_number_vector[jh] = gmsh_data.__pyramids_number[j];
  }

  descriptor.setCellNumberVector(cell_number_vector);
  descriptor.setCellTypeVector(cell_type_vector);
  descriptor.setCellToNodeMatrix(ConnectivityMatrix(cell_to_node_row, cell_to_node_list));

  std::map<unsigned int, std::vector<unsigned int>> ref_cells_map;
  for (unsigned int j = 0; j < gmsh_data.__tetrahedra_ref.size(); ++j) {
    const unsigned int elem_number = j;
    const unsigned int& ref        = gmsh_data.__tetrahedra_ref[j];
    ref_cells_map[ref].push_back(elem_number);
  }

  for (unsigned int j = 0; j < gmsh_data.__hexahedra_ref.size(); ++j) {
    const size_t elem_number = nb_tetrahedra + j;
    const unsigned int& ref  = gmsh_data.__hexahedra_ref[j];
    ref_cells_map[ref].push_back(elem_number);
  }

  for (unsigned int j = 0; j < gmsh_data.__prisms_ref.size(); ++j) {
    const size_t elem_number = nb_tetrahedra + nb_hexahedra + j;
    const unsigned int& ref  = gmsh_data.__prisms_ref[j];
    ref_cells_map[ref].push_back(elem_number);
  }

  for (unsigned int j = 0; j < gmsh_data.__pyramids_ref.size(); ++j) {
    const size_t elem_number = nb_tetrahedra + nb_hexahedra + nb_prisms + j;
    const unsigned int& ref  = gmsh_data.__pyramids_ref[j];
    ref_cells_map[ref].push_back(elem_number);
  }

  for (const auto& ref_cell_list : ref_cells_map) {
    Array<CellId> cell_list(ref_cell_list.second.size());
    for (size_t j = 0; j < ref_cell_list.second.size(); ++j) {
      cell_list[j] = ref_cell_list.second[j];
    }

    auto ref_id = [&] {
      if (auto i_physical_ref = gmsh_data.m_physical_ref_map.find(ref_cell_list.first);
          i_physical_ref != gmsh_data.m_physical_ref_map.end()) {
        return i_physical_ref->second.refId();
      } else {
        return RefId{ref_cell_list.first};
      }
    }();

    descriptor.addRefItemList(RefCellList(ref_id, cell_list, RefItemListBase::Type::set));
  }

  ConnectivityBuilderBase::_computeCellFaceAndFaceNodeConnectivities<3>(descriptor);

  const auto& node_number_vector = descriptor.nodeNumberVector();

  Array<size_t> face_nb_cell(descriptor.faceNumberVector().size());
  face_nb_cell.fill(0);

  const auto& cell_to_face_matrix = descriptor.cellToFaceMatrix();

  for (size_t j = 0; j < cell_to_face_matrix.numberOfRows(); ++j) {
    const auto& cell_face_list = cell_to_face_matrix[j];
    for (size_t l = 0; l < cell_face_list.size(); ++l) {
      face_nb_cell[cell_face_list[l]] += 1;
    }
  }

  const auto& descriptor_face_number_vector = descriptor.faceNumberVector();
  {
    std::unordered_map<int, FaceId> face_number_id_map = [&] {
      std::unordered_map<int, FaceId> mutable_face_number_id_map;
      for (size_t l = 0; l < descriptor_face_number_vector.size(); ++l) {
        mutable_face_number_id_map[descriptor_face_number_vector[l]] = l;
      }
      Assert(mutable_face_number_id_map.size() == descriptor_face_number_vector.size());
      return mutable_face_number_id_map;
    }();

    const auto& node_to_face_matrix = descriptor.nodeToFaceMatrix();
    const auto& face_to_node_matrix = descriptor.faceToNodeMatrix();

    const auto find_face = [&](std::vector<uint32_t> node_list) {
      size_t i_node_smallest_number = 0;
      for (size_t i_node = 1; i_node < node_list.size(); ++i_node) {
        if (node_number_vector[node_list[i_node]] < node_number_vector[node_list[i_node_smallest_number]]) {
          i_node_smallest_number = i_node;
        }
      }

      if (i_node_smallest_number != 0) {
        std::vector<uint64_t> buffer(node_list.size());
        for (size_t i_node = i_node_smallest_number; i_node < buffer.size(); ++i_node) {
          buffer[i_node - i_node_smallest_number] = node_list[i_node];
        }
        for (size_t i_node = 0; i_node < i_node_smallest_number; ++i_node) {
          buffer[i_node + node_list.size() - i_node_smallest_number] = node_list[i_node];
        }

        for (size_t i_node = 0; i_node < node_list.size(); ++i_node) {
          node_list[i_node] = buffer[i_node];
        }
      }

      if (node_number_vector[node_list[1]] > node_number_vector[node_list[node_list.size() - 1]]) {
        for (size_t i_node = 1; i_node <= (node_list.size() + 1) / 2 - 1; ++i_node) {
          std::swap(node_list[i_node], node_list[node_list.size() - i_node]);
        }
      }

      const auto& face_id_vector = node_to_face_matrix[node_list[0]];

      for (size_t i_face = 0; i_face < face_id_vector.size(); ++i_face) {
        const FaceId face_id          = face_id_vector[i_face];
        const auto& face_node_id_list = face_to_node_matrix[face_id];
        if (face_node_id_list.size() == node_list.size()) {
          bool is_same = true;
          for (size_t i_node = 1; i_node < face_node_id_list.size(); ++i_node) {
            is_same &= (face_node_id_list[i_node] == node_list[i_node]);
          }
          if (is_same) {
            return face_id;
          }
        }
      }

      std::stringstream error_msg;
      error_msg << "face (" << node_list[0];
      for (size_t i = 1; i < node_list.size(); ++i) {
        error_msg << ',' << node_list[i];
      }
      error_msg << ") not found";
      throw NormalError(error_msg.str());
    };

    Array<int> face_number_vector = copy(descriptor.faceNumberVector());
    std::map<unsigned int, std::vector<unsigned int>> ref_faces_map;
    for (unsigned int f = 0; f < gmsh_data.__triangles.size(); ++f) {
      const unsigned int face_id =
        find_face({gmsh_data.__triangles[f][0], gmsh_data.__triangles[f][1], gmsh_data.__triangles[f][2]});

      const unsigned int& ref = gmsh_data.__triangles_ref[f];
      ref_faces_map[ref].push_back(face_id);

      if (face_number_vector[face_id] != gmsh_data.__triangles_number[f]) {
        if (auto i_face = face_number_id_map.find(gmsh_data.__triangles_number[f]);
            i_face != face_number_id_map.end()) {
          const int other_face_id = i_face->second;
          std::swap(face_number_vector[face_id], face_number_vector[other_face_id]);

          face_number_id_map.erase(face_number_vector[face_id]);
          face_number_id_map.erase(face_number_vector[other_face_id]);

          face_number_id_map[face_number_vector[face_id]]       = face_id;
          face_number_id_map[face_number_vector[other_face_id]] = other_face_id;
        } else {
          face_number_id_map.erase(face_number_vector[face_id]);
          face_number_vector[face_id]                     = gmsh_data.__triangles_number[f];
          face_number_id_map[face_number_vector[face_id]] = face_id;
        }
      }
    }

    for (unsigned int f = 0; f < gmsh_data.__quadrangles.size(); ++f) {
      const unsigned int face_id = find_face({gmsh_data.__quadrangles[f][0], gmsh_data.__quadrangles[f][1],
                                              gmsh_data.__quadrangles[f][2], gmsh_data.__quadrangles[f][3]});

      const unsigned int& ref = gmsh_data.__quadrangles_ref[f];
      ref_faces_map[ref].push_back(face_id);
      if (face_number_vector[face_id] != gmsh_data.__quadrangles_number[f]) {
        if (auto i_face = face_number_id_map.find(gmsh_data.__quadrangles_number[f]);
            i_face != face_number_id_map.end()) {
          const int other_face_id = i_face->second;
          std::swap(face_number_vector[face_id], face_number_vector[other_face_id]);

          face_number_id_map.erase(face_number_vector[face_id]);
          face_number_id_map.erase(face_number_vector[other_face_id]);

          face_number_id_map[face_number_vector[face_id]]       = face_id;
          face_number_id_map[face_number_vector[other_face_id]] = other_face_id;
        } else {
          face_number_id_map.erase(face_number_vector[face_id]);
          face_number_vector[face_id]                     = gmsh_data.__quadrangles_number[f];
          face_number_id_map[face_number_vector[face_id]] = face_id;
        }
      }
    }
    descriptor.setFaceNumberVector(face_number_vector);

    for (const auto& ref_face_list : ref_faces_map) {
      Array<FaceId> face_list(ref_face_list.second.size());
      for (size_t j = 0; j < ref_face_list.second.size(); ++j) {
        face_list[j] = ref_face_list.second[j];
      }

      auto ref_id = [&] {
        if (auto i_physical_ref = gmsh_data.m_physical_ref_map.find(ref_face_list.first);
            i_physical_ref != gmsh_data.m_physical_ref_map.end()) {
          return i_physical_ref->second.refId();
        } else {
          return RefId{ref_face_list.first};
        }
      }();

      RefItemListBase::Type list_type = [&] {
        bool is_boundary = true;
        for (size_t i_face = 0; i_face < face_list.size(); ++i_face) {
          if (face_nb_cell[face_list[i_face]] > 1) {
            is_boundary = false;
            break;
          }
        }
        if (is_boundary) {
          return RefItemListBase::Type::boundary;
        }

        bool is_interface = true;
        for (size_t i_face = 0; i_face < face_list.size(); ++i_face) {
          if (face_nb_cell[face_list[i_face]] < 2) {
            is_interface = false;
            break;
          }
        }
        if (is_interface) {
          return RefItemListBase::Type::interface;
        }

        return RefItemListBase::Type::set;
      }();

      descriptor.addRefItemList(RefFaceList(ref_id, face_list, list_type));
    }
  }

  ConnectivityBuilderBase::_computeFaceEdgeAndEdgeNodeAndCellEdgeConnectivities<3>(descriptor);

  {
    Array<bool> is_boundary_edge(descriptor.edgeNumberVector().size());
    is_boundary_edge.fill(false);

    const auto& face_to_edge_matrix = descriptor.faceToEdgeMatrix();

    for (size_t i_face = 0; i_face < face_nb_cell.size(); ++i_face) {
      if (face_nb_cell[i_face] == 1) {
        auto face_edge_list = face_to_edge_matrix[i_face];
        for (size_t i_edge = 0; i_edge < face_edge_list.size(); ++i_edge) {
          is_boundary_edge[face_edge_list[i_edge]] = true;
        }
      }
    }

    const auto& node_to_edge_matrix = descriptor.nodeToEdgeMatrix();
    const auto& edge_to_node_matrix = descriptor.edgeToNodeMatrix();

    const auto find_edge = [&](uint32_t node0, uint32_t node1) {
      if (node_number_vector[node0] > node_number_vector[node1]) {
        std::swap(node0, node1);
      }
      const auto& edge_id_vector = node_to_edge_matrix[node0];

      for (size_t i_edge = 0; i_edge < edge_id_vector.size(); ++i_edge) {
        const EdgeId edge_id = edge_id_vector[i_edge];
        if (edge_to_node_matrix[edge_id][1] == node1) {
          return edge_id;
        }
      }

      std::stringstream error_msg;
      error_msg << "edge (" << node0 << ',' << node1 << ") not found";
      throw NormalError(error_msg.str());
    };

    std::unordered_map<int, EdgeId> edge_number_id_map = [&] {
      const auto& edge_number_vector = descriptor.edgeNumberVector();
      std::unordered_map<int, EdgeId> mutable_edge_number_id_map;
      for (size_t l = 0; l < edge_number_vector.size(); ++l) {
        mutable_edge_number_id_map[edge_number_vector[l]] = l;
      }
      Assert(mutable_edge_number_id_map.size() == edge_number_vector.size());
      return mutable_edge_number_id_map;
    }();

    Array<int> edge_number_vector = copy(descriptor.edgeNumberVector());
    std::map<unsigned int, std::vector<unsigned int>> ref_edges_map;
    for (unsigned int e = 0; e < gmsh_data.__edges.size(); ++e) {
      const unsigned int edge_id = find_edge(gmsh_data.__edges[e][0], gmsh_data.__edges[e][1]);
      const unsigned int& ref    = gmsh_data.__edges_ref[e];
      ref_edges_map[ref].push_back(edge_id);

      if (edge_number_vector[edge_id] != gmsh_data.__edges_number[e]) {
        if (auto i_edge = edge_number_id_map.find(gmsh_data.__edges_number[e]); i_edge != edge_number_id_map.end()) {
          const int other_edge_id = i_edge->second;
          std::swap(edge_number_vector[edge_id], edge_number_vector[other_edge_id]);

          edge_number_id_map.erase(edge_number_vector[edge_id]);
          edge_number_id_map.erase(edge_number_vector[other_edge_id]);

          edge_number_id_map[edge_number_vector[edge_id]]       = edge_id;
          edge_number_id_map[edge_number_vector[other_edge_id]] = other_edge_id;
        } else {
          edge_number_id_map.erase(edge_number_vector[edge_id]);
          edge_number_vector[edge_id]                     = gmsh_data.__edges_number[e];
          edge_number_id_map[edge_number_vector[edge_id]] = edge_id;
        }
      }
    }
    descriptor.setEdgeNumberVector(edge_number_vector);

    for (const auto& ref_edge_list : ref_edges_map) {
      Array<EdgeId> edge_list(ref_edge_list.second.size());
      for (size_t j = 0; j < ref_edge_list.second.size(); ++j) {
        edge_list[j] = ref_edge_list.second[j];
      }

      auto ref_id = [&] {
        if (auto i_physical_ref = gmsh_data.m_physical_ref_map.find(ref_edge_list.first);
            i_physical_ref != gmsh_data.m_physical_ref_map.end()) {
          return i_physical_ref->second.refId();
        } else {
          return RefId{ref_edge_list.first};
        }
      }();

      RefItemListBase::Type list_type = [&] {
        bool is_boundary = true;
        for (size_t i_edge = 0; i_edge < edge_list.size(); ++i_edge) {
          if (not is_boundary_edge[edge_list[i_edge]]) {
            is_boundary = false;
          }
        }
        if (is_boundary) {
          return RefItemListBase::Type::boundary;
        }

        return RefItemListBase::Type::set;
      }();

      descriptor.addRefItemList(RefEdgeList(ref_id, edge_list, list_type));
    }
  }

  Array<bool> is_boundary_node(node_number_vector.size());
  is_boundary_node.fill(false);

  const auto& face_to_node_matrix = descriptor.faceToNodeMatrix();

  for (size_t i_face = 0; i_face < face_nb_cell.size(); ++i_face) {
    if (face_nb_cell[i_face] == 1) {
      const auto& face_node_list = face_to_node_matrix[i_face];
      for (size_t i_node = 0; i_node < face_node_list.size(); ++i_node) {
        const NodeId node_id      = face_node_list[i_node];
        is_boundary_node[node_id] = true;
      }
    }
  }

  std::map<unsigned int, std::vector<unsigned int>> ref_points_map;
  for (unsigned int r = 0; r < gmsh_data.__points.size(); ++r) {
    const unsigned int point_number = gmsh_data.__points[r];
    const unsigned int& ref         = gmsh_data.__points_ref[r];
    ref_points_map[ref].push_back(point_number);
  }

  for (const auto& ref_point_list : ref_points_map) {
    Array<NodeId> point_list(ref_point_list.second.size());
    for (size_t j = 0; j < ref_point_list.second.size(); ++j) {
      point_list[j] = ref_point_list.second[j];
    }

    auto ref_id = [&] {
      if (auto i_physical_ref = gmsh_data.m_physical_ref_map.find(ref_point_list.first);
          i_physical_ref != gmsh_data.m_physical_ref_map.end()) {
        return i_physical_ref->second.refId();
      } else {
        return RefId{ref_point_list.first};
      }
    }();

    RefItemListBase::Type list_type = [&] {
      bool is_boundary = true;
      for (size_t i_node = 0; i_node < point_list.size(); ++i_node) {
        if (not is_boundary_node[point_list[i_node]]) {
          is_boundary = false;
        }
      }
      if (is_boundary) {
        return RefItemListBase::Type::boundary;
      }

      return RefItemListBase::Type::set;
    }();

    descriptor.addRefItemList(RefNodeList(ref_id, point_list, list_type));
  }

  descriptor.setCellOwnerVector([&] {
    Array<int> cell_owner_vector(nb_cells);
    cell_owner_vector.fill(parallel::rank());
    return cell_owner_vector;
  }());

  descriptor.setFaceOwnerVector([&] {
    Array<int> face_owner_vector(descriptor.faceNumberVector().size());
    face_owner_vector.fill(parallel::rank());
    return face_owner_vector;
  }());

  descriptor.setEdgeOwnerVector([&] {
    Array<int> edge_owner_vector(descriptor.edgeNumberVector().size());
    edge_owner_vector.fill(parallel::rank());
    return edge_owner_vector;
  }());

  descriptor.setNodeOwnerVector([&] {
    Array<int> node_owner_vector(node_number_vector.size());
    node_owner_vector.fill(parallel::rank());
    return node_owner_vector;
  }());

  m_connectivity = Connectivity3D::build(descriptor);
}

GmshReader::GmshReader(const std::string& filename) : m_filename(filename)
{
  if (parallel::rank() == 0) {
    m_fin.open(m_filename);
    if (not m_fin) {
      std::ostringstream os;
      os << "cannot read file '" << m_filename << "'";
      throw NormalError(os.str());
    }

    // gmsh 2.2 format keywords
    __keywordList["$MeshFormat"]       = MESHFORMAT;
    __keywordList["$EndMeshFormat"]    = ENDMESHFORMAT;
    __keywordList["$Nodes"]            = NODES;
    __keywordList["$EndNodes"]         = ENDNODES;
    __keywordList["$Elements"]         = ELEMENTS;
    __keywordList["$EndElements"]      = ENDELEMENTS;
    __keywordList["$PhysicalNames"]    = PHYSICALNAMES;
    __keywordList["$EndPhysicalNames"] = ENDPHYSICALNAMES;
    __keywordList["$Periodic"]         = PERIODIC;
    __keywordList["$EndPeriodic"]      = ENDPERIODIC;

    __numberOfPrimitiveNodes.resize(16);
    __numberOfPrimitiveNodes[0]  = 2;    // edge
    __numberOfPrimitiveNodes[1]  = 3;    // triangle
    __numberOfPrimitiveNodes[2]  = 4;    // quadrangle
    __numberOfPrimitiveNodes[3]  = 4;    // Tetrahedron
    __numberOfPrimitiveNodes[4]  = 8;    // Hexaredron
    __numberOfPrimitiveNodes[5]  = 6;    // Prism
    __numberOfPrimitiveNodes[6]  = 5;    // Pyramid
    __numberOfPrimitiveNodes[7]  = 3;    // second order edge
    __numberOfPrimitiveNodes[8]  = 6;    // second order triangle
    __numberOfPrimitiveNodes[9]  = 9;    // second order quadrangle
    __numberOfPrimitiveNodes[10] = 10;   // second order tetrahedron
    __numberOfPrimitiveNodes[11] = 27;   // second order hexahedron
    __numberOfPrimitiveNodes[12] = 18;   // second order prism
    __numberOfPrimitiveNodes[13] = 14;   // second order pyramid
    __numberOfPrimitiveNodes[14] = 1;    // point

    __primitivesNames[0]      = "edges";
    __supportedPrimitives[0]  = true;
    __primitivesNames[1]      = "triangles";
    __supportedPrimitives[1]  = true;
    __primitivesNames[2]      = "quadrangles";
    __supportedPrimitives[2]  = true;
    __primitivesNames[3]      = "tetrahedra";
    __supportedPrimitives[3]  = true;
    __primitivesNames[4]      = "hexahedra";
    __supportedPrimitives[4]  = true;
    __primitivesNames[5]      = "prisms";
    __supportedPrimitives[5]  = true;
    __primitivesNames[6]      = "pyramids";
    __supportedPrimitives[6]  = true;
    __primitivesNames[7]      = "second order edges";
    __supportedPrimitives[7]  = false;
    __primitivesNames[8]      = "second order triangles";
    __supportedPrimitives[8]  = false;
    __primitivesNames[9]      = "second order quadrangles";
    __supportedPrimitives[9]  = false;
    __primitivesNames[10]     = "second order tetrahedra";
    __supportedPrimitives[10] = false;
    __primitivesNames[11]     = "second order hexahedra";
    __supportedPrimitives[11] = false;
    __primitivesNames[12]     = "second order prisms";
    __supportedPrimitives[12] = false;
    __primitivesNames[13]     = "second order pyramids";
    __supportedPrimitives[13] = false;
    __primitivesNames[14]     = "point";
    __supportedPrimitives[14] = true;

    std::cout << "Reading file '" << m_filename << "'\n";

    // Getting vertices list
    GmshReader::Keyword kw = this->__nextKeyword();
    switch (kw.second) {
    // case NOD: {
    //   this->__readGmsh1();
    //   break;
    // }
    case MESHFORMAT: {
      double fileVersion = this->_getReal();
      if (fileVersion != 2.2) {
        throw NormalError("Cannot read Gmsh format '" + stringify(fileVersion) + "'");
      }
      int fileType = this->_getInteger();
      if ((fileType < 0) or (fileType > 1)) {
        throw NormalError("Cannot read Gmsh file type '" + stringify(fileType) + "'");
      }

      int dataSize = this->_getInteger();
      if (dataSize != sizeof(double)) {
        throw NormalError("Data size not supported '" + stringify(dataSize) + "'");
      }

      kw = this->__nextKeyword();
      if (kw.second != ENDMESHFORMAT) {
        throw NormalError("reading file '" + m_filename + "': expecting $EndMeshFormat, '" + kw.first + "' was found");
      }

      this->__readGmshFormat2_2();

      break;
    }
    default: {
      throw NormalError("cannot determine format version of '" + m_filename + "'");
    }
    }

    this->__proceedData();
  }
  std::cout << std::flush;

  if (parallel::size() > 1) {
    std::cout << "Sequential mesh read! Need to be dispatched\n" << std::flush;

    const int mesh_dimension = [&]() {
      int mutable_mesh_dimension = -1;   // unknown mesh dimension
      if (m_mesh) {
        mutable_mesh_dimension = std::visit([](auto&& mesh) { return mesh->dimension(); }, m_mesh->variant());
      }

      Array<int> dimensions = parallel::allGather(mutable_mesh_dimension);
      std::set<int> dimension_set;
      for (size_t i = 0; i < dimensions.size(); ++i) {
        const int i_dimension = dimensions[i];
        if (i_dimension != -1) {
          dimension_set.insert(i_dimension);
        }
      }
      if (dimension_set.size() != 1) {
        throw NormalError("dimensions of read mesh parts differ!\n");
      }

      return *begin(dimension_set);
    }();

    switch (mesh_dimension) {
    case 1: {
      this->_dispatch<1>(DispatchType::initial);
      break;
    }
    case 2: {
      this->_dispatch<2>(DispatchType::initial);
      break;
    }
    case 3: {
      this->_dispatch<3>(DispatchType::initial);
      break;
    }
    default: {
      throw NormalError("invalid mesh dimension" + stringify((mesh_dimension)));
    }
    }
  }
}

void
GmshReader::__readVertices()
{
  const int numberOfVerices = this->_getInteger();
  std::cout << "- Number Of Vertices: " << numberOfVerices << '\n';
  if (numberOfVerices < 0) {
    throw NormalError("reading file '" + this->m_filename + "': number of vertices is negative");
  }

  m_mesh_data.__verticesNumbers.resize(numberOfVerices);
  m_mesh_data.__vertices = Array<R3>(numberOfVerices);

  for (int i = 0; i < numberOfVerices; ++i) {
    m_mesh_data.__verticesNumbers[i] = this->_getInteger();
    const double x                   = this->_getReal();
    const double y                   = this->_getReal();
    const double z                   = this->_getReal();
    m_mesh_data.__vertices[i]        = TinyVector<3, double>(x, y, z);
  }
}

void
GmshReader::__readElements2_2()
{
  const int numberOfElements = this->_getInteger();
  std::cout << "- Number Of Elements: " << numberOfElements << '\n';
  if (numberOfElements < 0) {
    throw NormalError("reading file '" + m_filename + "': number of elements is negative");
  }

  m_mesh_data.__elementNumber.resize(numberOfElements);
  m_mesh_data.__elementType.resize(numberOfElements);
  m_mesh_data.__references.resize(numberOfElements);
  m_mesh_data.__elementVertices.resize(numberOfElements);

  for (int i = 0; i < numberOfElements; ++i) {
    m_mesh_data.__elementNumber[i] = this->_getInteger();
    const int elementType          = this->_getInteger() - 1;

    if ((elementType < 0) or (elementType > 14)) {
      throw NormalError("reading file '" + m_filename + "': unknown element type '" + stringify(elementType) + "'");
    }
    m_mesh_data.__elementType[i] = elementType;
    const int numberOfTags       = this->_getInteger();
    m_mesh_data.__references[i]  = this->_getInteger();   // physical reference
    for (int tag = 1; tag < numberOfTags; ++tag) {
      this->_getInteger();   // drops remaining tags
    }

    const int numberOfVertices = __numberOfPrimitiveNodes[elementType];
    m_mesh_data.__elementVertices[i].resize(numberOfVertices);
    for (int j = 0; j < numberOfVertices; ++j) {
      m_mesh_data.__elementVertices[i][j] = this->_getInteger();
    }
  }
}

void
GmshReader::__readPhysicalNames2_2()
{
  const int number_of_names = this->_getInteger();
  for (int i = 0; i < number_of_names; ++i) {
    const int physical_dimension = this->_getInteger();
    const int physical_number    = this->_getInteger();
    std::string physical_name;
    m_fin >> physical_name;
    physical_name = std::regex_replace(physical_name, std::regex("(\")"), "");

    PhysicalRefId physical_ref_id(physical_dimension, RefId(physical_number, physical_name));

    if (auto i_searched_physical_ref_id = m_mesh_data.m_physical_ref_map.find(physical_number);
        i_searched_physical_ref_id != m_mesh_data.m_physical_ref_map.end()) {
      std::stringstream os;
      os << "Physical reference id '" << physical_ref_id << "' already defined as '"
         << i_searched_physical_ref_id->second << "'!";
      throw NormalError(os.str());
    }
    m_mesh_data.m_physical_ref_map[physical_number] = physical_ref_id;
  }
}

void
GmshReader::__readPeriodic2_2()
{
  // This is just a compatibility reading, periodic information is not
  // used
  const int number_of_periodic = this->_getInteger();
  for (int i = 0; i < number_of_periodic; ++i) {
    [[maybe_unused]] const int dimension              = this->_getInteger();
    [[maybe_unused]] const int id                     = this->_getInteger();
    [[maybe_unused]] const int master_id              = this->_getInteger();
    [[maybe_unused]] const int nb_corresponding_nodes = this->_getInteger();
    for (int i_node = 0; i_node < nb_corresponding_nodes; ++i_node) {
      [[maybe_unused]] const int node_id        = this->_getInteger();
      [[maybe_unused]] const int node_master_id = this->_getInteger();
    }
  }
}

void
GmshReader::__proceedData()
{
  TinyVector<15, int> elementNumber = zero;
  std::vector<std::set<size_t>> elementReferences(15);

  for (size_t i = 0; i < m_mesh_data.__elementType.size(); ++i) {
    const int elementType = m_mesh_data.__elementType[i];
    elementNumber[elementType]++;
    elementReferences[elementType].insert(m_mesh_data.__references[i]);
  }

  for (size_t i = 0; i < elementNumber.dimension(); ++i) {
    if (elementNumber[i] > 0) {
      std::cout << "  - Number of " << __primitivesNames[i] << ": " << elementNumber[i];
      if (not(__supportedPrimitives[i])) {
        std::cout << " [" << rang::fg::yellow << "IGNORED" << rang::style::reset << "]";
      } else {
        std::cout << " references: ";
        for (std::set<size_t>::const_iterator iref = elementReferences[i].begin(); iref != elementReferences[i].end();
             ++iref) {
          if (iref != elementReferences[i].begin())
            std::cout << ',';
          std::cout << *iref;
        }

        switch (i) {
        // Supported entities
        case 0: {   // edges
          m_mesh_data.__edges = Array<GmshData::Edge>(elementNumber[i]);
          m_mesh_data.__edges_ref.resize(elementNumber[i]);
          m_mesh_data.__edges_number.resize(elementNumber[i]);
          break;
        }
        case 1: {   // triangles
          m_mesh_data.__triangles = Array<GmshData::Triangle>(elementNumber[i]);
          m_mesh_data.__triangles_ref.resize(elementNumber[i]);
          m_mesh_data.__triangles_number.resize(elementNumber[i]);
          break;
        }
        case 2: {   // quadrangles
          m_mesh_data.__quadrangles = Array<GmshData::Quadrangle>(elementNumber[i]);
          m_mesh_data.__quadrangles_ref.resize(elementNumber[i]);
          m_mesh_data.__quadrangles_number.resize(elementNumber[i]);
          break;
        }
        case 3: {   // tetrahedra
          m_mesh_data.__tetrahedra = Array<GmshData::Tetrahedron>(elementNumber[i]);
          m_mesh_data.__tetrahedra_ref.resize(elementNumber[i]);
          m_mesh_data.__tetrahedra_number.resize(elementNumber[i]);
          break;
        }
        case 4: {   // hexahedra
          m_mesh_data.__hexahedra = Array<GmshData::Hexahedron>(elementNumber[i]);
          m_mesh_data.__hexahedra_ref.resize(elementNumber[i]);
          m_mesh_data.__hexahedra_number.resize(elementNumber[i]);
          break;
        }
        case 5: {   // prism
          m_mesh_data.__prisms = Array<GmshData::Prism>(elementNumber[i]);
          m_mesh_data.__prisms_ref.resize(elementNumber[i]);
          m_mesh_data.__prisms_number.resize(elementNumber[i]);
          break;
        }
        case 6: {   // pyramid
          m_mesh_data.__pyramids = Array<GmshData::Pyramid>(elementNumber[i]);
          m_mesh_data.__pyramids_ref.resize(elementNumber[i]);
          m_mesh_data.__pyramids_number.resize(elementNumber[i]);
          break;
        }
        case 14: {   // point
          m_mesh_data.__points = Array<GmshData::Point>(elementNumber[i]);
          m_mesh_data.__points_ref.resize(elementNumber[i]);
          m_mesh_data.__points_number.resize(elementNumber[i]);
          break;
        }
          // Unsupported entities
        case 7:    // second order edge
        case 8:    // second order triangle
        case 9:    // second order quadrangle
        case 10:   // second order tetrahedron
        case 11:   // second order hexahedron
        case 12:   // second order prism
        case 13:   // second order pyramid
        default: {
          throw NormalError("reading file '" + m_filename + "': ff3d cannot read this kind of element");
        }
        }
      }
      std::cout << '\n';
    }
  }

  std::cout << "- Building correspondance table\n";
  int minNumber = std::numeric_limits<int>::max();
  int maxNumber = std::numeric_limits<int>::min();
  for (size_t i = 0; i < m_mesh_data.__verticesNumbers.size(); ++i) {
    const int& vertexNumber = m_mesh_data.__verticesNumbers[i];
    minNumber               = std::min(minNumber, vertexNumber);
    maxNumber               = std::max(maxNumber, vertexNumber);
  }

  if (minNumber < 0) {
    throw NotImplementedError("reading file '" + m_filename + "': negative vertices number");
  }

  // A value of -1 means that the vertex is unknown
  m_mesh_data.__verticesCorrepondance.resize(maxNumber + 1, -1);

  for (size_t i = 0; i < m_mesh_data.__verticesNumbers.size(); ++i) {
    m_mesh_data.__verticesCorrepondance[m_mesh_data.__verticesNumbers[i]] = i;
  }

  // reset element number to count them while filling structures
  elementNumber = zero;

  // Element structures filling
  for (size_t i = 0; i < m_mesh_data.__elementType.size(); ++i) {
    std::vector<int>& elementVertices = m_mesh_data.__elementVertices[i];
    switch (m_mesh_data.__elementType[i]) {
    // Supported entities
    case 0: {   // edge
      int& edgeNumber = elementNumber[0];
      const int a     = m_mesh_data.__verticesCorrepondance[elementVertices[0]];
      const int b     = m_mesh_data.__verticesCorrepondance[elementVertices[1]];
      if ((a < 0) or (b < 0)) {
        throw NormalError("reading file '" + m_filename + "': error reading element " + stringify(i) +
                          " [bad vertices definition]");
      }
      m_mesh_data.__edges[edgeNumber]        = GmshData::Edge(a, b);
      m_mesh_data.__edges_ref[edgeNumber]    = m_mesh_data.__references[i];
      m_mesh_data.__edges_number[edgeNumber] = m_mesh_data.__elementNumber[i];
      edgeNumber++;   // one more edge
      break;
    }
    case 1: {   // triangles
      int& triangleNumber = elementNumber[1];

      const int a = m_mesh_data.__verticesCorrepondance[elementVertices[0]];
      const int b = m_mesh_data.__verticesCorrepondance[elementVertices[1]];
      const int c = m_mesh_data.__verticesCorrepondance[elementVertices[2]];
      if ((a < 0) or (b < 0) or (c < 0)) {
        throw NormalError("reading file '" + m_filename + "': error reading element " + stringify(i) +
                          " [bad vertices definition]");
      }
      m_mesh_data.__triangles[triangleNumber]        = GmshData::Triangle(a, b, c);
      m_mesh_data.__triangles_ref[triangleNumber]    = m_mesh_data.__references[i];
      m_mesh_data.__triangles_number[triangleNumber] = m_mesh_data.__elementNumber[i];
      triangleNumber++;   // one more triangle
      break;
    }
    case 2: {   // quadrangle
      int& quadrilateralNumber = elementNumber[2];

      const int a = m_mesh_data.__verticesCorrepondance[elementVertices[0]];
      const int b = m_mesh_data.__verticesCorrepondance[elementVertices[1]];
      const int c = m_mesh_data.__verticesCorrepondance[elementVertices[2]];
      const int d = m_mesh_data.__verticesCorrepondance[elementVertices[3]];
      if ((a < 0) or (b < 0) or (c < 0) or (d < 0)) {
        throw NormalError("reading file '" + m_filename + "': error reading element " + stringify(i) +
                          " [bad vertices definition]");
      }
      m_mesh_data.__quadrangles[quadrilateralNumber]        = GmshData::Quadrangle(a, b, c, d);
      m_mesh_data.__quadrangles_ref[quadrilateralNumber]    = m_mesh_data.__references[i];
      m_mesh_data.__quadrangles_number[quadrilateralNumber] = m_mesh_data.__elementNumber[i];
      quadrilateralNumber++;   // one more quadrangle
      break;
    }
    case 3: {   // tetrahedra
      int& tetrahedronNumber = elementNumber[3];

      const int a = m_mesh_data.__verticesCorrepondance[elementVertices[0]];
      const int b = m_mesh_data.__verticesCorrepondance[elementVertices[1]];
      const int c = m_mesh_data.__verticesCorrepondance[elementVertices[2]];
      const int d = m_mesh_data.__verticesCorrepondance[elementVertices[3]];
      if ((a < 0) or (b < 0) or (c < 0) or (d < 0)) {
        throw NormalError("reading file '" + m_filename + "': error reading element " + stringify(i) +
                          " [bad vertices definition]");
      }
      m_mesh_data.__tetrahedra[tetrahedronNumber]        = GmshData::Tetrahedron(a, b, c, d);
      m_mesh_data.__tetrahedra_ref[tetrahedronNumber]    = m_mesh_data.__references[i];
      m_mesh_data.__tetrahedra_number[tetrahedronNumber] = m_mesh_data.__elementNumber[i];
      tetrahedronNumber++;   // one more tetrahedron
      break;
    }
    case 4: {   // hexaredron
      int& hexahedronNumber = elementNumber[4];

      const int a = m_mesh_data.__verticesCorrepondance[elementVertices[0]];
      const int b = m_mesh_data.__verticesCorrepondance[elementVertices[1]];
      const int c = m_mesh_data.__verticesCorrepondance[elementVertices[2]];
      const int d = m_mesh_data.__verticesCorrepondance[elementVertices[3]];
      const int e = m_mesh_data.__verticesCorrepondance[elementVertices[4]];
      const int f = m_mesh_data.__verticesCorrepondance[elementVertices[5]];
      const int g = m_mesh_data.__verticesCorrepondance[elementVertices[6]];
      const int h = m_mesh_data.__verticesCorrepondance[elementVertices[7]];
      if ((a < 0) or (b < 0) or (c < 0) or (d < 0) or (e < 0) or (f < 0) or (g < 0) or (h < 0)) {
        throw NormalError("reading file '" + m_filename + "': error reading element " + stringify(i) +
                          " [bad vertices definition]");
      }
      m_mesh_data.__hexahedra[hexahedronNumber]        = GmshData::Hexahedron(a, b, c, d, e, f, g, h);
      m_mesh_data.__hexahedra_ref[hexahedronNumber]    = m_mesh_data.__references[i];
      m_mesh_data.__hexahedra_number[hexahedronNumber] = m_mesh_data.__elementNumber[i];
      hexahedronNumber++;   // one more hexahedron
      break;
    }
    case 5: {   // prism
      int& prism_number = elementNumber[5];
      const int a       = m_mesh_data.__verticesCorrepondance[elementVertices[0]];
      const int b       = m_mesh_data.__verticesCorrepondance[elementVertices[1]];
      const int c       = m_mesh_data.__verticesCorrepondance[elementVertices[2]];
      const int d       = m_mesh_data.__verticesCorrepondance[elementVertices[3]];
      const int e       = m_mesh_data.__verticesCorrepondance[elementVertices[4]];
      const int f       = m_mesh_data.__verticesCorrepondance[elementVertices[5]];
      if ((a < 0) or (b < 0) or (c < 0) or (d < 0) or (e < 0) or (f < 0)) {
        throw NormalError("reading file '" + m_filename + "': error reading element " + stringify(i) +
                          " [bad vertices definition]");
      }
      m_mesh_data.__prisms[prism_number]        = GmshData::Prism(a, b, c, d, e, f);
      m_mesh_data.__prisms_ref[prism_number]    = m_mesh_data.__references[i];
      m_mesh_data.__prisms_number[prism_number] = m_mesh_data.__elementNumber[i];
      prism_number++;   // one more prism
      break;
    }
    case 6: {   // pyramid
      int& pyramid_number = elementNumber[6];

      const int a = m_mesh_data.__verticesCorrepondance[elementVertices[0]];
      const int b = m_mesh_data.__verticesCorrepondance[elementVertices[1]];
      const int c = m_mesh_data.__verticesCorrepondance[elementVertices[2]];
      const int d = m_mesh_data.__verticesCorrepondance[elementVertices[3]];
      const int e = m_mesh_data.__verticesCorrepondance[elementVertices[4]];
      if ((a < 0) or (b < 0) or (c < 0) or (d < 0) or (e < 0)) {
        throw NormalError("reading file '" + m_filename + "': error reading element " + stringify(i) +
                          " [bad vertices definition]");
      }
      m_mesh_data.__pyramids[pyramid_number]        = GmshData::Pyramid(a, b, c, d, e);
      m_mesh_data.__pyramids_ref[pyramid_number]    = m_mesh_data.__references[i];
      m_mesh_data.__pyramids_number[pyramid_number] = m_mesh_data.__elementNumber[i];
      pyramid_number++;   // one more hexahedron
      break;
    }
      // Unsupported entities
    case 14: {   // point
      int& point_number                         = elementNumber[14];
      const int a                               = m_mesh_data.__verticesCorrepondance[elementVertices[0]];
      m_mesh_data.__points[point_number]        = a;
      m_mesh_data.__points_ref[point_number]    = m_mesh_data.__references[i];
      m_mesh_data.__points_number[point_number] = m_mesh_data.__elementNumber[i];
      point_number++;
      break;
    }
    case 7:      // second order edge
    case 8:      // second order triangle
    case 9:      // second order quadrangle
    case 10:     // second order tetrahedron
    case 11:     // second order hexahedron
    case 12:     // second order prism
    case 13: {   // second order pyramid
    }
    default: {
      throw NormalError("reading file '" + m_filename + "': ff3d cannot read this kind of element");
    }
    }
  }

  TinyVector<15, int> dimension0_mask = zero;
  dimension0_mask[14]                 = 1;
  TinyVector<15, int> dimension1_mask = zero;
  dimension1_mask[0]                  = 1;
  dimension1_mask[7]                  = 1;
  TinyVector<15, int> dimension2_mask = zero;
  dimension2_mask[1]                  = 1;
  dimension2_mask[2]                  = 1;
  dimension2_mask[8]                  = 1;
  dimension2_mask[9]                  = 1;
  TinyVector<15, int> dimension3_mask = zero;
  dimension3_mask[3]                  = 1;
  dimension3_mask[4]                  = 1;
  dimension3_mask[5]                  = 1;
  dimension3_mask[6]                  = 1;
  dimension3_mask[10]                 = 1;
  dimension3_mask[11]                 = 1;
  dimension3_mask[12]                 = 1;
  dimension3_mask[13]                 = 1;

  std::cout << "- dimension 0 entities: " << dot(dimension0_mask, elementNumber) << '\n';
  std::cout << "- dimension 1 entities: " << dot(dimension1_mask, elementNumber) << '\n';
  std::cout << "- dimension 2 entities: " << dot(dimension2_mask, elementNumber) << '\n';
  std::cout << "- dimension 3 entities: " << dot(dimension3_mask, elementNumber) << '\n';
  if (dot(dimension3_mask, elementNumber) > 0) {
    const size_t nb_cells = dot(dimension3_mask, elementNumber);

    GmshConnectivityBuilder<3> connectivity_builder(m_mesh_data, nb_cells);

    std::shared_ptr p_connectivity =
      std::dynamic_pointer_cast<const Connectivity3D>(connectivity_builder.connectivity());
    const Connectivity3D& connectivity = *p_connectivity;

    using MeshType = Mesh<3>;
    using Rd       = TinyVector<3, double>;

    NodeValue<Rd> xr(connectivity);
    for (NodeId i = 0; i < m_mesh_data.__vertices.size(); ++i) {
      xr[i][0] = m_mesh_data.__vertices[i][0];
      xr[i][1] = m_mesh_data.__vertices[i][1];
      xr[i][2] = m_mesh_data.__vertices[i][2];
    }
    m_mesh = std::make_shared<MeshVariant>(std::make_shared<const MeshType>(p_connectivity, xr));
    this->_checkMesh<3>();

  } else if (dot(dimension2_mask, elementNumber) > 0) {
    const size_t nb_cells = dot(dimension2_mask, elementNumber);

    GmshConnectivityBuilder<2> connectivity_builder(m_mesh_data, nb_cells);

    std::shared_ptr p_connectivity =
      std::dynamic_pointer_cast<const Connectivity2D>(connectivity_builder.connectivity());
    const Connectivity2D& connectivity = *p_connectivity;

    using MeshType = Mesh<2>;
    using Rd       = TinyVector<2, double>;

    NodeValue<Rd> xr(connectivity);
    for (NodeId i = 0; i < m_mesh_data.__vertices.size(); ++i) {
      xr[i][0] = m_mesh_data.__vertices[i][0];
      xr[i][1] = m_mesh_data.__vertices[i][1];
    }
    m_mesh = std::make_shared<MeshVariant>(std::make_shared<const MeshType>(p_connectivity, xr));
    this->_checkMesh<2>();

  } else if (dot(dimension1_mask, elementNumber) > 0) {
    const size_t nb_cells = dot(dimension1_mask, elementNumber);

    GmshConnectivityBuilder<1> connectivity_builder(m_mesh_data, nb_cells);

    std::shared_ptr p_connectivity =
      std::dynamic_pointer_cast<const Connectivity1D>(connectivity_builder.connectivity());
    const Connectivity1D& connectivity = *p_connectivity;

    using MeshType = Mesh<1>;
    using Rd       = TinyVector<1, double>;

    NodeValue<Rd> xr(connectivity);
    for (NodeId i = 0; i < m_mesh_data.__vertices.size(); ++i) {
      xr[i][0] = m_mesh_data.__vertices[i][0];
    }

    m_mesh = std::make_shared<MeshVariant>(std::make_shared<const MeshType>(p_connectivity, xr));
    this->_checkMesh<1>();

  } else {
    throw NormalError("using a dimension 0 mesh is forbidden");
  }
}

GmshReader::Keyword
GmshReader::__nextKeyword()
{
  GmshReader::Keyword kw;

  std::string aKeyword;
  m_fin >> aKeyword;
  if (not m_fin) {
    kw.second = EndOfFile;
    return kw;
  }

  KeywordList::iterator i = __keywordList.find(aKeyword.c_str());

  if (i != __keywordList.end()) {
    kw.first  = (*i).first;
    kw.second = (*i).second;
    return kw;
  }

  throw NormalError("reading file '" + m_filename + "': unknown keyword '" + aKeyword + "'");

  kw.first  = aKeyword;
  kw.second = Unknown;
  return kw;
}

void
GmshReader::__readGmshFormat2_2()
{
  std::cout << "- Reading Gmsh format 2.2\n";
  GmshReader::Keyword kw = std::make_pair("", Unknown);
  while (kw.second != EndOfFile) {
    kw = this->__nextKeyword();
    switch (kw.second) {
    case NODES: {
      this->__readVertices();
      if (this->__nextKeyword().second != ENDNODES) {
        throw NormalError("reading file '" + m_filename + "': expecting $EndNodes, '" + kw.first + "' was found");
      }
      break;
    }
    case ELEMENTS: {
      this->__readElements2_2();
      kw = this->__nextKeyword();
      if (kw.second != ENDELEMENTS) {
        throw NormalError("reading file '" + m_filename + "': expecting $EndElements, '" + kw.first + "' was found");
      }
      break;
    }
    case PHYSICALNAMES: {
      this->__readPhysicalNames2_2();
      if (this->__nextKeyword().second != ENDPHYSICALNAMES) {
        throw NormalError("reading file '" + m_filename + "': expecting $EndPhysicalNames, '" + kw.first +
                          "' was found");
      }
      break;
    }
    case PERIODIC: {
      this->__readPeriodic2_2();
      if (this->__nextKeyword().second != ENDPERIODIC) {
        throw NormalError("reading file '" + m_filename + "': expecting $EndPeriodic, '" + kw.first + "' was found");
      }
      break;
    }

    case EndOfFile: {
      break;
    }
    default: {
      throw NormalError("reading file '" + m_filename + "': unexpected '" + kw.first + "'");
    }
    }
  }
}
