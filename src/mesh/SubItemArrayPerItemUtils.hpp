#ifndef SUB_ITEM_ARRAY_PER_ITEM_UTILS_HPP
#define SUB_ITEM_ARRAY_PER_ITEM_UTILS_HPP

#include <utils/Messenger.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/SubItemArrayPerItem.hpp>
#include <mesh/Synchronizer.hpp>
#include <mesh/SynchronizerManager.hpp>
#include <utils/PugsTraits.hpp>

template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
std::remove_const_t<DataType>
min(const SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_array_per_item)
{
  using SubItemArrayPerItemType = SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>;
  constexpr ItemType item_type  = ItemOfItem::item_type;
  using ItemIsOwnedType         = ItemValue<const bool, item_type>;
  using data_type               = std::remove_const_t<typename SubItemArrayPerItemType::data_type>;
  using index_type              = typename SubItemArrayPerItemType::index_type;

  static_assert(std::is_arithmetic_v<data_type>, "min cannot be called on non-arithmetic data");
  static_assert(not std::is_same_v<data_type, bool>, "min cannot be called on boolean data");

  class SubItemArrayPerItemMin
  {
   private:
    const SubItemArrayPerItemType& m_sub_item_array_per_item;
    const ItemIsOwnedType m_is_owned;

   public:
    PUGS_INLINE
    operator data_type()
    {
      data_type reduced_value;
      parallel_reduce(m_sub_item_array_per_item.numberOfItems(), *this, reduced_value);
      return reduced_value;
    }

    PUGS_INLINE
    void
    operator()(const index_type& item_id, data_type& data) const
    {
      if (m_is_owned[item_id]) {
        auto sub_item_table = m_sub_item_array_per_item.itemTable(item_id);
        for (size_t i = 0; i < sub_item_table.numberOfRows(); ++i) {
          for (size_t j = 0; j < sub_item_table.numberOfColumns(); ++j) {
            if (sub_item_table(i, j) < data) {
              data = sub_item_table(i, j);
            }
          }
        }
      }
    }

    PUGS_INLINE
    void
    join(data_type& dst, const data_type& src) const
    {
      if (src < dst) {
        // cannot be reached if initial array is the min
        dst = src;   // LCOV_EXCL_LINE
      }
    }

    PUGS_INLINE
    void
    init(data_type& value) const
    {
      value = std::numeric_limits<data_type>::max();
    }

    PUGS_INLINE
    SubItemArrayPerItemMin(const SubItemArrayPerItemType& item_array)
      : m_sub_item_array_per_item(item_array), m_is_owned([&](const IConnectivity& connectivity) {
          Assert((connectivity.dimension() > 0) and (connectivity.dimension() <= 3),
                 "unexpected connectivity dimension");

          switch (connectivity.dimension()) {
          case 1: {
            const auto& connectivity_1d = static_cast<const Connectivity1D&>(connectivity);
            return connectivity_1d.isOwned<item_type>();
            break;
          }
          case 2: {
            const auto& connectivity_2d = static_cast<const Connectivity2D&>(connectivity);
            return connectivity_2d.isOwned<item_type>();
            break;
          }
          case 3: {
            const auto& connectivity_3d = static_cast<const Connectivity3D&>(connectivity);
            return connectivity_3d.isOwned<item_type>();
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("unexpected dimension");
          }
            // LCOV_EXCL_STOP
          }
        }(*item_array.connectivity_ptr()))
    {
      ;
    }

    PUGS_INLINE
    ~SubItemArrayPerItemMin() = default;
  };

  const DataType local_min = SubItemArrayPerItemMin{sub_item_array_per_item};
  return parallel::allReduceMin(local_min);
}

template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
std::remove_const_t<DataType>
max(const SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_array_per_item)
{
  using SubItemArrayPerItemType = SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>;
  constexpr ItemType item_type  = ItemOfItem::item_type;
  using ItemIsOwnedType         = ItemValue<const bool, item_type>;
  using data_type               = std::remove_const_t<typename SubItemArrayPerItemType::data_type>;
  using index_type              = typename SubItemArrayPerItemType::index_type;

  static_assert(std::is_arithmetic_v<data_type>, "min cannot be called on non-arithmetic data");
  static_assert(not std::is_same_v<data_type, bool>, "max cannot be called on boolean data");

  class SubItemArrayPerItemMax
  {
   private:
    const SubItemArrayPerItemType& m_sub_item_array_per_item;
    const ItemIsOwnedType m_is_owned;

   public:
    PUGS_INLINE
    operator data_type()
    {
      data_type reduced_value;
      parallel_reduce(m_sub_item_array_per_item.numberOfItems(), *this, reduced_value);
      return reduced_value;
    }

    PUGS_INLINE
    void
    operator()(const index_type& item_id, data_type& data) const
    {
      if (m_is_owned[item_id]) {
        auto sub_item_table = m_sub_item_array_per_item.itemTable(item_id);
        for (size_t i = 0; i < sub_item_table.numberOfRows(); ++i) {
          for (size_t j = 0; j < sub_item_table.numberOfColumns(); ++j) {
            if (sub_item_table(i, j) > data) {
              data = sub_item_table(i, j);
            }
          }
        }
      }
    }

    PUGS_INLINE
    void
    join(data_type& dst, const data_type& src) const
    {
      if (src > dst) {
        // cannot be reached if initial array is the max
        dst = src;   // LCOV_EXCL_LINE
      }
    }

    PUGS_INLINE
    void
    init(data_type& value) const
    {
      value = std::numeric_limits<data_type>::lowest();
    }

    PUGS_INLINE
    SubItemArrayPerItemMax(const SubItemArrayPerItemType& item_array)
      : m_sub_item_array_per_item(item_array), m_is_owned([&](const IConnectivity& connectivity) {
          Assert((connectivity.dimension() > 0) and (connectivity.dimension() <= 3),
                 "unexpected connectivity dimension");

          switch (connectivity.dimension()) {
          case 1: {
            const auto& connectivity_1d = static_cast<const Connectivity1D&>(connectivity);
            return connectivity_1d.isOwned<item_type>();
            break;
          }
          case 2: {
            const auto& connectivity_2d = static_cast<const Connectivity2D&>(connectivity);
            return connectivity_2d.isOwned<item_type>();
            break;
          }
          case 3: {
            const auto& connectivity_3d = static_cast<const Connectivity3D&>(connectivity);
            return connectivity_3d.isOwned<item_type>();
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("unexpected dimension");
          }
            // LCOV_EXCL_STOP
          }
        }(*item_array.connectivity_ptr()))
    {
      ;
    }

    PUGS_INLINE
    ~SubItemArrayPerItemMax() = default;
  };

  const DataType local_max = SubItemArrayPerItemMax{sub_item_array_per_item};
  return parallel::allReduceMax(local_max);
}

template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
std::remove_const_t<DataType>
sum(const SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_array_per_item)
{
  using SubItemArrayPerItemType = SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>;
  constexpr ItemType item_type  = ItemOfItem::item_type;
  using data_type               = std::remove_const_t<typename SubItemArrayPerItemType::data_type>;
  using index_type              = typename SubItemArrayPerItemType::index_type;

  static_assert(not std::is_same_v<data_type, bool>, "sum cannot be called on boolean data");

  ItemValue<data_type, item_type> item_sum{*sub_item_array_per_item.connectivity_ptr()};
  parallel_for(
    sub_item_array_per_item.numberOfItems(), PUGS_LAMBDA(index_type item_id) {
      auto sub_item_table          = sub_item_array_per_item.itemTable(item_id);
      data_type sub_item_table_sum = [] {
        if constexpr (std::is_arithmetic_v<data_type>) {
          return 0;
        } else {
          static_assert(is_tiny_vector_v<data_type> or is_tiny_matrix_v<data_type>, "invalid data type");
          return zero;
        }
      }();

      for (size_t i = 0; i < sub_item_table.numberOfRows(); ++i) {
        for (size_t j = 0; j < sub_item_table.numberOfColumns(); ++j) {
          sub_item_table_sum += sub_item_table(i, j);
        }
      }

      item_sum[item_id] = sub_item_table_sum;
    });

  return sum(item_sum);
}

template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
void
synchronize(SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_array_per_item)
{
  static_assert(not std::is_const_v<DataType>, "cannot synchronize SubItemArrayPerItem of const data");
  if (parallel::size() > 1) {
    auto& manager                     = SynchronizerManager::instance();
    const IConnectivity* connectivity = sub_item_array_per_item.connectivity_ptr().get();
    Synchronizer& synchronizer        = manager.getConnectivitySynchronizer(connectivity);
    synchronizer.synchronize(sub_item_array_per_item);
  }
}

template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
bool
isSynchronized(const SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_array_per_item)
{
  bool is_synchronized = true;
  if (parallel::size() > 1) {
    SubItemArrayPerItem<std::remove_const_t<DataType>, ItemOfItem> sub_item_array_per_item_copy =
      copy(sub_item_array_per_item);

    synchronize(sub_item_array_per_item_copy);

    for (size_t i = 0; i < sub_item_array_per_item.numberOfArrays(); ++i) {
      auto sub_item_array      = sub_item_array_per_item[i];
      auto sub_item_array_copy = sub_item_array_per_item_copy[i];
      for (size_t j = 0; j < sub_item_array.size(); ++j) {
        if (sub_item_array_copy[j] != sub_item_array[j]) {
          is_synchronized = false;
        }
      }
    }

    is_synchronized = parallel::allReduceAnd(is_synchronized);
  }

  return is_synchronized;
}

#endif   // SUB_ITEM_ARRAY_PER_ITEM_UTILS_HPP
