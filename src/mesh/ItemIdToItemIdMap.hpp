#ifndef ITEM_ID_TO_ITEM_ID_MAP_HPP
#define ITEM_ID_TO_ITEM_ID_MAP_HPP

#include <mesh/ItemId.hpp>
#include <utils/Array.hpp>

template <ItemType type1, ItemType type2>
using ItemIdToItemIdMap = Array<std::pair<ItemIdT<type1>, ItemIdT<type2>>>;

template <ItemType type1, ItemType type2>
using ConstItemIdToItemIdMap = Array<const std::pair<ItemIdT<type1>, ItemIdT<type2>>>;

using NodeIdToNodeIdMap = ItemIdToItemIdMap<ItemType::node, ItemType::node>;
using NodeIdToEdgeIdMap = ItemIdToItemIdMap<ItemType::node, ItemType::edge>;
using NodeIdToFaceIdMap = ItemIdToItemIdMap<ItemType::node, ItemType::face>;
using NodeIdToCellIdMap = ItemIdToItemIdMap<ItemType::node, ItemType::cell>;

using EdgeIdToNodeIdMap = ItemIdToItemIdMap<ItemType::edge, ItemType::node>;
using EdgeIdToEdgeIdMap = ItemIdToItemIdMap<ItemType::edge, ItemType::edge>;
using EdgeIdToFaceIdMap = ItemIdToItemIdMap<ItemType::edge, ItemType::face>;
using EdgeIdToCellIdMap = ItemIdToItemIdMap<ItemType::edge, ItemType::cell>;

using FaceIdToNodeIdMap = ItemIdToItemIdMap<ItemType::face, ItemType::node>;
using FaceIdToEdgeIdMap = ItemIdToItemIdMap<ItemType::face, ItemType::edge>;
using FaceIdToFaceIdMap = ItemIdToItemIdMap<ItemType::face, ItemType::face>;
using FaceIdToCellIdMap = ItemIdToItemIdMap<ItemType::face, ItemType::cell>;

using CellIdToNodeIdMap = ItemIdToItemIdMap<ItemType::cell, ItemType::node>;
using CellIdToEdgeIdMap = ItemIdToItemIdMap<ItemType::cell, ItemType::edge>;
using CellIdToFaceIdMap = ItemIdToItemIdMap<ItemType::cell, ItemType::face>;
using CellIdToCellIdMap = ItemIdToItemIdMap<ItemType::cell, ItemType::cell>;

using ConstNodeIdToNodeIdMap = ConstItemIdToItemIdMap<ItemType::node, ItemType::node>;
using ConstNodeIdToEdgeIdMap = ConstItemIdToItemIdMap<ItemType::node, ItemType::edge>;
using ConstNodeIdToFaceIdMap = ConstItemIdToItemIdMap<ItemType::node, ItemType::face>;
using ConstNodeIdToCellIdMap = ConstItemIdToItemIdMap<ItemType::node, ItemType::cell>;

using ConstEdgeIdToNodeIdMap = ConstItemIdToItemIdMap<ItemType::edge, ItemType::node>;
using ConstEdgeIdToEdgeIdMap = ConstItemIdToItemIdMap<ItemType::edge, ItemType::edge>;
using ConstEdgeIdToFaceIdMap = ConstItemIdToItemIdMap<ItemType::edge, ItemType::face>;
using ConstEdgeIdToCellIdMap = ConstItemIdToItemIdMap<ItemType::edge, ItemType::cell>;

using ConstFaceIdToNodeIdMap = ConstItemIdToItemIdMap<ItemType::face, ItemType::node>;
using ConstFaceIdToEdgeIdMap = ConstItemIdToItemIdMap<ItemType::face, ItemType::edge>;
using ConstFaceIdToFaceIdMap = ConstItemIdToItemIdMap<ItemType::face, ItemType::face>;
using ConstFaceIdToCellIdMap = ConstItemIdToItemIdMap<ItemType::face, ItemType::cell>;

using ConstCellIdToNodeIdMap = ConstItemIdToItemIdMap<ItemType::cell, ItemType::node>;
using ConstCellIdToEdgeIdMap = ConstItemIdToItemIdMap<ItemType::cell, ItemType::edge>;
using ConstCellIdToFaceIdMap = ConstItemIdToItemIdMap<ItemType::cell, ItemType::face>;
using ConstCellIdToCellIdMap = ConstItemIdToItemIdMap<ItemType::cell, ItemType::cell>;

#endif   // ITEM_ID_TO_ITEM_ID_MAP_HPP
