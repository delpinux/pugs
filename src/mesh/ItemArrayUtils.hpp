#ifndef ITEM_ARRAY_UTILS_HPP
#define ITEM_ARRAY_UTILS_HPP

#include <utils/Messenger.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ItemArray.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Synchronizer.hpp>
#include <mesh/SynchronizerManager.hpp>

#include <iostream>

template <typename DataType, ItemType item_type, typename ConnectivityPtr>
std::remove_const_t<DataType>
min(const ItemArray<DataType, item_type, ConnectivityPtr>& item_array)
{
  using ItemArrayType   = ItemArray<DataType, item_type, ConnectivityPtr>;
  using ItemIsOwnedType = ItemValue<const bool, item_type>;
  using data_type       = std::remove_const_t<typename ItemArrayType::data_type>;
  using index_type      = typename ItemArrayType::index_type;

  static_assert(std::is_arithmetic_v<data_type>, "min cannot be called on non-arithmetic data");
  static_assert(not std::is_same_v<data_type, bool>, "min cannot be called on boolean data");

  class ItemArrayMin
  {
   private:
    const ItemArrayType& m_item_array;
    const ItemIsOwnedType m_is_owned;

   public:
    PUGS_INLINE
    operator data_type()
    {
      data_type reduced_value;
      parallel_reduce(m_item_array.numberOfItems(), *this, reduced_value);
      return reduced_value;
    }

    PUGS_INLINE
    void
    operator()(const index_type& i_item, data_type& data) const
    {
      if (m_is_owned[i_item]) {
        auto array = m_item_array[i_item];
        for (size_t i = 0; i < m_item_array.sizeOfArrays(); ++i) {
          if (array[i] < data) {
            data = array[i];
          }
        }
      }
    }

    PUGS_INLINE
    void
    join(data_type& dst, const data_type& src) const
    {
      if (src < dst) {
        // cannot be reached if initial value is the min
        dst = src;   // LCOV_EXCL_LINE
      }
    }

    PUGS_INLINE
    void
    init(data_type& value) const
    {
      value = std::numeric_limits<data_type>::max();
    }

    PUGS_INLINE
    ItemArrayMin(const ItemArrayType& item_array)
      : m_item_array(item_array), m_is_owned([&](const IConnectivity& connectivity) {
          Assert((connectivity.dimension() > 0) and (connectivity.dimension() <= 3),
                 "unexpected connectivity dimension");

          switch (connectivity.dimension()) {
          case 1: {
            const auto& connectivity_1d = static_cast<const Connectivity1D&>(connectivity);
            return connectivity_1d.isOwned<item_type>();
            break;
          }
          case 2: {
            const auto& connectivity_2d = static_cast<const Connectivity2D&>(connectivity);
            return connectivity_2d.isOwned<item_type>();
            break;
          }
          case 3: {
            const auto& connectivity_3d = static_cast<const Connectivity3D&>(connectivity);
            return connectivity_3d.isOwned<item_type>();
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("unexpected dimension");
          }
            // LCOV_EXCL_STOP
          }
        }(*item_array.connectivity_ptr()))
    {
      ;
    }

    PUGS_INLINE
    ~ItemArrayMin() = default;
  };

  const DataType local_min = ItemArrayMin{item_array};
  return parallel::allReduceMin(local_min);
}

template <typename DataType, ItemType item_type, typename ConnectivityPtr>
std::remove_const_t<DataType>
max(const ItemArray<DataType, item_type, ConnectivityPtr>& item_array)
{
  using ItemArrayType   = ItemArray<DataType, item_type, ConnectivityPtr>;
  using ItemIsOwnedType = ItemValue<const bool, item_type>;
  using data_type       = std::remove_const_t<typename ItemArrayType::data_type>;
  using index_type      = typename ItemArrayType::index_type;

  static_assert(std::is_arithmetic_v<data_type>, "max cannot be called on non-arithmetic data");
  static_assert(not std::is_same_v<data_type, bool>, "max cannot be called on boolean data");

  class ItemArrayMax
  {
   private:
    const ItemArrayType& m_item_array;
    const ItemIsOwnedType m_is_owned;

   public:
    PUGS_INLINE
    operator data_type()
    {
      data_type reduced_value;
      parallel_reduce(m_item_array.numberOfItems(), *this, reduced_value);
      return reduced_value;
    }

    PUGS_INLINE
    void
    operator()(const index_type& i_item, data_type& data) const
    {
      if (m_is_owned[i_item]) {
        auto array = m_item_array[i_item];
        for (size_t i = 0; i < m_item_array.sizeOfArrays(); ++i) {
          if (array[i] > data) {
            data = array[i];
          }
        }
      }
    }

    PUGS_INLINE
    void
    join(data_type& dst, const data_type& src) const
    {
      if (src > dst) {
        // cannot be reached if initial value is the max
        dst = src;   // LCOV_EXCL_LINE
      }
    }

    PUGS_INLINE
    void
    init(data_type& value) const
    {
      value = std::numeric_limits<data_type>::lowest();
    }

    PUGS_INLINE
    ItemArrayMax(const ItemArrayType& item_array)
      : m_item_array(item_array), m_is_owned([&](const IConnectivity& connectivity) {
          Assert((connectivity.dimension() > 0) and (connectivity.dimension() <= 3),
                 "unexpected connectivity dimension");

          switch (connectivity.dimension()) {
          case 1: {
            const auto& connectivity_1d = static_cast<const Connectivity1D&>(connectivity);
            return connectivity_1d.isOwned<item_type>();
            break;
          }
          case 2: {
            const auto& connectivity_2d = static_cast<const Connectivity2D&>(connectivity);
            return connectivity_2d.isOwned<item_type>();
            break;
          }
          case 3: {
            const auto& connectivity_3d = static_cast<const Connectivity3D&>(connectivity);
            return connectivity_3d.isOwned<item_type>();
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("unexpected dimension");
          }
            // LCOV_EXCL_STOP
          }
        }(*item_array.connectivity_ptr()))
    {
      ;
    }

    PUGS_INLINE
    ~ItemArrayMax() = default;
  };

  const DataType local_max = ItemArrayMax{item_array};
  return parallel::allReduceMax(local_max);
}

template <typename DataType, ItemType item_type, typename ConnectivityPtr>
std::remove_const_t<DataType>
sum(const ItemArray<DataType, item_type, ConnectivityPtr>& item_array)
{
  using ItemArrayType = ItemArray<DataType, item_type, ConnectivityPtr>;
  using data_type     = std::remove_const_t<typename ItemArrayType::data_type>;
  using index_type    = typename ItemArrayType::index_type;

  static_assert(not std::is_same_v<data_type, bool>, "sum cannot be called on boolean data");

  ItemValue<data_type, item_type> item_sum{*item_array.connectivity_ptr()};

  parallel_for(
    item_array.numberOfItems(), PUGS_LAMBDA(index_type item_id) {
      auto row          = item_array[item_id];
      data_type row_sum = row[0];

      for (size_t i = 1; i < row.size(); ++i) {
        row_sum += row[i];
      }
      item_sum[item_id] = row_sum;
    });

  return sum(item_sum);
}

template <typename DataType, ItemType item_type, typename ConnectivityPtr>
void
synchronize(ItemArray<DataType, item_type, ConnectivityPtr> item_array)
{
  static_assert(not std::is_const_v<DataType>, "cannot synchronize ItemArray of const data");
  if (parallel::size() > 1) {
    auto& manager                     = SynchronizerManager::instance();
    const IConnectivity* connectivity = item_array.connectivity_ptr().get();
    Synchronizer& synchronizer        = manager.getConnectivitySynchronizer(connectivity);
    synchronizer.synchronize(item_array);
  }
}

template <typename DataType, ItemType item_type, typename ConnectivityPtr>
bool
isSynchronized(ItemArray<DataType, item_type, ConnectivityPtr> item_array)
{
  bool is_synchronized = true;

  if (parallel::size() > 1) {
    ItemArray<std::remove_const_t<DataType>, item_type> item_array_copy = copy(item_array);
    synchronize(item_array_copy);
    for (ItemIdT<item_type> item_id = 0; item_id < item_array_copy.numberOfItems(); ++item_id) {
      for (size_t i = 0; i < item_array.sizeOfArrays(); ++i) {
        if (item_array_copy[item_id][i] != item_array[item_id][i]) {
          is_synchronized = false;
        }
      }
    }

    is_synchronized = parallel::allReduceAnd(is_synchronized);
  }

  return is_synchronized;
}

#endif   // ITEM_ARRAY_UTILS_HPP
