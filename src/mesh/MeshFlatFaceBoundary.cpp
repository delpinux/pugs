#include <mesh/MeshFlatFaceBoundary.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshFlatNodeBoundary.hpp>

template <typename MeshType>
MeshFlatFaceBoundary<MeshType>
getMeshFlatFaceBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor)
{
  MeshFaceBoundary mesh_face_boundary          = getMeshFaceBoundary(mesh, boundary_descriptor);
  MeshFlatNodeBoundary mesh_flat_node_boundary = getMeshFlatNodeBoundary(mesh, boundary_descriptor);

  return MeshFlatFaceBoundary<MeshType>{mesh, mesh_face_boundary.refFaceList(),
                                        mesh_flat_node_boundary.outgoingNormal()};
}

template MeshFlatFaceBoundary<Mesh<1>> getMeshFlatFaceBoundary(const Mesh<1>&, const IBoundaryDescriptor&);
template MeshFlatFaceBoundary<Mesh<2>> getMeshFlatFaceBoundary(const Mesh<2>&, const IBoundaryDescriptor&);
template MeshFlatFaceBoundary<Mesh<3>> getMeshFlatFaceBoundary(const Mesh<3>&, const IBoundaryDescriptor&);
