#include <mesh/ConnectivityBuilderBase.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityDescriptor.hpp>
#include <mesh/ItemId.hpp>
#include <mesh/Mesh.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>

#include <vector>

template <size_t Dimension>
void
ConnectivityBuilderBase::_computeCellFaceAndFaceNodeConnectivities(ConnectivityDescriptor& descriptor)
{
  static_assert((Dimension == 2) or (Dimension == 3), "Invalid dimension to compute cell-face connectivities");

  const auto& node_number_vector  = descriptor.nodeNumberVector();
  const auto& cell_to_node_matrix = descriptor.cellToNodeMatrix();
  const auto& cell_type_vector    = descriptor.cellTypeVector();

  size_t total_number_of_faces = 0;

  for (CellId j = 0; j < cell_to_node_matrix.numberOfRows(); ++j) {
    const auto& cell_nodes = cell_to_node_matrix[j];

    if constexpr (Dimension == 2) {
      total_number_of_faces += cell_nodes.size();
    } else if constexpr (Dimension == 3) {
      switch (cell_type_vector[j]) {
      case CellType::Hexahedron: {
        total_number_of_faces += 6;
        break;
      }
      case CellType::Tetrahedron: {
        total_number_of_faces += 4;
        break;
      }
      case CellType::Prism: {
        total_number_of_faces += 5;
        break;
      }
      case CellType::Pyramid: {
        total_number_of_faces += cell_nodes.size();
        break;
      }
      case CellType::Diamond: {
        total_number_of_faces += 2 * (cell_nodes.size() - 2);
        break;
      }
      default: {
        std::ostringstream error_msg;
        error_msg << name(cell_type_vector[j]) << ": unexpected cell type in dimension 3";
        throw UnexpectedError(error_msg.str());
      }
      }
    }
  }

  const size_t total_number_of_face_by_node = [&] {
    if constexpr (Dimension == 2) {
      return 2 * total_number_of_faces;
    } else {
      Assert(Dimension == 3);
      size_t count_number_of_face_by_node = 0;
      for (CellId j = 0; j < cell_to_node_matrix.numberOfRows(); ++j) {
        switch (cell_type_vector[j]) {
        case CellType::Hexahedron: {
          count_number_of_face_by_node += 6 * 4;
          break;
        }
        case CellType::Tetrahedron: {
          count_number_of_face_by_node += 4 * 3;
          break;
        }
        case CellType::Prism: {
          count_number_of_face_by_node += 3 * 4 + 2 * 3;
          break;
        }
        case CellType::Pyramid: {
          const auto& cell_nodes = cell_to_node_matrix[j];
          count_number_of_face_by_node += 1 * cell_nodes.size() + cell_nodes.size() * 3;
          break;
        }
        case CellType::Diamond: {
          const auto& cell_nodes = cell_to_node_matrix[j];
          count_number_of_face_by_node += cell_nodes.size() * 3 * 2;
          break;
        }
        default: {
          std::ostringstream error_msg;
          error_msg << name(cell_type_vector[j]) << ": unexpected cell type in dimension 3";
          throw UnexpectedError(error_msg.str());
        }
        }
      }
      return count_number_of_face_by_node;
    }
  }();

  Array<unsigned int> dup_faces_to_node_list(total_number_of_face_by_node);
  Array<unsigned int> dup_face_to_node_row(total_number_of_faces + 1);
  size_t face_couter      = 0;
  dup_face_to_node_row[0] = 0;

  Array<unsigned short> cell_nb_faces(cell_to_node_matrix.numberOfRows());
  {
    size_t i_face_node = 0;
    for (CellId j = 0; j < cell_to_node_matrix.numberOfRows(); ++j) {
      const auto& cell_nodes = cell_to_node_matrix[j];

      if constexpr (Dimension == 2) {
        switch (cell_type_vector[j]) {
        case CellType::Triangle: {
          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 2;
          face_couter++;
          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 2;
          face_couter++;
          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 2;
          face_couter++;
          cell_nb_faces[j] = 3;
          break;
        }
        case CellType::Quadrangle: {
          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[3];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[3];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 2;
          face_couter++;
          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 2;
          face_couter++;
          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 2;
          face_couter++;
          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 2;
          face_couter++;
          cell_nb_faces[j] = 4;
          break;
        }
        case CellType::Polygon: {
          for (size_t i = 0; i < cell_nodes.size(); ++i) {
            dup_faces_to_node_list[i_face_node] = cell_nodes[i];
            i_face_node++;
            dup_faces_to_node_list[i_face_node] = cell_nodes[(i + 1) % cell_nodes.size()];
            i_face_node++;

            dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 2;
            face_couter++;
          }

          cell_nb_faces[j] = cell_nodes.size();
          break;
        }
        default: {
          std::ostringstream error_msg;
          error_msg << name(cell_type_vector[j]) << ": unexpected cell type in dimension 2";
          throw UnexpectedError(error_msg.str());
        }
        }
      } else if constexpr (Dimension == 3) {
        switch (cell_type_vector[j]) {
        case CellType::Hexahedron: {
          dup_faces_to_node_list[i_face_node++] = cell_nodes[3];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 4;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[4];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[5];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[6];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[7];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 4;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[4];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[7];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[3];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 4;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[6];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[5];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 4;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[5];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[4];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 4;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[3];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[7];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[6];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 4;
          face_couter++;

          cell_nb_faces[j] = 6;
          break;
        }
        case CellType::Tetrahedron: {
          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[3];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 3;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[3];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 3;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[3];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 3;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 3;
          face_couter++;

          cell_nb_faces[j] = 4;
          break;
        }
        case CellType::Prism: {
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 3;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[3];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[4];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[5];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 3;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[5];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[4];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 4;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[1];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[4];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[3];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 4;
          face_couter++;

          dup_faces_to_node_list[i_face_node++] = cell_nodes[2];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[0];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[3];
          dup_faces_to_node_list[i_face_node++] = cell_nodes[5];

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 4;
          face_couter++;

          cell_nb_faces[j] = 5;
          break;
        }
        case CellType::Pyramid: {
          cell_nb_faces[j] = cell_nodes.size();
          std::vector<unsigned int> base_nodes(cell_nodes.size() - 1);
          for (size_t i = 0; i < base_nodes.size(); ++i) {
            base_nodes[base_nodes.size() - 1 - i] = cell_nodes[i];
          }

          for (size_t i = 0; i < base_nodes.size(); ++i) {
            dup_faces_to_node_list[i_face_node++] = base_nodes[i];
          }

          dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + base_nodes.size();
          face_couter++;

          // side faces
          const auto pyramid_vertex = cell_nodes[cell_nodes.size() - 1];
          for (size_t i_node = 0; i_node < base_nodes.size(); ++i_node) {
            dup_faces_to_node_list[i_face_node++] = base_nodes[(i_node + 1) % base_nodes.size()];
            dup_faces_to_node_list[i_face_node++] = base_nodes[i_node];
            dup_faces_to_node_list[i_face_node++] = pyramid_vertex;

            dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 3;
            face_couter++;
          }
          break;
        }
        case CellType::Diamond: {
          auto base_nodes = [&](size_t i) { return cell_nodes[i + 1]; };

          {   // top faces
            const auto top_vertex = cell_nodes[cell_nodes.size() - 1];
            for (size_t i_node = 0; i_node < cell_nodes.size() - 2; ++i_node) {
              dup_faces_to_node_list[i_face_node++] = base_nodes(i_node);
              dup_faces_to_node_list[i_face_node++] = base_nodes((i_node + 1) % (cell_nodes.size() - 2));
              dup_faces_to_node_list[i_face_node++] = top_vertex;

              dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 3;
              face_couter++;
            }
          }

          {   // bottom faces
            const auto bottom_vertex = cell_nodes[0];
            for (size_t i_node = 0; i_node < cell_nodes.size() - 2; ++i_node) {
              dup_faces_to_node_list[i_face_node++] = base_nodes((i_node + 1) % (cell_nodes.size() - 2));
              dup_faces_to_node_list[i_face_node++] = base_nodes(i_node);
              dup_faces_to_node_list[i_face_node++] = bottom_vertex;

              dup_face_to_node_row[face_couter + 1] = dup_face_to_node_row[face_couter] + 3;
              face_couter++;
            }
          }
          cell_nb_faces[j] = 2 * (cell_nodes.size() - 2);
          break;
        }
        default: {
          std::ostringstream error_msg;
          error_msg << name(cell_type_vector[j]) << ": unexpected cell type in dimension 3";
          throw UnexpectedError(error_msg.str());
        }
        }
      }
    }
  }

  Array<bool> cell_face_is_reversed(total_number_of_faces);

  if constexpr (Dimension == 2) {
    for (size_t i_face = 0; i_face < total_number_of_faces; ++i_face) {
      if (node_number_vector[dup_faces_to_node_list[2 * i_face]] >
          node_number_vector[dup_faces_to_node_list[2 * i_face + 1]]) {
        std::swap(dup_faces_to_node_list[2 * i_face], dup_faces_to_node_list[2 * i_face + 1]);
        cell_face_is_reversed[i_face] = true;
      } else {
        cell_face_is_reversed[i_face] = false;
      }
    }
  } else if constexpr (Dimension == 3) {
    std::vector<int> buffer;

    for (size_t i_face = 0; i_face < total_number_of_faces; ++i_face) {
      const size_t face_node_number      = dup_face_to_node_row[i_face + 1] - dup_face_to_node_row[i_face];
      size_t i_face_node_smallest_number = 0;
      for (size_t i_face_node = 1; i_face_node < face_node_number; ++i_face_node) {
        if (node_number_vector[dup_faces_to_node_list[dup_face_to_node_row[i_face] + i_face_node]] <
            node_number_vector[dup_faces_to_node_list[dup_face_to_node_row[i_face] + i_face_node_smallest_number]]) {
          i_face_node_smallest_number = i_face_node;
        }
      }

      if (i_face_node_smallest_number != 0) {
        buffer.resize(face_node_number);
        for (size_t i_node = i_face_node_smallest_number; i_node < face_node_number; ++i_node) {
          buffer[i_node - i_face_node_smallest_number] = dup_faces_to_node_list[dup_face_to_node_row[i_face] + i_node];
        }
        for (size_t i_node = 0; i_node < i_face_node_smallest_number; ++i_node) {
          buffer[i_node + face_node_number - i_face_node_smallest_number] =
            dup_faces_to_node_list[dup_face_to_node_row[i_face] + i_node];
        }

        for (size_t i_node = 0; i_node < face_node_number; ++i_node) {
          dup_faces_to_node_list[dup_face_to_node_row[i_face] + i_node] = buffer[i_node];
        }
      }

      if (node_number_vector[dup_faces_to_node_list[dup_face_to_node_row[i_face] + 1]] >
          node_number_vector[dup_faces_to_node_list[dup_face_to_node_row[i_face + 1] - 1]]) {
        for (size_t i_node = 1; i_node <= (face_node_number + 1) / 2 - 1; ++i_node) {
          std::swap(dup_faces_to_node_list[dup_face_to_node_row[i_face] + i_node],
                    dup_faces_to_node_list[dup_face_to_node_row[i_face + 1] - i_node]);
        }

        cell_face_is_reversed[i_face] = true;
      } else {
        cell_face_is_reversed[i_face] = false;
      }
    }
  }

  Array<unsigned int> node_to_duplicate_face_row(node_number_vector.size() + 1);
  node_to_duplicate_face_row.fill(0);
  for (size_t i_face = 0; i_face < total_number_of_faces; ++i_face) {
    for (size_t i_node_face = dup_face_to_node_row[i_face]; i_node_face < dup_face_to_node_row[i_face + 1];
         ++i_node_face) {
      node_to_duplicate_face_row[dup_faces_to_node_list[i_node_face] + 1]++;
    }
  }

  for (size_t i_node = 1; i_node < node_to_duplicate_face_row.size(); ++i_node) {
    node_to_duplicate_face_row[i_node] += node_to_duplicate_face_row[i_node - 1];
  }

  Array<unsigned int> node_duplicate_face_list(node_to_duplicate_face_row[node_to_duplicate_face_row.size() - 1]);

  {
    Array<unsigned int> node_duplicate_face_row_idx(node_number_vector.size());
    node_duplicate_face_row_idx.fill(0);

    for (size_t i_face = 0; i_face < total_number_of_faces; ++i_face) {
      for (size_t i_node_face = dup_face_to_node_row[i_face]; i_node_face < dup_face_to_node_row[i_face + 1];
           ++i_node_face) {
        const size_t node_id = dup_faces_to_node_list[i_node_face];
        node_duplicate_face_list[node_to_duplicate_face_row[node_id] + node_duplicate_face_row_idx[node_id]] = i_face;
        node_duplicate_face_row_idx[node_id]++;
      }
    }
  }

  Array<unsigned int> dup_face_to_face(total_number_of_faces);
  parallel_for(
    total_number_of_faces, PUGS_LAMBDA(size_t i_face) { dup_face_to_face[i_face] = i_face; });

  auto is_same_face = [=](const size_t i_face, const size_t j_face) {
    if ((dup_face_to_node_row[i_face + 1] - dup_face_to_node_row[i_face]) !=
        (dup_face_to_node_row[j_face + 1] - dup_face_to_node_row[j_face])) {
      return false;
    } else {
      auto i_face_node = dup_face_to_node_row[i_face];
      auto j_face_node = dup_face_to_node_row[j_face];
      while (i_face_node < dup_face_to_node_row[i_face + 1]) {
        if (dup_faces_to_node_list[i_face_node] != dup_faces_to_node_list[j_face_node]) {
          return false;
        }
        i_face_node++;
        j_face_node++;
      }
      return true;
    }
  };

  size_t nb_dup_faces = 0;
  for (size_t i_node = 0; i_node < node_number_vector.size(); ++i_node) {
    for (size_t i_node_face = node_to_duplicate_face_row[i_node];
         i_node_face < node_to_duplicate_face_row[i_node + 1] - 1; ++i_node_face) {
      for (size_t j_node_face = i_node_face + 1; j_node_face < node_to_duplicate_face_row[i_node + 1]; ++j_node_face) {
        if (dup_face_to_face[node_duplicate_face_list[i_node_face]] ==
            dup_face_to_face[node_duplicate_face_list[j_node_face]])
          continue;
        if (is_same_face(node_duplicate_face_list[i_node_face], node_duplicate_face_list[j_node_face])) {
          dup_face_to_face[node_duplicate_face_list[j_node_face]] =
            dup_face_to_face[node_duplicate_face_list[i_node_face]];
          nb_dup_faces++;
        }
      }
    }
  }

  // compute face_id
  Array<FaceId> dup_face_to_face_id(total_number_of_faces);
  {
    FaceId face_id = 0;
    for (size_t i_dup_face = 0; i_dup_face < total_number_of_faces; ++i_dup_face) {
      if (dup_face_to_face[i_dup_face] == i_dup_face) {
        dup_face_to_face_id[i_dup_face] = face_id;
        ++face_id;
      } else {
        size_t i_face = dup_face_to_face[i_dup_face];
        while (i_face != dup_face_to_face[i_face]) {
          i_face = dup_face_to_face[i_face];
        }
        dup_face_to_face_id[i_dup_face] = dup_face_to_face_id[i_face];
      }
    }
  }

  Array<unsigned int> node_to_face_row(node_to_duplicate_face_row.size());
  {
    unsigned int nb_faces = 0;
    for (size_t i_node = 0; i_node < node_to_duplicate_face_row.size() - 1; ++i_node) {
      node_to_face_row[i_node] = nb_faces;
      for (size_t i_face = node_to_duplicate_face_row[i_node]; i_face < node_to_duplicate_face_row[i_node + 1];
           ++i_face) {
        if (dup_face_to_face[node_duplicate_face_list[i_face]] == node_duplicate_face_list[i_face]) {
          ++nb_faces;
        }
      }
    }
    node_to_face_row[node_to_duplicate_face_row.size() - 1] = nb_faces;
  }

  Array<unsigned int> node_to_face_list(node_to_face_row[node_to_duplicate_face_row.size() - 1]);
  {
    unsigned int i_node_to_face = 0;
    for (size_t i_node = 0; i_node < node_to_duplicate_face_row.size() - 1; ++i_node) {
      for (size_t i_face = node_to_duplicate_face_row[i_node]; i_face < node_to_duplicate_face_row[i_node + 1];
           ++i_face) {
        if (dup_face_to_face[node_duplicate_face_list[i_face]] == node_duplicate_face_list[i_face]) {
          node_to_face_list[i_node_to_face++] = dup_face_to_face_id[node_duplicate_face_list[i_face]];
        }
      }
    }
  }

  descriptor.setNodeToFaceMatrix(ConnectivityMatrix(node_to_face_row, node_to_face_list));

  Array<unsigned int> cell_to_face_row(cell_nb_faces.size() + 1);
  cell_to_face_row[0] = 0;
  for (size_t i = 0; i < cell_nb_faces.size(); ++i) {
    cell_to_face_row[i + 1] = cell_to_face_row[i] + cell_nb_faces[i];
  }

  Array<unsigned int> cell_to_face_list(cell_to_face_row[cell_to_face_row.size() - 1]);
  {
    size_t i_cell_face = 0;
    for (CellId cell_id = 0; cell_id < cell_nb_faces.size(); ++cell_id) {
      for (size_t i_face = 0; i_face < cell_nb_faces[cell_id]; ++i_face) {
        cell_to_face_list[i_cell_face++] = dup_face_to_face_id[cell_to_face_row[cell_id] + i_face];
      }
    }
  }

  descriptor.setCellToFaceMatrix(ConnectivityMatrix(cell_to_face_row, cell_to_face_list));

  descriptor.setFaceNumberVector([&] {
    Array<int> face_number_vector(total_number_of_faces - nb_dup_faces);
    parallel_for(
      face_number_vector.size(), PUGS_LAMBDA(const size_t l) { face_number_vector[l] = l; });
    return face_number_vector;
  }());

  Array<unsigned int> face_ending(total_number_of_faces - nb_dup_faces + 1);
  {
    size_t i_face  = 0;
    face_ending[0] = 0;
    for (size_t i_dup_face = 0; i_dup_face < dup_face_to_face.size(); ++i_dup_face) {
      if (dup_face_to_face[i_dup_face] == i_dup_face) {
        face_ending[i_face + 1] =
          dup_face_to_node_row[i_dup_face + 1] - dup_face_to_node_row[i_dup_face] + face_ending[i_face];
        ++i_face;
      }
    }
  }

  Array<unsigned int> faces_node_list(face_ending[face_ending.size() - 1]);
  {
    size_t i_face  = 0;
    face_ending[0] = 0;
    for (size_t i_dup_face = 0; i_dup_face < dup_face_to_face.size(); ++i_dup_face) {
      if (dup_face_to_face[i_dup_face] == i_dup_face) {
        size_t dup_face_node_begin = dup_face_to_node_row[i_dup_face];
        size_t face_node_begin     = face_ending[i_face];

        for (size_t i_face_node = 0;
             i_face_node < dup_face_to_node_row[i_dup_face + 1] - dup_face_to_node_row[i_dup_face]; ++i_face_node) {
          faces_node_list[face_node_begin + i_face_node] = dup_faces_to_node_list[dup_face_node_begin + i_face_node];
        }
        ++i_face;
      }
    }
  }

  descriptor.setFaceToNodeMatrix(ConnectivityMatrix(face_ending, faces_node_list));

  descriptor.setCellFaceIsReversed(cell_face_is_reversed);
}

template <size_t Dimension>
void
ConnectivityBuilderBase::_computeFaceEdgeAndEdgeNodeAndCellEdgeConnectivities(ConnectivityDescriptor& descriptor)
{
  static_assert(Dimension == 3, "Invalid dimension to compute face-edge connectivities");

  const auto& face_to_node_matrix = descriptor.faceToNodeMatrix();

  Array<const unsigned int> face_to_edge_row = face_to_node_matrix.rowsMap();

  const size_t total_number_of_face_edges = face_to_edge_row[face_to_edge_row.size() - 1];

  const size_t total_number_of_node_by_face_edges = 2 * total_number_of_face_edges;

  Array<unsigned int> face_edge_to_node_list(total_number_of_node_by_face_edges);
  {
    size_t i_edge_node = 0;
    for (size_t i_face = 0; i_face < face_to_edge_row.size() - 1; ++i_face) {
      const auto& face_node_list = face_to_node_matrix[i_face];
      for (size_t i_node = 0; i_node < face_node_list.size() - 1; ++i_node) {
        face_edge_to_node_list[i_edge_node++] = face_node_list[i_node];
        face_edge_to_node_list[i_edge_node++] = face_node_list[i_node + 1];
      }
      face_edge_to_node_list[i_edge_node++] = face_node_list[face_node_list.size() - 1];
      face_edge_to_node_list[i_edge_node++] = face_node_list[0];
    }
  }

  auto node_number_vector = descriptor.nodeNumberVector();
  Array<bool> face_edge_is_reversed(total_number_of_face_edges);
  for (size_t i_edge = 0; i_edge < total_number_of_face_edges; ++i_edge) {
    if (node_number_vector[face_edge_to_node_list[2 * i_edge]] >
        node_number_vector[face_edge_to_node_list[2 * i_edge + 1]]) {
      std::swap(face_edge_to_node_list[2 * i_edge], face_edge_to_node_list[2 * i_edge + 1]);
      face_edge_is_reversed[i_edge] = true;
    } else {
      face_edge_is_reversed[i_edge] = false;
    }
  }

  const size_t total_number_of_edges = face_edge_is_reversed.size();
  Array<unsigned int> node_to_face_edge_row(node_number_vector.size() + 1);
  node_to_face_edge_row.fill(0);
  for (size_t i_edge = 0; i_edge < total_number_of_edges; ++i_edge) {
    for (size_t i_edge_node = 0; i_edge_node < 2; ++i_edge_node) {
      node_to_face_edge_row[face_edge_to_node_list[2 * i_edge + i_edge_node] + 1]++;
    }
  }
  for (size_t i_node = 1; i_node < node_to_face_edge_row.size(); ++i_node) {
    node_to_face_edge_row[i_node] += node_to_face_edge_row[i_node - 1];
  }

  Array<unsigned int> node_to_face_edge_list(node_to_face_edge_row[node_to_face_edge_row.size() - 1]);

  {
    Array<unsigned int> node_to_face_edge_row_idx(node_number_vector.size());
    node_to_face_edge_row_idx.fill(0);

    for (size_t i_edge = 0; i_edge < total_number_of_edges; ++i_edge) {
      for (size_t i_edge_node = 0; i_edge_node < 2; ++i_edge_node) {
        const size_t node_id = face_edge_to_node_list[2 * i_edge + i_edge_node];

        node_to_face_edge_list[node_to_face_edge_row[node_id] + node_to_face_edge_row_idx[node_id]] = i_edge;
        node_to_face_edge_row_idx[node_id]++;
      }
    }
  }

  Array<unsigned int> face_edge_to_edge(total_number_of_edges);
  parallel_for(
    total_number_of_edges, PUGS_LAMBDA(size_t i_edge) { face_edge_to_edge[i_edge] = i_edge; });

  auto is_same_face = [=](const size_t i_edge, const size_t j_edge) {
    auto i_edge_first_node = 2 * i_edge;
    auto j_edge_first_node = 2 * j_edge;
    return ((face_edge_to_node_list[i_edge_first_node] == face_edge_to_node_list[j_edge_first_node]) and
            (face_edge_to_node_list[i_edge_first_node + 1] == face_edge_to_node_list[j_edge_first_node + 1]));
  };

  size_t nb_duplicate_edges = 0;
  for (size_t i_node = 0; i_node < node_number_vector.size(); ++i_node) {
    for (size_t i_node_face = node_to_face_edge_row[i_node]; i_node_face < node_to_face_edge_row[i_node + 1] - 1;
         ++i_node_face) {
      const unsigned int i_edge    = node_to_face_edge_list[i_node_face];
      const unsigned int i_edge_id = face_edge_to_edge[i_edge];
      for (size_t j_node_face = i_node_face + 1; j_node_face < node_to_face_edge_row[i_node + 1]; ++j_node_face) {
        const unsigned int j_edge = node_to_face_edge_list[j_node_face];
        unsigned int& j_edge_id   = face_edge_to_edge[j_edge];
        if (i_edge_id != j_edge_id) {
          if (is_same_face(i_edge, j_edge)) {
            j_edge_id = i_edge_id;
            nb_duplicate_edges++;
          }
        }
      }
    }
  }

  // compute edge_id
  Array<EdgeId> dup_edge_to_edge_id(total_number_of_edges);
  {
    EdgeId edge_id = 0;
    for (size_t i_dup_edge = 0; i_dup_edge < total_number_of_edges; ++i_dup_edge) {
      if (face_edge_to_edge[i_dup_edge] == i_dup_edge) {
        dup_edge_to_edge_id[i_dup_edge] = edge_id++;
      } else {
        size_t i_edge = i_dup_edge;
        do {
          i_edge = face_edge_to_edge[i_edge];
        } while (i_edge != face_edge_to_edge[i_edge]);

        dup_edge_to_edge_id[i_dup_edge] = dup_edge_to_edge_id[i_edge];
      }
    }
  }

  Array<unsigned int> edge_to_node_list(2 * (total_number_of_edges - nb_duplicate_edges));
  {
    for (size_t i_dup_edge = 0; i_dup_edge < total_number_of_edges; ++i_dup_edge) {
      if (face_edge_to_edge[i_dup_edge] == i_dup_edge) {
        const EdgeId edge_id               = dup_edge_to_edge_id[i_dup_edge];
        edge_to_node_list[2 * edge_id]     = face_edge_to_node_list[2 * i_dup_edge];
        edge_to_node_list[2 * edge_id + 1] = face_edge_to_node_list[2 * i_dup_edge + 1];
      }
    }
  }

  descriptor.setEdgeNumberVector([&] {
    Array<int> edge_number_vector(total_number_of_edges - nb_duplicate_edges);
    parallel_for(
      edge_number_vector.size(), PUGS_LAMBDA(const size_t i_edge) { edge_number_vector[i_edge] = i_edge; });
    return edge_number_vector;
  }());

  Array<unsigned int> edge_to_node_row(total_number_of_edges - nb_duplicate_edges + 1);
  parallel_for(
    edge_to_node_row.size(), PUGS_LAMBDA(const size_t i_edge) { edge_to_node_row[i_edge] = 2 * i_edge; });

  descriptor.setEdgeToNodeMatrix(ConnectivityMatrix(edge_to_node_row, edge_to_node_list));

  // Use real edge ids
  for (size_t i_edge = 0; i_edge < face_edge_to_edge.size(); ++i_edge) {
    face_edge_to_edge[i_edge] = dup_edge_to_edge_id[face_edge_to_edge[i_edge]];
  }

  descriptor.setFaceToEdgeMatrix(ConnectivityMatrix(face_to_edge_row, face_edge_to_edge));
  descriptor.setFaceEdgeIsReversed(face_edge_is_reversed);

  Array<size_t> node_to_duplicated_edge_id_list(node_to_face_edge_list.size());
  for (size_t i_node_edge = 0; i_node_edge < node_to_duplicated_edge_id_list.size(); ++i_node_edge) {
    node_to_duplicated_edge_id_list[i_node_edge] = dup_edge_to_edge_id[node_to_face_edge_list[i_node_edge]];
  }

  Array<bool> node_to_edge_is_duplicated(node_to_face_edge_list.size());
  node_to_edge_is_duplicated.fill(false);

  Array<unsigned int> node_nb_edges(node_number_vector.size());
  node_nb_edges.fill(0);

  for (size_t node_id = 0; node_id < node_number_vector.size(); ++node_id) {
    size_t nb_dup_edges = 0;
    for (EdgeId i_edge = node_to_face_edge_row[node_id]; i_edge < node_to_face_edge_row[node_id + 1] - 1; ++i_edge) {
      if (not node_to_edge_is_duplicated[i_edge]) {
        for (EdgeId j_edge = i_edge + 1; j_edge < node_to_face_edge_row[node_id + 1]; ++j_edge) {
          if (node_to_duplicated_edge_id_list[i_edge] == node_to_duplicated_edge_id_list[j_edge]) {
            node_to_edge_is_duplicated[j_edge] = true;
            nb_dup_edges++;
          }
        }
      }
    }
    node_nb_edges[node_id] = node_to_face_edge_row[node_id + 1] - node_to_face_edge_row[node_id] - nb_dup_edges;
  }

  Array<unsigned int> node_to_edge_row(node_number_vector.size() + 1);
  node_to_edge_row[0] = 0;
  for (size_t node_id = 0; node_id < node_number_vector.size(); ++node_id) {
    node_to_edge_row[node_id + 1] = node_to_edge_row[node_id] + node_nb_edges[node_id];
  }

  Array<unsigned int> node_to_edge_list(node_to_edge_row[node_to_edge_row.size() - 1]);
  {
    size_t l = 0;
    for (size_t i_edge_id = 0; i_edge_id < node_to_duplicated_edge_id_list.size(); ++i_edge_id) {
      if (not node_to_edge_is_duplicated[i_edge_id]) {
        node_to_edge_list[l++] = node_to_duplicated_edge_id_list[i_edge_id];
      }
    }
  }

  descriptor.setNodeToEdgeMatrix(ConnectivityMatrix(node_to_edge_row, node_to_edge_list));

  auto find_edge = [=](const NodeId& node0_id, const NodeId& node1_id) -> EdgeId {
    auto [first_node_id, second_node_id] = [&] {
      if (node_number_vector[node0_id] < node_number_vector[node1_id]) {
        return std::make_pair(node0_id, node1_id);
      } else {
        return std::make_pair(node1_id, node0_id);
      }
    }();

    for (size_t i_node_edge = node_to_edge_row[first_node_id]; i_node_edge < node_to_edge_row[first_node_id + 1];
         ++i_node_edge) {
      EdgeId edge_id = node_to_edge_list[i_node_edge];
      if (edge_to_node_list[2 * edge_id + 1] == second_node_id) {
        return edge_id;
      }
    }
    throw UnexpectedError("Cannot find cell edge in edge list");
  };

  const auto& cell_to_node_matrix = descriptor.cellToNodeMatrix();
  const auto& cell_type_vector    = descriptor.cellTypeVector();
  {
    Array<unsigned int> cell_to_edge_row(cell_to_node_matrix.numberOfRows() + 1);
    {
      cell_to_edge_row[0] = 0;

      for (CellId cell_id = 0; cell_id < cell_to_node_matrix.numberOfRows(); ++cell_id) {
        switch (cell_type_vector[cell_id]) {
        case CellType::Tetrahedron: {
          cell_to_edge_row[cell_id + 1] = cell_to_edge_row[cell_id] + 6;
          break;
        }
        case CellType::Hexahedron: {
          cell_to_edge_row[cell_id + 1] = cell_to_edge_row[cell_id] + 12;
          break;
        }
        case CellType::Prism: {
          cell_to_edge_row[cell_id + 1] = cell_to_edge_row[cell_id] + 9;
          break;
        }
        case CellType::Pyramid: {
          cell_to_edge_row[cell_id + 1] = cell_to_edge_row[cell_id] + 2 * (cell_to_node_matrix[cell_id].size() - 1);
          break;
        }
        case CellType::Diamond: {
          cell_to_edge_row[cell_id + 1] = cell_to_edge_row[cell_id] + 3 * (cell_to_node_matrix[cell_id].size() - 2);
          break;
        }
        default: {
          std::stringstream error_msg;
          error_msg << name(cell_type_vector[cell_id]) << ": unexpected cell type in dimension 3";
          throw NotImplementedError(error_msg.str());
        }
        }
      }
    }

    Array<unsigned int> cell_to_edge_list(cell_to_edge_row[cell_to_edge_row.size() - 1]);
    {
      size_t i_cell_edge = 0;
      for (CellId cell_id = 0; cell_id < cell_to_node_matrix.numberOfRows(); ++cell_id) {
        const auto& cell_nodes = cell_to_node_matrix[cell_id];

        switch (cell_type_vector[cell_id]) {
        case CellType::Tetrahedron: {
          constexpr int local_edge[6][2] = {{0, 1}, {0, 2}, {0, 3}, {1, 2}, {2, 3}, {3, 1}};
          for (int i_edge = 0; i_edge < 6; ++i_edge) {
            const auto& e                    = local_edge[i_edge];
            cell_to_edge_list[i_cell_edge++] = find_edge(cell_nodes[e[0]], cell_nodes[e[1]]);
          }
          break;
        }
        case CellType::Hexahedron: {
          constexpr int local_edge[12][2] = {{0, 1}, {1, 2}, {2, 3}, {3, 0}, {4, 5}, {5, 6},
                                             {6, 7}, {7, 4}, {0, 4}, {1, 5}, {2, 6}, {3, 7}};

          for (int i_edge = 0; i_edge < 12; ++i_edge) {
            const auto& e                    = local_edge[i_edge];
            cell_to_edge_list[i_cell_edge++] = find_edge(cell_nodes[e[0]], cell_nodes[e[1]]);
          }
          break;
        }
        case CellType::Prism: {
          constexpr int local_edge[9][2] = {{0, 1}, {1, 2}, {2, 0}, {3, 4}, {4, 5}, {5, 3}, {0, 3}, {1, 4}, {2, 5}};
          for (int i_edge = 0; i_edge < 9; ++i_edge) {
            const auto& e                    = local_edge[i_edge];
            cell_to_edge_list[i_cell_edge++] = find_edge(cell_nodes[e[0]], cell_nodes[e[1]]);
          }
          break;
        }
        case CellType::Pyramid: {
          auto base_nodes = [&](size_t i) { return cell_nodes[i]; };

          for (size_t i_edge = 0; i_edge < cell_nodes.size() - 1; ++i_edge) {
            cell_to_edge_list[i_cell_edge++] =
              find_edge(base_nodes(i_edge), base_nodes((i_edge + 1) % (cell_nodes.size() - 1)));
          }

          const unsigned int top_vertex = cell_nodes[cell_nodes.size() - 1];
          for (size_t i_edge = 0; i_edge < cell_nodes.size() - 1; ++i_edge) {
            cell_to_edge_list[i_cell_edge++] = find_edge(base_nodes(i_edge), top_vertex);
          }
          break;
        }
        case CellType::Diamond: {
          auto base_nodes = [&](size_t i) { return cell_nodes[i + 1]; };

          for (size_t i_edge = 0; i_edge < cell_nodes.size() - 2; ++i_edge) {
            cell_to_edge_list[i_cell_edge++] =
              find_edge(base_nodes(i_edge), base_nodes((i_edge + 1) % (cell_nodes.size() - 2)));
          }

          {
            const unsigned int top_vertex = cell_nodes[cell_nodes.size() - 1];
            for (size_t i_edge = 0; i_edge < cell_nodes.size() - 2; ++i_edge) {
              cell_to_edge_list[i_cell_edge++] = find_edge(base_nodes(i_edge), top_vertex);
            }
          }

          {
            const unsigned int bottom_vertex = cell_nodes[0];
            for (size_t i_edge = 0; i_edge < cell_nodes.size() - 2; ++i_edge) {
              cell_to_edge_list[i_cell_edge++] = find_edge(base_nodes(i_edge), bottom_vertex);
            }
          }

          break;
        }
        default: {
          std::stringstream error_msg;
          error_msg << name(cell_type_vector[cell_id]) << ": unexpected cell type in dimension 3";
          throw NotImplementedError(error_msg.str());
        }
        }
      }
    }

    descriptor.setCellToEdgeMatrix(ConnectivityMatrix(cell_to_edge_row, cell_to_edge_list));
  }
}

template void ConnectivityBuilderBase::_computeCellFaceAndFaceNodeConnectivities<2>(ConnectivityDescriptor& descriptor);
template void ConnectivityBuilderBase::_computeCellFaceAndFaceNodeConnectivities<3>(ConnectivityDescriptor& descriptor);

template void ConnectivityBuilderBase::_computeFaceEdgeAndEdgeNodeAndCellEdgeConnectivities<3>(
  ConnectivityDescriptor& descriptor);
