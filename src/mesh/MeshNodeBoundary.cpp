#include <mesh/MeshNodeBoundary.hpp>

#include <Kokkos_Vector.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <utils/Messenger.hpp>

template <MeshConcept MeshType>
MeshNodeBoundary::MeshNodeBoundary(const MeshType& mesh, const RefFaceList& ref_face_list)
{
  constexpr size_t Dimension = MeshType::Dimension;

  const Array<const FaceId>& face_list = ref_face_list.list();
  if (ref_face_list.type() != RefItemListBase::Type::boundary) {
    std::ostringstream ost;
    ost << "invalid boundary \"" << rang::fgB::yellow << ref_face_list.refId() << rang::style::reset
        << "\": inner faces cannot be used to define mesh boundaries";
    throw NormalError(ost.str());
  }

  if constexpr (Dimension > 1) {
    Kokkos::vector<unsigned int> node_ids;
    // not enough but should reduce significantly the number of resizing
    node_ids.reserve(Dimension * face_list.size());
    const auto& face_to_node_matrix = mesh.connectivity().faceToNodeMatrix();

    for (size_t l = 0; l < face_list.size(); ++l) {
      const FaceId face_number = face_list[l];
      const auto& face_nodes   = face_to_node_matrix[face_number];

      for (size_t r = 0; r < face_nodes.size(); ++r) {
        node_ids.push_back(face_nodes[r]);
      }
    }
    std::sort(node_ids.begin(), node_ids.end());
    auto last = std::unique(node_ids.begin(), node_ids.end());
    node_ids.resize(std::distance(node_ids.begin(), last));

    Array<NodeId> node_list(node_ids.size());
    parallel_for(
      node_ids.size(), PUGS_LAMBDA(int r) { node_list[r] = node_ids[r]; });
    m_ref_node_list = RefNodeList{ref_face_list.refId(), node_list, ref_face_list.type()};
  } else {
    Array<NodeId> node_list(face_list.size());
    parallel_for(
      face_list.size(), PUGS_LAMBDA(int r) { node_list[r] = static_cast<FaceId::base_type>(face_list[r]); });
    m_ref_node_list = RefNodeList{ref_face_list.refId(), node_list, ref_face_list.type()};
  }

  // This is quite dirty but it allows a non negligible performance
  // improvement
  const_cast<Connectivity<Dimension>&>(mesh.connectivity()).addRefItemList(m_ref_node_list);
}

template <MeshConcept MeshType>
MeshNodeBoundary::MeshNodeBoundary(const MeshType& mesh, const RefEdgeList& ref_edge_list)
{
  constexpr size_t Dimension = MeshType::Dimension;

  const Array<const EdgeId>& edge_list = ref_edge_list.list();
  if (ref_edge_list.type() != RefItemListBase::Type::boundary) {
    std::ostringstream ost;
    ost << "invalid boundary \"" << rang::fgB::yellow << ref_edge_list.refId() << rang::style::reset
        << "\": inner edges cannot be used to define mesh boundaries";
    throw NormalError(ost.str());
  }

  if constexpr (Dimension > 1) {
    const auto& edge_to_node_matrix = mesh.connectivity().edgeToNodeMatrix();
    Kokkos::vector<unsigned int> node_ids;
    node_ids.reserve(2 * edge_list.size());

    for (size_t l = 0; l < edge_list.size(); ++l) {
      const EdgeId edge_number = edge_list[l];
      const auto& edge_nodes   = edge_to_node_matrix[edge_number];

      for (size_t r = 0; r < edge_nodes.size(); ++r) {
        node_ids.push_back(edge_nodes[r]);
      }
    }
    std::sort(node_ids.begin(), node_ids.end());
    auto last = std::unique(node_ids.begin(), node_ids.end());
    node_ids.resize(std::distance(node_ids.begin(), last));

    Array<NodeId> node_list(node_ids.size());
    parallel_for(
      node_ids.size(), PUGS_LAMBDA(int r) { node_list[r] = node_ids[r]; });
    m_ref_node_list = RefNodeList{ref_edge_list.refId(), node_list, ref_edge_list.type()};
  } else {
    Array<NodeId> node_list(edge_list.size());
    parallel_for(
      edge_list.size(), PUGS_LAMBDA(int r) { node_list[r] = static_cast<EdgeId::base_type>(edge_list[r]); });
    m_ref_node_list = RefNodeList{ref_edge_list.refId(), node_list, ref_edge_list.type()};
  }

  // This is quite dirty but it allows a non negligible performance
  // improvement
  const_cast<Connectivity<Dimension>&>(mesh.connectivity()).addRefItemList(m_ref_node_list);
}

template <MeshConcept MeshType>
MeshNodeBoundary::MeshNodeBoundary(const MeshType&, const RefNodeList& ref_node_list) : m_ref_node_list(ref_node_list)
{
  if (ref_node_list.type() != RefItemListBase::Type::boundary) {
    std::ostringstream ost;
    ost << "invalid boundary \"" << rang::fgB::yellow << this->m_ref_node_list.refId() << rang::style::reset
        << "\": inner nodes cannot be used to define mesh boundaries";
    throw NormalError(ost.str());
  }
}

template MeshNodeBoundary::MeshNodeBoundary(const Mesh<1>&, const RefFaceList&);
template MeshNodeBoundary::MeshNodeBoundary(const Mesh<2>&, const RefFaceList&);
template MeshNodeBoundary::MeshNodeBoundary(const Mesh<3>&, const RefFaceList&);

template MeshNodeBoundary::MeshNodeBoundary(const Mesh<1>&, const RefEdgeList&);
template MeshNodeBoundary::MeshNodeBoundary(const Mesh<2>&, const RefEdgeList&);
template MeshNodeBoundary::MeshNodeBoundary(const Mesh<3>&, const RefEdgeList&);

template MeshNodeBoundary::MeshNodeBoundary(const Mesh<1>&, const RefNodeList&);
template MeshNodeBoundary::MeshNodeBoundary(const Mesh<2>&, const RefNodeList&);
template MeshNodeBoundary::MeshNodeBoundary(const Mesh<3>&, const RefNodeList&);

template <MeshConcept MeshType>
MeshNodeBoundary
getMeshNodeBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor)
{
  for (size_t i_ref_node_list = 0; i_ref_node_list < mesh.connectivity().template numberOfRefItemList<ItemType::node>();
       ++i_ref_node_list) {
    const auto& ref_node_list = mesh.connectivity().template refItemList<ItemType::node>(i_ref_node_list);
    const RefId& ref          = ref_node_list.refId();
    if (ref == boundary_descriptor) {
      return MeshNodeBoundary{mesh, ref_node_list};
    }
  }
  for (size_t i_ref_edge_list = 0; i_ref_edge_list < mesh.connectivity().template numberOfRefItemList<ItemType::edge>();
       ++i_ref_edge_list) {
    const auto& ref_edge_list = mesh.connectivity().template refItemList<ItemType::edge>(i_ref_edge_list);
    const RefId& ref          = ref_edge_list.refId();
    if (ref == boundary_descriptor) {
      return MeshNodeBoundary{mesh, ref_edge_list};
    }
  }
  for (size_t i_ref_face_list = 0; i_ref_face_list < mesh.connectivity().template numberOfRefItemList<ItemType::face>();
       ++i_ref_face_list) {
    const auto& ref_face_list = mesh.connectivity().template refItemList<ItemType::face>(i_ref_face_list);
    const RefId& ref          = ref_face_list.refId();
    if (ref == boundary_descriptor) {
      return MeshNodeBoundary{mesh, ref_face_list};
    }
  }

  std::ostringstream ost;
  ost << "cannot find node list with name " << rang::fgB::red << boundary_descriptor << rang::style::reset;

  throw NormalError(ost.str());
}

template MeshNodeBoundary getMeshNodeBoundary(const Mesh<1>&, const IBoundaryDescriptor&);
template MeshNodeBoundary getMeshNodeBoundary(const Mesh<2>&, const IBoundaryDescriptor&);
template MeshNodeBoundary getMeshNodeBoundary(const Mesh<3>&, const IBoundaryDescriptor&);
