#ifndef I_ZONE_DESCRIPTOR_HPP
#define I_ZONE_DESCRIPTOR_HPP

#include <mesh/RefId.hpp>

#include <iostream>

class IZoneDescriptor
{
 public:
  enum class Type
  {
    named,
    numbered
  };

 protected:
  virtual std::ostream& _write(std::ostream& os) const = 0;

 public:
  friend std::ostream&
  operator<<(std::ostream& os, const IZoneDescriptor& zone_descriptor)
  {
    return zone_descriptor._write(os);
  }

  [[nodiscard]] virtual bool operator==(const RefId& ref_id) const = 0;

  [[nodiscard]] friend bool
  operator==(const RefId& ref_id, const IZoneDescriptor& zone_descriptor)
  {
    return zone_descriptor == ref_id;
  }

  [[nodiscard]] virtual Type type() const = 0;

  IZoneDescriptor(const IZoneDescriptor&) = delete;
  IZoneDescriptor(IZoneDescriptor&&)      = delete;
  IZoneDescriptor()                       = default;

  virtual ~IZoneDescriptor() = default;
};

#endif   // I_ZONE_DESCRIPTOR_HPP
