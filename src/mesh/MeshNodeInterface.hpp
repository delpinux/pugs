#ifndef MESH_NODE_INTERFACE_HPP
#define MESH_NODE_INTERFACE_HPP

#include <algebra/TinyVector.hpp>
#include <mesh/IInterfaceDescriptor.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/RefItemList.hpp>
#include <utils/Array.hpp>

class [[nodiscard]] MeshNodeInterface   // clazy:exclude=copyable-polymorphic
{
 protected:
  RefNodeList m_ref_node_list;

 public:
  template <MeshConcept MeshType>
  friend MeshNodeInterface getMeshNodeInterface(const MeshType& mesh, const IInterfaceDescriptor& interface_descriptor);

  MeshNodeInterface& operator=(const MeshNodeInterface&) = default;
  MeshNodeInterface& operator=(MeshNodeInterface&&) = default;

  PUGS_INLINE
  const RefNodeList& refNodeList() const
  {
    return m_ref_node_list;
  }

  PUGS_INLINE
  const Array<const NodeId>& nodeList() const
  {
    return m_ref_node_list.list();
  }

 protected:
  template <MeshConcept MeshType>
  MeshNodeInterface(const MeshType& mesh, const RefFaceList& ref_face_list);
  template <MeshConcept MeshType>
  MeshNodeInterface(const MeshType& mesh, const RefEdgeList& ref_edge_list);
  template <MeshConcept MeshType>
  MeshNodeInterface(const MeshType&, const RefNodeList& ref_node_list);

 public:
  MeshNodeInterface(const MeshNodeInterface&) = default;
  MeshNodeInterface(MeshNodeInterface &&)     = default;

  MeshNodeInterface()          = default;
  virtual ~MeshNodeInterface() = default;
};

template <MeshConcept MeshType>
MeshNodeInterface getMeshNodeInterface(const MeshType& mesh, const IInterfaceDescriptor& interface_descriptor);

#endif   // MESH_NODE_INTERFACE_HPP
