#ifndef I_PRIMAL_TO_DUAL_CONNECTIVITY_DATA_MAPPER_HPP
#define I_PRIMAL_TO_DUAL_CONNECTIVITY_DATA_MAPPER_HPP

class IPrimalToDualConnectivityDataMapper
{
 public:
  IPrimalToDualConnectivityDataMapper(const IPrimalToDualConnectivityDataMapper&) = delete;
  IPrimalToDualConnectivityDataMapper(IPrimalToDualConnectivityDataMapper&&)      = delete;

  IPrimalToDualConnectivityDataMapper()          = default;
  virtual ~IPrimalToDualConnectivityDataMapper() = default;
};

#endif   // I_PRIMAL_TO_DUAL_CONNECTIVITY_DATA_MAPPER_HPP
