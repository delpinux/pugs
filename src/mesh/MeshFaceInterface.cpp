#include <mesh/MeshFaceInterface.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <utils/Messenger.hpp>

template <MeshConcept MeshType>
MeshFaceInterface::MeshFaceInterface(const MeshType&, const RefFaceList& ref_face_list) : m_ref_face_list(ref_face_list)
{}

template MeshFaceInterface::MeshFaceInterface(const Mesh<1>&, const RefFaceList&);
template MeshFaceInterface::MeshFaceInterface(const Mesh<2>&, const RefFaceList&);
template MeshFaceInterface::MeshFaceInterface(const Mesh<3>&, const RefFaceList&);

template <MeshConcept MeshType>
MeshFaceInterface
getMeshFaceInterface(const MeshType& mesh, const IInterfaceDescriptor& interface_descriptor)
{
  for (size_t i_ref_face_list = 0; i_ref_face_list < mesh.connectivity().template numberOfRefItemList<ItemType::face>();
       ++i_ref_face_list) {
    const auto& ref_face_list = mesh.connectivity().template refItemList<ItemType::face>(i_ref_face_list);
    const RefId& ref          = ref_face_list.refId();

    if (ref == interface_descriptor) {
      if (ref_face_list.type() != RefItemListBase::Type::interface) {
        std::ostringstream ost;
        ost << "invalid interface " << rang::fgB::yellow << interface_descriptor << rang::style::reset
            << ": boundary faces cannot be used to define mesh interfaces";
        throw NormalError(ost.str());
      }

      return MeshFaceInterface{mesh, ref_face_list};
    }
  }

  std::ostringstream ost;
  ost << "cannot find face list with name " << rang::fgB::red << interface_descriptor << rang::style::reset;

  throw NormalError(ost.str());
}

template MeshFaceInterface getMeshFaceInterface(const Mesh<1>&, const IInterfaceDescriptor&);
template MeshFaceInterface getMeshFaceInterface(const Mesh<2>&, const IInterfaceDescriptor&);
template MeshFaceInterface getMeshFaceInterface(const Mesh<3>&, const IInterfaceDescriptor&);
