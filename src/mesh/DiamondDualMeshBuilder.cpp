#include <mesh/DiamondDualMeshBuilder.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/DualConnectivityManager.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>
#include <mesh/PrimalToDiamondDualConnectivityDataMapper.hpp>
#include <utils/Stringify.hpp>

template <MeshConcept MeshType>
void
DiamondDualMeshBuilder::_buildDualDiamondMeshFrom(const MeshType& primal_mesh)
{
  constexpr size_t Dimension = MeshType::Dimension;
  using ConnectivityType     = typename MeshType::Connectivity;

  static_assert(Dimension > 1);

  DualConnectivityManager& manager = DualConnectivityManager::instance();

  std::shared_ptr<const ConnectivityType> p_diamond_connectivity =
    manager.getDiamondDualConnectivity(primal_mesh.connectivity());

  const ConnectivityType& diamond_connectivity = *p_diamond_connectivity;

  const NodeValue<const TinyVector<Dimension>> primal_xr = primal_mesh.xr();

  MeshData<MeshType>& primal_mesh_data                   = MeshDataManager::instance().getMeshData(primal_mesh);
  const CellValue<const TinyVector<Dimension>> primal_xj = primal_mesh_data.xj();

  std::shared_ptr primal_to_diamond_dual_connectivity_data_mapper =
    manager.getPrimalToDiamondDualConnectivityDataMapper(primal_mesh.connectivity());

  NodeValue<TinyVector<Dimension>> diamond_xr{diamond_connectivity};
  primal_to_diamond_dual_connectivity_data_mapper->toDualNode(primal_xr, primal_xj, diamond_xr);

  m_mesh = std::make_shared<MeshVariant>(std::make_shared<const MeshType>(p_diamond_connectivity, diamond_xr));
}

DiamondDualMeshBuilder::DiamondDualMeshBuilder(const std::shared_ptr<const MeshVariant>& mesh_v)
{
  std::visit(
    [&](auto&& p_mesh) {
      using MeshType = mesh_type_t<decltype(p_mesh)>;
      if constexpr (is_polygonal_mesh_v<MeshType>) {
        if constexpr (MeshType::Dimension > 1) {
          this->_buildDualDiamondMeshFrom(*p_mesh);
        } else {
          throw UnexpectedError("invalid polygonal mesh dimension");
        }
      } else {
        throw UnexpectedError("invalid mesh type");
      }
    },
    mesh_v->variant());
}
