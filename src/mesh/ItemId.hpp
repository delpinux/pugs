#ifndef ITEM_ID_HPP
#define ITEM_ID_HPP

#include <mesh/ItemType.hpp>

#include <utils/PugsMacros.hpp>

template <ItemType item_type>
class ItemIdT
{
 public:
  using base_type = uint32_t;

 private:
  base_type m_id;

 public:
  PUGS_INLINE
  constexpr operator const base_type&() const
  {
    return m_id;
  }

  PUGS_INLINE
  constexpr ItemIdT
  operator++(int)
  {
    return ItemIdT{m_id++};
  }

  PUGS_INLINE
  constexpr ItemIdT&
  operator++()
  {
    ++m_id;
    return *this;
  }

  PUGS_INLINE
  constexpr ItemIdT&
  operator=(base_type id)
  {
    m_id = id;
    return *this;
  }

  PUGS_INLINE
  constexpr ItemIdT& operator=(const ItemIdT&) = default;

  PUGS_INLINE
  constexpr ItemIdT& operator=(ItemIdT&&) = default;

  PUGS_INLINE
  constexpr ItemIdT(base_type id) : m_id{id} {}

  PUGS_INLINE
  constexpr ItemIdT(const ItemIdT&) = default;

  PUGS_INLINE
  constexpr ItemIdT(ItemIdT&&) = default;

  PUGS_INLINE
  constexpr ItemIdT() = default;

  PUGS_INLINE
  ~ItemIdT() = default;

  // forbidden rules
  template <ItemType other_item_type>
  ItemIdT(const ItemIdT<other_item_type>&)
  {
    static_assert(other_item_type == item_type, "wrong type of item");
  }

  template <ItemType other_item_type>
  ItemIdT&
  operator=(const ItemIdT<other_item_type>&)
  {
    static_assert(other_item_type == item_type, "wrong type of item");
  }

  template <ItemType other_item_type>
  ItemIdT&
  operator=(ItemIdT<other_item_type>&&)
  {
    static_assert(other_item_type == item_type, "wrong type of item");
  }
};

using NodeId = ItemIdT<ItemType::node>;
using EdgeId = ItemIdT<ItemType::edge>;
using FaceId = ItemIdT<ItemType::face>;
using CellId = ItemIdT<ItemType::cell>;

#endif   // ITEM_ID_HPP
