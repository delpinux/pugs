#ifndef ITEM_TO_ITEM_MATRIX_HPP
#define ITEM_TO_ITEM_MATRIX_HPP

#include <mesh/ConnectivityMatrix.hpp>
#include <mesh/ItemId.hpp>
#include <mesh/ItemType.hpp>

#include <utils/Exceptions.hpp>
#include <utils/PugsUtils.hpp>

template <ItemType SourceItemType, ItemType TargetItemType>
class ItemToItemMatrix
{
 public:
  using SourceItemId = ItemIdT<SourceItemType>;
  using TargetItemId = ItemIdT<TargetItemType>;

  class UnsafeSubItemArray
  {
   private:
    using IndexType = typename ConnectivityMatrix::IndexType;

    const IndexType* const m_values;
    const size_t m_size;

   public:
    PUGS_INLINE
    size_t
    size() const
    {
      return m_size;
    }

    PUGS_INLINE TargetItemId
    operator[](size_t j) const
    {
      Assert(j < m_size);
      return m_values[j];
    }

    PUGS_INLINE
    UnsafeSubItemArray(const ConnectivityMatrix& connectivity_matrix, SourceItemId source_item_id)
      : m_values{&(connectivity_matrix.values()[connectivity_matrix.rowsMap()[source_item_id]])},
        m_size(connectivity_matrix.rowsMap()[source_item_id + 1] - connectivity_matrix.rowsMap()[source_item_id])
    {}

    PUGS_INLINE
    UnsafeSubItemArray& operator=(const UnsafeSubItemArray&) = delete;

    PUGS_INLINE
    UnsafeSubItemArray(const UnsafeSubItemArray&) = delete;

    PUGS_INLINE
    ~UnsafeSubItemArray() = default;
  };

 private:
  const ConnectivityMatrix& m_connectivity_matrix;

 public:
  PUGS_INLINE
  size_t
  numberOfValues() const
  {
    return m_connectivity_matrix.numberOfValues();
  }

  PUGS_INLINE
  auto
  values() const
  {
    return m_connectivity_matrix.values();
  }

  PUGS_INLINE
  auto
  operator[](const SourceItemId& source_id) const
  {
    Assert(source_id < m_connectivity_matrix.numberOfRows());
    return UnsafeSubItemArray(m_connectivity_matrix, source_id);
  }

  template <typename IndexType>
  PUGS_INLINE void
  operator[](const IndexType&) const
  {
    static_assert(std::is_same_v<IndexType, SourceItemId>, "ItemToItemMatrix must be indexed using correct ItemId");
  }

  PUGS_INLINE
  ItemToItemMatrix(const ConnectivityMatrix& connectivity_matrix) : m_connectivity_matrix{connectivity_matrix} {}

  PUGS_INLINE
  ItemToItemMatrix& operator=(const ItemToItemMatrix&) = default;

  PUGS_INLINE
  ItemToItemMatrix& operator=(ItemToItemMatrix&&) = default;

  PUGS_INLINE
  ItemToItemMatrix(ItemToItemMatrix&&) = default;

  PUGS_INLINE
  ItemToItemMatrix(const ItemToItemMatrix&) = default;

  PUGS_INLINE
  ~ItemToItemMatrix() = default;
};

#endif   // ITEM_TO_ITEM_MATRIX_HPP
