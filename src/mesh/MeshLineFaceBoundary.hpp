#ifndef MESH_LINE_FACE_BOUNDARY_HPP
#define MESH_LINE_FACE_BOUNDARY_HPP

#include <algebra/TinyMatrix.hpp>
#include <mesh/MeshFaceBoundary.hpp>

template <MeshConcept MeshType>
class [[nodiscard]] MeshLineFaceBoundary final : public MeshFaceBoundary   // clazy:exclude=copyable-polymorphic
{
 public:
  static_assert(not std::is_const_v<MeshType>, "MeshType must be non-const");
  static_assert(MeshType::Dimension == 2, "MeshLineFaceBoundary makes only sense in dimension 2");

  using Rd = TinyVector<MeshType::Dimension, double>;

 private:
  const Rd m_direction;

 public:
  template <typename MeshTypeT>
  friend MeshLineFaceBoundary<MeshTypeT> getMeshLineFaceBoundary(const MeshTypeT& mesh,
                                                                 const IBoundaryDescriptor& boundary_descriptor);

  PUGS_INLINE
  const Rd&
  direction() const
  {
    return m_direction;
  }

  MeshLineFaceBoundary& operator=(const MeshLineFaceBoundary&) = default;
  MeshLineFaceBoundary& operator=(MeshLineFaceBoundary&&)      = default;

 private:
  MeshLineFaceBoundary(const MeshType& mesh, const RefFaceList& ref_face_list, const Rd& direction)
    : MeshFaceBoundary(mesh, ref_face_list), m_direction(direction)
  {}

 public:
  MeshLineFaceBoundary()                            = default;
  MeshLineFaceBoundary(const MeshLineFaceBoundary&) = default;
  MeshLineFaceBoundary(MeshLineFaceBoundary&&)      = default;
  ~MeshLineFaceBoundary()                           = default;
};

template <typename MeshType>
MeshLineFaceBoundary<MeshType> getMeshLineFaceBoundary(const MeshType& mesh,
                                                       const IBoundaryDescriptor& boundary_descriptor);

#endif   // MESH_LINE_FACE_BOUNDARY_HPP
