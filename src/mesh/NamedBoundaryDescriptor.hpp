#ifndef NAMED_BOUNDARY_DESCRIPTOR_HPP
#define NAMED_BOUNDARY_DESCRIPTOR_HPP

#include <mesh/IBoundaryDescriptor.hpp>

#include <iostream>
#include <string>

class NamedBoundaryDescriptor final : public IBoundaryDescriptor
{
 private:
  std::string m_name;

  std::ostream&
  _write(std::ostream& os) const final
  {
    os << '"' << m_name << '"';
    return os;
  }

 public:
  [[nodiscard]] const std::string&
  name() const
  {
    return m_name;
  }

  [[nodiscard]] bool
  operator==(const RefId& ref_id) const final
  {
    return m_name == ref_id.tagName();
  }

  [[nodiscard]] Type
  type() const final
  {
    return Type::named;
  }

  NamedBoundaryDescriptor(const NamedBoundaryDescriptor&) = delete;
  NamedBoundaryDescriptor(NamedBoundaryDescriptor&&)      = delete;
  NamedBoundaryDescriptor(const std::string& name) : m_name(name) {}
  virtual ~NamedBoundaryDescriptor() = default;
};

#endif   // NAMED_BOUNDARY_DESCRIPTOR_HPP
