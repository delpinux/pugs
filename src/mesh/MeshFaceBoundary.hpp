#ifndef MESH_FACE_BOUNDARY_HPP
#define MESH_FACE_BOUNDARY_HPP

#include <algebra/TinyVector.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/RefItemList.hpp>
#include <utils/Array.hpp>

class [[nodiscard]] MeshFaceBoundary   // clazy:exclude=copyable-polymorphic
{
 protected:
  RefFaceList m_ref_face_list;

 public:
  template <MeshConcept MeshTypeT>
  friend MeshFaceBoundary getMeshFaceBoundary(const MeshTypeT& mesh, const IBoundaryDescriptor& boundary_descriptor);

  MeshFaceBoundary& operator=(const MeshFaceBoundary&) = default;
  MeshFaceBoundary& operator=(MeshFaceBoundary&&) = default;

  PUGS_INLINE
  const RefFaceList& refFaceList() const
  {
    return m_ref_face_list;
  }

  PUGS_INLINE
  const Array<const FaceId>& faceList() const
  {
    return m_ref_face_list.list();
  }

 protected:
  template <MeshConcept MeshType>
  MeshFaceBoundary(const MeshType& mesh, const RefFaceList& ref_face_list);

 public:
  MeshFaceBoundary(const MeshFaceBoundary&) = default;   // LCOV_EXCL_LINE
  MeshFaceBoundary(MeshFaceBoundary &&)     = default;   // LCOV_EXCL_LINE

  MeshFaceBoundary()          = default;
  virtual ~MeshFaceBoundary() = default;
};

template <MeshConcept MeshType>
MeshFaceBoundary getMeshFaceBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor);

#endif   // MESH_FACE_BOUNDARY_HPP
