#ifndef SUB_ITEM_VALUE_PER_ITEM_UTILS_HPP
#define SUB_ITEM_VALUE_PER_ITEM_UTILS_HPP

#include <utils/Messenger.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/SubItemValuePerItem.hpp>
#include <mesh/Synchronizer.hpp>
#include <mesh/SynchronizerManager.hpp>
#include <utils/PugsTraits.hpp>

template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
std::remove_const_t<DataType>
min(const SubItemValuePerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_value_per_item)
{
  using SubItemValuePerItemType = SubItemValuePerItem<DataType, ItemOfItem, ConnectivityPtr>;
  constexpr ItemType item_type  = ItemOfItem::item_type;
  using ItemIsOwnedType         = ItemValue<const bool, item_type>;
  using data_type               = std::remove_const_t<typename SubItemValuePerItemType::data_type>;
  using index_type              = typename SubItemValuePerItemType::index_type;

  static_assert(std::is_arithmetic_v<data_type>, "min cannot be called on non-arithmetic data");
  static_assert(not std::is_same_v<data_type, bool>, "min cannot be called on boolean data");

  class SubItemValuePerItemMin
  {
   private:
    const SubItemValuePerItemType& m_sub_item_value_per_item;
    const ItemIsOwnedType m_is_owned;

   public:
    PUGS_INLINE
    operator data_type()
    {
      data_type reduced_value;
      parallel_reduce(m_sub_item_value_per_item.numberOfItems(), *this, reduced_value);
      return reduced_value;
    }

    PUGS_INLINE
    void
    operator()(const index_type& item_id, data_type& data) const
    {
      if (m_is_owned[item_id]) {
        auto sub_item_array = m_sub_item_value_per_item.itemArray(item_id);
        for (size_t i = 0; i < sub_item_array.size(); ++i) {
          if (sub_item_array[i] < data) {
            data = sub_item_array[i];
          }
        }
      }
    }

    PUGS_INLINE
    void
    join(data_type& dst, const data_type& src) const
    {
      if (src < dst) {
        // cannot be reached if initial value is the min
        dst = src;   // LCOV_EXCL_LINE
      }
    }

    PUGS_INLINE
    void
    init(data_type& value) const
    {
      value = std::numeric_limits<data_type>::max();
    }

    PUGS_INLINE
    SubItemValuePerItemMin(const SubItemValuePerItemType& item_value)
      : m_sub_item_value_per_item(item_value), m_is_owned([&](const IConnectivity& connectivity) {
          Assert((connectivity.dimension() > 0) and (connectivity.dimension() <= 3),
                 "unexpected connectivity dimension");

          switch (connectivity.dimension()) {
          case 1: {
            const auto& connectivity_1d = static_cast<const Connectivity1D&>(connectivity);
            return connectivity_1d.isOwned<item_type>();
            break;
          }
          case 2: {
            const auto& connectivity_2d = static_cast<const Connectivity2D&>(connectivity);
            return connectivity_2d.isOwned<item_type>();
            break;
          }
          case 3: {
            const auto& connectivity_3d = static_cast<const Connectivity3D&>(connectivity);
            return connectivity_3d.isOwned<item_type>();
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("unexpected dimension");
          }
            // LCOV_EXCL_STOP
          }
        }(*item_value.connectivity_ptr()))
    {
      ;
    }

    PUGS_INLINE
    ~SubItemValuePerItemMin() = default;
  };

  const DataType local_min = SubItemValuePerItemMin{sub_item_value_per_item};
  return parallel::allReduceMin(local_min);
}

template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
std::remove_const_t<DataType>
max(const SubItemValuePerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_value_per_item)
{
  using SubItemValuePerItemType = SubItemValuePerItem<DataType, ItemOfItem, ConnectivityPtr>;
  constexpr ItemType item_type  = ItemOfItem::item_type;
  using ItemIsOwnedType         = ItemValue<const bool, item_type>;
  using data_type               = std::remove_const_t<typename SubItemValuePerItemType::data_type>;
  using index_type              = typename SubItemValuePerItemType::index_type;

  static_assert(std::is_arithmetic_v<data_type>, "min cannot be called on non-arithmetic data");
  static_assert(not std::is_same_v<data_type, bool>, "max cannot be called on boolean data");

  class SubItemValuePerItemMax
  {
   private:
    const SubItemValuePerItemType& m_sub_item_value_per_item;
    const ItemIsOwnedType m_is_owned;

   public:
    PUGS_INLINE
    operator data_type()
    {
      data_type reduced_value;
      parallel_reduce(m_sub_item_value_per_item.numberOfItems(), *this, reduced_value);
      return reduced_value;
    }

    PUGS_INLINE
    void
    operator()(const index_type& item_id, data_type& data) const
    {
      if (m_is_owned[item_id]) {
        auto sub_item_array = m_sub_item_value_per_item.itemArray(item_id);
        for (size_t i = 0; i < sub_item_array.size(); ++i) {
          if (sub_item_array[i] > data) {
            data = sub_item_array[i];
          }
        }
      }
    }

    PUGS_INLINE
    void
    join(data_type& dst, const data_type& src) const
    {
      if (src > dst) {
        // cannot be reached if initial value is the max
        dst = src;   // LCOV_EXCL_LINE
      }
    }

    PUGS_INLINE
    void
    init(data_type& value) const
    {
      value = std::numeric_limits<data_type>::lowest();
    }

    PUGS_INLINE
    SubItemValuePerItemMax(const SubItemValuePerItemType& item_value)
      : m_sub_item_value_per_item(item_value), m_is_owned([&](const IConnectivity& connectivity) {
          Assert((connectivity.dimension() > 0) and (connectivity.dimension() <= 3),
                 "unexpected connectivity dimension");

          switch (connectivity.dimension()) {
          case 1: {
            const auto& connectivity_1d = static_cast<const Connectivity1D&>(connectivity);
            return connectivity_1d.isOwned<item_type>();
            break;
          }
          case 2: {
            const auto& connectivity_2d = static_cast<const Connectivity2D&>(connectivity);
            return connectivity_2d.isOwned<item_type>();
            break;
          }
          case 3: {
            const auto& connectivity_3d = static_cast<const Connectivity3D&>(connectivity);
            return connectivity_3d.isOwned<item_type>();
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("unexpected dimension");
          }
            // LCOV_EXCL_STOP
          }
        }(*item_value.connectivity_ptr()))
    {
      ;
    }

    PUGS_INLINE
    ~SubItemValuePerItemMax() = default;
  };

  const DataType local_max = SubItemValuePerItemMax{sub_item_value_per_item};
  return parallel::allReduceMax(local_max);
}

template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
std::remove_const_t<DataType>
sum(const SubItemValuePerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_value_per_item)
{
  using SubItemValuePerItemType = SubItemValuePerItem<DataType, ItemOfItem, ConnectivityPtr>;
  constexpr ItemType item_type  = ItemOfItem::item_type;
  using data_type               = std::remove_const_t<typename SubItemValuePerItemType::data_type>;
  using index_type              = typename SubItemValuePerItemType::index_type;

  static_assert(not std::is_same_v<data_type, bool>, "sum cannot be called on boolean data");

  ItemValue<data_type, item_type> item_sum{*sub_item_value_per_item.connectivity_ptr()};
  parallel_for(
    sub_item_value_per_item.numberOfItems(), PUGS_LAMBDA(index_type item_id) {
      auto sub_item_array          = sub_item_value_per_item.itemArray(item_id);
      data_type sub_item_array_sum = sub_item_array[0];
      for (size_t i = 1; i < sub_item_array.size(); ++i) {
        sub_item_array_sum += sub_item_array[i];
      }

      item_sum[item_id] = sub_item_array_sum;
    });

  return sum(item_sum);
}

template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
void
synchronize(SubItemValuePerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_value_per_item)
{
  static_assert(not std::is_const_v<DataType>, "cannot synchronize SubItemValuePerItem of const data");
  if (parallel::size() > 1) {
    auto& manager                     = SynchronizerManager::instance();
    const IConnectivity* connectivity = sub_item_value_per_item.connectivity_ptr().get();
    Synchronizer& synchronizer        = manager.getConnectivitySynchronizer(connectivity);
    synchronizer.synchronize(sub_item_value_per_item);
  }
}

template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
bool
isSynchronized(const SubItemValuePerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_value_per_item)
{
  bool is_synchronized = true;
  if (parallel::size() > 1) {
    SubItemValuePerItem<std::remove_const_t<DataType>, ItemOfItem> sub_item_value_per_item_copy =
      copy(sub_item_value_per_item);

    synchronize(sub_item_value_per_item_copy);

    for (size_t i = 0; i < sub_item_value_per_item.numberOfValues(); ++i) {
      if (sub_item_value_per_item_copy[i] != sub_item_value_per_item[i]) {
        is_synchronized = false;
        break;
      }
    }

    is_synchronized = parallel::allReduceAnd(is_synchronized);
  }

  return is_synchronized;
}

#endif   // SUB_ITEM_VALUE_PER_ITEM_UTILS_HPP
