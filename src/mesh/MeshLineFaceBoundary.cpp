#include <mesh/MeshLineFaceBoundary.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshLineNodeBoundary.hpp>
#include <utils/Messenger.hpp>

template <typename MeshType>
MeshLineFaceBoundary<MeshType>
getMeshLineFaceBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor)
{
  MeshFaceBoundary mesh_face_boundary          = getMeshFaceBoundary(mesh, boundary_descriptor);
  MeshLineNodeBoundary mesh_line_node_boundary = getMeshLineNodeBoundary(mesh, boundary_descriptor);

  return MeshLineFaceBoundary<MeshType>{mesh, mesh_face_boundary.refFaceList(), mesh_line_node_boundary.direction()};
}

template MeshLineFaceBoundary<Mesh<2>> getMeshLineFaceBoundary(const Mesh<2>&, const IBoundaryDescriptor&);
