#ifndef DIAMOND_DUAL_CONNECTIVITY_BUILDER_HPP
#define DIAMOND_DUAL_CONNECTIVITY_BUILDER_HPP

#include <mesh/ConnectivityBuilderBase.hpp>
#include <mesh/IPrimalToDualConnectivityDataMapper.hpp>
#include <mesh/ItemIdToItemIdMap.hpp>

#include <memory>

template <size_t>
class Connectivity;
class ConnectivityDescriptor;

class DiamondDualConnectivityBuilder : public ConnectivityBuilderBase
{
 private:
  NodeIdToNodeIdMap m_primal_node_to_dual_node_map;
  CellIdToNodeIdMap m_primal_cell_to_dual_node_map;
  FaceIdToCellIdMap m_primal_face_to_dual_cell_map;

  std::shared_ptr<IPrimalToDualConnectivityDataMapper> m_mapper;

  template <size_t Dimension>
  void _buildDiamondConnectivityDescriptor(const Connectivity<Dimension>&, ConnectivityDescriptor&);

  template <size_t Dimension>
  void _buildDiamondConnectivityFrom(const IConnectivity&);

  friend class DualConnectivityManager;
  DiamondDualConnectivityBuilder(const IConnectivity&);

 public:
  std::shared_ptr<IPrimalToDualConnectivityDataMapper>
  mapper() const
  {
    return m_mapper;
  }

  ~DiamondDualConnectivityBuilder() = default;
};

#endif   // DIAMOND_DUAL_CONNECTIVITY_BUILDER_HPP
