#ifndef NUMBERED_INTERFACE_DESCRIPTOR_HPP
#define NUMBERED_INTERFACE_DESCRIPTOR_HPP

#include <mesh/IInterfaceDescriptor.hpp>

#include <iostream>

class NumberedInterfaceDescriptor final : public IInterfaceDescriptor
{
 private:
  unsigned int m_number;

  std::ostream&
  _write(std::ostream& os) const final
  {
    os << '"' << m_number << '"';
    return os;
  }

  [[nodiscard]] bool
  operator==(const RefId& ref_id) const final
  {
    return m_number == ref_id.tagNumber();
  }

 public:
  [[nodiscard]] unsigned int
  number() const
  {
    return m_number;
  }

  [[nodiscard]] Type
  type() const final
  {
    return Type::numbered;
  }

  NumberedInterfaceDescriptor(const NumberedInterfaceDescriptor&) = delete;
  NumberedInterfaceDescriptor(NumberedInterfaceDescriptor&&)      = delete;
  NumberedInterfaceDescriptor(unsigned int number) : m_number(number)
  {
    ;
  }
  virtual ~NumberedInterfaceDescriptor() = default;
};

#endif   // NUMBERED_INTERFACE_DESCRIPTOR_HPP
