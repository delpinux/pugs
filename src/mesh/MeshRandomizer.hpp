#ifndef MESH_RANDOMIZER_HPP
#define MESH_RANDOMIZER_HPP

#include <mesh/MeshTraits.hpp>

#include <memory>
#include <vector>

class MeshVariant;
class IBoundaryConditionDescriptor;
class FunctionSymbolId;

class MeshRandomizerHandler
{
 private:
  template <MeshConcept MeshType>
  class MeshRandomizer;

 public:
  std::shared_ptr<const MeshVariant> getRandomizedMesh(
    const std::shared_ptr<const MeshVariant>& mesh_v,
    const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>& bc_descriptor_list) const;

  std::shared_ptr<const MeshVariant> getRandomizedMesh(
    const std::shared_ptr<const MeshVariant>& mesh_v,
    const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>& bc_descriptor_list,
    const FunctionSymbolId& function_symbol_id) const;

  MeshRandomizerHandler()                        = default;
  MeshRandomizerHandler(MeshRandomizerHandler&&) = default;
  ~MeshRandomizerHandler()                       = default;
};

#endif   // MESH_RANDOMIZER_HPP
