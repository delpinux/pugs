#include <mesh/MeshCellZone.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <utils/Messenger.hpp>

template <size_t Dimension>
MeshCellZone<Dimension>::MeshCellZone(const Mesh<Dimension>&, const RefCellList& ref_cell_list)
  : m_cell_list(ref_cell_list.list()), m_zone_name(ref_cell_list.refId().tagName())
{}

template <size_t Dimension>
MeshCellZone<Dimension>
getMeshCellZone(const Mesh<Dimension>& mesh, const IZoneDescriptor& zone_descriptor)
{
  for (size_t i_ref_cell_list = 0; i_ref_cell_list < mesh.connectivity().template numberOfRefItemList<ItemType::cell>();
       ++i_ref_cell_list) {
    const auto& ref_cell_list = mesh.connectivity().template refItemList<ItemType::cell>(i_ref_cell_list);
    const RefId& ref          = ref_cell_list.refId();
    if (ref == zone_descriptor) {
      return MeshCellZone<Dimension>{mesh, ref_cell_list};
    }
  }

  std::ostringstream ost;
  ost << "cannot find cell set with name \"" << rang::fgB::red << zone_descriptor << rang::style::reset << '\"';

  throw NormalError(ost.str());
}

template MeshCellZone<1> getMeshCellZone(const Mesh<1>&, const IZoneDescriptor&);
template MeshCellZone<2> getMeshCellZone(const Mesh<2>&, const IZoneDescriptor&);
template MeshCellZone<3> getMeshCellZone(const Mesh<3>&, const IZoneDescriptor&);
