#include <mesh/MeshLineEdgeBoundary.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshLineNodeBoundary.hpp>
#include <utils/Messenger.hpp>

template <typename MeshType>
MeshLineEdgeBoundary<MeshType>
getMeshLineEdgeBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor)
{
  MeshEdgeBoundary mesh_edge_boundary          = getMeshEdgeBoundary(mesh, boundary_descriptor);
  MeshLineNodeBoundary mesh_line_node_boundary = getMeshLineNodeBoundary(mesh, boundary_descriptor);

  return MeshLineEdgeBoundary<MeshType>{mesh, mesh_edge_boundary.refEdgeList(), mesh_line_node_boundary.direction()};
}

template MeshLineEdgeBoundary<Mesh<2>> getMeshLineEdgeBoundary(const Mesh<2>&, const IBoundaryDescriptor&);
template MeshLineEdgeBoundary<Mesh<3>> getMeshLineEdgeBoundary(const Mesh<3>&, const IBoundaryDescriptor&);
