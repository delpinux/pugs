#ifndef MESH_NODE_BOUNDARY_HPP
#define MESH_NODE_BOUNDARY_HPP

#include <algebra/TinyVector.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/RefItemList.hpp>
#include <utils/Array.hpp>

class [[nodiscard]] MeshNodeBoundary   // clazy:exclude=copyable-polymorphic
{
 protected:
  RefNodeList m_ref_node_list;

 public:
  template <MeshConcept MeshTypeT>
  friend MeshNodeBoundary getMeshNodeBoundary(const MeshTypeT& mesh, const IBoundaryDescriptor& boundary_descriptor);

  MeshNodeBoundary& operator=(const MeshNodeBoundary&) = default;
  MeshNodeBoundary& operator=(MeshNodeBoundary&&) = default;

  PUGS_INLINE
  const RefNodeList& refNodeList() const
  {
    return m_ref_node_list;
  }

  PUGS_INLINE
  const Array<const NodeId>& nodeList() const
  {
    return m_ref_node_list.list();
  }

 protected:
  template <MeshConcept MeshType>
  MeshNodeBoundary(const MeshType& mesh, const RefFaceList& ref_face_list);
  template <MeshConcept MeshType>
  MeshNodeBoundary(const MeshType& mesh, const RefEdgeList& ref_edge_list);
  template <MeshConcept MeshType>
  MeshNodeBoundary(const MeshType&, const RefNodeList& ref_node_list);

 public:
  MeshNodeBoundary(const MeshNodeBoundary&) = default;
  MeshNodeBoundary(MeshNodeBoundary &&)     = default;

  MeshNodeBoundary()          = default;
  virtual ~MeshNodeBoundary() = default;
};

template <MeshConcept MeshType>
MeshNodeBoundary getMeshNodeBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor);

#endif   // MESH_NODE_BOUNDARY_HPP
