#ifndef CONNECTIVITY_MATRIX_HPP
#define CONNECTIVITY_MATRIX_HPP

#include <utils/Array.hpp>
#include <utils/PugsUtils.hpp>

class ConnectivityMatrix
{
 public:
  using IndexType = uint32_t;

 private:
  Array<const IndexType> m_row_map;
  Array<const IndexType> m_column_indices;

  bool m_is_built{false};

 public:
  const bool&
  isBuilt() const
  {
    return m_is_built;
  }

  const Array<const IndexType>&
  values() const
  {
    return m_column_indices;
  }

  const auto&
  rowsMap() const
  {
    return m_row_map;
  }

  PUGS_INLINE
  size_t
  numberOfValues() const
  {
    return m_column_indices.size();
  }

  PUGS_INLINE
  size_t
  numberOfRows() const
  {
    return m_row_map.size() - 1;
  }

  PUGS_INLINE
  auto
  operator[](size_t j) const
  {
    return subArrayView(m_column_indices, m_row_map[j], m_row_map[j + 1] - m_row_map[j]);
  }

  PUGS_INLINE
  ConnectivityMatrix(const Array<const uint32_t>& row_map,
                     const Array<const uint32_t>& column_indices) noexcept(NO_ASSERT)
    : m_row_map{row_map}, m_column_indices{column_indices}, m_is_built{true}
  {
    Assert(m_column_indices.size() == m_row_map[m_row_map.size() - 1], "incompatible row map and column indices");
    Assert(m_row_map.size() > 0, "invalid row map");
    Assert(m_row_map[0] == 0, "row map should start with 0");
#ifndef NDEBUG
    for (size_t i = 1; i < m_row_map.size(); ++i) {
      Assert(m_row_map[i] > m_row_map[i - 1], "row map values must be strictly increasing");
    }
#endif   // NDEBUG
  }

  ConnectivityMatrix& operator=(const ConnectivityMatrix&) = default;
  ConnectivityMatrix& operator=(ConnectivityMatrix&&)      = default;

  ConnectivityMatrix(bool is_built = false) : m_is_built{is_built}
  {
    // this is useful to build
    if (is_built) {
      m_row_map = [&] {
        Array<uint32_t> row_map(1);
        row_map[0] = 0;
        return row_map;
      }();

      m_column_indices = Array<uint32_t>(0);
    }
  }
  ConnectivityMatrix(const ConnectivityMatrix&) = default;
  ConnectivityMatrix(ConnectivityMatrix&&)      = default;
  ~ConnectivityMatrix()                         = default;
};

#endif   // CONNECTIVITY_MATRIX_HPP
