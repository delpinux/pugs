#ifndef ITEM_VALUE_HPP
#define ITEM_VALUE_HPP

#include <mesh/IConnectivity.hpp>
#include <mesh/ItemId.hpp>
#include <mesh/ItemType.hpp>
#include <utils/Array.hpp>
#include <utils/PugsAssert.hpp>

#include <memory>

template <typename DataType, ItemType item_type, typename ConnectivityPtr = std::shared_ptr<const IConnectivity>>
class ItemValue
{
 public:
  static constexpr ItemType item_t{item_type};
  using data_type = DataType;

  using ItemId     = ItemIdT<item_type>;
  using index_type = ItemId;

 private:
  using ConnectivitySharedPtr = std::shared_ptr<const IConnectivity>;
  using ConnectivityWeakPtr   = std::weak_ptr<const IConnectivity>;

  static_assert(std::is_same_v<ConnectivityPtr, ConnectivitySharedPtr> or
                std::is_same_v<ConnectivityPtr, ConnectivityWeakPtr>);

  ConnectivityPtr m_connectivity_ptr;

  Array<DataType> m_values;

  // Allow const std:shared_ptr version to access our data
  friend ItemValue<std::add_const_t<DataType>, item_type, ConnectivitySharedPtr>;

  // Allow const std:weak_ptr version to access our data
  friend ItemValue<std::add_const_t<DataType>, item_type, ConnectivityWeakPtr>;

  // Allow non-const std:shared_ptr version to access our data
  friend ItemValue<std::remove_const_t<DataType>, item_type, ConnectivitySharedPtr>;

  // Allow non-const std:weak_ptr version to access our data
  friend ItemValue<std::remove_const_t<DataType>, item_type, ConnectivityWeakPtr>;

 public:
  // This is not the correct way to look at ItemValue, use with care
  Array<const DataType>
  arrayView() const
  {
    return m_values;
  }

  [[nodiscard]] friend PUGS_INLINE ItemValue<std::remove_const_t<DataType>, item_type, ConnectivityPtr>
  copy(const ItemValue<DataType, item_type, ConnectivityPtr>& source)
  {
    ItemValue<std::remove_const_t<DataType>, item_type, ConnectivityPtr> image;

    image.m_connectivity_ptr = source.m_connectivity_ptr;
    image.m_values           = copy(source.m_values);
    return image;
  }

  template <typename ConnectivityPtr2>
  friend PUGS_INLINE void copy_to(const ItemValue<std::add_const_t<DataType>, item_type, ConnectivityPtr>& source,
                                  const ItemValue<DataType, item_type, ConnectivityPtr2>& destination);

  template <typename ConnectivityPtr2>
  friend PUGS_INLINE void
  copy_to(const ItemValue<DataType, item_type, ConnectivityPtr>& source,
          const ItemValue<std::remove_const_t<DataType>, item_type, ConnectivityPtr2>& destination)
  {
    Assert(destination.m_connectivity_ptr->id() == source.m_connectivity_ptr->id(), "different connectivities");
    copy_to(source.m_values, destination.m_values);
  }

  [[nodiscard]] PUGS_INLINE bool
  isBuilt() const noexcept
  {
    return m_connectivity_ptr.use_count() != 0;
  }

  [[nodiscard]] PUGS_INLINE std::shared_ptr<const IConnectivity>
  connectivity_ptr() const noexcept
  {
    if constexpr (std::is_same_v<ConnectivityPtr, ConnectivitySharedPtr>) {
      return m_connectivity_ptr;
    } else {
      return m_connectivity_ptr.lock();
    }
  }

  [[nodiscard]] PUGS_INLINE size_t
  numberOfItems() const noexcept(NO_ASSERT)
  {
    Assert(this->isBuilt(), "ItemValue is not built");
    return m_values.size();
  }

  PUGS_INLINE
  void
  fill(const DataType& data) const noexcept
  {
    static_assert(not std::is_const_v<DataType>, "Cannot modify ItemValue of const");
    m_values.fill(data);
  }

  template <ItemType item_t>
  [[nodiscard]] PUGS_INLINE DataType&
  operator[](const ItemIdT<item_t>& item_id) const noexcept(NO_ASSERT)
  {
    static_assert(item_t == item_type, "invalid ItemId type");
    Assert(this->isBuilt(), "ItemValue is not built");
    Assert(item_id < this->numberOfItems(), "invalid item_id");
    return m_values[item_id];
  }

  template <typename DataType2, typename ConnectivityPtr2>
  PUGS_INLINE ItemValue&
  operator=(const ItemValue<DataType2, item_type, ConnectivityPtr2>& value_per_item) noexcept
  {
    // ensures that DataType is the same as source DataType2
    static_assert(std::is_same_v<std::remove_const_t<DataType>, std::remove_const_t<DataType2>>,
                  "Cannot assign ItemValue of different type");
    // ensures that const is not lost through copy
    static_assert(((std::is_const_v<DataType2> and std::is_const_v<DataType>) or not std::is_const_v<DataType2>),
                  "Cannot assign ItemValue of const to ItemValue of non-const");

    m_values = value_per_item.m_values;
    if constexpr (std::is_same_v<ConnectivityPtr, ConnectivitySharedPtr> and
                  std::is_same_v<ConnectivityPtr2, ConnectivityWeakPtr>) {
      m_connectivity_ptr = value_per_item.m_connectivity_ptr.lock();
    } else {
      m_connectivity_ptr = value_per_item.m_connectivity_ptr;
    }

    return *this;
  }

  friend std::ostream&
  operator<<(std::ostream& os, const ItemValue& item_value)
  {
    os << item_value.m_values;
    return os;
  }

  template <typename DataType2, typename ConnectivityPtr2>
  PUGS_INLINE
  ItemValue(const ItemValue<DataType2, item_type, ConnectivityPtr2>& value_per_item) noexcept
  {
    this->operator=(value_per_item);
  }

  PUGS_INLINE
  ItemValue() = default;

  PUGS_INLINE
  ItemValue(const IConnectivity& connectivity) noexcept
    : m_connectivity_ptr{connectivity.shared_ptr()}, m_values{connectivity.numberOf<item_type>()}
  {
    static_assert(not std::is_const_v<DataType>, "Cannot allocate ItemValue of const data: only view is "
                                                 "supported");
  }

  PUGS_INLINE
  ItemValue(const IConnectivity& connectivity, const Array<DataType>& values) noexcept(NO_ASSERT)
    : m_connectivity_ptr{connectivity.shared_ptr()}, m_values{values}
  {
    Assert(m_values.size() == connectivity.numberOf<item_type>(), "invalid values size");
  }

  PUGS_INLINE
  ~ItemValue() = default;
};

template <typename DataType, ItemType item_type, typename ConnectivityPtr>
PUGS_INLINE size_t
size(const ItemValue<DataType, item_type, ConnectivityPtr>& item_value)
{
  return item_value.numberOfItems();
}

template <typename DataType>
using NodeValue = ItemValue<DataType, ItemType::node>;

template <typename DataType>
using EdgeValue = ItemValue<DataType, ItemType::edge>;

template <typename DataType>
using FaceValue = ItemValue<DataType, ItemType::face>;

template <typename DataType>
using CellValue = ItemValue<DataType, ItemType::cell>;

// Weak versions: should not be used outside of Connectivity

template <typename DataType, ItemType item_type>
using WeakItemValue = ItemValue<DataType, item_type, std::weak_ptr<const IConnectivity>>;

template <typename DataType>
using WeakNodeValue = WeakItemValue<DataType, ItemType::node>;

template <typename DataType>
using WeakEdgeValue = WeakItemValue<DataType, ItemType::edge>;

template <typename DataType>
using WeakFaceValue = WeakItemValue<DataType, ItemType::face>;

template <typename DataType>
using WeakCellValue = WeakItemValue<DataType, ItemType::cell>;

#endif   // ITEM_VALUE_HPP
