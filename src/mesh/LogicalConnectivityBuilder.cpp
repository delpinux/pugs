#include <mesh/LogicalConnectivityBuilder.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityDescriptor.hpp>
#include <mesh/RefId.hpp>
#include <utils/Array.hpp>
#include <utils/Messenger.hpp>

#include <unordered_map>

template <size_t Dimension>
inline void
LogicalConnectivityBuilder::_buildBoundaryNodeList(const TinyVector<Dimension, uint64_t>&, ConnectivityDescriptor&)
{
  static_assert(Dimension <= 3, "unexpected dimension");
}

template <>
inline void
LogicalConnectivityBuilder::_buildBoundaryNodeList(
  const TinyVector<1, uint64_t>& cell_size,   // clazy:exclude=function-args-by-value
  ConnectivityDescriptor& descriptor)
{
  {   // xmin
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = 0;
    descriptor.addRefItemList(RefNodeList{RefId{0, "XMIN"}, boundary_nodes, RefItemListBase::Type::boundary});
  }

  {   // xmax
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = cell_size[0];
    descriptor.addRefItemList(RefNodeList{RefId{1, "XMAX"}, boundary_nodes, RefItemListBase::Type::boundary});
  }
}

template <>
void
LogicalConnectivityBuilder::_buildBoundaryNodeList(
  const TinyVector<2, uint64_t>& cell_size,   // clazy:exclude=function-args-by-value
  ConnectivityDescriptor& descriptor)
{
  const TinyVector<2, uint64_t> node_size{cell_size[0] + 1, cell_size[1] + 1};

  const auto node_number = [&](const uint64_t i, const uint64_t j) { return i * node_size[1] + j; };

  {   // xminymin
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(0, 0);
    descriptor.addRefItemList(RefNodeList{RefId{10, "XMINYMIN"}, boundary_nodes, RefItemListBase::Type::boundary});
  }

  {   // xmaxymin
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(cell_size[0], 0);
    descriptor.addRefItemList(RefNodeList{RefId{11, "XMAXYMIN"}, boundary_nodes, RefItemListBase::Type::boundary});
  }

  {   // xmaxymax
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(cell_size[0], cell_size[1]);
    descriptor.addRefItemList(RefNodeList{RefId{12, "XMAXYMAX"}, boundary_nodes, RefItemListBase::Type::boundary});
  }

  {   // xminymax
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(0, cell_size[1]);
    descriptor.addRefItemList(RefNodeList{RefId{13, "XMINYMAX"}, boundary_nodes, RefItemListBase::Type::boundary});
  }
}

template <>
void
LogicalConnectivityBuilder::_buildBoundaryNodeList(const TinyVector<3, uint64_t>& cell_size,
                                                   ConnectivityDescriptor& descriptor)
{
  const TinyVector<3, uint64_t> node_size{cell_size[0] + 1, cell_size[1] + 1, cell_size[2] + 1};

  const auto node_number = [&](const uint64_t i, const uint64_t j, const uint64_t k) {
    return (i * node_size[1] + j) * node_size[2] + k;
  };

  {   // xminyminzmin
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(0, 0, 0);
    descriptor.addRefItemList(RefNodeList{RefId{10, "XMINYMINZMIN"}, boundary_nodes, RefItemListBase::Type::boundary});
  }

  {   // xmaxyminzmin
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(cell_size[0], 0, 0);
    descriptor.addRefItemList(RefNodeList{RefId{11, "XMAXYMINZMIN"}, boundary_nodes, RefItemListBase::Type::boundary});
  }

  {   // xmaxymaxzmin
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(cell_size[0], cell_size[1], 0);
    descriptor.addRefItemList(RefNodeList{RefId{12, "XMAXYMAXZMIN"}, boundary_nodes, RefItemListBase::Type::boundary});
  }

  {   // xminymaxzmin
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(0, cell_size[1], 0);
    descriptor.addRefItemList(RefNodeList{RefId{13, "XMINYMAXZMIN"}, boundary_nodes, RefItemListBase::Type::boundary});
  }

  {   // xminyminzmax
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(0, 0, cell_size[2]);
    descriptor.addRefItemList(RefNodeList{RefId{14, "XMINYMINZMAX"}, boundary_nodes, RefItemListBase::Type::boundary});
  }

  {   // xmaxyminzmax
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(cell_size[0], 0, cell_size[2]);
    descriptor.addRefItemList(RefNodeList{RefId{15, "XMAXYMINZMAX"}, boundary_nodes, RefItemListBase::Type::boundary});
  }

  {   // xmaxymaxzmax
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(cell_size[0], cell_size[1], cell_size[2]);
    descriptor.addRefItemList(RefNodeList{RefId{16, "XMAXYMAXZMAX"}, boundary_nodes, RefItemListBase::Type::boundary});
  }

  {   // xminymaxzmax
    Array<NodeId> boundary_nodes(1);
    boundary_nodes[0] = node_number(0, cell_size[1], cell_size[2]);
    descriptor.addRefItemList(RefNodeList{RefId{17, "XMINYMAXZMAX"}, boundary_nodes, RefItemListBase::Type::boundary});
  }
}

template <size_t Dimension>
void
LogicalConnectivityBuilder::_buildBoundaryEdgeList(const TinyVector<Dimension, uint64_t>&, ConnectivityDescriptor&)
{
  static_assert(Dimension == 3, "unexpected dimension");
}

template <>
void
LogicalConnectivityBuilder::_buildBoundaryEdgeList(const TinyVector<3, uint64_t>& cell_size,
                                                   ConnectivityDescriptor& descriptor)
{
  const auto& node_number_vector  = descriptor.nodeNumberVector();
  const auto& node_to_edge_matrix = descriptor.nodeToEdgeMatrix();
  const auto& edge_to_node_matrix = descriptor.edgeToNodeMatrix();

  const auto find_edge = [&](uint32_t node0, uint32_t node1) {
    if (node_number_vector[node0] > node_number_vector[node1]) {
      std::swap(node0, node1);
    }
    const auto& node_edge_list = node_to_edge_matrix[node0];

    for (size_t i_edge = 0; i_edge < node_edge_list.size(); ++i_edge) {
      const EdgeId edge_id = node_edge_list[i_edge];
      if (edge_to_node_matrix[edge_id][1] == node1) {
        return edge_id;
      }
    }
    throw UnexpectedError("Cannot find edge");
  };

  const TinyVector<3, uint64_t> node_size{cell_size[0] + 1, cell_size[1] + 1, cell_size[2] + 1};
  const auto get_node_id = [&](const TinyVector<3, uint64_t>& node_logic_id) {
    return (node_logic_id[0] * node_size[1] + node_logic_id[1]) * node_size[2] + node_logic_id[2];
  };

  {   // Ridge edges in direction of Z axis
    const auto add_ref_item_list_along_z = [&](const size_t i, const size_t j, const unsigned ref_id,
                                               const std::string& ref_name) {
      Array<EdgeId> boundary_edges(cell_size[2]);
      size_t l = 0;
      for (size_t k = 0; k < cell_size[2]; ++k) {
        const uint32_t node_0_id = get_node_id(TinyVector<3, uint64_t>{i, j, k});
        const uint32_t node_1_id = get_node_id(TinyVector<3, uint64_t>{i, j, k + 1});

        boundary_edges[l++] = find_edge(node_0_id, node_1_id);
      }
      Assert(l == cell_size[2]);
      descriptor.addRefItemList(RefEdgeList{RefId{ref_id, ref_name}, boundary_edges, RefItemListBase::Type::boundary});
    };

    add_ref_item_list_along_z(0, 0, 20, "XMINYMIN");
    add_ref_item_list_along_z(0, cell_size[1], 21, "XMINYMAX");
    add_ref_item_list_along_z(cell_size[0], cell_size[1], 22, "XMAXYMAX");
    add_ref_item_list_along_z(cell_size[0], 0, 23, "XMAXYMIN");
  }

  {   // Ridge edges in direction of Y axis
    const auto add_ref_item_list_along_y = [&](const size_t i, const size_t k, const unsigned ref_id,
                                               const std::string& ref_name) {
      Array<EdgeId> boundary_edges(cell_size[1]);
      size_t l = 0;
      for (size_t j = 0; j < cell_size[1]; ++j) {
        const uint32_t node_0_id = get_node_id(TinyVector<3, uint64_t>{i, j, k});
        const uint32_t node_1_id = get_node_id(TinyVector<3, uint64_t>{i, j + 1, k});

        boundary_edges[l++] = find_edge(node_0_id, node_1_id);
      }
      Assert(l == cell_size[1]);
      descriptor.addRefItemList(RefEdgeList{RefId{ref_id, ref_name}, boundary_edges, RefItemListBase::Type::boundary});
    };

    add_ref_item_list_along_y(0, 0, 24, "XMINZMIN");
    add_ref_item_list_along_y(0, cell_size[2], 25, "XMINZMAX");
    add_ref_item_list_along_y(cell_size[0], cell_size[2], 26, "XMAXZMAX");
    add_ref_item_list_along_y(cell_size[0], 0, 27, "XMAXZMIN");
  }

  {   // Ridge edges in direction of X axis
    const auto add_ref_item_list_along_x = [&](const size_t j, const size_t k, const unsigned ref_id,
                                               const std::string& ref_name) {
      Array<EdgeId> boundary_edges(cell_size[0]);
      size_t l = 0;
      for (size_t i = 0; i < cell_size[0]; ++i) {
        const uint32_t node_0_id = get_node_id(TinyVector<3, uint64_t>{i, j, k});
        const uint32_t node_1_id = get_node_id(TinyVector<3, uint64_t>{i + 1, j, k});

        boundary_edges[l++] = find_edge(node_0_id, node_1_id);
      }
      Assert(l == cell_size[0]);
      descriptor.addRefItemList(RefEdgeList{RefId{ref_id, ref_name}, boundary_edges, RefItemListBase::Type::boundary});
    };

    add_ref_item_list_along_x(0, 0, 28, "YMINZMIN");
    add_ref_item_list_along_x(0, cell_size[2], 29, "YMINZMAX");
    add_ref_item_list_along_x(cell_size[1], cell_size[2], 30, "YMAXZMAX");
    add_ref_item_list_along_x(cell_size[1], 0, 31, "YMAXZMIN");
  }
}

template <size_t Dimension>
void
LogicalConnectivityBuilder::_buildBoundaryFaceList(const TinyVector<Dimension, uint64_t>&, ConnectivityDescriptor&)
{
  static_assert(Dimension >= 2 and Dimension <= 3, "unexpected dimension");
}

template <>
void
LogicalConnectivityBuilder::_buildBoundaryFaceList(
  const TinyVector<2, uint64_t>& cell_size,   // clazy:exclude=function-args-by-value
  ConnectivityDescriptor& descriptor)
{
  const auto cell_number = [&](const TinyVector<2, uint64_t> cell_logic_id) {
    return cell_logic_id[0] * cell_size[1] + cell_logic_id[1];
  };

  const auto& cell_to_face_matrix = descriptor.cellToFaceMatrix();

  {   // xmin
    const size_t i = 0;
    Array<FaceId> boundary_faces(cell_size[1]);
    for (size_t j = 0; j < cell_size[1]; ++j) {
      constexpr size_t left_face = 3;

      const size_t cell_id = cell_number(TinyVector<2, uint64_t>{i, j});
      const size_t face_id = cell_to_face_matrix[cell_id][left_face];

      boundary_faces[j] = face_id;
    }
    descriptor.addRefItemList(RefFaceList{RefId{0, "XMIN"}, boundary_faces, RefItemListBase::Type::boundary});
  }

  {   // xmax
    const size_t i = cell_size[0] - 1;
    Array<FaceId> boundary_faces(cell_size[1]);
    for (size_t j = 0; j < cell_size[1]; ++j) {
      constexpr size_t right_face = 1;

      const size_t cell_id = cell_number(TinyVector<2, uint64_t>{i, j});
      const size_t face_id = cell_to_face_matrix[cell_id][right_face];

      boundary_faces[j] = face_id;
    }
    descriptor.addRefItemList(RefFaceList{RefId{1, "XMAX"}, boundary_faces, RefItemListBase::Type::boundary});
  }

  {   // ymin
    const size_t j = 0;
    Array<FaceId> boundary_faces(cell_size[0]);
    for (size_t i = 0; i < cell_size[0]; ++i) {
      constexpr size_t bottom_face = 0;

      const size_t cell_id = cell_number(TinyVector<2, uint64_t>{i, j});
      const size_t face_id = cell_to_face_matrix[cell_id][bottom_face];

      boundary_faces[i] = face_id;
    }
    descriptor.addRefItemList(RefFaceList{RefId{2, "YMIN"}, boundary_faces, RefItemListBase::Type::boundary});
  }

  {   // ymax
    const size_t j = cell_size[1] - 1;
    Array<FaceId> boundary_faces(cell_size[0]);
    for (size_t i = 0; i < cell_size[0]; ++i) {
      constexpr size_t top_face = 2;

      const size_t cell_id = cell_number(TinyVector<2, uint64_t>{i, j});
      const size_t face_id = cell_to_face_matrix[cell_id][top_face];

      boundary_faces[i] = face_id;
    }
    descriptor.addRefItemList(RefFaceList{RefId{3, "YMAX"}, boundary_faces, RefItemListBase::Type::boundary});
  }
}

template <>
void
LogicalConnectivityBuilder::_buildBoundaryFaceList(const TinyVector<3, uint64_t>& cell_size,
                                                   ConnectivityDescriptor& descriptor)
{
  const auto& node_number_vector  = descriptor.nodeNumberVector();
  const auto& face_to_node_matrix = descriptor.faceToNodeMatrix();
  const auto& node_to_face_matrix = descriptor.nodeToFaceMatrix();

  const auto find_face = [&](std::array<uint32_t, 4> node_list) {
    size_t i_node_smallest_number = 0;
    for (size_t i_node = 1; i_node < node_list.size(); ++i_node) {
      if (node_number_vector[node_list[i_node]] < node_number_vector[node_list[i_node_smallest_number]]) {
        i_node_smallest_number = i_node;
      }
    }

    if (i_node_smallest_number != 0) {
      std::array<uint64_t, 4> buffer;
      for (size_t i_node = i_node_smallest_number; i_node < buffer.size(); ++i_node) {
        buffer[i_node - i_node_smallest_number] = node_list[i_node];
      }
      for (size_t i_node = 0; i_node < i_node_smallest_number; ++i_node) {
        buffer[i_node + node_list.size() - i_node_smallest_number] = node_list[i_node];
      }

      for (size_t i_node = 0; i_node < node_list.size(); ++i_node) {
        node_list[i_node] = buffer[i_node];
      }
    }

    if (node_number_vector[node_list[1]] > node_number_vector[node_list[node_list.size() - 1]]) {
      for (size_t i_node = 1; i_node <= (node_list.size() + 1) / 2 - 1; ++i_node) {
        std::swap(node_list[i_node], node_list[node_list.size() - i_node]);
      }
    }

    const auto& node_face_list = node_to_face_matrix[node_list[0]];

    for (size_t i_face = 0; i_face < node_face_list.size(); ++i_face) {
      const FaceId face_id       = node_face_list[i_face];
      const auto& face_node_list = face_to_node_matrix[face_id];
      if ((face_node_list[1] == node_list[1]) and (face_node_list[2] == node_list[2]) and
          (face_node_list[3] == node_list[3])) {
        return face_id;
      }
    }
    throw UnexpectedError("Cannot find edge");
  };

  const TinyVector<3, uint64_t> node_size{cell_size[0] + 1, cell_size[1] + 1, cell_size[2] + 1};
  const auto get_node_id = [&](const TinyVector<3, uint64_t>& node_logic_id) {
    return (node_logic_id[0] * node_size[1] + node_logic_id[1]) * node_size[2] + node_logic_id[2];
  };

  {   // faces orthogonal to X
    const auto add_ref_item_list_for_x = [&](const size_t i, const unsigned ref_id, const std::string& ref_name) {
      Array<FaceId> boundary_faces(cell_size[1] * cell_size[2]);
      size_t l = 0;
      for (size_t j = 0; j < cell_size[1]; ++j) {
        for (size_t k = 0; k < cell_size[2]; ++k) {
          const uint32_t node_0_id = get_node_id(TinyVector<3, uint64_t>{i, j, k});
          const uint32_t node_1_id = get_node_id(TinyVector<3, uint64_t>{i, j + 1, k});
          const uint32_t node_2_id = get_node_id(TinyVector<3, uint64_t>{i, j + 1, k + 1});
          const uint32_t node_3_id = get_node_id(TinyVector<3, uint64_t>{i, j, k + 1});

          boundary_faces[l++] = find_face({node_0_id, node_1_id, node_2_id, node_3_id});
        }
      }
      Assert(l == cell_size[1] * cell_size[2]);
      descriptor.addRefItemList(RefFaceList{RefId{ref_id, ref_name}, boundary_faces, RefItemListBase::Type::boundary});
    };

    add_ref_item_list_for_x(0, 0, "XMIN");
    add_ref_item_list_for_x(cell_size[0], 1, "XMAX");
  }

  {   // faces orthogonal to Y
    const auto add_ref_item_list_for_y = [&](const size_t j, const unsigned ref_id, const std::string& ref_name) {
      Array<FaceId> boundary_faces(cell_size[0] * cell_size[2]);
      size_t l = 0;
      for (size_t i = 0; i < cell_size[0]; ++i) {
        for (size_t k = 0; k < cell_size[2]; ++k) {
          const uint32_t node_0_id = get_node_id(TinyVector<3, uint64_t>{i, j, k});
          const uint32_t node_1_id = get_node_id(TinyVector<3, uint64_t>{i + 1, j, k});
          const uint32_t node_2_id = get_node_id(TinyVector<3, uint64_t>{i + 1, j, k + 1});
          const uint32_t node_3_id = get_node_id(TinyVector<3, uint64_t>{i, j, k + 1});

          boundary_faces[l++] = find_face({node_0_id, node_1_id, node_2_id, node_3_id});
        }
      }
      Assert(l == cell_size[0] * cell_size[2]);
      descriptor.addRefItemList(RefFaceList{RefId{ref_id, ref_name}, boundary_faces, RefItemListBase::Type::boundary});
    };

    add_ref_item_list_for_y(0, 2, "YMIN");
    add_ref_item_list_for_y(cell_size[1], 3, "YMAX");
  }

  {   // faces orthogonal to Z
    const auto add_ref_item_list_for_z = [&](const size_t k, const unsigned ref_id, const std::string& ref_name) {
      Array<FaceId> boundary_faces(cell_size[0] * cell_size[1]);
      size_t l = 0;
      for (size_t i = 0; i < cell_size[0]; ++i) {
        for (size_t j = 0; j < cell_size[1]; ++j) {
          const uint32_t node_0_id = get_node_id(TinyVector<3, uint64_t>{i, j, k});
          const uint32_t node_1_id = get_node_id(TinyVector<3, uint64_t>{i + 1, j, k});
          const uint32_t node_2_id = get_node_id(TinyVector<3, uint64_t>{i + 1, j + 1, k});
          const uint32_t node_3_id = get_node_id(TinyVector<3, uint64_t>{i, j + 1, k});

          boundary_faces[l++] = find_face({node_0_id, node_1_id, node_2_id, node_3_id});
        }
      }
      Assert(l == cell_size[0] * cell_size[1]);
      descriptor.addRefItemList(RefFaceList{RefId{ref_id, ref_name}, boundary_faces, RefItemListBase::Type::boundary});
    };

    add_ref_item_list_for_z(0, 4, "ZMIN");
    add_ref_item_list_for_z(cell_size[2], 5, "ZMAX");
  }
}

template <>
void
LogicalConnectivityBuilder::_buildConnectivity(
  const TinyVector<1, uint64_t>& cell_size)   // clazy:exclude=function-args-by-value
{
  const size_t number_of_cells = cell_size[0];
  const size_t number_of_nodes = cell_size[0] + 1;

  ConnectivityDescriptor descriptor;
  descriptor.setNodeNumberVector([&] {
    Array<int> node_number_vector(number_of_nodes);
    parallel_for(
      number_of_nodes, PUGS_LAMBDA(const size_t i) { node_number_vector[i] = i; });
    return node_number_vector;
  }());

  descriptor.setCellNumberVector([&] {
    Array<int> cell_number_vector(number_of_cells);
    for (size_t i = 0; i < number_of_cells; ++i) {
      cell_number_vector[i] = i;
    }
    return cell_number_vector;
  }());

  Array<CellType> cell_type_vector(number_of_cells);
  cell_type_vector.fill(CellType::Line);
  descriptor.setCellTypeVector(cell_type_vector);

  Array<unsigned int> cell_to_node_row_map(number_of_cells + 1);
  for (size_t i = 0; i < cell_to_node_row_map.size(); ++i) {
    cell_to_node_row_map[i] = 2 * i;
  }
  Array<unsigned int> cell_to_node_list(2 * number_of_cells);
  for (size_t i = 0; i < number_of_cells; ++i) {
    cell_to_node_list[2 * i]     = i;
    cell_to_node_list[2 * i + 1] = i + 1;
  }

  descriptor.setCellToNodeMatrix(ConnectivityMatrix(cell_to_node_row_map, cell_to_node_list));

  this->_buildBoundaryNodeList(cell_size, descriptor);

  descriptor.setCellOwnerVector([&] {
    Array<int> cell_owner_vector(number_of_cells);
    cell_owner_vector.fill(parallel::rank());
    return cell_owner_vector;
  }());

  descriptor.setNodeOwnerVector([&] {
    Array<int> node_owner_vector(number_of_nodes);
    node_owner_vector.fill(parallel::rank());
    return node_owner_vector;
  }());

  m_connectivity = Connectivity1D::build(descriptor);
}

template <>
void
LogicalConnectivityBuilder::_buildConnectivity(
  const TinyVector<2, uint64_t>& cell_size)   // clazy:exclude=function-args-by-value
{
  constexpr size_t Dimension = 2;

  const TinyVector<Dimension, uint64_t> node_size{cell_size[0] + 1, cell_size[1] + 1};

  const size_t number_of_cells = cell_size[0] * cell_size[1];
  const size_t number_of_nodes = node_size[0] * node_size[1];

  ConnectivityDescriptor descriptor;
  descriptor.setNodeNumberVector([&] {
    Array<int> node_number_vector(number_of_nodes);
    parallel_for(
      number_of_nodes, PUGS_LAMBDA(const size_t i) { node_number_vector[i] = i; });
    return node_number_vector;
  }());

  descriptor.setCellNumberVector([&] {
    Array<int> cell_number_vector(number_of_cells);
    parallel_for(
      number_of_cells, PUGS_LAMBDA(size_t i) { cell_number_vector[i] = i; });
    return cell_number_vector;
  }());

  Array<CellType> cell_type_vector(number_of_cells);
  cell_type_vector.fill(CellType::Quadrangle);
  descriptor.setCellTypeVector(cell_type_vector);

  const auto node_number = [&](const TinyVector<Dimension, uint64_t> node_logic_id) {
    return node_logic_id[0] * node_size[1] + node_logic_id[1];
  };

  const auto cell_logic_id = [&](size_t j) {
    const uint64_t j0 = j / cell_size[1];
    const uint64_t j1 = j % cell_size[1];
    return TinyVector<Dimension, uint64_t>{j0, j1};
  };

  Array<unsigned int> cell_to_node_row_map(number_of_cells + 1);
  for (size_t i = 0; i < cell_to_node_row_map.size(); ++i) {
    cell_to_node_row_map[i] = 4 * i;
  }
  Array<unsigned int> cell_to_node_list(4 * number_of_cells);
  for (size_t j = 0; j < number_of_cells; ++j) {
    TinyVector<Dimension, size_t> cell_index = cell_logic_id(j);

    cell_to_node_list[4 * j]     = node_number(cell_index + TinyVector<Dimension, uint64_t>{0, 0});
    cell_to_node_list[4 * j + 1] = node_number(cell_index + TinyVector<Dimension, uint64_t>{1, 0});
    cell_to_node_list[4 * j + 2] = node_number(cell_index + TinyVector<Dimension, uint64_t>{1, 1});
    cell_to_node_list[4 * j + 3] = node_number(cell_index + TinyVector<Dimension, uint64_t>{0, 1});
  }

  descriptor.setCellToNodeMatrix(ConnectivityMatrix(cell_to_node_row_map, cell_to_node_list));

  ConnectivityBuilderBase::_computeCellFaceAndFaceNodeConnectivities<Dimension>(descriptor);

  this->_buildBoundaryNodeList(cell_size, descriptor);
  this->_buildBoundaryFaceList(cell_size, descriptor);

  descriptor.setCellOwnerVector([&] {
    Array<int> cell_owner_vector(number_of_cells);
    cell_owner_vector.fill(parallel::rank());
    return cell_owner_vector;
  }());

  descriptor.setFaceOwnerVector([&] {
    Array<int> face_owner_vector(descriptor.faceNumberVector().size());
    face_owner_vector.fill(parallel::rank());
    return face_owner_vector;
  }());

  descriptor.setNodeOwnerVector([&] {
    Array<int> node_owner_vector(descriptor.nodeNumberVector().size());
    node_owner_vector.fill(parallel::rank());
    return node_owner_vector;
  }());

  m_connectivity = Connectivity<Dimension>::build(descriptor);
}

template <>
void
LogicalConnectivityBuilder::_buildConnectivity(const TinyVector<3, uint64_t>& cell_size)
{
  constexpr size_t Dimension = 3;

  const TinyVector<Dimension, uint64_t> node_size = [&] {
    TinyVector mutable_node_size{cell_size};
    for (size_t i = 0; i < Dimension; ++i) {
      mutable_node_size[i] += 1;
    }
    return mutable_node_size;
  }();

  auto count_items = [](auto&& v) {
    using V_Type = std::decay_t<decltype(v)>;
    static_assert(is_tiny_vector_v<V_Type>);

    size_t sum = v[0];
    for (size_t i = 1; i < Dimension; ++i) {
      sum *= v[i];
    }
    return sum;
  };

  const size_t number_of_cells = count_items(cell_size);
  const size_t number_of_nodes = count_items(node_size);

  ConnectivityDescriptor descriptor;
  descriptor.setNodeNumberVector([&] {
    Array<int> node_number_vector(number_of_nodes);
    parallel_for(
      number_of_nodes, PUGS_LAMBDA(const size_t i) { node_number_vector[i] = i; });
    return node_number_vector;
  }());

  descriptor.setCellNumberVector([&] {
    Array<int> cell_number_vector(number_of_cells);
    parallel_for(
      number_of_cells, PUGS_LAMBDA(size_t i) { cell_number_vector[i] = i; });
    return cell_number_vector;
  }());

  Array<CellType> cell_type_vector(number_of_cells);
  cell_type_vector.fill(CellType::Hexahedron);
  descriptor.setCellTypeVector(cell_type_vector);

  const auto cell_logic_id = [&](size_t j) {
    const size_t slice1  = cell_size[1] * cell_size[2];
    const size_t& slice2 = cell_size[2];
    const uint64_t j0    = j / slice1;
    const uint64_t j1    = (j - j0 * slice1) / slice2;
    const uint64_t j2    = j - (j0 * slice1 + j1 * slice2);
    return TinyVector<Dimension, uint64_t>{j0, j1, j2};
  };

  const auto node_number = [&](const TinyVector<Dimension, uint64_t>& node_logic_id) {
    return (node_logic_id[0] * node_size[1] + node_logic_id[1]) * node_size[2] + node_logic_id[2];
  };

  Array<unsigned int> cell_to_node_row_map(number_of_cells + 1);
  for (size_t i = 0; i < cell_to_node_row_map.size(); ++i) {
    cell_to_node_row_map[i] = 8 * i;
  }
  Array<unsigned int> cell_to_node_list(8 * number_of_cells);
  for (size_t j = 0; j < number_of_cells; ++j) {
    TinyVector<Dimension, size_t> cell_index = cell_logic_id(j);

    cell_to_node_list[8 * j]     = node_number(cell_index + TinyVector<Dimension, uint64_t>{0, 0, 0});
    cell_to_node_list[8 * j + 1] = node_number(cell_index + TinyVector<Dimension, uint64_t>{1, 0, 0});
    cell_to_node_list[8 * j + 2] = node_number(cell_index + TinyVector<Dimension, uint64_t>{1, 1, 0});
    cell_to_node_list[8 * j + 3] = node_number(cell_index + TinyVector<Dimension, uint64_t>{0, 1, 0});
    cell_to_node_list[8 * j + 4] = node_number(cell_index + TinyVector<Dimension, uint64_t>{0, 0, 1});
    cell_to_node_list[8 * j + 5] = node_number(cell_index + TinyVector<Dimension, uint64_t>{1, 0, 1});
    cell_to_node_list[8 * j + 6] = node_number(cell_index + TinyVector<Dimension, uint64_t>{1, 1, 1});
    cell_to_node_list[8 * j + 7] = node_number(cell_index + TinyVector<Dimension, uint64_t>{0, 1, 1});
  }

  descriptor.setCellToNodeMatrix(ConnectivityMatrix(cell_to_node_row_map, cell_to_node_list));

  ConnectivityBuilderBase::_computeCellFaceAndFaceNodeConnectivities<Dimension>(descriptor);
  ConnectivityBuilderBase::_computeFaceEdgeAndEdgeNodeAndCellEdgeConnectivities<Dimension>(descriptor);

  this->_buildBoundaryNodeList(cell_size, descriptor);
  this->_buildBoundaryEdgeList(cell_size, descriptor);
  this->_buildBoundaryFaceList(cell_size, descriptor);

  descriptor.setCellOwnerVector([&] {
    Array<int> cell_owner_vector(number_of_cells);
    cell_owner_vector.fill(parallel::rank());
    return cell_owner_vector;
  }());

  descriptor.setFaceOwnerVector([&] {
    Array<int> face_owner_vector(descriptor.faceNumberVector().size());
    face_owner_vector.fill(parallel::rank());
    return face_owner_vector;
  }());

  descriptor.setEdgeOwnerVector([&] {
    Array<int> edge_owner_vector(descriptor.edgeNumberVector().size());
    edge_owner_vector.fill(parallel::rank());
    return edge_owner_vector;
  }());

  descriptor.setNodeOwnerVector([&] {
    Array<int> node_owner_vector(descriptor.nodeNumberVector().size());
    node_owner_vector.fill(parallel::rank());
    return node_owner_vector;
  }());

  m_connectivity = Connectivity<Dimension>::build(descriptor);
}

template <size_t Dimension>
LogicalConnectivityBuilder::LogicalConnectivityBuilder(const TinyVector<Dimension, uint64_t>& size)
{
  if (parallel::rank() == 0) {
    this->_buildConnectivity(size);
  }
}

template LogicalConnectivityBuilder::LogicalConnectivityBuilder(const TinyVector<1, uint64_t>&);

template LogicalConnectivityBuilder::LogicalConnectivityBuilder(const TinyVector<2, uint64_t>&);

template LogicalConnectivityBuilder::LogicalConnectivityBuilder(const TinyVector<3, uint64_t>&);
