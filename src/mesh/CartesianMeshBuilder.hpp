#ifndef CARTESIAN_MESH_BUILDER_HPP
#define CARTESIAN_MESH_BUILDER_HPP

#include <algebra/TinyVector.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshBuilderBase.hpp>

class CartesianMeshBuilder : public MeshBuilderBase
{
 private:
  template <size_t Dimension>
  void _buildCartesianMesh(const TinyVector<Dimension>& a,
                           const TinyVector<Dimension>& b,
                           const TinyVector<Dimension, uint64_t>& size);

  template <size_t Dimension>
  NodeValue<TinyVector<Dimension>> _getNodeCoordinates(const TinyVector<Dimension>& a,
                                                       const TinyVector<Dimension>& b,
                                                       const TinyVector<Dimension, uint64_t>& size,
                                                       const IConnectivity& connectivity) const;

 public:
  template <size_t Dimension>
  CartesianMeshBuilder(const TinyVector<Dimension>& a,
                       const TinyVector<Dimension>& b,
                       const TinyVector<Dimension, uint64_t>& size);
  ~CartesianMeshBuilder() = default;
};

#endif   // CARTESIAN_MESH_BUILDER_HPP
