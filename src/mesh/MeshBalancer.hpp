#ifndef MESH_BALANCER_HPP
#define MESH_BALANCER_HPP

#include <mesh/MeshBuilderBase.hpp>

class MeshBalancer : public MeshBuilderBase
{
 public:
  MeshBalancer(const std::shared_ptr<const MeshVariant>& initial_mesh);
  ~MeshBalancer() = default;
};

#endif   // MESH_BALANCER_HPP
