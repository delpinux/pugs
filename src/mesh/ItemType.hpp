#ifndef ITEM_TYPE_HPP
#define ITEM_TYPE_HPP

#include <utils/PugsMacros.hpp>

#include <limits>
#include <string>
#include <utility>

enum class ItemType
{
  node = 0,
  edge = 1,
  face = 2,
  cell = 3
};

PUGS_INLINE
constexpr std::string_view
itemName(ItemType item_type)
{
  std::string_view name;
  switch (item_type) {
  case ItemType::node: {
    name = "node";
    break;
  }
  case ItemType::edge: {
    name = "edge";
    break;
  }
  case ItemType::face: {
    name = "face";
    break;
  }
  case ItemType::cell: {
    name = "cell";
    break;
  }
  }
  return name;
}

template <size_t Dimension>
struct ItemTypeId
{
};

template <>
struct ItemTypeId<1>
{
  PUGS_INLINE
  static constexpr size_t
  itemTId(ItemType item_type)
  {
    size_t i = std::numeric_limits<size_t>::max();
    switch (item_type) {
    case ItemType::cell: {
      i = 0;
      break;
    }
    case ItemType::face:
    case ItemType::edge:
    case ItemType::node: {
      // in 1d, faces, edges and nodes are the same
      i = 1;
      break;
    }
    }
    return i;
  }

  PUGS_INLINE
  static constexpr size_t
  dimension(ItemType item_type)
  {
    return 1 - itemTId(item_type);
  }
};

template <ItemType item_type>
inline constexpr bool is_node_in_1d_v = ItemTypeId<1>::itemTId(item_type) == ItemTypeId<1>::itemTId(ItemType::node);

template <>
struct ItemTypeId<2>
{
  PUGS_INLINE
  static constexpr size_t
  itemTId(ItemType item_type)
  {
    size_t i = std::numeric_limits<size_t>::max();
    switch (item_type) {
    case ItemType::cell: {
      i = 0;
      break;
    }
    case ItemType::face:
    case ItemType::edge: {
      // in 2d, faces and edges are the same
      i = 1;
      break;
    }
    case ItemType::node: {
      i = 2;
      break;
    }
    }
    return i;
  }

  PUGS_INLINE
  static constexpr size_t
  dimension(ItemType item_type)
  {
    return 2 - itemTId(item_type);
  }
};

template <ItemType item_type>
inline constexpr bool is_face_in_2d_v = ItemTypeId<2>::itemTId(item_type) == ItemTypeId<2>::itemTId(ItemType::face);

template <>
struct ItemTypeId<3>
{
  PUGS_INLINE
  static constexpr size_t
  itemTId(ItemType item_type)
  {
    size_t i = std::numeric_limits<size_t>::max();
    switch (item_type) {
    case ItemType::cell: {
      i = 0;
      break;
    }
    case ItemType::face: {
      i = 1;
      break;
    }
    case ItemType::edge: {
      i = 2;
      break;
    }
    case ItemType::node: {
      i = 3;
      break;
    }
    }
    return i;
  }

  PUGS_INLINE
  static constexpr size_t
  dimension(ItemType item_type)
  {
    return 3 - itemTId(item_type);
  }
};

template <ItemType item_type>
PUGS_INLINE constexpr bool is_false_item_type_v = false;

#endif   // ITEM_TYPE_HPP
