#ifndef DUAL_1D_CONNECTIVITY_BUILDER_HPP
#define DUAL_1D_CONNECTIVITY_BUILDER_HPP

#include <mesh/ConnectivityBuilderBase.hpp>
#include <mesh/IPrimalToDualConnectivityDataMapper.hpp>
#include <mesh/ItemIdToItemIdMap.hpp>

#include <memory>

template <size_t>
class Connectivity;
class ConnectivityDescriptor;

class Dual1DConnectivityBuilder : public ConnectivityBuilderBase
{
  NodeIdToNodeIdMap m_primal_node_to_dual_node_map;
  CellIdToNodeIdMap m_primal_cell_to_dual_node_map;
  NodeIdToCellIdMap m_primal_node_to_dual_cell_map;

  std::shared_ptr<IPrimalToDualConnectivityDataMapper> m_mapper;

  void _buildConnectivityDescriptor(const Connectivity<1>&, ConnectivityDescriptor&);

  void _buildConnectivityFrom(const IConnectivity&);

  friend class DualConnectivityManager;
  Dual1DConnectivityBuilder(const IConnectivity&);

 public:
  std::shared_ptr<IPrimalToDualConnectivityDataMapper>
  mapper() const
  {
    return m_mapper;
  }

  ~Dual1DConnectivityBuilder() = default;
};

#endif   // DUAL_1D_CONNECTIVITY_BUILDER_HPP
