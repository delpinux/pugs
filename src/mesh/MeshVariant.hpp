#ifndef MESH_VARIANT_HPP
#define MESH_VARIANT_HPP

#include <mesh/MeshTraits.hpp>
#include <utils/Demangle.hpp>
#include <utils/Exceptions.hpp>
#include <utils/PugsMacros.hpp>

#include <rang.hpp>

#include <iostream>
#include <memory>
#include <sstream>
#include <variant>

class IConnectivity;

template <size_t Dimension>
class Mesh;

class MeshVariant
{
 private:
  using Variant = std::variant<std::shared_ptr<const Mesh<1>>,   //
                               std::shared_ptr<const Mesh<2>>,   //
                               std::shared_ptr<const Mesh<3>>>;

  Variant m_p_mesh_variant;

 public:
  friend std::ostream& operator<<(std::ostream& os, const MeshVariant& mesh_v);

  size_t id() const;

  size_t numberOfCells() const;
  size_t numberOfFaces() const;
  size_t numberOfEdges() const;
  size_t numberOfNodes() const;

  const IConnectivity& connectivity() const;
  std::shared_ptr<const IConnectivity> shared_connectivity() const;

  template <MeshConcept MeshType>
  PUGS_INLINE std::shared_ptr<const MeshType>
  get() const
  {
    if (not std::holds_alternative<std::shared_ptr<const MeshType>>(this->m_p_mesh_variant)) {
      std::ostringstream error_msg;
      error_msg << "invalid mesh type type\n";
      error_msg << "- required " << rang::fgB::red << demangle<MeshType>() << rang::fg::reset << '\n';
      error_msg << "- contains " << rang::fgB::yellow
                << std::visit([](auto&& p_mesh) -> std::string { return demangle<mesh_type_t<decltype(p_mesh)>>(); },
                              this->m_p_mesh_variant)
                << rang::fg::reset;
      throw NormalError(error_msg.str());
    }
    return std::get<std::shared_ptr<const MeshType>>(m_p_mesh_variant);
  }

  PUGS_INLINE
  Variant
  variant() const
  {
    return m_p_mesh_variant;
  }

  MeshVariant() = delete;

  template <MeshConcept MeshType>
  MeshVariant(const std::shared_ptr<const MeshType>& p_mesh) : m_p_mesh_variant{p_mesh}
  {}

  MeshVariant(const MeshVariant&) = default;
  MeshVariant(MeshVariant&&)      = default;

  ~MeshVariant() = default;
};

#endif   // MESH_VARIANT_HPP
