#include <mesh/MeshData.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ItemValueVariant.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshVariant.hpp>
#include <output/NamedItemValueVariant.hpp>
#include <output/VTKWriter.hpp>
#include <utils/Exceptions.hpp>

template <size_t Dimension>
void
MeshData<Mesh<Dimension>>::_storeBadMesh()
{
  VTKWriter writer("bad_mesh");
  writer.writeOnMesh(std::make_shared<MeshVariant>(
                       std::make_shared<const Mesh<Dimension>>(m_mesh.shared_connectivity(), m_mesh.xr())),
                     {std::make_shared<NamedItemValueVariant>(std::make_shared<ItemValueVariant>(m_Vj), "volume")});
  std::ostringstream error_msg;
  error_msg << "mesh contains cells of non-positive volume (see " << rang::fgB::yellow << "bad_mesh.pvd"
            << rang::fg::reset << " file).";
  throw NormalError(error_msg.str());
}

template void MeshData<Mesh<1>>::_storeBadMesh();
template void MeshData<Mesh<2>>::_storeBadMesh();
template void MeshData<Mesh<3>>::_storeBadMesh();
