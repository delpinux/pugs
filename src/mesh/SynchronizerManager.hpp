#ifndef SYNCHRONIZER_MANAGER_HPP
#define SYNCHRONIZER_MANAGER_HPP

#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>

#include <memory>
#include <unordered_map>

class IConnectivity;
class Synchronizer;

class SynchronizerManager
{
 private:
  std::unordered_map<const IConnectivity*, std::shared_ptr<Synchronizer>> m_connectivity_synchronizer_map;

  static SynchronizerManager* m_instance;

  SynchronizerManager(const SynchronizerManager&) = delete;
  SynchronizerManager(SynchronizerManager&&)      = delete;

  SynchronizerManager()  = default;
  ~SynchronizerManager() = default;

 public:
  static void create();
  static void destroy();

  PUGS_INLINE
  static SynchronizerManager&
  instance()
  {
    Assert(m_instance != nullptr, "SynchronizerManager was not created!");
    return *m_instance;
  }

  void deleteConnectivitySynchronizer(const IConnectivity*);
  Synchronizer& getConnectivitySynchronizer(const IConnectivity*);
};

#endif   // SYNCHRONIZER_MANAGER_HPP
