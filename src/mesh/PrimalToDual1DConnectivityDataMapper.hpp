#ifndef PRIMAL_TO_DUAL_1D_CONNECTIVITY_DATA_MAPPER_HPP
#define PRIMAL_TO_DUAL_1D_CONNECTIVITY_DATA_MAPPER_HPP

#include <mesh/Connectivity.hpp>
#include <mesh/IPrimalToDualConnectivityDataMapper.hpp>
#include <mesh/ItemIdToItemIdMap.hpp>
#include <mesh/ItemValue.hpp>
#include <utils/Array.hpp>
#include <utils/PugsAssert.hpp>

class PrimalToDual1DConnectivityDataMapper : public IPrimalToDualConnectivityDataMapper
{
 private:
  const IConnectivity* m_primal_connectivity;
  const IConnectivity* m_dual_connectivity;

  ConstNodeIdToNodeIdMap m_primal_node_to_dual_node_map;
  ConstCellIdToNodeIdMap m_primal_cell_to_dual_node_map;
  ConstNodeIdToCellIdMap m_primal_node_to_dual_cell_map;

 public:
  template <typename OriginDataType1,
            typename OriginDataType2,
            typename DestinationDataType,
            ItemType origin_node_type,
            ItemType destination_node_type>
  void
  toDualNode(const ItemValue<OriginDataType1, origin_node_type>& primal_node_value,
             const CellValue<OriginDataType2>& primal_cell_value,
             const ItemValue<DestinationDataType, destination_node_type>& dual_node_value) const
  {
    static_assert(is_node_in_1d_v<origin_node_type>, "invalid origin node type");
    static_assert(is_node_in_1d_v<destination_node_type>, "invalid destination node type");

    static_assert(not std::is_const_v<DestinationDataType>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType1>, DestinationDataType>, "incompatible types");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType2>, DestinationDataType>, "incompatible types");

    Assert(m_primal_connectivity == primal_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for primal CellValue");
    Assert(m_primal_connectivity == primal_node_value.connectivity_ptr().get(),
           "unexpected connectivity for primal NodeValue");
    Assert(m_dual_connectivity == dual_node_value.connectivity_ptr().get(),
           "unexpected connectivity for dual NodeValue");

    using OriginNodeId      = ItemIdT<origin_node_type>;
    using DestinationNodeId = ItemIdT<destination_node_type>;

    parallel_for(
      m_primal_node_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_node_id, dual_node_id] = m_primal_node_to_dual_node_map[i];

        DestinationNodeId destination_node_id = static_cast<typename DestinationNodeId::base_type>(dual_node_id);
        OriginNodeId origin_node_id           = static_cast<typename OriginNodeId::base_type>(primal_node_id);

        dual_node_value[destination_node_id] = primal_node_value[origin_node_id];
      });

    parallel_for(
      m_primal_cell_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_cell_id, dual_node_id] = m_primal_cell_to_dual_node_map[i];

        DestinationNodeId destination_node_id = static_cast<typename DestinationNodeId::base_type>(dual_node_id);

        dual_node_value[destination_node_id] = primal_cell_value[primal_cell_id];
      });
  }

  template <typename OriginDataType,
            typename DestinationDataType1,
            typename DestinationDataType2,
            ItemType origin_node_type,
            ItemType destination_node_type>
  void
  fromDualNode(const ItemValue<OriginDataType, origin_node_type>& dual_node_value,
               const ItemValue<DestinationDataType1, destination_node_type>& primal_node_value,
               const CellValue<DestinationDataType2>& primal_cell_value) const
  {
    static_assert(is_node_in_1d_v<origin_node_type>, "invalid origin node type");
    static_assert(is_node_in_1d_v<destination_node_type>, "invalid destination node type");

    static_assert(not std::is_const_v<DestinationDataType1>, "destination data type must not be constant");
    static_assert(not std::is_const_v<DestinationDataType2>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType1>, "incompatible types");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType2>, "incompatible types");

    Assert(m_primal_connectivity == primal_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for primal CellValue");
    Assert(m_primal_connectivity == primal_node_value.connectivity_ptr().get(),
           "unexpected connectivity for primal NodeValue");
    Assert(m_dual_connectivity == dual_node_value.connectivity_ptr().get(),
           "unexpected connectivity for dual NodeValue");

    using OriginNodeId      = ItemIdT<origin_node_type>;
    using DestinationNodeId = ItemIdT<destination_node_type>;

    parallel_for(
      m_primal_node_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_node_id, dual_node_id] = m_primal_node_to_dual_node_map[i];

        DestinationNodeId destination_node_id = static_cast<typename DestinationNodeId::base_type>(primal_node_id);
        OriginNodeId origin_node_id           = static_cast<typename OriginNodeId::base_type>(dual_node_id);

        primal_node_value[destination_node_id] = dual_node_value[origin_node_id];
      });

    parallel_for(
      m_primal_cell_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_cell_id, dual_node_id] = m_primal_cell_to_dual_node_map[i];

        OriginNodeId origin_node_id = static_cast<typename OriginNodeId::base_type>(dual_node_id);

        primal_cell_value[primal_cell_id] = dual_node_value[origin_node_id];
      });
  }

  template <typename OriginDataType, typename DestinationDataType, ItemType origin_node_type>
  void
  toDualCell(const ItemValue<OriginDataType, origin_node_type>& primal_node_value,
             const CellValue<DestinationDataType>& dual_cell_value) const
  {
    static_assert(is_node_in_1d_v<origin_node_type>, "invalid origin node type");

    static_assert(not std::is_const_v<DestinationDataType>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType>, "incompatible types");

    Assert(m_primal_connectivity == primal_node_value.connectivity_ptr().get(),
           "unexpected connectivity for primal NodeValue");
    Assert(m_dual_connectivity == dual_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for dual CellValue");
    using OriginNodeId = ItemIdT<origin_node_type>;

    parallel_for(
      m_primal_node_to_dual_cell_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_node_id, dual_cell_id] = m_primal_node_to_dual_cell_map[i];

        const OriginNodeId origin_node_id = static_cast<typename OriginNodeId::base_type>(primal_node_id);

        dual_cell_value[dual_cell_id] = primal_node_value[origin_node_id];
      });
  }

  template <typename OriginDataType, typename DestinationDataType, ItemType destination_node_type>
  void
  fromDualCell(const CellValue<OriginDataType>& dual_cell_value,
               const ItemValue<DestinationDataType, destination_node_type>& primal_node_value) const
  {
    static_assert(is_node_in_1d_v<destination_node_type>, "invalid destination node type");

    static_assert(not std::is_const_v<DestinationDataType>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType>, "incompatible types");

    using DestinationNodeId = ItemIdT<destination_node_type>;

    Assert(m_primal_connectivity == primal_node_value.connectivity_ptr().get(),
           "unexpected connectivity for primal NodeValue");
    Assert(m_dual_connectivity == dual_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for dual CellValue");

    parallel_for(
      m_primal_node_to_dual_cell_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_node_id, dual_cell_id] = m_primal_node_to_dual_cell_map[i];

        const DestinationNodeId destination_node_id =
          static_cast<typename DestinationNodeId::base_type>(primal_node_id);

        primal_node_value[destination_node_id] = dual_cell_value[dual_cell_id];
      });
  }

  PrimalToDual1DConnectivityDataMapper(const Connectivity<1>& primal_connectivity,
                                       const Connectivity<1>& dual_connectivity,
                                       const ConstNodeIdToNodeIdMap& primal_node_to_dual_node_map,
                                       const ConstCellIdToNodeIdMap& primal_cell_to_dual_node_map,
                                       const ConstNodeIdToCellIdMap& primal_node_to_dual_cell_map)
    : m_primal_connectivity{&primal_connectivity},
      m_dual_connectivity{&dual_connectivity},
      m_primal_node_to_dual_node_map{primal_node_to_dual_node_map},
      m_primal_cell_to_dual_node_map{primal_cell_to_dual_node_map},
      m_primal_node_to_dual_cell_map{primal_node_to_dual_cell_map}
  {}
};

#endif   // PRIMAL_TO_DUAL_1D_CONNECTIVITY_DATA_MAPPER_HPP
