#include <mesh/MeshUtils.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshVariant.hpp>

bool
checkConnectivityOrdering(const std::shared_ptr<const MeshVariant>& mesh_v)
{
  return std::visit([](auto&& mesh) { return checkConnectivityOrdering(mesh->connectivity()); }, mesh_v->variant());
}
