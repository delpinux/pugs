#ifndef MESH_BUILDER_BASE_HPP
#define MESH_BUILDER_BASE_HPP

class MeshVariant;
class ConnectivityDispatcherVariant;

#include <memory>

class MeshBuilderBase
{
 public:
  enum class DispatchType : uint8_t
  {
    initial,
    balance
  };

 protected:
  std::shared_ptr<const MeshVariant> m_mesh;
  std::shared_ptr<const ConnectivityDispatcherVariant> m_connectivity_dispatcher;

  template <size_t Dimension>
  void _dispatch(const DispatchType balance);

  template <size_t Dimension>
  void _checkMesh() const;

 public:
  std::shared_ptr<const ConnectivityDispatcherVariant>
  connectivityDispatcher() const
  {
    return m_connectivity_dispatcher;
  }

  std::shared_ptr<const MeshVariant>
  mesh() const
  {
    return m_mesh;
  }

  MeshBuilderBase()          = default;
  virtual ~MeshBuilderBase() = default;
};

#endif   // MESH_BUILDER_BASE_HPP
