#ifndef MESH_HPP
#define MESH_HPP

#include <algebra/TinyVector.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemValue.hpp>
#include <mesh/MeshVariant.hpp>
#include <utils/GlobalVariableManager.hpp>

#include <memory>

template <size_t MeshDimension>
class Mesh : public std::enable_shared_from_this<Mesh<MeshDimension>>
{
 public:
  static constexpr size_t Dimension = MeshDimension;
  using Connectivity                = ::Connectivity<Dimension>;

  using Rd = TinyVector<Dimension>;

 private:
  const size_t m_id;

  const std::shared_ptr<const Connectivity> m_connectivity;
  const NodeValue<const Rd> m_xr;

  std::ostream&
  _write(std::ostream& os) const
  {
    return os << *m_connectivity;
  }

 public:
  friend std::ostream&
  operator<<(std::ostream& os, const Mesh& mesh)
  {
    return mesh._write(os);
  }

  PUGS_INLINE
  size_t
  id() const
  {
    return m_id;
  }

  PUGS_INLINE
  std::shared_ptr<const Mesh<Dimension>>
  shared_ptr() const
  {
    return this->shared_from_this();
  }

  PUGS_INLINE
  std::shared_ptr<const MeshVariant>
  meshVariant() const
  {
    return std::make_shared<const MeshVariant>(this->shared_ptr());
  }

  PUGS_INLINE
  constexpr size_t
  dimension() const
  {
    return Dimension;
  }

  PUGS_INLINE
  const Connectivity&
  connectivity() const
  {
    return *m_connectivity;
  }

  PUGS_INLINE
  const auto&
  shared_connectivity() const
  {
    return m_connectivity;
  }

  PUGS_INLINE
  size_t
  numberOfNodes() const
  {
    return m_connectivity->numberOfNodes();
  }

  PUGS_INLINE
  size_t
  numberOfEdges() const
  {
    return m_connectivity->numberOfEdges();
  }

  PUGS_INLINE
  size_t
  numberOfFaces() const
  {
    return m_connectivity->numberOfFaces();
  }

  PUGS_INLINE
  size_t
  numberOfCells() const
  {
    return m_connectivity->numberOfCells();
  }

  template <ItemType item_type>
  PUGS_INLINE size_t
  numberOf() const
  {
    return m_connectivity->template numberOf<item_type>();
  }

  PUGS_INLINE
  const NodeValue<const Rd>&
  xr() const
  {
    return m_xr;
  }

  Mesh() = delete;

  PUGS_INLINE
  Mesh(const std::shared_ptr<const Connectivity>& connectivity, const NodeValue<const Rd>& xr)
    : m_id{GlobalVariableManager::instance().getAndIncrementMeshId()}, m_connectivity{connectivity}, m_xr{xr}
  {
    ;
  }

  Mesh(const Mesh&) = delete;
  Mesh(Mesh&&)      = delete;

  ~Mesh();
};

#endif   // MESH_HPP
