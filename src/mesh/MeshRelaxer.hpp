#ifndef MESH_RELAXER_HPP
#define MESH_RELAXER_HPP

#include <mesh/MeshTraits.hpp>

class MeshVariant;

#include <memory>

class MeshRelaxer
{
 private:
  template <MeshConcept MeshType>
  std::shared_ptr<const MeshVariant> _relax(const MeshType& source_mesh,
                                            const MeshType& destination_mesh,
                                            const double& theta) const;

 public:
  std::shared_ptr<const MeshVariant> relax(const std::shared_ptr<const MeshVariant>& p_source_mesh,
                                           const std::shared_ptr<const MeshVariant>& p_destination_mesh,
                                           const double& theta) const;
  MeshRelaxer()  = default;
  ~MeshRelaxer() = default;
};

#endif   // MESH_RELAXER_HPP
