#ifndef REF_ITEM_LIST_HPP
#define REF_ITEM_LIST_HPP

#include <utils/Array.hpp>

#include <mesh/ItemId.hpp>
#include <mesh/RefId.hpp>

class RefItemListBase
{
 public:
  enum class Type
  {
    boundary,
    interface,
    set,
    undefined
  };

 protected:
  Type m_type;

  RefItemListBase(Type type) : m_type{type} {}

  RefItemListBase() : m_type{Type::undefined} {}

  RefItemListBase(const RefItemListBase&) = default;
  RefItemListBase(RefItemListBase&&)      = default;

  RefItemListBase& operator=(const RefItemListBase&) = default;
  RefItemListBase& operator=(RefItemListBase&&) = default;

 public:
  PUGS_INLINE Type
  type() const
  {
    return m_type;
  }

  virtual ~RefItemListBase() = default;
};

template <ItemType item_type>
class RefItemList final : public RefItemListBase
{
 public:
  using ItemId = ItemIdT<item_type>;

 private:
  RefId m_ref_id;
  Array<const ItemId> m_item_id_list;

 public:
  const RefId&
  refId() const
  {
    return m_ref_id;
  }

  const Array<const ItemId>&
  list() const
  {
    return m_item_id_list;
  }

  RefItemList(const RefId& ref_id, const Array<const ItemId>& item_id_list, Type type)
    : RefItemListBase{type}, m_ref_id{ref_id}, m_item_id_list{item_id_list}
  {
    ;
  }

  RefItemList& operator=(const RefItemList&) = default;
  RefItemList& operator=(RefItemList&&) = default;

  RefItemList()                   = default;
  RefItemList(const RefItemList&) = default;
  RefItemList(RefItemList&&)      = default;
  ~RefItemList()                  = default;
};

using RefNodeList = RefItemList<ItemType::node>;
using RefEdgeList = RefItemList<ItemType::edge>;
using RefFaceList = RefItemList<ItemType::face>;
using RefCellList = RefItemList<ItemType::cell>;

#endif   // REF_ITEM_LIST_HPP
