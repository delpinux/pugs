#ifndef SUB_ITEM_ARRAY_PER_ITEM_VARIANT_HPP
#define SUB_ITEM_ARRAY_PER_ITEM_VARIANT_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <mesh/SubItemArrayPerItem.hpp>
#include <utils/Exceptions.hpp>

class SubItemArrayPerItemVariant
{
 private:
  using Variant = std::variant<NodeArrayPerEdge<const bool>,
                               NodeArrayPerEdge<const long int>,
                               NodeArrayPerEdge<const unsigned long int>,
                               NodeArrayPerEdge<const double>,
                               NodeArrayPerEdge<const TinyVector<1, double>>,
                               NodeArrayPerEdge<const TinyVector<2, double>>,
                               NodeArrayPerEdge<const TinyVector<3, double>>,
                               NodeArrayPerEdge<const TinyMatrix<1, 1, double>>,
                               NodeArrayPerEdge<const TinyMatrix<2, 2, double>>,
                               NodeArrayPerEdge<const TinyMatrix<3, 3, double>>,

                               NodeArrayPerFace<const bool>,
                               NodeArrayPerFace<const long int>,
                               NodeArrayPerFace<const unsigned long int>,
                               NodeArrayPerFace<const double>,
                               NodeArrayPerFace<const TinyVector<1, double>>,
                               NodeArrayPerFace<const TinyVector<2, double>>,
                               NodeArrayPerFace<const TinyVector<3, double>>,
                               NodeArrayPerFace<const TinyMatrix<1, 1, double>>,
                               NodeArrayPerFace<const TinyMatrix<2, 2, double>>,
                               NodeArrayPerFace<const TinyMatrix<3, 3, double>>,

                               NodeArrayPerCell<const bool>,
                               NodeArrayPerCell<const long int>,
                               NodeArrayPerCell<const unsigned long int>,
                               NodeArrayPerCell<const double>,
                               NodeArrayPerCell<const TinyVector<1, double>>,
                               NodeArrayPerCell<const TinyVector<2, double>>,
                               NodeArrayPerCell<const TinyVector<3, double>>,
                               NodeArrayPerCell<const TinyMatrix<1, 1, double>>,
                               NodeArrayPerCell<const TinyMatrix<2, 2, double>>,
                               NodeArrayPerCell<const TinyMatrix<3, 3, double>>,

                               EdgeArrayPerNode<const bool>,
                               EdgeArrayPerNode<const long int>,
                               EdgeArrayPerNode<const unsigned long int>,
                               EdgeArrayPerNode<const double>,
                               EdgeArrayPerNode<const TinyVector<1, double>>,
                               EdgeArrayPerNode<const TinyVector<2, double>>,
                               EdgeArrayPerNode<const TinyVector<3, double>>,
                               EdgeArrayPerNode<const TinyMatrix<1, 1, double>>,
                               EdgeArrayPerNode<const TinyMatrix<2, 2, double>>,
                               EdgeArrayPerNode<const TinyMatrix<3, 3, double>>,

                               EdgeArrayPerFace<const bool>,
                               EdgeArrayPerFace<const long int>,
                               EdgeArrayPerFace<const unsigned long int>,
                               EdgeArrayPerFace<const double>,
                               EdgeArrayPerFace<const TinyVector<1, double>>,
                               EdgeArrayPerFace<const TinyVector<2, double>>,
                               EdgeArrayPerFace<const TinyVector<3, double>>,
                               EdgeArrayPerFace<const TinyMatrix<1, 1, double>>,
                               EdgeArrayPerFace<const TinyMatrix<2, 2, double>>,
                               EdgeArrayPerFace<const TinyMatrix<3, 3, double>>,

                               EdgeArrayPerCell<const bool>,
                               EdgeArrayPerCell<const long int>,
                               EdgeArrayPerCell<const unsigned long int>,
                               EdgeArrayPerCell<const double>,
                               EdgeArrayPerCell<const TinyVector<1, double>>,
                               EdgeArrayPerCell<const TinyVector<2, double>>,
                               EdgeArrayPerCell<const TinyVector<3, double>>,
                               EdgeArrayPerCell<const TinyMatrix<1, 1, double>>,
                               EdgeArrayPerCell<const TinyMatrix<2, 2, double>>,
                               EdgeArrayPerCell<const TinyMatrix<3, 3, double>>,

                               FaceArrayPerNode<const bool>,
                               FaceArrayPerNode<const long int>,
                               FaceArrayPerNode<const unsigned long int>,
                               FaceArrayPerNode<const double>,
                               FaceArrayPerNode<const TinyVector<1, double>>,
                               FaceArrayPerNode<const TinyVector<2, double>>,
                               FaceArrayPerNode<const TinyVector<3, double>>,
                               FaceArrayPerNode<const TinyMatrix<1, 1, double>>,
                               FaceArrayPerNode<const TinyMatrix<2, 2, double>>,
                               FaceArrayPerNode<const TinyMatrix<3, 3, double>>,

                               FaceArrayPerEdge<const bool>,
                               FaceArrayPerEdge<const long int>,
                               FaceArrayPerEdge<const unsigned long int>,
                               FaceArrayPerEdge<const double>,
                               FaceArrayPerEdge<const TinyVector<1, double>>,
                               FaceArrayPerEdge<const TinyVector<2, double>>,
                               FaceArrayPerEdge<const TinyVector<3, double>>,
                               FaceArrayPerEdge<const TinyMatrix<1, 1, double>>,
                               FaceArrayPerEdge<const TinyMatrix<2, 2, double>>,
                               FaceArrayPerEdge<const TinyMatrix<3, 3, double>>,

                               FaceArrayPerCell<const bool>,
                               FaceArrayPerCell<const long int>,
                               FaceArrayPerCell<const unsigned long int>,
                               FaceArrayPerCell<const double>,
                               FaceArrayPerCell<const TinyVector<1, double>>,
                               FaceArrayPerCell<const TinyVector<2, double>>,
                               FaceArrayPerCell<const TinyVector<3, double>>,
                               FaceArrayPerCell<const TinyMatrix<1, 1, double>>,
                               FaceArrayPerCell<const TinyMatrix<2, 2, double>>,
                               FaceArrayPerCell<const TinyMatrix<3, 3, double>>,

                               CellArrayPerNode<const bool>,
                               CellArrayPerNode<const long int>,
                               CellArrayPerNode<const unsigned long int>,
                               CellArrayPerNode<const double>,
                               CellArrayPerNode<const TinyVector<1, double>>,
                               CellArrayPerNode<const TinyVector<2, double>>,
                               CellArrayPerNode<const TinyVector<3, double>>,
                               CellArrayPerNode<const TinyMatrix<1, 1, double>>,
                               CellArrayPerNode<const TinyMatrix<2, 2, double>>,
                               CellArrayPerNode<const TinyMatrix<3, 3, double>>,

                               CellArrayPerEdge<const bool>,
                               CellArrayPerEdge<const long int>,
                               CellArrayPerEdge<const unsigned long int>,
                               CellArrayPerEdge<const double>,
                               CellArrayPerEdge<const TinyVector<1, double>>,
                               CellArrayPerEdge<const TinyVector<2, double>>,
                               CellArrayPerEdge<const TinyVector<3, double>>,
                               CellArrayPerEdge<const TinyMatrix<1, 1, double>>,
                               CellArrayPerEdge<const TinyMatrix<2, 2, double>>,
                               CellArrayPerEdge<const TinyMatrix<3, 3, double>>,

                               CellArrayPerFace<const bool>,
                               CellArrayPerFace<const long int>,
                               CellArrayPerFace<const unsigned long int>,
                               CellArrayPerFace<const double>,
                               CellArrayPerFace<const TinyVector<1, double>>,
                               CellArrayPerFace<const TinyVector<2, double>>,
                               CellArrayPerFace<const TinyVector<3, double>>,
                               CellArrayPerFace<const TinyMatrix<1, 1, double>>,
                               CellArrayPerFace<const TinyMatrix<2, 2, double>>,
                               CellArrayPerFace<const TinyMatrix<3, 3, double>>>;

  Variant m_sub_item_array_per_item;

 public:
  PUGS_INLINE
  const Variant&
  subItemArrayPerItem() const
  {
    return m_sub_item_array_per_item;
  }

  template <typename SubItemArrayPerItemT>
  PUGS_INLINE auto
  get() const
  {
    using DataType        = typename SubItemArrayPerItemT::data_type;
    using ItemOfItemTypeT = typename SubItemArrayPerItemT::ItemOfItemType;

    if constexpr (std::is_same_v<SubItemArrayPerItemT, SubItemArrayPerItem<DataType, ItemOfItemTypeT>> or
                  std::is_same_v<SubItemArrayPerItemT, SubItemArrayPerItem<const DataType, ItemOfItemTypeT>> or
                  std::is_same_v<SubItemArrayPerItemT, WeakSubItemArrayPerItem<DataType, ItemOfItemTypeT>> or
                  std::is_same_v<SubItemArrayPerItemT, WeakSubItemArrayPerItem<const DataType, ItemOfItemTypeT>>) {
      if (not std::holds_alternative<SubItemArrayPerItem<const DataType, ItemOfItemTypeT>>(
            this->m_sub_item_array_per_item)) {
        throw NormalError("invalid SubItemArrayPerItem type");
      }
      return std::get<SubItemArrayPerItem<const DataType, ItemOfItemTypeT>>(this->m_sub_item_array_per_item);
    } else {
      static_assert(std::is_same_v<SubItemArrayPerItemT, SubItemArrayPerItemT>, "invalid template argument");
    }
  }

  template <typename DataType, typename ItemOfItemTypeT>
  SubItemArrayPerItemVariant(const SubItemArrayPerItem<DataType, ItemOfItemTypeT>& sub_item_array_per_item)
    : m_sub_item_array_per_item{SubItemArrayPerItem<const DataType, ItemOfItemTypeT>{sub_item_array_per_item}}
  {
    static_assert(std::is_same_v<std::remove_const_t<DataType>, bool> or                         //
                    std::is_same_v<std::remove_const_t<DataType>, long int> or                   //
                    std::is_same_v<std::remove_const_t<DataType>, unsigned long int> or          //
                    std::is_same_v<std::remove_const_t<DataType>, double> or                     //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<1, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<2, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<3, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<1, 1, double>> or   //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<2, 2, double>> or   //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<3, 3, double>>,
                  "SubItemArrayPerItem with this DataType is not allowed in variant");
  }

  SubItemArrayPerItemVariant& operator=(SubItemArrayPerItemVariant&&)      = default;
  SubItemArrayPerItemVariant& operator=(const SubItemArrayPerItemVariant&) = default;

  SubItemArrayPerItemVariant(const SubItemArrayPerItemVariant&) = default;
  SubItemArrayPerItemVariant(SubItemArrayPerItemVariant&&)      = default;

  SubItemArrayPerItemVariant()  = delete;
  ~SubItemArrayPerItemVariant() = default;
};

#endif   // SUB_ITEM_ARRAY_PER_ITEM_VARIANT_HPP
