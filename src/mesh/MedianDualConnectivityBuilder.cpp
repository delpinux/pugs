#include <mesh/MedianDualConnectivityBuilder.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityDescriptor.hpp>
#include <mesh/ConnectivityDispatcher.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/PrimalToMedianDualConnectivityDataMapper.hpp>
#include <mesh/RefId.hpp>
#include <utils/Array.hpp>
#include <utils/Messenger.hpp>
#include <utils/Stringify.hpp>

#include <vector>

template <>
void
MedianDualConnectivityBuilder::_buildConnectivityDescriptor<2>(const Connectivity<2>& primal_connectivity,
                                                               ConnectivityDescriptor& dual_descriptor)
{
  const size_t primal_number_of_nodes = primal_connectivity.numberOfNodes();
  const size_t primal_number_of_faces = primal_connectivity.numberOfFaces();
  const size_t primal_number_of_cells = primal_connectivity.numberOfCells();

  const auto& primal_node_number = primal_connectivity.nodeNumber();
  const auto& primal_face_number = primal_connectivity.faceNumber();
  const auto& primal_cell_number = primal_connectivity.cellNumber();

  const size_t primal_number_of_boundary_faces = [&] {
    size_t number_of_boundary_faces        = 0;
    const auto& primal_face_is_owned       = primal_connectivity.faceIsOwned();
    const auto& primal_face_to_cell_matrix = primal_connectivity.faceToCellMatrix();

    parallel_reduce(
      primal_number_of_faces,
      PUGS_LAMBDA(const FaceId face_id, size_t& nb_of_boundary_faces) {
        nb_of_boundary_faces += (primal_face_is_owned[face_id] and (primal_face_to_cell_matrix[face_id].size() == 1));
      },
      number_of_boundary_faces);
    return number_of_boundary_faces;
  }();

  const size_t primal_number_of_boundary_nodes = primal_number_of_boundary_faces;

  const size_t dual_number_of_nodes = primal_number_of_cells + primal_number_of_faces + primal_number_of_boundary_nodes;
  const size_t dual_number_of_cells = primal_number_of_nodes;

  {
    m_primal_node_to_dual_cell_map = NodeIdToCellIdMap{primal_number_of_nodes};
    CellId dual_cell_id            = 0;
    for (NodeId primal_node_id = 0; primal_node_id < primal_number_of_nodes; ++primal_node_id) {
      m_primal_node_to_dual_cell_map[primal_node_id] = std::make_pair(primal_node_id, dual_cell_id++);
    }
  }

  NodeValue<NodeId> node_to_dual_node_correpondance{primal_connectivity};
  node_to_dual_node_correpondance.fill(std::numeric_limits<NodeId>::max());

  {
    NodeId dual_node_id = 0;

    m_primal_cell_to_dual_node_map = CellIdToNodeIdMap{primal_number_of_cells};
    for (CellId primal_cell_id = 0; primal_cell_id < primal_number_of_cells; ++primal_cell_id) {
      m_primal_cell_to_dual_node_map[primal_cell_id] = std::make_pair(primal_cell_id, dual_node_id++);
    }

    m_primal_face_to_dual_node_map = FaceIdToNodeIdMap{primal_number_of_faces};
    for (FaceId primal_face_id = 0; primal_face_id < primal_number_of_faces; ++primal_face_id) {
      m_primal_face_to_dual_node_map[primal_face_id] = std::make_pair(primal_face_id, dual_node_id++);
    }

    const auto& primal_face_is_owned       = primal_connectivity.faceIsOwned();
    const auto& primal_face_to_cell_matrix = primal_connectivity.faceToCellMatrix();
    const auto& primal_face_to_node_matrix = primal_connectivity.faceToNodeMatrix();

    m_primal_boundary_node_to_dual_node_map = NodeIdToNodeIdMap{primal_number_of_boundary_nodes};
    m_primal_boundary_node_to_dual_node_map.fill(std::make_pair(1234, 5678));
    size_t i_boundary_node = 0;
    for (FaceId primal_face_id = 0; primal_face_id < primal_number_of_faces; ++primal_face_id) {
      if (primal_face_is_owned[primal_face_id] and (primal_face_to_cell_matrix[primal_face_id].size() == 1)) {
        const auto& primal_face_to_node_list = primal_face_to_node_matrix[primal_face_id];
        for (size_t i_face_node = 0; i_face_node < primal_face_to_node_list.size(); ++i_face_node) {
          const NodeId node_id = primal_face_to_node_list[i_face_node];
          if (node_to_dual_node_correpondance[node_id] == std::numeric_limits<NodeId>::max()) {
            node_to_dual_node_correpondance[node_id]                   = dual_node_id;
            m_primal_boundary_node_to_dual_node_map[i_boundary_node++] = std::make_pair(node_id, dual_node_id++);
          }
        }
      }
    }
    Assert(i_boundary_node == primal_number_of_boundary_nodes);
  }

  Array<int> node_number_vector(dual_number_of_nodes);
  {
    parallel_for(m_primal_cell_to_dual_node_map.size(), [&](size_t i) {
      const auto [primal_cell_id, dual_node_id] = m_primal_cell_to_dual_node_map[i];
      node_number_vector[dual_node_id]          = primal_cell_number[primal_cell_id];
    });

    const size_t face_number_shift = max(primal_cell_number) + 1;
    parallel_for(primal_number_of_faces, [&](size_t i) {
      const auto [primal_face_id, dual_node_id] = m_primal_face_to_dual_node_map[i];
      node_number_vector[dual_node_id]          = primal_face_number[primal_face_id] + face_number_shift;
    });

    const size_t node_number_shift = face_number_shift + max(primal_face_number) + 1;
    parallel_for(m_primal_boundary_node_to_dual_node_map.size(), [&](size_t i) {
      const auto [primal_node_id, dual_node_id] = m_primal_boundary_node_to_dual_node_map[i];
      node_number_vector[dual_node_id]          = primal_node_number[primal_node_id] + node_number_shift;
    });
  }
  dual_descriptor.setNodeNumberVector(node_number_vector);

  {
    Array<int> cell_number_vector(dual_number_of_cells);
    parallel_for(dual_number_of_cells, [&](size_t i) {
      const auto [primal_node_id, dual_cell_id] = m_primal_node_to_dual_cell_map[i];
      cell_number_vector[dual_cell_id]          = primal_node_number[primal_node_id];
    });
    dual_descriptor.setCellNumberVector(cell_number_vector);
  }

  Array<CellType> cell_type_vector(dual_number_of_cells);

  const auto& primal_node_to_cell_matrix = primal_connectivity.nodeToCellMatrix();

  parallel_for(primal_number_of_nodes, [&](NodeId node_id) {
    const size_t i_dual_cell          = node_id;
    const auto& primal_node_cell_list = primal_node_to_cell_matrix[node_id];

    if (primal_node_cell_list.size() == 1) {
      cell_type_vector[i_dual_cell] = CellType::Quadrangle;
    } else {
      cell_type_vector[i_dual_cell] = CellType::Polygon;
    }
  });
  dual_descriptor.setCellTypeVector(cell_type_vector);

  const auto& primal_cell_to_face_matrix               = primal_connectivity.cellToFaceMatrix();
  const auto& primal_node_to_face_matrix               = primal_connectivity.nodeToFaceMatrix();
  const auto& primal_face_to_cell_matrix               = primal_connectivity.faceToCellMatrix();
  const auto& primal_face_to_node_matrix               = primal_connectivity.faceToNodeMatrix();
  const auto& primal_cell_face_is_reversed             = primal_connectivity.cellFaceIsReversed();
  const auto& primal_face_local_numbers_in_their_cells = primal_connectivity.faceLocalNumbersInTheirCells();

  auto next_face = [&](const CellId cell_id, const FaceId face_id, const NodeId node_id) -> FaceId {
    const auto& primal_cell_to_face_list = primal_cell_to_face_matrix[cell_id];
    for (size_t i_face = 0; i_face < primal_cell_to_face_list.size(); ++i_face) {
      const FaceId cell_face_id = primal_cell_to_face_list[i_face];
      if (cell_face_id != face_id) {
        const auto& face_node_list = primal_face_to_node_matrix[cell_face_id];
        if ((face_node_list[0] == node_id) or (face_node_list[1] == node_id)) {
          return cell_face_id;
        }
      }
    }
    // LCOV_EXCL_START
    throw UnexpectedError("could not find next face");
    // LCOV_EXCL_STOP
  };

  auto next_cell = [&](const CellId cell_id, const FaceId face_id) -> CellId {
    const auto& primal_face_to_cell_list = primal_face_to_cell_matrix[face_id];
    for (size_t i_cell = 0; i_cell < primal_face_to_cell_list.size(); ++i_cell) {
      const CellId face_cell_id = primal_face_to_cell_list[i_cell];
      if (face_cell_id != cell_id) {
        return face_cell_id;
      }
    }
    // LCOV_EXCL_START
    throw UnexpectedError("could not find next face");
    // LCOV_EXCL_STOP
  };

  Array<unsigned int> cell_to_node_row(dual_number_of_cells + 1);
  cell_to_node_row[0] = 0;
  {
    for (NodeId node_id = 0; node_id < primal_number_of_nodes; ++node_id) {
      // const size_t i_dual_cell             = node_id;
      const auto& primal_node_to_cell_list = primal_node_to_cell_matrix[node_id];
      const auto& primal_node_to_face_list = primal_node_to_face_matrix[node_id];

      if (primal_node_to_cell_list.size() != primal_node_to_face_list.size()) {
        // boundary cell
        cell_to_node_row[node_id + 1] =
          cell_to_node_row[node_id] + 1 + primal_node_to_cell_list.size() + primal_node_to_face_list.size();
      } else {
        // inner cell
        cell_to_node_row[node_id + 1] =
          cell_to_node_row[node_id] + primal_node_to_cell_list.size() + primal_node_to_face_list.size();
      }
    }
  }

  Array<unsigned int> cell_to_node_list(cell_to_node_row[cell_to_node_row.size() - 1]);
  parallel_for(primal_number_of_nodes, [&](NodeId node_id) {
    const auto& primal_node_to_cell_list = primal_node_to_cell_matrix[node_id];
    const auto& primal_node_to_face_list = primal_node_to_face_matrix[node_id];

    size_t i_dual_cell_node = cell_to_node_row[node_id];

    if (primal_node_to_cell_list.size() != primal_node_to_face_list.size()) {
      // boundary cell

      auto [face_id, cell_id] = [&]() -> std::pair<FaceId, CellId> {
        for (size_t i_face = 0; i_face < primal_node_to_face_list.size(); ++i_face) {
          const FaceId primal_face_id = primal_node_to_face_list[i_face];
          if (primal_face_to_cell_matrix[primal_face_id].size() > 1) {
            continue;
          }

          const CellId primal_cell_id = primal_face_to_cell_matrix[primal_face_id][0];
          const size_t i_face_in_cell = primal_face_local_numbers_in_their_cells(primal_face_id, 0);

          if (primal_face_to_node_matrix[primal_face_id]
                                        [primal_cell_face_is_reversed(primal_cell_id, i_face_in_cell)] == node_id) {
            return std::make_pair(primal_face_id, primal_cell_id);
          }
        }
        // LCOV_EXCL_START
        throw UnexpectedError("cannot find first face");
        // LCOV_EXCL_STOP
      }();

      cell_to_node_list[i_dual_cell_node++] = m_primal_face_to_dual_node_map[face_id].second;
      cell_to_node_list[i_dual_cell_node++] = m_primal_cell_to_dual_node_map[cell_id].second;

      face_id = next_face(cell_id, face_id, node_id);

      while (primal_face_to_cell_matrix[face_id].size() > 1) {
        cell_to_node_list[i_dual_cell_node++] = m_primal_face_to_dual_node_map[face_id].second;

        cell_id                               = next_cell(cell_id, face_id);
        cell_to_node_list[i_dual_cell_node++] = m_primal_cell_to_dual_node_map[cell_id].second;

        face_id = next_face(cell_id, face_id, node_id);
      }
      cell_to_node_list[i_dual_cell_node++] = m_primal_face_to_dual_node_map[face_id].second;
      cell_to_node_list[i_dual_cell_node++] = node_to_dual_node_correpondance[node_id];

    } else {
      // inner cell

      auto [face_id, cell_id] = [&]() -> std::pair<FaceId, CellId> {
        const FaceId primal_face_id = primal_node_to_face_list[0];

        for (size_t i_face_cell = 0; i_face_cell < primal_face_to_cell_matrix[primal_face_id].size(); ++i_face_cell) {
          const CellId primal_cell_id = primal_face_to_cell_matrix[primal_face_id][i_face_cell];
          const size_t i_face_in_cell = primal_face_local_numbers_in_their_cells(primal_face_id, i_face_cell);

          if (primal_face_to_node_matrix[primal_face_id]
                                        [primal_cell_face_is_reversed(primal_cell_id, i_face_in_cell)] == node_id) {
            return std::make_pair(primal_face_id, primal_cell_id);
          }
        }
        // LCOV_EXCL_START
        throw UnexpectedError("could not find first face/cell couple");
        // LCOV_EXCL_STOP
      }();

      const FaceId first_face_id = face_id;
      do {
        cell_to_node_list[i_dual_cell_node++] = m_primal_face_to_dual_node_map[face_id].second;
        cell_to_node_list[i_dual_cell_node++] = m_primal_cell_to_dual_node_map[cell_id].second;

        face_id = next_face(cell_id, face_id, node_id);
        cell_id = next_cell(cell_id, face_id);
      } while (face_id != first_face_id);
    }
  });

  dual_descriptor.setCellToNodeMatrix(ConnectivityMatrix(cell_to_node_row, cell_to_node_list));
}

template <>
void
MedianDualConnectivityBuilder::_buildConnectivityFrom<2>(const IConnectivity& i_primal_connectivity)
{
  using ConnectivityType = Connectivity<2>;

  const ConnectivityType& primal_connectivity = dynamic_cast<const ConnectivityType&>(i_primal_connectivity);

  ConnectivityDescriptor dual_descriptor;

  this->_buildConnectivityDescriptor(primal_connectivity, dual_descriptor);

  ConnectivityBuilderBase::_computeCellFaceAndFaceNodeConnectivities<2>(dual_descriptor);

  {
    const std::unordered_map<unsigned int, NodeId> primal_boundary_node_id_to_dual_node_id_map = [&] {
      std::unordered_map<unsigned int, NodeId> node_to_id_map;
      for (size_t i_node = 0; i_node < m_primal_boundary_node_to_dual_node_map.size(); ++i_node) {
        auto [primal_node_id, dual_node_id] = m_primal_boundary_node_to_dual_node_map[i_node];
        node_to_id_map[primal_node_id]      = dual_node_id;
      }
      return node_to_id_map;
    }();

    for (size_t i_node_list = 0; i_node_list < primal_connectivity.template numberOfRefItemList<ItemType::node>();
         ++i_node_list) {
      const auto& primal_ref_node_list = primal_connectivity.template refItemList<ItemType::node>(i_node_list);
      const auto& primal_node_list     = primal_ref_node_list.list();

      const std::vector<NodeId> dual_node_list = [&]() {
        std::vector<NodeId> tmp_dual_node_list;

        for (size_t i_primal_node = 0; i_primal_node < primal_node_list.size(); ++i_primal_node) {
          auto primal_node_id = primal_node_list[i_primal_node];
          const auto i_dual_node =
            primal_boundary_node_id_to_dual_node_id_map.find(primal_connectivity.nodeNumber()[primal_node_id]);
          if (i_dual_node != primal_boundary_node_id_to_dual_node_id_map.end()) {
            tmp_dual_node_list.push_back(i_dual_node->second);
          }
        }

        return tmp_dual_node_list;
      }();

      if (parallel::allReduceOr(dual_node_list.size() > 0)) {
        auto dual_node_array = convert_to_array(dual_node_list);
        dual_descriptor.addRefItemList(
          RefNodeList{primal_ref_node_list.refId(), dual_node_array, primal_ref_node_list.type()});
      }
    }
  }

  for (size_t i_face_list = 0; i_face_list < primal_connectivity.template numberOfRefItemList<ItemType::face>();
       ++i_face_list) {
    const auto& primal_ref_face_list = primal_connectivity.template refItemList<ItemType::face>(i_face_list);
    const auto& primal_face_list     = primal_ref_face_list.list();

    const std::vector<FaceId> boundary_dual_face_id_list = [&]() {
      std::vector<NodeId> bounday_face_dual_node_id_list(primal_face_list.size());
      for (size_t i_face = 0; i_face < primal_face_list.size(); ++i_face) {
        bounday_face_dual_node_id_list[i_face] = m_primal_face_to_dual_node_map[primal_face_list[i_face]].second;
      }

      std::vector<bool> is_dual_node_from_boundary_face(dual_descriptor.nodeNumberVector().size(), false);
      for (size_t i_face = 0; i_face < bounday_face_dual_node_id_list.size(); ++i_face) {
        is_dual_node_from_boundary_face[bounday_face_dual_node_id_list[i_face]] = true;
      }

      std::vector<bool> is_dual_node_from_boundary_node(dual_descriptor.nodeNumberVector().size(), false);
      for (size_t i_node = 0; i_node < m_primal_boundary_node_to_dual_node_map.size(); ++i_node) {
        is_dual_node_from_boundary_node[m_primal_boundary_node_to_dual_node_map[i_node].second] = true;
      }

      const auto& dual_face_to_node_matrix = dual_descriptor.faceToNodeMatrix();

      std::vector<FaceId> dual_face_list;
      dual_face_list.reserve(2 * primal_face_list.size());
      for (size_t i_dual_face = 0; i_dual_face < dual_face_to_node_matrix.numberOfRows(); ++i_dual_face) {
        const NodeId dual_node_0 = dual_face_to_node_matrix[i_dual_face][0];
        const NodeId dual_node_1 = dual_face_to_node_matrix[i_dual_face][1];

        if ((is_dual_node_from_boundary_face[dual_node_0] and is_dual_node_from_boundary_node[dual_node_1]) or
            (is_dual_node_from_boundary_node[dual_node_0] and is_dual_node_from_boundary_face[dual_node_1])) {
          dual_face_list.push_back(i_dual_face);
        }
      }
      return dual_face_list;
    }();

    if (parallel::allReduceOr(boundary_dual_face_id_list.size() > 0)) {
      dual_descriptor.addRefItemList(RefFaceList{primal_ref_face_list.refId(),
                                                 convert_to_array(boundary_dual_face_id_list),
                                                 primal_ref_face_list.type()});
    }
  }

  const auto& primal_node_owner = primal_connectivity.nodeOwner();

  dual_descriptor.setNodeOwnerVector([&] {
    Array<int> node_owner_vector(dual_descriptor.nodeNumberVector().size());

    node_owner_vector.fill(-1);
    for (size_t i_primal_node_to_dual_node = 0;
         i_primal_node_to_dual_node < m_primal_boundary_node_to_dual_node_map.size(); ++i_primal_node_to_dual_node) {
      const auto& [primal_node_id, dual_node_id] = m_primal_boundary_node_to_dual_node_map[i_primal_node_to_dual_node];
      node_owner_vector[dual_node_id]            = primal_node_owner[primal_node_id];
    }

    const auto& primal_face_owner = primal_connectivity.faceOwner();
    for (size_t i_primal_face_to_dual_node = 0; i_primal_face_to_dual_node < m_primal_face_to_dual_node_map.size();
         ++i_primal_face_to_dual_node) {
      const auto& [primal_face_id, dual_node_id] = m_primal_face_to_dual_node_map[i_primal_face_to_dual_node];
      node_owner_vector[dual_node_id]            = primal_face_owner[primal_face_id];
    }

    const auto& primal_cell_owner = primal_connectivity.cellOwner();
    for (size_t i_primal_cell_to_dual_node = 0; i_primal_cell_to_dual_node < m_primal_cell_to_dual_node_map.size();
         ++i_primal_cell_to_dual_node) {
      const auto& [primal_cell_id, dual_node_id] = m_primal_cell_to_dual_node_map[i_primal_cell_to_dual_node];
      node_owner_vector[dual_node_id]            = primal_cell_owner[primal_cell_id];
    }

    return node_owner_vector;
  }());

  dual_descriptor.setCellOwnerVector([&] {
    Array<int> cell_owner_vector(dual_descriptor.cellNumberVector().size());
    cell_owner_vector.fill(-1);
    for (size_t i_primal_cell_to_dual_node = 0; i_primal_cell_to_dual_node < m_primal_node_to_dual_cell_map.size();
         ++i_primal_cell_to_dual_node) {
      const auto& [primal_node_id, dual_cell_id] = m_primal_node_to_dual_cell_map[i_primal_cell_to_dual_node];
      cell_owner_vector[dual_cell_id]            = primal_node_owner[primal_node_id];
    }
    return cell_owner_vector;
  }());

  dual_descriptor.setFaceOwnerVector([&] {
    const auto& dual_cell_to_face_matrix = dual_descriptor.cellToFaceMatrix();
    const auto& dual_cell_owner_vector   = dual_descriptor.cellOwnerVector();

    Array<int> face_owner_vector(dual_descriptor.faceNumberVector().size());
    face_owner_vector.fill(parallel::size());
    for (size_t i_cell = 0; i_cell < dual_cell_to_face_matrix.numberOfRows(); ++i_cell) {
      const auto& cell_face_list = dual_cell_to_face_matrix[i_cell];
      for (size_t i_face = 0; i_face < cell_face_list.size(); ++i_face) {
        const size_t face_id       = cell_face_list[i_face];
        face_owner_vector[face_id] = std::min(face_owner_vector[face_id], dual_cell_owner_vector[i_cell]);
      }
    }
    return face_owner_vector;
  }());

  m_connectivity = ConnectivityType::build(dual_descriptor);

  const ConnectivityType& dual_connectivity = dynamic_cast<const ConnectivityType&>(*m_connectivity);

  m_mapper = std::make_shared<PrimalToMedianDualConnectivityDataMapper<2>>(primal_connectivity, dual_connectivity,
                                                                           m_primal_boundary_node_to_dual_node_map,
                                                                           m_primal_face_to_dual_node_map,
                                                                           m_primal_cell_to_dual_node_map,
                                                                           m_primal_node_to_dual_cell_map);
}

MedianDualConnectivityBuilder::MedianDualConnectivityBuilder(const IConnectivity& connectivity)
{
  // LCOV_EXCL_START
  if (parallel::size() > 1) {
    throw NotImplementedError("Construction of median dual mesh is not implemented in parallel");
  }
  // LCOV_EXCL_STOP
  switch (connectivity.dimension()) {
  case 2: {
    this->_buildConnectivityFrom<2>(connectivity);
    break;
  }
  case 3: {
    throw NotImplementedError("median dual connectivity");
    break;
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("invalid connectivity dimension: " + stringify(connectivity.dimension()));
  }
    // LCOV_EXCL_STOP
  }
}
