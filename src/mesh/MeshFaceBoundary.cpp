#include <mesh/MeshFaceBoundary.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <utils/Messenger.hpp>

template <MeshConcept MeshType>
MeshFaceBoundary::MeshFaceBoundary(const MeshType&, const RefFaceList& ref_face_list) : m_ref_face_list(ref_face_list)
{}

template MeshFaceBoundary::MeshFaceBoundary(const Mesh<1>&, const RefFaceList&);
template MeshFaceBoundary::MeshFaceBoundary(const Mesh<2>&, const RefFaceList&);
template MeshFaceBoundary::MeshFaceBoundary(const Mesh<3>&, const RefFaceList&);

template <MeshConcept MeshType>
MeshFaceBoundary
getMeshFaceBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor)
{
  for (size_t i_ref_face_list = 0; i_ref_face_list < mesh.connectivity().template numberOfRefItemList<ItemType::face>();
       ++i_ref_face_list) {
    const auto& ref_face_list = mesh.connectivity().template refItemList<ItemType::face>(i_ref_face_list);
    const RefId& ref          = ref_face_list.refId();

    if (ref == boundary_descriptor) {
      auto face_list = ref_face_list.list();
      if (ref_face_list.type() != RefItemListBase::Type::boundary) {
        std::ostringstream ost;
        ost << "invalid boundary " << rang::fgB::yellow << boundary_descriptor << rang::style::reset
            << ": inner faces cannot be used to define mesh boundaries";
        throw NormalError(ost.str());
      }

      return MeshFaceBoundary{mesh, ref_face_list};
    }
  }

  std::ostringstream ost;
  ost << "cannot find face list with name " << rang::fgB::red << boundary_descriptor << rang::style::reset;

  throw NormalError(ost.str());
}

template MeshFaceBoundary getMeshFaceBoundary(const Mesh<1>&, const IBoundaryDescriptor&);
template MeshFaceBoundary getMeshFaceBoundary(const Mesh<2>&, const IBoundaryDescriptor&);
template MeshFaceBoundary getMeshFaceBoundary(const Mesh<3>&, const IBoundaryDescriptor&);
