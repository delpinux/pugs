#ifndef DUAL_MESH_MANAGER_HPP
#define DUAL_MESH_MANAGER_HPP

#include <mesh/DualMeshType.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>

#include <memory>
#include <unordered_map>

class MeshVariant;

class DualMeshManager
{
 private:
  using Key = std::pair<DualMeshType, size_t>;
  struct HashKey
  {
    size_t
    operator()(const Key& key) const
    {
      return (std::hash<typename Key::first_type>()(key.first)) ^ (std::hash<typename Key::second_type>()(key.second));
    }
  };

  std::unordered_map<Key, std::shared_ptr<const MeshVariant>, HashKey> m_mesh_to_dual_mesh_map;

  static DualMeshManager* m_instance;

  DualMeshManager(const DualMeshManager&) = delete;
  DualMeshManager(DualMeshManager&&)      = delete;

  DualMeshManager()  = default;
  ~DualMeshManager() = default;

 public:
  const auto&
  primalToDualMeshMap() const
  {
    return m_mesh_to_dual_mesh_map;
  }

  static void create();
  static void destroy();

  PUGS_INLINE
  static DualMeshManager&
  instance()
  {
    Assert(m_instance != nullptr, "DualMeshManager was not created!");
    return *m_instance;
  }

  void deleteMesh(const size_t mesh_id);

  std::shared_ptr<const MeshVariant> getDual1DMesh(const std::shared_ptr<const MeshVariant>&);

  std::shared_ptr<const MeshVariant> getDiamondDualMesh(const std::shared_ptr<const MeshVariant>&);

  std::shared_ptr<const MeshVariant> getMedianDualMesh(const std::shared_ptr<const MeshVariant>&);
};

#endif   // DUAL_MESH_MANAGER_HPP
