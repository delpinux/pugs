#include <utils/PugsAssert.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/DiamondDualConnectivityBuilder.hpp>
#include <mesh/Dual1DConnectivityBuilder.hpp>
#include <mesh/DualConnectivityManager.hpp>
#include <mesh/MedianDualConnectivityBuilder.hpp>
#include <mesh/PrimalToDiamondDualConnectivityDataMapper.hpp>
#include <mesh/PrimalToDual1DConnectivityDataMapper.hpp>
#include <mesh/PrimalToMedianDualConnectivityDataMapper.hpp>
#include <utils/Exceptions.hpp>

#include <sstream>

DualConnectivityManager* DualConnectivityManager::m_instance{nullptr};

void
DualConnectivityManager::create()
{
  Assert(m_instance == nullptr, "DualConnectivityManager is already created");
  m_instance = new DualConnectivityManager;
}

void
DualConnectivityManager::destroy()
{
  Assert(m_instance != nullptr, "DualConnectivityManager was not created!");

  // LCOV_EXCL_START
  if (m_instance->m_primal_to_dual_connectivity_info_map.size() > 0) {
    std::stringstream error;
    error << ": some connectivities are still registered\n";
    for (const auto& [key, diamond_dual_connectivity_info] : m_instance->m_primal_to_dual_connectivity_info_map) {
      error << " - connectivity " << rang::fgB::magenta << key.second << rang::style::reset << ": " << name(key.first)
            << " dual connectivity of " << rang::fgB::yellow << diamond_dual_connectivity_info.dualConnectivity().get()
            << rang::style::reset << '\n';
    }
    throw UnexpectedError(error.str());
    // LCOV_EXCL_STOP
  }
  delete m_instance;
  m_instance = nullptr;
}

void
DualConnectivityManager::deleteConnectivity(const IConnectivity* p_connectivity)
{
  bool has_removed = false;
  do {
    has_removed = false;
    for (const auto& [key, dual_connectivity] : m_primal_to_dual_connectivity_info_map) {
      const auto& [type, p_parent_connectivity] = key;
      if (p_connectivity == p_parent_connectivity) {
        m_primal_to_dual_connectivity_info_map.erase(key);
        has_removed = true;
        break;
      }
    }
  } while (has_removed);
}

template <typename DualConnectivityBuilderType>
DualConnectivityManager::DualConnectivityInfo
DualConnectivityManager::_buildDualConnectivity(const Key& key, const IConnectivity& connectivity)
{
  DualConnectivityBuilderType builder{connectivity};
  DualConnectivityInfo connectivity_info{builder.connectivity(), builder.mapper()};
  m_primal_to_dual_connectivity_info_map[key] = connectivity_info;
  return connectivity_info;
}

DualConnectivityManager::DualConnectivityInfo
DualConnectivityManager::_getDualConnectivityInfo(const DualMeshType& type, const IConnectivity& connectivity)
{
  const IConnectivity* p_connectivity = &connectivity;

  auto key = std::make_pair(type, p_connectivity);
  if (auto i_connectivity = m_primal_to_dual_connectivity_info_map.find(key);
      i_connectivity != m_primal_to_dual_connectivity_info_map.end()) {
    return i_connectivity->second;
  } else {
    switch (type) {
    case DualMeshType::Diamond: {
      return this->_buildDualConnectivity<DiamondDualConnectivityBuilder>(key, connectivity);
    }
    case DualMeshType::Dual1D: {
      return this->_buildDualConnectivity<Dual1DConnectivityBuilder>(key, connectivity);
    }
    case DualMeshType::Median: {
      return this->_buildDualConnectivity<MedianDualConnectivityBuilder>(key, connectivity);
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("invalid dual mesh type");
    }
      // LCOV_EXCL_STOP
    }
  }
}

std::shared_ptr<const Connectivity<1>>
DualConnectivityManager::getDual1DConnectivity(const Connectivity<1>& connectivity)
{
  return std::dynamic_pointer_cast<const Connectivity<1>>(
    this->_getDualConnectivityInfo(DualMeshType::Dual1D, connectivity).dualConnectivity());
}

std::shared_ptr<const PrimalToDual1DConnectivityDataMapper>
DualConnectivityManager::getPrimalToDual1DConnectivityDataMapper(const Connectivity<1>& connectivity)
{
  auto i_data_mapper =
    this->_getDualConnectivityInfo(DualMeshType::Dual1D, connectivity).connectivityToDualConnectivityDataMapper();
  auto dual_1d_data_mapper = std::dynamic_pointer_cast<const PrimalToDual1DConnectivityDataMapper>(i_data_mapper);

  if (dual_1d_data_mapper.use_count() > 0) {
    return dual_1d_data_mapper;
  } else {
    // LCOV_EXCL_START
    throw UnexpectedError("invalid connectivity data mapper type");
    // LCOV_EXCL_STOP
  }
}

template <size_t Dimension>
std::shared_ptr<const Connectivity<Dimension>>
DualConnectivityManager::getDiamondDualConnectivity(const Connectivity<Dimension>& connectivity)
{
  auto dual_mesh_type = (connectivity.dimension() == 1) ? DualMeshType::Dual1D : DualMeshType::Diamond;

  return std::dynamic_pointer_cast<const Connectivity<Dimension>>(
    this->_getDualConnectivityInfo(dual_mesh_type, connectivity).dualConnectivity());
}

std::shared_ptr<const PrimalToDual1DConnectivityDataMapper>
DualConnectivityManager::getPrimalToDiamondDualConnectivityDataMapper(const Connectivity<1>& connectivity)
{
  return this->getPrimalToDual1DConnectivityDataMapper(connectivity);
}

template <size_t Dimension>
std::shared_ptr<const PrimalToDiamondDualConnectivityDataMapper<Dimension>>
DualConnectivityManager::getPrimalToDiamondDualConnectivityDataMapper(const Connectivity<Dimension>& connectivity)
{
  static_assert((Dimension == 2) or (Dimension == 3));
  auto i_data_mapper =
    this->_getDualConnectivityInfo(DualMeshType::Diamond, connectivity).connectivityToDualConnectivityDataMapper();
  auto diamond_data_mapper =
    std::dynamic_pointer_cast<const PrimalToDiamondDualConnectivityDataMapper<Dimension>>(i_data_mapper);

  if (diamond_data_mapper.use_count() > 0) {
    return diamond_data_mapper;
  } else {
    // LCOV_EXCL_START
    throw UnexpectedError("invalid connectivity data mapper type");
    // LCOV_EXCL_STOP
  }
}

template <size_t Dimension>
std::shared_ptr<const Connectivity<Dimension>>
DualConnectivityManager::getMedianDualConnectivity(const Connectivity<Dimension>& connectivity)
{
  auto dual_mesh_type = (connectivity.dimension() == 1) ? DualMeshType::Dual1D : DualMeshType::Median;

  return std::dynamic_pointer_cast<const Connectivity<Dimension>>(
    this->_getDualConnectivityInfo(dual_mesh_type, connectivity).dualConnectivity());
}

std::shared_ptr<const PrimalToDual1DConnectivityDataMapper>
DualConnectivityManager::getPrimalToMedianDualConnectivityDataMapper(const Connectivity<1>& connectivity)
{
  return this->getPrimalToDual1DConnectivityDataMapper(connectivity);
}

template <size_t Dimension>
std::shared_ptr<const PrimalToMedianDualConnectivityDataMapper<Dimension>>
DualConnectivityManager::getPrimalToMedianDualConnectivityDataMapper(const Connectivity<Dimension>& connectivity)
{
  auto i_data_mapper =
    this->_getDualConnectivityInfo(DualMeshType::Median, connectivity).connectivityToDualConnectivityDataMapper();
  auto data_mapper =
    std::dynamic_pointer_cast<const PrimalToMedianDualConnectivityDataMapper<Dimension>>(i_data_mapper);

  if (data_mapper.use_count() > 0) {
    return data_mapper;
  } else {
    // LCOV_EXCL_START
    throw UnexpectedError("invalid connectivity data mapper type");
    // LCOV_EXCL_STOP
  }
}

template std::shared_ptr<const Connectivity<1>> DualConnectivityManager::getDiamondDualConnectivity(
  const Connectivity<1>& connectivity);

template std::shared_ptr<const Connectivity<2>> DualConnectivityManager::getDiamondDualConnectivity(
  const Connectivity<2>& connectivity);

template std::shared_ptr<const Connectivity<3>> DualConnectivityManager::getDiamondDualConnectivity(
  const Connectivity<3>& connectivity);

template std::shared_ptr<const PrimalToDiamondDualConnectivityDataMapper<2>>
DualConnectivityManager::getPrimalToDiamondDualConnectivityDataMapper(const Connectivity<2>&);

template std::shared_ptr<const PrimalToDiamondDualConnectivityDataMapper<3>>
DualConnectivityManager::getPrimalToDiamondDualConnectivityDataMapper(const Connectivity<3>&);

template std::shared_ptr<const Connectivity<1>> DualConnectivityManager::getMedianDualConnectivity(
  const Connectivity<1>& connectivity);

template std::shared_ptr<const Connectivity<2>> DualConnectivityManager::getMedianDualConnectivity(
  const Connectivity<2>& connectivity);

template std::shared_ptr<const Connectivity<3>> DualConnectivityManager::getMedianDualConnectivity(
  const Connectivity<3>& connectivity);

template std::shared_ptr<const PrimalToMedianDualConnectivityDataMapper<2>>
DualConnectivityManager::getPrimalToMedianDualConnectivityDataMapper(const Connectivity<2>&);

template std::shared_ptr<const PrimalToMedianDualConnectivityDataMapper<3>>
DualConnectivityManager::getPrimalToMedianDualConnectivityDataMapper(const Connectivity<3>&);
