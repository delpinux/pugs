#ifndef MESH_UTILS_HPP
#define MESH_UTILS_HPP

#include <memory>

class MeshVariant;

bool checkConnectivityOrdering(const std::shared_ptr<const MeshVariant>& mesh_v);

#endif   // MESH_UTILS_HPP
