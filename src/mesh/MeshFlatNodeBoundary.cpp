#include <mesh/MeshFlatNodeBoundary.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshNodeBoundaryUtils.hpp>
#include <utils/Messenger.hpp>

template <MeshConcept MeshType>
void
MeshFlatNodeBoundary<MeshType>::_checkBoundaryIsFlat(const TinyVector<MeshType::Dimension, double>& normal,
                                                     const TinyVector<MeshType::Dimension, double>& origin,
                                                     const double length,
                                                     const MeshType& mesh) const
{
  const NodeValue<const Rd>& xr = mesh.xr();

  bool is_bad = false;

  auto node_list = this->m_ref_node_list.list();
  parallel_for(node_list.size(), [=, &is_bad](int r) {
    const Rd& x = xr[node_list[r]];
    if (std::abs(dot(x - origin, normal)) > 1E-13 * length) {
      is_bad = true;
    }
  });

  if (parallel::allReduceOr(is_bad)) {
    std::ostringstream ost;
    ost << "invalid boundary \"" << rang::fgB::yellow << this->m_ref_node_list.refId() << rang::style::reset
        << "\": boundary is not flat!";
    throw NormalError(ost.str());
  }
}

template <>
TinyVector<1, double>
MeshFlatNodeBoundary<Mesh<1>>::_getNormal(const Mesh<1>& mesh)
{
  using R = TinyVector<1, double>;

  const size_t number_of_bc_nodes = [&]() {
    size_t nb_bc_nodes = 0;
    auto node_is_owned = mesh.connectivity().nodeIsOwned();
    auto node_list     = m_ref_node_list.list();
    for (size_t i_node = 0; i_node < node_list.size(); ++i_node) {
      nb_bc_nodes += (node_is_owned[node_list[i_node]]);
    }
    return parallel::allReduceMax(nb_bc_nodes);
  }();

  if (number_of_bc_nodes != 1) {
    std::ostringstream ost;
    ost << "invalid boundary " << rang::fgB::yellow << m_ref_node_list.refId() << rang::style::reset
        << ": node boundaries in 1D require to have exactly 1 node";
    throw NormalError(ost.str());
  }

  return R{1};
}

template <>
TinyVector<2, double>
MeshFlatNodeBoundary<Mesh<2>>::_getNormal(const Mesh<2>& mesh)
{
  using R2 = TinyVector<2, double>;

  std::array<R2, 2> bounds = getBounds(mesh, m_ref_node_list);

  const R2& xmin = bounds[0];
  const R2& xmax = bounds[1];

  if (xmin == xmax) {
    std::ostringstream ost;
    ost << "invalid boundary \"" << rang::fgB::yellow << m_ref_node_list.refId() << rang::style::reset
        << "\": unable to compute normal";
    throw NormalError(ost.str());
  }

  R2 dx = xmax - xmin;
  dx *= 1. / l2Norm(dx);

  const R2 normal(-dx[1], dx[0]);

  this->_checkBoundaryIsFlat(normal, 0.5 * (xmin + xmax), l2Norm(xmax - xmin), mesh);

  return normal;
}

template <>
TinyVector<3, double>
MeshFlatNodeBoundary<Mesh<3>>::_getFarestNode(const Mesh<3>& mesh, const Rd& x0, const Rd& x1)
{
  const NodeValue<const Rd>& xr = mesh.xr();
  const auto node_number        = mesh.connectivity().nodeNumber();

  using NodeNumberType = std::remove_const_t<typename decltype(node_number)::data_type>;

  Rd t = x1 - x0;
  t *= 1. / l2Norm(t);

  double farest_distance       = 0;
  Rd farest_x                  = zero;
  NodeNumberType farest_number = std::numeric_limits<NodeNumberType>::max();

  auto node_list = this->m_ref_node_list.list();

  for (size_t i_node = 0; i_node < node_list.size(); ++i_node) {
    const NodeId& node_id = node_list[i_node];
    const Rd& x           = xr[node_id];
    const double distance = l2Norm(crossProduct(t, x - x0));

    if ((distance > farest_distance) or ((distance == farest_distance) and (node_number[node_id] < farest_number))) {
      farest_distance = distance;
      farest_number   = node_number[node_id];
      farest_x        = x;
    }
  }

  if (parallel::size()) {
    Array<double> farest_distance_array       = parallel::allGather(farest_distance);
    Array<Rd> farest_x_array                  = parallel::allGather(farest_x);
    Array<NodeNumberType> farest_number_array = parallel::allGather(farest_number);

    Assert(farest_distance_array.size() == farest_x_array.size());
    Assert(farest_distance_array.size() == farest_number_array.size());

    for (size_t i = 0; i < farest_distance_array.size(); ++i) {
      if ((farest_distance_array[i] > farest_distance) or
          ((farest_distance_array[i] == farest_distance) and (farest_number_array[i] < farest_number))) {
        farest_distance = farest_distance_array[i];
        farest_number   = farest_number_array[i];
        farest_x        = farest_x_array[i];
      }
    }
  }

  return farest_x;
}

template <>
TinyVector<3, double>
MeshFlatNodeBoundary<Mesh<3>>::_getNormal(const Mesh<3>& mesh)
{
  using R3 = TinyVector<3, double>;

  std::array<R3, 2> diagonal = [](const std::array<R3, 6>& bounds) {
    size_t max_i      = 0;
    size_t max_j      = 0;
    double max_length = 0;

    for (size_t i = 0; i < bounds.size(); ++i) {
      for (size_t j = i + 1; j < bounds.size(); ++j) {
        double length = l2Norm(bounds[i] - bounds[j]);
        if (length > max_length) {
          max_i      = i;
          max_j      = j;
          max_length = length;
        }
      }
    }

    return std::array<R3, 2>{bounds[max_i], bounds[max_j]};
  }(getBounds(mesh, m_ref_node_list));

  const R3& x0 = diagonal[0];
  const R3& x1 = diagonal[1];

  if (x0 == x1) {
    std::ostringstream ost;
    ost << "invalid boundary \"" << rang::fgB::yellow << m_ref_node_list.refId() << rang::style::reset
        << "\": unable to compute normal";
    throw NormalError(ost.str());
  }

  const R3 x2 = this->_getFarestNode(mesh, x0, x1);

  const R3 u = x1 - x0;
  const R3 v = x2 - x0;

  R3 normal                = crossProduct(u, v);
  const double normal_norm = l2Norm(normal);

  if (normal_norm == 0) {
    std::ostringstream ost;
    ost << "invalid boundary \"" << rang::fgB::yellow << m_ref_node_list.refId() << rang::style::reset
        << "\": unable to compute normal";
    throw NormalError(ost.str());
  }

  normal *= (1. / normal_norm);

  this->_checkBoundaryIsFlat(normal, 1. / 3. * (x0 + x1 + x2), normal_norm, mesh);

  return normal;
}

template <>
TinyVector<1, double>
MeshFlatNodeBoundary<Mesh<1>>::_getOutgoingNormal(const Mesh<1>& mesh)
{
  using R = TinyVector<1, double>;

  const R normal = this->_getNormal(mesh);

  double max_height = 0;
  auto node_list    = m_ref_node_list.list();
  if (node_list.size() > 0) {
    const NodeValue<const R>& xr    = mesh.xr();
    const auto& cell_to_node_matrix = mesh.connectivity().cellToNodeMatrix();

    const auto& node_to_cell_matrix = mesh.connectivity().nodeToCellMatrix();

    const NodeId r0      = node_list[0];
    const CellId j0      = node_to_cell_matrix[r0][0];
    const auto& j0_nodes = cell_to_node_matrix[j0];

    for (size_t r = 0; r < j0_nodes.size(); ++r) {
      const double height = dot(xr[j0_nodes[r]] - xr[r0], normal);
      if (std::abs(height) > std::abs(max_height)) {
        max_height = height;
      }
    }
  }

  Array<double> max_height_array = parallel::allGather(max_height);
  for (size_t i = 0; i < max_height_array.size(); ++i) {
    const double height = max_height_array[i];
    if (std::abs(height) > std::abs(max_height)) {
      max_height = height;
    }
  }

  if (max_height > 0) {
    return -normal;
  } else {
    return normal;
  }
}

template <>
TinyVector<2, double>
MeshFlatNodeBoundary<Mesh<2>>::_getOutgoingNormal(const Mesh<2>& mesh)
{
  using R2 = TinyVector<2, double>;

  const R2 normal = this->_getNormal(mesh);

  double max_height = 0;

  auto node_list = m_ref_node_list.list();
  if (node_list.size() > 0) {
    const NodeValue<const R2>& xr   = mesh.xr();
    const auto& cell_to_node_matrix = mesh.connectivity().cellToNodeMatrix();

    const auto& node_to_cell_matrix = mesh.connectivity().nodeToCellMatrix();

    const NodeId r0      = node_list[0];
    const CellId j0      = node_to_cell_matrix[r0][0];
    const auto& j0_nodes = cell_to_node_matrix[j0];
    for (size_t r = 0; r < j0_nodes.size(); ++r) {
      const double height = dot(xr[j0_nodes[r]] - xr[r0], normal);
      if (std::abs(height) > std::abs(max_height)) {
        max_height = height;
      }
    }
  }

  Array<double> max_height_array = parallel::allGather(max_height);
  for (size_t i = 0; i < max_height_array.size(); ++i) {
    const double height = max_height_array[i];
    if (std::abs(height) > std::abs(max_height)) {
      max_height = height;
    }
  }

  if (max_height > 0) {
    return -normal;
  } else {
    return normal;
  }
}

template <>
TinyVector<3, double>
MeshFlatNodeBoundary<Mesh<3>>::_getOutgoingNormal(const Mesh<3>& mesh)
{
  using R3 = TinyVector<3, double>;

  const R3 normal = this->_getNormal(mesh);

  double max_height = 0;
  auto node_list    = m_ref_node_list.list();
  if (node_list.size() > 0) {
    const NodeValue<const R3>& xr   = mesh.xr();
    const auto& cell_to_node_matrix = mesh.connectivity().cellToNodeMatrix();

    const auto& node_to_cell_matrix = mesh.connectivity().nodeToCellMatrix();

    const NodeId r0      = node_list[0];
    const CellId j0      = node_to_cell_matrix[r0][0];
    const auto& j0_nodes = cell_to_node_matrix[j0];

    for (size_t r = 0; r < j0_nodes.size(); ++r) {
      const double height = dot(xr[j0_nodes[r]] - xr[r0], normal);
      if (std::abs(height) > std::abs(max_height)) {
        max_height = height;
      }
    }
  }

  Array<double> max_height_array = parallel::allGather(max_height);
  for (size_t i = 0; i < max_height_array.size(); ++i) {
    const double height = max_height_array[i];
    if (std::abs(height) > std::abs(max_height)) {
      max_height = height;
    }
  }

  if (max_height > 0) {
    return -normal;
  } else {
    return normal;
  }
}

template <typename MeshType>
MeshFlatNodeBoundary<MeshType>
getMeshFlatNodeBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor)
{
  for (size_t i_ref_node_list = 0; i_ref_node_list < mesh.connectivity().template numberOfRefItemList<ItemType::node>();
       ++i_ref_node_list) {
    const auto& ref_node_list = mesh.connectivity().template refItemList<ItemType::node>(i_ref_node_list);
    const RefId& ref          = ref_node_list.refId();
    if (ref == boundary_descriptor) {
      return MeshFlatNodeBoundary<MeshType>{mesh, ref_node_list};
    }
  }
  for (size_t i_ref_face_list = 0; i_ref_face_list < mesh.connectivity().template numberOfRefItemList<ItemType::face>();
       ++i_ref_face_list) {
    const auto& ref_face_list = mesh.connectivity().template refItemList<ItemType::face>(i_ref_face_list);
    const RefId& ref          = ref_face_list.refId();
    if (ref == boundary_descriptor) {
      return MeshFlatNodeBoundary<MeshType>{mesh, ref_face_list};
    }
  }

  std::ostringstream ost;
  ost << "cannot find surface with name " << rang::fgB::red << boundary_descriptor << rang::style::reset;

  throw NormalError(ost.str());
}

template MeshFlatNodeBoundary<Mesh<1>> getMeshFlatNodeBoundary(const Mesh<1>&, const IBoundaryDescriptor&);
template MeshFlatNodeBoundary<Mesh<2>> getMeshFlatNodeBoundary(const Mesh<2>&, const IBoundaryDescriptor&);
template MeshFlatNodeBoundary<Mesh<3>> getMeshFlatNodeBoundary(const Mesh<3>&, const IBoundaryDescriptor&);
