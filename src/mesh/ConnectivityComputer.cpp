#include <mesh/ConnectivityComputer.hpp>

#include <mesh/Connectivity.hpp>
#include <utils/Exceptions.hpp>

#include <iostream>

template <typename ConnectivityType>
PUGS_INLINE ConnectivityMatrix
ConnectivityComputer::computeInverseConnectivityMatrix(const ConnectivityType& connectivity,
                                                       ItemType item_type,
                                                       ItemType child_item_type) const
{
  if (item_type < child_item_type) {
    ConnectivityMatrix item_to_child_item_matrix;
    if (connectivity.isConnectivityMatrixBuilt(child_item_type, item_type)) {
      const ConnectivityMatrix& child_to_item_matrix = connectivity.getMatrix(child_item_type, item_type);

      switch (child_item_type) {
      case ItemType::cell: {
        item_to_child_item_matrix =
          this->_computeInverseConnectivity<ConnectivityType, ItemType::cell>(connectivity, child_to_item_matrix);
        break;
      }
      case ItemType::face: {
        item_to_child_item_matrix =
          this->_computeInverseConnectivity<ConnectivityType, ItemType::face>(connectivity, child_to_item_matrix);
        break;
      }
      case ItemType::edge: {
        item_to_child_item_matrix =
          this->_computeInverseConnectivity<ConnectivityType, ItemType::edge>(connectivity, child_to_item_matrix);
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("node cannot be child item when computing inverse connectivity");
      }
        // LCOV_EXCL_STOP
      }
    } else {
      std::stringstream error_msg;
      error_msg << "unable to compute connectivity " << itemName(item_type) << " -> " << itemName(child_item_type);
      throw UnexpectedError(error_msg.str());
    }

    return item_to_child_item_matrix;
  } else {
    std::stringstream error_msg;
    error_msg << "cannot deduce " << itemName(item_type) << " -> " << itemName(child_item_type) << " connectivity";
    throw UnexpectedError(error_msg.str());
  }
}

template ConnectivityMatrix ConnectivityComputer::computeInverseConnectivityMatrix(const Connectivity1D&,
                                                                                   ItemType,
                                                                                   ItemType) const;

template ConnectivityMatrix ConnectivityComputer::computeInverseConnectivityMatrix(const Connectivity2D&,
                                                                                   ItemType,
                                                                                   ItemType) const;

template ConnectivityMatrix ConnectivityComputer::computeInverseConnectivityMatrix(const Connectivity3D&,
                                                                                   ItemType,
                                                                                   ItemType) const;

template <typename ConnectivityType, ItemType child_item_type>
ConnectivityMatrix
ConnectivityComputer::_computeInverseConnectivity(const ConnectivityType& connectivity,
                                                  const ConnectivityMatrix& child_to_item_matrix) const
{
  const size_t number_of_transposed_columns = child_to_item_matrix.numberOfRows();

  if ((child_to_item_matrix.values().size() > 0)) {
    const size_t number_of_transposed_rows = max(child_to_item_matrix.values()) + 1;

    Array<uint32_t> transposed_next_free_column_index(number_of_transposed_rows);
    transposed_next_free_column_index.fill(0);

    Array<uint32_t> transposed_rows_map(transposed_next_free_column_index.size() + 1);
    transposed_rows_map.fill(0);
    for (size_t i = 0; i < number_of_transposed_columns; ++i) {
      for (size_t j = child_to_item_matrix.rowsMap()[i]; j < child_to_item_matrix.rowsMap()[i + 1]; ++j) {
        transposed_rows_map[child_to_item_matrix.values()[j] + 1]++;
      }
    }
    for (size_t i = 1; i < transposed_rows_map.size(); ++i) {
      transposed_rows_map[i] += transposed_rows_map[i - 1];
    }
    Array<uint32_t> transposed_column_indices(transposed_rows_map[transposed_rows_map.size() - 1]);
    for (size_t i = 0; i < number_of_transposed_columns; ++i) {
      for (size_t j = child_to_item_matrix.rowsMap()[i]; j < child_to_item_matrix.rowsMap()[i + 1]; ++j) {
        size_t i_column_index = child_to_item_matrix.values()[j];
        uint32_t& shift       = transposed_next_free_column_index[i_column_index];

        transposed_column_indices[transposed_rows_map[i_column_index] + shift] = i;
        Assert(shift < transposed_rows_map[i_column_index + 1]);
        shift++;
      }
    }

    auto target_item_number = connectivity.template number<child_item_type>();
    // Finally one sorts target item_ids for parallel reproducibility
    for (size_t i = 0; i < number_of_transposed_rows; ++i) {
      auto row_begining = &(transposed_column_indices[transposed_rows_map[i]]);
      auto row_size     = (transposed_rows_map[i + 1] - transposed_rows_map[i]);
      std::sort(row_begining, row_begining + row_size,
                [&target_item_number](const ItemIdT<child_item_type> item0_id,
                                      const ItemIdT<child_item_type> item1_id) {
                  return target_item_number[item0_id] < target_item_number[item1_id];
                });
    }

    return ConnectivityMatrix{transposed_rows_map, transposed_column_indices};
  } else {
    // empty connectivity
    Array<uint32_t> transposed_rows_map{1};
    transposed_rows_map[0] = 0;
    Array<uint32_t> transposed_column_indices;

    return ConnectivityMatrix{transposed_rows_map, transposed_column_indices};
  }
}

template <typename ItemOfItem, typename ConnectivityType>
void
ConnectivityComputer::computeLocalItemNumberInChildItem(const ConnectivityType& connectivity) const
{
  constexpr ItemType item_type     = ItemOfItem::item_type;
  constexpr ItemType sub_item_type = ItemOfItem::sub_item_type;

  ItemToItemMatrix item_to_child_items_matrix = connectivity.template getItemToItemMatrix<item_type, sub_item_type>();
  ItemToItemMatrix child_item_to_items_matrix = connectivity.template getItemToItemMatrix<sub_item_type, item_type>();

  Array<unsigned short> number_in_child_array{item_to_child_items_matrix.numberOfValues()};
  {
    WeakSubItemValuePerItem<unsigned short, ItemOfItem> item_number_in_child_item(connectivity, number_in_child_array);
    for (ItemIdT<item_type> item_id = 0; item_id < connectivity.template numberOf<item_type>(); ++item_id) {
      const auto& item_to_child_items = item_to_child_items_matrix[item_id];
      for (unsigned short i_item_child = 0; i_item_child < item_to_child_items_matrix[item_id].size(); ++i_item_child) {
        ItemIdT j                       = item_to_child_items[i_item_child];
        const auto& child_item_to_items = child_item_to_items_matrix[j];

        for (unsigned int i_item_in_child = 0; i_item_in_child < child_item_to_items.size(); ++i_item_in_child) {
          if (child_item_to_items[i_item_in_child] == item_id) {
            item_number_in_child_item(item_id, i_item_child) = i_item_in_child;
            break;
          }
        }
      }
    }
  }

  if constexpr (ConnectivityType::Dimension == 1) {
    if constexpr (item_type == ItemType::cell) {
      const_cast<WeakSubItemValuePerItem<const unsigned short, FaceOfCell>&>(
        connectivity.m_cell_local_numbers_in_their_faces) =
        WeakSubItemValuePerItem<unsigned short, FaceOfCell>(connectivity, number_in_child_array);
      const_cast<WeakSubItemValuePerItem<const unsigned short, EdgeOfCell>&>(
        connectivity.m_cell_local_numbers_in_their_edges) =
        WeakSubItemValuePerItem<unsigned short, EdgeOfCell>(connectivity, number_in_child_array);
      const_cast<WeakSubItemValuePerItem<const unsigned short, NodeOfCell>&>(
        connectivity.m_cell_local_numbers_in_their_nodes) =
        WeakSubItemValuePerItem<unsigned short, NodeOfCell>(connectivity, number_in_child_array);
    } else if constexpr (sub_item_type == ItemType::cell) {
      const_cast<WeakSubItemValuePerItem<const unsigned short, CellOfFace>&>(
        connectivity.m_face_local_numbers_in_their_cells) =
        WeakSubItemValuePerItem<unsigned short, CellOfFace>(connectivity, number_in_child_array);
      const_cast<WeakSubItemValuePerItem<const unsigned short, CellOfEdge>&>(
        connectivity.m_edge_local_numbers_in_their_cells) =
        WeakSubItemValuePerItem<unsigned short, CellOfEdge>(connectivity, number_in_child_array);
      const_cast<WeakSubItemValuePerItem<const unsigned short, CellOfNode>&>(
        connectivity.m_node_local_numbers_in_their_cells) =
        WeakSubItemValuePerItem<unsigned short, CellOfNode>(connectivity, number_in_child_array);
    }
  } else if constexpr (ConnectivityType::Dimension == 2) {
    if constexpr (item_type == ItemType::cell) {
      if constexpr ((sub_item_type == ItemType::face) or (sub_item_type == ItemType::edge)) {
        const_cast<WeakSubItemValuePerItem<const unsigned short, FaceOfCell>&>(
          connectivity.m_cell_local_numbers_in_their_faces) =
          WeakSubItemValuePerItem<unsigned short, FaceOfCell>(connectivity, number_in_child_array);
        const_cast<WeakSubItemValuePerItem<const unsigned short, EdgeOfCell>&>(
          connectivity.m_cell_local_numbers_in_their_edges) =
          WeakSubItemValuePerItem<unsigned short, EdgeOfCell>(connectivity, number_in_child_array);
      } else {
        static_assert(sub_item_type == ItemType::node);
        const_cast<WeakSubItemValuePerItem<const unsigned short, NodeOfCell>&>(
          connectivity.m_cell_local_numbers_in_their_nodes) =
          WeakSubItemValuePerItem<unsigned short, NodeOfCell>(connectivity, number_in_child_array);
      }
    } else if constexpr (item_type == ItemType::face or item_type == ItemType::edge) {
      if constexpr (sub_item_type == ItemType::cell) {
        const_cast<WeakSubItemValuePerItem<const unsigned short, CellOfFace>&>(
          connectivity.m_face_local_numbers_in_their_cells) =
          WeakSubItemValuePerItem<unsigned short, CellOfFace>(connectivity, number_in_child_array);
        const_cast<WeakSubItemValuePerItem<const unsigned short, CellOfEdge>&>(
          connectivity.m_edge_local_numbers_in_their_cells) =
          WeakSubItemValuePerItem<unsigned short, CellOfEdge>(connectivity, number_in_child_array);
      } else {
        static_assert(sub_item_type == ItemType::node);
        const_cast<WeakSubItemValuePerItem<const unsigned short, NodeOfFace>&>(
          connectivity.m_face_local_numbers_in_their_nodes) =
          WeakSubItemValuePerItem<unsigned short, NodeOfFace>(connectivity, number_in_child_array);
        const_cast<WeakSubItemValuePerItem<const unsigned short, NodeOfEdge>&>(
          connectivity.m_edge_local_numbers_in_their_nodes) =
          WeakSubItemValuePerItem<unsigned short, NodeOfEdge>(connectivity, number_in_child_array);
      }
    } else {
      static_assert(item_type == ItemType::node);
      if constexpr (sub_item_type == ItemType::cell) {
        const_cast<WeakSubItemValuePerItem<const unsigned short, CellOfNode>&>(
          connectivity.m_node_local_numbers_in_their_cells) =
          WeakSubItemValuePerItem<unsigned short, CellOfNode>(connectivity, number_in_child_array);
      } else {
        static_assert(sub_item_type == ItemType::face or sub_item_type == ItemType::edge);
        const_cast<WeakSubItemValuePerItem<const unsigned short, FaceOfNode>&>(
          connectivity.m_node_local_numbers_in_their_faces) =
          WeakSubItemValuePerItem<unsigned short, FaceOfNode>(connectivity, number_in_child_array);
        const_cast<WeakSubItemValuePerItem<const unsigned short, EdgeOfNode>&>(
          connectivity.m_node_local_numbers_in_their_edges) =
          WeakSubItemValuePerItem<unsigned short, EdgeOfNode>(connectivity, number_in_child_array);
      }
    }
  } else if constexpr (ConnectivityType::Dimension == 3) {
    if constexpr (item_type == ItemType::cell) {
      if constexpr (sub_item_type == ItemType::face) {
        const_cast<WeakSubItemValuePerItem<const unsigned short, FaceOfCell>&>(
          connectivity.m_cell_local_numbers_in_their_faces) =
          WeakSubItemValuePerItem<unsigned short, FaceOfCell>(connectivity, number_in_child_array);
      } else if constexpr (sub_item_type == ItemType::edge) {
        const_cast<WeakSubItemValuePerItem<const unsigned short, EdgeOfCell>&>(
          connectivity.m_cell_local_numbers_in_their_edges) =
          WeakSubItemValuePerItem<unsigned short, EdgeOfCell>(connectivity, number_in_child_array);
      } else {
        static_assert(sub_item_type == ItemType::node);
        const_cast<WeakSubItemValuePerItem<const unsigned short, NodeOfCell>&>(
          connectivity.m_cell_local_numbers_in_their_nodes) =
          WeakSubItemValuePerItem<unsigned short, NodeOfCell>(connectivity, number_in_child_array);
      }
    } else if constexpr (item_type == ItemType::face) {
      if constexpr (sub_item_type == ItemType::cell) {
        const_cast<WeakSubItemValuePerItem<const unsigned short, CellOfFace>&>(
          connectivity.m_face_local_numbers_in_their_cells) =
          WeakSubItemValuePerItem<unsigned short, CellOfFace>(connectivity, number_in_child_array);
      } else if constexpr (sub_item_type == ItemType::edge) {
        const_cast<WeakSubItemValuePerItem<const unsigned short, EdgeOfFace>&>(
          connectivity.m_face_local_numbers_in_their_edges) =
          WeakSubItemValuePerItem<unsigned short, EdgeOfFace>(connectivity, number_in_child_array);
      } else {
        static_assert(sub_item_type == ItemType::node);
        const_cast<WeakSubItemValuePerItem<const unsigned short, NodeOfFace>&>(
          connectivity.m_face_local_numbers_in_their_nodes) =
          WeakSubItemValuePerItem<unsigned short, NodeOfFace>(connectivity, number_in_child_array);
      }
    } else if constexpr (item_type == ItemType::edge) {
      if constexpr (sub_item_type == ItemType::cell) {
        const_cast<WeakSubItemValuePerItem<const unsigned short, CellOfEdge>&>(
          connectivity.m_edge_local_numbers_in_their_cells) =
          WeakSubItemValuePerItem<unsigned short, CellOfEdge>(connectivity, number_in_child_array);
      } else if constexpr (sub_item_type == ItemType::face) {
        const_cast<WeakSubItemValuePerItem<const unsigned short, FaceOfEdge>&>(
          connectivity.m_edge_local_numbers_in_their_faces) =
          WeakSubItemValuePerItem<unsigned short, FaceOfEdge>(connectivity, number_in_child_array);
      } else {
        static_assert(sub_item_type == ItemType::node);
        const_cast<WeakSubItemValuePerItem<const unsigned short, NodeOfEdge>&>(
          connectivity.m_edge_local_numbers_in_their_nodes) =
          WeakSubItemValuePerItem<unsigned short, NodeOfEdge>(connectivity, number_in_child_array);
      }
    } else {
      static_assert(item_type == ItemType::node);
      if constexpr (sub_item_type == ItemType::cell) {
        const_cast<WeakSubItemValuePerItem<const unsigned short, CellOfNode>&>(
          connectivity.m_node_local_numbers_in_their_cells) =
          WeakSubItemValuePerItem<unsigned short, CellOfNode>(connectivity, number_in_child_array);
      } else if constexpr (sub_item_type == ItemType::face) {
        const_cast<WeakSubItemValuePerItem<const unsigned short, FaceOfNode>&>(
          connectivity.m_node_local_numbers_in_their_faces) =
          WeakSubItemValuePerItem<unsigned short, FaceOfNode>(connectivity, number_in_child_array);
      } else {
        static_assert(sub_item_type == ItemType::edge);
        const_cast<WeakSubItemValuePerItem<const unsigned short, EdgeOfNode>&>(
          connectivity.m_node_local_numbers_in_their_edges) =
          WeakSubItemValuePerItem<unsigned short, EdgeOfNode>(connectivity, number_in_child_array);
      }
    }
  }
}

// 1D

template void ConnectivityComputer::computeLocalItemNumberInChildItem<CellOfFace>(const Connectivity1D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<CellOfEdge>(const Connectivity1D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<CellOfNode>(const Connectivity1D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<FaceOfCell>(const Connectivity1D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<EdgeOfCell>(const Connectivity1D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<NodeOfCell>(const Connectivity1D&) const;

// 2D

template void ConnectivityComputer::computeLocalItemNumberInChildItem<CellOfFace>(const Connectivity2D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<CellOfEdge>(const Connectivity2D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<CellOfNode>(const Connectivity2D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<FaceOfCell>(const Connectivity2D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<FaceOfNode>(const Connectivity2D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<EdgeOfCell>(const Connectivity2D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<EdgeOfNode>(const Connectivity2D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<NodeOfCell>(const Connectivity2D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<NodeOfFace>(const Connectivity2D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<NodeOfEdge>(const Connectivity2D&) const;

// 3D

template void ConnectivityComputer::computeLocalItemNumberInChildItem<CellOfFace>(const Connectivity3D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<CellOfEdge>(const Connectivity3D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<CellOfNode>(const Connectivity3D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<FaceOfCell>(const Connectivity3D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<FaceOfEdge>(const Connectivity3D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<FaceOfNode>(const Connectivity3D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<EdgeOfCell>(const Connectivity3D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<EdgeOfFace>(const Connectivity3D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<EdgeOfNode>(const Connectivity3D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<NodeOfCell>(const Connectivity3D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<NodeOfFace>(const Connectivity3D&) const;

template void ConnectivityComputer::computeLocalItemNumberInChildItem<NodeOfEdge>(const Connectivity3D&) const;
