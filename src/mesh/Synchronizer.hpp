#ifndef SYNCHRONIZER_HPP
#define SYNCHRONIZER_HPP

#include <mesh/Connectivity.hpp>
#include <mesh/ItemArray.hpp>
#include <mesh/ItemValue.hpp>
#include <mesh/SubItemArrayPerItem.hpp>
#include <mesh/SubItemValuePerItem.hpp>
#include <utils/Exceptions.hpp>
#include <utils/Messenger.hpp>

#include <utils/pugs_config.hpp>

#include <memory>
#include <unordered_map>

#ifdef PUGS_HAS_MPI

class Synchronizer
{
 private:
  template <ItemType item_type>
  using ExchangeItemTypeInfo = std::vector<Array<const ItemIdT<item_type>>>;

  using ExchangeItemInfoRepository = std::tuple<std::unique_ptr<ExchangeItemTypeInfo<ItemType::node>>,
                                                std::unique_ptr<ExchangeItemTypeInfo<ItemType::edge>>,
                                                std::unique_ptr<ExchangeItemTypeInfo<ItemType::face>>,
                                                std::unique_ptr<ExchangeItemTypeInfo<ItemType::cell>>>;

  ExchangeItemInfoRepository m_requested_item_info_list;
  ExchangeItemInfoRepository m_provided_item_info_list;

  // here 12 is the maximum number (3d) of sub item of item
  // configurations (cell->edge, face->node...)
  using ExchangeSubItemPerItemTotalSizeList = std::array<std::unique_ptr<std::vector<size_t>>, 12>;
  using SubItemPerItemProvidedList          = std::array<std::unique_ptr<std::vector<Array<const size_t>>>, 12>;
  using NumberOfSubItemPerItemProvidedList  = std::array<std::unique_ptr<std::vector<Array<const size_t>>>, 12>;

  ExchangeSubItemPerItemTotalSizeList m_sub_item_per_item_requested_total_size_list;
  ExchangeSubItemPerItemTotalSizeList m_sub_item_per_item_provided_total_size_list;
  SubItemPerItemProvidedList m_sub_item_per_item_provided_list;
  NumberOfSubItemPerItemProvidedList m_number_of_sub_item_per_item_provided_list;

  template <typename ConnectivityType, ItemType item_type>
  void
  _buildSynchronizeInfoIfNeeded(const ConnectivityType& connectivity)
  {
    const auto& item_owner = connectivity.template owner<item_type>();
    using ItemId           = ItemIdT<item_type>;

    auto& p_requested_item_info = std::get<static_cast<int>(item_type)>(m_requested_item_info_list);
    auto& p_provided_item_info  = std::get<static_cast<int>(item_type)>(m_provided_item_info_list);

    Assert(static_cast<bool>(p_provided_item_info) == static_cast<bool>(p_requested_item_info));

    if (not p_provided_item_info) {
      p_requested_item_info = [&]() {
        std::vector<std::vector<ItemId>> requested_item_vector_info(parallel::size());
        for (ItemId item_id = 0; item_id < item_owner.numberOfItems(); ++item_id) {
          if (const size_t owner = item_owner[item_id]; owner != parallel::rank()) {
            requested_item_vector_info[owner].emplace_back(item_id);
          }
        }
        ExchangeItemTypeInfo<item_type> requested_item_info(parallel::size());
        for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
          const auto& requested_item_vector = requested_item_vector_info[i_rank];
          requested_item_info[i_rank]       = convert_to_array(requested_item_vector);
        }
        return std::make_unique<ExchangeItemTypeInfo<item_type>>(std::move(requested_item_info));
      }();

      auto& requested_item_info = *p_requested_item_info;

      Array<unsigned int> local_number_of_requested_values(parallel::size());
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        local_number_of_requested_values[i_rank] = requested_item_info[i_rank].size();
      }

      Array<unsigned int> local_number_of_values_to_send = parallel::allToAll(local_number_of_requested_values);

      std::vector<Array<const int>> requested_item_number_list_by_proc(parallel::size());
      const auto& item_number = connectivity.template number<item_type>();
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        const auto& requested_item_info_from_rank = requested_item_info[i_rank];
        Array<int> item_number_list{requested_item_info_from_rank.size()};
        parallel_for(
          requested_item_info_from_rank.size(), PUGS_LAMBDA(size_t i_item) {
            item_number_list[i_item] = item_number[requested_item_info_from_rank[i_item]];
          });
        requested_item_number_list_by_proc[i_rank] = item_number_list;
      }

      std::vector<Array<int>> provided_item_number_list_by_rank(parallel::size());
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        provided_item_number_list_by_rank[i_rank] = Array<int>{local_number_of_values_to_send[i_rank]};
      }

      parallel::exchange(requested_item_number_list_by_proc, provided_item_number_list_by_rank);

      std::unordered_map<int, ItemId> item_number_to_id_correspondance;
      for (ItemId item_id = 0; item_id < item_number.numberOfItems(); ++item_id) {
        item_number_to_id_correspondance[item_number[item_id]] = item_id;
      }

      p_provided_item_info = [&]() {
        ExchangeItemTypeInfo<item_type> provided_item_info(parallel::size());
        for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
          Array<ItemId> provided_item_id_to_rank{local_number_of_values_to_send[i_rank]};
          const Array<int>& provided_item_number_to_rank = provided_item_number_list_by_rank[i_rank];
          for (size_t i = 0; i < provided_item_number_to_rank.size(); ++i) {
            provided_item_id_to_rank[i] =
              item_number_to_id_correspondance.find(provided_item_number_to_rank[i])->second;
          }
          provided_item_info[i_rank] = provided_item_id_to_rank;
        }
        return std::make_unique<ExchangeItemTypeInfo<item_type>>(provided_item_info);
      }();
    }
  }

  template <typename ConnectivityType, ItemType item_type>
  PUGS_INLINE constexpr auto&
  _getRequestedItemInfo(const ConnectivityType& connectivity)
  {
    this->_buildSynchronizeInfoIfNeeded<ConnectivityType, item_type>(connectivity);
    return *std::get<static_cast<int>(item_type)>(m_requested_item_info_list);
  }

  template <typename ConnectivityType, ItemType item_type>
  PUGS_INLINE constexpr auto&
  _getProvidedItemInfo(const ConnectivityType& connectivity)
  {
    this->_buildSynchronizeInfoIfNeeded<ConnectivityType, item_type>(connectivity);
    return *std::get<static_cast<int>(item_type)>(m_provided_item_info_list);
  }

  template <ItemType item_type, ItemType sub_item_type, typename ConnectivityType>
  PUGS_INLINE const std::vector<size_t>&
  _getSubItemPerItemRequestedTotalSize(const ConnectivityType& connectivity)
  {
    auto& p_sub_item_per_item_requested_total_size =
      m_sub_item_per_item_requested_total_size_list[item_of_item_type_index<sub_item_type, item_type>];
    if (not p_sub_item_per_item_requested_total_size) {
      std::vector<size_t> sub_item_per_item_requested_total_size(parallel::size());
      const auto& requested_item_info = this->_getRequestedItemInfo<ConnectivityType, item_type>(connectivity);
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        const auto& requested_item_info_from_rank = requested_item_info[i_rank];

        const auto& item_to_item_matrix = connectivity.template getItemToItemMatrix<item_type, sub_item_type>();

        size_t count = 0;
        for (size_t i = 0; i < requested_item_info_from_rank.size(); ++i) {
          count += item_to_item_matrix[requested_item_info_from_rank[i]].size();
        }

        sub_item_per_item_requested_total_size[i_rank] = count;
      }
      p_sub_item_per_item_requested_total_size =
        std::make_unique<std::vector<size_t>>(std::move(sub_item_per_item_requested_total_size));
    }

    return (*p_sub_item_per_item_requested_total_size);
  }

  template <ItemType item_type, ItemType sub_item_type, typename ConnectivityType>
  PUGS_INLINE const std::vector<size_t>&
  _getSubItemPerItemProvidedTotalSize(const ConnectivityType& connectivity)
  {
    static_assert(item_type != sub_item_type);

    auto& p_sub_item_per_item_provided_total_size =
      m_sub_item_per_item_provided_total_size_list[item_of_item_type_index<sub_item_type, item_type>];

    if (not p_sub_item_per_item_provided_total_size) {
      if constexpr (ItemTypeId<ConnectivityType::Dimension>::dimension(item_type) >
                    ItemTypeId<ConnectivityType::Dimension>::dimension(sub_item_type)) {
        std::vector<size_t> sub_item_per_item_provided_total_size(parallel::size());
        const auto& provided_item_info = this->_getProvidedItemInfo<ConnectivityType, item_type>(connectivity);
        for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
          const auto& provided_item_info_from_rank = provided_item_info[i_rank];

          const auto& item_to_item_matrix = connectivity.template getItemToItemMatrix<item_type, sub_item_type>();

          size_t count = 0;
          for (size_t i = 0; i < provided_item_info_from_rank.size(); ++i) {
            count += item_to_item_matrix[provided_item_info_from_rank[i]].size();
          }

          sub_item_per_item_provided_total_size[i_rank] = count;
        }
        p_sub_item_per_item_provided_total_size =
          std::make_unique<std::vector<size_t>>(std::move(sub_item_per_item_provided_total_size));
      } else {
        std::vector<size_t> sub_item_per_item_provided_total_size(parallel::size());

        const auto& sub_item_required_total_size =
          _getSubItemPerItemRequestedTotalSize<item_type, sub_item_type>(connectivity);
        const auto& requested_item_info = this->_getRequestedItemInfo<ConnectivityType, item_type>(connectivity);

        std::vector<Array<size_t>> sub_item_required_total_size_exchange(parallel::size());
        for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
          Assert((sub_item_required_total_size[i_rank] == 0) xor (requested_item_info[i_rank].size() > 0),
                 "unexpected sub_item size info");
          if (requested_item_info[i_rank].size() > 0) {
            Array<size_t> size_0d_array(1);
            size_0d_array[0]                              = sub_item_required_total_size[i_rank];
            sub_item_required_total_size_exchange[i_rank] = size_0d_array;
          }
        }

        const auto& provided_item_info = this->_getProvidedItemInfo<ConnectivityType, item_type>(connectivity);
        std::vector<Array<size_t>> sub_item_provided_total_size_exchange(parallel::size());
        for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
          if (provided_item_info[i_rank].size() > 0) {
            Array<size_t> size_0d_array(1);
            sub_item_provided_total_size_exchange[i_rank] = size_0d_array;
          }
        }

        parallel::exchange(sub_item_required_total_size_exchange, sub_item_provided_total_size_exchange);

        for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
          if (sub_item_provided_total_size_exchange[i_rank].size() > 0) {
            sub_item_per_item_provided_total_size[i_rank] = sub_item_provided_total_size_exchange[i_rank][0];
          }
        }

        p_sub_item_per_item_provided_total_size =
          std::make_unique<std::vector<size_t>>(std::move(sub_item_per_item_provided_total_size));
      }
    }

    return (*p_sub_item_per_item_provided_total_size);
  }

  template <ItemType item_type, ItemType sub_item_type, typename ConnectivityType>
  PUGS_INLINE const std::vector<Array<const size_t>>&
  _getNumberOfSubItemPerItemProvidedList(const ConnectivityType& connectivity)
  {
    static_assert(ItemTypeId<ConnectivityType::Dimension>::dimension(sub_item_type) >
                    ItemTypeId<ConnectivityType::Dimension>::dimension(item_type),
                  "should not be called if dimension of sub item is lower than item");

    auto& p_number_of_sub_item_per_item_provided_list =
      m_number_of_sub_item_per_item_provided_list[item_of_item_type_index<sub_item_type, item_type>];

    if (not p_number_of_sub_item_per_item_provided_list) {
      using ItemId = ItemIdT<item_type>;

      const auto& item_to_item_matrix = connectivity.template getItemToItemMatrix<item_type, sub_item_type>();
      const auto number_of_sub_item   = connectivity.template number<sub_item_type>();

      const auto& requested_item_info = this->_getRequestedItemInfo<ConnectivityType, item_type>(connectivity);

      std::vector<Array<size_t>> number_of_sub_item_per_item_required_exchange(parallel::size());
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        const auto& requested_item_info_from_rank = requested_item_info[i_rank];
        if (requested_item_info_from_rank.size() > 0) {
          Array<size_t> number_of_sub_item_per_item(requested_item_info_from_rank.size());

          size_t count = 0;
          for (size_t i_item = 0; i_item < requested_item_info_from_rank.size(); ++i_item) {
            const ItemId item_id                 = requested_item_info_from_rank[i_item];
            number_of_sub_item_per_item[count++] = item_to_item_matrix[item_id].size();
          }
          number_of_sub_item_per_item_required_exchange[i_rank] = number_of_sub_item_per_item;
        }
      }

      const auto& provided_item_info = this->_getProvidedItemInfo<ConnectivityType, item_type>(connectivity);
      std::vector<Array<size_t>> number_of_sub_item_per_item_provided_exchange(parallel::size());
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        if (provided_item_info[i_rank].size() > 0) {
          number_of_sub_item_per_item_provided_exchange[i_rank] = Array<size_t>{provided_item_info[i_rank].size()};
        }
      }

      parallel::exchange(number_of_sub_item_per_item_required_exchange, number_of_sub_item_per_item_provided_exchange);

      std::vector<Array<const size_t>> number_of_sub_item_per_item_provided_list(parallel::size());
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        number_of_sub_item_per_item_provided_list[i_rank] = number_of_sub_item_per_item_provided_exchange[i_rank];
      }
      p_number_of_sub_item_per_item_provided_list =
        std::make_unique<std::vector<Array<const size_t>>>(std::move(number_of_sub_item_per_item_provided_list));
    }
    return *p_number_of_sub_item_per_item_provided_list;
  }

  template <ItemType item_type, ItemType sub_item_type, typename ConnectivityType>
  PUGS_INLINE const std::vector<Array<const size_t>>&
  _getSubItemPerItemProvidedList(const ConnectivityType& connectivity)
  {
    static_assert(ItemTypeId<ConnectivityType::Dimension>::dimension(sub_item_type) >
                    ItemTypeId<ConnectivityType::Dimension>::dimension(item_type),
                  "should not be called if dimension of sub item is lower than item");
    auto& p_sub_item_per_item_provided_list =
      m_sub_item_per_item_provided_list[item_of_item_type_index<sub_item_type, item_type>];

    if (not p_sub_item_per_item_provided_list) {
      using ItemId = ItemIdT<item_type>;

      const auto& item_to_item_matrix = connectivity.template getItemToItemMatrix<item_type, sub_item_type>();
      const auto number_of_sub_item   = connectivity.template number<sub_item_type>();

      const auto& sub_item_required_size = _getSubItemPerItemRequestedTotalSize<item_type, sub_item_type>(connectivity);
      const auto& requested_item_info    = this->_getRequestedItemInfo<ConnectivityType, item_type>(connectivity);

      std::vector<Array<int>> sub_item_per_item_required_numbers_exchange(parallel::size());
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        Assert((sub_item_required_size[i_rank] == 0) xor (requested_item_info[i_rank].size() > 0),
               "unexpected sub_item size info");
        if (requested_item_info[i_rank].size() > 0) {
          Array<int> sub_item_numbers(sub_item_required_size[i_rank]);

          size_t count                              = 0;
          const auto& requested_item_info_from_rank = requested_item_info[i_rank];
          for (size_t i_item = 0; i_item < requested_item_info_from_rank.size(); ++i_item) {
            const ItemId item_id = requested_item_info_from_rank[i_item];
            auto item_sub_items  = item_to_item_matrix[item_id];
            for (size_t i_sub_item = 0; i_sub_item < item_sub_items.size(); ++i_sub_item) {
              sub_item_numbers[count++] = number_of_sub_item[item_sub_items[i_sub_item]];
            }
          }
          Assert(count == sub_item_numbers.size());
          sub_item_per_item_required_numbers_exchange[i_rank] = sub_item_numbers;
        }
      }

      const auto& provided_item_info     = this->_getProvidedItemInfo<ConnectivityType, item_type>(connectivity);
      const auto& sub_item_provided_size = _getSubItemPerItemProvidedTotalSize<item_type, sub_item_type>(connectivity);
      std::vector<Array<int>> sub_item_per_item_provided_numbers_exchange(parallel::size());
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        if (provided_item_info[i_rank].size() > 0) {
          sub_item_per_item_provided_numbers_exchange[i_rank] = Array<int>{sub_item_provided_size[i_rank]};
        }
      }

      parallel::exchange(sub_item_per_item_required_numbers_exchange, sub_item_per_item_provided_numbers_exchange);

      const auto& number_of_sub_item_per_item_provided_list =
        this->_getNumberOfSubItemPerItemProvidedList<item_type, sub_item_type>(connectivity);

      std::vector<Array<const size_t>> sub_item_per_item_provided_list(parallel::size());
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        if (provided_item_info[i_rank].size() > 0) {
          const auto& sub_item_numbers            = sub_item_per_item_provided_numbers_exchange[i_rank];
          const auto& number_of_sub_item_per_item = number_of_sub_item_per_item_provided_list[i_rank];
          Array<size_t> sub_item_list{sub_item_provided_size[i_rank]};
          size_t count = 0;

          const auto& provided_item_info_to_rank = provided_item_info[i_rank];
          for (size_t i_item = 0; i_item < provided_item_info_to_rank.size(); ++i_item) {
            const ItemId item_id = provided_item_info_to_rank[i_item];
            auto item_sub_items  = item_to_item_matrix[item_id];
            bool found           = false;
            for (size_t i_sub_item = 0, i_requied_sub_item = 0; i_sub_item < item_sub_items.size(); ++i_sub_item) {
              found      = false;
              int number = sub_item_numbers[count];
              if (number == number_of_sub_item[item_sub_items[i_sub_item]]) {
                found                = true;
                sub_item_list[count] = i_sub_item;
                i_requied_sub_item++;
                count++;
                if (i_requied_sub_item == number_of_sub_item_per_item[i_item]) {
                  break;
                }
              }
            }
            Assert(found, "something wierd occured");
          }

          Assert(count == sub_item_list.size());
          sub_item_per_item_provided_list[i_rank] = sub_item_list;
        }
      }

      p_sub_item_per_item_provided_list =
        std::make_unique<std::vector<Array<const size_t>>>(std::move(sub_item_per_item_provided_list));
    }

    return (*p_sub_item_per_item_provided_list);
  }

  template <typename ConnectivityType, typename DataType, ItemType item_type, typename ConnectivityPtr>
  PUGS_INLINE void
  _synchronize(const ConnectivityType& connectivity, ItemValue<DataType, item_type, ConnectivityPtr>& item_value)
  {
    static_assert(not std::is_abstract_v<ConnectivityType>, "_synchronize must be called on a concrete connectivity");

    using ItemId = ItemIdT<item_type>;

    const auto& provided_item_info  = this->_getProvidedItemInfo<ConnectivityType, item_type>(connectivity);
    const auto& requested_item_info = this->_getRequestedItemInfo<ConnectivityType, item_type>(connectivity);

    std::vector<Array<const DataType>> provided_data_list(parallel::size());
    for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
      const Array<const ItemId>& provided_item_info_to_rank = provided_item_info[i_rank];
      Array<DataType> provided_data{provided_item_info_to_rank.size()};
      parallel_for(
        provided_item_info_to_rank.size(),
        PUGS_LAMBDA(size_t i) { provided_data[i] = item_value[provided_item_info_to_rank[i]]; });
      provided_data_list[i_rank] = provided_data;
    }

    std::vector<Array<DataType>> requested_data_list(parallel::size());
    for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
      const auto& requested_item_info_from_rank = requested_item_info[i_rank];
      requested_data_list[i_rank]               = Array<DataType>{requested_item_info_from_rank.size()};
    }

    parallel::exchange(provided_data_list, requested_data_list);

    for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
      const auto& requested_item_info_from_rank = requested_item_info[i_rank];
      const auto& requested_data                = requested_data_list[i_rank];
      parallel_for(
        requested_item_info_from_rank.size(),
        PUGS_LAMBDA(size_t i) { item_value[requested_item_info_from_rank[i]] = requested_data[i]; });
    }
  }

  template <typename ConnectivityType, typename DataType, ItemType item_type, typename ConnectivityPtr>
  PUGS_INLINE void
  _synchronize(const ConnectivityType& connectivity, ItemArray<DataType, item_type, ConnectivityPtr>& item_array)
  {
    static_assert(not std::is_abstract_v<ConnectivityType>, "_synchronize must be called on a concrete connectivity");

    using ItemId = ItemIdT<item_type>;

    const auto& provided_item_info  = this->_getProvidedItemInfo<ConnectivityType, item_type>(connectivity);
    const auto& requested_item_info = this->_getRequestedItemInfo<ConnectivityType, item_type>(connectivity);

    Assert(requested_item_info.size() == provided_item_info.size());
    const size_t size_of_arrays = item_array.sizeOfArrays();

    std::vector<Array<const DataType>> provided_data_list(parallel::size());
    for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
      const Array<const ItemId>& provided_item_info_to_rank = provided_item_info[i_rank];
      Array<DataType> provided_data{provided_item_info_to_rank.size() * size_of_arrays};
      parallel_for(
        provided_item_info_to_rank.size(), PUGS_LAMBDA(size_t i) {
          const size_t j   = i * size_of_arrays;
          const auto array = item_array[provided_item_info_to_rank[i]];
          for (size_t k = 0; k < size_of_arrays; ++k) {
            provided_data[j + k] = array[k];
          }
        });
      provided_data_list[i_rank] = provided_data;
    }

    std::vector<Array<DataType>> requested_data_list(parallel::size());
    for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
      const auto& requested_item_info_from_rank = requested_item_info[i_rank];
      requested_data_list[i_rank] = Array<DataType>{requested_item_info_from_rank.size() * size_of_arrays};
    }

    parallel::exchange(provided_data_list, requested_data_list);

    for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
      const auto& requested_item_info_from_rank = requested_item_info[i_rank];
      const auto& requested_data                = requested_data_list[i_rank];
      parallel_for(
        requested_item_info_from_rank.size(), PUGS_LAMBDA(size_t i) {
          const size_t j = i * size_of_arrays;
          auto array     = item_array[requested_item_info_from_rank[i]];
          for (size_t k = 0; k < size_of_arrays; ++k) {
            array[k] = requested_data[j + k];
          }
        });
    }
  }

  template <typename ConnectivityType, typename DataType, typename ItemOfItem, typename ConnectivityPtr>
  PUGS_INLINE void
  _synchronize(const ConnectivityType& connectivity,
               SubItemValuePerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_value_per_item)
  {
    static_assert(ItemOfItem::item_type != ItemOfItem::sub_item_type);
    static_assert(not std::is_abstract_v<ConnectivityType>, "_synchronize must be called on a concrete connectivity");

    constexpr ItemType item_type     = ItemOfItem::item_type;
    constexpr ItemType sub_item_type = ItemOfItem::sub_item_type;

    using ItemId = ItemIdT<item_type>;

    const auto& provided_item_info  = this->_getProvidedItemInfo<ConnectivityType, item_type>(connectivity);
    const auto& requested_item_info = this->_getRequestedItemInfo<ConnectivityType, item_type>(connectivity);

    const auto& sub_item_per_item_provided_size =
      _getSubItemPerItemProvidedTotalSize<item_type, sub_item_type>(connectivity);

    std::vector<Array<const DataType>> provided_data_list(parallel::size());

    if constexpr (ItemTypeId<ConnectivityType::Dimension>::dimension(ItemOfItem::item_type) >
                  ItemTypeId<ConnectivityType::Dimension>::dimension(ItemOfItem::sub_item_type)) {
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        const Array<const ItemId>& provided_item_info_to_rank = provided_item_info[i_rank];
        Array<DataType> provided_data{sub_item_per_item_provided_size[i_rank]};
        size_t index = 0;
        for (size_t i = 0; i < provided_item_info_to_rank.size(); ++i) {
          const ItemId item_id   = provided_item_info_to_rank[i];
          const auto item_values = sub_item_value_per_item.itemArray(item_id);
          for (size_t j = 0; j < item_values.size(); ++j) {
            provided_data[index++] = item_values[j];
          }
        }
        provided_data_list[i_rank] = provided_data;
      }
    } else if constexpr (ItemTypeId<ConnectivityType::Dimension>::dimension(ItemOfItem::item_type) <
                         ItemTypeId<ConnectivityType::Dimension>::dimension(ItemOfItem::sub_item_type)) {
      const auto& number_of_sub_item_per_item_provided_list =
        this->_getNumberOfSubItemPerItemProvidedList<item_type, sub_item_type>(connectivity);
      const auto& sub_item_per_item_provided_list =
        this->_getSubItemPerItemProvidedList<item_type, sub_item_type>(connectivity);

      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        const Array<const ItemId>& provided_item_info_to_rank              = provided_item_info[i_rank];
        const Array<const size_t>& sub_item_per_item_provided_list_to_rank = sub_item_per_item_provided_list[i_rank];
        const Array<const size_t>& number_of_sub_item_per_item_provided_list_to_rank =
          number_of_sub_item_per_item_provided_list[i_rank];

        Array<DataType> provided_data{sub_item_per_item_provided_size[i_rank]};
        size_t index = 0;
        for (size_t i = 0; i < provided_item_info_to_rank.size(); ++i) {
          const ItemId item_id   = provided_item_info_to_rank[i];
          const auto item_values = sub_item_value_per_item.itemArray(item_id);
          for (size_t j = 0; j < number_of_sub_item_per_item_provided_list_to_rank[i]; ++j, ++index) {
            provided_data[index] = item_values[sub_item_per_item_provided_list_to_rank[index]];
          }
        }
        provided_data_list[i_rank] = provided_data;
      }
    }

    const auto& sub_item_per_item_requested_size =
      _getSubItemPerItemRequestedTotalSize<item_type, sub_item_type>(connectivity);

    std::vector<Array<DataType>> requested_data_list(parallel::size());
    for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
      requested_data_list[i_rank] = Array<DataType>{sub_item_per_item_requested_size[i_rank]};
    }

    parallel::exchange(provided_data_list, requested_data_list);

    for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
      const auto& requested_item_info_from_rank = requested_item_info[i_rank];
      const auto& requested_data                = requested_data_list[i_rank];

      size_t index = 0;
      for (size_t i = 0; i < requested_item_info_from_rank.size(); ++i) {
        const ItemId item_id       = requested_item_info_from_rank[i];
        const auto sub_item_values = sub_item_value_per_item.itemArray(item_id);
        for (size_t j = 0; j < sub_item_values.size(); ++j) {
          sub_item_values[j] = requested_data[index++];
        }
      }
    }
  }

  template <typename ConnectivityType, typename DataType, typename ItemOfItem, typename ConnectivityPtr>
  PUGS_INLINE void
  _synchronize(const ConnectivityType& connectivity,
               SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_array_per_item)
  {
    static_assert(ItemOfItem::item_type != ItemOfItem::sub_item_type);
    static_assert(not std::is_abstract_v<ConnectivityType>, "_synchronize must be called on a concrete connectivity");

    constexpr ItemType item_type     = ItemOfItem::item_type;
    constexpr ItemType sub_item_type = ItemOfItem::sub_item_type;

    using ItemId = ItemIdT<item_type>;

    const auto& provided_item_info  = this->_getProvidedItemInfo<ConnectivityType, item_type>(connectivity);
    const auto& requested_item_info = this->_getRequestedItemInfo<ConnectivityType, item_type>(connectivity);

    const auto& sub_item_per_item_provided_size =
      _getSubItemPerItemProvidedTotalSize<item_type, sub_item_type>(connectivity);

    std::vector<Array<const DataType>> provided_data_list(parallel::size());

    if constexpr (ItemTypeId<ConnectivityType::Dimension>::dimension(ItemOfItem::item_type) >
                  ItemTypeId<ConnectivityType::Dimension>::dimension(ItemOfItem::sub_item_type)) {
      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        const Array<const ItemId>& provided_item_info_to_rank = provided_item_info[i_rank];

        Array<DataType> provided_data{sub_item_per_item_provided_size[i_rank] * sub_item_array_per_item.sizeOfArrays()};
        size_t index = 0;
        for (size_t i = 0; i < provided_item_info_to_rank.size(); ++i) {
          const ItemId item_id  = provided_item_info_to_rank[i];
          const auto item_table = sub_item_array_per_item.itemTable(item_id);
          for (size_t j = 0; j < item_table.numberOfRows(); ++j) {
            Assert(item_table.numberOfColumns() == sub_item_array_per_item.sizeOfArrays());
            for (size_t k = 0; k < sub_item_array_per_item.sizeOfArrays(); ++k) {
              provided_data[index++] = item_table(j, k);
            }
          }
        }
        provided_data_list[i_rank] = provided_data;
      }
    } else if constexpr (ItemTypeId<ConnectivityType::Dimension>::dimension(ItemOfItem::item_type) <
                         ItemTypeId<ConnectivityType::Dimension>::dimension(ItemOfItem::sub_item_type)) {
      const auto& number_of_sub_item_per_item_provided_list =
        this->_getNumberOfSubItemPerItemProvidedList<item_type, sub_item_type>(connectivity);
      const auto& sub_item_per_item_provided_list =
        this->_getSubItemPerItemProvidedList<item_type, sub_item_type>(connectivity);

      for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
        const Array<const ItemId>& provided_item_info_to_rank              = provided_item_info[i_rank];
        const Array<const size_t>& sub_item_per_item_provided_list_to_rank = sub_item_per_item_provided_list[i_rank];
        const Array<const size_t>& number_of_sub_item_per_item_provided_list_to_rank =
          number_of_sub_item_per_item_provided_list[i_rank];

        Array<DataType> provided_data{sub_item_per_item_provided_size[i_rank] * sub_item_array_per_item.sizeOfArrays()};
        size_t index = 0;
        for (size_t i = 0; i < provided_item_info_to_rank.size(); ++i) {
          const ItemId item_id  = provided_item_info_to_rank[i];
          const auto item_table = sub_item_array_per_item.itemTable(item_id);
          for (size_t j = 0; j < number_of_sub_item_per_item_provided_list_to_rank[i]; ++j, ++index) {
            Assert(item_table.numberOfColumns() == sub_item_array_per_item.sizeOfArrays());
            for (size_t k = 0; k < sub_item_array_per_item.sizeOfArrays(); ++k) {
              provided_data[sub_item_array_per_item.sizeOfArrays() * index + k] =
                item_table(sub_item_per_item_provided_list_to_rank[index], k);
            }
          }
        }
        Assert(index == sub_item_per_item_provided_list_to_rank.size());
        provided_data_list[i_rank] = provided_data;
      }
    }

    const auto& sub_item_per_item_requested_size =
      _getSubItemPerItemRequestedTotalSize<item_type, sub_item_type>(connectivity);

    std::vector<Array<DataType>> requested_data_list(parallel::size());
    for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
      requested_data_list[i_rank] =
        Array<DataType>{sub_item_per_item_requested_size[i_rank] * sub_item_array_per_item.sizeOfArrays()};
    }

    parallel::exchange(provided_data_list, requested_data_list);
    for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
      const auto& requested_item_info_from_rank = requested_item_info[i_rank];
      const auto& requested_data                = requested_data_list[i_rank];

      size_t index = 0;
      for (size_t i = 0; i < requested_item_info_from_rank.size(); ++i) {
        const ItemId item_id  = requested_item_info_from_rank[i];
        const auto item_table = sub_item_array_per_item.itemTable(item_id);
        for (size_t j = 0; j < item_table.numberOfRows(); ++j) {
          Assert(item_table.numberOfColumns() == sub_item_array_per_item.sizeOfArrays());
          for (size_t k = 0; k < sub_item_array_per_item.sizeOfArrays(); ++k) {
            item_table(j, k) = requested_data[index++];
          }
        }
      }
    }
  }

 public:
  template <typename DataType, ItemType item_type, typename ConnectivityPtr>
  PUGS_INLINE void
  synchronize(ItemValue<DataType, item_type, ConnectivityPtr>& item_value)
  {
    static_assert(not std::is_const_v<DataType>, "cannot synchronize ItemValue of const data");
    Assert(item_value.connectivity_ptr().use_count() > 0, "No connectivity is associated to this ItemValue");
    const IConnectivity& connectivity = *item_value.connectivity_ptr();

    switch (connectivity.dimension()) {
    case 1: {
      this->_synchronize(static_cast<const Connectivity1D&>(connectivity), item_value);
      break;
    }
    case 2: {
      this->_synchronize(static_cast<const Connectivity2D&>(connectivity), item_value);
      break;
    }
    case 3: {
      this->_synchronize(static_cast<const Connectivity3D&>(connectivity), item_value);
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("unexpected dimension");
    }
      // LCOV_EXCL_STOP
    }
  }

  template <typename DataType, ItemType item_type, typename ConnectivityPtr>
  PUGS_INLINE void
  synchronize(ItemArray<DataType, item_type, ConnectivityPtr>& item_array)
  {
    static_assert(not std::is_const_v<DataType>, "cannot synchronize ItemArray of const data");
    Assert(item_array.connectivity_ptr().use_count() > 0, "No connectivity is associated to this ItemArray");
    const IConnectivity& connectivity = *item_array.connectivity_ptr();

    switch (connectivity.dimension()) {
    case 1: {
      this->_synchronize(static_cast<const Connectivity1D&>(connectivity), item_array);
      break;
    }
    case 2: {
      this->_synchronize(static_cast<const Connectivity2D&>(connectivity), item_array);
      break;
    }
    case 3: {
      this->_synchronize(static_cast<const Connectivity3D&>(connectivity), item_array);
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("unexpected dimension");
    }
      // LCOV_EXCL_STOP
    }
  }

  template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
  PUGS_INLINE void
  synchronize(SubItemValuePerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_value_per_item)
  {
    static_assert(not std::is_const_v<DataType>, "cannot synchronize SubItemValuePerItem of const data");
    Assert(sub_item_value_per_item.connectivity_ptr().use_count() > 0,
           "No connectivity is associated to this SubItemValuePerItem");

    const IConnectivity& connectivity = *sub_item_value_per_item.connectivity_ptr();

    switch (connectivity.dimension()) {
    case 1: {
      this->_synchronize(static_cast<const Connectivity1D&>(connectivity), sub_item_value_per_item);
      break;
    }
    case 2: {
      this->_synchronize(static_cast<const Connectivity2D&>(connectivity), sub_item_value_per_item);
      break;
    }
    case 3: {
      this->_synchronize(static_cast<const Connectivity3D&>(connectivity), sub_item_value_per_item);
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("unexpected dimension");
    }
      // LCOV_EXCL_STOP
    }
  }

  template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
  PUGS_INLINE void
  synchronize(SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_value_per_item)
  {
    static_assert(not std::is_const_v<DataType>, "cannot synchronize SubItemArrayPerItem of const data");
    Assert(sub_item_value_per_item.connectivity_ptr().use_count() > 0,
           "No connectivity is associated to this SubItemValuePerItem");

    const IConnectivity& connectivity = *sub_item_value_per_item.connectivity_ptr();

    switch (connectivity.dimension()) {
    case 1: {
      this->_synchronize(static_cast<const Connectivity1D&>(connectivity), sub_item_value_per_item);
      break;
    }
    case 2: {
      this->_synchronize(static_cast<const Connectivity2D&>(connectivity), sub_item_value_per_item);
      break;
    }
    case 3: {
      this->_synchronize(static_cast<const Connectivity3D&>(connectivity), sub_item_value_per_item);
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("unexpected dimension");
    }
      // LCOV_EXCL_STOP
    }
  }

  Synchronizer(const Synchronizer&) = delete;
  Synchronizer(Synchronizer&&)      = delete;

 private:
  friend class SynchronizerManager;

  PUGS_INLINE
  Synchronizer()
  {
    ;
  }
};

#else   // PUGS_HAS_MPI

class Synchronizer
{
 public:
  template <typename DataType, ItemType item_type, typename ConnectivityPtr>
  PUGS_INLINE void
  synchronize(ItemValue<DataType, item_type, ConnectivityPtr>& item_value)
  {
    static_assert(not std::is_const_v<DataType>, "cannot synchronize ItemValue of const data");
    Assert(item_value.connectivity_ptr().use_count() > 0, "No connectivity is associated to this ItemValue");
  }

  template <typename DataType, ItemType item_type, typename ConnectivityPtr>
  PUGS_INLINE void
  synchronize(ItemArray<DataType, item_type, ConnectivityPtr>& item_value)
  {
    static_assert(not std::is_const_v<DataType>, "cannot synchronize ItemArray of const data");
    Assert(item_value.connectivity_ptr().use_count() > 0, "No connectivity is associated to this ItemValue");
  }

  template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
  PUGS_INLINE void
  synchronize(SubItemValuePerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_value_per_item)
  {
    static_assert(not std::is_const_v<DataType>, "cannot synchronize SubItemValuePerItem of const data");
    Assert(sub_item_value_per_item.connectivity_ptr().use_count() > 0,
           "No connectivity is associated to this SubItemValuePerItem");
  }

  template <typename DataType, typename ItemOfItem, typename ConnectivityPtr>
  PUGS_INLINE void
  synchronize(SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>& sub_item_array_per_item)
  {
    static_assert(not std::is_const_v<DataType>, "cannot synchronize SubItemArrayPerItem of const data");
    Assert(sub_item_array_per_item.connectivity_ptr().use_count() > 0,
           "No connectivity is associated to this SubItemArrayPerItem");
  }

  Synchronizer(const Synchronizer&) = delete;
  Synchronizer(Synchronizer&&)      = delete;

 private:
  friend class SynchronizerManager;

  PUGS_INLINE
  Synchronizer()
  {
    ;
  }
};

#endif   // PUGS_HAS_MPI

#endif   // SYNCHRONIZER_HPP
