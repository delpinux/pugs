#ifndef MEDIAN_DUAL_CONNECTIVITY_BUILDER_HPP
#define MEDIAN_DUAL_CONNECTIVITY_BUILDER_HPP

#include <mesh/ConnectivityBuilderBase.hpp>
#include <mesh/IPrimalToDualConnectivityDataMapper.hpp>
#include <mesh/ItemIdToItemIdMap.hpp>

#include <memory>

template <size_t>
class Connectivity;
class ConnectivityDescriptor;

class MedianDualConnectivityBuilder : public ConnectivityBuilderBase
{
 private:
  FaceIdToNodeIdMap m_primal_face_to_dual_node_map;
  CellIdToNodeIdMap m_primal_cell_to_dual_node_map;
  NodeIdToNodeIdMap m_primal_boundary_node_to_dual_node_map;
  NodeIdToCellIdMap m_primal_node_to_dual_cell_map;

  std::shared_ptr<IPrimalToDualConnectivityDataMapper> m_mapper;

  template <size_t Dimension>
  void _buildConnectivityDescriptor(const Connectivity<Dimension>&, ConnectivityDescriptor&);

  template <size_t Dimension>
  void _buildConnectivityFrom(const IConnectivity&);

  friend class DualConnectivityManager;
  MedianDualConnectivityBuilder(const IConnectivity&);

 public:
  std::shared_ptr<IPrimalToDualConnectivityDataMapper>
  mapper() const
  {
    return m_mapper;
  }

  ~MedianDualConnectivityBuilder() = default;
};

#endif   // MEDIAN_DUAL_CONNECTIVITY_BUILDER_HPP
