#ifndef NUMBERED_BOUNDARY_DESCRIPTOR_HPP
#define NUMBERED_BOUNDARY_DESCRIPTOR_HPP

#include <mesh/IBoundaryDescriptor.hpp>

#include <iostream>

class NumberedBoundaryDescriptor final : public IBoundaryDescriptor
{
 private:
  unsigned int m_number;

  std::ostream&
  _write(std::ostream& os) const final
  {
    os << '"' << m_number << '"';
    return os;
  }

  [[nodiscard]] bool
  operator==(const RefId& ref_id) const final
  {
    return m_number == ref_id.tagNumber();
  }

 public:
  [[nodiscard]] unsigned int
  number() const
  {
    return m_number;
  }

  [[nodiscard]] Type
  type() const final
  {
    return Type::numbered;
  }

  NumberedBoundaryDescriptor(const NumberedBoundaryDescriptor&) = delete;
  NumberedBoundaryDescriptor(NumberedBoundaryDescriptor&&)      = delete;
  NumberedBoundaryDescriptor(unsigned int number) : m_number(number)
  {
    ;
  }
  virtual ~NumberedBoundaryDescriptor() = default;
};

#endif   // NUMBERED_BOUNDARY_DESCRIPTOR_HPP
