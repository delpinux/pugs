#ifndef DIAMOND_DUAL_MESH_BUILDER_HPP
#define DIAMOND_DUAL_MESH_BUILDER_HPP

#include <mesh/MeshBuilderBase.hpp>
#include <mesh/MeshTraits.hpp>

class MeshVariant;
class DiamondDualMeshBuilder : public MeshBuilderBase
{
 private:
  template <MeshConcept MeshType>
  void _buildDualDiamondMeshFrom(const MeshType&);

  friend class DualMeshManager;
  DiamondDualMeshBuilder(const std::shared_ptr<const MeshVariant>&);

 public:
  ~DiamondDualMeshBuilder() = default;
};

#endif   // DIAMOND_DUAL_MESH_BUILDER_HPP
