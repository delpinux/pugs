#include <mesh/Connectivity.hpp>

#include <mesh/ConnectivityDescriptor.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <utils/GlobalVariableManager.hpp>
#include <utils/Messenger.hpp>

#include <map>

template <size_t Dimension>
Connectivity<Dimension>::Connectivity() : m_id{GlobalVariableManager::instance().getAndIncrementConnectivityId()}
{}

template <size_t Dimension>
void
Connectivity<Dimension>::_buildFrom(const ConnectivityDescriptor& descriptor)
{
  Assert(descriptor.cellToNodeMatrix().numberOfRows() == descriptor.cellTypeVector().size());
  Assert(descriptor.cellNumberVector().size() == descriptor.cellTypeVector().size());
  if constexpr (Dimension > 1) {
    Assert(descriptor.cellToFaceMatrix().numberOfRows() == descriptor.cellTypeVector().size());
    Assert(descriptor.faceToNodeMatrix().numberOfRows() == descriptor.faceNumberVector().size());
    Assert(descriptor.faceOwnerVector().size() == descriptor.faceNumberVector().size());
  }

  m_number_of_cells = descriptor.cellNumberVector().size();
  m_number_of_nodes = descriptor.nodeNumberVector().size();

  if constexpr (Dimension == 1) {
    m_number_of_edges = m_number_of_nodes;
    m_number_of_faces = m_number_of_nodes;
  } else {
    m_number_of_faces = descriptor.faceNumberVector().size();
    if constexpr (Dimension == 2) {
      m_number_of_edges = m_number_of_faces;
    } else {
      static_assert(Dimension == 3, "unexpected dimension");
      m_number_of_edges = descriptor.edgeNumberVector().size();
    }
  }

  auto& cell_to_node_matrix = m_item_to_item_matrix[itemTId(ItemType::cell)][itemTId(ItemType::node)];
  cell_to_node_matrix       = descriptor.cellToNodeMatrix();

  m_cell_type   = WeakCellValue<const CellType>(*this, descriptor.cellTypeVector());
  m_cell_number = WeakCellValue<const int>(*this, descriptor.cellNumberVector());
  m_node_number = WeakNodeValue<const int>(*this, descriptor.nodeNumberVector());

  m_cell_owner = WeakCellValue<const int>(*this, descriptor.cellOwnerVector());

  {
    const int rank = parallel::rank();
    WeakCellValue<bool> cell_is_owned(*this);
    parallel_for(
      this->numberOfCells(), PUGS_CLASS_LAMBDA(CellId j) { cell_is_owned[j] = (m_cell_owner[j] == rank); });
    m_cell_is_owned = cell_is_owned;
  }

  m_node_owner = WeakNodeValue<const int>{*this, descriptor.nodeOwnerVector()};

  Array<bool> node_is_owned_array(this->numberOfNodes());
  {
    const int rank = parallel::rank();
    WeakNodeValue<bool> node_is_owned(*this, node_is_owned_array);
    parallel_for(
      this->numberOfNodes(), PUGS_CLASS_LAMBDA(NodeId r) { node_is_owned[r] = (m_node_owner[r] == rank); });
    m_node_is_owned = node_is_owned;
  }

  m_ref_node_list_vector = descriptor.template refItemListVector<ItemType::node>();
  m_ref_cell_list_vector = descriptor.template refItemListVector<ItemType::cell>();

  if constexpr (Dimension == 1) {
    // faces are similar to nodes
    m_face_number   = WeakFaceValue<const int>(*this, descriptor.nodeNumberVector());
    m_face_owner    = WeakFaceValue<const int>(*this, descriptor.nodeOwnerVector());
    m_face_is_owned = WeakFaceValue<bool>(*this, node_is_owned_array);

    // edges are similar to nodes
    m_edge_number   = WeakEdgeValue<const int>(*this, descriptor.nodeNumberVector());
    m_edge_owner    = WeakEdgeValue<const int>(*this, descriptor.nodeOwnerVector());
    m_edge_is_owned = WeakEdgeValue<bool>(*this, node_is_owned_array);

    // edge and face references are set equal to node references
    m_ref_edge_list_vector.reserve(descriptor.template refItemListVector<ItemType::node>().size());
    m_ref_face_list_vector.reserve(descriptor.template refItemListVector<ItemType::node>().size());
    for (const auto& ref_node_list : descriptor.template refItemListVector<ItemType::node>()) {
      const RefId ref_id            = ref_node_list.refId();
      Array<const NodeId> node_list = ref_node_list.list();
      Array<EdgeId> edge_list(node_list.size());
      Array<FaceId> face_list(node_list.size());
      for (size_t i = 0; i < node_list.size(); ++i) {
        edge_list[i] = EdgeId::base_type{node_list[i]};
        face_list[i] = FaceId::base_type{node_list[i]};
      }

      m_ref_edge_list_vector.emplace_back(RefItemList<ItemType::edge>(ref_id, edge_list, ref_node_list.type()));
      m_ref_face_list_vector.emplace_back(RefItemList<ItemType::face>(ref_id, face_list, ref_node_list.type()));
    }

  } else {
    m_item_to_item_matrix[itemTId(ItemType::face)][itemTId(ItemType::node)] = descriptor.faceToNodeMatrix();

    m_item_to_item_matrix[itemTId(ItemType::cell)][itemTId(ItemType::face)] = descriptor.cellToFaceMatrix();

    m_cell_face_is_reversed = FaceValuePerCell<const bool>(*this, descriptor.cellFaceIsReversed());

    m_face_number = WeakFaceValue<const int>(*this, descriptor.faceNumberVector());

    m_face_owner = WeakFaceValue<const int>(*this, descriptor.faceOwnerVector());

    Array<bool> face_is_owned_array(this->numberOfFaces());
    {
      const int rank = parallel::rank();
      WeakFaceValue<bool> face_is_owned(*this, face_is_owned_array);
      parallel_for(
        this->numberOfFaces(), PUGS_CLASS_LAMBDA(FaceId l) { face_is_owned[l] = (m_face_owner[l] == rank); });
      m_face_is_owned = face_is_owned;
    }

    m_ref_face_list_vector = descriptor.template refItemListVector<ItemType::face>();

    if constexpr (Dimension == 2) {
      // edges are similar to faces
      m_edge_number   = WeakEdgeValue<const int>(*this, descriptor.faceNumberVector());
      m_edge_owner    = WeakEdgeValue<const int>(*this, descriptor.faceOwnerVector());
      m_edge_is_owned = WeakEdgeValue<bool>(*this, face_is_owned_array);

      // edge references are set equal to face references
      m_ref_edge_list_vector.reserve(descriptor.template refItemListVector<ItemType::face>().size());
      for (const auto& ref_face_list : descriptor.template refItemListVector<ItemType::face>()) {
        const RefId ref_id            = ref_face_list.refId();
        Array<const FaceId> face_list = ref_face_list.list();
        Array<EdgeId> edge_list(face_list.size());
        for (size_t i = 0; i < face_list.size(); ++i) {
          edge_list[i] = EdgeId::base_type{face_list[i]};
        }

        m_ref_edge_list_vector.emplace_back(RefItemList<ItemType::edge>(ref_id, edge_list, ref_face_list.type()));
      }

    } else {
      m_item_to_item_matrix[itemTId(ItemType::edge)][itemTId(ItemType::node)] = descriptor.edgeToNodeMatrix();

      m_item_to_item_matrix[itemTId(ItemType::face)][itemTId(ItemType::edge)] = descriptor.faceToEdgeMatrix();

      m_item_to_item_matrix[itemTId(ItemType::cell)][itemTId(ItemType::edge)] = descriptor.cellToEdgeMatrix();

      m_face_edge_is_reversed = EdgeValuePerFace<const bool>(*this, descriptor.faceEdgeIsReversed());

      m_edge_number = WeakEdgeValue<const int>(*this, descriptor.edgeNumberVector());
      m_edge_owner  = WeakEdgeValue<const int>(*this, descriptor.edgeOwnerVector());

      {
        const int rank = parallel::rank();
        WeakEdgeValue<bool> edge_is_owned(*this);
        parallel_for(
          this->numberOfEdges(), PUGS_CLASS_LAMBDA(EdgeId e) { edge_is_owned[e] = (m_edge_owner[e] == rank); });
        m_edge_is_owned = edge_is_owned;
      }

      m_ref_edge_list_vector = descriptor.template refItemListVector<ItemType::edge>();
    }
  }
}

template <size_t Dimension>
void
Connectivity<Dimension>::_buildIsBoundaryFace() const
{
  Array<bool> is_face_boundary_array(this->numberOfFaces());
  WeakFaceValue<bool> is_boundary_face(*this, is_face_boundary_array);
  const auto& face_to_cell_matrix = this->faceToCellMatrix();
  const auto& face_is_owned       = this->faceIsOwned();
  parallel_for(
    this->numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
      is_boundary_face[face_id] = face_is_owned[face_id] and (face_to_cell_matrix[face_id].size() == 1);
    });
  synchronize(is_boundary_face);
  const_cast<WeakFaceValue<const bool>&>(m_is_boundary_face) = is_boundary_face;

  if constexpr (Dimension <= 2) {
    const_cast<WeakEdgeValue<const bool>&>(m_is_boundary_edge) = WeakEdgeValue<bool>(*this, is_face_boundary_array);
    if constexpr (Dimension == 1) {
      const_cast<WeakNodeValue<const bool>&>(m_is_boundary_node) = WeakNodeValue<bool>(*this, is_face_boundary_array);
    } else {
      static_assert(Dimension == 2, "unexpected dimension");
    }
  }
}

template <size_t Dimension>
void
Connectivity<Dimension>::_buildIsBoundaryEdge() const
{
  if constexpr (Dimension < 3) {
    this->_buildIsBoundaryFace();
  } else {
    auto is_boundary_face = this->isBoundaryFace();
    WeakEdgeValue<bool> is_boundary_edge(*this);
    is_boundary_edge.fill(false);
    const auto& face_to_edge_matrix = this->faceToEdgeMatrix();
    for (FaceId face_id = 0; face_id < this->numberOfFaces(); ++face_id) {
      if (is_boundary_face[face_id]) {
        auto face_edge = face_to_edge_matrix[face_id];
        for (size_t i_edge = 0; i_edge < face_edge.size(); ++i_edge) {
          is_boundary_edge[face_edge[i_edge]] = true;
        }
      }
    }
    synchronize(is_boundary_edge);
    const_cast<WeakEdgeValue<const bool>&>(m_is_boundary_edge) = is_boundary_edge;
  }
}

template <size_t Dimension>
void
Connectivity<Dimension>::_buildIsBoundaryNode() const
{
  if constexpr (Dimension == 1) {
    this->_buildIsBoundaryFace();
  } else {
    auto is_boundary_face = this->isBoundaryFace();
    WeakNodeValue<bool> is_boundary_node(*this);
    is_boundary_node.fill(false);
    const auto& face_to_node_matrix = this->faceToNodeMatrix();
    for (FaceId face_id = 0; face_id < this->numberOfFaces(); ++face_id) {
      if (is_boundary_face[face_id]) {
        auto face_nodes = face_to_node_matrix[face_id];
        for (size_t i_node = 0; i_node < face_nodes.size(); ++i_node) {
          is_boundary_node[face_nodes[i_node]] = true;
        }
      }
    }
    synchronize(is_boundary_node);
    const_cast<WeakNodeValue<const bool>&>(m_is_boundary_node) = is_boundary_node;
  }
}

template <ItemType item_type, size_t Dimension>
inline void
_printReference(std::ostream& os, const Connectivity<Dimension>& connectivity, std::set<std::string>& already_printed)
{
  auto count_all_items = [](const auto& item_is_owned) -> size_t {
    using ItemId  = typename std::decay_t<decltype(item_is_owned)>::index_type;
    size_t number = 0;
    for (ItemId item_id = 0; item_id < item_is_owned.numberOfItems(); ++item_id) {
      number += item_is_owned[item_id];
    }
    return parallel::allReduceSum(number);
  };

  auto count_zone_items = [](const auto& item_is_owned, const auto& item_list) -> size_t {
    size_t number = 0;
    for (size_t i_item = 0; i_item < item_list.size(); ++i_item) {
      number += item_is_owned[item_list[i_item]];
    }
    return parallel::allReduceSum(number);
  };

  os << "- number of " << itemName(item_type) << "s: " << rang::fgB::yellow
     << count_all_items(connectivity.template isOwned<item_type>()) << rang::style::reset << '\n';

  // This is done to avoid printing deduced references on subitems
  std::vector<size_t> to_print_list;
  for (size_t i_ref_item = 0; i_ref_item < connectivity.template numberOfRefItemList<item_type>(); ++i_ref_item) {
    auto ref_item_list = connectivity.template refItemList<item_type>(i_ref_item);
    if (already_printed.find(ref_item_list.refId().tagName()) == already_printed.end()) {
      to_print_list.push_back(i_ref_item);
      already_printed.insert(ref_item_list.refId().tagName());
    }
  }

  os << "  " << rang::fgB::yellow << to_print_list.size() << rang::style::reset << " references\n";
  if (to_print_list.size() > 0) {
    for (size_t i_ref_item : to_print_list) {
      auto ref_item_list = connectivity.template refItemList<item_type>(i_ref_item);
      os << "  - " << rang::fgB::green << ref_item_list.refId().tagName() << rang::style::reset << " ("
         << rang::fgB::green << ref_item_list.refId().tagNumber() << rang::style::reset << ") number "
         << rang::fgB::yellow << count_zone_items(connectivity.template isOwned<item_type>(), ref_item_list.list())
         << rang::style::reset << '\n';
    }
  }
}

template <size_t Dimension>
std::ostream&
Connectivity<Dimension>::_write(std::ostream& os) const
{
  std::set<std::string> already_printed;

  os << "connectivity of dimension " << Dimension << '\n';
  _printReference<ItemType::cell>(os, *this, already_printed);
  if constexpr (Dimension > 1) {
    _printReference<ItemType::face>(os, *this, already_printed);
  }
  if constexpr (Dimension > 2) {
    _printReference<ItemType::edge>(os, *this, already_printed);
  }
  _printReference<ItemType::node>(os, *this, already_printed);

  return os;
}

template <size_t Dimension>
CRSGraph
Connectivity<Dimension>::ownCellToCellGraph() const
{
  std::vector<std::vector<int>> cell_cells(this->numberOfCells());
  const auto& face_to_cell_matrix = this->faceToCellMatrix();

  for (FaceId l = 0; l < this->numberOfFaces(); ++l) {
    const auto& face_to_cell = face_to_cell_matrix[l];
    if (face_to_cell.size() > 1) {
      const CellId cell_0 = face_to_cell[0];
      const CellId cell_1 = face_to_cell[1];

      if (m_cell_is_owned[cell_0]) {
        cell_cells[cell_0].push_back(cell_1);
      }
      if (m_cell_is_owned[cell_1]) {
        cell_cells[cell_1].push_back(cell_0);
      }
    }
  }

  std::vector<std::vector<int>> owned_cell_cells;
  owned_cell_cells.reserve(this->numberOfCells());

  size_t number_of_owned_cells = 0;
  for (CellId cell_id = 0; cell_id < this->numberOfCells(); ++cell_id) {
    if (m_cell_is_owned[cell_id]) {
      ++number_of_owned_cells;
    }
  }

  for (size_t i_cell = 0; i_cell < this->numberOfCells(); ++i_cell) {
    if (cell_cells[i_cell].size() > 0) {
      owned_cell_cells.emplace_back(std::move(cell_cells[i_cell]));
    }
  }

  Array number_of_owned_cells_by_rank = parallel::allGather(number_of_owned_cells);

  // index is defined locally. First we compute the first index on the
  // processor
  size_t global_index = 0;
  for (size_t i = 0; i < parallel::rank(); ++i) {
    global_index += number_of_owned_cells_by_rank[i];
  }

  CellValue<int> global_cell_index{*this};
  for (CellId cell_id = 0; cell_id < this->numberOfCells(); ++cell_id) {
    if (m_cell_is_owned[cell_id]) {
      global_cell_index[cell_id] = global_index++;
    }
  }
  synchronize(global_cell_index);

  Array<int> entries(owned_cell_cells.size() + 1);
  entries[0] = 0;
  for (size_t i_cell = 0; i_cell < owned_cell_cells.size(); ++i_cell) {
    entries[i_cell + 1] = entries[i_cell] + owned_cell_cells[i_cell].size();
  }
  Array<int> neighbors(entries[owned_cell_cells.size()]);
  {
    size_t i = 0;
    for (size_t i_cell = 0; i_cell < owned_cell_cells.size(); ++i_cell) {
      for (CellId cell_id : owned_cell_cells[i_cell]) {
        neighbors[i] = global_cell_index[cell_id];
        ++i;
      }
    }
  }

  return CRSGraph(entries, neighbors);
}

template std::ostream& Connectivity<1>::_write(std::ostream&) const;
template std::ostream& Connectivity<2>::_write(std::ostream&) const;
template std::ostream& Connectivity<3>::_write(std::ostream&) const;

template void Connectivity<1>::_buildIsBoundaryFace() const;
template void Connectivity<2>::_buildIsBoundaryFace() const;
template void Connectivity<3>::_buildIsBoundaryFace() const;

template void Connectivity<1>::_buildIsBoundaryEdge() const;
template void Connectivity<2>::_buildIsBoundaryEdge() const;
template void Connectivity<3>::_buildIsBoundaryEdge() const;

template void Connectivity<1>::_buildIsBoundaryNode() const;
template void Connectivity<2>::_buildIsBoundaryNode() const;
template void Connectivity<3>::_buildIsBoundaryNode() const;

template Connectivity<1>::Connectivity();
template Connectivity<2>::Connectivity();
template Connectivity<3>::Connectivity();

template void Connectivity<1>::_buildFrom(const ConnectivityDescriptor&);
template void Connectivity<2>::_buildFrom(const ConnectivityDescriptor&);
template void Connectivity<3>::_buildFrom(const ConnectivityDescriptor&);

template CRSGraph Connectivity<1>::ownCellToCellGraph() const;
template CRSGraph Connectivity<2>::ownCellToCellGraph() const;
template CRSGraph Connectivity<3>::ownCellToCellGraph() const;
