#ifndef GMSH_READER_HPP
#define GMSH_READER_HPP

#include <algebra/TinyVector.hpp>
#include <mesh/MeshBuilderBase.hpp>
#include <mesh/RefId.hpp>
#include <utils/Array.hpp>

#include <array>
#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <vector>

class GmshReader : public MeshBuilderBase
{
 public:
  using R3 = TinyVector<3, double>;

  class PhysicalRefId
  {
   private:
    int m_dimension;
    RefId m_ref_id;

   public:
    friend std::ostream&
    operator<<(std::ostream& os, const PhysicalRefId& physical_ref_id)
    {
      os << physical_ref_id.m_ref_id << "[dimension " << physical_ref_id.m_dimension << "]";
      return os;
    }

    bool
    operator<(const PhysicalRefId& physical_ref_id) const
    {
      return m_ref_id.tagNumber() < physical_ref_id.m_ref_id.tagNumber();
    }

    bool
    operator==(const PhysicalRefId& physical_ref_id) const
    {
      return m_ref_id.tagNumber() == physical_ref_id.m_ref_id.tagNumber();
    }

    const int&
    dimension() const
    {
      return m_dimension;
    }

    const RefId&
    refId() const
    {
      return m_ref_id;
    }

    PhysicalRefId& operator=(const PhysicalRefId&) = default;
    PhysicalRefId& operator=(PhysicalRefId&&) = default;
    PhysicalRefId(int dimension, const RefId& ref_id) : m_dimension(dimension), m_ref_id(ref_id)
    {
      ;
    }
    PhysicalRefId()                     = default;
    PhysicalRefId(const PhysicalRefId&) = default;
    PhysicalRefId(PhysicalRefId&&)      = default;
    ~PhysicalRefId()                    = default;
  };

  struct GmshData
  {
    /**
     * Gmsh format provides a numbered, none ordrered and none dense
     * vertices list, this stores the number of read vertices.
     */
    std::vector<int> __verticesNumbers;

    Array<R3> __vertices;

    using Point = unsigned int;
    Array<Point> __points;
    std::vector<int> __points_ref;
    std::vector<int> __points_number;

    using Edge = TinyVector<2, unsigned int>;
    Array<Edge> __edges;
    std::vector<int> __edges_ref;
    std::vector<int> __edges_number;

    using Triangle = TinyVector<3, unsigned int>;
    Array<Triangle> __triangles;
    std::vector<int> __triangles_ref;
    std::vector<int> __triangles_number;

    using Quadrangle = TinyVector<4, unsigned int>;
    Array<Quadrangle> __quadrangles;
    std::vector<int> __quadrangles_ref;
    std::vector<int> __quadrangles_number;

    using Tetrahedron = TinyVector<4, unsigned int>;
    Array<Tetrahedron> __tetrahedra;
    std::vector<int> __tetrahedra_ref;
    std::vector<int> __tetrahedra_number;

    using Pyramid = TinyVector<5, unsigned int>;
    Array<Pyramid> __pyramids;
    std::vector<int> __pyramids_ref;
    std::vector<int> __pyramids_number;

    using Prism = TinyVector<6, unsigned int>;
    Array<Prism> __prisms;
    std::vector<int> __prisms_ref;
    std::vector<int> __prisms_number;

    using Hexahedron = TinyVector<8, unsigned int>;
    Array<Hexahedron> __hexahedra;
    std::vector<int> __hexahedra_ref;
    std::vector<int> __hexahedra_number;

    /**
     * Gmsh format provides a numbered, none ordrered and none dense
     * vertices list, this provides vertices renumbering correspondence
     */
    std::vector<int> __verticesCorrepondance;

    /**
     * elements types
     */
    std::vector<int> __elementNumber;

    /**
     * elements types
     */
    std::vector<short> __elementType;

    /**
     * References
     */
    std::vector<int> __references;

    /**
     * References
     */
    std::vector<std::vector<int> > __elementVertices;

    std::map<unsigned int, PhysicalRefId> m_physical_ref_map;
  };

 private:
  std::ifstream m_fin;
  const std::string m_filename;

  GmshData m_mesh_data;

  /**
   * Stores the number of nodes associated to each primitive
   *
   */
  std::vector<int> __numberOfPrimitiveNodes;

  /**
   * Array of boolean describing gmsh supported primitives by ff3d
   *
   */
  std::array<bool, 15> __supportedPrimitives;

  /**
   * Primitives names according to there number
   *
   */
  std::map<int, std::string> __primitivesNames;

  int
  _getInteger()
  {
    int i;
    m_fin >> i;

    return i;
  }

  double
  _getReal()
  {
    double d;
    m_fin >> d;

    return d;
  }

  /**
   * Adapts gmsh data to ff3d's structures
   *
   */
  void __proceedData();

  /**
   * Reads data in format 2.2
   *
   */
  void __readGmshFormat2_2();

  /**
   * List of allowed keyword in mesh file
   *
   */
  enum KeywordType
  {
    // gmsh 2.2
    MESHFORMAT,
    ENDMESHFORMAT,
    NODES,
    ENDNODES,
    ELEMENTS,
    ENDELEMENTS,
    PHYSICALNAMES,
    ENDPHYSICALNAMES,
    PERIODIC,
    ENDPERIODIC,

    Unknown,
    EndOfFile
  };

  /**
   * List of known keywords
   *
   */
  using KeywordList = std::map<std::string, int>;

  KeywordList __keywordList; /**< The keyword list */

  /**
   * Type for keyword
   *
   */
  using Keyword = std::pair<std::string, int>;

  /**
   * Skips next comments if exists
   *
   */
  void __skipComments();

  /**
   * Reads the next keyword and returns its token
   *
   * @return KeywordToken
   */
  Keyword __nextKeyword();

  /**
   * get list of vertices
   *
   */
  void __getVertices();

  /**
   * Read list of vertices
   *
   */
  void __readVertices();

  /**
   * Read all elements in format 2.0
   *
   */
  void __readElements2_2();

  /**
   * Reads physical names
   *
   */
  void __readPhysicalNames2_2();

  /**
   * Reads periodic
   *
   */
  void __readPeriodic2_2();

 public:
  GmshReader(const std::string& filename);
  ~GmshReader() = default;
};

#endif   // GMSH_READER_HPP
