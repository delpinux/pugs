#include <mesh/Dual1DConnectivityBuilder.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ConnectivityDescriptor.hpp>
#include <mesh/ConnectivityDispatcher.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/PrimalToDual1DConnectivityDataMapper.hpp>
#include <mesh/RefId.hpp>
#include <utils/Array.hpp>
#include <utils/Messenger.hpp>
#include <utils/Stringify.hpp>

#include <vector>

void
Dual1DConnectivityBuilder::_buildConnectivityDescriptor(const Connectivity<1>& primal_connectivity,
                                                        ConnectivityDescriptor& dual_descriptor)
{
  const size_t primal_number_of_nodes = primal_connectivity.numberOfNodes();
  const size_t primal_number_of_cells = primal_connectivity.numberOfCells();

  const size_t dual_number_of_nodes = 2 * primal_number_of_nodes - primal_number_of_cells;

  const size_t dual_number_of_cells = primal_number_of_nodes;
  const size_t number_of_kept_nodes = 2 * (dual_number_of_nodes - dual_number_of_cells);

  const auto& primal_node_to_cell_matrix = primal_connectivity.nodeToCellMatrix();
  size_t next_kept_node_id               = 0;

  Array<int> node_number_vector(dual_number_of_nodes);

  const auto& primal_node_number = primal_connectivity.nodeNumber();

  for (NodeId primal_node_id = 0; primal_node_id < primal_connectivity.numberOfNodes(); ++primal_node_id) {
    const auto& primal_node_cell_list = primal_node_to_cell_matrix[primal_node_id];
    if (primal_node_cell_list.size() == 1) {
      node_number_vector[next_kept_node_id++] = primal_node_number[primal_node_id];
    }
  }

  const size_t primal_number_of_kept_nodes = next_kept_node_id;

  const auto& primal_cell_number = primal_connectivity.cellNumber();

  const size_t cell_number_shift = max(primal_node_number) + 1;

  for (CellId primal_cell_id = 0; primal_cell_id < primal_number_of_cells; ++primal_cell_id) {
    node_number_vector[primal_number_of_kept_nodes + primal_cell_id] =
      primal_cell_number[primal_cell_id] + cell_number_shift;
  }
  dual_descriptor.setNodeNumberVector(node_number_vector);

  Assert(number_of_kept_nodes == next_kept_node_id, "unexpected number of kept nodes");

  dual_descriptor.setCellNumberVector([&] {
    Array<int> cell_number_vector(dual_number_of_cells);
    parallel_for(
      primal_number_of_nodes, PUGS_LAMBDA(const NodeId primal_node_id) {
        cell_number_vector[primal_node_id] = primal_node_number[primal_node_id];
      });
    return cell_number_vector;
  }());

  Array<CellType> cell_type_vector(dual_number_of_cells);
  cell_type_vector.fill(CellType::Line);
  dual_descriptor.setCellTypeVector(cell_type_vector);

  const auto& primal_node_local_number_in_their_cells = primal_connectivity.nodeLocalNumbersInTheirCells();

  Array<unsigned int> cell_to_node_row(dual_number_of_cells + 1);
  parallel_for(
    cell_to_node_row.size(), PUGS_LAMBDA(const CellId cell_id) { cell_to_node_row[cell_id] = 2 * cell_id; });

  Array<unsigned int> cell_to_node_list(cell_to_node_row[cell_to_node_row.size() - 1]);
  {
    size_t i_kept_node_id = 0;
    size_t i_cell_node    = 0;
    for (NodeId i_node = 0; i_node < primal_connectivity.numberOfNodes(); ++i_node) {
      const auto& primal_node_cell_list = primal_node_to_cell_matrix[i_node];
      if (primal_node_cell_list.size() == 1) {
        const auto i_node_in_cell = primal_node_local_number_in_their_cells(i_node, 0);

        cell_to_node_list[i_cell_node + i_node_in_cell]     = i_kept_node_id++;
        cell_to_node_list[i_cell_node + 1 - i_node_in_cell] = number_of_kept_nodes + primal_node_cell_list[0];

        i_cell_node += 2;

      } else {
        const auto i0 = primal_node_local_number_in_their_cells(i_node, 0);

        cell_to_node_list[i_cell_node++] = number_of_kept_nodes + primal_node_cell_list[1 - i0];
        cell_to_node_list[i_cell_node++] = number_of_kept_nodes + primal_node_cell_list[i0];
      }
    }
  }

  dual_descriptor.setCellToNodeMatrix(ConnectivityMatrix(cell_to_node_row, cell_to_node_list));
}

void
Dual1DConnectivityBuilder::_buildConnectivityFrom(const IConnectivity& i_primal_connectivity)
{
  using ConnectivityType = Connectivity<1>;

  const ConnectivityType& primal_connectivity = dynamic_cast<const ConnectivityType&>(i_primal_connectivity);

  ConnectivityDescriptor dual_descriptor;

  this->_buildConnectivityDescriptor(primal_connectivity, dual_descriptor);

  {
    const auto& dual_node_number_vector = dual_descriptor.nodeNumberVector();

    const std::unordered_map<unsigned int, NodeId> node_to_id_map = [&] {
      std::unordered_map<unsigned int, NodeId> mutable_node_to_id_map;
      for (size_t i_node = 0; i_node < dual_node_number_vector.size(); ++i_node) {
        mutable_node_to_id_map[dual_node_number_vector[i_node]] = i_node;
      }
      return mutable_node_to_id_map;
    }();

    for (size_t i_node_list = 0; i_node_list < primal_connectivity.template numberOfRefItemList<ItemType::node>();
         ++i_node_list) {
      const auto& primal_ref_node_list = primal_connectivity.template refItemList<ItemType::node>(i_node_list);
      const auto& primal_node_list     = primal_ref_node_list.list();

      const std::vector<NodeId> dual_node_list = [&]() {
        std::vector<NodeId> mutable_dual_node_list;

        for (size_t i_primal_node = 0; i_primal_node < primal_node_list.size(); ++i_primal_node) {
          auto primal_node_id    = primal_node_list[i_primal_node];
          const auto i_dual_node = node_to_id_map.find(primal_connectivity.nodeNumber()[primal_node_id]);
          if (i_dual_node != node_to_id_map.end()) {
            mutable_dual_node_list.push_back(i_dual_node->second);
          }
        }

        return mutable_dual_node_list;
      }();

      if (parallel::allReduceOr(dual_node_list.size() > 0)) {
        Array<NodeId> node_array(dual_node_list.size());
        for (size_t i = 0; i < dual_node_list.size(); ++i) {
          node_array[i] = dual_node_list[i];
        }
        dual_descriptor.addRefItemList(
          RefNodeList{primal_ref_node_list.refId(), node_array, primal_ref_node_list.type()});
      }
    }
  }

  const size_t primal_number_of_nodes = primal_connectivity.numberOfNodes();
  const size_t primal_number_of_cells = primal_connectivity.numberOfCells();

  dual_descriptor.setNodeOwnerVector([&] {
    Array<int> node_owner_vector(dual_descriptor.nodeNumberVector().size());
    const auto& node_to_cell_matrix = primal_connectivity.nodeToCellMatrix();
    const auto& primal_node_owner   = primal_connectivity.nodeOwner();
    size_t next_kept_node_id        = 0;
    for (NodeId primal_node_id = 0; primal_node_id < primal_connectivity.numberOfNodes(); ++primal_node_id) {
      if (node_to_cell_matrix[primal_node_id].size() == 1) {
        node_owner_vector[next_kept_node_id++] = primal_node_owner[primal_node_id];
      }
    }
    const size_t number_of_kept_nodes = next_kept_node_id;
    const auto& primal_cell_owner     = primal_connectivity.cellOwner();
    for (CellId primal_cell_id = 0; primal_cell_id < primal_number_of_cells; ++primal_cell_id) {
      node_owner_vector[number_of_kept_nodes + primal_cell_id] = primal_cell_owner[primal_cell_id];
    }

    return node_owner_vector;
  }());

  dual_descriptor.setCellOwnerVector([&] {
    Array<int> cell_owner_vector(dual_descriptor.cellNumberVector().size());
    const auto& primal_node_owner = primal_connectivity.nodeOwner();
    parallel_for(
      primal_number_of_nodes,
      PUGS_LAMBDA(NodeId primal_node_id) { cell_owner_vector[primal_node_id] = primal_node_owner[primal_node_id]; });
    return cell_owner_vector;
  }());

  m_connectivity = ConnectivityType::build(dual_descriptor);

  {
    const auto& node_to_cell_matrix = primal_connectivity.nodeToCellMatrix();

    NodeId dual_node_id            = 0;
    m_primal_node_to_dual_node_map = [&]() {
      std::vector<std::pair<NodeId, NodeId>> primal_node_to_dual_node_vector;
      for (NodeId primal_node_id = 0; primal_node_id < primal_connectivity.numberOfNodes(); ++primal_node_id) {
        if (node_to_cell_matrix[primal_node_id].size() == 1) {
          primal_node_to_dual_node_vector.push_back(std::make_pair(primal_node_id, dual_node_id++));
        }
      }
      return convert_to_array(primal_node_to_dual_node_vector);
    }();

    m_primal_cell_to_dual_node_map = [&]() {
      CellIdToNodeIdMap primal_cell_to_dual_node_map{primal_number_of_cells};
      for (CellId primal_cell_id = 0; primal_cell_id < primal_cell_to_dual_node_map.size(); ++primal_cell_id) {
        primal_cell_to_dual_node_map[primal_cell_id] = std::make_pair(primal_cell_id, dual_node_id++);
      }
      return primal_cell_to_dual_node_map;
    }();

    m_primal_node_to_dual_cell_map = [&]() {
      NodeIdToCellIdMap primal_node_to_dual_cell_map{primal_connectivity.numberOfFaces()};
      for (size_t id = 0; id < primal_node_to_dual_cell_map.size(); ++id) {
        const CellId dual_cell_id   = id;
        const NodeId primal_node_id = id;

        primal_node_to_dual_cell_map[id] = std::make_pair(primal_node_id, dual_cell_id);
      }
      return primal_node_to_dual_cell_map;
    }();
  }

  m_mapper =
    std::make_shared<PrimalToDual1DConnectivityDataMapper>(primal_connectivity,
                                                           dynamic_cast<const ConnectivityType&>(*m_connectivity),
                                                           m_primal_node_to_dual_node_map,
                                                           m_primal_cell_to_dual_node_map,
                                                           m_primal_node_to_dual_cell_map);
}

Dual1DConnectivityBuilder::Dual1DConnectivityBuilder(const IConnectivity& connectivity)
{
  // LCOV_EXCL_START
  if (parallel::size() > 1) {
    throw NotImplementedError("Construction of diamond dual mesh is not implemented in parallel");
  }
  // LCOV_EXCL_STOP

  if (connectivity.dimension() == 1) {
    this->_buildConnectivityFrom(connectivity);
  } else {
    // LCOV_EXCL_START
    throw UnexpectedError("invalid connectivity dimension: " + stringify(connectivity.dimension()));
    // LCOV_EXCL_STOP
  }
}
