#ifndef MESH_TRANSFORMER_HPP
#define MESH_TRANSFORMER_HPP

class MeshVariant;
class FunctionSymbolId;

#include <memory>

class MeshTransformer
{
  template <typename T>
  class MeshTransformation;

 public:
  std::shared_ptr<const MeshVariant> transform(const FunctionSymbolId& function_symbol_id,
                                               std::shared_ptr<const MeshVariant> p_mesh);

  MeshTransformer()  = default;
  ~MeshTransformer() = default;
};

#endif   // MESH_TRANSFORMER_HPP
