#ifndef CONNECTIVITY_BUILDER_BASE_HPP
#define CONNECTIVITY_BUILDER_BASE_HPP

#include <mesh/IConnectivity.hpp>

#include <mesh/ItemId.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>

#include <memory>
#include <vector>

class ConnectivityDescriptor;

class ConnectivityBuilderBase
{
 protected:
  std::shared_ptr<const IConnectivity> m_connectivity;

  template <size_t Dimension>
  static void _computeCellFaceAndFaceNodeConnectivities(ConnectivityDescriptor& descriptor);

  template <size_t Dimension>
  static void _computeFaceEdgeAndEdgeNodeAndCellEdgeConnectivities(ConnectivityDescriptor& descriptor);

 public:
  std::shared_ptr<const IConnectivity>
  connectivity() const
  {
    return m_connectivity;
  }

  ConnectivityBuilderBase()  = default;
  ~ConnectivityBuilderBase() = default;
};

#endif   // CONNECTIVITY_BUILDER_BASE_HPP
