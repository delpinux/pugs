#include <mesh/MeshRelaxer.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>

template <MeshConcept MeshType>
std::shared_ptr<const MeshVariant>
MeshRelaxer::_relax(const MeshType& source_mesh, const MeshType& destination_mesh, const double& theta) const
{
  static_assert(is_polygonal_mesh_v<MeshType>);
  if (source_mesh.connectivity().id() == destination_mesh.connectivity().id()) {
    using ConnectivityType               = typename MeshType::Connectivity;
    constexpr size_t Dimension           = MeshType::Dimension;
    const ConnectivityType& connectivity = source_mesh.connectivity();

    NodeValue<TinyVector<Dimension>> theta_xr{connectivity};
    const NodeValue<const TinyVector<Dimension>> source_xr      = source_mesh.xr();
    const NodeValue<const TinyVector<Dimension>> destination_xr = destination_mesh.xr();

    const double one_minus_theta = 1 - theta;
    parallel_for(
      connectivity.numberOfNodes(), PUGS_LAMBDA(const NodeId node_id) {
        theta_xr[node_id] = one_minus_theta * source_xr[node_id] + theta * destination_xr[node_id];
      });

    return std::make_shared<MeshVariant>(std::make_shared<const MeshType>(source_mesh.shared_connectivity(), theta_xr));
  } else {
    throw NormalError("relaxed meshes must share the same connectivity");
  }
}

std::shared_ptr<const MeshVariant>
MeshRelaxer::relax(const std::shared_ptr<const MeshVariant>& p_source_mesh,
                   const std::shared_ptr<const MeshVariant>& p_destination_mesh,
                   const double& theta) const
{
  return std::visit(
    [&](auto&& source_mesh, auto&& destination_mesh) -> std::shared_ptr<const MeshVariant> {
      using SourceMeshType      = mesh_type_t<decltype(source_mesh)>;
      using DestinationMeshType = mesh_type_t<decltype(destination_mesh)>;
      if constexpr (std::is_same_v<SourceMeshType, DestinationMeshType>) {
        return this->_relax(*source_mesh, *destination_mesh, theta);
      } else {
        throw UnexpectedError("invalid mesh dimension");
      }
    },
    p_source_mesh->variant(), p_destination_mesh->variant());
}
