#include <mesh/ConnectivityUtils.hpp>

#include <mesh/Connectivity.hpp>
#include <utils/Messenger.hpp>

template <size_t Dimension, ItemType SourceItemType, ItemType TargetItemType>
bool
checkItemToHigherDimensionItem(const Connectivity<Dimension>& connectivity)
{
  static_assert(SourceItemType < TargetItemType);
  bool is_valid = true;

  const auto& item_to_item_matrix = connectivity.template getItemToItemMatrix<SourceItemType, TargetItemType>();
  const auto& item_number         = connectivity.template number<TargetItemType>();

  for (ItemIdT<SourceItemType> source_item_id = 0; source_item_id < connectivity.template numberOf<SourceItemType>();
       ++source_item_id) {
    auto target_item_list = item_to_item_matrix[source_item_id];
    for (size_t i = 0; i < target_item_list.size() - 1; ++i) {
      is_valid &= (item_number[target_item_list[i + 1]] > item_number[target_item_list[i]]);
    }
  }

  return is_valid;
}

template <size_t Dimension>
bool checkConnectivityOrdering(const Connectivity<Dimension>&);

template <>
bool
checkConnectivityOrdering(const Connectivity<1>& connectivity)
{
  bool is_valid = true;
  is_valid &= checkItemToHigherDimensionItem<1, ItemType::node, ItemType::cell>(connectivity);

  return is_valid;
}
template <>

bool
checkConnectivityOrdering(const Connectivity<2>& connectivity)
{
  bool is_valid = true;
  is_valid &= checkItemToHigherDimensionItem<2, ItemType::node, ItemType::face>(connectivity);
  is_valid &= checkItemToHigherDimensionItem<2, ItemType::node, ItemType::cell>(connectivity);

  is_valid &= checkItemToHigherDimensionItem<2, ItemType::face, ItemType::cell>(connectivity);

  return is_valid;
}

bool
checkConnectivityOrdering(const Connectivity<3>& connectivity)
{
  bool is_valid = true;
  is_valid &= checkItemToHigherDimensionItem<3, ItemType::node, ItemType::edge>(connectivity);
  is_valid &= checkItemToHigherDimensionItem<3, ItemType::node, ItemType::face>(connectivity);
  is_valid &= checkItemToHigherDimensionItem<3, ItemType::node, ItemType::cell>(connectivity);

  is_valid &= checkItemToHigherDimensionItem<3, ItemType::edge, ItemType::face>(connectivity);
  is_valid &= checkItemToHigherDimensionItem<3, ItemType::edge, ItemType::cell>(connectivity);

  is_valid &= checkItemToHigherDimensionItem<3, ItemType::face, ItemType::cell>(connectivity);

  return parallel::allReduceAnd(is_valid);
}

bool
checkConnectivityOrdering(const IConnectivity& connecticity)
{
  switch (connecticity.dimension()) {
  case 1: {
    return checkConnectivityOrdering(dynamic_cast<const Connectivity<1>&>(connecticity));
  }
  case 2: {
    return checkConnectivityOrdering(dynamic_cast<const Connectivity<2>&>(connecticity));
  }
  case 3: {
    return checkConnectivityOrdering(dynamic_cast<const Connectivity<3>&>(connecticity));
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("invalid dimension");
  }
    // LCOV_EXCL_STOP
  }
}
