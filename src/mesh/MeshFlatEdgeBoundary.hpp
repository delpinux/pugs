#ifndef MESH_FLAT_EDGE_BOUNDARY_HPP
#define MESH_FLAT_EDGE_BOUNDARY_HPP

#include <mesh/MeshEdgeBoundary.hpp>

template <MeshConcept MeshType>
class MeshFlatEdgeBoundary final : public MeshEdgeBoundary   // clazy:exclude=copyable-polymorphic
{
 public:
  static_assert(not std::is_const_v<MeshType>, "MeshType must be non-const");

  using Rd = TinyVector<MeshType::Dimension, double>;

 private:
  const Rd m_outgoing_normal;

 public:
  const Rd&
  outgoingNormal() const
  {
    return m_outgoing_normal;
  }

  MeshFlatEdgeBoundary& operator=(const MeshFlatEdgeBoundary&) = default;
  MeshFlatEdgeBoundary& operator=(MeshFlatEdgeBoundary&&)      = default;

  template <typename MeshTypeT>
  friend MeshFlatEdgeBoundary<MeshTypeT> getMeshFlatEdgeBoundary(const MeshTypeT& mesh,
                                                                 const IBoundaryDescriptor& boundary_descriptor);

 private:
  MeshFlatEdgeBoundary(const MeshType& mesh, const RefEdgeList& ref_edge_list, const Rd& outgoing_normal)
    : MeshEdgeBoundary(mesh, ref_edge_list), m_outgoing_normal(outgoing_normal)
  {}

 public:
  MeshFlatEdgeBoundary()                            = default;
  MeshFlatEdgeBoundary(const MeshFlatEdgeBoundary&) = default;
  MeshFlatEdgeBoundary(MeshFlatEdgeBoundary&&)      = default;
  ~MeshFlatEdgeBoundary()                           = default;
};

template <typename MeshType>
MeshFlatEdgeBoundary<MeshType> getMeshFlatEdgeBoundary(const MeshType& mesh,
                                                       const IBoundaryDescriptor& boundary_descriptor);

#endif   // MESH_FLAT_EDGE_BOUNDARY_HPP
