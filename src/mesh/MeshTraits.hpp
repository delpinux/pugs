#ifndef MESH_TRAITS_HPP
#define MESH_TRAITS_HPP

#include <utils/PugsTraits.hpp>

#include <memory>
#include <type_traits>

template <size_t Dimension>
class Mesh;

template <typename T>
constexpr inline bool is_mesh_v = false;

template <size_t Dimension>
constexpr inline bool is_mesh_v<Mesh<Dimension>> = true;

template <typename T>
constexpr bool is_mesh_v<const T> = is_mesh_v<std::remove_cvref_t<T>>;

template <typename T>
constexpr bool is_mesh_v<T&> = is_mesh_v<std::remove_cvref_t<T>>;

template <typename T>
concept MeshConcept = is_mesh_v<T>;

// Get mesh type (from std::shared_ptr for instance)

template <typename T>
struct mesh_type_t_from
{
  static_assert(is_mesh_v<T>, "Requires mesh type");
  using type = std::remove_const_t<T>;
};

template <MeshConcept T, template <typename T1> typename C>
struct mesh_type_t_from<C<T>>
{
  static_assert(is_shared_ptr_v<std::decay_t<C<T>>>, "Requires std::shared_ptr container");
  using type = std::remove_const_t<T>;
};

template <typename T>
using mesh_type_t = typename mesh_type_t_from<std::decay_t<T>>::type;

// Check mesh kind
template <MeshConcept MeshType>
constexpr inline bool is_polygonal_mesh_v = false;

template <size_t Dimension>
constexpr inline bool is_polygonal_mesh_v<Mesh<Dimension>> = true;

template <typename T>
constexpr bool is_polygonal_mesh_v<const T> = is_polygonal_mesh_v<std::remove_cvref_t<T>>;

template <typename T>
constexpr bool is_polygonal_mesh_v<T&> = is_polygonal_mesh_v<std::remove_cvref_t<T>>;

#endif   // MESH_TRAITS_HPP
