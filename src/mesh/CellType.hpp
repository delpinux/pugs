#ifndef CELL_TYPE_HPP
#define CELL_TYPE_HPP

#include <utils/PugsMacros.hpp>

#include <string_view>

enum class CellType : unsigned short
{
  Line = 0,

  Triangle,
  Quadrangle,
  Polygon,

  Diamond,
  Hexahedron,
  Prism,
  Pyramid,
  Tetrahedron
};

PUGS_INLINE
std::string_view
name(CellType cell_type)
{
  switch (cell_type) {
  case CellType::Line:
    return "line";
  case CellType::Triangle:
    return "triangle";
  case CellType::Polygon:
    return "polygon";
  case CellType::Quadrangle:
    return "quadrangle";
  case CellType::Diamond:
    return "diamond";
  case CellType::Hexahedron:
    return "hexahedron";
  case CellType::Prism:
    return "prism";
  case CellType::Pyramid:
    return "pyramid";
  case CellType::Tetrahedron:
    return "tetrahedron";
  default:
    return "unknown cell type";
  }
}

#endif   // CELL_TYPE_HPP
