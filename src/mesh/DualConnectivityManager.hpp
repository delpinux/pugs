#ifndef DUAL_CONNECTIVITY_MANAGER_HPP
#define DUAL_CONNECTIVITY_MANAGER_HPP

#include <mesh/DualMeshType.hpp>
#include <mesh/IConnectivity.hpp>
#include <mesh/IPrimalToDualConnectivityDataMapper.hpp>

#include <memory>
#include <unordered_map>

template <size_t Dimension>
class Connectivity;

template <size_t Dimension>
class PrimalToDiamondDualConnectivityDataMapper;

template <size_t Dimension>
class PrimalToMedianDualConnectivityDataMapper;

class PrimalToDual1DConnectivityDataMapper;

class DualConnectivityManager
{
 private:
  class DualConnectivityInfo
  {
   private:
    std::shared_ptr<const IConnectivity> m_dual_connectivity;
    std::shared_ptr<const IPrimalToDualConnectivityDataMapper> m_primal_to_dual_connectivity_data_mapper;

   public:
    PUGS_INLINE
    std::shared_ptr<const IConnectivity>
    dualConnectivity() const
    {
      return m_dual_connectivity;
    }

    PUGS_INLINE
    std::shared_ptr<const IPrimalToDualConnectivityDataMapper>
    connectivityToDualConnectivityDataMapper()
    {
      return m_primal_to_dual_connectivity_data_mapper;
    }

    DualConnectivityInfo& operator=(const DualConnectivityInfo&) = default;
    DualConnectivityInfo& operator=(DualConnectivityInfo&&)      = delete;

    DualConnectivityInfo()                            = default;
    DualConnectivityInfo(const DualConnectivityInfo&) = default;
    DualConnectivityInfo(DualConnectivityInfo&&)      = default;

    DualConnectivityInfo(
      const std::shared_ptr<const IConnectivity>& dual_connectivity,
      const std::shared_ptr<const IPrimalToDualConnectivityDataMapper>& primal_to_dual_connectivity_data_mapper)
      : m_dual_connectivity{dual_connectivity},
        m_primal_to_dual_connectivity_data_mapper{primal_to_dual_connectivity_data_mapper}
    {}

    ~DualConnectivityInfo() = default;
  };

  using Key = std::pair<DualMeshType, const IConnectivity*>;
  struct HashKey
  {
    size_t
    operator()(const Key& key) const
    {
      return (std::hash<typename Key::first_type>()(key.first)) ^ (std::hash<typename Key::second_type>()(key.second));
    }
  };

  template <typename DualConnectivityBuilderType>
  DualConnectivityInfo _buildDualConnectivity(const Key& key, const IConnectivity& connectivity);

  DualConnectivityInfo _getDualConnectivityInfo(const DualMeshType& type, const IConnectivity& connectivity);

  std::unordered_map<Key, DualConnectivityInfo, HashKey> m_primal_to_dual_connectivity_info_map;

  static DualConnectivityManager* m_instance;

  DualConnectivityManager(const DualConnectivityManager&) = delete;
  DualConnectivityManager(DualConnectivityManager&&)      = delete;

  DualConnectivityManager()  = default;
  ~DualConnectivityManager() = default;

 public:
  const auto&
  primalToDualConnectivityInfoMap() const
  {
    return m_primal_to_dual_connectivity_info_map;
  }

  static void create();
  static void destroy();

  PUGS_INLINE
  static DualConnectivityManager&
  instance()
  {
    Assert(m_instance != nullptr, "DualConnectivityManager was not created!");
    return *m_instance;
  }

  void deleteConnectivity(const IConnectivity*);

  std::shared_ptr<const Connectivity<1>> getDual1DConnectivity(const Connectivity<1>&);

  std::shared_ptr<const PrimalToDual1DConnectivityDataMapper> getPrimalToDual1DConnectivityDataMapper(
    const Connectivity<1>&);

  template <size_t Dimension>
  std::shared_ptr<const Connectivity<Dimension>> getMedianDualConnectivity(const Connectivity<Dimension>&);

  std::shared_ptr<const PrimalToDual1DConnectivityDataMapper>   // special 1D case
  getPrimalToMedianDualConnectivityDataMapper(const Connectivity<1>&);
  template <size_t Dimension>
  std::shared_ptr<const PrimalToMedianDualConnectivityDataMapper<Dimension>>
  getPrimalToMedianDualConnectivityDataMapper(const Connectivity<Dimension>&);

  template <size_t Dimension>
  std::shared_ptr<const Connectivity<Dimension>> getDiamondDualConnectivity(const Connectivity<Dimension>&);

  std::shared_ptr<const PrimalToDual1DConnectivityDataMapper>   // special 1D case
  getPrimalToDiamondDualConnectivityDataMapper(const Connectivity<1>&);
  template <size_t Dimension>
  std::shared_ptr<const PrimalToDiamondDualConnectivityDataMapper<Dimension>>
  getPrimalToDiamondDualConnectivityDataMapper(const Connectivity<Dimension>&);
};

#endif   // DUAL_CONNECTIVITY_MANAGER_HPP
