#ifndef LOGICAL_CONNECTIVITY_BUILDER_HPP
#define LOGICAL_CONNECTIVITY_BUILDER_HPP

#include <algebra/TinyVector.hpp>

#include <mesh/ConnectivityBuilderBase.hpp>

class LogicalConnectivityBuilder : public ConnectivityBuilderBase
{
 private:
  template <size_t Dimension>
  void _buildBoundaryNodeList(const TinyVector<Dimension, uint64_t>& cell_size, ConnectivityDescriptor& descriptor);

  template <size_t Dimension>
  void _buildBoundaryEdgeList(const TinyVector<Dimension, uint64_t>& cell_size, ConnectivityDescriptor& descriptor);

  template <size_t Dimension>
  void _buildBoundaryFaceList(const TinyVector<Dimension, uint64_t>& cell_size, ConnectivityDescriptor& descriptor);

  template <size_t Dimension>
  void _buildConnectivity(const TinyVector<Dimension, uint64_t>& size);

 public:
  template <size_t Dimension>
  LogicalConnectivityBuilder(const TinyVector<Dimension, uint64_t>& size);
  ~LogicalConnectivityBuilder() = default;
};

#endif   // LOGICAL_CONNECTIVITY_BUILDER_HPP
