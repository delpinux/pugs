#ifndef SUBITEM_ARRAY_PER_ITEM_HPP
#define SUBITEM_ARRAY_PER_ITEM_HPP

#include <mesh/ConnectivityMatrix.hpp>
#include <mesh/IConnectivity.hpp>
#include <mesh/ItemId.hpp>
#include <mesh/ItemOfItemType.hpp>
#include <mesh/ItemType.hpp>
#include <utils/Array.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/Table.hpp>

#include <memory>

template <typename DataType, typename ItemOfItem, typename ConnectivityPtr = std::shared_ptr<const IConnectivity>>
class SubItemArrayPerItem
{
 public:
  static constexpr ItemType item_type{ItemOfItem::item_type};
  static constexpr ItemType sub_item_type{ItemOfItem::sub_item_type};

  using ItemOfItemType = ItemOfItem;
  using data_type      = DataType;
  using ItemId         = ItemIdT<item_type>;
  using index_type     = ItemId;

 private:
  using ConnectivitySharedPtr = std::shared_ptr<const IConnectivity>;
  using ConnectivityWeakPtr   = std::weak_ptr<const IConnectivity>;

  static_assert(std::is_same_v<ConnectivityPtr, ConnectivitySharedPtr> or
                std::is_same_v<ConnectivityPtr, ConnectivityWeakPtr>);

  ConnectivityPtr m_connectivity_ptr;

  Array<const uint32_t> m_row_map;
  Table<DataType> m_values;

  // Allow const std:shared_ptr version to access our data
  friend SubItemArrayPerItem<std::add_const_t<DataType>, ItemOfItem, ConnectivitySharedPtr>;

  // Allow const std:weak_ptr version to access our data
  friend SubItemArrayPerItem<std::add_const_t<DataType>, ItemOfItem, ConnectivityWeakPtr>;

  // Allow non-const std:shared_ptr version to access our data
  friend SubItemArrayPerItem<std::remove_const_t<DataType>, ItemOfItem, ConnectivitySharedPtr>;

  // Allow non-const std:weak_ptr version to access our data
  friend SubItemArrayPerItem<std::remove_const_t<DataType>, ItemOfItem, ConnectivityWeakPtr>;

 public:
  // This is not the correct way to look at SubItemArrayPerItem, use with care
  Table<const DataType>
  tableView() const
  {
    return m_values;
  }

  [[nodiscard]] friend PUGS_INLINE SubItemArrayPerItem<std::remove_const_t<DataType>, ItemOfItem, ConnectivityPtr>
  copy(const SubItemArrayPerItem<DataType, ItemOfItem, ConnectivityPtr>& source)
  {
    SubItemArrayPerItem<std::remove_const_t<DataType>, ItemOfItem, ConnectivityPtr> image;

    image.m_connectivity_ptr = source.m_connectivity_ptr;
    image.m_row_map          = source.m_row_map;
    image.m_values           = copy(source.m_values);

    return image;
  }

  [[nodiscard]] PUGS_INLINE bool
  isBuilt() const noexcept
  {
    return m_connectivity_ptr.use_count() != 0;
  }

  [[nodiscard]] PUGS_INLINE std::shared_ptr<const IConnectivity>
  connectivity_ptr() const noexcept
  {
    if constexpr (std::is_same_v<ConnectivityPtr, ConnectivitySharedPtr>) {
      return m_connectivity_ptr;
    } else {
      return m_connectivity_ptr.lock();
    }
  }

  template <typename IndexType, typename SubIndexType>
  [[nodiscard]] PUGS_INLINE typename Table<DataType>::UnsafeRowView
  operator()(IndexType item_id, SubIndexType i) const noexcept(NO_ASSERT)
  {
    static_assert(std::is_same_v<IndexType, ItemId>, "first index must be of the correct ItemId type");
    static_assert(std::is_integral_v<SubIndexType>, "second index must be an integral type");
    Assert(this->isBuilt(), "SubItemArrayPerItem is not built");
    Assert(item_id < this->numberOfItems(), "invalid item_id");

    Assert(i + m_row_map[size_t{item_id}] < m_row_map[size_t{item_id} + 1], "invalid index");
    return m_values[m_row_map[size_t{item_id}] + i];
  }

  template <ItemType item_t>
  [[nodiscard]] PUGS_INLINE typename Table<DataType>::UnsafeTableView
  operator[](const ItemIdT<item_t>& item_id) const noexcept(NO_ASSERT)
  {
    static_assert(item_t == item_type, "invalid ItemId type");
    return this->itemTable(item_id);
  }

  template <typename ArrayIndexType>
  [[nodiscard]] PUGS_INLINE typename Table<DataType>::UnsafeRowView
  operator[](const ArrayIndexType& i) const noexcept(NO_ASSERT)
  {
    static_assert(std::is_integral_v<ArrayIndexType>, "index must be an integral type");
    Assert(this->isBuilt(), "SubItemArrayPerItem is not built");
    Assert(static_cast<size_t>(i) < m_values.numberOfRows(), "invalid index");
    return m_values[i];
  }

  [[nodiscard]] PUGS_INLINE size_t
  numberOfArrays() const noexcept(NO_ASSERT)
  {
    Assert(this->isBuilt(), "SubItemArrayPerItem is not built");
    return m_values.numberOfRows();
  }

  [[nodiscard]] PUGS_INLINE size_t
  numberOfItems() const noexcept(NO_ASSERT)
  {
    Assert(this->isBuilt(), "SubItemArrayPerItem is not built");
    Assert(m_row_map.size() > 0, "invalid row map");
    return m_row_map.size() - 1;
  }

  [[nodiscard]] PUGS_INLINE size_t
  sizeOfArrays() const noexcept(NO_ASSERT)
  {
    Assert(this->isBuilt(), "SubItemArrayPerItem is not built");
    return m_values.numberOfColumns();
  }

  template <typename IndexType>
  [[nodiscard]] PUGS_INLINE size_t
  numberOfSubArrays(IndexType item_id) const noexcept(NO_ASSERT)
  {
    static_assert(std::is_same_v<IndexType, ItemId>, "index must be an ItemId");
    Assert(this->isBuilt(), "SubItemArrayPerItem is not built");
    Assert(item_id < this->numberOfItems(), "invalid item_id");
    return m_row_map[size_t{item_id} + 1] - m_row_map[size_t{item_id}];
  }

  PUGS_INLINE void
  fill(const DataType& data) const noexcept
  {
    static_assert(not std::is_const_v<DataType>, "Cannot modify SubItemArrayPerItem of const");
    m_values.fill(data);
  }

  template <typename IndexType>
  [[nodiscard]] PUGS_INLINE typename Table<DataType>::UnsafeTableView
  itemTable(IndexType item_id) const noexcept(NO_ASSERT)
  {
    static_assert(std::is_same_v<IndexType, ItemId>, "index must be an ItemId");
    Assert(this->isBuilt(), "SubItemArrayPerItem is not built");
    Assert(item_id < this->numberOfItems(), "invalid item_id");
    const auto& item_begin = m_row_map[size_t{item_id}];
    const auto& item_end   = m_row_map[size_t{item_id} + 1];
    return subTableView(m_values, item_begin, item_end - item_begin, 0, this->sizeOfArrays());
  }

  template <typename DataType2, typename ConnectivityPtr2>
  PUGS_INLINE SubItemArrayPerItem&
  operator=(const SubItemArrayPerItem<DataType2, ItemOfItem, ConnectivityPtr2>& sub_item_array_per_item) noexcept
  {
    // ensures that DataType is the same as source DataType2
    static_assert(std::is_same_v<std::remove_const_t<DataType>, std::remove_const_t<DataType2>>,
                  "Cannot assign SubItemArrayPerItem of different type");
    // ensures that const is not lost through copy
    static_assert(((std::is_const_v<DataType2> and std::is_const_v<DataType>) or not std::is_const_v<DataType2>),
                  "Cannot assign SubItemArrayPerItem of const data to "
                  "SubItemArrayPerItem of non-const data");
    m_row_map = sub_item_array_per_item.m_row_map;
    m_values  = sub_item_array_per_item.m_values;

    if constexpr (std::is_same_v<ConnectivityPtr, ConnectivitySharedPtr> and
                  std::is_same_v<ConnectivityPtr2, ConnectivityWeakPtr>) {
      m_connectivity_ptr = sub_item_array_per_item.m_connectivity_ptr.lock();
    } else {
      m_connectivity_ptr = sub_item_array_per_item.m_connectivity_ptr;
    }

    return *this;
  }

  template <typename DataType2, typename ConnectivityPtr2>
  PUGS_INLINE
  SubItemArrayPerItem(
    const SubItemArrayPerItem<DataType2, ItemOfItem, ConnectivityPtr2>& sub_item_array_per_item) noexcept
  {
    this->operator=(sub_item_array_per_item);
  }

  SubItemArrayPerItem() = default;

  SubItemArrayPerItem(const IConnectivity& connectivity, size_t size_of_arrays) noexcept
    : m_connectivity_ptr{connectivity.shared_ptr()}
  {
    static_assert(not std::is_const_v<DataType>, "Cannot allocate SubItemArrayPerItem of const data: only "
                                                 "view is supported");

    ConnectivityMatrix connectivity_matrix = connectivity.getMatrix(item_type, sub_item_type);

    m_row_map = connectivity_matrix.rowsMap();
    m_values  = Table<std::remove_const_t<DataType>>(connectivity_matrix.numberOfValues(), size_of_arrays);
  }

  SubItemArrayPerItem(const IConnectivity& connectivity, const Table<DataType>& table) noexcept(NO_ASSERT)
    : m_connectivity_ptr{connectivity.shared_ptr()}, m_values{table}
  {
    Assert(m_values.numberOfRows() == connectivity.getMatrix(item_type, sub_item_type).numberOfValues(),
           "invalid size of provided table");
    m_row_map = connectivity.getMatrix(item_type, sub_item_type).rowsMap();
  }
  ~SubItemArrayPerItem() = default;
};

// Item arrays at nodes

template <typename DataType>
using NodeArrayPerEdge = SubItemArrayPerItem<DataType, NodeOfEdge>;

template <typename DataType>
using NodeArrayPerFace = SubItemArrayPerItem<DataType, NodeOfFace>;

template <typename DataType>
using NodeArrayPerCell = SubItemArrayPerItem<DataType, NodeOfCell>;

// Item arrays at edges

template <typename DataType>
using EdgeArrayPerNode = SubItemArrayPerItem<DataType, EdgeOfNode>;

template <typename DataType>
using EdgeArrayPerFace = SubItemArrayPerItem<DataType, EdgeOfFace>;

template <typename DataType>
using EdgeArrayPerCell = SubItemArrayPerItem<DataType, EdgeOfCell>;

// Item arrays at faces

template <typename DataType>
using FaceArrayPerNode = SubItemArrayPerItem<DataType, FaceOfNode>;

template <typename DataType>
using FaceArrayPerEdge = SubItemArrayPerItem<DataType, FaceOfEdge>;

template <typename DataType>
using FaceArrayPerCell = SubItemArrayPerItem<DataType, FaceOfCell>;

// Item arrays at cells

template <typename DataType>
using CellArrayPerNode = SubItemArrayPerItem<DataType, CellOfNode>;

template <typename DataType>
using CellArrayPerEdge = SubItemArrayPerItem<DataType, CellOfEdge>;

template <typename DataType>
using CellArrayPerFace = SubItemArrayPerItem<DataType, CellOfFace>;

// Weak versions: should not be used outside of Connectivity
// Item arrays at nodes

template <typename DataType, typename ItemOfItem>
using WeakSubItemArrayPerItem = SubItemArrayPerItem<DataType, ItemOfItem, std::weak_ptr<const IConnectivity>>;

template <typename DataType>
using WeakNodeArrayPerEdge = WeakSubItemArrayPerItem<DataType, NodeOfEdge>;

template <typename DataType>
using WeakNodeArrayPerFace = WeakSubItemArrayPerItem<DataType, NodeOfFace>;

template <typename DataType>
using WeakNodeArrayPerCell = WeakSubItemArrayPerItem<DataType, NodeOfCell>;

// Item arrays at edges

template <typename DataType>
using WeakEdgeArrayPerNode = WeakSubItemArrayPerItem<DataType, EdgeOfNode>;

template <typename DataType>
using WeakEdgeArrayPerFace = WeakSubItemArrayPerItem<DataType, EdgeOfFace>;

template <typename DataType>
using WeakEdgeArrayPerCell = WeakSubItemArrayPerItem<DataType, EdgeOfCell>;

// Item arrays at faces

template <typename DataType>
using WeakFaceArrayPerNode = WeakSubItemArrayPerItem<DataType, FaceOfNode>;

template <typename DataType>
using WeakFaceArrayPerEdge = WeakSubItemArrayPerItem<DataType, FaceOfEdge>;

template <typename DataType>
using WeakFaceArrayPerCell = WeakSubItemArrayPerItem<DataType, FaceOfCell>;

// Item arrays at cells

template <typename DataType>
using WeakCellArrayPerNode = WeakSubItemArrayPerItem<DataType, CellOfNode>;

template <typename DataType>
using WeakCellArrayPerEdge = WeakSubItemArrayPerItem<DataType, CellOfEdge>;

template <typename DataType>
using WeakCellArrayPerFace = WeakSubItemArrayPerItem<DataType, CellOfFace>;

#endif   // SUBITEM_ARRAY_PER_ITEM_HPP
