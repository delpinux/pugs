#include <mesh/MeshFlatEdgeBoundary.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshFlatNodeBoundary.hpp>

template <typename MeshType>
MeshFlatEdgeBoundary<MeshType>
getMeshFlatEdgeBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor)
{
  MeshEdgeBoundary mesh_edge_boundary          = getMeshEdgeBoundary(mesh, boundary_descriptor);
  MeshFlatNodeBoundary mesh_flat_node_boundary = getMeshFlatNodeBoundary(mesh, boundary_descriptor);

  return MeshFlatEdgeBoundary<MeshType>{mesh, mesh_edge_boundary.refEdgeList(),
                                        mesh_flat_node_boundary.outgoingNormal()};
}

template MeshFlatEdgeBoundary<Mesh<1>> getMeshFlatEdgeBoundary(const Mesh<1>&, const IBoundaryDescriptor&);
template MeshFlatEdgeBoundary<Mesh<2>> getMeshFlatEdgeBoundary(const Mesh<2>&, const IBoundaryDescriptor&);
template MeshFlatEdgeBoundary<Mesh<3>> getMeshFlatEdgeBoundary(const Mesh<3>&, const IBoundaryDescriptor&);
