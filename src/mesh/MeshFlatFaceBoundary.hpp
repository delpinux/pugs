#ifndef MESH_FLAT_FACE_BOUNDARY_HPP
#define MESH_FLAT_FACE_BOUNDARY_HPP

#include <mesh/MeshFaceBoundary.hpp>

template <MeshConcept MeshType>
class MeshFlatFaceBoundary final : public MeshFaceBoundary   // clazy:exclude=copyable-polymorphic
{
 public:
  static_assert(not std::is_const_v<MeshType>, "MeshType must be non-const");

  using Rd = TinyVector<MeshType::Dimension, double>;

 private:
  const Rd m_outgoing_normal;

 public:
  const Rd&
  outgoingNormal() const
  {
    return m_outgoing_normal;
  }

  MeshFlatFaceBoundary& operator=(const MeshFlatFaceBoundary&) = default;
  MeshFlatFaceBoundary& operator=(MeshFlatFaceBoundary&&)      = default;

  template <typename MeshTypeT>
  friend MeshFlatFaceBoundary<MeshTypeT> getMeshFlatFaceBoundary(const MeshTypeT& mesh,
                                                                 const IBoundaryDescriptor& boundary_descriptor);

 private:
  MeshFlatFaceBoundary(const MeshType& mesh, const RefFaceList& ref_face_list, const Rd& outgoing_normal)
    : MeshFaceBoundary(mesh, ref_face_list), m_outgoing_normal(outgoing_normal)
  {}

 public:
  MeshFlatFaceBoundary()                            = default;
  MeshFlatFaceBoundary(const MeshFlatFaceBoundary&) = default;
  MeshFlatFaceBoundary(MeshFlatFaceBoundary&&)      = default;
  ~MeshFlatFaceBoundary()                           = default;
};

template <typename MeshType>
MeshFlatFaceBoundary<MeshType> getMeshFlatFaceBoundary(const MeshType& mesh,
                                                       const IBoundaryDescriptor& boundary_descriptor);

#endif   // MESH_FLAT_FACE_BOUNDARY_HPP
