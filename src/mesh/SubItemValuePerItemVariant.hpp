#ifndef SUB_ITEM_VALUE_PER_ITEM_VARIANT_HPP
#define SUB_ITEM_VALUE_PER_ITEM_VARIANT_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <mesh/SubItemValuePerItem.hpp>
#include <utils/Exceptions.hpp>

class SubItemValuePerItemVariant
{
 private:
  using Variant = std::variant<NodeValuePerEdge<const bool>,
                               NodeValuePerEdge<const long int>,
                               NodeValuePerEdge<const unsigned long int>,
                               NodeValuePerEdge<const double>,
                               NodeValuePerEdge<const TinyVector<1, double>>,
                               NodeValuePerEdge<const TinyVector<2, double>>,
                               NodeValuePerEdge<const TinyVector<3, double>>,
                               NodeValuePerEdge<const TinyMatrix<1, 1, double>>,
                               NodeValuePerEdge<const TinyMatrix<2, 2, double>>,
                               NodeValuePerEdge<const TinyMatrix<3, 3, double>>,

                               NodeValuePerFace<const bool>,
                               NodeValuePerFace<const long int>,
                               NodeValuePerFace<const unsigned long int>,
                               NodeValuePerFace<const double>,
                               NodeValuePerFace<const TinyVector<1, double>>,
                               NodeValuePerFace<const TinyVector<2, double>>,
                               NodeValuePerFace<const TinyVector<3, double>>,
                               NodeValuePerFace<const TinyMatrix<1, 1, double>>,
                               NodeValuePerFace<const TinyMatrix<2, 2, double>>,
                               NodeValuePerFace<const TinyMatrix<3, 3, double>>,

                               NodeValuePerCell<const bool>,
                               NodeValuePerCell<const long int>,
                               NodeValuePerCell<const unsigned long int>,
                               NodeValuePerCell<const double>,
                               NodeValuePerCell<const TinyVector<1, double>>,
                               NodeValuePerCell<const TinyVector<2, double>>,
                               NodeValuePerCell<const TinyVector<3, double>>,
                               NodeValuePerCell<const TinyMatrix<1, 1, double>>,
                               NodeValuePerCell<const TinyMatrix<2, 2, double>>,
                               NodeValuePerCell<const TinyMatrix<3, 3, double>>,

                               EdgeValuePerNode<const bool>,
                               EdgeValuePerNode<const long int>,
                               EdgeValuePerNode<const unsigned long int>,
                               EdgeValuePerNode<const double>,
                               EdgeValuePerNode<const TinyVector<1, double>>,
                               EdgeValuePerNode<const TinyVector<2, double>>,
                               EdgeValuePerNode<const TinyVector<3, double>>,
                               EdgeValuePerNode<const TinyMatrix<1, 1, double>>,
                               EdgeValuePerNode<const TinyMatrix<2, 2, double>>,
                               EdgeValuePerNode<const TinyMatrix<3, 3, double>>,

                               EdgeValuePerFace<const bool>,
                               EdgeValuePerFace<const long int>,
                               EdgeValuePerFace<const unsigned long int>,
                               EdgeValuePerFace<const double>,
                               EdgeValuePerFace<const TinyVector<1, double>>,
                               EdgeValuePerFace<const TinyVector<2, double>>,
                               EdgeValuePerFace<const TinyVector<3, double>>,
                               EdgeValuePerFace<const TinyMatrix<1, 1, double>>,
                               EdgeValuePerFace<const TinyMatrix<2, 2, double>>,
                               EdgeValuePerFace<const TinyMatrix<3, 3, double>>,

                               EdgeValuePerCell<const bool>,
                               EdgeValuePerCell<const long int>,
                               EdgeValuePerCell<const unsigned long int>,
                               EdgeValuePerCell<const double>,
                               EdgeValuePerCell<const TinyVector<1, double>>,
                               EdgeValuePerCell<const TinyVector<2, double>>,
                               EdgeValuePerCell<const TinyVector<3, double>>,
                               EdgeValuePerCell<const TinyMatrix<1, 1, double>>,
                               EdgeValuePerCell<const TinyMatrix<2, 2, double>>,
                               EdgeValuePerCell<const TinyMatrix<3, 3, double>>,

                               FaceValuePerNode<const bool>,
                               FaceValuePerNode<const long int>,
                               FaceValuePerNode<const unsigned long int>,
                               FaceValuePerNode<const double>,
                               FaceValuePerNode<const TinyVector<1, double>>,
                               FaceValuePerNode<const TinyVector<2, double>>,
                               FaceValuePerNode<const TinyVector<3, double>>,
                               FaceValuePerNode<const TinyMatrix<1, 1, double>>,
                               FaceValuePerNode<const TinyMatrix<2, 2, double>>,
                               FaceValuePerNode<const TinyMatrix<3, 3, double>>,

                               FaceValuePerEdge<const bool>,
                               FaceValuePerEdge<const long int>,
                               FaceValuePerEdge<const unsigned long int>,
                               FaceValuePerEdge<const double>,
                               FaceValuePerEdge<const TinyVector<1, double>>,
                               FaceValuePerEdge<const TinyVector<2, double>>,
                               FaceValuePerEdge<const TinyVector<3, double>>,
                               FaceValuePerEdge<const TinyMatrix<1, 1, double>>,
                               FaceValuePerEdge<const TinyMatrix<2, 2, double>>,
                               FaceValuePerEdge<const TinyMatrix<3, 3, double>>,

                               FaceValuePerCell<const bool>,
                               FaceValuePerCell<const long int>,
                               FaceValuePerCell<const unsigned long int>,
                               FaceValuePerCell<const double>,
                               FaceValuePerCell<const TinyVector<1, double>>,
                               FaceValuePerCell<const TinyVector<2, double>>,
                               FaceValuePerCell<const TinyVector<3, double>>,
                               FaceValuePerCell<const TinyMatrix<1, 1, double>>,
                               FaceValuePerCell<const TinyMatrix<2, 2, double>>,
                               FaceValuePerCell<const TinyMatrix<3, 3, double>>,

                               CellValuePerNode<const bool>,
                               CellValuePerNode<const long int>,
                               CellValuePerNode<const unsigned long int>,
                               CellValuePerNode<const double>,
                               CellValuePerNode<const TinyVector<1, double>>,
                               CellValuePerNode<const TinyVector<2, double>>,
                               CellValuePerNode<const TinyVector<3, double>>,
                               CellValuePerNode<const TinyMatrix<1, 1, double>>,
                               CellValuePerNode<const TinyMatrix<2, 2, double>>,
                               CellValuePerNode<const TinyMatrix<3, 3, double>>,

                               CellValuePerEdge<const bool>,
                               CellValuePerEdge<const long int>,
                               CellValuePerEdge<const unsigned long int>,
                               CellValuePerEdge<const double>,
                               CellValuePerEdge<const TinyVector<1, double>>,
                               CellValuePerEdge<const TinyVector<2, double>>,
                               CellValuePerEdge<const TinyVector<3, double>>,
                               CellValuePerEdge<const TinyMatrix<1, 1, double>>,
                               CellValuePerEdge<const TinyMatrix<2, 2, double>>,
                               CellValuePerEdge<const TinyMatrix<3, 3, double>>,

                               CellValuePerFace<const bool>,
                               CellValuePerFace<const long int>,
                               CellValuePerFace<const unsigned long int>,
                               CellValuePerFace<const double>,
                               CellValuePerFace<const TinyVector<1, double>>,
                               CellValuePerFace<const TinyVector<2, double>>,
                               CellValuePerFace<const TinyVector<3, double>>,
                               CellValuePerFace<const TinyMatrix<1, 1, double>>,
                               CellValuePerFace<const TinyMatrix<2, 2, double>>,
                               CellValuePerFace<const TinyMatrix<3, 3, double>>>;

  Variant m_sub_item_value_per_item;

 public:
  PUGS_INLINE
  const Variant&
  subItemValuePerItem() const
  {
    return m_sub_item_value_per_item;
  }

  template <typename SubItemValuePerItemT>
  PUGS_INLINE auto
  get() const
  {
    using DataType        = typename SubItemValuePerItemT::data_type;
    using ItemOfItemTypeT = typename SubItemValuePerItemT::ItemOfItemType;

    if constexpr (std::is_same_v<SubItemValuePerItemT, SubItemValuePerItem<DataType, ItemOfItemTypeT>> or
                  std::is_same_v<SubItemValuePerItemT, SubItemValuePerItem<const DataType, ItemOfItemTypeT>> or
                  std::is_same_v<SubItemValuePerItemT, WeakSubItemValuePerItem<DataType, ItemOfItemTypeT>> or
                  std::is_same_v<SubItemValuePerItemT, WeakSubItemValuePerItem<const DataType, ItemOfItemTypeT>>) {
      if (not std::holds_alternative<SubItemValuePerItem<const DataType, ItemOfItemTypeT>>(
            this->m_sub_item_value_per_item)) {
        throw NormalError("invalid SubItemValuePerItem type");
      }
      return std::get<SubItemValuePerItem<const DataType, ItemOfItemTypeT>>(this->m_sub_item_value_per_item);
    } else {
      static_assert(std::is_same_v<SubItemValuePerItemT, SubItemValuePerItemT>, "invalid template argument");
    }
  }

  template <typename DataType, typename ItemOfItemTypeT>
  SubItemValuePerItemVariant(const SubItemValuePerItem<DataType, ItemOfItemTypeT>& sub_item_value_per_item)
    : m_sub_item_value_per_item{SubItemValuePerItem<const DataType, ItemOfItemTypeT>{sub_item_value_per_item}}
  {
    static_assert(std::is_same_v<std::remove_const_t<DataType>, bool> or                         //
                    std::is_same_v<std::remove_const_t<DataType>, long int> or                   //
                    std::is_same_v<std::remove_const_t<DataType>, unsigned long int> or          //
                    std::is_same_v<std::remove_const_t<DataType>, double> or                     //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<1, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<2, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<3, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<1, 1, double>> or   //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<2, 2, double>> or   //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<3, 3, double>>,
                  "SubItemValuePerItem with this DataType is not allowed in variant");
  }

  SubItemValuePerItemVariant& operator=(SubItemValuePerItemVariant&&)      = default;
  SubItemValuePerItemVariant& operator=(const SubItemValuePerItemVariant&) = default;

  SubItemValuePerItemVariant(const SubItemValuePerItemVariant&) = default;
  SubItemValuePerItemVariant(SubItemValuePerItemVariant&&)      = default;

  SubItemValuePerItemVariant()  = delete;
  ~SubItemValuePerItemVariant() = default;
};

#endif   // SUB_ITEM_VALUE_PER_ITEM_VARIANT_HPP
