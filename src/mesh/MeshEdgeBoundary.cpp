#include <mesh/MeshEdgeBoundary.hpp>

#include <Kokkos_Vector.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <utils/Messenger.hpp>

template <MeshConcept MeshType>
MeshEdgeBoundary::MeshEdgeBoundary(const MeshType&, const RefEdgeList& ref_edge_list) : m_ref_edge_list(ref_edge_list)
{}

template MeshEdgeBoundary::MeshEdgeBoundary(const Mesh<1>&, const RefEdgeList&);
template MeshEdgeBoundary::MeshEdgeBoundary(const Mesh<2>&, const RefEdgeList&);
template MeshEdgeBoundary::MeshEdgeBoundary(const Mesh<3>&, const RefEdgeList&);

template <MeshConcept MeshType>
MeshEdgeBoundary::MeshEdgeBoundary(const MeshType& mesh, const RefFaceList& ref_face_list)
{
  constexpr size_t Dimension = MeshType::Dimension;

  const Array<const FaceId>& face_list = ref_face_list.list();
  static_assert(Dimension > 1, "conversion from to edge from face is valid in dimension > 1");

  if constexpr (Dimension > 2) {
    Kokkos::vector<unsigned int> edge_ids;
    // not enough but should reduce significantly the number of resizing
    edge_ids.reserve(Dimension * face_list.size());
    const auto& face_to_edge_matrix = mesh.connectivity().faceToEdgeMatrix();

    for (size_t l = 0; l < face_list.size(); ++l) {
      const FaceId face_number = face_list[l];
      const auto& face_edges   = face_to_edge_matrix[face_number];

      for (size_t e = 0; e < face_edges.size(); ++e) {
        edge_ids.push_back(face_edges[e]);
      }
    }
    std::sort(edge_ids.begin(), edge_ids.end());
    auto last = std::unique(edge_ids.begin(), edge_ids.end());
    edge_ids.resize(std::distance(edge_ids.begin(), last));

    Array<EdgeId> edge_list(edge_ids.size());
    parallel_for(
      edge_ids.size(), PUGS_LAMBDA(int r) { edge_list[r] = edge_ids[r]; });
    m_ref_edge_list = RefEdgeList{ref_face_list.refId(), edge_list, ref_face_list.type()};
  } else if constexpr (Dimension == 2) {
    Array<EdgeId> edge_list(face_list.size());
    parallel_for(
      face_list.size(), PUGS_LAMBDA(int r) { edge_list[r] = static_cast<FaceId::base_type>(face_list[r]); });
    m_ref_edge_list = RefEdgeList{ref_face_list.refId(), edge_list, ref_face_list.type()};
  }

  // This is quite dirty but it allows a non negligible performance
  // improvement
  const_cast<Connectivity<Dimension>&>(mesh.connectivity()).addRefItemList(m_ref_edge_list);
}

template MeshEdgeBoundary::MeshEdgeBoundary(const Mesh<2>&, const RefFaceList&);
template MeshEdgeBoundary::MeshEdgeBoundary(const Mesh<3>&, const RefFaceList&);

template <MeshConcept MeshType>
MeshEdgeBoundary
getMeshEdgeBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor)
{
  constexpr size_t Dimension = MeshType::Dimension;

  for (size_t i_ref_edge_list = 0; i_ref_edge_list < mesh.connectivity().template numberOfRefItemList<ItemType::edge>();
       ++i_ref_edge_list) {
    const auto& ref_edge_list = mesh.connectivity().template refItemList<ItemType::edge>(i_ref_edge_list);
    const RefId& ref          = ref_edge_list.refId();

    if (ref == boundary_descriptor) {
      auto edge_list = ref_edge_list.list();
      if (ref_edge_list.type() != RefItemListBase::Type::boundary) {
        std::ostringstream ost;
        ost << "invalid boundary " << rang::fgB::yellow << boundary_descriptor << rang::style::reset
            << ": inner edges cannot be used to define mesh boundaries";
        throw NormalError(ost.str());
      }

      return MeshEdgeBoundary{mesh, ref_edge_list};
    }
  }
  if constexpr (Dimension > 1) {
    for (size_t i_ref_face_list = 0;
         i_ref_face_list < mesh.connectivity().template numberOfRefItemList<ItemType::face>(); ++i_ref_face_list) {
      const auto& ref_face_list = mesh.connectivity().template refItemList<ItemType::face>(i_ref_face_list);
      const RefId& ref          = ref_face_list.refId();

      if (ref == boundary_descriptor) {
        auto face_list = ref_face_list.list();
        if (ref_face_list.type() != RefItemListBase::Type::boundary) {
          std::ostringstream ost;
          ost << "invalid boundary " << rang::fgB::yellow << boundary_descriptor << rang::style::reset
              << ": inner edges cannot be used to define mesh boundaries";
          throw NormalError(ost.str());
        }

        return MeshEdgeBoundary{mesh, ref_face_list};
      }
    }
  }

  std::ostringstream ost;
  ost << "cannot find edge list with name " << rang::fgB::red << boundary_descriptor << rang::style::reset;

  throw NormalError(ost.str());
}

template MeshEdgeBoundary getMeshEdgeBoundary(const Mesh<1>&, const IBoundaryDescriptor&);
template MeshEdgeBoundary getMeshEdgeBoundary(const Mesh<2>&, const IBoundaryDescriptor&);
template MeshEdgeBoundary getMeshEdgeBoundary(const Mesh<3>&, const IBoundaryDescriptor&);
