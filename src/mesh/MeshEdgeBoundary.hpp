#ifndef MESH_EDGE_BOUNDARY_HPP
#define MESH_EDGE_BOUNDARY_HPP

#include <algebra/TinyVector.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/RefItemList.hpp>
#include <utils/Array.hpp>

class [[nodiscard]] MeshEdgeBoundary   // clazy:exclude=copyable-polymorphic
{
 protected:
  RefEdgeList m_ref_edge_list;

 public:
  template <MeshConcept MeshType>
  friend MeshEdgeBoundary getMeshEdgeBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor);

  MeshEdgeBoundary& operator=(const MeshEdgeBoundary&) = default;
  MeshEdgeBoundary& operator=(MeshEdgeBoundary&&) = default;

  PUGS_INLINE
  const RefEdgeList& refEdgeList() const
  {
    return m_ref_edge_list;
  }

  PUGS_INLINE
  const Array<const EdgeId>& edgeList() const
  {
    return m_ref_edge_list.list();
  }

 protected:
  template <MeshConcept MeshType>
  MeshEdgeBoundary(const MeshType& mesh, const RefEdgeList& ref_edge_list);
  template <MeshConcept MeshType>
  MeshEdgeBoundary(const MeshType& mesh, const RefFaceList& ref_face_list);

 public:
  MeshEdgeBoundary(const MeshEdgeBoundary&) = default;   // LCOV_EXCL_LINE
  MeshEdgeBoundary(MeshEdgeBoundary &&)     = default;   // LCOV_EXCL_LINE

  MeshEdgeBoundary()          = default;
  virtual ~MeshEdgeBoundary() = default;
};

template <MeshConcept MeshType>
MeshEdgeBoundary getMeshEdgeBoundary(const MeshType& mesh, const IBoundaryDescriptor& boundary_descriptor);

#endif   // MESH_EDGE_BOUNDARY_HPP
