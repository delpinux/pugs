#ifndef ITEM_ARRAY_VARIANT_HPP
#define ITEM_ARRAY_VARIANT_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <mesh/ItemArray.hpp>
#include <utils/Exceptions.hpp>

class ItemArrayVariant
{
 private:
  using Variant = std::variant<NodeArray<const bool>,
                               NodeArray<const long int>,
                               NodeArray<const unsigned long int>,
                               NodeArray<const double>,
                               NodeArray<const TinyVector<1, double>>,
                               NodeArray<const TinyVector<2, double>>,
                               NodeArray<const TinyVector<3, double>>,
                               NodeArray<const TinyMatrix<1, 1, double>>,
                               NodeArray<const TinyMatrix<2, 2, double>>,
                               NodeArray<const TinyMatrix<3, 3, double>>,

                               EdgeArray<const bool>,
                               EdgeArray<const long int>,
                               EdgeArray<const unsigned long int>,
                               EdgeArray<const double>,
                               EdgeArray<const TinyVector<1, double>>,
                               EdgeArray<const TinyVector<2, double>>,
                               EdgeArray<const TinyVector<3, double>>,
                               EdgeArray<const TinyMatrix<1, 1, double>>,
                               EdgeArray<const TinyMatrix<2, 2, double>>,
                               EdgeArray<const TinyMatrix<3, 3, double>>,

                               FaceArray<const bool>,
                               FaceArray<const long int>,
                               FaceArray<const unsigned long int>,
                               FaceArray<const double>,
                               FaceArray<const TinyVector<1, double>>,
                               FaceArray<const TinyVector<2, double>>,
                               FaceArray<const TinyVector<3, double>>,
                               FaceArray<const TinyMatrix<1, 1, double>>,
                               FaceArray<const TinyMatrix<2, 2, double>>,
                               FaceArray<const TinyMatrix<3, 3, double>>,

                               CellArray<const bool>,
                               CellArray<const long int>,
                               CellArray<const unsigned long int>,
                               CellArray<const double>,
                               CellArray<const TinyVector<1, double>>,
                               CellArray<const TinyVector<2, double>>,
                               CellArray<const TinyVector<3, double>>,
                               CellArray<const TinyMatrix<1, 1, double>>,
                               CellArray<const TinyMatrix<2, 2, double>>,
                               CellArray<const TinyMatrix<3, 3, double>>>;

  Variant m_item_array;

 public:
  PUGS_INLINE
  const Variant&
  itemArray() const
  {
    return m_item_array;
  }

  template <typename ItemArrayT>
  PUGS_INLINE auto
  get() const
  {
    using DataType               = typename ItemArrayT::data_type;
    constexpr ItemType item_type = ItemArrayT::item_t;

    if constexpr (std::is_same_v<ItemArrayT, ItemArray<DataType, item_type>> or
                  std::is_same_v<ItemArrayT, ItemArray<const DataType, item_type>> or
                  std::is_same_v<ItemArrayT, WeakItemArray<DataType, item_type>> or
                  std::is_same_v<ItemArrayT, WeakItemArray<const DataType, item_type>>) {
      if (not std::holds_alternative<ItemArray<const DataType, item_type>>(this->m_item_array)) {
        throw NormalError("invalid ItemArray type");
      }
      return std::get<ItemArray<const DataType, item_type>>(this->itemArray());
    } else {
      static_assert(std::is_same_v<ItemArrayT, ItemArrayT>, "invalid template argument");
    }
  }

  template <typename DataType, ItemType item_type>
  ItemArrayVariant(const ItemArray<DataType, item_type>& item_array)
    : m_item_array{ItemArray<const DataType, item_type>{item_array}}
  {
    static_assert(std::is_same_v<std::remove_const_t<DataType>, bool> or                         //
                    std::is_same_v<std::remove_const_t<DataType>, long int> or                   //
                    std::is_same_v<std::remove_const_t<DataType>, unsigned long int> or          //
                    std::is_same_v<std::remove_const_t<DataType>, double> or                     //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<1, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<2, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<3, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<1, 1, double>> or   //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<2, 2, double>> or   //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<3, 3, double>>,
                  "ItemArray with this DataType is not allowed in variant");
  }

  ItemArrayVariant& operator=(ItemArrayVariant&&) = default;
  ItemArrayVariant& operator=(const ItemArrayVariant&) = default;

  ItemArrayVariant(const ItemArrayVariant&) = default;
  ItemArrayVariant(ItemArrayVariant&&)      = default;

  ItemArrayVariant()  = delete;
  ~ItemArrayVariant() = default;
};

#endif   // ITEM_ARRAY_VARIANT_HPP
