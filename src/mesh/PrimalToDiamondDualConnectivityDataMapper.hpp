#ifndef PRIMAL_TO_DIAMOND_DUAL_CONNECTIVITY_DATA_MAPPER_HPP
#define PRIMAL_TO_DIAMOND_DUAL_CONNECTIVITY_DATA_MAPPER_HPP

#include <mesh/Connectivity.hpp>
#include <mesh/IPrimalToDualConnectivityDataMapper.hpp>
#include <mesh/ItemIdToItemIdMap.hpp>
#include <mesh/ItemValue.hpp>
#include <utils/Array.hpp>
#include <utils/PugsAssert.hpp>

template <size_t Dimension>
class PrimalToDiamondDualConnectivityDataMapper : public IPrimalToDualConnectivityDataMapper
{
  static_assert(Dimension == 2 or Dimension == 3,
                "primal to diamond dual connectivity mapper is defined in dimension 2 or 3");

 private:
  const IConnectivity* m_primal_connectivity;
  const IConnectivity* m_dual_connectivity;

  ConstNodeIdToNodeIdMap m_primal_node_to_dual_node_map;
  ConstCellIdToNodeIdMap m_primal_cell_to_dual_node_map;
  ConstFaceIdToCellIdMap m_primal_face_to_dual_cell_map;

 public:
  template <typename OriginDataType1, typename OriginDataType2, typename DestinationDataType>
  void
  toDualNode(const NodeValue<OriginDataType1>& primal_node_value,
             const CellValue<OriginDataType2>& primal_cell_value,
             const NodeValue<DestinationDataType>& dual_node_value) const
  {
    static_assert(not std::is_const_v<DestinationDataType>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType1>, DestinationDataType>, "incompatible types");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType2>, DestinationDataType>, "incompatible types");

    Assert(m_primal_connectivity == primal_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for primal CellValue");
    Assert(m_primal_connectivity == primal_node_value.connectivity_ptr().get(),
           "unexpected connectivity for primal NodeValue");
    Assert(m_dual_connectivity == dual_node_value.connectivity_ptr().get(),
           "unexpected connectivity for dual NodeValue");

    parallel_for(
      m_primal_node_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_node_id, dual_node_id] = m_primal_node_to_dual_node_map[i];

        dual_node_value[dual_node_id] = primal_node_value[primal_node_id];
      });

    parallel_for(
      m_primal_cell_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_cell_id, dual_node_id] = m_primal_cell_to_dual_node_map[i];
        dual_node_value[dual_node_id]             = primal_cell_value[primal_cell_id];
      });
  }

  template <typename OriginDataType, typename DestinationDataType1, typename DestinationDataType2>
  void
  fromDualNode(const NodeValue<OriginDataType>& dual_node_value,
               const NodeValue<DestinationDataType1>& primal_node_value,
               const CellValue<DestinationDataType2>& primal_cell_value) const
  {
    static_assert(not std::is_const_v<DestinationDataType1>, "destination data type must not be constant");
    static_assert(not std::is_const_v<DestinationDataType2>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType1>, "incompatible types");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType2>, "incompatible types");

    Assert(m_primal_connectivity == primal_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for primal CellValue");
    Assert(m_primal_connectivity == primal_node_value.connectivity_ptr().get(),
           "unexpected connectivity for primal NodeValue");
    Assert(m_dual_connectivity == dual_node_value.connectivity_ptr().get(),
           "unexpected connectivity for dual NodeValue");

    parallel_for(
      m_primal_node_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_node_id, dual_node_id] = m_primal_node_to_dual_node_map[i];

        primal_node_value[primal_node_id] = dual_node_value[dual_node_id];
      });

    parallel_for(
      m_primal_cell_to_dual_node_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_cell_id, dual_node_id] = m_primal_cell_to_dual_node_map[i];
        primal_cell_value[primal_cell_id]         = dual_node_value[dual_node_id];
      });
  }

  template <typename OriginDataType, typename DestinationDataType, ItemType origin_face_type>
  void
  toDualCell(const ItemValue<OriginDataType, origin_face_type>& primal_face_value,
             const CellValue<DestinationDataType>& dual_cell_value) const
  {
    static_assert(not std::is_const_v<DestinationDataType>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType>, "incompatible types");

    static_assert(((Dimension == 3) and (origin_face_type == ItemType::face)) or (is_face_in_2d_v<origin_face_type>),
                  "invalid destination face type");

    Assert(m_primal_connectivity == primal_face_value.connectivity_ptr().get(),
           "unexpected connectivity for primal FaceValue");
    Assert(m_dual_connectivity == dual_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for dual CellValue");

    using OriginFaceId = ItemIdT<origin_face_type>;

    parallel_for(
      m_primal_face_to_dual_cell_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_face_id, dual_cell_id] = m_primal_face_to_dual_cell_map[i];

        const OriginFaceId origin_face_id = static_cast<typename OriginFaceId::base_type>(primal_face_id);

        dual_cell_value[dual_cell_id] = primal_face_value[origin_face_id];
      });
  }

  template <typename OriginDataType, typename DestinationDataType, ItemType destination_face_type>
  void
  fromDualCell(const CellValue<OriginDataType>& dual_cell_value,
               const ItemValue<DestinationDataType, destination_face_type>& primal_face_value) const
  {
    static_assert(not std::is_const_v<DestinationDataType>, "destination data type must not be constant");
    static_assert(std::is_same_v<std::remove_const_t<OriginDataType>, DestinationDataType>, "incompatible types");

    Assert(m_primal_connectivity == primal_face_value.connectivity_ptr().get(),
           "unexpected connectivity for primal FaceValue");
    Assert(m_dual_connectivity == dual_cell_value.connectivity_ptr().get(),
           "unexpected connectivity for dual CellValue");

    static_assert(((Dimension == 3) and (destination_face_type == ItemType::face)) or
                    (is_face_in_2d_v<destination_face_type>),
                  "invalid destination face type");

    using DestinationFaceId = ItemIdT<destination_face_type>;

    parallel_for(
      m_primal_face_to_dual_cell_map.size(), PUGS_CLASS_LAMBDA(size_t i) {
        const auto [primal_face_id, dual_cell_id] = m_primal_face_to_dual_cell_map[i];

        const DestinationFaceId destination_face_id =
          static_cast<typename DestinationFaceId::base_type>(primal_face_id);

        primal_face_value[destination_face_id] = dual_cell_value[dual_cell_id];
      });
  }

  PrimalToDiamondDualConnectivityDataMapper(const Connectivity<Dimension>& primal_connectivity,
                                            const Connectivity<Dimension>& dual_connectivity,
                                            const ConstNodeIdToNodeIdMap& primal_node_to_dual_node_map,
                                            const ConstCellIdToNodeIdMap& primal_cell_to_dual_node_map,
                                            const ConstFaceIdToCellIdMap& primal_face_to_dual_cell_map)
    : m_primal_connectivity{&primal_connectivity},
      m_dual_connectivity{&dual_connectivity},
      m_primal_node_to_dual_node_map{primal_node_to_dual_node_map},
      m_primal_cell_to_dual_node_map{primal_cell_to_dual_node_map},
      m_primal_face_to_dual_cell_map{primal_face_to_dual_cell_map}
  {}
};

#endif   // PRIMAL_TO_DIAMOND_DUAL_CONNECTIVITY_DATA_MAPPER_HPP
