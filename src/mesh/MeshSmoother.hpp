#ifndef MESH_SMOOTHER_HPP
#define MESH_SMOOTHER_HPP

#include <mesh/MeshTraits.hpp>

#include <memory>
#include <vector>

class MeshVariant;
class FunctionSymbolId;
class IBoundaryConditionDescriptor;
class IZoneDescriptor;
class DiscreteFunctionVariant;

class MeshSmootherHandler
{
 private:
  template <MeshConcept MeshType>
  class MeshSmoother;

 public:
  std::shared_ptr<const MeshVariant> getSmoothedMesh(
    const std::shared_ptr<const MeshVariant>& mesh_v,
    const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>& bc_descriptor_list) const;

  std::shared_ptr<const MeshVariant> getSmoothedMesh(
    const std::shared_ptr<const MeshVariant>& mesh_v,
    const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>& bc_descriptor_list,
    const FunctionSymbolId& function_symbol_id) const;

  std::shared_ptr<const MeshVariant> getSmoothedMesh(
    const std::shared_ptr<const MeshVariant>& mesh_v,
    const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>& bc_descriptor_list,
    const std::vector<std::shared_ptr<const IZoneDescriptor>>& smoothing_zone_list) const;

  std::shared_ptr<const MeshVariant> getSmoothedMesh(
    const std::shared_ptr<const MeshVariant>& mesh_v,
    const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>& bc_descriptor_list,
    const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& smoothing_zone_list) const;

  MeshSmootherHandler()                      = default;
  MeshSmootherHandler(MeshSmootherHandler&&) = default;
  ~MeshSmootherHandler()                     = default;
};

#endif   // MESH_RANDOMIZER_HPP
