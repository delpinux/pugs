#include <mesh/MeshVariant.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>

size_t
MeshVariant::id() const
{
  return std::visit([](auto&& mesh) { return mesh->id(); }, m_p_mesh_variant);
}

std::ostream&
operator<<(std::ostream& os, const MeshVariant& mesh_v)
{
  std::visit([&](auto&& p_mesh) { os << *p_mesh; }, mesh_v.variant());
  return os;
}

size_t
MeshVariant::numberOfCells() const
{
  return std::visit([](auto&& mesh) { return mesh->numberOfCells(); }, m_p_mesh_variant);
}

size_t
MeshVariant::numberOfFaces() const
{
  return std::visit([](auto&& mesh) { return mesh->numberOfFaces(); }, m_p_mesh_variant);
}

size_t
MeshVariant::numberOfEdges() const
{
  return std::visit([](auto&& mesh) { return mesh->numberOfEdges(); }, m_p_mesh_variant);
}

size_t
MeshVariant::numberOfNodes() const
{
  return std::visit([](auto&& mesh) { return mesh->numberOfNodes(); }, m_p_mesh_variant);
}

const IConnectivity&
MeshVariant::connectivity() const
{
  return std::visit([](auto&& mesh) -> const IConnectivity& { return mesh->connectivity(); }, m_p_mesh_variant);
}

std::shared_ptr<const IConnectivity>
MeshVariant::shared_connectivity() const
{
  return std::visit([](auto&& mesh) -> std::shared_ptr<const IConnectivity> { return mesh->shared_connectivity(); },
                    m_p_mesh_variant);
}
