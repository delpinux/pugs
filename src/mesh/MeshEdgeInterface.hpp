#ifndef MESH_EDGE_INTERFACE_HPP
#define MESH_EDGE_INTERFACE_HPP

#include <algebra/TinyVector.hpp>
#include <mesh/IInterfaceDescriptor.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/RefItemList.hpp>
#include <utils/Array.hpp>

class [[nodiscard]] MeshEdgeInterface   // clazy:exclude=copyable-polymorphic
{
 protected:
  RefEdgeList m_ref_edge_list;

 public:
  template <MeshConcept MeshType>
  friend MeshEdgeInterface getMeshEdgeInterface(const MeshType& mesh, const IInterfaceDescriptor& interface_descriptor);

  MeshEdgeInterface& operator=(const MeshEdgeInterface&) = default;
  MeshEdgeInterface& operator=(MeshEdgeInterface&&) = default;

  PUGS_INLINE
  const RefEdgeList& refEdgeList() const
  {
    return m_ref_edge_list;
  }

  PUGS_INLINE
  const Array<const EdgeId>& edgeList() const
  {
    return m_ref_edge_list.list();
  }

 protected:
  template <MeshConcept MeshType>
  MeshEdgeInterface(const MeshType& mesh, const RefEdgeList& ref_edge_list);
  template <MeshConcept MeshType>
  MeshEdgeInterface(const MeshType& mesh, const RefFaceList& ref_face_list);

 public:
  MeshEdgeInterface(const MeshEdgeInterface&) = default;   // LCOV_EXCL_LINE
  MeshEdgeInterface(MeshEdgeInterface &&)     = default;   // LCOV_EXCL_LINE

  MeshEdgeInterface()          = default;
  virtual ~MeshEdgeInterface() = default;
};

template <MeshConcept MeshType>
MeshEdgeInterface getMeshEdgeInterface(const MeshType& mesh, const IInterfaceDescriptor& interface_descriptor);

#endif   // MESH_EDGE_INTERFACE_HPP
