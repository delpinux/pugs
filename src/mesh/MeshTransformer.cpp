#include <mesh/MeshTransformer.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ItemArray.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>

#include <language/utils/EvaluateAtPoints.hpp>

template <typename OutputType, typename InputType>
class MeshTransformer::MeshTransformation<OutputType(InputType)>
{
  static constexpr size_t Dimension = OutputType::Dimension;

 public:
  template <size_t Dimension>
  static std::shared_ptr<const Mesh<Dimension>>
  transform(const FunctionSymbolId& function_symbol_id, const Mesh<Dimension>& mesh)
  {
    NodeValue<OutputType> xr(mesh.connectivity());
    NodeValue<const InputType> given_xr = mesh.xr();
    EvaluateAtPoints<OutputType(InputType)>::evaluateTo(function_symbol_id, given_xr, xr);

    return std::make_shared<const Mesh<Dimension>>(mesh.shared_connectivity(), xr);
  }
};

std::shared_ptr<const MeshVariant>
MeshTransformer::transform(const FunctionSymbolId& function_id, std::shared_ptr<const MeshVariant> mesh_v)

{
  return std::visit(
    [&](auto&& mesh) {
      using MeshType             = mesh_type_t<decltype(mesh)>;
      constexpr size_t Dimension = MeshType::Dimension;
      using TransformT           = TinyVector<Dimension>(TinyVector<Dimension>);

      return std::make_shared<MeshVariant>(MeshTransformation<TransformT>::transform(function_id, *mesh));
    },
    mesh_v->variant());
}
