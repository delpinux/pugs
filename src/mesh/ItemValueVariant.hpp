#ifndef ITEM_VALUE_VARIANT_HPP
#define ITEM_VALUE_VARIANT_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <mesh/ItemValue.hpp>
#include <utils/Exceptions.hpp>

class ItemValueVariant
{
 private:
  using Variant = std::variant<NodeValue<const bool>,
                               NodeValue<const long int>,
                               NodeValue<const unsigned long int>,
                               NodeValue<const double>,
                               NodeValue<const TinyVector<1, double>>,
                               NodeValue<const TinyVector<2, double>>,
                               NodeValue<const TinyVector<3, double>>,
                               NodeValue<const TinyMatrix<1, 1, double>>,
                               NodeValue<const TinyMatrix<2, 2, double>>,
                               NodeValue<const TinyMatrix<3, 3, double>>,

                               EdgeValue<const bool>,
                               EdgeValue<const long int>,
                               EdgeValue<const unsigned long int>,
                               EdgeValue<const double>,
                               EdgeValue<const TinyVector<1, double>>,
                               EdgeValue<const TinyVector<2, double>>,
                               EdgeValue<const TinyVector<3, double>>,
                               EdgeValue<const TinyMatrix<1, 1, double>>,
                               EdgeValue<const TinyMatrix<2, 2, double>>,
                               EdgeValue<const TinyMatrix<3, 3, double>>,

                               FaceValue<const bool>,
                               FaceValue<const long int>,
                               FaceValue<const unsigned long int>,
                               FaceValue<const double>,
                               FaceValue<const TinyVector<1, double>>,
                               FaceValue<const TinyVector<2, double>>,
                               FaceValue<const TinyVector<3, double>>,
                               FaceValue<const TinyMatrix<1, 1, double>>,
                               FaceValue<const TinyMatrix<2, 2, double>>,
                               FaceValue<const TinyMatrix<3, 3, double>>,

                               CellValue<const bool>,
                               CellValue<const long int>,
                               CellValue<const unsigned long int>,
                               CellValue<const double>,
                               CellValue<const TinyVector<1, double>>,
                               CellValue<const TinyVector<2, double>>,
                               CellValue<const TinyVector<3, double>>,
                               CellValue<const TinyMatrix<1, 1, double>>,
                               CellValue<const TinyMatrix<2, 2, double>>,
                               CellValue<const TinyMatrix<3, 3, double>>>;

  Variant m_item_value;

 public:
  PUGS_INLINE
  const Variant&
  itemValue() const
  {
    return m_item_value;
  }

  template <typename ItemValueT>
  PUGS_INLINE auto
  get() const
  {
    using DataType               = typename ItemValueT::data_type;
    constexpr ItemType item_type = ItemValueT::item_t;

    if constexpr (std::is_same_v<ItemValueT, ItemValue<DataType, item_type>> or
                  std::is_same_v<ItemValueT, ItemValue<const DataType, item_type>> or
                  std::is_same_v<ItemValueT, WeakItemValue<DataType, item_type>> or
                  std::is_same_v<ItemValueT, WeakItemValue<const DataType, item_type>>) {
      if (not std::holds_alternative<ItemValue<const DataType, item_type>>(this->m_item_value)) {
        throw NormalError("invalid ItemValue type");
      }
      return std::get<ItemValue<const DataType, item_type>>(this->itemValue());
    } else {
      static_assert(std::is_same_v<ItemValueT, ItemValueT>, "invalid template argument");
    }
  }

  template <typename DataType, ItemType item_type>
  ItemValueVariant(const ItemValue<DataType, item_type>& item_value)
    : m_item_value{ItemValue<const DataType, item_type>{item_value}}
  {
    static_assert(std::is_same_v<std::remove_const_t<DataType>, bool> or                         //
                    std::is_same_v<std::remove_const_t<DataType>, long int> or                   //
                    std::is_same_v<std::remove_const_t<DataType>, unsigned long int> or          //
                    std::is_same_v<std::remove_const_t<DataType>, double> or                     //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<1, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<2, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<3, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<1, 1, double>> or   //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<2, 2, double>> or   //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<3, 3, double>>,
                  "ItemValue with this DataType is not allowed in variant");
  }

  ItemValueVariant& operator=(ItemValueVariant&&) = default;
  ItemValueVariant& operator=(const ItemValueVariant&) = default;

  ItemValueVariant(const ItemValueVariant&) = default;
  ItemValueVariant(ItemValueVariant&&)      = default;

  ItemValueVariant()  = delete;
  ~ItemValueVariant() = default;
};

#endif   // ITEM_VALUE_VARIANT_HPP
