#include <utils/PugsAssert.hpp>

#include <mesh/Synchronizer.hpp>
#include <mesh/SynchronizerManager.hpp>

SynchronizerManager* SynchronizerManager::m_instance{nullptr};

void
SynchronizerManager::create()
{
  Assert(m_instance == nullptr, "SynchronizerManager is already created");
  m_instance = new SynchronizerManager;
}

void
SynchronizerManager::destroy()
{
  Assert(m_instance != nullptr, "SynchronizerManager was not created!");

  if (m_instance->m_connectivity_synchronizer_map.size() > 0) {
    // LCOV_EXCL_START
    std::stringstream error;
    error << ": some connectivities are still registered\n";
    for (const auto& i_connectivity_synchronizer : m_instance->m_connectivity_synchronizer_map) {
      error << " - connectivity " << rang::fgB::magenta << i_connectivity_synchronizer.first << rang::style::reset
            << '\n';
    }
    throw UnexpectedError(error.str());
    // LCOV_EXCL_STOP
  }
  delete m_instance;
  m_instance = nullptr;
}

void
SynchronizerManager::deleteConnectivitySynchronizer(const IConnectivity* connectivity)
{
  m_connectivity_synchronizer_map.erase(connectivity);
}

Synchronizer&
SynchronizerManager::getConnectivitySynchronizer(const IConnectivity* connectivity)
{
  if (auto connectivity_synchronizer = m_connectivity_synchronizer_map.find(connectivity);
      connectivity_synchronizer != m_connectivity_synchronizer_map.end()) {
    return (*connectivity_synchronizer->second);
  } else {
    std::shared_ptr<Synchronizer> synchronizer(new Synchronizer);
    m_connectivity_synchronizer_map[connectivity] = synchronizer;
    return *synchronizer;
  }
}
