#ifndef MEDIAN_DUAL_MESH_BUILDER_HPP
#define MEDIAN_DUAL_MESH_BUILDER_HPP

#include <mesh/MeshBuilderBase.hpp>
#include <mesh/MeshTraits.hpp>

class MeshVariant;
class MedianDualMeshBuilder : public MeshBuilderBase
{
 private:
  template <MeshConcept MeshType>
  void _buildMedianDualMeshFrom(const MeshType&);

  friend class DualMeshManager;
  MedianDualMeshBuilder(const std::shared_ptr<const MeshVariant>&);

 public:
  ~MedianDualMeshBuilder() = default;
};

#endif   // MEDIAN_DUAL_MESH_BUILDER_HPP
