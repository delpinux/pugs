#ifndef NDEBUG

#ifndef INVALID_DATA_HPP
#define INVALID_DATA_HPP

#include <limits>
#include <type_traits>

template <typename DataType>
struct invalid_data
{
  static constexpr DataType value = [] {
    using T = std::decay_t<DataType>;
    static_assert(std::is_arithmetic_v<T>);
    if constexpr (std::is_floating_point_v<T>) {
      return std::numeric_limits<T>::signaling_NaN();
    } else if constexpr (std::is_integral_v<T>) {
      return std::numeric_limits<T>::max() / 2;
    } else {
      return T{};
    }
  }();
};

template <typename DataType>
inline constexpr DataType invalid_data_v = invalid_data<DataType>::value;

#endif   // INVALID_DATA_HPP

#endif   // NDEBUG
