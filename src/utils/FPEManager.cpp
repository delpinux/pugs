#include <utils/FPEManager.hpp>

#include <utils/PugsMacros.hpp>
#include <utils/pugs_config.hpp>

#include <rang.hpp>

#ifdef PUGS_HAS_FENV_H
#include <fenv.h>

#define MANAGED_FPE (FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW)

#ifdef SYSTEM_IS_DARWIN
// MacOS-X requires definition of feenableexcept and fedisableexcept

// Public domain polyfill for feenableexcept on OS X
// http://www-personal.umich.edu/~williams/archive/computation/fe-handling-example.c
PUGS_INLINE
int
feenableexcept(unsigned int excepts)
{
  static fenv_t fenv;
  unsigned int new_excepts = excepts & FE_ALL_EXCEPT;
  // previous masks
  unsigned int old_excepts;

  if (fegetenv(&fenv)) {
    return -1;
  }
  old_excepts = fenv.__control & FE_ALL_EXCEPT;

  // unmask
  fenv.__control &= ~new_excepts;
  fenv.__mxcsr &= ~(new_excepts << 7);

  return fesetenv(&fenv) ? -1 : old_excepts;
}

PUGS_INLINE
int
fedisableexcept(unsigned int excepts)
{
  static fenv_t fenv;
  unsigned int new_excepts = excepts & FE_ALL_EXCEPT;
  // all previous masks
  unsigned int old_excepts;

  if (fegetenv(&fenv)) {
    return -1;
  }
  old_excepts = fenv.__control & FE_ALL_EXCEPT;

  // mask
  fenv.__control |= new_excepts;
  fenv.__mxcsr |= new_excepts << 7;

  return fesetenv(&fenv) ? -1 : old_excepts;
}

#endif   // SYSTEM_IS_DARWIN

void
FPEManager::enable()
{
  ::feenableexcept(MANAGED_FPE);
}

void
FPEManager::disable()
{
  ::fedisableexcept(MANAGED_FPE);
}

#else   // PUGS_HAS_FENV_H

void
FPEManager::enable()
{}

void
FPEManager::disable()
{}

#endif   // PUGS_HAS_FENV_H

void
FPEManager::init(bool enable)
{
  if (enable) {
    FPEManager::enable();
  } else {
    FPEManager::disable();
  }
}
