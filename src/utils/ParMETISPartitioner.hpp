#ifndef PARMETIS_PARTITIONER_HPP
#define PARMETIS_PARTITIONER_HPP

#include <utils/Array.hpp>
#include <utils/CRSGraph.hpp>

class ParMETISPartitioner
{
 private:
  friend class Partitioner;

  // Forbids direct calls
  static Array<int> partition(const CRSGraph& graph);

 public:
  ParMETISPartitioner() = delete;
};

#endif   // PARMETIS_PARTITIONER_HPP
