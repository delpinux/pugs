#ifndef EXECUTION_STAT_MANAGER_HPP
#define EXECUTION_STAT_MANAGER_HPP

#include <utils/PugsAssert.hpp>
#include <utils/Timer.hpp>

#include <iostream>

class ExecutionStatManager
{
 private:
  // For unit tests only
  friend class ExecutionStatManagerTester;

  static ExecutionStatManager* m_instance;

  Timer m_elapse_time;
  bool m_do_print = true;
  int m_exit_code = 0;

  size_t m_run_number                         = 1;
  double m_previous_cumulative_elapse_time    = 0;
  double m_previous_cumulative_total_cpu_time = 0;

  std::string _prettyPrintTime(double seconds) const;

  void _printMaxResidentMemory(std::ostream& os) const;
  void _printElapseTime(std::ostream& os) const;
  void _printTotalCPUTime(std::ostream& os) const;

  explicit ExecutionStatManager()                   = default;
  ExecutionStatManager(ExecutionStatManager&&)      = delete;
  ExecutionStatManager(const ExecutionStatManager&) = delete;
  ~ExecutionStatManager()                           = default;

 public:
  double getElapseTime() const;

  double getCumulativeElapseTime() const;

  double getCumulativeTotalCPUTime() const;

  PUGS_INLINE
  void
  setPreviousCumulativeElapseTime(double cumulative_elapse_time)
  {
    m_previous_cumulative_elapse_time = cumulative_elapse_time;
  }

  PUGS_INLINE
  void
  setPreviousCumulativeTotalCPUTime(double cumulative_cpu_time)
  {
    m_previous_cumulative_total_cpu_time = cumulative_cpu_time;
  }

  PUGS_INLINE
  bool
  doPrint() const
  {
    return m_do_print;
  }

  PUGS_INLINE
  void
  setPrint(bool do_print)
  {
    m_do_print = do_print;
  }

  PUGS_INLINE
  size_t
  runNumber() const
  {
    return m_run_number;
  }

  PUGS_INLINE
  void
  setRunNumber(size_t run_number)
  {
    m_run_number = run_number;
  }

  PUGS_INLINE
  int
  exitCode() const
  {
    return m_exit_code;
  }

  PUGS_INLINE
  void
  setExitCode(int exit_code)
  {
    m_exit_code = exit_code;
  }

  PUGS_INLINE
  static ExecutionStatManager&
  getInstance()
  {
    Assert(m_instance != nullptr);   // LCOV_EXCL_LINE
    return *m_instance;
  }

  static void printInfo(std::ostream& os = std::cout);
  static void create();
  static void destroy();
};

#endif   // EXECUTION_STAT_MANAGER_HPP
