#ifndef RANDOM_ENGINE_HPP
#define RANDOM_ENGINE_HPP

#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>

#include <memory>
#include <random>

class RandomEngine
{
 private:
  static std::unique_ptr<RandomEngine> m_instance;

  std::default_random_engine m_random_engine;

  RandomEngine();

 public:
  void setRandomSeed(const uint64_t random_seed);
  void resetRandomSeed();

  friend bool isSynchronized(const RandomEngine& random_engine);

  static void create();

  uint64_t getCurrentSeed() const;

  PUGS_INLINE
  std::default_random_engine&
  engine()
  {
    return m_random_engine;
  }

  PUGS_INLINE
  static RandomEngine&
  instance()
  {
    Assert(m_instance.get() != nullptr, "undefined random engine instance");
    return *m_instance;
  }

  static void destroy();

  RandomEngine(const RandomEngine&) = delete;
  RandomEngine(RandomEngine&&)      = delete;

  ~RandomEngine() = default;
};

#endif   // RANDOM_ENGINE_HPP
