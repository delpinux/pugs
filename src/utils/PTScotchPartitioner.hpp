#ifndef PT_SCOTCH_PARTITIONER_HPP
#define PT_SCOTCH_PARTITIONER_HPP

#include <utils/Array.hpp>
#include <utils/CRSGraph.hpp>

class PTScotchPartitioner
{
 private:
  friend class Partitioner;

  // Forbids direct calls
  static Array<int> partition(const CRSGraph& graph);

 public:
  PTScotchPartitioner() = delete;
};

#endif   // PT_SCOTCH_PARTITIONER_HPP
