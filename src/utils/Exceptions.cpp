#include <utils/Exceptions.hpp>

#include <rang.hpp>

#include <sstream>
#include <string>

RawError::RawError(std::string_view error_msg) : IExitError(std::string{error_msg}) {}

NormalError::NormalError(std::string_view error_msg)
  : IExitError([&] {
      std::ostringstream os;
      os << rang::fgB::red << "error:" << rang::style::reset << ' ' << error_msg;
      return os.str();
    }())
{}

UnexpectedError::UnexpectedError(std::string_view error_msg, const std::experimental::source_location& source_location)
  : IBacktraceError([&] {
      std::ostringstream os;
      os << rang::fgB::red << "unexpected error:" << rang::style::reset << ' ' << error_msg;
      return os.str();
    }()),
    m_source_location{source_location}
{}

NotImplementedError::NotImplementedError(std::string_view error_msg,
                                         const std::experimental::source_location& source_location)
  : IBacktraceError([&] {
      std::ostringstream os;
      os << rang::fgB::yellow << "not implemented yet:" << rang::style::reset << ' ' << error_msg;
      return os.str();
    }()),
    m_source_location{source_location}
{}
