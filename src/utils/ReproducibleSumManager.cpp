#include <utils/ReproducibleSumManager.hpp>

bool ReproducibleSumManager::s_reproducible_sums = true;

bool
ReproducibleSumManager::reproducibleSums()
{
  return s_reproducible_sums;
}

void
ReproducibleSumManager::setReproducibleSums(bool reproducible_sum)
{
  s_reproducible_sums = reproducible_sum;
}
