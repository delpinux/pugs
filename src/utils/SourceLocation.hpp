#ifndef SOURCE_LOCATION_HPP
#define SOURCE_LOCATION_HPP

#include <experimental/source_location>
#include <string>

class SourceLocation
{
 private:
  std::string m_filename;
  std::string m_function;
  size_t m_line;
  size_t m_column;

 public:
  const std::string&
  filename() const
  {
    return m_filename;
  }

  const std::string&
  function() const
  {
    return m_function;
  }

  size_t
  line() const
  {
    return m_line;
  }

  size_t
  column() const
  {
    return m_column;
  }

  SourceLocation(
    const std::experimental::source_location& source_location = std::experimental::source_location::current())
    : m_filename{source_location.file_name()},
      m_function{source_location.function_name()},
      m_line{source_location.line()},
      m_column{source_location.column()}
  {}

  SourceLocation(std::string filename, size_t line, size_t column, std::string function = "")
    : m_filename{filename}, m_function{function}, m_line{line}, m_column{column}
  {}

  SourceLocation(SourceLocation&&)      = default;
  SourceLocation(const SourceLocation&) = default;

  ~SourceLocation() = default;
};

#endif   // SOURCE_LOCATION_HPP
