#ifndef EXCEPTIONS_HPP
#define EXCEPTIONS_HPP

#include <stdexcept>
#include <string>
#include <string_view>

#include <experimental/source_location>

struct IExitError : public std::runtime_error
{
  IExitError(std::string_view error_msg) : std::runtime_error(std::string{error_msg}){};

  IExitError(const IExitError&) = delete;
  IExitError(IExitError&&)      = delete;

  virtual ~IExitError() = default;
};

struct RawError : public IExitError
{
  RawError(const RawError&) = delete;
  RawError(RawError&&)      = delete;

  RawError(std::string_view error_msg);
};

struct NormalError : public IExitError
{
  NormalError(const NormalError&) = delete;
  NormalError(NormalError&&)      = delete;

  NormalError(std::string_view error_msg);
};

struct IBacktraceError : public std::runtime_error
{
  virtual const std::experimental::source_location& sourceLocation() const = 0;

  IBacktraceError(const IBacktraceError&) = delete;
  IBacktraceError(IBacktraceError&&)      = delete;

  IBacktraceError(const std::string& error_msg) : std::runtime_error(std::string{error_msg}){};
  virtual ~IBacktraceError() = default;
};

class UnexpectedError final : public IBacktraceError
{
 private:
  const std::experimental::source_location m_source_location;

 public:
  const std::experimental::source_location&
  sourceLocation() const
  {
    return m_source_location;
  }

  UnexpectedError(const UnexpectedError&) = delete;
  UnexpectedError(UnexpectedError&&)      = delete;

  UnexpectedError(std::string_view error_msg,
                  const std::experimental::source_location& = std::experimental::source_location::current());
};

class NotImplementedError final : public IBacktraceError
{
 private:
  const std::experimental::source_location m_source_location;

 public:
  const std::experimental::source_location&
  sourceLocation() const
  {
    return m_source_location;
  }

  NotImplementedError(const NotImplementedError&) = delete;
  NotImplementedError(NotImplementedError&&)      = delete;

  NotImplementedError(std::string_view error_msg,
                      const std::experimental::source_location& = std::experimental::source_location::current());
};

#endif   // EXCEPTIONS_HPP
