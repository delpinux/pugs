#ifndef PLUGINS_LOADER_HPP
#define PLUGINS_LOADER_HPP

#include <stack>
#include <string>

class PluginsLoader
{
 private:
  std::stack<void*> m_dl_handler_stack;

  void _open(const std::string& filename);

 public:
  PluginsLoader();

  PluginsLoader(const PluginsLoader&) = delete;
  PluginsLoader(PluginsLoader&&)      = delete;

  ~PluginsLoader();
};

#endif   // PLUGINS_LOADER_HPP
