#ifndef HIGH_FIVE_PUGS_UTILS_HPP
#define HIGH_FIVE_PUGS_UTILS_HPP

#include <utils/pugs_config.hpp>

#ifdef PUGS_HAS_HDF5
#include <highfive/highfive.hpp>

template <size_t Dimension, typename T>
class TinyVector;
template <size_t M, size_t N, typename T>
class TinyMatrix;

namespace HighFive
{
template <size_t Dimension, typename T>
class AtomicType<TinyVector<Dimension, T>> : public HighFive::DataType
{
 public:
  inline AtomicType()
  {
    hsize_t dim[]     = {Dimension};
    auto h5_data_type = HighFive::create_datatype<T>();
    _hid              = H5Tarray_create(h5_data_type.getId(), 1, dim);
  }
};

template <size_t M, size_t N, typename T>
class AtomicType<TinyMatrix<M, N, T>> : public HighFive::DataType
{
 public:
  inline AtomicType()
  {
    hsize_t dim[]     = {M, N};
    auto h5_data_type = HighFive::create_datatype<T>();
    _hid              = H5Tarray_create(h5_data_type.getId(), 2, dim);
  }
};

}   // namespace HighFive

#else

namespace HighFive
{
class File;
class Group;
}   // namespace HighFive

#endif   // PUGS_HAS_HDF5

#endif   // HIGH_FIVE_PUGS_UTILS_HPP
