#include <utils/RevisionInfo.hpp>

#include <utils/pugs_git_revision.hpp>
#include <utils/pugs_version.hpp>

std::string
RevisionInfo::version()
{
  return PUGS_VERSION;
}

bool
RevisionInfo::hasGitInfo()
{
#ifdef HAS_PUGS_GIT_INFO
  return true;
#else
  return false;
#endif
}

std::string
RevisionInfo::gitTag()
{
#ifdef HAS_PUGS_GIT_INFO
  return PUGS_GIT_TAG;
#else
  return "unknown tag";
#endif
}

std::string
RevisionInfo::gitHead()
{
#ifdef HAS_PUGS_GIT_INFO
  return PUGS_GIT_HEAD;
#else
  return "unknown head";
#endif
}

std::string
RevisionInfo::gitHash()
{
#ifdef HAS_PUGS_GIT_INFO
  return PUGS_GIT_HASH;
#else
  return "unknown hash";
#endif
}

bool
RevisionInfo::gitIsClean()
{
#ifdef HAS_PUGS_GIT_INFO
  return PUGS_GIT_IS_CLEAN;
#else
  return false;
#endif
}
