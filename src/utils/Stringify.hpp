#ifndef STRINGIFY_HPP
#define STRINGIFY_HPP

#include <sstream>
#include <string>

// This utility is used in replacement of std::to_string. It allows a
// better coherence of outputs especially in the case of double values
template <typename T>
std::string
stringify(const T& value)
{
  std::ostringstream os;
  os << std::boolalpha << value;
  return os.str();
}

#endif   // STRINGIFY_HPP
