#ifndef SIGNAL_MANAGER_HPP
#define SIGNAL_MANAGER_HPP

#include <iostream>

struct SignalManager
{
 private:
  static bool s_pause_on_error;
  static std::string signalName(int signal);
  static void pauseForDebug(int signal);
  static void handler(int signal);

 public:
  static bool pauseOnError();
  static void setPauseForDebug(bool pause_on_error);
  static void init(bool enable);
};

#endif   // SIGNAL_MANAGER_HPP
