#ifndef PUGS_UTILS_HPP
#define PUGS_UTILS_HPP

#include <Kokkos_Core.hpp>
#include <utils/PugsMacros.hpp>

#include <string>

template <typename FunctionType>
PUGS_FORCEINLINE void
parallel_for(size_t size, const FunctionType& lambda)
{
  Kokkos::parallel_for(size, lambda);
}

template <typename FunctorType, typename ReturnType>
PUGS_FORCEINLINE void
parallel_reduce(size_t size, const FunctorType& functor, ReturnType& value)
{
  Kokkos::parallel_reduce(size, functor, value);
}

void setDefaultOMPEnvironment();

std::string pugsBuildInfo();

std::string pugsVersion();

std::string initialize(int& argc, char* argv[]);

void finalize();

#endif   // PUGS_UTILS_HPP
