#ifndef PUGS_TRAITS_HPP
#define PUGS_TRAITS_HPP

#include <cstddef>
#include <memory>
#include <tuple>
#include <type_traits>
#include <variant>
#include <vector>

#include <mesh/ItemType.hpp>

template <size_t N, typename T>
class TinyVector;
template <size_t M, size_t N, typename T>
class TinyMatrix;

template <typename DataType>
class SmallMatrix;

template <typename DataType, typename IndexType>
class CRSMatrix;

template <typename DataType, ItemType item_type, typename ConnectivityPtr>
class ItemValue;
template <typename DataType, ItemType item_type, typename ConnectivityPtr>
class ItemArray;

template <typename DataType>
class DiscreteFunctionP0;

template <typename DataType>
class DiscreteFunctionP0Vector;

// Traits is_trivially_castable

template <typename T>
inline constexpr bool is_trivially_castable = std::is_trivial_v<T>;

template <size_t N, typename T>
inline constexpr bool is_trivially_castable<TinyVector<N, T>> = is_trivially_castable<T>;
template <size_t N, typename T>
inline constexpr bool is_trivially_castable<const TinyVector<N, T>> = is_trivially_castable<T>;

template <size_t M, size_t N, typename T>
inline constexpr bool is_trivially_castable<TinyMatrix<M, N, T>> = is_trivially_castable<T>;
template <size_t M, size_t N, typename T>
inline constexpr bool is_trivially_castable<const TinyMatrix<M, N, T>> = is_trivially_castable<T>;

// Traits is_false

template <typename T>
inline constexpr bool is_false_v = false;

// Traits is_shared_ptr

template <typename T>
inline constexpr bool is_shared_ptr_v = false;

template <typename T>
inline constexpr bool is_shared_ptr_v<std::shared_ptr<T>> = true;

// Traits is_unique_ptr

template <typename T>
inline constexpr bool is_unique_ptr_v = false;

template <typename T>
inline constexpr bool is_unique_ptr_v<std::unique_ptr<T>> = true;

// Traits is_weak_ptr

template <typename T>
inline constexpr bool is_weak_ptr_v = false;

template <typename T>
inline constexpr bool is_weak_ptr_v<std::weak_ptr<T>> = true;

// Traits is_std_ptr

template <typename T>
inline constexpr bool is_std_ptr_v = false;

template <typename T>
inline constexpr bool is_std_ptr_v<std::shared_ptr<T>> = true;
template <typename T>
inline constexpr bool is_std_ptr_v<std::unique_ptr<T>> = true;
template <typename T>
inline constexpr bool is_std_ptr_v<std::weak_ptr<T>> = true;

// Traits is_std_vector

template <typename T>
inline constexpr bool is_std_vector_v = false;

template <typename T>
inline constexpr bool is_std_vector_v<std::vector<T>> = true;

// Traits is_std_tuple

template <typename... T>
inline constexpr bool is_std_tuple_v = false;

template <typename... T>
inline constexpr bool is_std_tuple_v<std::tuple<T...>> = true;

// Traits is_tiny_vector

template <typename T>
inline constexpr bool is_tiny_vector_v = false;

template <size_t N, typename T>
inline constexpr bool is_tiny_vector_v<TinyVector<N, T>> = true;

// Traits is_tiny_matrix

template <typename T>
inline constexpr bool is_tiny_matrix_v = false;

template <size_t M, size_t N, typename T>
inline constexpr bool is_tiny_matrix_v<TinyMatrix<M, N, T>> = true;

// Traits is_small_matrix

template <typename T>
inline constexpr bool is_small_matrix_v = false;

template <typename DataType>
inline constexpr bool is_small_matrix_v<SmallMatrix<DataType>> = true;

// Traits is_crs_matrix

template <typename T>
inline constexpr bool is_crs_matrix_v = false;

template <typename DataType, typename IndexType>
inline constexpr bool is_crs_matrix_v<CRSMatrix<DataType, IndexType>> = true;

// Trais is ItemValue

template <typename T>
constexpr inline bool is_item_value_v = false;

template <typename DataType, ItemType item_type, typename ConnectivityPtr>
constexpr inline bool is_item_value_v<ItemValue<DataType, item_type, ConnectivityPtr>> = true;

// Trais is ItemArray

template <typename T>
constexpr inline bool is_item_array_v = false;

template <typename DataType, ItemType item_type, typename ConnectivityPtr>
constexpr inline bool is_item_array_v<ItemArray<DataType, item_type, ConnectivityPtr>> = true;

// Trais is DiscreteFunctionP0

template <typename T>
constexpr inline bool is_discrete_function_P0_v = false;

template <typename DataType>
constexpr inline bool is_discrete_function_P0_v<DiscreteFunctionP0<DataType>> = true;

// Trais is DiscreteFunctionP0Vector

template <typename T>
constexpr inline bool is_discrete_function_P0_vector_v = false;

template <typename DataType>
constexpr inline bool is_discrete_function_P0_vector_v<DiscreteFunctionP0Vector<DataType>> = true;

// Trais is DiscreteFunction

template <typename T>
constexpr inline bool is_discrete_function_v = is_discrete_function_P0_v<T> or is_discrete_function_P0_vector_v<T>;

// helper to check if a type is part of a variant

template <typename T, typename V>
struct is_variant;

template <typename T, typename... ALL_T>
struct is_variant<T, std::variant<ALL_T...>> : public std::disjunction<std::is_same<T, ALL_T>...>
{
};

#endif   // PUGS_TRAITS_HPP
