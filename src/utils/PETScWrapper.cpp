#include <utils/PETScWrapper.hpp>

#include <utils/Messenger.hpp>
#include <utils/pugs_config.hpp>

#ifdef PUGS_HAS_PETSC
#include <petsc.h>
#endif   // PUGS_HAS_PETSC

namespace PETScWrapper
{
void
initialize([[maybe_unused]] int& argc, [[maybe_unused]] char* argv[])
{
#ifdef PUGS_HAS_PETSC
  PETSC_COMM_WORLD = parallel::Messenger::getInstance().comm();
  PetscOptionsSetValue(NULL, "-no_signal_handler", "true");
  PetscOptionsSetValue(NULL, "-fp_trap", "false");
  PetscOptionsSetValue(NULL, "-options_left", "false");
  PetscInitialize(&argc, &argv, 0, 0);
#endif   // PUGS_HAS_PETSC
}

void
finalize()
{
#ifdef PUGS_HAS_PETSC
  PetscFinalize();
#endif   // PUGS_HAS_PETSC
}
}   // namespace PETScWrapper
