#ifndef CONSOLE_MANAGER_HPP
#define CONSOLE_MANAGER_HPP

#include <ostream>

class ConsoleManager
{
 private:
  static bool s_show_preamble;

 public:
  static bool showPreamble();
  static void setShowPreamble(bool show_preamble);
  static bool isTerminal(std::ostream& os);
  static void init(bool colorize);
};

#endif   // CONSOLE_MANAGER_HPP
