#ifndef PARTITIONER_HPP
#define PARTITIONER_HPP

#include <utils/CRSGraph.hpp>
#include <utils/PartitionerOptions.hpp>

class Partitioner
{
 private:
  PartitionerOptions m_partitioner_options;

 public:
  Partitioner(const PartitionerOptions& options = PartitionerOptions::default_options);
  Partitioner(const Partitioner&) = default;
  ~Partitioner()                  = default;

  Array<int> partition(const CRSGraph& graph);
};

#endif   // PARTITIONER_HPP
