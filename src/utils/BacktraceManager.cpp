#include <utils/BacktraceManager.hpp>

#include <utils/Demangle.hpp>

#include <cmath>
#include <execinfo.h>
#include <iomanip>
#include <rang.hpp>
#include <regex>

bool BacktraceManager::s_show = false;

void
BacktraceManager::setShow(bool show_backtrace)
{
  s_show = show_backtrace;
}

BacktraceManager::BacktraceManager()
{
  const int size = 100;
  void* buffer[size];

  int ret    = ::backtrace(buffer, size);
  char** ptr = backtrace_symbols(buffer, ret);

  for (int i = 2; i < ret; ++i) {
    m_stack_lines.push_back(ptr[i]);
  }

  free(ptr);
}

std::ostream&
operator<<(std::ostream& os, const BacktraceManager& btm)
{
  if (BacktraceManager::s_show) {
    const std::vector<std::string>& stack_lines = btm.m_stack_lines;

    const std::regex mangled_function(R"%(\(.*\+)%");
    const int width = std::log10(stack_lines.size()) + 1;

    for (size_t i_stack_line = 0; i_stack_line < stack_lines.size(); ++i_stack_line) {
      const size_t i_line    = stack_lines.size() - i_stack_line - 1;
      const auto& stack_line = stack_lines[i_line];
      os << rang::fg::green << "[" << std::setw(width) << i_line + 1 << '/' << stack_lines.size() << "] "
         << rang::fg::reset;
      std::smatch matchex;
      if (std::regex_search(stack_line, matchex, mangled_function)) {
        std::string prefix   = matchex.prefix().str();
        std::string function = stack_line.substr(matchex.position() + 1, matchex.length() - 2);
        std::string suffix   = matchex.suffix().str();

        os << prefix << '\n';
        os << std::setw(5 + 2 * width) << "from " << '(';
        if (function.size() > 0) {
          std::string function_full_name = demangle(function);
          if (function_full_name.size() > 80) {
            function_full_name.resize(75);
            function_full_name += "[...]";
          }
          os << rang::style::bold << function_full_name << rang::style::reset;
        }
        os << '+' << suffix << '\n';
      } else {
        os << stack_line << '\n';
      }
    }
  } else {
    os << rang::fg::yellow << "\n[To display backtrace launch pugs with the --backtrace option]" << rang::style::reset
       << '\n';
  }

  return os;
}
