#include <utils/checkpointing/PrintCheckpointInfo.hpp>

#include <utils/Exceptions.hpp>
#include <utils/Messenger.hpp>
#include <utils/pugs_config.hpp>

#include <rang.hpp>

#ifdef PUGS_HAS_HDF5

#include <utils/HighFivePugsUtils.hpp>

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>

#include <iomanip>
#include <regex>

#endif   // PUGS_HAS_HDF5

#ifdef PUGS_HAS_HDF5

template <typename T>
void
printAttributeValue(const HighFive::Attribute& attribute, std::ostream& os)
{
  std::string delim = "";

  if constexpr (std::is_same_v<T, std::string>) {
    delim = "\"";
  }

  HighFive::DataSpace data_space = attribute.getSpace();
  if (data_space.getNumberDimensions() == 0) {
    os << std::boolalpha << delim << attribute.read<T>() << delim;
  } else if (data_space.getNumberDimensions() == 1) {
    std::vector value = attribute.read<std::vector<T>>();
    if (value.size() > 0) {
      os << '(' << std::boolalpha << delim << value[0] << delim;
      for (size_t i = 1; i < value.size(); ++i) {
        os << ", " << std::boolalpha << delim << value[i] << delim;
      }
      os << ')';
    }
  }
}

void
printCheckpointInfo(const std::string& filename, std::ostream& os)
{
  if (parallel::rank() == 0) {
    try {
      HighFive::SilenceHDF5 m_silence_hdf5{true};
      HighFive::File file(filename, HighFive::File::ReadOnly);

      std::map<size_t, std::string> checkpoint_name_list;

      for (auto name : file.listObjectNames()) {
        std::smatch number_string;
        const std::regex checkpoint_regex("checkpoint_([0-9]+)");
        if (std::regex_match(name, number_string, checkpoint_regex)) {
          std::stringstream ss;
          ss << number_string[1].str();

          size_t id = 0;
          ss >> id;

          checkpoint_name_list[id] = name;
        }
      }

      for (auto&& [id, checkpoint_name] : checkpoint_name_list) {
        HighFive::Group checkpoint      = file.getGroup(checkpoint_name);
        const std::string creation_date = checkpoint.getAttribute("creation_date").read<std::string>();

        os << rang::fgB::yellow << " * " << rang::fg::reset << rang::fgB::magenta << checkpoint_name << rang::fg::reset
           << " [" << rang::fg::green << creation_date << rang::fg::reset << "]\n";

        HighFive::Group saved_symbol_table = checkpoint.getGroup("symbol table");

        bool finished = true;
        do {
          finished = true;

          for (auto symbol_name : saved_symbol_table.listAttributeNames()) {
            HighFive::Attribute attribute = saved_symbol_table.getAttribute(symbol_name);
            HighFive::DataType data_type  = attribute.getDataType();

            os << "   ";
            os << std::setw(25) << std::setfill('.') << std::left << symbol_name + ' ' << std::setfill(' ');
            os << ' ';

            switch (data_type.getClass()) {
            case HighFive::DataTypeClass::Float: {
              printAttributeValue<double>(attribute, os);
              break;
            }
            case HighFive::DataTypeClass::Integer: {
              if (data_type == HighFive::AtomicType<uint64_t>()) {
                printAttributeValue<uint64_t>(attribute, os);
              } else if (data_type == HighFive::AtomicType<int64_t>()) {
                printAttributeValue<int64_t>(attribute, os);
              }
              break;
            }
            case HighFive::DataTypeClass::Array: {
              HighFive::DataSpace data_space = attribute.getSpace();

              if (data_type == HighFive::AtomicType<TinyVector<1>>()) {
                printAttributeValue<TinyVector<1>>(attribute, os);
              } else if (data_type == HighFive::AtomicType<TinyVector<2>>()) {
                printAttributeValue<TinyVector<2>>(attribute, os);
              } else if (data_type == HighFive::AtomicType<TinyVector<3>>()) {
                printAttributeValue<TinyVector<3>>(attribute, os);
              } else if (data_type == HighFive::AtomicType<TinyMatrix<1>>()) {
                printAttributeValue<TinyMatrix<1>>(attribute, os);
              } else if (data_type == HighFive::AtomicType<TinyMatrix<2>>()) {
                printAttributeValue<TinyMatrix<2>>(attribute, os);
              } else if (data_type == HighFive::AtomicType<TinyMatrix<3>>()) {
                printAttributeValue<TinyMatrix<3>>(attribute, os);
              }
              break;
            }
            case HighFive::DataTypeClass::Enum: {
              if (data_type == HighFive::create_datatype<bool>()) {
                printAttributeValue<bool>(attribute, os);
              } else {
                os << "????";   // LCOV_EXCL_LINE
              }
              break;
            }
            case HighFive::DataTypeClass::String: {
              printAttributeValue<std::string>(attribute, os);
              break;
            }
              // LCOV_EXCL_START
            default: {
              std::ostringstream error_msg;
              error_msg << "invalid data type class '" << rang::fgB::yellow << data_type.string() << rang::fg::reset
                        << "' for symbol " << rang::fgB::cyan << symbol_name << rang::fg::reset;
              throw UnexpectedError(error_msg.str());
            }
              // LCOV_EXCL_STOP
            }

            os << rang::style::reset << '\n';
          }

          if (saved_symbol_table.exist("embedded")) {
            HighFive::Group embedded_data_list = saved_symbol_table.getGroup("embedded");
            for (auto name : embedded_data_list.listObjectNames()) {
              os << "   ";
              os << std::setw(25) << std::setfill('.') << std::left << name + ' ' << std::setfill(' ');
              os << ' ';
              os << embedded_data_list.getGroup(name).getAttribute("type").read<std::string>() << '\n';
            }
          }

          const bool saved_symbol_table_has_parent = saved_symbol_table.exist("symbol table");

          if (saved_symbol_table_has_parent) {
            saved_symbol_table = saved_symbol_table.getGroup("symbol table");

            finished = false;
          }

        } while (not finished);
      }
      os << "-------------------------------------------------------\n";
      for (auto path : std::array{"resuming_checkpoint", "last_checkpoint"}) {
        os << rang::fgB::yellow << " * " << rang::fg::reset << rang::style::bold << path << rang::style::reset << " -> "
           << rang::fgB::green << file.getGroup(path).getAttribute("name").read<std::string>() << rang::style::reset
           << '\n';
      }
    }
    // LCOV_EXCL_START
    catch (HighFive::Exception& e) {
      std::cerr << rang::fgB::red << "error: " << rang::fg::reset << rang::style::bold << e.what() << rang::style::reset
                << '\n';
    }
    // LCOV_EXCL_STOP
  }
}

#else   // PUGS_HAS_HDF5

void
printCheckpointInfo(const std::string&, std::ostream&)
{
  std::cerr << rang::fgB::red << "error: " << rang::fg::reset << "checkpoint info requires HDF5\n";
}

#endif   // PUGS_HAS_HDF5
