#include <utils/checkpointing/ReadSubItemArrayPerItemVariant.hpp>

#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/SubItemArrayPerItemVariant.hpp>
#include <utils/checkpointing/ItemTypeHFType.hpp>
#include <utils/checkpointing/ReadTable.hpp>
#include <utils/checkpointing/ResumingData.hpp>

namespace checkpointing
{

template <typename T, ItemType item_type, ItemType sub_item_type>
SubItemArrayPerItem<T, ItemOfItemType<sub_item_type, item_type>>
readSubItemArrayPerItem(const HighFive::Group& group, const std::string& name, const IConnectivity& connectivity)
{
  return {connectivity, readTable<T>(group, name)};
}

template <ItemType item_type, ItemType sub_item_type>
std::shared_ptr<SubItemArrayPerItemVariant>
readSubItemArrayPerItemVariant(const HighFive::Group& sub_item_array_per_item_variant_group)
{
  if constexpr (item_type != sub_item_type) {
    const std::string data_type  = sub_item_array_per_item_variant_group.getAttribute("data_type").read<std::string>();
    const size_t connectivity_id = sub_item_array_per_item_variant_group.getAttribute("connectivity_id").read<size_t>();

    const IConnectivity& connectivity = *ResumingData::instance().iConnectivity(connectivity_id);

    std::shared_ptr<SubItemArrayPerItemVariant> p_sub_item_array_per_item_variant;

    if (data_type == dataTypeName(ast_node_data_type_from<bool>)) {
      p_sub_item_array_per_item_variant = std::make_shared<SubItemArrayPerItemVariant>(
        readSubItemArrayPerItem<bool, item_type, sub_item_type>(sub_item_array_per_item_variant_group, "arrays",
                                                                connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<long int>)) {
      p_sub_item_array_per_item_variant = std::make_shared<SubItemArrayPerItemVariant>(
        readSubItemArrayPerItem<long int, item_type, sub_item_type>(sub_item_array_per_item_variant_group, "arrays",
                                                                    connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<unsigned long int>)) {
      p_sub_item_array_per_item_variant = std::make_shared<SubItemArrayPerItemVariant>(
        readSubItemArrayPerItem<unsigned long int, item_type, sub_item_type>(sub_item_array_per_item_variant_group,
                                                                             "arrays", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<double>)) {
      p_sub_item_array_per_item_variant = std::make_shared<SubItemArrayPerItemVariant>(
        readSubItemArrayPerItem<double, item_type, sub_item_type>(sub_item_array_per_item_variant_group, "arrays",
                                                                  connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<1>>)) {
      p_sub_item_array_per_item_variant = std::make_shared<SubItemArrayPerItemVariant>(
        readSubItemArrayPerItem<TinyVector<1>, item_type, sub_item_type>(sub_item_array_per_item_variant_group,
                                                                         "arrays", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<2>>)) {
      p_sub_item_array_per_item_variant = std::make_shared<SubItemArrayPerItemVariant>(
        readSubItemArrayPerItem<TinyVector<2>, item_type, sub_item_type>(sub_item_array_per_item_variant_group,
                                                                         "arrays", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<3>>)) {
      p_sub_item_array_per_item_variant = std::make_shared<SubItemArrayPerItemVariant>(
        readSubItemArrayPerItem<TinyVector<3>, item_type, sub_item_type>(sub_item_array_per_item_variant_group,
                                                                         "arrays", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<1>>)) {
      p_sub_item_array_per_item_variant = std::make_shared<SubItemArrayPerItemVariant>(
        readSubItemArrayPerItem<TinyMatrix<1>, item_type, sub_item_type>(sub_item_array_per_item_variant_group,
                                                                         "arrays", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<2>>)) {
      p_sub_item_array_per_item_variant = std::make_shared<SubItemArrayPerItemVariant>(
        readSubItemArrayPerItem<TinyMatrix<2>, item_type, sub_item_type>(sub_item_array_per_item_variant_group,
                                                                         "arrays", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<3>>)) {
      p_sub_item_array_per_item_variant = std::make_shared<SubItemArrayPerItemVariant>(
        readSubItemArrayPerItem<TinyMatrix<3>, item_type, sub_item_type>(sub_item_array_per_item_variant_group,
                                                                         "arrays", connectivity));
    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("unexpected discrete function data type: " + data_type);
      // LCOV_EXCL_STOP
    }
    return p_sub_item_array_per_item_variant;
  } else {
    // LCOV_EXCL_START
    throw UnexpectedError("item_type and sub_item_type must be different");
    // LCOV_EXCL_STOP
  }
}

template <ItemType item_type>
std::shared_ptr<SubItemArrayPerItemVariant>
readSubItemArrayPerItemVariant(const HighFive::Group& sub_item_array_per_item_variant_group)
{
  const ItemType sub_item_type = sub_item_array_per_item_variant_group.getAttribute("sub_item_type").read<ItemType>();

  std::shared_ptr<SubItemArrayPerItemVariant> p_sub_item_array_per_item_variant;

  switch (sub_item_type) {
  case ItemType::cell: {
    p_sub_item_array_per_item_variant =
      readSubItemArrayPerItemVariant<item_type, ItemType::cell>(sub_item_array_per_item_variant_group);
    break;
  }
  case ItemType::face: {
    p_sub_item_array_per_item_variant =
      readSubItemArrayPerItemVariant<item_type, ItemType::face>(sub_item_array_per_item_variant_group);
    break;
  }
  case ItemType::edge: {
    p_sub_item_array_per_item_variant =
      readSubItemArrayPerItemVariant<item_type, ItemType::edge>(sub_item_array_per_item_variant_group);
    break;
  }
  case ItemType::node: {
    p_sub_item_array_per_item_variant =
      readSubItemArrayPerItemVariant<item_type, ItemType::node>(sub_item_array_per_item_variant_group);
    break;
  }
  }

  return p_sub_item_array_per_item_variant;
}

std::shared_ptr<SubItemArrayPerItemVariant>
readSubItemArrayPerItemVariant(const HighFive::Group& sub_item_array_per_item_variant_group)
{
  const ItemType item_type = sub_item_array_per_item_variant_group.getAttribute("item_type").read<ItemType>();

  std::shared_ptr<SubItemArrayPerItemVariant> p_sub_item_array_per_item_variant;

  switch (item_type) {
  case ItemType::cell: {
    p_sub_item_array_per_item_variant =
      readSubItemArrayPerItemVariant<ItemType::cell>(sub_item_array_per_item_variant_group);
    break;
  }
  case ItemType::face: {
    p_sub_item_array_per_item_variant =
      readSubItemArrayPerItemVariant<ItemType::face>(sub_item_array_per_item_variant_group);
    break;
  }
  case ItemType::edge: {
    p_sub_item_array_per_item_variant =
      readSubItemArrayPerItemVariant<ItemType::edge>(sub_item_array_per_item_variant_group);
    break;
  }
  case ItemType::node: {
    p_sub_item_array_per_item_variant =
      readSubItemArrayPerItemVariant<ItemType::node>(sub_item_array_per_item_variant_group);
    break;
  }
  }

  return p_sub_item_array_per_item_variant;
}

EmbeddedData
readSubItemArrayPerItemVariant(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group sub_item_array_per_item_variant_group = symbol_table_group.getGroup("embedded/" + symbol_name);
  return {std::make_shared<DataHandler<const SubItemArrayPerItemVariant>>(
    readSubItemArrayPerItemVariant(sub_item_array_per_item_variant_group))};
}

}   // namespace checkpointing
