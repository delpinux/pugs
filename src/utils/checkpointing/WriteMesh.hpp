#ifndef WRITE_MESH_HPP
#define WRITE_MESH_HPP

#include <language/utils/EmbeddedData.hpp>
#include <utils/HighFivePugsUtils.hpp>

class MeshVariant;

namespace checkpointing
{

void writeMesh(std::shared_ptr<const MeshVariant> mesh_v, HighFive::File& file, HighFive::Group& checkpoint_group);

void writeMesh(const std::string& symbol_name,
               const EmbeddedData& embedded_data,
               HighFive::File& file,
               HighFive::Group& checkpoint_group,
               HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // WRITE_MESH_HPP
