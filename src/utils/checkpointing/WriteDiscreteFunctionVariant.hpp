#ifndef WRITE_DISCRETE_FUNCTION_VARIANT_HPP
#define WRITE_DISCRETE_FUNCTION_VARIANT_HPP

#include <utils/HighFivePugsUtils.hpp>

class DiscreteFunctionVariant;
class EmbeddedData;

namespace checkpointing
{

void writeDiscreteFunctionVariant(HighFive::Group& variable_group,
                                  std::shared_ptr<const DiscreteFunctionVariant> discrete_function_v,
                                  HighFive::File& file,
                                  HighFive::Group& checkpoint_group);

void writeDiscreteFunctionVariant(const std::string& symbol_name,
                                  const EmbeddedData& embedded_data,
                                  HighFive::File& file,
                                  HighFive::Group& checkpoint_group,
                                  HighFive::Group& symbol_table_group);
}   // namespace checkpointing

#endif   // WRITE_DISCRETE_FUNCTION_VARIANT_HPP
