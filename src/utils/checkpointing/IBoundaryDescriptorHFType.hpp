#ifndef I_BOUNDARY_DESCRIPTOR_HF_TYPE_HPP
#define I_BOUNDARY_DESCRIPTOR_HF_TYPE_HPP

#include <mesh/IBoundaryDescriptor.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<IBoundaryDescriptor::Type> PUGS_INLINE
create_enum_i_boundary_descriptor_type()
{
  return {{"named", IBoundaryDescriptor::Type::named}, {"numbered", IBoundaryDescriptor::Type::numbered}};
}
HIGHFIVE_REGISTER_TYPE(IBoundaryDescriptor::Type, create_enum_i_boundary_descriptor_type)

#endif   // I_BOUNDARY_DESCRIPTOR_HF_TYPE_HPP
