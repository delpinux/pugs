#ifndef WRITE_ARRAY_HPP
#define WRITE_ARRAY_HPP

#include <utils/HighFivePugsUtils.hpp>

#include <mesh/CellType.hpp>
#include <mesh/ItemId.hpp>
#include <utils/Array.hpp>
#include <utils/Messenger.hpp>

namespace checkpointing
{

template <typename DataType>
PUGS_INLINE void
write(HighFive::Group& group, const std::string& name, const Array<DataType>& array)
{
  auto get_address = [](auto& x) { return (x.size() > 0) ? &(x[0]) : nullptr; };

  Array<size_t> size_per_rank = parallel::allGather(array.size());
  size_t global_size          = sum(size_per_rank);

  size_t current_offset = 0;
  for (size_t i = 0; i < parallel::rank(); ++i) {
    current_offset += size_per_rank[i];   // LCOV_EXCL_LINE
  }
  std::vector<size_t> offset{current_offset, 0ul};
  std::vector<size_t> count{array.size()};

  using data_type = std::remove_const_t<DataType>;
  HighFive::DataSetCreateProps properties;
  // properties.add(HighFive::Chunking(std::vector<hsize_t>{std::min(4ul * 1024ul * 1024ul, global_size)}));
  // properties.add(HighFive::Shuffle());
  // properties.add(HighFive::Deflate(3));

  auto xfer_props = HighFive::DataTransferProps{};
  xfer_props.add(HighFive::UseCollectiveIO{});

  HighFive::DataSet dataset;
  if constexpr (std::is_same_v<CellType, data_type>) {
    using base_type = std::underlying_type_t<CellType>;
    dataset = group.createDataSet<base_type>(name, HighFive::DataSpace{std::vector<size_t>{global_size}}, properties);
    dataset.select(offset, count)
      .template write_raw<base_type>(reinterpret_cast<const base_type*>(get_address(array)), xfer_props);
  } else if constexpr ((std::is_same_v<CellId, data_type>) or (std::is_same_v<FaceId, data_type>) or
                       (std::is_same_v<EdgeId, data_type>) or (std::is_same_v<NodeId, data_type>)) {
    using base_type = typename data_type::base_type;

    dataset = group.createDataSet<base_type>(name, HighFive::DataSpace{std::vector<size_t>{global_size}}, properties);
    dataset.select(offset, count)
      .template write_raw<base_type>(reinterpret_cast<const base_type*>(get_address(array)), xfer_props);
  } else {
    dataset = group.createDataSet<data_type>(name, HighFive::DataSpace{std::vector<size_t>{global_size}}, properties);
    dataset.select(offset, count).template write_raw<data_type>(get_address(array), xfer_props);
  }

  std::vector<size_t> size_vector;
  for (size_t i = 0; i < size_per_rank.size(); ++i) {
    size_vector.push_back(size_per_rank[i]);
  }

  dataset.createAttribute("size_per_rank", size_vector);
}

}   // namespace checkpointing

#endif   // WRITE_ARRAY_HPP
