#ifndef I_INTERFACE_DESCRIPTOR_HF_TYPE_HPP
#define I_INTERFACE_DESCRIPTOR_HF_TYPE_HPP

#include <mesh/IInterfaceDescriptor.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<IInterfaceDescriptor::Type> PUGS_INLINE
create_enum_i_interface_descriptor_type()
{
  return {{"named", IInterfaceDescriptor::Type::named}, {"numbered", IInterfaceDescriptor::Type::numbered}};
}
HIGHFIVE_REGISTER_TYPE(IInterfaceDescriptor::Type, create_enum_i_interface_descriptor_type)

#endif   // I_INTERFACE_DESCRIPTOR_HF_TYPE_HPP
