#include <utils/checkpointing/WriteVariableBCDescriptor.hpp>

#include <language/modules/SchemeModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>
#include <scheme/VariableBCDescriptor.hpp>
#include <utils/checkpointing/DiscreteFunctionTypeHFType.hpp>
#include <utils/checkpointing/IBoundaryConditionDescriptorHFType.hpp>
#include <utils/checkpointing/WriteDiscreteFunctionVariant.hpp>
#include <utils/checkpointing/WriteIBoundaryConditionDescriptor.hpp>

namespace checkpointing
{

void
writeVariableBCDescriptor(const std::string& symbol_name,
                          const EmbeddedData& embedded_data,
                          HighFive::File& file,
                          HighFive::Group& checkpoint_group,
                          HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const VariableBCDescriptor> variable_bc_descriptor_p =
    dynamic_cast<const DataHandler<const VariableBCDescriptor>&>(embedded_data.get()).data_ptr();

  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(variable_bc_descriptor_p)>));

  HighFive::Group discrete_function_group = variable_group.createGroup("discrete_function");
  writeDiscreteFunctionVariant(discrete_function_group, variable_bc_descriptor_p->discreteFunctionVariant(), file,
                               checkpoint_group);

  const auto bc_descriptor_list            = variable_bc_descriptor_p->bcDescriptorList();
  HighFive::Group bc_descriptor_list_group = variable_group.createGroup("bc_descriptor_list");
  for (size_t i_bc_descriptor = 0; i_bc_descriptor < bc_descriptor_list.size(); ++i_bc_descriptor) {
    HighFive::Group bc_descriptor_group = bc_descriptor_list_group.createGroup(std::to_string(i_bc_descriptor));

    writeIBoundaryConditionDescriptor(bc_descriptor_group, bc_descriptor_list[i_bc_descriptor]);
  }
}

}   // namespace checkpointing
