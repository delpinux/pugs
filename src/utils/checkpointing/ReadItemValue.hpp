#ifndef READ_ITEM_VALUE_HPP
#define READ_ITEM_VALUE_HPP

#include <mesh/ItemValue.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/checkpointing/ReadArray.hpp>

namespace checkpointing
{

template <typename T, ItemType item_type>
ItemValue<T, item_type>
readItemValue(const HighFive::Group& group, const std::string& name, const IConnectivity& connectivity)
{
  return {connectivity, readArray<T>(group, name)};
}

}   // namespace checkpointing

#endif   // READ_ITEM_VALUE_HPP
