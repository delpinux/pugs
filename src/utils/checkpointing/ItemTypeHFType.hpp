#ifndef ITEM_TYPE_HF_TYPE_HPP
#define ITEM_TYPE_HF_TYPE_HPP

#include <mesh/ItemType.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<ItemType> PUGS_INLINE
create_enum_item_type()
{
  return {{"node", ItemType::node}, {"edge", ItemType::edge}, {"face", ItemType::face}, {"cell", ItemType::cell}};
}
HIGHFIVE_REGISTER_TYPE(ItemType, create_enum_item_type)

#endif   // ITEM_TYPE_HF_TYPE_HPP
