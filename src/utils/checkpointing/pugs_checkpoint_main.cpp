#include <CLI/CLI.hpp>
#include <rang.hpp>
#include <utils/ConsoleManager.hpp>
#include <utils/Messenger.hpp>
#include <utils/checkpointing/PrintCheckpointInfo.hpp>
#include <utils/checkpointing/PrintScriptFrom.hpp>
#include <utils/checkpointing/SetResumeFrom.hpp>

int
main(int argc, char* argv[])
{
  std::string filename;

  CLI::App app{"pugs_checkpoint help"};
  app.description("A collection of simple tools to manage checkpoint/resume files for pugs.");
  app.add_option("filename", filename, "pugs checkpoint file (HDF5)")->check(CLI::ExistingFile)->required();

  bool enable_color = true;
  app.add_flag("--color,!--no-color", enable_color, "Colorize console output [default: true]");

  bool print_info = false;
  auto info_flag  = app.add_flag("--info", print_info, "Print checkpoints info");

  uint64_t checkpoint_number = 0;
  auto resume_from =
    app.add_option("--resume-from", checkpoint_number, "Use the given checkpoint number for next resume")
      ->excludes(info_flag);

  auto get_script_resume_from =
    app.add_option("--get-script-from", checkpoint_number, "Print script file used for a given checkpoint number")
      ->excludes(info_flag)
      ->excludes(resume_from);

  std::atexit([]() { std::cout << rang::style::reset; });
  try {
    app.parse(argc, argv);
  }
  catch (const CLI::ParseError& e) {
    std::exit(app.exit(e, std::cout, std::cerr));
  }

  parallel::Messenger::create(argc, argv);
  ConsoleManager::init(enable_color);

  try {
    if (*info_flag) {
      printCheckpointInfo(filename);
    } else if (*resume_from) {
      setResumeFrom(filename, checkpoint_number);
    } else if (*get_script_resume_from) {
      printScriptFrom(filename, checkpoint_number);
    }
  }
  catch (const std::runtime_error& e) {
    std::cerr << e.what() << '\n';
    parallel::Messenger::destroy();
    std::exit(1);
  }

  parallel::Messenger::destroy();

  return 0;
}
