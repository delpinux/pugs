#include <utils/checkpointing/ReadOStream.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <language/utils/OFStream.hpp>
#include <utils/Exceptions.hpp>
#include <utils/checkpointing/OStreamTypeHFType.hpp>

namespace checkpointing
{

EmbeddedData
readOStream(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group ostream_group = symbol_table_group.getGroup("embedded/" + symbol_name);

  const OStream::Type ostream_type = ostream_group.getAttribute("ostream_type").read<OStream::Type>();

  std::shared_ptr<const OStream> p_ostream;

  switch (ostream_type) {
  case OStream::Type::std_ofstream: {
    std::string filename = ostream_group.getAttribute("filename").read<std::string>();

    p_ostream = std::make_shared<OFStream>(filename, true);
    break;
  }
    // LCOV_EXCL_START
  case OStream::Type::std_ostream: {
    throw NotImplementedError("std::ostream resume");
  }
    // LCOV_EXCL_STOP
  }

  return {std::make_shared<DataHandler<const OStream>>(p_ostream)};
}

}   // namespace checkpointing
