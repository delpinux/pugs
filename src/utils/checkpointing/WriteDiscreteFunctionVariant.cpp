#include <utils/checkpointing/WriteDiscreteFunctionVariant.hpp>

#include <language/modules/SchemeModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>
#include <utils/checkpointing/DiscreteFunctionTypeHFType.hpp>
#include <utils/checkpointing/WriteIDiscreteFunctionDescriptor.hpp>
#include <utils/checkpointing/WriteItemArray.hpp>
#include <utils/checkpointing/WriteItemValue.hpp>
#include <utils/checkpointing/WriteMesh.hpp>

namespace checkpointing
{

void
writeDiscreteFunctionVariant(HighFive::Group& variable_group,
                             std::shared_ptr<const DiscreteFunctionVariant> discrete_function_v,
                             HighFive::File& file,
                             HighFive::Group& checkpoint_group)
{
  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(discrete_function_v)>));

  std::visit(
    [&](auto&& discrete_function) {
      auto mesh_v  = discrete_function.meshVariant();
      using DFType = std::decay_t<decltype(discrete_function)>;
      variable_group.createAttribute("Vh_type", discrete_function.descriptor().type());

      variable_group.createAttribute("mesh_id", mesh_v->id());
      writeMesh(mesh_v, file, checkpoint_group);
      if constexpr (is_discrete_function_P0_v<DFType>) {
        using data_type = std::decay_t<typename DFType::data_type>;
        variable_group.createAttribute("data_type", dataTypeName(ast_node_data_type_from<data_type>));
        write(variable_group, "values", discrete_function.cellValues());
      } else if constexpr (is_discrete_function_P0_vector_v<DFType>) {
        using data_type = std::decay_t<typename DFType::data_type>;
        variable_group.createAttribute("data_type", dataTypeName(ast_node_data_type_from<data_type>));
        write(variable_group, "values", discrete_function.cellArrays());
      }
    },
    discrete_function_v->discreteFunction());
}

void
writeDiscreteFunctionVariant(const std::string& symbol_name,
                             const EmbeddedData& embedded_data,
                             HighFive::File& file,
                             HighFive::Group& checkpoint_group,
                             HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const DiscreteFunctionVariant> discrete_function_p =
    dynamic_cast<const DataHandler<const DiscreteFunctionVariant>&>(embedded_data.get()).data_ptr();

  writeDiscreteFunctionVariant(variable_group, discrete_function_p, file, checkpoint_group);
}

}   // namespace checkpointing
