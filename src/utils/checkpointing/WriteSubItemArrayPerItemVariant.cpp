#include <utils/checkpointing/WriteSubItemArrayPerItemVariant.hpp>

#include <language/modules/MeshModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/SubItemArrayPerItemVariant.hpp>
#include <utils/checkpointing/ItemTypeHFType.hpp>
#include <utils/checkpointing/WriteConnectivity.hpp>
#include <utils/checkpointing/WriteTable.hpp>

namespace checkpointing
{

void
writeSubItemArrayPerItemVariant(HighFive::Group& variable_group,
                                std::shared_ptr<const SubItemArrayPerItemVariant> sub_item_array_per_item_variant_v,
                                HighFive::File& file,
                                HighFive::Group& checkpoint_group)
{
  variable_group.createAttribute("type",
                                 dataTypeName(ast_node_data_type_from<decltype(sub_item_array_per_item_variant_v)>));

  std::visit(
    [&](auto&& sub_item_array_per_item) {
      using SubItemArrayPerItemT = std::decay_t<decltype(sub_item_array_per_item)>;

      variable_group.createAttribute("item_type", SubItemArrayPerItemT::item_type);
      variable_group.createAttribute("sub_item_type", SubItemArrayPerItemT::sub_item_type);
      using data_type = std::decay_t<typename SubItemArrayPerItemT::data_type>;
      variable_group.createAttribute("data_type", dataTypeName(ast_node_data_type_from<data_type>));

      const IConnectivity& connectivity = *sub_item_array_per_item.connectivity_ptr();
      variable_group.createAttribute("connectivity_id", connectivity.id());
      writeConnectivity(connectivity, file, checkpoint_group);

      write(variable_group, "arrays", sub_item_array_per_item.tableView());
    },
    sub_item_array_per_item_variant_v->subItemArrayPerItem());
}

void
writeSubItemArrayPerItemVariant(const std::string& symbol_name,
                                const EmbeddedData& embedded_data,
                                HighFive::File& file,
                                HighFive::Group& checkpoint_group,
                                HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const SubItemArrayPerItemVariant> sub_item_array_per_item_variant_p =
    dynamic_cast<const DataHandler<const SubItemArrayPerItemVariant>&>(embedded_data.get()).data_ptr();

  writeSubItemArrayPerItemVariant(variable_group, sub_item_array_per_item_variant_p, file, checkpoint_group);
}

}   // namespace checkpointing
