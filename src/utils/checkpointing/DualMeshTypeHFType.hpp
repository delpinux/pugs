#ifndef DUAL_MESH_TYPE_HF_TYPE_HPP
#define DUAL_MESH_TYPE_HF_TYPE_HPP

#include <mesh/DualConnectivityManager.hpp>
#include <utils/HighFivePugsUtils.hpp>

HighFive::EnumType<DualMeshType> PUGS_INLINE
create_enum_DualMeshType_type()
{
  return {{"dual1d", DualMeshType::Dual1D}, {"diamond", DualMeshType::Diamond}, {"median", DualMeshType::Median}};
}
HIGHFIVE_REGISTER_TYPE(DualMeshType, create_enum_DualMeshType_type)

#endif   // DUAL_MESH_TYPE_HF_TYPE_HPP
