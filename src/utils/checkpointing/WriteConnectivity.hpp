#ifndef WRITE_CONNECTIVITY_HPP
#define WRITE_CONNECTIVITY_HPP

#include <utils/HighFivePugsUtils.hpp>

class IConnectivity;

namespace checkpointing
{

void writeConnectivity(const IConnectivity& connectivity, HighFive::File& file, HighFive::Group& checkpoint_group);

}   // namespace checkpointing

#endif   // WRITE_CONNECTIVITY_HPP
