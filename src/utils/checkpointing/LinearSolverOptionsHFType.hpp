#ifndef LINEAR_SOLVER_OPTIONS_HF_TYPE_HPP
#define LINEAR_SOLVER_OPTIONS_HF_TYPE_HPP

#include <algebra/LinearSolverOptions.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<LSLibrary> PUGS_INLINE
create_enum_LSOptions_library_type()
{
  return {{"builtin", LSLibrary::builtin}, {"eigen3", LSLibrary::eigen3}, {"petsc", LSLibrary::petsc}};
}
HIGHFIVE_REGISTER_TYPE(LSLibrary, create_enum_LSOptions_library_type)

HighFive::EnumType<LSMethod> PUGS_INLINE
create_enum_LSOptions_method_type()
{
  return {{"cg", LSMethod::cg},
          {"bicgstab", LSMethod::bicgstab},
          {"bicgstab2", LSMethod::bicgstab2},
          {"gmres", LSMethod::gmres},
          {"lu", LSMethod::lu},
          {"cholesky", LSMethod::cholesky}};
}
HIGHFIVE_REGISTER_TYPE(LSMethod, create_enum_LSOptions_method_type)

HighFive::EnumType<LSPrecond> PUGS_INLINE
create_enum_LSOptions_precond_type()
{
  return {{"none", LSPrecond::none},
          {"diagonal", LSPrecond::diagonal},
          {"incomplete_cholesky", LSPrecond::incomplete_cholesky},
          {"incomplete_LU", LSPrecond::incomplete_LU},
          {"amg", LSPrecond::amg}};
}
HIGHFIVE_REGISTER_TYPE(LSPrecond, create_enum_LSOptions_precond_type)

#endif   // LINEAR_SOLVER_OPTIONS_HF_TYPE_HPP
