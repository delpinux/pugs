#ifndef READ_IBOUNDARY_DESCRIPTOR_HPP
#define READ_IBOUNDARY_DESCRIPTOR_HPP

#include <utils/HighFivePugsUtils.hpp>

class IBoundaryDescriptor;
class EmbeddedData;

namespace checkpointing
{

std::shared_ptr<const IBoundaryDescriptor> readIBoundaryDescriptor(const HighFive::Group& iboundarydescriptor_group);

EmbeddedData readIBoundaryDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_IBOUNDARY_DESCRIPTOR_HPP
