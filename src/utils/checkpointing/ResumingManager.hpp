#ifndef RESUMING_MANAGER_HPP
#define RESUMING_MANAGER_HPP

#include <utils/PugsAssert.hpp>

#include <memory>

class ResumingManager
{
 private:
  bool m_is_resuming = false;
  std::string m_filename;
  // numbering of the checkpoints (during execution)
  uint64_t m_checkpoint_number = 0;
  // id of the checkpoint defined in the script file
  std::unique_ptr<uint64_t> m_checkpoint_id;
  size_t m_current_ast_level = 0;

  ResumingManager() = default;

  ResumingManager(ResumingManager&&)      = delete;
  ResumingManager(const ResumingManager&) = delete;

  ~ResumingManager() = default;

  static ResumingManager* m_instance;

 public:
  static void create();
  static void destroy();
  static ResumingManager& getInstance();

  uint64_t&
  checkpointNumber()
  {
    return m_checkpoint_number;
  }

  uint64_t checkpointId();

  uint64_t&
  currentASTLevel()
  {
    return m_current_ast_level;
  }

  void
  setIsResuming(const bool is_resuming)
  {
    m_is_resuming = is_resuming;
  }

  bool
  isResuming() const
  {
    return m_is_resuming;
  }

  void
  setFilename(const std::string& filename)
  {
    m_filename = filename;
  }

  const std::string&
  filename() const
  {
    return m_filename;
  }
};

#endif   // RESUMING_MANAGER_HPP
