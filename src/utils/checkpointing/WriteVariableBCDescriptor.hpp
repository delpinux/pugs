#ifndef WRITE_VARIABLE_BC_DESCRIPTOR_HPP
#define WRITE_VARIABLE_BC_DESCRIPTOR_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

void writeVariableBCDescriptor(const std::string& symbol_name,
                               const EmbeddedData& embedded_data,
                               HighFive::File& file,
                               HighFive::Group& checkpoint_group,
                               HighFive::Group& symbol_table_group);

}

#endif   // WRITE_VARIABLE_BC_DESCRIPTOR_HPP
