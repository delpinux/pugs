#include <utils/checkpointing/WriteSubItemValuePerItemVariant.hpp>

#include <language/modules/MeshModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/SubItemValuePerItemVariant.hpp>
#include <utils/checkpointing/ItemTypeHFType.hpp>
#include <utils/checkpointing/WriteArray.hpp>
#include <utils/checkpointing/WriteConnectivity.hpp>

namespace checkpointing
{

void
writeSubItemValuePerItemVariant(HighFive::Group& variable_group,
                                std::shared_ptr<const SubItemValuePerItemVariant> sub_item_value_per_item_variant_v,
                                HighFive::File& file,
                                HighFive::Group& checkpoint_group)
{
  variable_group.createAttribute("type",
                                 dataTypeName(ast_node_data_type_from<decltype(sub_item_value_per_item_variant_v)>));

  std::visit(
    [&](auto&& sub_item_value_per_item) {
      using SubItemValuePerItemT = std::decay_t<decltype(sub_item_value_per_item)>;

      variable_group.createAttribute("item_type", SubItemValuePerItemT::item_type);
      variable_group.createAttribute("sub_item_type", SubItemValuePerItemT::sub_item_type);
      using data_type = std::decay_t<typename SubItemValuePerItemT::data_type>;
      variable_group.createAttribute("data_type", dataTypeName(ast_node_data_type_from<data_type>));

      const IConnectivity& connectivity = *sub_item_value_per_item.connectivity_ptr();
      variable_group.createAttribute("connectivity_id", connectivity.id());
      writeConnectivity(connectivity, file, checkpoint_group);

      write(variable_group, "values", sub_item_value_per_item.arrayView());
    },
    sub_item_value_per_item_variant_v->subItemValuePerItem());
}

void
writeSubItemValuePerItemVariant(const std::string& symbol_name,
                                const EmbeddedData& embedded_data,
                                HighFive::File& file,
                                HighFive::Group& checkpoint_group,
                                HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const SubItemValuePerItemVariant> sub_item_value_per_item_variant_p =
    dynamic_cast<const DataHandler<const SubItemValuePerItemVariant>&>(embedded_data.get()).data_ptr();

  writeSubItemValuePerItemVariant(variable_group, sub_item_value_per_item_variant_p, file, checkpoint_group);
}

}   // namespace checkpointing
