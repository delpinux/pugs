#include <utils/checkpointing/ReadIDiscreteFunctionDescriptor.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <scheme/DiscreteFunctionDescriptorP0.hpp>
#include <scheme/DiscreteFunctionDescriptorP0Vector.hpp>
#include <utils/checkpointing/DiscreteFunctionTypeHFType.hpp>

namespace checkpointing
{

EmbeddedData
readIDiscreteFunctionDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group idiscrete_function_descriptor_group = symbol_table_group.getGroup("embedded/" + symbol_name);
  const DiscreteFunctionType discrete_function_type =
    idiscrete_function_descriptor_group.getAttribute("discrete_function_type").read<DiscreteFunctionType>();

  std::shared_ptr<const IDiscreteFunctionDescriptor> idiscrete_function_descriptor;

  switch (discrete_function_type) {
  case DiscreteFunctionType::P0: {
    idiscrete_function_descriptor = std::make_shared<const DiscreteFunctionDescriptorP0>();
    break;
  }
  case DiscreteFunctionType::P0Vector: {
    idiscrete_function_descriptor = std::make_shared<const DiscreteFunctionDescriptorP0Vector>();
    break;
  }
  }

  return {std::make_shared<DataHandler<const IDiscreteFunctionDescriptor>>(idiscrete_function_descriptor)};
}

}   // namespace checkpointing
