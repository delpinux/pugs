#include <utils/checkpointing/WriteIZoneDescriptor.hpp>

#include <language/modules/MeshModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/NamedZoneDescriptor.hpp>
#include <mesh/NumberedZoneDescriptor.hpp>
#include <utils/checkpointing/IZoneDescriptorHFType.hpp>

namespace checkpointing
{

void
writeIZoneDescriptor(const std::string& symbol_name,
                     const EmbeddedData& embedded_data,
                     HighFive::File&,
                     HighFive::Group&,
                     HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const IZoneDescriptor> izone_descriptor_p =
    dynamic_cast<const DataHandler<const IZoneDescriptor>&>(embedded_data.get()).data_ptr();

  const IZoneDescriptor& izone_descriptor = *izone_descriptor_p;

  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(izone_descriptor_p)>));
  variable_group.createAttribute("izone_descriptor_type", izone_descriptor.type());

  switch (izone_descriptor.type()) {
  case IZoneDescriptor::Type::named: {
    const NamedZoneDescriptor& named_zone_descriptor = dynamic_cast<const NamedZoneDescriptor&>(izone_descriptor);
    variable_group.createAttribute("name", named_zone_descriptor.name());
    break;
  }
  case IZoneDescriptor::Type::numbered: {
    const NumberedZoneDescriptor& numbered_boundary_descriptor =
      dynamic_cast<const NumberedZoneDescriptor&>(izone_descriptor);
    variable_group.createAttribute("number", numbered_boundary_descriptor.number());
    break;
  }
  }
}

}   // namespace checkpointing
