#ifndef READ_ITEM_VALUE_VARIANT_HPP
#define READ_ITEM_VALUE_VARIANT_HPP

#include <mesh/ItemValueVariant.hpp>
#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

std::shared_ptr<ItemValueVariant> readItemValueVariant(const HighFive::Group& item_value_variant_group);

EmbeddedData readItemValueVariant(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_ITEM_VALUE_VARIANT_HPP
