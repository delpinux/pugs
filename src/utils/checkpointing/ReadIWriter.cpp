#include <utils/checkpointing/ReadIWriter.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <output/GnuplotWriter.hpp>
#include <output/GnuplotWriter1D.hpp>
#include <output/GnuplotWriterRaw.hpp>
#include <output/VTKWriter.hpp>
#include <utils/checkpointing/IWriterHFType.hpp>

namespace checkpointing
{

EmbeddedData
readIWriter(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group iwriter_group = symbol_table_group.getGroup("embedded/" + symbol_name);

  std::shared_ptr<WriterBase> p_writer;

  IWriter::Type type = iwriter_group.getAttribute("iwriter_type").read<IWriter::Type>();

  std::string base_filename = iwriter_group.getAttribute("base_filename").read<std::string>();

  switch (type) {
  case IWriter::Type::gnuplot: {
    p_writer = std::make_shared<GnuplotWriter>(base_filename);
    break;
  }
  case IWriter::Type::gnuplot_1d: {
    p_writer = std::make_shared<GnuplotWriter1D>(base_filename);
    break;
  }
  case IWriter::Type::gnuplot_raw: {
    p_writer = std::make_shared<GnuplotWriterRaw>(base_filename);
    break;
  }
  case IWriter::Type::vtk: {
    p_writer = std::make_shared<VTKWriter>(base_filename);
    break;
  }
  }

  if (iwriter_group.exist("period_manager")) {
    HighFive::Group period_manager_group = iwriter_group.getGroup("period_manager");

    const double time_period = period_manager_group.getAttribute("time_period").read<double>();
    const double next_time   = period_manager_group.getAttribute("next_time").read<double>();
    const std::vector<double> saved_times =
      period_manager_group.getAttribute("saved_times").read<std::vector<double>>();

    p_writer->m_period_manager.emplace(WriterBase::PeriodManager(time_period, next_time, saved_times));
  }

  if (iwriter_group.hasAttribute("signature")) {
    p_writer->m_signature = iwriter_group.getAttribute("signature").read<std::string>();
  }

  return {std::make_shared<DataHandler<const IWriter>>(p_writer)};
}

}   // namespace checkpointing
