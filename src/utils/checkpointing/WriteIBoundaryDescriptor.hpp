#ifndef WRITE_IBOUNDARY_DESCRIPTOR_HPP
#define WRITE_IBOUNDARY_DESCRIPTOR_HPP

#include <language/utils/EmbeddedData.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <utils/HighFivePugsUtils.hpp>

namespace checkpointing
{

void writeIBoundaryDescriptor(HighFive::Group& variable_group, const IBoundaryDescriptor& iboundary_descriptor);

void writeIBoundaryDescriptor(const std::string& symbol_name,
                              const EmbeddedData& embedded_data,
                              HighFive::File&,
                              HighFive::Group&,
                              HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // WRITE_IBOUNDARY_DESCRIPTOR_HPP
