#ifndef READ_ARRAY_HPP
#define READ_ARRAY_HPP

#include <mesh/CellType.hpp>
#include <mesh/ItemId.hpp>
#include <utils/Array.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/Messenger.hpp>

namespace checkpointing
{

template <typename DataType>
PUGS_INLINE Array<DataType>
readArray(const HighFive::Group& group, const std::string& name)
{
  auto get_address = [](auto& x) { return (x.size() > 0) ? &(x[0]) : nullptr; };

  using data_type = std::remove_const_t<DataType>;

  auto dataset = group.getDataSet(name);

  std::vector<size_t> size_per_rank = dataset.getAttribute("size_per_rank").read<std::vector<size_t>>();

  if (size_per_rank.size() != parallel::size()) {
    throw NormalError("cannot change number of processes");   // LCOV_EXCL_LINE
  }

  std::vector<size_t> offset = {0, 0ul};
  for (size_t i = 0; i < parallel::rank(); ++i) {
    offset[0] += size_per_rank[i];   // LCOV_EXCL_LINE
  }
  std::vector<size_t> count = {size_per_rank[parallel::rank()]};

  Array<DataType> array(size_per_rank[parallel::rank()]);
  if constexpr (std::is_same_v<CellType, data_type>) {
    using base_type = std::underlying_type_t<CellType>;
    dataset.select(offset, count).read_raw(reinterpret_cast<base_type*>(get_address(array)));
  } else if constexpr ((std::is_same_v<CellId, data_type>) or (std::is_same_v<FaceId, data_type>) or
                       (std::is_same_v<EdgeId, data_type>) or (std::is_same_v<NodeId, data_type>)) {
    using base_type = typename data_type::base_type;
    dataset.select(offset, count).read_raw(reinterpret_cast<base_type*>(get_address(array)));
  } else {
    dataset.select(offset, count).read_raw(get_address(array));
  }

  return array;
}

}   // namespace checkpointing

#endif   // READ_ARRAY_HPP
