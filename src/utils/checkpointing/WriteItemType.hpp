#ifndef WRITE_ITEM_TYPE_HPP
#define WRITE_ITEM_TYPE_HPP

#include <language/utils/EmbeddedData.hpp>
#include <utils/HighFivePugsUtils.hpp>

namespace checkpointing
{

void writeItemType(const std::string& symbol_name,
                   const EmbeddedData& embedded_data,
                   HighFive::File&,
                   HighFive::Group&,
                   HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // WRITE_ITEM_TYPE_HPP
