#ifndef SET_RESUME_FROM_HPP
#define SET_RESUME_FROM_HPP

#include <cstdint>
#include <iostream>
#include <string>

void setResumeFrom(const std::string& filename, const uint64_t& checkpoint_number, std::ostream& os = std::cout);

#endif   // SET_RESUME_FROM_HPP
