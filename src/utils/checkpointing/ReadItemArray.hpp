#ifndef READ_ITEM_ARRAY_HPP
#define READ_ITEM_ARRAY_HPP

#include <mesh/ItemArray.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/checkpointing/ReadTable.hpp>

namespace checkpointing
{

template <typename T, ItemType item_type>
ItemArray<T, item_type>
readItemArray(const HighFive::Group& group, const std::string& name, const IConnectivity& connectivity)
{
  return {connectivity, readTable<T>(group, name)};
}

}   // namespace checkpointing

#endif   // READ_ITEM_ARRAY_HPP
