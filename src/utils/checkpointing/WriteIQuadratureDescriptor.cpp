#include <utils/checkpointing/WriteIQuadratureDescriptor.hpp>

#include <analysis/IQuadratureDescriptor.hpp>
#include <language/modules/SchemeModuleTypes.hpp>
#include <language/utils/ASTNodeDataType.hpp>
#include <language/utils/DataHandler.hpp>
#include <utils/checkpointing/QuadratureTypeHFType.hpp>

namespace checkpointing
{
void
writeIQuadratureDescriptor(const std::string& symbol_name,
                           const EmbeddedData& embedded_data,
                           HighFive::File&,
                           HighFive::Group&,
                           HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const IQuadratureDescriptor> iquadrature_descriptor_p =
    dynamic_cast<const DataHandler<const IQuadratureDescriptor>&>(embedded_data.get()).data_ptr();

  const IQuadratureDescriptor& iquadrature_descriptor = *iquadrature_descriptor_p;

  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(iquadrature_descriptor_p)>));
  variable_group.createAttribute("quadrature_type", iquadrature_descriptor.type());
  variable_group.createAttribute("quadrature_degree", iquadrature_descriptor.degree());
}

}   // namespace checkpointing
