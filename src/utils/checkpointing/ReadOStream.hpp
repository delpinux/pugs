#ifndef READ_OSTREAM_HPP
#define READ_OSTREAM_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

EmbeddedData readOStream(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_OSTREAM_HPP
