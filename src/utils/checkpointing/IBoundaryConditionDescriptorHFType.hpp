#ifndef I_BOUNDARY_CONDITION_DESCRIPTOR_HF_TYPE_HPP
#define I_BOUNDARY_CONDITION_DESCRIPTOR_HF_TYPE_HPP

#include <scheme/IBoundaryConditionDescriptor.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<IBoundaryConditionDescriptor::Type> PUGS_INLINE
create_enum_i_boundary_condition_descriptor_type()
{
  return {{"axis", IBoundaryConditionDescriptor::Type::axis},
          {"dirichlet", IBoundaryConditionDescriptor::Type::dirichlet},
          {"dirichlet_vector", IBoundaryConditionDescriptor::Type::dirichlet_vector},
          {"external", IBoundaryConditionDescriptor::Type::external},
          {"fixed", IBoundaryConditionDescriptor::Type::fixed},
          {"fourier", IBoundaryConditionDescriptor::Type::fourier},
          {"free", IBoundaryConditionDescriptor::Type::free},
          {"inflow", IBoundaryConditionDescriptor::Type::inflow},
          {"inflow_list", IBoundaryConditionDescriptor::Type::inflow_list},
          {"neumann", IBoundaryConditionDescriptor::Type::neumann},
          {"outflow", IBoundaryConditionDescriptor::Type::outflow},
          {"symmetry", IBoundaryConditionDescriptor::Type::symmetry},
          {"wall", IBoundaryConditionDescriptor::Type::wall}};
}
HIGHFIVE_REGISTER_TYPE(IBoundaryConditionDescriptor::Type, create_enum_i_boundary_condition_descriptor_type)

#endif   // I_BOUNDARY_CONDITION_DESCRIPTOR_HF_TYPE_HPP
