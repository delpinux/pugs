#include <utils/checkpointing/WriteMesh.hpp>

#include <language/modules/MeshModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshVariant.hpp>
#include <utils/checkpointing/WriteConnectivity.hpp>
#include <utils/checkpointing/WriteItemValue.hpp>

namespace checkpointing
{

void
writeMesh(std::shared_ptr<const MeshVariant> mesh_v, HighFive::File& file, HighFive::Group& checkpoint_group)
{
  std::string mesh_group_name = "mesh/" + std::to_string(mesh_v->id());
  if (not checkpoint_group.exist(mesh_group_name)) {
    bool linked = false;
    for (auto group_name : file.listObjectNames()) {
      if (file.exist(group_name + "/" + mesh_group_name)) {
        checkpoint_group.createHardLink(mesh_group_name, file.getGroup(group_name + "/" + mesh_group_name));
        linked = true;
        break;
      }
    }

    if (not linked) {
      HighFive::Group mesh_group = checkpoint_group.createGroup(mesh_group_name);
      mesh_group.createAttribute("connectivity", mesh_v->connectivity().id());
      std::visit(
        [&](auto&& mesh) {
          using MeshType = mesh_type_t<decltype(mesh)>;
          if constexpr (is_polygonal_mesh_v<MeshType>) {
            mesh_group.createAttribute("id", mesh->id());
            mesh_group.createAttribute("type", std::string{"polygonal"});
            mesh_group.createAttribute("dimension", mesh->dimension());
            write(mesh_group, "xr", mesh->xr());
          } else {
            throw UnexpectedError("unexpected mesh type");
          }
        },
        mesh_v->variant());
    }
  }

  std::visit(
    [&](auto&& mesh) {
      using MeshType = mesh_type_t<decltype(mesh)>;
      if constexpr (is_polygonal_mesh_v<MeshType>) {
        writeConnectivity(mesh->connectivity(), file, checkpoint_group);
      }
    },
    mesh_v->variant());
}

void
writeMesh(const std::string& symbol_name,
          const EmbeddedData& embedded_data,
          HighFive::File& file,
          HighFive::Group& checkpoint_group,
          HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const MeshVariant> mesh_v =
    dynamic_cast<const DataHandler<const MeshVariant>&>(embedded_data.get()).data_ptr();

  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(mesh_v)>));
  variable_group.createAttribute("id", mesh_v->id());

  writeMesh(mesh_v, file, checkpoint_group);
}

}   // namespace checkpointing
