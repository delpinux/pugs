#include <utils/checkpointing/ReadIInterfaceDescriptor.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/NamedInterfaceDescriptor.hpp>
#include <mesh/NumberedInterfaceDescriptor.hpp>
#include <utils/checkpointing/IInterfaceDescriptorHFType.hpp>

namespace checkpointing
{

EmbeddedData
readIInterfaceDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group iinterfacedescriptor_group = symbol_table_group.getGroup("embedded/" + symbol_name);
  const IInterfaceDescriptor::Type iinterface_descriptor_type =
    iinterfacedescriptor_group.getAttribute("iinterface_descriptor_type").read<IInterfaceDescriptor::Type>();

  std::shared_ptr<const IInterfaceDescriptor> iinterface_descriptor;

  switch (iinterface_descriptor_type) {
  case IInterfaceDescriptor::Type::named: {
    const std::string name = iinterfacedescriptor_group.getAttribute("name").read<std::string>();
    iinterface_descriptor  = std::make_shared<const NamedInterfaceDescriptor>(name);
    break;
  }
  case IInterfaceDescriptor::Type::numbered: {
    const unsigned int number = iinterfacedescriptor_group.getAttribute("number").read<unsigned int>();
    iinterface_descriptor     = std::make_shared<const NumberedInterfaceDescriptor>(number);
    break;
  }
  }
  return {std::make_shared<DataHandler<const IInterfaceDescriptor>>(iinterface_descriptor)};
}

}   // namespace checkpointing
