#ifndef READ_IZONE_DESCRIPTOR_HPP
#define READ_IZONE_DESCRIPTOR_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

EmbeddedData readIZoneDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_IZONE_DESCRIPTOR_HPP
