#include <utils/checkpointing/WriteIWriter.hpp>

#include <language/modules/WriterModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <output/WriterBase.hpp>
#include <utils/checkpointing/IWriterHFType.hpp>

namespace checkpointing
{

void
writeIWriter(const std::string& symbol_name,
             const EmbeddedData& embedded_data,
             HighFive::File&,
             HighFive::Group&,
             HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const IWriter> iwriter_p =
    dynamic_cast<const DataHandler<const IWriter>&>(embedded_data.get()).data_ptr();

  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(iwriter_p)>));

  variable_group.createAttribute("iwriter_type", iwriter_p->type());

  switch (iwriter_p->type()) {
  case IWriter::Type::gnuplot:
  case IWriter::Type::gnuplot_1d:
  case IWriter::Type::gnuplot_raw:
  case IWriter::Type::vtk: {
    const WriterBase& writer = dynamic_cast<const WriterBase&>(*iwriter_p);

    variable_group.createAttribute("base_filename", writer.m_base_filename);
    if (writer.m_signature.has_value()) {
      variable_group.createAttribute("signature", writer.m_signature.value());
    }
    if (writer.m_period_manager.has_value()) {
      const WriterBase::PeriodManager period_manager = writer.m_period_manager.value();
      HighFive::Group period_manager_group           = variable_group.createGroup("period_manager");

      period_manager_group.createAttribute("time_period", period_manager.timePeriod());
      period_manager_group.createAttribute("next_time", period_manager.nextTime());
      period_manager_group.createAttribute("saved_times", period_manager.getSavedTimes());
    }

    break;
  }
  }
}

}   // namespace checkpointing
