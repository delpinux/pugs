#ifndef READ_VARIABLE_BC_DESCRIPTOR_HPP
#define READ_VARIABLE_BC_DESCRIPTOR_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

EmbeddedData readVariableBCDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_VARIABLE_BC_DESCRIPTOR_HPP
