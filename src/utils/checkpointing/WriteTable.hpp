#ifndef WRITE_TABLE_HPP
#define WRITE_TABLE_HPP

#include <utils/HighFivePugsUtils.hpp>

#include <mesh/CellType.hpp>
#include <mesh/ItemId.hpp>
#include <utils/Messenger.hpp>
#include <utils/Table.hpp>

namespace checkpointing
{

template <typename DataType>
PUGS_INLINE void
write(HighFive::Group& group, const std::string& name, const Table<DataType>& table)
{
  const size_t number_of_columns = parallel::allReduceMax(table.numberOfColumns());
  // LCOV_EXCL_START
  if ((table.numberOfColumns() != number_of_columns) and (table.numberOfRows() > 0)) {
    throw UnexpectedError("table must have same number of columns in parallel");
  }
  // LCOV_EXCL_STOP

  auto get_address = [](auto& t) { return (t.numberOfRows() * t.numberOfColumns() > 0) ? &(t(0, 0)) : nullptr; };

  Array<size_t> number_of_rows_per_rank = parallel::allGather(table.numberOfRows());
  size_t global_size                    = sum(number_of_rows_per_rank) * number_of_columns;

  size_t current_offset = 0;
  for (size_t i = 0; i < parallel::rank(); ++i) {
    current_offset += number_of_rows_per_rank[i] * table.numberOfColumns();   // LCOV_EXCL_LINE
  }
  std::vector<size_t> offset{current_offset, 0ul};
  std::vector<size_t> count{table.numberOfRows() * table.numberOfColumns()};

  using data_type = std::remove_const_t<DataType>;
  HighFive::DataSetCreateProps properties;
  // properties.add(HighFive::Chunking(std::vector<hsize_t>{std::min(4ul * 1024ul * 1024ul, global_size)}));
  // properties.add(HighFive::Shuffle());
  // properties.add(HighFive::Deflate(3));

  auto xfer_props = HighFive::DataTransferProps{};
  xfer_props.add(HighFive::UseCollectiveIO{});

  HighFive::DataSet dataset;

  if constexpr (std::is_same_v<CellType, data_type>) {
    using base_type = std::underlying_type_t<CellType>;
    dataset = group.createDataSet<base_type>(name, HighFive::DataSpace{std::vector<size_t>{global_size}}, properties);
    dataset.select(offset, count)
      .template write_raw<base_type>(reinterpret_cast<const base_type*>(get_address(table)), xfer_props);
  } else if constexpr ((std::is_same_v<CellId, data_type>) or (std::is_same_v<FaceId, data_type>) or
                       (std::is_same_v<EdgeId, data_type>) or (std::is_same_v<NodeId, data_type>)) {
    using base_type = typename data_type::base_type;

    dataset = group.createDataSet<base_type>(name, HighFive::DataSpace{std::vector<size_t>{global_size}}, properties);
    dataset.select(offset, count)
      .template write_raw<base_type>(reinterpret_cast<const base_type*>(get_address(table)), xfer_props);
  } else {
    dataset = group.createDataSet<data_type>(name, HighFive::DataSpace{std::vector<size_t>{global_size}}, properties);
    dataset.select(offset, count).template write_raw<data_type>(get_address(table), xfer_props);
  }

  std::vector<size_t> number_of_rows_per_rank_vector;
  for (size_t i = 0; i < number_of_rows_per_rank.size(); ++i) {
    number_of_rows_per_rank_vector.push_back(number_of_rows_per_rank[i]);
  }

  dataset.createAttribute("number_of_rows_per_rank", number_of_rows_per_rank_vector);
  dataset.createAttribute("number_of_columns", number_of_columns);
}

}   // namespace checkpointing

#endif   // WRITE_TABLE_HPP
