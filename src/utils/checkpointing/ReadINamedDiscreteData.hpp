#ifndef READ_INAMED_DISCRETE_DATA_HPP
#define READ_INAMED_DISCRETE_DATA_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

EmbeddedData readINamedDiscreteData(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_INAMED_DISCRETE_DATA_HPP
