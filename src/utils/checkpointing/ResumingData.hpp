#ifndef RESUMING_DATA_HPP
#define RESUMING_DATA_HPP

#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsAssert.hpp>

#include <map>

class IConnectivity;
class MeshVariant;
class SymbolTable;
class FunctionSymbolId;

namespace checkpointing
{

class ResumingData
{
 private:
  std::map<size_t, std::shared_ptr<const IConnectivity>> m_id_to_iconnectivity_map;
  std::map<size_t, std::shared_ptr<const MeshVariant>> m_id_to_mesh_variant_map;
  std::map<size_t, std::shared_ptr<const FunctionSymbolId>> m_id_to_function_symbol_id_map;

  ResumingData()  = default;
  ~ResumingData() = default;

  static ResumingData* m_instance;

  template <size_t Dimension>
  std::shared_ptr<const MeshVariant> _readPolygonalMesh(const HighFive::Group& mesh_group);

  void _getConnectivityList(const HighFive::Group& checkpoint);
  void _getMeshVariantList(const HighFive::Group& checkpoint);
  void _getFunctionIds(const HighFive::Group& checkpoint, std::shared_ptr<SymbolTable> p_symbol_table);

 public:
  void readData(const HighFive::Group& checkpoint, std::shared_ptr<SymbolTable> p_symbol_table);

  const std::shared_ptr<const IConnectivity>& iConnectivity(const size_t connectivity_id) const;
  const std::shared_ptr<const MeshVariant>& meshVariant(const size_t mesh_id) const;
  const std::shared_ptr<const FunctionSymbolId>& functionSymbolId(const size_t function_symbol_id) const;

  static void create();
  static void destroy();

  static ResumingData&
  instance()
  {
    Assert(m_instance != nullptr, "instance not created");
    return *m_instance;
  }

  ResumingData(const ResumingData&) = delete;
  ResumingData(ResumingData&&)      = delete;
};

}   // namespace checkpointing

#endif   // RESUMING_DATA_HPP
