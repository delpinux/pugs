#include <utils/checkpointing/PrintScriptFrom.hpp>

#include <rang.hpp>
#include <utils/pugs_config.hpp>

#include <iostream>

#ifdef PUGS_HAS_HDF5

#include <utils/Exceptions.hpp>
#include <utils/HighFivePugsUtils.hpp>

void
printScriptFrom(const std::string& filename, const uint64_t& checkpoint_number, std::ostream& os)
{
  try {
    HighFive::SilenceHDF5 m_silence_hdf5{true};
    HighFive::File file(filename, HighFive::File::ReadWrite);
    const std::string checkpoint_name = "checkpoint_" + std::to_string(checkpoint_number);

    if (not file.exist(checkpoint_name)) {
      std::ostringstream error_msg;
      error_msg << "cannot find checkpoint " << rang::fgB::magenta << checkpoint_number << rang::fg::reset << " in "
                << rang::fgB::yellow << filename << rang::fg::reset;
      throw NormalError(error_msg.str());
    }

    HighFive::Group checkpoint = file.getGroup(checkpoint_name);
    os << checkpoint.getAttribute("data.pgs").read<std::string>();
  }
  catch (HighFive::Exception& e) {
    throw NormalError(e.what());
  }
}

#else   // PUGS_HAS_HDF5

void
printScriptFrom(const std::string&, const uint64_t&, std::ostream&)
{
  std::cerr << rang::fgB::red << "error: " << rang::fg::reset << "printing  checkpoint's script requires HDF5\n";
}

#endif   // PUGS_HAS_HDF5
