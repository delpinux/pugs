#include <utils/checkpointing/WriteIInterfaceDescriptor.hpp>

#include <language/modules/MeshModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/NamedInterfaceDescriptor.hpp>
#include <mesh/NumberedInterfaceDescriptor.hpp>
#include <utils/checkpointing/IInterfaceDescriptorHFType.hpp>

namespace checkpointing
{

void
writeIInterfaceDescriptor(const std::string& symbol_name,
                          const EmbeddedData& embedded_data,
                          HighFive::File&,
                          HighFive::Group&,
                          HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const IInterfaceDescriptor> iinterface_descriptor_p =
    dynamic_cast<const DataHandler<const IInterfaceDescriptor>&>(embedded_data.get()).data_ptr();

  const IInterfaceDescriptor& iinterface_descriptor = *iinterface_descriptor_p;

  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(iinterface_descriptor_p)>));
  variable_group.createAttribute("iinterface_descriptor_type", iinterface_descriptor.type());

  switch (iinterface_descriptor.type()) {
  case IInterfaceDescriptor::Type::named: {
    const NamedInterfaceDescriptor& named_interface_descriptor =
      dynamic_cast<const NamedInterfaceDescriptor&>(iinterface_descriptor);
    variable_group.createAttribute("name", named_interface_descriptor.name());
    break;
  }
  case IInterfaceDescriptor::Type::numbered: {
    const NumberedInterfaceDescriptor& numbered_boundary_descriptor =
      dynamic_cast<const NumberedInterfaceDescriptor&>(iinterface_descriptor);
    variable_group.createAttribute("number", numbered_boundary_descriptor.number());
    break;
  }
  }
}

}   // namespace checkpointing
