#ifndef WRITE_IDISCRETE_FUNCTION_DESCRIPTOR_HPP
#define WRITE_IDISCRETE_FUNCTION_DESCRIPTOR_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

void writeIDiscreteFunctionDescriptor(const std::string& symbol_name,
                                      const EmbeddedData& embedded_data,
                                      HighFive::File&,
                                      HighFive::Group&,
                                      HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // WRITE_IDISCRETE_FUNCTION_DESCRIPTOR_HPP
