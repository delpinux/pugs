#include <utils/checkpointing/ReadINamedDiscreteData.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <output/NamedDiscreteFunction.hpp>
#include <output/NamedItemArrayVariant.hpp>
#include <output/NamedItemValueVariant.hpp>
#include <utils/checkpointing/INamedDiscreteDataHFType.hpp>
#include <utils/checkpointing/ReadDiscreteFunctionVariant.hpp>
#include <utils/checkpointing/ReadItemArrayVariant.hpp>
#include <utils/checkpointing/ReadItemValueVariant.hpp>

namespace checkpointing
{

EmbeddedData
readINamedDiscreteData(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group inamed_discrete_data_group = symbol_table_group.getGroup("embedded/" + symbol_name);

  const INamedDiscreteData::Type& type =
    inamed_discrete_data_group.getAttribute("named_discrete_data_type").read<INamedDiscreteData::Type>();

  const std::string name = inamed_discrete_data_group.getAttribute("named_discrete_data_name").read<std::string>();

  std::shared_ptr<const INamedDiscreteData> inamed_discrete_data;

  switch (type) {
  case INamedDiscreteData::Type::discrete_function: {
    HighFive::Group discrete_function_group = inamed_discrete_data_group.getGroup("discrete_function_variant");
    inamed_discrete_data =
      std::make_shared<const NamedDiscreteFunction>(readDiscreteFunctionVariant(discrete_function_group), name);
    break;
  }
  case INamedDiscreteData::Type::item_array: {
    HighFive::Group item_array_group = inamed_discrete_data_group.getGroup("item_array_variant");
    inamed_discrete_data = std::make_shared<const NamedItemArrayVariant>(readItemArrayVariant(item_array_group), name);
    break;
  }
  case INamedDiscreteData::Type::item_value: {
    HighFive::Group item_value_group = inamed_discrete_data_group.getGroup("item_value_variant");
    inamed_discrete_data = std::make_shared<const NamedItemValueVariant>(readItemValueVariant(item_value_group), name);
    break;
  }
  }

  return {std::make_shared<DataHandler<const INamedDiscreteData>>(inamed_discrete_data)};
}

}   // namespace checkpointing
