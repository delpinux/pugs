#ifndef READ_IBOUNDARY_CONDITION_DESCRIPTOR_HPP
#define READ_IBOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <utils/HighFivePugsUtils.hpp>

class IBoundaryConditionDescriptor;
class EmbeddedData;

namespace checkpointing
{

std::shared_ptr<const IBoundaryConditionDescriptor> readIBoundaryConditionDescriptor(
  const HighFive::Group& iboundaryconditiondecriptor_group);

EmbeddedData readIBoundaryConditionDescriptor(const std::string& symbol_name,
                                              const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_IBOUNDARY_CONDITION_DESCRIPTOR_HPP
