#ifndef WRITE_IQUADRATURE_DESCRIPTOR_HPP
#define WRITE_IQUADRATURE_DESCRIPTOR_HPP

#include <language/utils/EmbeddedData.hpp>
#include <utils/HighFivePugsUtils.hpp>

namespace checkpointing
{

void writeIQuadratureDescriptor(const std::string& symbol_name,
                                const EmbeddedData& embedded_data,
                                HighFive::File&,
                                HighFive::Group&,
                                HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // WRITE_IQUADRATURE_DESCRIPTOR_HPP
