#ifndef OSTREAM_TYPE_HF_TYPE_HPP
#define OSTREAM_TYPE_HF_TYPE_HPP

#include <language/utils/OStream.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<OStream::Type> PUGS_INLINE
create_enum_ostream_type()
{
  return {{"std_ostream", OStream::Type::std_ostream}, {"std_ofstream", OStream::Type::std_ofstream}};
}
HIGHFIVE_REGISTER_TYPE(OStream::Type, create_enum_ostream_type)

#endif   // OSTREAM_TYPE_HF_TYPE_HPP
