#include <utils/checkpointing/ResumingData.hpp>

#include <language/utils/SymbolTable.hpp>
#include <mesh/ConnectivityDescriptor.hpp>
#include <mesh/DualConnectivityManager.hpp>
#include <mesh/DualMeshManager.hpp>
#include <mesh/Mesh.hpp>
#include <utils/Exceptions.hpp>
#include <utils/checkpointing/DualMeshTypeHFType.hpp>
#include <utils/checkpointing/ReadArray.hpp>
#include <utils/checkpointing/RefItemListHFType.hpp>

namespace checkpointing
{

ResumingData* ResumingData::m_instance = nullptr;

void
ResumingData::_getConnectivityList(const HighFive::Group& checkpoint)
{
  if (checkpoint.exist("connectivity")) {
    HighFive::Group connectivity_group = checkpoint.getGroup("connectivity");

    // it is important to use a map here. "id"s must be treated in
    // increasing order.
    std::map<size_t, std::string> id_name_map;
    for (auto connectivity_id_name : connectivity_group.listObjectNames()) {
      HighFive::Group connectivity_info = connectivity_group.getGroup(connectivity_id_name);
      id_name_map[connectivity_info.getAttribute("id").read<uint64_t>()] = connectivity_id_name;
    }

    for (auto [id, name] : id_name_map) {
      HighFive::Group connectivity_data = connectivity_group.getGroup(name);
      const std::string type            = connectivity_data.getAttribute("type").read<std::string>();

      while (id > GlobalVariableManager::instance().getConnectivityId()) {
        GlobalVariableManager::instance().getAndIncrementConnectivityId();
      }

      // LCOV_EXCL_START
      if (m_id_to_iconnectivity_map.contains(id)) {
        throw UnexpectedError("connectivity of id " + std::to_string(id) + " already defined!");
      }
      // LCOV_EXCL_STOP

      if (type == "unstructured") {
        const uint64_t dimension = connectivity_data.getAttribute("dimension").read<uint64_t>();

        ConnectivityDescriptor descriptor;
        descriptor.setCellTypeVector(readArray<CellType>(connectivity_data, "cell_type"));
        descriptor.setCellNumberVector(readArray<int>(connectivity_data, "cell_numbers"));
        descriptor.setNodeNumberVector(readArray<int>(connectivity_data, "node_numbers"));

        descriptor.setCellOwnerVector(readArray<int>(connectivity_data, "cell_owner"));
        descriptor.setNodeOwnerVector(readArray<int>(connectivity_data, "node_owner"));

        using index_type = typename ConnectivityMatrix::IndexType;

        descriptor.setCellToNodeMatrix(
          ConnectivityMatrix{readArray<index_type>(connectivity_data, "cell_to_node_matrix_rowsMap"),
                             readArray<index_type>(connectivity_data, "cell_to_node_matrix_values")});

        if (dimension > 1) {
          descriptor.setFaceNumberVector(readArray<int>(connectivity_data, "face_numbers"));
          descriptor.setFaceOwnerVector(readArray<int>(connectivity_data, "face_owner"));

          descriptor.setCellToFaceMatrix(
            ConnectivityMatrix{readArray<index_type>(connectivity_data, "cell_to_face_matrix_rowsMap"),
                               readArray<index_type>(connectivity_data, "cell_to_face_matrix_values")});

          descriptor.setFaceToNodeMatrix(
            ConnectivityMatrix{readArray<index_type>(connectivity_data, "face_to_node_matrix_rowsMap"),
                               readArray<index_type>(connectivity_data, "face_to_node_matrix_values")});

          descriptor.setNodeToFaceMatrix(
            ConnectivityMatrix{readArray<index_type>(connectivity_data, "node_to_face_matrix_rowsMap"),
                               readArray<index_type>(connectivity_data, "node_to_face_matrix_values")});

          descriptor.setCellFaceIsReversed(readArray<bool>(connectivity_data, "cell_face_is_reversed"));
        }

        if (dimension > 2) {
          descriptor.setEdgeNumberVector(readArray<int>(connectivity_data, "edge_numbers"));
          descriptor.setEdgeOwnerVector(readArray<int>(connectivity_data, "edge_owner"));

          descriptor.setCellToEdgeMatrix(
            ConnectivityMatrix{readArray<index_type>(connectivity_data, "cell_to_edge_matrix_rowsMap"),
                               readArray<index_type>(connectivity_data, "cell_to_edge_matrix_values")});

          descriptor.setFaceToEdgeMatrix(
            ConnectivityMatrix{readArray<index_type>(connectivity_data, "face_to_edge_matrix_rowsMap"),
                               readArray<index_type>(connectivity_data, "face_to_edge_matrix_values")});

          descriptor.setEdgeToNodeMatrix(
            ConnectivityMatrix{readArray<index_type>(connectivity_data, "edge_to_node_matrix_rowsMap"),
                               readArray<index_type>(connectivity_data, "edge_to_node_matrix_values")});

          descriptor.setNodeToEdgeMatrix(
            ConnectivityMatrix{readArray<index_type>(connectivity_data, "node_to_edge_matrix_rowsMap"),
                               readArray<index_type>(connectivity_data, "node_to_edge_matrix_values")});

          descriptor.setFaceEdgeIsReversed(readArray<bool>(connectivity_data, "face_edge_is_reversed"));
        }

        if (connectivity_data.exist("item_ref_list")) {
          HighFive::Group item_group = connectivity_data.getGroup("item_ref_list");
          for (auto item_type_name : item_group.listObjectNames()) {
            HighFive::Group item_ref_name_list = item_group.getGroup(item_type_name);

            for (auto item_ref_list_id : item_ref_name_list.listObjectNames()) {
              HighFive::Group item_ref_list_data = item_ref_name_list.getGroup(item_ref_list_id);

              RefId ref_id(item_ref_list_data.getAttribute("tag_number").read<uint64_t>(),
                           item_ref_list_data.getAttribute("tag_name").read<std::string>());

              RefItemListBase::Type ref_item_list_type =
                item_ref_list_data.getAttribute("type").read<RefItemListBase::Type>();

              if (item_type_name == "cell") {
                descriptor.addRefItemList(RefItemList<ItemType::cell>{ref_id,
                                                                      readArray<CellId>(item_ref_list_data, "list"),
                                                                      ref_item_list_type});
              } else if (item_type_name == "face") {
                descriptor.addRefItemList(RefItemList<ItemType::face>{ref_id,
                                                                      readArray<FaceId>(item_ref_list_data, "list"),
                                                                      ref_item_list_type});
              } else if (item_type_name == "edge") {
                descriptor.addRefItemList(RefItemList<ItemType::edge>{ref_id,
                                                                      readArray<EdgeId>(item_ref_list_data, "list"),
                                                                      ref_item_list_type});
              } else if (item_type_name == "node") {
                descriptor.addRefItemList(RefItemList<ItemType::node>{ref_id,
                                                                      readArray<NodeId>(item_ref_list_data, "list"),
                                                                      ref_item_list_type});
              } else {
                // LCOV_EXCL_START
                throw UnexpectedError("invalid item type: " + item_type_name);
                // LCOV_EXCL_STOP
              }
            }
          }
        }

        switch (dimension) {
        case 1: {
          m_id_to_iconnectivity_map.insert({id, Connectivity<1>::build(descriptor)});
          break;
        }
        case 2: {
          m_id_to_iconnectivity_map.insert({id, Connectivity<2>::build(descriptor)});
          break;
        }
        case 3: {
          m_id_to_iconnectivity_map.insert({id, Connectivity<3>::build(descriptor)});
          break;
        }
          // LCOV_EXCL_START
        default: {
          throw UnexpectedError("invalid dimension " + std::to_string(dimension));
        }
          // LCOV_EXCL_STOP
        }
      } else if (type == "dual_connectivity") {
        const DualMeshType type_of_dual     = connectivity_data.getAttribute("type_of_dual").read<DualMeshType>();
        const size_t primal_connectivity_id = connectivity_data.getAttribute("primal_connectivity_id").read<size_t>();

        std::shared_ptr<const IConnectivity> iprimal_connectivity =
          m_id_to_iconnectivity_map.at(primal_connectivity_id);

        switch (iprimal_connectivity->dimension()) {
        case 1: {
          if (type_of_dual != DualMeshType::Dual1D) {
            // LCOV_EXCL_START
            throw UnexpectedError("unexpected dual mesh type");
            // LCOV_EXCL_STOP
          }

          const Connectivity<1>& primal_connectivity = dynamic_cast<const Connectivity<1>&>(*iprimal_connectivity);
          m_id_to_iconnectivity_map.insert(
            {id, DualConnectivityManager::instance().getDual1DConnectivity(primal_connectivity)});
          break;
        }
        case 2: {
          const Connectivity<2>& primal_connectivity = dynamic_cast<const Connectivity<2>&>(*iprimal_connectivity);

          if (type_of_dual == DualMeshType::Diamond) {
            m_id_to_iconnectivity_map.insert(
              {id, DualConnectivityManager::instance().getDiamondDualConnectivity(primal_connectivity)});
          } else if (type_of_dual == DualMeshType::Median) {
            m_id_to_iconnectivity_map.insert(
              {id, DualConnectivityManager::instance().getMedianDualConnectivity(primal_connectivity)});
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("unexpected dual mesh type");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case 3: {
          const Connectivity<3>& primal_connectivity = dynamic_cast<const Connectivity<3>&>(*iprimal_connectivity);

          if (type_of_dual == DualMeshType::Diamond) {
            m_id_to_iconnectivity_map.insert(
              {id, DualConnectivityManager::instance().getDiamondDualConnectivity(primal_connectivity)});
          } else if (type_of_dual == DualMeshType::Median) {
            m_id_to_iconnectivity_map.insert(
              {id, DualConnectivityManager::instance().getMedianDualConnectivity(primal_connectivity)});
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("unexpected dual mesh type");
            // LCOV_EXCL_STOP
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          throw UnexpectedError("unexpected dimension");
        }
          // LCOV_EXCL_STOP
        }
      } else {
        // LCOV_EXCL_START
        throw UnexpectedError("invalid connectivity type: " + type);
        // LCOV_EXCL_STOP
      }
    }
  }

  const size_t next_connectivity_id =
    checkpoint.getGroup("singleton/global_variables").getAttribute("connectivity_id").read<size_t>();

  while (next_connectivity_id > GlobalVariableManager::instance().getConnectivityId()) {
    GlobalVariableManager::instance().getAndIncrementConnectivityId();
  }
}

template <size_t Dimension>
std::shared_ptr<const MeshVariant>
ResumingData::_readPolygonalMesh(const HighFive::Group& mesh_data)
{
  const uint64_t connectivity_id = mesh_data.getAttribute("connectivity").read<uint64_t>();
  auto i_id_to_iconnectivity     = m_id_to_iconnectivity_map.find(connectivity_id);
  if (i_id_to_iconnectivity == m_id_to_iconnectivity_map.end()) {
    // LCOV_EXCL_START
    throw UnexpectedError("cannot find connectivity " + std::to_string(connectivity_id));
    // LCOV_EXCL_STOP
  }
  std::shared_ptr<const IConnectivity> i_connectivity = i_id_to_iconnectivity->second;
  if (i_connectivity->dimension() != Dimension) {
    // LCOV_EXCL_START
    throw UnexpectedError("invalid connectivity dimension " + std::to_string(i_connectivity->dimension()));
    // LCOV_EXCL_STOP
  }

  std::shared_ptr<const Connectivity<Dimension>> connectivity =
    std::dynamic_pointer_cast<const Connectivity<Dimension>>(i_connectivity);

  Array<const TinyVector<Dimension>> xr_array = readArray<TinyVector<Dimension>>(mesh_data, "xr");
  NodeValue<const TinyVector<Dimension>> xr{*connectivity, xr_array};

  std::shared_ptr mesh = std::make_shared<const Mesh<Dimension>>(connectivity, xr);
  return std::make_shared<const MeshVariant>(mesh);
}

void
ResumingData::_getMeshVariantList(const HighFive::Group& checkpoint)
{
  if (checkpoint.exist("mesh")) {
    HighFive::Group mesh_group = checkpoint.getGroup("mesh");

    // it is important to use a map here. "id"s must be treated in
    // increasing order.
    std::map<size_t, std::string> id_name_map;
    for (auto mesh_id_name : mesh_group.listObjectNames()) {
      HighFive::Group mesh_info                                  = mesh_group.getGroup(mesh_id_name);
      id_name_map[mesh_info.getAttribute("id").read<uint64_t>()] = mesh_id_name;
    }

    for (auto&& id_name : id_name_map) {
      // workaround due to clang not handling correctly captures
      const auto& id   = id_name.first;
      const auto& name = id_name.second;

      HighFive::Group mesh_data = mesh_group.getGroup(name);
      const std::string type    = mesh_data.getAttribute("type").read<std::string>();

      while (id > GlobalVariableManager::instance().getMeshId()) {
        GlobalVariableManager::instance().getAndIncrementMeshId();
      }

      if (type == "polygonal") {
        const uint64_t dimension = mesh_data.getAttribute("dimension").read<uint64_t>();

        switch (dimension) {
        case 1: {
          m_id_to_mesh_variant_map.insert({id, this->_readPolygonalMesh<1>(mesh_data)});
          break;
        }
        case 2: {
          m_id_to_mesh_variant_map.insert({id, this->_readPolygonalMesh<2>(mesh_data)});
          break;
        }
        case 3: {
          m_id_to_mesh_variant_map.insert({id, this->_readPolygonalMesh<3>(mesh_data)});
          break;
        }
          // LCOV_EXCL_START
        default: {
          throw UnexpectedError("invalid mesh dimension " + std::to_string(dimension));
        }
          // LCOV_EXCL_STOP
        }
      } else if (type == "dual_mesh") {
        const DualMeshType type_of_dual = mesh_data.getAttribute("type_of_dual").read<DualMeshType>();
        const size_t primal_mesh_id     = mesh_data.getAttribute("primal_mesh_id").read<size_t>();

        std::shared_ptr<const MeshVariant> primal_mesh_variant = m_id_to_mesh_variant_map.at(primal_mesh_id);

        std::visit(
          [&](auto&& mesh) {
            using MeshType = std::decay_t<decltype(*mesh)>;
            if constexpr (std::is_same_v<MeshType, Mesh<1>>) {
              if (type_of_dual == DualMeshType::Dual1D) {
                m_id_to_mesh_variant_map.insert({id, DualMeshManager::instance().getDual1DMesh(primal_mesh_variant)});
              } else {
                // LCOV_EXCL_START
                throw UnexpectedError("unexpected dual mesh type");
                // LCOV_EXCL_STOP
              }
            } else if constexpr (std::is_same_v<MeshType, Mesh<2>> or std::is_same_v<MeshType, Mesh<3>>) {
              if (type_of_dual == DualMeshType::Diamond) {
                m_id_to_mesh_variant_map.insert(
                  {id, DualMeshManager::instance().getDiamondDualMesh(primal_mesh_variant)});
              } else if (type_of_dual == DualMeshType::Median) {
                m_id_to_mesh_variant_map.insert(
                  {id, DualMeshManager::instance().getMedianDualMesh(primal_mesh_variant)});
              } else {
                // LCOV_EXCL_START
                throw UnexpectedError("unexpected dual mesh type");
                // LCOV_EXCL_STOP
              }
            }
          },
          primal_mesh_variant->variant());

      } else {
        // LCOV_EXCL_START
        throw UnexpectedError("invalid mesh type");
        // LCOV_EXCL_STOP
      }
    }
  }

  const size_t next_mesh_id = checkpoint.getGroup("singleton/global_variables").getAttribute("mesh_id").read<size_t>();

  while (next_mesh_id > GlobalVariableManager::instance().getMeshId()) {
    GlobalVariableManager::instance().getAndIncrementMeshId();
  }
}

void
ResumingData::_getFunctionIds(const HighFive::Group& checkpoint, std::shared_ptr<SymbolTable> p_symbol_table)
{
  if (checkpoint.exist("functions")) {
    size_t symbol_table_id               = 0;
    const HighFive::Group function_group = checkpoint.getGroup("functions");
    while (p_symbol_table.use_count() > 0) {
      for (auto symbol : p_symbol_table->symbolList()) {
        if (symbol.attributes().dataType() == ASTNodeDataType::function_t) {
          if (not function_group.exist(symbol.name())) {
            // LCOV_EXCL_START
            std::ostringstream error_msg;
            error_msg << "cannot find function " << rang::fgB::yellow << symbol.name() << rang::fg::reset << " in "
                      << rang::fgB::cyan << checkpoint.getFile().getName() << rang::fg::reset;
            throw UnexpectedError(error_msg.str());
            // LCOV_EXCL_STOP
          } else {   // LCOV_EXCL_LINE
            const HighFive::Group function  = function_group.getGroup(symbol.name());
            const size_t stored_function_id = function.getAttribute("id").read<size_t>();
            const size_t function_id        = std::get<size_t>(symbol.attributes().value());
            if (symbol_table_id != function.getAttribute("symbol_table_id").read<size_t>()) {
              // LCOV_EXCL_START
              std::ostringstream error_msg;
              error_msg << "symbol table of function " << rang::fgB::yellow << symbol.name() << rang::fg::reset
                        << " does not match the one stored in " << rang::fgB::cyan << checkpoint.getFile().getName()
                        << rang::fg::reset;
              throw UnexpectedError(error_msg.str());
              // LCOV_EXCL_STOP
            } else if (function_id != stored_function_id) {
              // LCOV_EXCL_START
              std::ostringstream error_msg;
              error_msg << "id (" << function_id << ") of function " << rang::fgB::yellow << symbol.name()
                        << rang::fg::reset << " does not match the one stored in " << rang::fgB::cyan
                        << checkpoint.getFile().getName() << rang::fg::reset << "(" << stored_function_id << ")";
              throw UnexpectedError(error_msg.str());
              // LCOV_EXCL_STOP
            } else {   // LCOV_EXCL_LINE
              if (m_id_to_function_symbol_id_map.contains(function_id)) {
                // LCOV_EXCL_START
                std::ostringstream error_msg;
                error_msg << "id (" << function_id << ") of function " << rang::fgB::yellow << symbol.name()
                          << rang::fg::reset << " is duplicated";
                throw UnexpectedError(error_msg.str());
                // LCOV_EXCL_STOP
              }   // LCOV_EXCL_LINE
              m_id_to_function_symbol_id_map[function_id] =
                std::make_shared<FunctionSymbolId>(function_id, p_symbol_table);
            }
          }
        }
      }
      p_symbol_table = p_symbol_table->parentTable();
      ++symbol_table_id;
    }
  }
}

void
ResumingData::readData(const HighFive::Group& checkpoint, std::shared_ptr<SymbolTable> p_symbol_table)
{
  this->_getConnectivityList(checkpoint);
  this->_getMeshVariantList(checkpoint);
  this->_getFunctionIds(checkpoint, p_symbol_table);
}

const std::shared_ptr<const IConnectivity>&
ResumingData::iConnectivity(const size_t connectivity_id) const
{
  auto i_id_to_connectivity = m_id_to_iconnectivity_map.find(connectivity_id);
  if (i_id_to_connectivity == m_id_to_iconnectivity_map.end()) {
    // LCOV_EXCL_START
    throw UnexpectedError("cannot find connectivity of id " + std::to_string(connectivity_id));
    // LCOV_EXCL_STOP
  } else {
    return i_id_to_connectivity->second;
  }
}

const std::shared_ptr<const MeshVariant>&
ResumingData::meshVariant(const size_t mesh_id) const
{
  auto i_id_to_mesh = m_id_to_mesh_variant_map.find(mesh_id);
  if (i_id_to_mesh == m_id_to_mesh_variant_map.end()) {
    // LCOV_EXCL_START
    throw UnexpectedError("cannot find mesh of id " + std::to_string(mesh_id));
    // LCOV_EXCL_STOP
  } else {
    return i_id_to_mesh->second;
  }
}

const std::shared_ptr<const FunctionSymbolId>&
ResumingData::functionSymbolId(const size_t function_symbol_id) const
{
  auto i_id_to_function_symbol_id = m_id_to_function_symbol_id_map.find(function_symbol_id);
  if (i_id_to_function_symbol_id == m_id_to_function_symbol_id_map.end()) {
    // LCOV_EXCL_START
    throw UnexpectedError("cannot find function_symbol_id of id " + std::to_string(function_symbol_id));
    // LCOV_EXCL_STOP
  } else {
    return i_id_to_function_symbol_id->second;
  }
}

void
ResumingData::create()
{
  Assert(m_instance == nullptr, "instance already created");
  m_instance = new ResumingData;
}

void
ResumingData::destroy()
{
  Assert(m_instance != nullptr, "instance not created");
  delete m_instance;
  m_instance = nullptr;
}

}   // namespace checkpointing
