#ifndef READ_IINTERFACE_DESCRIPTOR_HPP
#define READ_IINTERFACE_DESCRIPTOR_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

EmbeddedData readIInterfaceDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_IINTERFACE_DESCRIPTOR_HPP
