#include <utils/checkpointing/ReadVariableBCDescriptor.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <scheme/VariableBCDescriptor.hpp>
#include <utils/checkpointing/ReadDiscreteFunctionVariant.hpp>
#include <utils/checkpointing/ReadIBoundaryConditionDescriptor.hpp>

namespace checkpointing
{

EmbeddedData
readVariableBCDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group variable_bc_descriptor_group = symbol_table_group.getGroup("embedded/" + symbol_name);

  HighFive::Group discrete_function_group = variable_bc_descriptor_group.getGroup("discrete_function");

  std::shared_ptr<const DiscreteFunctionVariant> discrete_function =
    readDiscreteFunctionVariant(discrete_function_group);

  HighFive::Group bc_descriptor_list_group = variable_bc_descriptor_group.getGroup("bc_descriptor_list");
  std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>> bc_descriptor_list;
  for (size_t i = 0; i < bc_descriptor_list_group.getNumberObjects(); ++i) {
    HighFive::Group bc_descriptor_group = bc_descriptor_list_group.getGroup(std::to_string(i));
    bc_descriptor_list.push_back(readIBoundaryConditionDescriptor(bc_descriptor_group));
  }

  std::shared_ptr<const VariableBCDescriptor> p_variable_bc_descriptor =
    std::make_shared<const VariableBCDescriptor>(discrete_function, bc_descriptor_list);

  return {std::make_shared<DataHandler<const VariableBCDescriptor>>(p_variable_bc_descriptor)};
}

}   // namespace checkpointing
