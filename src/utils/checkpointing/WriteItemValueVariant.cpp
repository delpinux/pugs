#include <utils/checkpointing/WriteItemValueVariant.hpp>

#include <language/modules/MeshModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/ItemValueVariant.hpp>
#include <scheme/VariableBCDescriptor.hpp>
#include <utils/checkpointing/ItemTypeHFType.hpp>
#include <utils/checkpointing/WriteConnectivity.hpp>
#include <utils/checkpointing/WriteItemValue.hpp>

namespace checkpointing
{

void
writeItemValueVariant(HighFive::Group& variable_group,
                      std::shared_ptr<const ItemValueVariant> item_value_variant_v,
                      HighFive::File& file,
                      HighFive::Group& checkpoint_group)
{
  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(item_value_variant_v)>));

  std::visit(
    [&](auto&& item_value) {
      using ItemValueT = std::decay_t<decltype(item_value)>;

      variable_group.createAttribute("item_type", ItemValueT::item_t);
      using data_type = std::decay_t<typename ItemValueT::data_type>;
      variable_group.createAttribute("data_type", dataTypeName(ast_node_data_type_from<data_type>));

      const IConnectivity& connectivity = *item_value.connectivity_ptr();
      variable_group.createAttribute("connectivity_id", connectivity.id());
      writeConnectivity(connectivity, file, checkpoint_group);

      write(variable_group, "values", item_value);
    },
    item_value_variant_v->itemValue());
}

void
writeItemValueVariant(const std::string& symbol_name,
                      const EmbeddedData& embedded_data,
                      HighFive::File& file,
                      HighFive::Group& checkpoint_group,
                      HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const ItemValueVariant> item_value_variant_p =
    dynamic_cast<const DataHandler<const ItemValueVariant>&>(embedded_data.get()).data_ptr();

  writeItemValueVariant(variable_group, item_value_variant_p, file, checkpoint_group);
}

}   // namespace checkpointing
