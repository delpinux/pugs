#ifndef WRITE_ITEM_VALUE_VARIANT_HPP
#define WRITE_ITEM_VALUE_VARIANT_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;
class ItemValueVariant;

namespace checkpointing
{

void writeItemValueVariant(HighFive::Group& variable_group,
                           std::shared_ptr<const ItemValueVariant> item_value_variant_v,
                           HighFive::File& file,
                           HighFive::Group& checkpoint_group);

void writeItemValueVariant(const std::string& symbol_name,
                           const EmbeddedData& embedded_data,
                           HighFive::File& file,
                           HighFive::Group& checkpoint_group,
                           HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // WRITE_ITEM_VALUE_VARIANT_HPP
