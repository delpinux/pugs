#include <utils/checkpointing/WriteIBoundaryDescriptor.hpp>

#include <language/modules/MeshModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <mesh/NamedBoundaryDescriptor.hpp>
#include <mesh/NumberedBoundaryDescriptor.hpp>
#include <utils/checkpointing/IBoundaryDescriptorHFType.hpp>

namespace checkpointing
{

void
writeIBoundaryDescriptor(HighFive::Group& variable_group, const IBoundaryDescriptor& iboundary_descriptor)
{
  variable_group.createAttribute("iboundary_descriptor_type", iboundary_descriptor.type());

  switch (iboundary_descriptor.type()) {
  case IBoundaryDescriptor::Type::named: {
    const NamedBoundaryDescriptor& named_boundary_descriptor =
      dynamic_cast<const NamedBoundaryDescriptor&>(iboundary_descriptor);
    variable_group.createAttribute("name", named_boundary_descriptor.name());
    break;
  }
  case IBoundaryDescriptor::Type::numbered: {
    const NumberedBoundaryDescriptor& numbered_boundary_descriptor =
      dynamic_cast<const NumberedBoundaryDescriptor&>(iboundary_descriptor);
    variable_group.createAttribute("number", numbered_boundary_descriptor.number());
    break;
  }
  }
}

void
writeIBoundaryDescriptor(const std::string& symbol_name,
                         const EmbeddedData& embedded_data,
                         HighFive::File&,
                         HighFive::Group&,
                         HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const IBoundaryDescriptor> iboundary_descriptor_p =
    dynamic_cast<const DataHandler<const IBoundaryDescriptor>&>(embedded_data.get()).data_ptr();

  const IBoundaryDescriptor& iboundary_descriptor = *iboundary_descriptor_p;

  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(iboundary_descriptor_p)>));

  writeIBoundaryDescriptor(variable_group, iboundary_descriptor);
}

}   // namespace checkpointing
