#include <utils/checkpointing/ReadDiscreteFunctionVariant.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>
#include <utils/checkpointing/DiscreteFunctionTypeHFType.hpp>
#include <utils/checkpointing/ReadItemArray.hpp>
#include <utils/checkpointing/ReadItemValue.hpp>
#include <utils/checkpointing/ResumingData.hpp>

namespace checkpointing
{

std::shared_ptr<DiscreteFunctionVariant>
readDiscreteFunctionVariant(const HighFive::Group& discrete_function_group)
{
  DiscreteFunctionType type = discrete_function_group.getAttribute("Vh_type").read<DiscreteFunctionType>();
  size_t mesh_id            = discrete_function_group.getAttribute("mesh_id").read<size_t>();

  std::shared_ptr<const MeshVariant> mesh_v = ResumingData::instance().meshVariant(mesh_id);

  const std::string data_type = discrete_function_group.getAttribute("data_type").read<std::string>();

  std::shared_ptr<DiscreteFunctionVariant> p_discrete_function;
  switch (type) {
  case DiscreteFunctionType::P0: {
    if (data_type == dataTypeName(ast_node_data_type_from<double>)) {
      p_discrete_function = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionP0<const double>(mesh_v,
                                         readItemValue<double, ItemType::cell>(discrete_function_group, "values",
                                                                               mesh_v->connectivity())));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<1>>)) {
      p_discrete_function = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionP0<const TinyVector<1>>(mesh_v,
                                                readItemValue<TinyVector<1>, ItemType::cell>(discrete_function_group,
                                                                                             "values",
                                                                                             mesh_v->connectivity())));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<2>>)) {
      p_discrete_function = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionP0<const TinyVector<2>>(mesh_v,
                                                readItemValue<TinyVector<2>, ItemType::cell>(discrete_function_group,
                                                                                             "values",
                                                                                             mesh_v->connectivity())));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<3>>)) {
      p_discrete_function = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionP0<const TinyVector<3>>(mesh_v,
                                                readItemValue<TinyVector<3>, ItemType::cell>(discrete_function_group,
                                                                                             "values",
                                                                                             mesh_v->connectivity())));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<1>>)) {
      p_discrete_function = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionP0<const TinyMatrix<1>>(mesh_v,
                                                readItemValue<TinyMatrix<1>, ItemType::cell>(discrete_function_group,
                                                                                             "values",
                                                                                             mesh_v->connectivity())));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<2>>)) {
      p_discrete_function = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionP0<const TinyMatrix<2>>(mesh_v,
                                                readItemValue<TinyMatrix<2>, ItemType::cell>(discrete_function_group,
                                                                                             "values",
                                                                                             mesh_v->connectivity())));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<3>>)) {
      p_discrete_function = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionP0<const TinyMatrix<3>>(mesh_v,
                                                readItemValue<TinyMatrix<3>, ItemType::cell>(discrete_function_group,
                                                                                             "values",
                                                                                             mesh_v->connectivity())));
    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("unexpected discrete function data type: " + data_type);
      // LCOV_EXCL_STOP
    }
    break;
  }
  case DiscreteFunctionType::P0Vector: {
    if (data_type == dataTypeName(ast_node_data_type_from<double>)) {
      p_discrete_function = std::make_shared<DiscreteFunctionVariant>(
        DiscreteFunctionP0Vector<const double>(mesh_v,
                                               readItemArray<double, ItemType::cell>(discrete_function_group, "values",
                                                                                     mesh_v->connectivity())));
    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("unexpected discrete function vector data type: " + data_type);
      // LCOV_EXCL_STOP
    }
    break;
  }
  }
  return p_discrete_function;
}

EmbeddedData
readDiscreteFunctionVariant(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group discrete_function_group = symbol_table_group.getGroup("embedded/" + symbol_name);

  std::shared_ptr<DiscreteFunctionVariant> p_discrete_function = readDiscreteFunctionVariant(discrete_function_group);

  return {std::make_shared<DataHandler<const DiscreteFunctionVariant>>(p_discrete_function)};
}

}   // namespace checkpointing
