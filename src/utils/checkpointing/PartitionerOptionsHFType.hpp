#ifndef PARTITIONER_OPTIONS_HF_TYPE_HPP
#define PARTITIONER_OPTIONS_HF_TYPE_HPP

#include <utils/HighFivePugsUtils.hpp>
#include <utils/PartitionerOptions.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<PartitionerLibrary> PUGS_INLINE
create_enum_PTOptions_library_type()
{
  return {{"ParMETIS", PartitionerLibrary::parmetis}, {"PTScotch", PartitionerLibrary::ptscotch}};
}
HIGHFIVE_REGISTER_TYPE(PartitionerLibrary, create_enum_PTOptions_library_type)

#endif   // PARTITIONER_OPTIONS_HF_TYPE_HPP
