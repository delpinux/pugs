#ifndef WRITE_IBOUNDARY_CONDITION_DESCRIPTOR_HPP
#define WRITE_IBOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <language/utils/EmbeddedData.hpp>
#include <scheme/IBoundaryConditionDescriptor.hpp>
#include <utils/HighFivePugsUtils.hpp>

namespace checkpointing
{

void writeIBoundaryConditionDescriptor(
  HighFive::Group& variable_group,
  std::shared_ptr<const IBoundaryConditionDescriptor> iboundary_condition_descriptor_p);

void writeIBoundaryConditionDescriptor(const std::string& symbol_name,
                                       const EmbeddedData& embedded_data,
                                       HighFive::File&,
                                       HighFive::Group&,
                                       HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // WRITE_IBOUNDARY_CONDITION_DESCRIPTOR_HPP
