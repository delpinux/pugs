#include <utils/checkpointing/ReadIBoundaryConditionDescriptor.hpp>

#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <scheme/AxisBoundaryConditionDescriptor.hpp>
#include <scheme/DirichletBoundaryConditionDescriptor.hpp>
#include <scheme/DirichletVectorBoundaryConditionDescriptor.hpp>
#include <scheme/ExternalBoundaryConditionDescriptor.hpp>
#include <scheme/FixedBoundaryConditionDescriptor.hpp>
#include <scheme/FourierBoundaryConditionDescriptor.hpp>
#include <scheme/FreeBoundaryConditionDescriptor.hpp>
#include <scheme/InflowBoundaryConditionDescriptor.hpp>
#include <scheme/InflowListBoundaryConditionDescriptor.hpp>
#include <scheme/NeumannBoundaryConditionDescriptor.hpp>
#include <scheme/OutflowBoundaryConditionDescriptor.hpp>
#include <scheme/SymmetryBoundaryConditionDescriptor.hpp>
#include <scheme/WallBoundaryConditionDescriptor.hpp>
#include <utils/Exceptions.hpp>
#include <utils/checkpointing/IBoundaryConditionDescriptorHFType.hpp>
#include <utils/checkpointing/ReadIBoundaryDescriptor.hpp>
#include <utils/checkpointing/ResumingData.hpp>

namespace checkpointing
{

std::shared_ptr<const IBoundaryConditionDescriptor>
readIBoundaryConditionDescriptor(const HighFive::Group& iboundaryconditiondecriptor_group)
{
  const IBoundaryConditionDescriptor::Type iboundary_condition_descriptor_type =
    iboundaryconditiondecriptor_group.getAttribute("iboundary_condition_descriptor_type")
      .read<IBoundaryConditionDescriptor::Type>();

  HighFive::Group boundary_group = iboundaryconditiondecriptor_group.getGroup("boundary");
  auto i_boundary_descriptor     = readIBoundaryDescriptor(boundary_group);

  std::shared_ptr<const IBoundaryConditionDescriptor> bc_descriptor;

  switch (iboundary_condition_descriptor_type) {
  case IBoundaryConditionDescriptor::Type::axis: {
    bc_descriptor = std::make_shared<const AxisBoundaryConditionDescriptor>(i_boundary_descriptor);
    break;
  }
  case IBoundaryConditionDescriptor::Type::dirichlet: {
    const std::string name = iboundaryconditiondecriptor_group.getAttribute("name").read<std::string>();
    const size_t rhs_id    = iboundaryconditiondecriptor_group.getAttribute("rhs_function_id").read<size_t>();

    bc_descriptor =
      std::make_shared<const DirichletBoundaryConditionDescriptor>(name, i_boundary_descriptor,
                                                                   *ResumingData::instance().functionSymbolId(rhs_id));
    break;
  }
  case IBoundaryConditionDescriptor::Type::dirichlet_vector: {
    const std::string name = iboundaryconditiondecriptor_group.getAttribute("name").read<std::string>();

    const std::vector function_id_list =
      iboundaryconditiondecriptor_group.getAttribute("function_id_list").read<std::vector<size_t>>();

    std::vector<FunctionSymbolId> function_symbol_id_list;
    for (auto function_id : function_id_list) {
      function_symbol_id_list.push_back(*ResumingData::instance().functionSymbolId(function_id));
    }

    bc_descriptor = std::make_shared<const DirichletVectorBoundaryConditionDescriptor>(name, i_boundary_descriptor,
                                                                                       function_symbol_id_list);
    break;
  }
    // LCOV_EXCL_START
  case IBoundaryConditionDescriptor::Type::external: {
    throw NotImplementedError("checkpoint/resume with sockets");

    break;
  }
    // LCOV_EXCL_STOP
  case IBoundaryConditionDescriptor::Type::fourier: {
    const std::string name = iboundaryconditiondecriptor_group.getAttribute("name").read<std::string>();
    const size_t rhs_id    = iboundaryconditiondecriptor_group.getAttribute("rhs_function_id").read<size_t>();
    const size_t mass_id   = iboundaryconditiondecriptor_group.getAttribute("mass_function_id").read<size_t>();

    bc_descriptor =
      std::make_shared<const FourierBoundaryConditionDescriptor>(name, i_boundary_descriptor,
                                                                 *ResumingData::instance().functionSymbolId(mass_id),
                                                                 *ResumingData::instance().functionSymbolId(rhs_id));
    break;
  }
  case IBoundaryConditionDescriptor::Type::fixed: {
    bc_descriptor = std::make_shared<const FixedBoundaryConditionDescriptor>(i_boundary_descriptor);
    break;
  }
  case IBoundaryConditionDescriptor::Type::free: {
    bc_descriptor = std::make_shared<const FreeBoundaryConditionDescriptor>(i_boundary_descriptor);
    break;
  }
  case IBoundaryConditionDescriptor::Type::inflow: {
    const size_t function_id = iboundaryconditiondecriptor_group.getAttribute("function_id").read<size_t>();

    bc_descriptor =
      std::make_shared<const InflowBoundaryConditionDescriptor>(i_boundary_descriptor,
                                                                *ResumingData::instance().functionSymbolId(
                                                                  function_id));
    break;
  }
  case IBoundaryConditionDescriptor::Type::inflow_list: {
    const std::vector function_id_list =
      iboundaryconditiondecriptor_group.getAttribute("function_id_list").read<std::vector<size_t>>();

    std::vector<FunctionSymbolId> function_symbol_id_list;
    for (auto function_id : function_id_list) {
      function_symbol_id_list.push_back(*ResumingData::instance().functionSymbolId(function_id));
    }

    bc_descriptor =
      std::make_shared<const InflowListBoundaryConditionDescriptor>(i_boundary_descriptor, function_symbol_id_list);
    break;
  }
  case IBoundaryConditionDescriptor::Type::neumann: {
    const std::string name = iboundaryconditiondecriptor_group.getAttribute("name").read<std::string>();
    const size_t rhs_id    = iboundaryconditiondecriptor_group.getAttribute("rhs_function_id").read<size_t>();

    bc_descriptor =
      std::make_shared<const NeumannBoundaryConditionDescriptor>(name, i_boundary_descriptor,
                                                                 *ResumingData::instance().functionSymbolId(rhs_id));
    break;
  }
  case IBoundaryConditionDescriptor::Type::outflow: {
    bc_descriptor = std::make_shared<const OutflowBoundaryConditionDescriptor>(i_boundary_descriptor);
    break;
  }
  case IBoundaryConditionDescriptor::Type::symmetry: {
    bc_descriptor = std::make_shared<const SymmetryBoundaryConditionDescriptor>(i_boundary_descriptor);
    break;
  }
  case IBoundaryConditionDescriptor::Type::wall: {
    bc_descriptor = std::make_shared<const WallBoundaryConditionDescriptor>(i_boundary_descriptor);
    break;
  }
  }

  return bc_descriptor;
}

EmbeddedData
readIBoundaryConditionDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  std::shared_ptr<const IBoundaryConditionDescriptor> bc_descriptor =
    readIBoundaryConditionDescriptor(symbol_table_group.getGroup("embedded/" + symbol_name));
  return {std::make_shared<DataHandler<const IBoundaryConditionDescriptor>>(bc_descriptor)};
}

}   // namespace checkpointing
