#include <utils/checkpointing/ReadMesh.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/MeshVariant.hpp>
#include <utils/checkpointing/ItemTypeHFType.hpp>
#include <utils/checkpointing/ResumingData.hpp>

namespace checkpointing
{

EmbeddedData
readMesh(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group mesh_group = symbol_table_group.getGroup("embedded/" + symbol_name);

  const size_t mesh_id = mesh_group.getAttribute("id").read<uint64_t>();

  return {std::make_shared<DataHandler<const MeshVariant>>(ResumingData::instance().meshVariant(mesh_id))};
}

}   // namespace checkpointing
