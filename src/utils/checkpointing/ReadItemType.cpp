#include <utils/checkpointing/ReadItemType.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <utils/checkpointing/ItemTypeHFType.hpp>

namespace checkpointing
{

EmbeddedData
readItemType(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group item_type_group = symbol_table_group.getGroup("embedded/" + symbol_name);
  const ItemType item_type              = item_type_group.getAttribute("item_type").read<ItemType>();

  return {std::make_shared<DataHandler<const ItemType>>(std::make_shared<const ItemType>(item_type))};
}

}   // namespace checkpointing
