#ifndef READ_DISCRETE_FUNCTION_VARIANT_HPP
#define READ_DISCRETE_FUNCTION_VARIANT_HPP

#include <utils/HighFivePugsUtils.hpp>

class DiscreteFunctionVariant;
class EmbeddedData;

namespace checkpointing
{

std::shared_ptr<DiscreteFunctionVariant> readDiscreteFunctionVariant(const HighFive::Group& discrete_function_group);

EmbeddedData readDiscreteFunctionVariant(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_DISCRETE_FUNCTION_VARIANT_HPP
