#include <utils/checkpointing/WriteItemArrayVariant.hpp>

#include <language/modules/MeshModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/ItemArrayVariant.hpp>
#include <scheme/VariableBCDescriptor.hpp>
#include <utils/checkpointing/ItemTypeHFType.hpp>
#include <utils/checkpointing/WriteConnectivity.hpp>
#include <utils/checkpointing/WriteItemArray.hpp>

namespace checkpointing
{

void
writeItemArrayVariant(HighFive::Group& variable_group,
                      std::shared_ptr<const ItemArrayVariant> item_array_variant_v,
                      HighFive::File& file,
                      HighFive::Group& checkpoint_group)
{
  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(item_array_variant_v)>));

  std::visit(
    [&](auto&& item_array) {
      using ItemArrayT = std::decay_t<decltype(item_array)>;

      variable_group.createAttribute("item_type", ItemArrayT::item_t);
      using data_type = std::decay_t<typename ItemArrayT::data_type>;
      variable_group.createAttribute("data_type", dataTypeName(ast_node_data_type_from<data_type>));

      const IConnectivity& connectivity = *item_array.connectivity_ptr();
      variable_group.createAttribute("connectivity_id", connectivity.id());
      writeConnectivity(connectivity, file, checkpoint_group);

      write(variable_group, "arrays", item_array);
    },
    item_array_variant_v->itemArray());
}

void
writeItemArrayVariant(const std::string& symbol_name,
                      const EmbeddedData& embedded_data,
                      HighFive::File& file,
                      HighFive::Group& checkpoint_group,
                      HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const ItemArrayVariant> item_array_variant_p =
    dynamic_cast<const DataHandler<const ItemArrayVariant>&>(embedded_data.get()).data_ptr();

  writeItemArrayVariant(variable_group, item_array_variant_p, file, checkpoint_group);
}

}   // namespace checkpointing
