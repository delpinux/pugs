#include <utils/checkpointing/ReadItemValueVariant.hpp>

#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemValueVariant.hpp>
#include <utils/checkpointing/ItemTypeHFType.hpp>
#include <utils/checkpointing/ReadItemValue.hpp>
#include <utils/checkpointing/ResumingData.hpp>

namespace checkpointing
{

template <ItemType item_type>
std::shared_ptr<ItemValueVariant>
readItemValueVariant(const HighFive::Group& item_value_variant_group)
{
  const std::string data_type  = item_value_variant_group.getAttribute("data_type").read<std::string>();
  const size_t connectivity_id = item_value_variant_group.getAttribute("connectivity_id").read<size_t>();

  const IConnectivity& connectivity = *ResumingData::instance().iConnectivity(connectivity_id);

  std::shared_ptr<ItemValueVariant> p_item_value;

  if (data_type == dataTypeName(ast_node_data_type_from<bool>)) {
    p_item_value = std::make_shared<ItemValueVariant>(
      readItemValue<bool, item_type>(item_value_variant_group, "values", connectivity));
  } else if (data_type == dataTypeName(ast_node_data_type_from<long int>)) {
    p_item_value = std::make_shared<ItemValueVariant>(
      readItemValue<long int, item_type>(item_value_variant_group, "values", connectivity));
  } else if (data_type == dataTypeName(ast_node_data_type_from<unsigned long int>)) {
    p_item_value = std::make_shared<ItemValueVariant>(
      readItemValue<unsigned long int, item_type>(item_value_variant_group, "values", connectivity));
  } else if (data_type == dataTypeName(ast_node_data_type_from<double>)) {
    p_item_value = std::make_shared<ItemValueVariant>(
      readItemValue<double, item_type>(item_value_variant_group, "values", connectivity));
  } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<1>>)) {
    p_item_value = std::make_shared<ItemValueVariant>(
      readItemValue<TinyVector<1>, item_type>(item_value_variant_group, "values", connectivity));
  } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<2>>)) {
    p_item_value = std::make_shared<ItemValueVariant>(
      readItemValue<TinyVector<2>, item_type>(item_value_variant_group, "values", connectivity));
  } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<3>>)) {
    p_item_value = std::make_shared<ItemValueVariant>(
      readItemValue<TinyVector<3>, item_type>(item_value_variant_group, "values", connectivity));
  } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<1>>)) {
    p_item_value = std::make_shared<ItemValueVariant>(
      readItemValue<TinyMatrix<1>, item_type>(item_value_variant_group, "values", connectivity));
  } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<2>>)) {
    p_item_value = std::make_shared<ItemValueVariant>(
      readItemValue<TinyMatrix<2>, item_type>(item_value_variant_group, "values", connectivity));
  } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<3>>)) {
    p_item_value = std::make_shared<ItemValueVariant>(
      readItemValue<TinyMatrix<3>, item_type>(item_value_variant_group, "values", connectivity));
  } else {
    // LCOV_EXCL_START
    throw UnexpectedError("unexpected discrete function data type: " + data_type);
    // LCOV_EXCL_STOP
  }
  return p_item_value;
}

std::shared_ptr<ItemValueVariant>
readItemValueVariant(const HighFive::Group& item_value_variant_group)
{
  const ItemType item_type = item_value_variant_group.getAttribute("item_type").read<ItemType>();

  std::shared_ptr<ItemValueVariant> p_item_value;

  switch (item_type) {
  case ItemType::cell: {
    p_item_value = readItemValueVariant<ItemType::cell>(item_value_variant_group);
    break;
  }
  case ItemType::face: {
    p_item_value = readItemValueVariant<ItemType::face>(item_value_variant_group);
    break;
  }
  case ItemType::edge: {
    p_item_value = readItemValueVariant<ItemType::edge>(item_value_variant_group);
    break;
  }
  case ItemType::node: {
    p_item_value = readItemValueVariant<ItemType::node>(item_value_variant_group);
    break;
  }
  }

  return p_item_value;
}

EmbeddedData
readItemValueVariant(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group item_value_variant_group = symbol_table_group.getGroup("embedded/" + symbol_name);
  return {std::make_shared<DataHandler<const ItemValueVariant>>(readItemValueVariant(item_value_variant_group))};
}

}   // namespace checkpointing
