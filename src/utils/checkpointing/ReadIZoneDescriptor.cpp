#include <utils/checkpointing/ReadIZoneDescriptor.hpp>

#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/NamedZoneDescriptor.hpp>
#include <mesh/NumberedZoneDescriptor.hpp>
#include <utils/checkpointing/IZoneDescriptorHFType.hpp>

namespace checkpointing
{

EmbeddedData
readIZoneDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group izonedescriptor_group = symbol_table_group.getGroup("embedded/" + symbol_name);
  const IZoneDescriptor::Type izone_descriptor_type =
    izonedescriptor_group.getAttribute("izone_descriptor_type").read<IZoneDescriptor::Type>();

  std::shared_ptr<const IZoneDescriptor> izone_descriptor;

  switch (izone_descriptor_type) {
  case IZoneDescriptor::Type::named: {
    const std::string name = izonedescriptor_group.getAttribute("name").read<std::string>();
    izone_descriptor       = std::make_shared<const NamedZoneDescriptor>(name);
    break;
  }
  case IZoneDescriptor::Type::numbered: {
    const unsigned int number = izonedescriptor_group.getAttribute("number").read<unsigned int>();
    izone_descriptor          = std::make_shared<const NumberedZoneDescriptor>(number);
    break;
  }
  }

  return {std::make_shared<DataHandler<const IZoneDescriptor>>(izone_descriptor)};
}

}   // namespace checkpointing
