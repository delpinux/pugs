#ifndef REF_ITEM_LIST_HF_TYPE_HPP
#define REF_ITEM_LIST_HF_TYPE_HPP

#include <mesh/RefItemList.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<RefItemListBase::Type> PUGS_INLINE
create_enum_ref_item_list_type()
{
  return {{"boundary", RefItemListBase::Type::boundary},
          {"interface", RefItemListBase::Type::interface},
          {"set", RefItemListBase::Type::set},
          {"undefined", RefItemListBase::Type::undefined}};
}
HIGHFIVE_REGISTER_TYPE(RefItemListBase::Type, create_enum_ref_item_list_type)

#endif   // REF_ITEM_LIST_HF_TYPE_HPP
