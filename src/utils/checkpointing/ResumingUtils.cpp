#include <utils/checkpointing/ResumingUtils.hpp>

#include <utils/Exceptions.hpp>

#include <utils/pugs_config.hpp>

#ifdef PUGS_HAS_HDF5

#include <utils/HighFivePugsUtils.hpp>

std::string
resumingDatafile(const std::string& filename)
{
  HighFive::File file(filename, HighFive::File::ReadOnly);
  return file.getGroup("/resuming_checkpoint").getAttribute("data.pgs").read<std::string>();
}

#else   // PUGS_HAS_HDF5

std::string
resumingDatafile(const std::string&)
{
  throw NormalError("Resuming requires HDF5");
}

#endif   // PUGS_HAS_HDF5
