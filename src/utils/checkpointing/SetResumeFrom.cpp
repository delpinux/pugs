#include <utils/checkpointing/SetResumeFrom.hpp>

#include <rang.hpp>
#include <utils/pugs_config.hpp>

#include <iostream>

#ifdef PUGS_HAS_HDF5

#include <utils/Exceptions.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/Messenger.hpp>

void
setResumeFrom(const std::string& filename, const uint64_t& checkpoint_number, std::ostream& os)
{
  try {
    HighFive::SilenceHDF5 m_silence_hdf5{true};

    HighFive::FileAccessProps fapl;
    fapl.add(HighFive::MPIOFileAccess{parallel::Messenger::getInstance().comm(), MPI_INFO_NULL});
    fapl.add(HighFive::MPIOCollectiveMetadata{});

    HighFive::File file(filename, HighFive::File::ReadWrite, fapl);
    const std::string checkpoint_name = "checkpoint_" + std::to_string(checkpoint_number);

    if (not file.exist(checkpoint_name)) {
      std::ostringstream error_msg;
      error_msg << "cannot find checkpoint " << rang::fgB::magenta << checkpoint_number << rang::fg::reset << " in "
                << rang::fgB::yellow << filename << rang::fg::reset;
      throw NormalError(error_msg.str());
    }

    HighFive::Group checkpoint = file.getGroup(checkpoint_name);
    if (file.exist("resuming_checkpoint")) {
      file.unlink("resuming_checkpoint");
    }
    file.createHardLink("resuming_checkpoint", checkpoint);
    os << "Resuming checkpoint " << rang::style::bold << "successfully" << rang::style::reset << " set to "
       << rang::fgB::yellow << checkpoint_number << rang::fg::reset << '\n';
  }
  // LCOV_EXCL_START
  catch (HighFive::Exception& e) {
    throw NormalError(e.what());
  }
  // LCOV_EXCL_STOP
}

#else   // PUGS_HAS_HDF5

void
setResumeFrom(const std::string&, const uint64_t&, std::ostream&)
{
  std::cerr << rang::fgB::red << "error: " << rang::fg::reset << "setting resuming checkpoint requires HDF5\n";
}

#endif   // PUGS_HAS_HDF5
