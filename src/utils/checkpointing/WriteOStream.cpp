#include <utils/checkpointing/WriteOStream.hpp>

#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/OFStream.hpp>
#include <language/utils/OStream.hpp>
#include <utils/Exceptions.hpp>
#include <utils/checkpointing/OStreamTypeHFType.hpp>

namespace checkpointing
{

void
writeOStream(const std::string& symbol_name,
             const EmbeddedData& embedded_data,
             HighFive::File&,
             HighFive::Group&,
             HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const OStream> ostream_p =
    dynamic_cast<const DataHandler<const OStream>&>(embedded_data.get()).data_ptr();

  const OStream& ostream = *ostream_p;

  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(ostream_p)>));
  variable_group.createAttribute("ostream_type", ostream.type());

  switch (ostream.type()) {
  case OStream::Type::std_ofstream: {
    const OFStream& ofstream = dynamic_cast<const OFStream&>(ostream);
    variable_group.createAttribute("filename", ofstream.filename());
    break;
  }
  case OStream::Type::std_ostream: {
    throw NotImplementedError("std::ostream checkpoint");
  }
  }
}

}   // namespace checkpointing
