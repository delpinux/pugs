#ifndef READ_SUB_ITEM_ARRAY_PER_ITEM_VARIANT_HPP
#define READ_SUB_ITEM_ARRAY_PER_ITEM_VARIANT_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

EmbeddedData readSubItemArrayPerItemVariant(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_SUB_ITEM_ARRAY_PER_ITEM_VARIANT_HPP
