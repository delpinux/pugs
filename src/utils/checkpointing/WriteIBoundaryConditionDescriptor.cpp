#include <utils/checkpointing/WriteIBoundaryConditionDescriptor.hpp>

#include <language/modules/SchemeModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <scheme/AxisBoundaryConditionDescriptor.hpp>
#include <scheme/DirichletBoundaryConditionDescriptor.hpp>
#include <scheme/DirichletVectorBoundaryConditionDescriptor.hpp>
#include <scheme/FixedBoundaryConditionDescriptor.hpp>
#include <scheme/FourierBoundaryConditionDescriptor.hpp>
#include <scheme/FreeBoundaryConditionDescriptor.hpp>
#include <scheme/InflowBoundaryConditionDescriptor.hpp>
#include <scheme/InflowListBoundaryConditionDescriptor.hpp>
#include <scheme/NeumannBoundaryConditionDescriptor.hpp>
#include <scheme/OutflowBoundaryConditionDescriptor.hpp>
#include <scheme/SymmetryBoundaryConditionDescriptor.hpp>
#include <scheme/WallBoundaryConditionDescriptor.hpp>
#include <utils/Exceptions.hpp>
#include <utils/checkpointing/IBoundaryConditionDescriptorHFType.hpp>
#include <utils/checkpointing/WriteIBoundaryDescriptor.hpp>

namespace checkpointing
{

void
writeIBoundaryConditionDescriptor(HighFive::Group& variable_group,
                                  std::shared_ptr<const IBoundaryConditionDescriptor> iboundary_condition_descriptor_p)
{
  const IBoundaryConditionDescriptor& iboundary_condition_descriptor = *iboundary_condition_descriptor_p;

  variable_group.createAttribute("type",
                                 dataTypeName(ast_node_data_type_from<decltype(iboundary_condition_descriptor_p)>));
  variable_group.createAttribute("iboundary_condition_descriptor_type", iboundary_condition_descriptor.type());

  HighFive::Group boundary_group = variable_group.createGroup("boundary");

  switch (iboundary_condition_descriptor.type()) {
  case IBoundaryConditionDescriptor::Type::axis: {
    const AxisBoundaryConditionDescriptor& axis_bc_descriptor =
      dynamic_cast<const AxisBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    writeIBoundaryDescriptor(boundary_group, axis_bc_descriptor.boundaryDescriptor());
    break;
  }
  case IBoundaryConditionDescriptor::Type::dirichlet: {
    const DirichletBoundaryConditionDescriptor& dirichlet_bc_descriptor =
      dynamic_cast<const DirichletBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    writeIBoundaryDescriptor(boundary_group, dirichlet_bc_descriptor.boundaryDescriptor());
    variable_group.createAttribute("name", dirichlet_bc_descriptor.name());
    variable_group.createAttribute("rhs_function_id", dirichlet_bc_descriptor.rhsSymbolId().id());
    break;
  }
  case IBoundaryConditionDescriptor::Type::dirichlet_vector: {
    const DirichletVectorBoundaryConditionDescriptor& dirichlet_vector_bc_descriptor =
      dynamic_cast<const DirichletVectorBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    variable_group.createAttribute("name", dirichlet_vector_bc_descriptor.name());
    writeIBoundaryDescriptor(boundary_group, dirichlet_vector_bc_descriptor.boundaryDescriptor());
    std::vector<size_t> function_id_list;
    for (auto&& function_symbol_id : dirichlet_vector_bc_descriptor.rhsSymbolIdList()) {
      function_id_list.push_back(function_symbol_id.id());
    }
    variable_group.createAttribute("function_id_list", function_id_list);
    break;
  }
    // LCOV_EXCL_START
  case IBoundaryConditionDescriptor::Type::external: {
    throw NotImplementedError("checkpoint/resume with sockets");

    break;
  }
    // LCOV_EXCL_STOP
  case IBoundaryConditionDescriptor::Type::fourier: {
    const FourierBoundaryConditionDescriptor& fourier_bc_descriptor =
      dynamic_cast<const FourierBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    writeIBoundaryDescriptor(boundary_group, fourier_bc_descriptor.boundaryDescriptor());
    variable_group.createAttribute("name", fourier_bc_descriptor.name());
    variable_group.createAttribute("rhs_function_id", fourier_bc_descriptor.rhsSymbolId().id());
    variable_group.createAttribute("mass_function_id", fourier_bc_descriptor.massSymbolId().id());
    break;
  }
  case IBoundaryConditionDescriptor::Type::fixed: {
    const FixedBoundaryConditionDescriptor& fixed_bc_descriptor =
      dynamic_cast<const FixedBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    writeIBoundaryDescriptor(boundary_group, fixed_bc_descriptor.boundaryDescriptor());
    break;
  }
  case IBoundaryConditionDescriptor::Type::free: {
    const FreeBoundaryConditionDescriptor& free_bc_descriptor =
      dynamic_cast<const FreeBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    writeIBoundaryDescriptor(boundary_group, free_bc_descriptor.boundaryDescriptor());
    break;
  }
  case IBoundaryConditionDescriptor::Type::inflow: {
    const InflowBoundaryConditionDescriptor& inflow_bc_descriptor =
      dynamic_cast<const InflowBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    writeIBoundaryDescriptor(boundary_group, inflow_bc_descriptor.boundaryDescriptor());
    variable_group.createAttribute("function_id", inflow_bc_descriptor.functionSymbolId().id());
    break;
  }
  case IBoundaryConditionDescriptor::Type::inflow_list: {
    const InflowListBoundaryConditionDescriptor& inflow_list_bc_descriptor =
      dynamic_cast<const InflowListBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    writeIBoundaryDescriptor(boundary_group, inflow_list_bc_descriptor.boundaryDescriptor());
    std::vector<size_t> function_id_list;
    for (auto&& function_symbol_id : inflow_list_bc_descriptor.functionSymbolIdList()) {
      function_id_list.push_back(function_symbol_id.id());
    }
    variable_group.createAttribute("function_id_list", function_id_list);
    break;
  }
  case IBoundaryConditionDescriptor::Type::neumann: {
    const NeumannBoundaryConditionDescriptor& neumann_bc_descriptor =
      dynamic_cast<const NeumannBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    writeIBoundaryDescriptor(boundary_group, neumann_bc_descriptor.boundaryDescriptor());
    variable_group.createAttribute("name", neumann_bc_descriptor.name());
    variable_group.createAttribute("rhs_function_id", neumann_bc_descriptor.rhsSymbolId().id());
    break;
  }
  case IBoundaryConditionDescriptor::Type::outflow: {
    const OutflowBoundaryConditionDescriptor& outflow_bc_descriptor =
      dynamic_cast<const OutflowBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    writeIBoundaryDescriptor(boundary_group, outflow_bc_descriptor.boundaryDescriptor());
    break;
  }
  case IBoundaryConditionDescriptor::Type::symmetry: {
    const SymmetryBoundaryConditionDescriptor& symmetric_bc_descriptor =
      dynamic_cast<const SymmetryBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    writeIBoundaryDescriptor(boundary_group, symmetric_bc_descriptor.boundaryDescriptor());
    break;
  }
  case IBoundaryConditionDescriptor::Type::wall: {
    const WallBoundaryConditionDescriptor& wall_bc_descriptor =
      dynamic_cast<const WallBoundaryConditionDescriptor&>(iboundary_condition_descriptor);
    writeIBoundaryDescriptor(boundary_group, wall_bc_descriptor.boundaryDescriptor());
    break;
  }
  }
}

void
writeIBoundaryConditionDescriptor(const std::string& symbol_name,
                                  const EmbeddedData& embedded_data,
                                  HighFive::File&,
                                  HighFive::Group&,
                                  HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const IBoundaryConditionDescriptor> iboundary_condition_descriptor_p =
    dynamic_cast<const DataHandler<const IBoundaryConditionDescriptor>&>(embedded_data.get()).data_ptr();

  writeIBoundaryConditionDescriptor(variable_group, iboundary_condition_descriptor_p);
}

}   // namespace checkpointing
