#ifndef QUADRATURE_TYPE_HF_TYPE_HPP
#define QUADRATURE_TYPE_HF_TYPE_HPP

#include <analysis/QuadratureType.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<QuadratureType> PUGS_INLINE
create_enum_quadrature_descriptor_type()
{
  return {{"gauss", QuadratureType::Gauss},
          {"gauss-legendre", QuadratureType::GaussLegendre},
          {"gauss-lobatto", QuadratureType::GaussLobatto}};
}
HIGHFIVE_REGISTER_TYPE(QuadratureType, create_enum_quadrature_descriptor_type)

#endif   // QUADRATURE_TYPE_HF_TYPE_HPP
