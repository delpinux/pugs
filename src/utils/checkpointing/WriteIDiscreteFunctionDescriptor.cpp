#include <utils/checkpointing/WriteIDiscreteFunctionDescriptor.hpp>

#include <language/modules/SchemeModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <scheme/IDiscreteFunctionDescriptor.hpp>
#include <utils/checkpointing/DiscreteFunctionTypeHFType.hpp>

namespace checkpointing
{

void
writeIDiscreteFunctionDescriptor(const std::string& symbol_name,
                                 const EmbeddedData& embedded_data,
                                 HighFive::File&,
                                 HighFive::Group&,
                                 HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const IDiscreteFunctionDescriptor> idiscrete_function_desriptor_p =
    dynamic_cast<const DataHandler<const IDiscreteFunctionDescriptor>&>(embedded_data.get()).data_ptr();

  const IDiscreteFunctionDescriptor& idiscrete_function_descriptor = *idiscrete_function_desriptor_p;

  variable_group.createAttribute("type",
                                 dataTypeName(ast_node_data_type_from<decltype(idiscrete_function_desriptor_p)>));
  variable_group.createAttribute("discrete_function_type", idiscrete_function_descriptor.type());
}

}   // namespace checkpointing
