#include <utils/checkpointing/ReadIQuadratureDescriptor.hpp>

#include <analysis/GaussLegendreQuadratureDescriptor.hpp>
#include <analysis/GaussLobattoQuadratureDescriptor.hpp>
#include <analysis/GaussQuadratureDescriptor.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <utils/checkpointing/QuadratureTypeHFType.hpp>

namespace checkpointing
{

EmbeddedData
readIQuadratureDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group iquadraturedescriptor_group = symbol_table_group.getGroup("embedded/" + symbol_name);
  const QuadratureType quadrature_type =
    iquadraturedescriptor_group.getAttribute("quadrature_type").read<QuadratureType>();
  const size_t degree = iquadraturedescriptor_group.getAttribute("quadrature_degree").read<size_t>();

  std::shared_ptr<const IQuadratureDescriptor> iquadrature_descrptor;

  switch (quadrature_type) {
  case QuadratureType::Gauss: {
    iquadrature_descrptor = std::make_shared<const GaussQuadratureDescriptor>(degree);
    break;
  }
  case QuadratureType::GaussLegendre: {
    iquadrature_descrptor = std::make_shared<const GaussLegendreQuadratureDescriptor>(degree);
    break;
  }
  case QuadratureType::GaussLobatto: {
    iquadrature_descrptor = std::make_shared<const GaussLobattoQuadratureDescriptor>(degree);
    break;
  }
  }

  return {std::make_shared<DataHandler<const IQuadratureDescriptor>>(iquadrature_descrptor)};
}

}   // namespace checkpointing
