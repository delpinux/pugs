#ifndef WRITE_IZONE_DESCRIPTOR_HPP
#define WRITE_IZONE_DESCRIPTOR_HPP

#include <utils/HighFivePugsUtils.hpp>
class EmbeddedData;

namespace checkpointing
{

void writeIZoneDescriptor(const std::string& symbol_name,
                          const EmbeddedData& embedded_data,
                          HighFive::File&,
                          HighFive::Group&,
                          HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // WRITE_IZONE_DESCRIPTOR_HPP
