#ifndef PRINT_CHECKPOINT_INFO_HPP
#define PRINT_CHECKPOINT_INFO_HPP

#include <iostream>
#include <string>

void printCheckpointInfo(const std::string& filename, std::ostream& os = std::cout);

#endif   // PRINT_CHECKPOINT_INFO_HPP
