#ifndef DISCRETE_FUNCTION_TYPE_HF_TYPE_HPP
#define DISCRETE_FUNCTION_TYPE_HF_TYPE_HPP

#include <scheme/DiscreteFunctionType.hpp>
#include <utils/HighFivePugsUtils.hpp>

HighFive::EnumType<DiscreteFunctionType> PUGS_INLINE
create_enum_discrete_function_type()
{
  return {{"P0", DiscreteFunctionType::P0}, {"P0Vector", DiscreteFunctionType::P0Vector}};
}
HIGHFIVE_REGISTER_TYPE(DiscreteFunctionType, create_enum_discrete_function_type)

#endif   // DISCRETE_FUNCTION_TYPE_HF_TYPE_HPP
