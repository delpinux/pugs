#ifndef READ_ITEM_ARRAY_VARIANT_HPP
#define READ_ITEM_ARRAY_VARIANT_HPP

#include <mesh/ItemArrayVariant.hpp>
#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

std::shared_ptr<ItemArrayVariant> readItemArrayVariant(const HighFive::Group& item_array_variant_group);

EmbeddedData readItemArrayVariant(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_ITEM_ARRAY_VARIANT_HPP
