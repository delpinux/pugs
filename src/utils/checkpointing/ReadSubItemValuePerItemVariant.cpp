#include <utils/checkpointing/ReadSubItemValuePerItemVariant.hpp>

#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/SubItemValuePerItemVariant.hpp>
#include <utils/checkpointing/ItemTypeHFType.hpp>
#include <utils/checkpointing/ReadArray.hpp>
#include <utils/checkpointing/ResumingData.hpp>

namespace checkpointing
{

template <typename T, ItemType item_type, ItemType sub_item_type>
SubItemValuePerItem<T, ItemOfItemType<sub_item_type, item_type>>
readSubItemValuePerItem(const HighFive::Group& group, const std::string& name, const IConnectivity& connectivity)
{
  return {connectivity, readArray<T>(group, name)};
}

template <ItemType item_type, ItemType sub_item_type>
std::shared_ptr<SubItemValuePerItemVariant>
readSubItemValuePerItemVariant(const HighFive::Group& sub_item_value_per_item_variant_group)
{
  if constexpr (item_type != sub_item_type) {
    const std::string data_type  = sub_item_value_per_item_variant_group.getAttribute("data_type").read<std::string>();
    const size_t connectivity_id = sub_item_value_per_item_variant_group.getAttribute("connectivity_id").read<size_t>();

    const IConnectivity& connectivity = *ResumingData::instance().iConnectivity(connectivity_id);

    std::shared_ptr<SubItemValuePerItemVariant> p_sub_item_value_per_item_variant;

    if (data_type == dataTypeName(ast_node_data_type_from<bool>)) {
      p_sub_item_value_per_item_variant = std::make_shared<SubItemValuePerItemVariant>(
        readSubItemValuePerItem<bool, item_type, sub_item_type>(sub_item_value_per_item_variant_group, "values",
                                                                connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<long int>)) {
      p_sub_item_value_per_item_variant = std::make_shared<SubItemValuePerItemVariant>(
        readSubItemValuePerItem<long int, item_type, sub_item_type>(sub_item_value_per_item_variant_group, "values",
                                                                    connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<unsigned long int>)) {
      p_sub_item_value_per_item_variant = std::make_shared<SubItemValuePerItemVariant>(
        readSubItemValuePerItem<unsigned long int, item_type, sub_item_type>(sub_item_value_per_item_variant_group,
                                                                             "values", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<double>)) {
      p_sub_item_value_per_item_variant = std::make_shared<SubItemValuePerItemVariant>(
        readSubItemValuePerItem<double, item_type, sub_item_type>(sub_item_value_per_item_variant_group, "values",
                                                                  connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<1>>)) {
      p_sub_item_value_per_item_variant = std::make_shared<SubItemValuePerItemVariant>(
        readSubItemValuePerItem<TinyVector<1>, item_type, sub_item_type>(sub_item_value_per_item_variant_group,
                                                                         "values", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<2>>)) {
      p_sub_item_value_per_item_variant = std::make_shared<SubItemValuePerItemVariant>(
        readSubItemValuePerItem<TinyVector<2>, item_type, sub_item_type>(sub_item_value_per_item_variant_group,
                                                                         "values", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyVector<3>>)) {
      p_sub_item_value_per_item_variant = std::make_shared<SubItemValuePerItemVariant>(
        readSubItemValuePerItem<TinyVector<3>, item_type, sub_item_type>(sub_item_value_per_item_variant_group,
                                                                         "values", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<1>>)) {
      p_sub_item_value_per_item_variant = std::make_shared<SubItemValuePerItemVariant>(
        readSubItemValuePerItem<TinyMatrix<1>, item_type, sub_item_type>(sub_item_value_per_item_variant_group,
                                                                         "values", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<2>>)) {
      p_sub_item_value_per_item_variant = std::make_shared<SubItemValuePerItemVariant>(
        readSubItemValuePerItem<TinyMatrix<2>, item_type, sub_item_type>(sub_item_value_per_item_variant_group,
                                                                         "values", connectivity));
    } else if (data_type == dataTypeName(ast_node_data_type_from<TinyMatrix<3>>)) {
      p_sub_item_value_per_item_variant = std::make_shared<SubItemValuePerItemVariant>(
        readSubItemValuePerItem<TinyMatrix<3>, item_type, sub_item_type>(sub_item_value_per_item_variant_group,
                                                                         "values", connectivity));
    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("unexpected discrete function data type: " + data_type);
      // LCOV_EXCL_STOP
    }
    return p_sub_item_value_per_item_variant;
  } else {
    // LCOV_EXCL_START
    throw UnexpectedError("item_type and sub_item_type must be different");
    // LCOV_EXCL_STOP
  }
}

template <ItemType item_type>
std::shared_ptr<SubItemValuePerItemVariant>
readSubItemValuePerItemVariant(const HighFive::Group& sub_item_value_per_item_variant_group)
{
  const ItemType sub_item_type = sub_item_value_per_item_variant_group.getAttribute("sub_item_type").read<ItemType>();

  std::shared_ptr<SubItemValuePerItemVariant> p_sub_item_value_per_item_variant;

  switch (sub_item_type) {
  case ItemType::cell: {
    p_sub_item_value_per_item_variant =
      readSubItemValuePerItemVariant<item_type, ItemType::cell>(sub_item_value_per_item_variant_group);
    break;
  }
  case ItemType::face: {
    p_sub_item_value_per_item_variant =
      readSubItemValuePerItemVariant<item_type, ItemType::face>(sub_item_value_per_item_variant_group);
    break;
  }
  case ItemType::edge: {
    p_sub_item_value_per_item_variant =
      readSubItemValuePerItemVariant<item_type, ItemType::edge>(sub_item_value_per_item_variant_group);
    break;
  }
  case ItemType::node: {
    p_sub_item_value_per_item_variant =
      readSubItemValuePerItemVariant<item_type, ItemType::node>(sub_item_value_per_item_variant_group);
    break;
  }
  }

  return p_sub_item_value_per_item_variant;
}

std::shared_ptr<SubItemValuePerItemVariant>
readSubItemValuePerItemVariant(const HighFive::Group& sub_item_value_per_item_variant_group)
{
  const ItemType item_type = sub_item_value_per_item_variant_group.getAttribute("item_type").read<ItemType>();

  std::shared_ptr<SubItemValuePerItemVariant> p_sub_item_value_per_item_variant;

  switch (item_type) {
  case ItemType::cell: {
    p_sub_item_value_per_item_variant =
      readSubItemValuePerItemVariant<ItemType::cell>(sub_item_value_per_item_variant_group);
    break;
  }
  case ItemType::face: {
    p_sub_item_value_per_item_variant =
      readSubItemValuePerItemVariant<ItemType::face>(sub_item_value_per_item_variant_group);
    break;
  }
  case ItemType::edge: {
    p_sub_item_value_per_item_variant =
      readSubItemValuePerItemVariant<ItemType::edge>(sub_item_value_per_item_variant_group);
    break;
  }
  case ItemType::node: {
    p_sub_item_value_per_item_variant =
      readSubItemValuePerItemVariant<ItemType::node>(sub_item_value_per_item_variant_group);
    break;
  }
  }

  return p_sub_item_value_per_item_variant;
}

EmbeddedData
readSubItemValuePerItemVariant(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group sub_item_value_per_item_variant_group = symbol_table_group.getGroup("embedded/" + symbol_name);
  return {std::make_shared<DataHandler<const SubItemValuePerItemVariant>>(
    readSubItemValuePerItemVariant(sub_item_value_per_item_variant_group))};
}

}   // namespace checkpointing
