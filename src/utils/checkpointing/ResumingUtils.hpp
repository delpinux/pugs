#ifndef RESUMING_UTILS_HPP
#define RESUMING_UTILS_HPP

#include <string>

std::string resumingDatafile(const std::string& filename);

#endif   // RESUMING_UTILS_HPP
