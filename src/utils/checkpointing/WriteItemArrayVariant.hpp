#ifndef WRITE_ITEM_ARRAY_VARIANT_HPP
#define WRITE_ITEM_ARRAY_VARIANT_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;
class ItemArrayVariant;

namespace checkpointing
{

void writeItemArrayVariant(HighFive::Group& variable_group,
                           std::shared_ptr<const ItemArrayVariant> item_array_variant_v,
                           HighFive::File& file,
                           HighFive::Group& checkpoint_group);

void writeItemArrayVariant(const std::string& symbol_name,
                           const EmbeddedData& embedded_data,
                           HighFive::File& file,
                           HighFive::Group& checkpoint_group,
                           HighFive::Group& symbol_table_group);
}   // namespace checkpointing

#endif   // WRITE_ITEM_ARRAY_VARIANT_HPP
