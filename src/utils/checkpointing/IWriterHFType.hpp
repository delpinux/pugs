#ifndef I_WRITER_HF_TYPE_HPP
#define I_WRITER_HF_TYPE_HPP

#include <output/IWriter.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<IWriter::Type> PUGS_INLINE
create_enum_i_writer_type()
{
  return {{"gnuplot", IWriter::Type::gnuplot},
          {"gnuplot_1d", IWriter::Type::gnuplot_1d},
          {"gnuplot_raw", IWriter::Type::gnuplot_raw},
          {"vtk", IWriter::Type::vtk}};
}
HIGHFIVE_REGISTER_TYPE(IWriter::Type, create_enum_i_writer_type)

#endif   // I_WRITER_HF_TYPE_HPP
