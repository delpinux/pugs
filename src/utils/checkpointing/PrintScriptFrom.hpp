#ifndef PRINT_SCRIPT_FROM_HPP
#define PRINT_SCRIPT_FROM_HPP

#include <cstdint>
#include <iostream>
#include <string>

void printScriptFrom(const std::string& filename, const uint64_t& checkpoint_number, std::ostream& os = std::cout);

#endif   // PRINT_SCRIPT_FROM_HPP
