#ifndef PARALLEL_CHECKER_HF_TYPE_HPP
#define PARALLEL_CHECKER_HF_TYPE_HPP

#include <dev/ParallelChecker.hpp>
#include <utils/HighFivePugsUtils.hpp>

HighFive::EnumType<ParallelChecker::Mode> PUGS_INLINE
create_enum_ParallelChecker_mode()
{
  return {{"automatic", ParallelChecker::Mode::automatic},
          {"read", ParallelChecker::Mode::read},
          {"write", ParallelChecker::Mode::write}};
}
HIGHFIVE_REGISTER_TYPE(ParallelChecker::Mode, create_enum_ParallelChecker_mode)

#endif   // PARALLEL_CHECKER_HF_TYPE_HPP
