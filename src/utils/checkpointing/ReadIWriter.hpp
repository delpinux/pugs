#ifndef READ_IWRITER_HPP
#define READ_IWRITER_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

EmbeddedData readIWriter(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_IWRITER_HPP
