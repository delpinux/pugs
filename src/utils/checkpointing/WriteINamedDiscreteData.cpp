#include <utils/checkpointing/WriteINamedDiscreteData.hpp>

#include <language/modules/WriterModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <output/INamedDiscreteData.hpp>
#include <output/NamedDiscreteFunction.hpp>
#include <output/NamedItemArrayVariant.hpp>
#include <output/NamedItemValueVariant.hpp>
#include <utils/checkpointing/INamedDiscreteDataHFType.hpp>
#include <utils/checkpointing/WriteDiscreteFunctionVariant.hpp>
#include <utils/checkpointing/WriteItemArrayVariant.hpp>
#include <utils/checkpointing/WriteItemValueVariant.hpp>

namespace checkpointing
{

void
writeINamedDiscreteData(const std::string& symbol_name,
                        const EmbeddedData& embedded_data,
                        HighFive::File& file,
                        HighFive::Group& checkpoint_group,
                        HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const INamedDiscreteData> inamed_discrete_data_p =
    dynamic_cast<const DataHandler<const INamedDiscreteData>&>(embedded_data.get()).data_ptr();

  const INamedDiscreteData& inamed_discrete_data = *inamed_discrete_data_p;

  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(inamed_discrete_data_p)>));
  variable_group.createAttribute("named_discrete_data_type", inamed_discrete_data.type());
  variable_group.createAttribute("named_discrete_data_name", inamed_discrete_data.name());

  switch (inamed_discrete_data.type()) {
  case INamedDiscreteData::Type::discrete_function: {
    const NamedDiscreteFunction& named_discrete_function =
      dynamic_cast<const NamedDiscreteFunction&>(inamed_discrete_data);

    HighFive::Group discrete_function_group = variable_group.createGroup("discrete_function_variant");
    writeDiscreteFunctionVariant(discrete_function_group, named_discrete_function.discreteFunctionVariant(), file,
                                 checkpoint_group);
    break;
  }
  case INamedDiscreteData::Type::item_array: {
    const NamedItemArrayVariant& named_item_array_v = dynamic_cast<const NamedItemArrayVariant&>(inamed_discrete_data);

    HighFive::Group item_array_group = variable_group.createGroup("item_array_variant");

    writeItemArrayVariant(item_array_group, named_item_array_v.itemArrayVariant(), file, checkpoint_group);
    break;
  }
  case INamedDiscreteData::Type::item_value: {
    const NamedItemValueVariant& named_item_value_v = dynamic_cast<const NamedItemValueVariant&>(inamed_discrete_data);

    HighFive::Group item_value_group = variable_group.createGroup("item_value_variant");

    writeItemValueVariant(item_value_group, named_item_value_v.itemValueVariant(), file, checkpoint_group);
    break;
  }
  }
}

}   // namespace checkpointing
