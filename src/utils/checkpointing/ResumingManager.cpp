#include <utils/checkpointing/ResumingManager.hpp>

#include <utils/HighFivePugsUtils.hpp>

ResumingManager* ResumingManager::m_instance = nullptr;

ResumingManager&
ResumingManager::getInstance()
{
  Assert(m_instance != nullptr, "instance was not created");
  return *m_instance;
}

void
ResumingManager::create()
{
  Assert(m_instance == nullptr, "Resuming manager was already created");
  m_instance = new ResumingManager;
}

void
ResumingManager::destroy()
{
  Assert(m_instance != nullptr, "Resuming manager was not created");
  delete m_instance;
  m_instance = nullptr;
}

size_t
ResumingManager::checkpointId()
{
#ifdef PUGS_HAS_HDF5
  if (not m_checkpoint_id) {
    HighFive::File file(m_filename, HighFive::File::ReadOnly);

    HighFive::Group checkpoint = file.getGroup("/resuming_checkpoint");

    m_checkpoint_id = std::make_unique<size_t>(checkpoint.getAttribute("id").read<uint64_t>());
  }
#else    // PUGS_HAS_HDF5
  m_checkpoint_id = std::make_unique<uint64_t>(0);
#endif   //  PUGS_HAS_HDF5
  return *m_checkpoint_id;
}
