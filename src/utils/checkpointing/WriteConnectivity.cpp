#include <utils/checkpointing/WriteConnectivity.hpp>

#include <mesh/Connectivity.hpp>
#include <utils/checkpointing/RefItemListHFType.hpp>
#include <utils/checkpointing/WriteArray.hpp>
#include <utils/checkpointing/WriteItemValue.hpp>

#include <iomanip>

namespace checkpointing
{

template <ItemType item_type, size_t Dimension>
void
writeRefItemList(const Connectivity<Dimension>& connectivity, HighFive::Group& connectivity_group)
{
  for (size_t i_item_list = 0; i_item_list < connectivity.template numberOfRefItemList<item_type>(); ++i_item_list) {
    auto ref_item_list = connectivity.template refItemList<item_type>(i_item_list);

    std::ostringstream ref_item_list_group_name;
    ref_item_list_group_name << "item_ref_list/" << itemName(item_type) << '/' << std::setfill('0')
                             << std::setw(
                                  std::to_string(connectivity.template numberOfRefItemList<item_type>()).length())
                             << i_item_list;
    HighFive::Group ref_item_list_group = connectivity_group.createGroup(ref_item_list_group_name.str());
    ref_item_list_group.createAttribute("tag_name", ref_item_list.refId().tagName());
    ref_item_list_group.createAttribute("tag_number", ref_item_list.refId().tagNumber());
    ref_item_list_group.createAttribute("type", ref_item_list.type());

    write(ref_item_list_group, "list", ref_item_list.list());
  }
}

template <size_t Dimension>
void
writeConnectivity(const Connectivity<Dimension>& connectivity, HighFive::File& file, HighFive::Group& checkpoint_group)
{
  std::string connectivity_group_name = "connectivity/" + std::to_string(connectivity.id());
  if (not checkpoint_group.exist(connectivity_group_name)) {
    bool linked = false;
    for (auto group_name : file.listObjectNames()) {
      const std::string stored_connectivity_group_name = group_name + "/" + connectivity_group_name;
      if (file.exist(stored_connectivity_group_name)) {
        HighFive::Group stored_connectivity_group = file.getGroup(stored_connectivity_group_name);

        const std::string type_name = stored_connectivity_group.getAttribute("type").read<std::string>();
        if (type_name != "dual_connectivity") {
          checkpoint_group.createHardLink(connectivity_group_name, file.getGroup(stored_connectivity_group_name));
          linked = true;
          break;
        }
      }
    }

    if (not linked) {
      HighFive::Group connectivity_group = checkpoint_group.createGroup(connectivity_group_name);

      connectivity_group.createAttribute("dimension", connectivity.dimension());
      connectivity_group.createAttribute("id", connectivity.id());
      connectivity_group.createAttribute("type", std::string{"unstructured"});

      write(connectivity_group, "cell_to_node_matrix_values",
            connectivity.getMatrix(ItemType::cell, ItemType::node).values());
      write(connectivity_group, "cell_to_node_matrix_rowsMap",
            connectivity.getMatrix(ItemType::cell, ItemType::node).rowsMap());

      if constexpr (Dimension > 1) {
        write(connectivity_group, "cell_to_face_matrix_values",
              connectivity.getMatrix(ItemType::cell, ItemType::face).values());
        write(connectivity_group, "cell_to_face_matrix_rowsMap",
              connectivity.getMatrix(ItemType::cell, ItemType::face).rowsMap());

        write(connectivity_group, "face_to_node_matrix_values",
              connectivity.getMatrix(ItemType::face, ItemType::node).values());
        write(connectivity_group, "face_to_node_matrix_rowsMap",
              connectivity.getMatrix(ItemType::face, ItemType::node).rowsMap());

        write(connectivity_group, "node_to_face_matrix_values",
              connectivity.getMatrix(ItemType::node, ItemType::face).values());
        write(connectivity_group, "node_to_face_matrix_rowsMap",
              connectivity.getMatrix(ItemType::node, ItemType::face).rowsMap());

        write(connectivity_group, "cell_face_is_reversed", connectivity.cellFaceIsReversed().arrayView());
      }

      if constexpr (Dimension > 2) {
        write(connectivity_group, "cell_to_edge_matrix_values",
              connectivity.getMatrix(ItemType::cell, ItemType::edge).values());
        write(connectivity_group, "cell_to_edge_matrix_rowsMap",
              connectivity.getMatrix(ItemType::cell, ItemType::edge).rowsMap());

        write(connectivity_group, "face_to_edge_matrix_values",
              connectivity.getMatrix(ItemType::face, ItemType::edge).values());
        write(connectivity_group, "face_to_edge_matrix_rowsMap",
              connectivity.getMatrix(ItemType::face, ItemType::edge).rowsMap());

        write(connectivity_group, "edge_to_node_matrix_values",
              connectivity.getMatrix(ItemType::edge, ItemType::node).values());
        write(connectivity_group, "edge_to_node_matrix_rowsMap",
              connectivity.getMatrix(ItemType::edge, ItemType::node).rowsMap());

        write(connectivity_group, "node_to_edge_matrix_values",
              connectivity.getMatrix(ItemType::node, ItemType::edge).values());
        write(connectivity_group, "node_to_edge_matrix_rowsMap",
              connectivity.getMatrix(ItemType::node, ItemType::edge).rowsMap());

        write(connectivity_group, "face_edge_is_reversed", connectivity.faceEdgeIsReversed().arrayView());
      }

      write(connectivity_group, "cell_type", connectivity.cellType());

      write(connectivity_group, "cell_numbers", connectivity.cellNumber());
      write(connectivity_group, "node_numbers", connectivity.nodeNumber());

      write(connectivity_group, "cell_owner", connectivity.cellOwner());
      write(connectivity_group, "node_owner", connectivity.nodeOwner());

      if constexpr (Dimension > 1) {
        write(connectivity_group, "face_numbers", connectivity.faceNumber());

        write(connectivity_group, "face_owner", connectivity.faceOwner());
      }
      if constexpr (Dimension > 2) {
        write(connectivity_group, "edge_numbers", connectivity.edgeNumber());

        write(connectivity_group, "edge_owner", connectivity.edgeOwner());
      }

      writeRefItemList<ItemType::cell>(connectivity, connectivity_group);
      writeRefItemList<ItemType::face>(connectivity, connectivity_group);
      writeRefItemList<ItemType::edge>(connectivity, connectivity_group);
      writeRefItemList<ItemType::node>(connectivity, connectivity_group);
    }
  }
}

void
writeConnectivity(const IConnectivity& connectivity, HighFive::File& file, HighFive::Group& checkpoint_group)
{
  switch (connectivity.dimension()) {
  case 1: {
    writeConnectivity(dynamic_cast<const Connectivity<1>&>(connectivity), file, checkpoint_group);
    break;
  }
  case 2: {
    writeConnectivity(dynamic_cast<const Connectivity<2>&>(connectivity), file, checkpoint_group);
    break;
  }
  case 3: {
    writeConnectivity(dynamic_cast<const Connectivity<3>&>(connectivity), file, checkpoint_group);
    break;
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("invalid connectivity dimension");
  }
    // LCOV_EXCL_STOP
  }
}

}   // namespace checkpointing
