#ifndef WRITE_SUB_ITEM_VALUE_PER_ITEM_VARIANT_HPP
#define WRITE_SUB_ITEM_VALUE_PER_ITEM_VARIANT_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

void writeSubItemValuePerItemVariant(const std::string& symbol_name,
                                     const EmbeddedData& embedded_data,
                                     HighFive::File& file,
                                     HighFive::Group& checkpoint_group,
                                     HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // WRITE_SUB_ITEM_VALUE_PER_ITEM_VARIANT_HPP
