#ifndef EIGENVALUE_SOLVER_OPTIONS_HF_TYPE_HPP
#define EIGENVALUE_SOLVER_OPTIONS_HF_TYPE_HPP

#include <algebra/EigenvalueSolverOptions.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<ESLibrary> PUGS_INLINE
create_enum_ESOptions_library_type()
{
  return {{"eigen3", ESLibrary::eigen3}, {"slepsc", ESLibrary::slepsc}};
}
HIGHFIVE_REGISTER_TYPE(ESLibrary, create_enum_ESOptions_library_type)

#endif   // EIGENVALUE_SOLVER_OPTIONS_HF_TYPE_HPP
