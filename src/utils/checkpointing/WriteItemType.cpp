#include <utils/checkpointing/WriteItemType.hpp>

#include <language/modules/MeshModuleTypes.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <mesh/ItemType.hpp>
#include <utils/checkpointing/ItemTypeHFType.hpp>

namespace checkpointing
{

void
writeItemType(const std::string& symbol_name,
              const EmbeddedData& embedded_data,
              HighFive::File&,
              HighFive::Group&,
              HighFive::Group& symbol_table_group)
{
  HighFive::Group variable_group = symbol_table_group.createGroup("embedded/" + symbol_name);

  std::shared_ptr<const ItemType> item_type_p =
    dynamic_cast<const DataHandler<const ItemType>&>(embedded_data.get()).data_ptr();

  const ItemType& item_type = *item_type_p;

  variable_group.createAttribute("type", dataTypeName(ast_node_data_type_from<decltype(item_type_p)>));
  variable_group.createAttribute("item_type", item_type);
}

}   // namespace checkpointing
