#include <utils/checkpointing/ReadIBoundaryDescriptor.hpp>

#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/EmbeddedData.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <mesh/NamedBoundaryDescriptor.hpp>
#include <mesh/NumberedBoundaryDescriptor.hpp>
#include <utils/checkpointing/IBoundaryDescriptorHFType.hpp>

namespace checkpointing
{

std::shared_ptr<const IBoundaryDescriptor>
readIBoundaryDescriptor(const HighFive::Group& iboundarydescriptor_group)
{
  const IBoundaryDescriptor::Type iboundary_descriptor_type =
    iboundarydescriptor_group.getAttribute("iboundary_descriptor_type").read<IBoundaryDescriptor::Type>();

  std::shared_ptr<const IBoundaryDescriptor> i_boundary_descriptor;

  switch (iboundary_descriptor_type) {
  case IBoundaryDescriptor::Type::named: {
    const std::string name = iboundarydescriptor_group.getAttribute("name").read<std::string>();
    i_boundary_descriptor  = std::make_shared<const NamedBoundaryDescriptor>(name);
    break;
  }
  case IBoundaryDescriptor::Type::numbered: {
    const unsigned int number = iboundarydescriptor_group.getAttribute("number").read<unsigned int>();
    i_boundary_descriptor     = std::make_shared<const NumberedBoundaryDescriptor>(number);
    break;
  }
  }

  return i_boundary_descriptor;
}

EmbeddedData
readIBoundaryDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group)
{
  const HighFive::Group iboundarydescriptor_group = symbol_table_group.getGroup("embedded/" + symbol_name);

  return {std::make_shared<DataHandler<const IBoundaryDescriptor>>(readIBoundaryDescriptor(iboundarydescriptor_group))};
}

}   // namespace checkpointing
