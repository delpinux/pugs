#ifndef READ_IQUADRATURE_DESCRIPTOR_HPP
#define READ_IQUADRATURE_DESCRIPTOR_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

EmbeddedData readIQuadratureDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_IQUADRATURE_DESCRIPTOR_HPP
