#ifndef I_NAMED_DISCRETE_DATA_HF_HPP
#define I_NAMED_DISCRETE_DATA_HF_HPP

#include <output/INamedDiscreteData.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<INamedDiscreteData::Type> PUGS_INLINE
create_enum_i_named_discrete_data_type()
{
  return {{"discrete_function", INamedDiscreteData::Type::discrete_function},
          {"item_array", INamedDiscreteData::Type::item_array},
          {"item_value", INamedDiscreteData::Type::item_value}};
}
HIGHFIVE_REGISTER_TYPE(INamedDiscreteData::Type, create_enum_i_named_discrete_data_type)

#endif   // I_NAMED_DISCRETE_DATA_HF_HPP
