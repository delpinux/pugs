#ifndef WRITE_ITEM_VALUE_HPP
#define WRITE_ITEM_VALUE_HPP

#include <mesh/ItemValue.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/checkpointing/WriteArray.hpp>

namespace checkpointing
{

template <typename DataType, ItemType item_type, typename ConnectivityPtr>
void
write(HighFive::Group& group,
      const std::string& name,
      const ItemValue<DataType, item_type, ConnectivityPtr>& item_value)
{
  write(group, name, item_value.arrayView());
}

}   // namespace checkpointing

#endif   // WRITE_ITEM_VALUE_HPP
