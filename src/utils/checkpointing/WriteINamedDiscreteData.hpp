#ifndef WRITE_INAMED_DISCRETE_DATA_HPP
#define WRITE_INAMED_DISCRETE_DATA_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

void writeINamedDiscreteData(const std::string& symbol_name,
                             const EmbeddedData& embedded_data,
                             HighFive::File& file,
                             HighFive::Group& checkpoint_group,
                             HighFive::Group& symbol_table_group);
}   // namespace checkpointing

#endif   // WRITE_INAMED_DISCRETE_DATA_HPP
