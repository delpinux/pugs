#ifndef READ_IDISCRETE_FUNCTION_DESCRIPTOR_HPP
#define READ_IDISCRETE_FUNCTION_DESCRIPTOR_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

EmbeddedData readIDiscreteFunctionDescriptor(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_IDISCRETE_FUNCTION_DESCRIPTOR_HPP
