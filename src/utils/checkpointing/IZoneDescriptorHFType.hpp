#ifndef I_ZONE_DESCRIPTOR_HF_TYPE_HPP
#define I_ZONE_DESCRIPTOR_HF_TYPE_HPP

#include <mesh/IZoneDescriptor.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/PugsMacros.hpp>

HighFive::EnumType<IZoneDescriptor::Type> PUGS_INLINE
create_enum_i_zone_descriptor_type()
{
  return {{"named", IZoneDescriptor::Type::named}, {"numbered", IZoneDescriptor::Type::numbered}};
}
HIGHFIVE_REGISTER_TYPE(IZoneDescriptor::Type, create_enum_i_zone_descriptor_type)

#endif   // I_ZONE_DESCRIPTOR_HF_TYPE_HPP
