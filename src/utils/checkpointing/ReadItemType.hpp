#ifndef READ_ITEM_TYPE_HPP
#define READ_ITEM_TYPE_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

EmbeddedData readItemType(const std::string& symbol_name, const HighFive::Group& symbol_table_group);

}   // namespace checkpointing

#endif   // READ_ITEM_TYPE_HPP
