#ifndef WRITE_IWRITER_HPP
#define WRITE_IWRITER_HPP

#include <utils/HighFivePugsUtils.hpp>

class EmbeddedData;

namespace checkpointing
{

void writeIWriter(const std::string& symbol_name,
                  const EmbeddedData& embedded_data,
                  HighFive::File&,
                  HighFive::Group&,
                  HighFive::Group& symbol_table_group);

}

#endif   // WRITE_IWRITER_HPP
