#ifndef WRITE_ITEM_ARRAY_HPP
#define WRITE_ITEM_ARRAY_HPP

#include <mesh/ItemArray.hpp>
#include <utils/HighFivePugsUtils.hpp>
#include <utils/checkpointing/WriteTable.hpp>

namespace checkpointing
{

template <typename DataType, ItemType item_type, typename ConnectivityPtr>
void
write(HighFive::Group& group,
      const std::string& name,
      const ItemArray<DataType, item_type, ConnectivityPtr>& item_array)
{
  write(group, name, item_array.tableView());
}

}   // namespace checkpointing

#endif   // WRITE_ITEM_ARRAY_HPP
