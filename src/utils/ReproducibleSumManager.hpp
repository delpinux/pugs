#ifndef REPRODUCIBLE_SUM_MANAGER_HPP
#define REPRODUCIBLE_SUM_MANAGER_HPP

class ReproducibleSumManager
{
 private:
  static bool s_reproducible_sums;

 public:
  static void setReproducibleSums(bool);
  static bool reproducibleSums();
};

#endif   // REPRODUCIBLE_SUM_MANAGER_HPP
