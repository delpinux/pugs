#ifndef GLOBAL_VARIABLE_MANAGER_HPP
#define GLOBAL_VARIABLE_MANAGER_HPP

#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>

class GlobalVariableManager
{
 private:
  size_t m_connectivity_id = 0;
  size_t m_mesh_id         = 0;

  static GlobalVariableManager* m_instance;

  explicit GlobalVariableManager()                    = default;
  GlobalVariableManager(GlobalVariableManager&&)      = delete;
  GlobalVariableManager(const GlobalVariableManager&) = delete;
  ~GlobalVariableManager()                            = default;

 public:
  PUGS_INLINE
  size_t
  getConnectivityId() const
  {
    return m_connectivity_id;
  }

  PUGS_INLINE
  size_t
  getAndIncrementConnectivityId()
  {
    return m_connectivity_id++;
  }

  PUGS_INLINE
  void
  setConnectivityId(size_t connectivity_id)
  {
    m_connectivity_id = connectivity_id;
  }

  PUGS_INLINE
  size_t
  getMeshId() const
  {
    return m_mesh_id;
  }

  PUGS_INLINE
  size_t
  getAndIncrementMeshId()
  {
    return m_mesh_id++;
  }

  PUGS_INLINE
  void
  setMeshId(size_t mesh_id)
  {
    m_mesh_id = mesh_id;
  }

  PUGS_INLINE
  static GlobalVariableManager&
  instance()
  {
    Assert(m_instance != nullptr);
    return *m_instance;
  }

  static void create();
  static void destroy();
};

#endif   // GLOBAL_VARIABLE_MANAGER_HPP
