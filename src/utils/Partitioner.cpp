#include <utils/Partitioner.hpp>

#include <utils/PTScotchPartitioner.hpp>
#include <utils/ParMETISPartitioner.hpp>

Partitioner::Partitioner(const PartitionerOptions& options) : m_partitioner_options{options} {}

Array<int>
Partitioner::partition(const CRSGraph& graph)
{
  switch (m_partitioner_options.library()) {
  case PartitionerLibrary::parmetis: {
    return ParMETISPartitioner::partition(graph);
  }
  case PartitionerLibrary::ptscotch: {
    return PTScotchPartitioner::partition(graph);
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("invalid partition library");
  }
    // LCOV_EXCL_STOP
  }
}
