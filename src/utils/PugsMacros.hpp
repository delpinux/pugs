#ifndef PUGS_MACROS_HPP
#define PUGS_MACROS_HPP

#include <Kokkos_Macros.hpp>

#define PUGS_RESTRICT KOKKOS_RESTRICT

#define PUGS_INLINE KOKKOS_INLINE_FUNCTION
#define PUGS_FORCEINLINE KOKKOS_FORCEINLINE_FUNCTION

#define PUGS_LAMBDA KOKKOS_LAMBDA
#define PUGS_CLASS_LAMBDA [ =, this ]

// Sets macro to ignore unknown pragma

#if !defined(__clang__) and defined(__GNUC__)

#define PRAGMA_DIAGNOSTIC_IGNORED_WATTRIBUTES _Pragma("GCC diagnostic ignored \"-Wattributes\"")
#define PRAGMA_DIAGNOSTIC_POP _Pragma("GCC diagnostic pop")

#else   // !defined(__clang__) and defined(__GNUC__)

#define PRAGMA_DIAGNOSTIC_IGNORED_WATTRIBUTES
#define PRAGMA_DIAGNOSTIC_POP

#endif

#endif   // PUGS_MACROS_HPP
