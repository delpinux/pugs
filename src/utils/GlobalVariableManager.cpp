#include <utils/GlobalVariableManager.hpp>

GlobalVariableManager* GlobalVariableManager::m_instance = nullptr;

void
GlobalVariableManager::create()
{
  Assert(m_instance == nullptr);
  m_instance = new GlobalVariableManager;
}

void
GlobalVariableManager::destroy()
{
  Assert(m_instance != nullptr);
  delete m_instance;
  m_instance = nullptr;
}
