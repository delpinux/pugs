#include <utils/Stop.hpp>

#include <utils/ExecutionStatManager.hpp>
#include <utils/Messenger.hpp>
#include <utils/pugs_config.hpp>

#include <filesystem>
#include <iostream>

#ifdef PUGS_HAS_SLURM
#include <slurm/slurm.h>

// LCOV_EXCL_START
class SlurmWrapper
{
 private:
  int m_slurm_job_id = -1;

 public:
  bool
  mustStop() const
  {
    if (m_slurm_job_id == -1) {
      return false;

    } else {
      return slurm_get_rem_time(m_slurm_job_id) < 150;
    }
  }

  SlurmWrapper()
  {
    char* env = getenv("SLURM_JOB_ID");
    if (env != nullptr) {
      slurm_init(nullptr);
      m_slurm_job_id = std::atoi(env);
    }
  }

  ~SlurmWrapper()
  {
    slurm_fini();
  }
};
// LCOV_EXCL_STOP

#endif   // PUGS_HAS_SLURM

bool
stop()
{
  bool stop = false;

  if (parallel::rank() == 0) {
    std::filesystem::path stop_file("stop");
    if (std::filesystem::exists(stop_file)) {
      const double elapse_time = ExecutionStatManager::getInstance().getElapseTime();

      const double stop_file_age =
        std::chrono::duration_cast<std::chrono::seconds>(std::filesystem::file_time_type::clock::now() -
                                                         std::filesystem::last_write_time(stop_file))
          .count();

      stop = elapse_time > stop_file_age;
    }

#ifdef PUGS_HAS_SLURM
    // LCOV_EXCL_START
    static SlurmWrapper slurm_wrapper;
    if (slurm_wrapper.mustStop()) {
      stop = true;
    }
    // LCOV_EXCL_STOP
#endif   // PUGS_HAS_SLURM
  }

  parallel::broadcast(stop, 0);

  return stop;
}
