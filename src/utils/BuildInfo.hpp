#ifndef BUILD_INFO_HPP
#define BUILD_INFO_HPP

#include <string>

struct BuildInfo
{
  static std::string type();
  static std::string compiler();
  static std::string kokkosDevices();
  static std::string mpiLibrary();
  static std::string parmetisLibrary();
  static std::string ptscotchLibrary();
  static std::string petscLibrary();
  static std::string slepcLibrary();
  static std::string eigen3Library();
  static std::string hdf5Library();
  static std::string slurmLibrary();
};

#endif   // BUILD_INFO_HPP
