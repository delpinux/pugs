#ifndef REPRODUCIBLE_SUM_MPI_HPP
#define REPRODUCIBLE_SUM_MPI_HPP

#include <utils/Messenger.hpp>
#include <utils/ReproducibleSumUtils.hpp>
#include <utils/pugs_config.hpp>

#ifdef PUGS_HAS_MPI

// LCOV_EXCL_START
// This piece of code is not catchable by coverage...

template <typename RSumType>
struct ReproducibleMPIReducer
{
  static void
  all_reduce_sum_embedder(void* local_value, void* sum_value, int* length, MPI_Datatype*)
  {
    Assert(*length == 1, "reduction must be performed on single bin");
    using Bin        = typename RSumType::Bin;
    Bin* p_local_bin = reinterpret_cast<Bin*>(local_value);
    Bin* p_sum_bin   = reinterpret_cast<Bin*>(sum_value);

    RSumType::addBinTo(*p_local_bin, *p_sum_bin);
  }
};

// LCOV_EXCL_STOP

template <typename ArrayT, typename BoolArrayT>
auto
allReduceReproducibleSum(const ReproducibleScalarSum<ArrayT, BoolArrayT>& s)
{
  using DataType = std::decay_t<typename ArrayT::data_type>;
  using RSumType = std::decay_t<decltype(s)>;
  using BinType  = typename RSumType::Bin;

  BinType local_bin = s.getSummationBin();

  MPI_Datatype mpi_bin_type;
  MPI_Type_contiguous(sizeof(BinType) / sizeof(DataType), parallel::Messenger::helper::mpiType<DataType>(),
                      &mpi_bin_type);
  MPI_Type_commit(&mpi_bin_type);

  MPI_Op mpi_bin_sum_op;
  MPI_Op_create(ReproducibleMPIReducer<RSumType>::all_reduce_sum_embedder, 1, &mpi_bin_sum_op);

  BinType sum_bin = zero;
  MPI_Allreduce(&local_bin, &sum_bin, 1, mpi_bin_type, mpi_bin_sum_op, parallel::Messenger::getInstance().comm());

  MPI_Type_free(&mpi_bin_type);
  MPI_Op_free(&mpi_bin_sum_op);

  return sum_bin;
}

template <typename ArrayT, typename BoolArrayT>
auto
allReduceReproducibleSum(const ReproducibleTinyVectorSum<ArrayT, BoolArrayT>& s)
{
  using TinyVectorType = std::decay_t<typename ArrayT::data_type>;
  using DataType       = typename TinyVectorType::data_type;
  using RSumType       = std::decay_t<decltype(s)>;
  using BinType        = typename RSumType::Bin;

  BinType local_bin = s.getSummationBin();

  MPI_Datatype mpi_bin_type;
  MPI_Type_contiguous(sizeof(BinType) / sizeof(DataType), parallel::Messenger::helper::mpiType<DataType>(),
                      &mpi_bin_type);
  MPI_Type_commit(&mpi_bin_type);

  MPI_Op mpi_bin_sum_op;
  MPI_Op_create(ReproducibleMPIReducer<RSumType>::all_reduce_sum_embedder, 1, &mpi_bin_sum_op);

  BinType sum_bin = zero;
  MPI_Allreduce(&local_bin, &sum_bin, 1, mpi_bin_type, mpi_bin_sum_op, parallel::Messenger::getInstance().comm());

  MPI_Op_free(&mpi_bin_sum_op);
  MPI_Type_free(&mpi_bin_type);

  return sum_bin;
}

template <typename ArrayT, typename BoolArrayT>
auto
allReduceReproducibleSum(const ReproducibleTinyMatrixSum<ArrayT, BoolArrayT>& s)
{
  using TinyVectorType = std::decay_t<typename ArrayT::data_type>;
  using DataType       = typename TinyVectorType::data_type;
  using RSumType       = std::decay_t<decltype(s)>;
  using BinType        = typename RSumType::Bin;

  BinType local_bin = s.getSummationBin();

  MPI_Datatype mpi_bin_type;
  MPI_Type_contiguous(sizeof(BinType) / sizeof(DataType), parallel::Messenger::helper::mpiType<DataType>(),
                      &mpi_bin_type);
  MPI_Type_commit(&mpi_bin_type);

  MPI_Op mpi_bin_sum_op;
  MPI_Op_create(ReproducibleMPIReducer<RSumType>::all_reduce_sum_embedder, 1, &mpi_bin_sum_op);

  BinType sum_bin = zero;
  MPI_Allreduce(&local_bin, &sum_bin, 1, mpi_bin_type, mpi_bin_sum_op, parallel::Messenger::getInstance().comm());

  MPI_Op_free(&mpi_bin_sum_op);
  MPI_Type_free(&mpi_bin_type);

  return sum_bin;
}

#else   // PUGS_HAS_MPI

template <typename ArrayT, typename BoolArrayT>
auto
allReduceReproducibleSum(const ReproducibleScalarSum<ArrayT, BoolArrayT>& s)
{
  return s.getSummationBin();
}

template <typename ArrayT, typename BoolArrayT>
auto
allReduceReproducibleSum(const ReproducibleTinyVectorSum<ArrayT, BoolArrayT>& s)
{
  return s.getSummationBin();
}

template <typename ArrayT, typename BoolArrayT>
auto
allReduceReproducibleSum(const ReproducibleTinyMatrixSum<ArrayT, BoolArrayT>& s)
{
  return s.getSummationBin();
}

#endif   // PUGS_HAS_MPI

#endif   // REPRODUCIBLE_SUM_MPI_HPP
