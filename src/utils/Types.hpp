#ifndef TYPES_HPP
#define TYPES_HPP

enum class ZeroType
{
  zero
};
constexpr inline ZeroType zero = ZeroType::zero;

enum class IdentityType
{
  identity
};
constexpr inline IdentityType identity = IdentityType::identity;

#endif   // TYPES_HPP
