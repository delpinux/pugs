#ifndef SLEPC_WRAPPER_HPP
#define SLEPC_WRAPPER_HPP

namespace SLEPcWrapper
{
void initialize(int& argc, char* argv[]);
void finalize();
}   // namespace SLEPcWrapper

#endif   // SLEPC_WRAPPER_HPP
