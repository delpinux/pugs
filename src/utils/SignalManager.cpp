#include <utils/SignalManager.hpp>

#include <language/ast/ASTExecutionStack.hpp>
#include <language/utils/ParseError.hpp>
#include <utils/BacktraceManager.hpp>
#include <utils/ConsoleManager.hpp>
#include <utils/Messenger.hpp>
#include <utils/PugsAssert.hpp>

#include <utils/pugs_config.hpp>

#include <rang.hpp>

#include <csignal>
#include <iostream>
#include <unistd.h>

bool SignalManager::s_pause_on_error = false;

bool
SignalManager::pauseOnError()
{
  return s_pause_on_error;
}

void
SignalManager::setPauseForDebug(bool pause_on_error)
{
  s_pause_on_error = pause_on_error;
}

std::string
SignalManager::signalName(int signal)
{
  switch (signal) {
  case SIGILL:
    return "SIGILL";
  case SIGFPE:
    return "SIGFPE";
  case SIGABRT:
    return "SIGABRT";
  case SIGINT:
    return "SIGINT";
  case SIGSEGV:
    return "SIGSEGV";
  case SIGTERM:
    return "SIGTERM";
  }
  return "SIGNAL undefined!";
}

void
SignalManager::pauseForDebug(int signal)
{
  if (std::string(PUGS_BUILD_TYPE) != "Release") {
    if (s_pause_on_error) {
      // Each failing process must write
      std::cerr.clear();
      std::cerr << "\n======================================\n"
                << rang::style::reset << rang::fg::reset << rang::style::bold << "to attach gdb to this process run\n"
                << "\tgdb -pid " << rang::fg::red << getpid() << rang::fg::reset << '\n'
                << "else press Control-C to exit\n"
                << rang::style::reset << "======================================\n"
                << std::flush;
      pause();
    }
  }
  std::exit(signal);
}

void
SignalManager::handler(int signal)
{
  static std::mutex mutex;

  if (mutex.try_lock()) {
    std::signal(SIGTERM, SIG_DFL);
    std::signal(SIGINT, SIG_DFL);
    std::signal(SIGABRT, SIG_DFL);

    // Each failing process must write
    std::cerr.clear();

    std::cerr << BacktraceManager{} << '\n';

    std::cerr << "\n *** " << rang::style::reset << rang::fg::reset << rang::style::bold << "Signal " << rang::fgB::red
              << signalName(signal) << rang::fg::reset << " caught" << rang::style::reset << " ***\n\n";

    std::exception_ptr eptr = std::current_exception();
    try {
      if (eptr) {
        std::rethrow_exception(eptr);
      } else {
        std::ostringstream error_msg;
        error_msg << "received " << signalName(signal);
        std::cerr << ASTExecutionStack::getInstance().errorMessageAt(error_msg.str()) << '\n';
      }
    }
    catch (const IBacktraceError& backtrace_error) {
      auto source_location = backtrace_error.sourceLocation();
      std::cerr << rang::fgB::cyan << source_location.file_name() << ':' << source_location.line() << ':'
                << source_location.column() << ':' << rang::fg::reset << rang::fgB::yellow
                << " threw the following exception" << rang::fg::reset << "\n\n";
      std::cerr << ASTExecutionStack::getInstance().errorMessageAt(backtrace_error.what()) << '\n';
    }
    catch (const ParseError& parse_error) {
      auto p = parse_error.positions().front();
      std::cerr << rang::style::bold << p.source << ':' << p.line << ':' << p.column << ':' << rang::style::reset
                << rang::fgB::red << " error: " << rang::fg::reset << rang::style::bold << parse_error.what()
                << rang::style::reset << '\n';
    }
    catch (const IExitError& exit_error) {
      std::cerr << ASTExecutionStack::getInstance().errorMessageAt(exit_error.what()) << '\n';
    }
    catch (const AssertError& assert_error) {
      std::cerr << assert_error << '\n';
    }
    catch (...) {
      std::cerr << "Unknown exception!\n";
    }

    SignalManager::pauseForDebug(signal);

    mutex.unlock();
  }
}

void
SignalManager::init(bool enable)
{
  if (enable) {
    std::signal(SIGFPE, SignalManager::handler);
    std::signal(SIGSEGV, SignalManager::handler);
    std::signal(SIGTERM, SignalManager::handler);
    std::signal(SIGINT, SignalManager::handler);
    std::signal(SIGABRT, SignalManager::handler);
    std::signal(SIGPIPE, SignalManager::handler);
  }
}
