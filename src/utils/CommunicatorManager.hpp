#ifndef COMMUNICATOR_MANAGER_HPP
#define COMMUNICATOR_MANAGER_HPP

#include <optional>

class CommunicatorManager
{
 private:
  static std::optional<int> s_split_color;

  friend void resetCommunicationManagerForTests();

 public:
  static bool hasSplitColor();
  static int splitColor();
  static void setSplitColor(int split_color);
};

#endif   // COMMUNICATOR_MANAGER_HPP
