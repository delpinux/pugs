#ifndef ESCAPED_STRING_HPP
#define ESCAPED_STRING_HPP

#include <utils/Exceptions.hpp>
#include <utils/PugsMacros.hpp>

#include <sstream>
#include <string>
#include <string_view>

PUGS_INLINE std::string
unescapeString(std::string_view input_string)
{
  std::stringstream ss;
  for (size_t i = 0; i < input_string.size(); ++i) {
    char c = input_string[i];
    if (c == '\\') {
      ++i;
      char next = input_string[i];
      switch (next) {
      case '\'': {
        ss << '\'';
        break;
      }
      case '"': {
        ss << '\"';
        break;
      }
      case '?': {
        ss << '\?';
        break;
      }
      case '\\': {
        ss << '\\';
        break;
      }
      case 'a': {
        ss << '\a';
        break;
      }
      case 'b': {
        ss << '\b';
        break;
      }
      case 'f': {
        ss << '\f';
        break;
      }
      case 'n': {
        ss << '\n';
        break;
      }
      case 'r': {
        ss << '\r';
        break;
      }
      case 't': {
        ss << '\t';
        break;
      }
      case 'v': {
        ss << '\v';
        break;
      }
      }
    } else {
      ss << input_string[i];
    }
  }

  return ss.str();
}

PUGS_INLINE std::string
escapeString(std::string_view input_string)
{
  std::stringstream ss;
  for (size_t i = 0; i < input_string.size(); ++i) {
    char c = input_string[i];
    switch (c) {
    case '\\': {
      ss << R"(\\)";
      break;
    }
    case '\'': {
      ss << R"(\')";
      break;
    }
    case '\"': {
      ss << R"(\")";
      break;
    }
    case '\?': {
      ss << R"(\?)";
      break;
    }
    case '\a': {
      ss << R"(\a)";
      break;
    }
    case '\b': {
      ss << R"(\b)";
      break;
    }
    case '\f': {
      ss << R"(\f)";
      break;
    }
    case '\n': {
      ss << R"(\n)";
      break;
    }
    case '\r': {
      ss << R"(\r)";
      break;
    }
    case '\t': {
      ss << R"(\t)";
      break;
    }
    case '\v': {
      ss << R"(\v)";
      break;
    }
    default: {
      ss << c;
    }
    }
  }
  return ss.str();
}

#endif   // ESCAPED_STRING_HPP
