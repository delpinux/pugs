#ifndef BACKTRACE_MANAGER_HPP
#define BACKTRACE_MANAGER_HPP

#include <string>
#include <vector>

class BacktraceManager
{
 private:
  static bool s_show;
  std::vector<std::string> m_stack_lines;

 public:
  static void setShow(bool show_backtrace);

  BacktraceManager();

  friend std::ostream& operator<<(std::ostream& os, const BacktraceManager& btm);

  BacktraceManager(const BacktraceManager&) = default;
  ~BacktraceManager()                       = default;
};

#endif   // BACKTRACE_MANAGER_HPP
