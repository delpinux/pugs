#ifndef REVISION_INFO_HPP
#define REVISION_INFO_HPP

#include <string>

struct RevisionInfo
{
  static std::string version();

  static bool hasGitInfo();
  static std::string gitTag();
  static std::string gitHead();
  static std::string gitHash();
  static bool gitIsClean();
};

#endif   // REVISION_INFO_HPP
