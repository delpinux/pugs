#ifndef SMALL_ARRAY_HPP
#define SMALL_ARRAY_HPP

#include <utils/InvalidData.hpp>
#include <utils/NaNHelper.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>
#include <utils/PugsUtils.hpp>
#include <utils/Types.hpp>

#include <algorithm>

template <typename DataType>
class [[nodiscard]] SmallArray
{
 public:
  using data_type  = DataType;
  using index_type = size_t;

 private:
  index_type m_size;
  std::shared_ptr<DataType[]> m_values;

  // Allows const version to access our data
  friend SmallArray<std::add_const_t<DataType>>;

 public:
  PUGS_INLINE size_t
  size() const noexcept
  {
    return m_size;
  }

  friend PUGS_INLINE SmallArray<std::remove_const_t<DataType>>
  copy(const SmallArray<DataType>& source)
  {
    SmallArray<std::remove_const_t<DataType>> image(source.m_size);
    std::copy(source.m_values.get(), source.m_values.get() + source.m_size, image.m_values.get());

    return image;
  }

  friend PUGS_INLINE void
  copy_to(const SmallArray<DataType>& source, const SmallArray<std::remove_const_t<DataType>>& destination)
  {
    Assert(source.size() == destination.size());
    std::copy(source.m_values.get(), source.m_values.get() + source.m_size, destination.m_values.get());
  }

  PUGS_INLINE DataType&
  operator[](index_type i) const noexcept(NO_ASSERT)
  {
    Assert(i < m_size);
    return m_values[i];
  }

  PUGS_INLINE
  void
  fill(const DataType& data) const
  {
    static_assert(not std::is_const_v<DataType>, "Cannot modify SmallArray of const");
    std::fill(m_values.get(), m_values.get() + m_size, data);
  }

  template <typename DataType2>
  PUGS_INLINE SmallArray&
  operator=(const SmallArray<DataType2>& array) noexcept
  {
    // ensures that DataType is the same as source DataType2
    static_assert(std::is_same<std::remove_const_t<DataType>, std::remove_const_t<DataType2>>(),
                  "Cannot assign SmallArray of different type");
    // ensures that const is not lost through copy
    static_assert(((std::is_const_v<DataType2> and std::is_const_v<DataType>) or not std::is_const_v<DataType2>),
                  "Cannot assign SmallArray of const to SmallArray of non-const");

    m_size   = array.size();
    m_values = array.m_values;
    return *this;
  }

  PUGS_INLINE
  SmallArray& operator=(const SmallArray&) = default;

  PUGS_INLINE
  SmallArray& operator=(SmallArray&&) = default;

  PUGS_INLINE
  explicit SmallArray(size_t size) : m_size{size}, m_values{std::shared_ptr<DataType[]>(new DataType[size])}
  {
    static_assert(not std::is_const<DataType>(), "Cannot allocate SmallArray of const data: only view is "
                                                 "supported");

#ifndef NDEBUG
    if constexpr (not std::is_const_v<DataType>) {
      using T = std::decay_t<DataType>;
      if constexpr (std::is_arithmetic_v<T>) {
        this->fill(invalid_data_v<T>);
      } else if constexpr (is_tiny_vector_v<T>) {
        this->fill(T{});
      } else if constexpr (is_tiny_matrix_v<T>) {
        this->fill(T{});
      }
    }
#endif   // NDEBUG
  }

  friend std::ostream&
  operator<<(std::ostream& os, const SmallArray& x)
  {
    if (x.size() > 0) {
      os << 0 << ':' << NaNHelper(x[0]);
    }
    for (size_t i = 1; i < x.size(); ++i) {
      os << ' ' << i << ':' << NaNHelper(x[i]);
    }
    return os;
  }

  PUGS_INLINE
  SmallArray() = default;

  PUGS_INLINE
  SmallArray(const SmallArray&) = default;

  template <typename DataType2>
  PUGS_INLINE
  SmallArray(const SmallArray<DataType2>& array) noexcept
  {
    this->operator=(array);
  }

  PUGS_INLINE
  SmallArray(SmallArray&&) = default;

  PUGS_INLINE
  ~SmallArray() = default;
};

template <typename DataType>
PUGS_INLINE size_t
size(const SmallArray<DataType>& array)
{
  return array.size();
}

// map, multimap, unordered_map and stack cannot be copied this way
template <typename Container>
PUGS_INLINE SmallArray<std::remove_const_t<typename Container::value_type>>
convert_to_small_array(const Container& given_container)
{
  using DataType = typename Container::value_type;
  SmallArray<std::remove_const_t<DataType>> array(given_container.size());
  if (given_container.size() > 0) {
    std::copy(begin(given_container), end(given_container), &(array[0]));
  }
  return array;
}

template <typename DataType>
[[nodiscard]] std::remove_const_t<DataType>
min(const SmallArray<DataType>& array)
{
  using data_type  = std::remove_const_t<DataType>;
  using index_type = typename SmallArray<DataType>::index_type;

  data_type min_value = std::numeric_limits<data_type>::max();
  for (index_type i = 0; i < array.size(); ++i) {
    if (array[i] < min_value) {
      min_value = array[i];
    }
  }
  return min_value;
}

template <typename DataType>
[[nodiscard]] std::remove_const_t<DataType>
max(const SmallArray<DataType>& array)
{
  using data_type  = std::remove_const_t<DataType>;
  using index_type = typename SmallArray<DataType>::index_type;

  data_type max_value = std::numeric_limits<data_type>::lowest();
  for (index_type i = 0; i < array.size(); ++i) {
    if (array[i] > max_value) {
      max_value = array[i];
    }
  }
  return max_value;
}

template <typename DataType>
[[nodiscard]] std::remove_const_t<DataType>
sum(const SmallArray<DataType>& array)
{
  using data_type  = std::remove_const_t<DataType>;
  using index_type = typename SmallArray<DataType>::index_type;

  data_type sum_value = [] {
    if constexpr (std::is_arithmetic_v<DataType>) {
      return 0;
    } else if constexpr (is_tiny_vector_v<DataType> or is_tiny_matrix_v<DataType>) {
      return zero;
    } else {
      static_assert(std::is_arithmetic_v<DataType>, "invalid data type");
    }
  }();

  for (index_type i = 0; i < array.size(); ++i) {
    sum_value += array[i];
  }

  return sum_value;
}

#endif   // SMALL_ARRAY_HPP
