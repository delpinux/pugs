#include <utils/SLEPcWrapper.hpp>

#include <utils/pugs_config.hpp>

#ifdef PUGS_HAS_SLEPC
#include <slepc.h>
#endif   // PUGS_HAS_SLEPC

namespace SLEPcWrapper
{
void
initialize([[maybe_unused]] int& argc, [[maybe_unused]] char* argv[])
{
#ifdef PUGS_HAS_SLEPC
  SlepcInitialize(&argc, &argv, 0, 0);
#endif   // PUGS_HAS_SLEPC
}

void
finalize()
{
#ifdef PUGS_HAS_SLEPC
  SlepcFinalize();
#endif   // PUGS_HAS_SLEPC
}
}   // namespace SLEPcWrapper
