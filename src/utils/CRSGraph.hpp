#ifndef CRS_GRAPH_HPP
#define CRS_GRAPH_HPP

#include <utils/Array.hpp>

class CRSGraph
{
 private:
  Array<const int> m_entries;
  Array<const int> m_neighbors;

 public:
  size_t
  numberOfNodes() const
  {
    Assert(m_entries.size() > 0);
    return m_entries.size() - 1;
  }

  const Array<const int>&
  entries() const
  {
    return m_entries;
  }

  const Array<const int>&
  neighbors() const
  {
    return m_neighbors;
  }

  CRSGraph& operator=(CRSGraph&&) = delete;
  CRSGraph& operator=(const CRSGraph&) = delete;

  CRSGraph(const Array<int>& entries, const Array<int>& neighbors) : m_entries(entries), m_neighbors(neighbors)
  {
    Assert(m_entries.size() > 0);
    Assert(static_cast<size_t>(m_entries[m_entries.size() - 1]) == m_neighbors.size());
  }

  CRSGraph()                = delete;
  CRSGraph(CRSGraph&&)      = delete;
  CRSGraph(const CRSGraph&) = delete;
  ~CRSGraph()               = default;
};

#endif   // CRS_GRAPH_HPP
