#include <utils/ConsoleManager.hpp>

#include <rang.hpp>

bool ConsoleManager::s_show_preamble = true;

bool
ConsoleManager::showPreamble()
{
  return s_show_preamble;
}

void
ConsoleManager::setShowPreamble(bool show_preamble)
{
  s_show_preamble = show_preamble;
}

bool
ConsoleManager::isTerminal(std::ostream& os)
{
  return rang::rang_implementation::isTerminal(os.rdbuf());
}

void
ConsoleManager::init(bool colorize)
{
  if (colorize) {
    rang::setControlMode(rang::control::Force);
  } else {
    rang::setControlMode(rang::control::Off);
  }
}
