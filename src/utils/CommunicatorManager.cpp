#include <utils/CommunicatorManager.hpp>

#include <utils/PugsAssert.hpp>

std::optional<int> CommunicatorManager::s_split_color;

bool
CommunicatorManager::hasSplitColor()
{
  return s_split_color.has_value();
}

int
CommunicatorManager::splitColor()
{
  Assert(s_split_color.has_value(), "split color has not been defined");

  return s_split_color.value();
}

void
CommunicatorManager::setSplitColor(int split_color)
{
  Assert(not s_split_color.has_value(), "split color has already been defined");

  s_split_color = split_color;
}
