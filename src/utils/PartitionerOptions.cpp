#include <utils/PartitionerOptions.hpp>

#include <rang.hpp>

std::ostream&
operator<<(std::ostream& os, const PartitionerOptions& options)
{
  os << "  library: " << rang::style::bold << name(options.library()) << rang::style::reset << '\n';
  return os;
}
