#ifndef PETSC_WRAPPER_HPP
#define PETSC_WRAPPER_HPP

namespace PETScWrapper
{
void initialize(int& argc, char* argv[]);
void finalize();
}   // namespace PETScWrapper

#endif   // PETSC_WRAPPER_HPP
