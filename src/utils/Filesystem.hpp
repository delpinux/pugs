#ifndef FILESYSTEM_HPP
#define FILESYSTEM_HPP

#include <utils/Exceptions.hpp>
#include <utils/PugsMacros.hpp>

#include <filesystem>

PUGS_INLINE void
createDirectoryIfNeeded(const std::string& filename)
{
  std::filesystem::path path = std::filesystem::path{filename}.parent_path();
  if (not path.empty()) {
    try {
      std::filesystem::create_directories(path);
    }
    // LCOV_EXCL_START
    catch (std::filesystem::filesystem_error& e) {
      throw NormalError(e.what());
    }
    // LCOV_EXCL_STOP
  }
}

#endif   // FILESYSTEM_HPP
