#include <utils/Messenger.hpp>

#include <utils/CommunicatorManager.hpp>

#include <iostream>

namespace parallel
{
Messenger* Messenger::m_instance = nullptr;

void
Messenger::create(int& argc, char* argv[], bool parallel_output)
{
  if (Messenger::m_instance == nullptr) {
    Messenger::m_instance = new Messenger(argc, argv, parallel_output);
  } else {
    throw UnexpectedError("Messenger already created");
  }
}

void
Messenger::destroy()
{
  // One allows multiple destruction to handle unexpected code exit
  if (Messenger::m_instance != nullptr) {
    delete Messenger::m_instance;
    Messenger::m_instance = nullptr;
  }
}

Messenger::Messenger([[maybe_unused]] int& argc, [[maybe_unused]] char* argv[], [[maybe_unused]] bool parallel_output)
{
#ifdef PUGS_HAS_MPI
  MPI_Init(&argc, &argv);

  if (CommunicatorManager::hasSplitColor()) {
    // LCOV_EXCL_START
    int key = 0;

    auto res = MPI_Comm_split(MPI_COMM_WORLD, CommunicatorManager::splitColor(), key, &m_pugs_comm_world);
    if (res) {
      MPI_Abort(MPI_COMM_WORLD, res);
    }
    // LCOV_EXCL_STOP
  }

  m_rank = [&]() {
    int rank;
    MPI_Comm_rank(m_pugs_comm_world, &rank);
    return rank;
  }();

  m_size = [&]() {
    int size = 0;
    MPI_Comm_size(m_pugs_comm_world, &size);
    return size;
  }();

  m_global_rank = [&]() {
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    return rank;
  }();

  m_global_size = [&]() {
    int size = 0;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    return size;
  }();

  if ((not parallel_output) and (m_rank != 0)) {
    // LCOV_EXCL_START
    std::cout.setstate(std::ios::badbit);
    std::cerr.setstate(std::ios::badbit);
    std::clog.setstate(std::ios::badbit);
    // LCOV_EXCL_STOP
  }
#endif   // PUGS_HAS_MPI
}

Messenger::~Messenger()
{
#ifdef PUGS_HAS_MPI
  MPI_Finalize();
#endif   // PUGS_HAS_MPI
}

void
Messenger::barrier() const
{
#ifdef PUGS_HAS_MPI
  MPI_Barrier(m_pugs_comm_world);
#endif   // PUGS_HAS_MPI
}

}   // namespace parallel
