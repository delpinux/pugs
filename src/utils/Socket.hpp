#ifndef SOCKET_HPP
#define SOCKET_HPP

#include <memory>
#include <string>
#include <type_traits>
#include <utils/PugsTraits.hpp>

class Socket
{
 private:
  class Internals;

  std::shared_ptr<Internals> m_internals;

  Socket(std::shared_ptr<Internals> internals) : m_internals{internals} {}

  void _write(const char* data, const size_t lenght) const;
  void _read(char* data, const size_t lenght) const;

 public:
  int portNumber() const;

  friend std::ostream& operator<<(std::ostream& os, const Socket& s);

  friend Socket createServerSocket(int port_number);
  friend Socket acceptClientSocket(const Socket& server);
  friend Socket connectServerSocket(const std::string& server_name, int port_number);

  template <typename T>
  friend void write(const Socket& socket, const T& value);

  template <typename T>
  friend void read(const Socket& socket, T& value);

  template <template <typename T, typename... R> typename ArrayT, typename T, typename... R>
  friend void write(const Socket& socket, const ArrayT<T, R...>& array);

  template <template <typename T, typename... R> typename ArrayT, typename T, typename... R>
  friend void read(const Socket& socket, ArrayT<T, R...>& array);

  Socket(Socket&&)      = default;
  Socket(const Socket&) = default;

  ~Socket() = default;
};

Socket createServerSocket(int port_number);
Socket acceptClientSocket(const Socket& server);
Socket connectServerSocket(const std::string& server_name, int port_number);

template <typename T>
inline void
write(const Socket& socket, const T& value)
{
  static_assert(std::is_arithmetic_v<T> or is_tiny_vector_v<T> or is_tiny_matrix_v<T>, "unexpected value type");
  socket._write(reinterpret_cast<const char*>(&value), sizeof(T) / sizeof(char));
}

template <template <typename T, typename... R> typename ArrayT, typename T, typename... R>
void
write(const Socket& socket, const ArrayT<T, R...>& array)
{
  socket._write(reinterpret_cast<const char*>(&array[0]), array.size() * sizeof(T) / sizeof(char));
}

template <typename T>
inline void
read(const Socket& socket, T& value)
{
  static_assert(not std::is_const_v<T>, "cannot read values into const data");
  socket._read(reinterpret_cast<char*>(&value), sizeof(T) / sizeof(char));
}

template <template <typename T, typename... R> typename ArrayT, typename T, typename... R>
void
read(const Socket& socket, ArrayT<T, R...>& array)
{
  static_assert(not std::is_const_v<T>, "cannot read values into const data");
  static_assert(std::is_arithmetic_v<T> or is_tiny_vector_v<T> or is_tiny_matrix_v<T>, "unexpected value type");
  if (array.size() > 0) {
    socket._read(reinterpret_cast<char*>(&array[0]), array.size() * sizeof(T) / sizeof(char));
  }
}

#endif   // SOCKET_HPP
