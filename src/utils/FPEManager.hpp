#ifndef FPEMANAGER_HPP
#define FPEMANAGER_HPP

struct FPEManager
{
  static void enable();
  static void disable();
  static void init(bool enable);
};

#endif   // FPEMANAGER_HPP
