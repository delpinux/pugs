#ifndef WALL_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define WALL_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>

class WallBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "wall(" << *m_boundary_descriptor << ")";
    return os;
  }

 public:
  Type
  type() const final
  {
    return Type::wall;
  }

  WallBoundaryConditionDescriptor(const std::shared_ptr<const IBoundaryDescriptor>& boundary_descriptor)

    : BoundaryConditionDescriptorBase{boundary_descriptor}
  {
    ;
  }

  WallBoundaryConditionDescriptor(const WallBoundaryConditionDescriptor&) = delete;
  WallBoundaryConditionDescriptor(WallBoundaryConditionDescriptor&&)      = delete;

  ~WallBoundaryConditionDescriptor() = default;
};

#endif   // WALL_BOUNDARY_CONDITION_DESCRIPTOR_HPP
