#include <scheme/DiscreteFunctionIntegrator.hpp>

#include <language/utils/IntegrateCellValue.hpp>
#include <mesh/MeshCellZone.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>
#include <scheme/DiscreteFunctionP0.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>
#include <utils/Exceptions.hpp>

template <MeshConcept MeshType, typename DataType, typename ValueType>
DiscreteFunctionVariant
DiscreteFunctionIntegrator::_integrateOnZoneList() const
{
  static_assert(std::is_convertible_v<DataType, ValueType>);
  Assert(m_zone_list.size() > 0, "no zone list provided");

  constexpr size_t Dimension = MeshType::Dimension;

  std::shared_ptr p_mesh = m_mesh->get<MeshType>();

  CellValue<bool> is_in_zone{p_mesh->connectivity()};
  is_in_zone.fill(false);

  size_t number_of_cells = 0;
  for (const auto& zone : m_zone_list) {
    auto mesh_cell_zone   = getMeshCellZone(*p_mesh, *zone);
    const auto& cell_list = mesh_cell_zone.cellList();
    for (size_t i_cell = 0; i_cell < cell_list.size(); ++i_cell) {
      const CellId cell_id = cell_list[i_cell];
      if (is_in_zone[cell_id]) {
        std::ostringstream os;
        os << "cell " << cell_id << " (number " << p_mesh->connectivity().cellNumber()[cell_id]
           << ") is present multiple times in zone list";
        throw NormalError(os.str());
      }
      ++number_of_cells;
      is_in_zone[cell_id] = true;
    }
  }

  Array<CellId> zone_cell_list{number_of_cells};
  {
    size_t i_cell = 0;
    for (CellId cell_id = 0; cell_id < p_mesh->numberOfCells(); ++cell_id) {
      if (is_in_zone[cell_id]) {
        zone_cell_list[i_cell++] = cell_id;
      }
    }
  }

  Array<ValueType> array =
    IntegrateCellValue<ValueType(TinyVector<Dimension>)>::template integrate<MeshType>(m_function_id,
                                                                                       *m_quadrature_descriptor,
                                                                                       *p_mesh, zone_cell_list);

  CellValue<ValueType> cell_value{p_mesh->connectivity()};
  if constexpr (is_tiny_vector_v<ValueType> or is_tiny_matrix_v<ValueType>) {
    cell_value.fill(zero);
  } else {
    cell_value.fill(0);
  }

  parallel_for(
    zone_cell_list.size(), PUGS_LAMBDA(const size_t i_cell) { cell_value[zone_cell_list[i_cell]] = array[i_cell]; });

  return DiscreteFunctionP0<ValueType>(m_mesh, cell_value);
}

template <MeshConcept MeshType, typename DataType, typename ValueType>
DiscreteFunctionVariant
DiscreteFunctionIntegrator::_integrateGlobally() const
{
  Assert(m_zone_list.size() == 0, "invalid call when zones are defined");

  std::shared_ptr mesh = m_mesh->get<MeshType>();

  static_assert(std::is_convertible_v<DataType, ValueType>);

  return DiscreteFunctionP0<ValueType>(m_mesh,
                                       IntegrateCellValue<ValueType(TinyVector<MeshType::Dimension>)>::
                                         template integrate<MeshType>(m_function_id, *m_quadrature_descriptor, *mesh));
}

template <MeshConcept MeshType, typename DataType, typename ValueType>
DiscreteFunctionVariant
DiscreteFunctionIntegrator::_integrate() const
{
  if (m_zone_list.size() == 0) {
    return this->_integrateGlobally<MeshType, DataType, ValueType>();
  } else {
    return this->_integrateOnZoneList<MeshType, DataType, ValueType>();
  }
}

template <MeshConcept MeshType>
DiscreteFunctionVariant
DiscreteFunctionIntegrator::_integrate() const
{
  const auto& function_descriptor = m_function_id.descriptor();
  Assert(function_descriptor.domainMappingNode().children[1]->m_data_type == ASTNodeDataType::typename_t);

  const ASTNodeDataType& data_type = function_descriptor.domainMappingNode().children[1]->m_data_type.contentType();

  switch (data_type) {
  case ASTNodeDataType::bool_t: {
    return this->_integrate<MeshType, bool, double>();
  }
  case ASTNodeDataType::unsigned_int_t: {
    return this->_integrate<MeshType, uint64_t, double>();
  }
  case ASTNodeDataType::int_t: {
    return this->_integrate<MeshType, int64_t, double>();
  }
  case ASTNodeDataType::double_t: {
    return this->_integrate<MeshType, double>();
  }
  case ASTNodeDataType::vector_t: {
    switch (data_type.dimension()) {
    case 1: {
      return this->_integrate<MeshType, TinyVector<1>>();
    }
    case 2: {
      return this->_integrate<MeshType, TinyVector<2>>();
    }
    case 3: {
      return this->_integrate<MeshType, TinyVector<3>>();
    }
      // LCOV_EXCL_START
    default: {
      std::ostringstream os;
      os << "invalid vector dimension " << rang::fgB::red << data_type.dimension() << rang::style::reset;

      throw UnexpectedError(os.str());
    }
      // LCOV_EXCL_STOP
    }
  }
  case ASTNodeDataType::matrix_t: {
    Assert(data_type.numberOfColumns() == data_type.numberOfRows(), "undefined matrix type");
    switch (data_type.numberOfColumns()) {
    case 1: {
      return this->_integrate<MeshType, TinyMatrix<1>>();
    }
    case 2: {
      return this->_integrate<MeshType, TinyMatrix<2>>();
    }
    case 3: {
      return this->_integrate<MeshType, TinyMatrix<3>>();
    }
      // LCOV_EXCL_START
    default: {
      std::ostringstream os;
      os << "invalid vector dimension " << rang::fgB::red << data_type.dimension() << rang::style::reset;

      throw UnexpectedError(os.str());
    }
      // LCOV_EXCL_STOP
    }
  }
    // LCOV_EXCL_START
  default: {
    std::ostringstream os;
    os << "invalid integrate value type: " << rang::fgB::red << dataTypeName(data_type) << rang::style::reset;

    throw UnexpectedError(os.str());
  }
    // LCOV_EXCL_STOP
  }
}

DiscreteFunctionVariant
DiscreteFunctionIntegrator::integrate() const
{
  return std::visit(
    [&](auto&& mesh) {
      using MeshType = mesh_type_t<decltype(mesh)>;
      if constexpr (is_polygonal_mesh_v<MeshType>) {
        return this->_integrate<MeshType>();
      } else {
        throw NormalError("unexpected mesh type");
      }
    },
    m_mesh->variant());
}
