#ifndef DISCRETE_FUNCTION_INTEGRATOR_HPP
#define DISCRETE_FUNCTION_INTEGRATOR_HPP

#include <analysis/IQuadratureDescriptor.hpp>
#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IZoneDescriptor.hpp>
#include <mesh/MeshTraits.hpp>

#include <memory>

class DiscreteFunctionVariant;
class MeshVariant;

class DiscreteFunctionIntegrator
{
 private:
  std::shared_ptr<const MeshVariant> m_mesh;
  const std::vector<std::shared_ptr<const IZoneDescriptor>> m_zone_list;
  std::shared_ptr<const IQuadratureDescriptor> m_quadrature_descriptor;
  const FunctionSymbolId m_function_id;

  template <MeshConcept MeshType, typename DataType, typename ValueType = DataType>
  DiscreteFunctionVariant _integrateOnZoneList() const;

  template <MeshConcept MeshType, typename DataType, typename ValueType = DataType>
  DiscreteFunctionVariant _integrateGlobally() const;

  template <MeshConcept MeshType, typename DataType, typename ValueType = DataType>
  DiscreteFunctionVariant _integrate() const;

  template <MeshConcept MeshType>
  DiscreteFunctionVariant _integrate() const;

 public:
  DiscreteFunctionVariant integrate() const;

  DiscreteFunctionIntegrator(const std::shared_ptr<const MeshVariant>& mesh,
                             const std::shared_ptr<const IQuadratureDescriptor>& quadrature_descriptor,
                             const FunctionSymbolId& function_id)
    : m_mesh{mesh}, m_quadrature_descriptor{quadrature_descriptor}, m_function_id{function_id}
  {}

  DiscreteFunctionIntegrator(const std::shared_ptr<const MeshVariant>& mesh,
                             const std::vector<std::shared_ptr<const IZoneDescriptor>>& zone_list,
                             const std::shared_ptr<const IQuadratureDescriptor>& quadrature_descriptor,
                             const FunctionSymbolId& function_id)
    : m_mesh{mesh}, m_zone_list{zone_list}, m_quadrature_descriptor{quadrature_descriptor}, m_function_id{function_id}
  {}

  DiscreteFunctionIntegrator(const DiscreteFunctionIntegrator&) = delete;
  DiscreteFunctionIntegrator(DiscreteFunctionIntegrator&&)      = delete;

  ~DiscreteFunctionIntegrator() = default;
};

#endif   // DISCRETE_FUNCTION_INTEGRATOR_HPP
