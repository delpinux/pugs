#ifndef INFLOW_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define INFLOW_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>

class InflowBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "inflow(" << ',' << *m_boundary_descriptor << ")";
    return os;
  }

  const FunctionSymbolId m_function_symbol_id;

 public:
  FunctionSymbolId
  functionSymbolId() const
  {
    return m_function_symbol_id;
  }

  Type
  type() const final
  {
    return Type::inflow;
  }

  InflowBoundaryConditionDescriptor(const std::shared_ptr<const IBoundaryDescriptor>& boundary_descriptor,
                                    const FunctionSymbolId& function_symbol_id)
    : BoundaryConditionDescriptorBase{boundary_descriptor}, m_function_symbol_id{function_symbol_id}
  {
    ;
  }

  InflowBoundaryConditionDescriptor(const InflowBoundaryConditionDescriptor&) = delete;
  InflowBoundaryConditionDescriptor(InflowBoundaryConditionDescriptor&&)      = delete;

  ~InflowBoundaryConditionDescriptor() = default;
};

#endif   // INFLOW_BOUNDARY_CONDITION_DESCRIPTOR_HPP
