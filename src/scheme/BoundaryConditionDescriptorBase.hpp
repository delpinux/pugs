#ifndef BOUNDARY_CONDITION_DESCRIPTOR_BASE_HPP
#define BOUNDARY_CONDITION_DESCRIPTOR_BASE_HPP

#include <scheme/IBoundaryConditionDescriptor.hpp>

class BoundaryConditionDescriptorBase : public IBoundaryConditionDescriptor
{
 protected:
  std::shared_ptr<const IBoundaryDescriptor> m_boundary_descriptor;

 public:
  const std::shared_ptr<const IBoundaryDescriptor>&
  boundaryDescriptor_shared() const final
  {
    return m_boundary_descriptor;
  }

  const IBoundaryDescriptor&
  boundaryDescriptor() const final
  {
    return *m_boundary_descriptor;
  }

  BoundaryConditionDescriptorBase(std::shared_ptr<const IBoundaryDescriptor> boundary_descriptor)
    : m_boundary_descriptor{boundary_descriptor}
  {}

  BoundaryConditionDescriptorBase(const BoundaryConditionDescriptorBase&) = delete;
  BoundaryConditionDescriptorBase(BoundaryConditionDescriptorBase&&)      = delete;

  virtual ~BoundaryConditionDescriptorBase() = default;
};

#endif   // BOUNDARY_CONDITION_DESCRIPTOR_BASE_HPP
