#include <scheme/FluxingAdvectionSolver.hpp>

#include <analysis/GaussQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/PrismTransformation.hpp>
#include <language/utils/EvaluateAtPoints.hpp>
#include <language/utils/InterpolateItemArray.hpp>
#include <language/utils/InterpolateItemValue.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemArrayUtils.hpp>
#include <mesh/ItemValueUtils.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/MeshFaceBoundary.hpp>
#include <mesh/MeshFlatFaceBoundary.hpp>
#include <mesh/MeshNodeBoundary.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/SubItemValuePerItem.hpp>
#include <scheme/DiscreteFunctionP0.hpp>
#include <scheme/DiscreteFunctionUtils.hpp>
#include <scheme/IDiscreteFunctionDescriptor.hpp>
#include <scheme/InflowBoundaryConditionDescriptor.hpp>
#include <scheme/OutflowBoundaryConditionDescriptor.hpp>
#include <scheme/SymmetryBoundaryConditionDescriptor.hpp>

#include <variant>
#include <vector>

template <MeshConcept MeshType>
class FluxingAdvectionSolver
{
 private:
  static constexpr size_t Dimension = MeshType::Dimension;

  using Rd = TinyVector<Dimension>;

  const std::shared_ptr<const MeshType> m_old_mesh;
  const std::shared_ptr<const MeshType> m_new_mesh;

  using RemapVariant = std::variant<CellValue<double>,
                                    CellValue<TinyVector<1>>,
                                    CellValue<TinyVector<2>>,
                                    CellValue<TinyVector<3>>,
                                    CellValue<TinyMatrix<1>>,
                                    CellValue<TinyMatrix<2>>,
                                    CellValue<TinyMatrix<3>>,

                                    CellArray<double>>;

  template <typename DataType>
  class InflowValueBoundaryCondition;
  class InflowArrayBoundaryCondition;
  class OutflowBoundaryCondition;
  class SymmetryBoundaryCondition;

  using BoundaryConditionVariant = std::variant<InflowValueBoundaryCondition<double>,   //
                                                InflowValueBoundaryCondition<TinyVector<1>>,
                                                InflowValueBoundaryCondition<TinyVector<2>>,
                                                InflowValueBoundaryCondition<TinyVector<3>>,
                                                InflowValueBoundaryCondition<TinyMatrix<1>>,
                                                InflowValueBoundaryCondition<TinyMatrix<2>>,
                                                InflowValueBoundaryCondition<TinyMatrix<3>>,
                                                InflowArrayBoundaryCondition,
                                                OutflowBoundaryCondition,
                                                SymmetryBoundaryCondition>;

  std::vector<RemapVariant> m_remapped_list;
  std::vector<std::vector<BoundaryConditionVariant>> m_boundary_condition_list;

  FaceValue<const CellId> m_donnor_cell;
  FaceValue<const double> m_cycle_fluxing_volume;
  size_t m_number_of_cycles;

  FaceValue<double> _computeAlgebraicFluxingVolume();
  void _computeDonorCells(FaceValue<const double> algebraic_fluxing_volumes);
  FaceValue<double> _computeFluxingVolume(FaceValue<double> algebraic_fluxing_volumes);
  void _computeCycleNumber(FaceValue<double> fluxing_volumes);
  void _computeGeometricalData();

  template <typename DataType>
  void
  _storeValues(const DiscreteFunctionP0<const DataType>& old_q)
  {
    m_remapped_list.emplace_back(copy(old_q.cellValues()));
  }

  void
  _storeValues(const DiscreteFunctionP0Vector<const double>& old_q)
  {
    m_remapped_list.emplace_back(copy(old_q.cellArrays()));
  }

  template <typename DataType>
  void
  _storeValueBCList(const size_t i_quantity,
                    const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>& bc_list)
  {
    const Mesh<Dimension>& mesh = *m_old_mesh;

    for (auto& i_bc : bc_list) {
      switch (i_bc->type()) {
      case IBoundaryConditionDescriptor::Type::symmetry: {
        m_boundary_condition_list[i_quantity].emplace_back(
          SymmetryBoundaryCondition(getMeshFlatFaceBoundary(mesh, i_bc->boundaryDescriptor())));
        break;
      }
      case IBoundaryConditionDescriptor::Type::outflow: {
        m_boundary_condition_list[i_quantity].emplace_back(
          OutflowBoundaryCondition(getMeshFaceBoundary(mesh, i_bc->boundaryDescriptor())));
        break;
      }
      case IBoundaryConditionDescriptor::Type::inflow: {
        const InflowBoundaryConditionDescriptor& inflow_bc_descriptor =
          dynamic_cast<const InflowBoundaryConditionDescriptor&>(*i_bc);

        MeshFaceBoundary mesh_face_boundary = getMeshFaceBoundary(mesh, inflow_bc_descriptor.boundaryDescriptor());

        FaceValue<const Rd> xl = MeshDataManager::instance().getMeshData(*m_old_mesh).xl();
        Array<const DataType> value_list =
          InterpolateItemValue<DataType(Rd)>::template interpolate<ItemType::face>(inflow_bc_descriptor
                                                                                     .functionSymbolId(),
                                                                                   xl, mesh_face_boundary.faceList());

        m_boundary_condition_list[i_quantity].emplace_back(
          InflowValueBoundaryCondition<DataType>{mesh_face_boundary, value_list});
        break;
      }
      default: {
        std::ostringstream error_msg;
        error_msg << "invalid boundary condition for advection: " << *i_bc;
        throw NormalError(error_msg.str());
      }
      }
    }
  }

  void
  _storeArrayBCList(const size_t i_quantity,
                    const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>& bc_list)
  {
    const Mesh<Dimension>& mesh = *m_old_mesh;

    for (auto& i_bc : bc_list) {
      switch (i_bc->type()) {
      case IBoundaryConditionDescriptor::Type::symmetry: {
        m_boundary_condition_list[i_quantity].emplace_back(
          SymmetryBoundaryCondition(getMeshFlatFaceBoundary(mesh, i_bc->boundaryDescriptor())));
        break;
      }
      case IBoundaryConditionDescriptor::Type::outflow: {
        m_boundary_condition_list[i_quantity].emplace_back(
          OutflowBoundaryCondition(getMeshFaceBoundary(mesh, i_bc->boundaryDescriptor())));
        break;
      }
      case IBoundaryConditionDescriptor::Type::inflow: {
        const InflowBoundaryConditionDescriptor& inflow_bc_descriptor =
          dynamic_cast<const InflowBoundaryConditionDescriptor&>(*i_bc);

        MeshFaceBoundary mesh_face_boundary = getMeshFaceBoundary(mesh, inflow_bc_descriptor.boundaryDescriptor());

        FaceValue<const Rd> xl = MeshDataManager::instance().getMeshData(*m_old_mesh).xl();
        Table<const double> array_list =
          InterpolateItemArray<double(Rd)>::template interpolate<ItemType::face>({inflow_bc_descriptor
                                                                                    .functionSymbolId()},
                                                                                 xl, mesh_face_boundary.faceList());

        m_boundary_condition_list[i_quantity].emplace_back(
          InflowArrayBoundaryCondition{mesh_face_boundary, array_list});
        break;
      }
      default: {
        std::ostringstream error_msg;
        error_msg << "invalid boundary condition for advection: " << *i_bc;
        throw NormalError(error_msg.str());
      }
      }
    }
  }

  template <typename CellDataType>
  void _remapOne(const CellValue<const double>& step_Vj,
                 const std::vector<BoundaryConditionVariant>& q_bc_list,
                 CellDataType& old_q);

  void _remapAllQuantities();

 public:
  std::vector<std::shared_ptr<const DiscreteFunctionVariant>>   //
  remap(const std::vector<std::shared_ptr<const VariableBCDescriptor>>& quantity_list_with_bc);

  FluxingAdvectionSolver(const std::shared_ptr<const MeshType>& i_old_mesh,
                         const std::shared_ptr<const MeshType>& i_new_mesh)
    : m_old_mesh{i_old_mesh}, m_new_mesh{i_new_mesh}
  {
    if ((m_old_mesh.use_count() == 0) or (m_new_mesh.use_count() == 0)) {
      throw NormalError("old and new meshes must be of same type");
    }

    if (m_new_mesh->connectivity().id() != m_old_mesh->connectivity().id()) {
      throw NormalError("old and new meshes must share the same connectivity");
    }

    this->_computeGeometricalData();
  }

  ~FluxingAdvectionSolver() = default;
};

template <MeshConcept MeshType>
void
FluxingAdvectionSolver<MeshType>::_computeDonorCells(FaceValue<const double> algebraic_fluxing_volumes)
{
  m_donnor_cell = [&] {
    const FaceValuePerCell<const bool> cell_face_is_reversed = m_new_mesh->connectivity().cellFaceIsReversed();
    const auto face_to_cell_matrix                           = m_new_mesh->connectivity().faceToCellMatrix();

    const auto face_local_number_in_their_cells = m_new_mesh->connectivity().faceLocalNumbersInTheirCells();

    FaceValue<CellId> donnor_cell{m_old_mesh->connectivity()};
    parallel_for(
      m_new_mesh->numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
        const auto& face_to_cell    = face_to_cell_matrix[face_id];
        const size_t i_face_in_cell = face_local_number_in_their_cells[face_id][0];
        const CellId cell_id        = face_to_cell[0];
        if (cell_face_is_reversed[cell_id][i_face_in_cell] xor (algebraic_fluxing_volumes[face_id] <= 0)) {
          donnor_cell[face_id] = cell_id;
        } else {
          if (face_to_cell.size() == 2) {
            donnor_cell[face_id] = face_to_cell[1];
          } else {
            donnor_cell[face_id] = std::numeric_limits<CellId::base_type>::max();
          }
        }
      });

    return donnor_cell;
  }();
}

template <>
void
FluxingAdvectionSolver<Mesh<1>>::_computeDonorCells(FaceValue<const double> algebraic_fluxing_volumes)
{
  m_donnor_cell = [&] {
    const auto face_to_cell_matrix = m_new_mesh->connectivity().faceToCellMatrix();
    const auto cell_to_face_matrix = m_new_mesh->connectivity().cellToFaceMatrix();

    const auto face_local_number_in_their_cells = m_new_mesh->connectivity().faceLocalNumbersInTheirCells();

    FaceValue<CellId> donnor_cell{m_old_mesh->connectivity()};
    parallel_for(
      m_new_mesh->numberOfFaces(), PUGS_LAMBDA(const FaceId face_id) {
        const auto& face_to_cell = face_to_cell_matrix[face_id];
        const CellId cell_id     = face_to_cell[0];
        if (face_to_cell.size() == 1) {
          if ((algebraic_fluxing_volumes[face_id] <= 0) xor (cell_to_face_matrix[cell_id][1] == face_id)) {
            donnor_cell[face_id] = std::numeric_limits<CellId::base_type>::max();
          } else {
            donnor_cell[face_id] = cell_id;
          }
        } else {
          if ((algebraic_fluxing_volumes[face_id] <= 0) xor (cell_to_face_matrix[cell_id][0] == face_id)) {
            donnor_cell[face_id] = cell_id;
          } else {
            donnor_cell[face_id] = face_to_cell[1];
          }
        }
      });

    return donnor_cell;
  }();
}

template <>
FaceValue<double>
FluxingAdvectionSolver<Mesh<1>>::_computeAlgebraicFluxingVolume()
{
  Array<double> fluxing_volumes{m_new_mesh->numberOfNodes()};
  NodeValue<double> nodal_fluxing_volume(m_new_mesh->connectivity(), fluxing_volumes);
  auto old_xr = m_old_mesh->xr();
  auto new_xr = m_new_mesh->xr();

  parallel_for(
    m_new_mesh->numberOfNodes(),
    PUGS_LAMBDA(NodeId node_id) { nodal_fluxing_volume[node_id] = new_xr[node_id][0] - old_xr[node_id][0]; });

  FaceValue<double> algebraic_fluxing_volumes(m_new_mesh->connectivity(), fluxing_volumes);

  synchronize(algebraic_fluxing_volumes);
  return algebraic_fluxing_volumes;
}

template <>
FaceValue<double>
FluxingAdvectionSolver<Mesh<2>>::_computeAlgebraicFluxingVolume()
{
  const auto face_to_node_matrix = m_old_mesh->connectivity().faceToNodeMatrix();
  FaceValue<double> algebraic_fluxing_volume(m_new_mesh->connectivity());
  auto old_xr = m_old_mesh->xr();
  auto new_xr = m_new_mesh->xr();
  parallel_for(
    m_new_mesh->numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
      const auto& face_to_node = face_to_node_matrix[face_id];

      const Rd& x0 = old_xr[face_to_node[0]];
      const Rd& x1 = old_xr[face_to_node[1]];
      const Rd& x2 = new_xr[face_to_node[1]];
      const Rd& x3 = new_xr[face_to_node[0]];

      TinyMatrix<2> M(x3[0] - x1[0], x2[0] - x0[0],   //
                      x3[1] - x1[1], x2[1] - x0[1]);

      algebraic_fluxing_volume[face_id] = 0.5 * det(M);
    });

  synchronize(algebraic_fluxing_volume);
  return algebraic_fluxing_volume;
}

template <>
FaceValue<double>
FluxingAdvectionSolver<Mesh<3>>::_computeAlgebraicFluxingVolume()
{
  // due to the z component of the jacobian determinant, degree 3
  // polynomials must be exactly integrated
  const QuadratureFormula<3>& gauss = QuadratureManager::instance().getPrismFormula(GaussQuadratureDescriptor(3));

  const auto face_to_node_matrix = m_old_mesh->connectivity().faceToNodeMatrix();
  FaceValue<double> algebraic_fluxing_volume(m_new_mesh->connectivity());
  auto old_xr = m_old_mesh->xr();
  auto new_xr = m_new_mesh->xr();
  parallel_for(
    m_new_mesh->numberOfFaces(), PUGS_LAMBDA(FaceId face_id) {
      const auto& face_to_node = face_to_node_matrix[face_id];

      const size_t face_nb_node = face_to_node.size();

      if (face_nb_node == 3) {
        const Rd& x0 = old_xr[face_to_node[0]];
        const Rd& x1 = old_xr[face_to_node[1]];
        const Rd& x2 = old_xr[face_to_node[2]];

        const Rd& x3 = new_xr[face_to_node[0]];
        const Rd& x4 = new_xr[face_to_node[1]];
        const Rd& x5 = new_xr[face_to_node[2]];

        double volume = 0;

        PrismTransformation T(x0, x1, x2, x3, x4, x5);
        for (size_t i = 0; i < gauss.numberOfPoints(); ++i) {
          volume += gauss.weight(i) * T.jacobianDeterminant(gauss.point(i));
        }

        algebraic_fluxing_volume[face_id] = volume;
      } else {
        Rd xg_old = zero;
        for (size_t i_node = 0; i_node < face_nb_node; ++i_node) {
          xg_old += old_xr[face_to_node[i_node]];
        }
        xg_old *= 1. / face_nb_node;
        Rd xg_new = zero;
        for (size_t i_node = 0; i_node < face_nb_node; ++i_node) {
          xg_new += new_xr[face_to_node[i_node]];
        }
        xg_new *= 1. / face_nb_node;

        double volume = 0;
        for (size_t i_node = 0; i_node < face_nb_node; ++i_node) {
          PrismTransformation T(xg_old,                                              //
                                old_xr[face_to_node[i_node]],                        //
                                old_xr[face_to_node[(i_node + 1) % face_nb_node]],   //
                                xg_new,                                              //
                                new_xr[face_to_node[i_node]],                        //
                                new_xr[face_to_node[(i_node + 1) % face_nb_node]]);
          for (size_t i = 0; i < gauss.numberOfPoints(); ++i) {
            volume += gauss.weight(i) * T.jacobianDeterminant(gauss.point(i));
          }
        }

        algebraic_fluxing_volume[face_id] = volume;
      }
    });

  return algebraic_fluxing_volume;
}

template <MeshConcept MeshType>
FaceValue<double>
FluxingAdvectionSolver<MeshType>::_computeFluxingVolume(FaceValue<double> algebraic_fluxing_volumes)
{
  Assert(m_donnor_cell.isBuilt());
  // Now that donnor cells are clearly defined, we consider the
  // non-algebraic volumes of fluxing
  parallel_for(
    algebraic_fluxing_volumes.numberOfItems(), PUGS_LAMBDA(const FaceId face_id) {
      algebraic_fluxing_volumes[face_id] = std::abs(algebraic_fluxing_volumes[face_id]);
    });

  return algebraic_fluxing_volumes;
}

template <MeshConcept MeshType>
void
FluxingAdvectionSolver<MeshType>::_computeCycleNumber(FaceValue<double> fluxing_volumes)
{
  const auto cell_to_face_matrix = m_old_mesh->connectivity().cellToFaceMatrix();

  const CellValue<double> total_negative_flux(m_old_mesh->connectivity());
  total_negative_flux.fill(0);

  parallel_for(
    m_old_mesh->numberOfCells(), PUGS_CLASS_LAMBDA(CellId cell_id) {
      const auto& cell_to_face = cell_to_face_matrix[cell_id];
      for (size_t i_face = 0; i_face < cell_to_face.size(); ++i_face) {
        FaceId face_id = cell_to_face[i_face];
        if (cell_id == m_donnor_cell[face_id]) {
          total_negative_flux[cell_id] += fluxing_volumes[face_id];
        }
      }
    });

  MeshData<MeshType>& mesh_data    = MeshDataManager::instance().getMeshData(*m_old_mesh);
  const CellValue<const double> Vj = mesh_data.Vj();
  CellValue<size_t> ratio(m_old_mesh->connectivity());

  parallel_for(
    m_old_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
      ratio[cell_id] = (1. / 0.6) * std::ceil(total_negative_flux[cell_id] / Vj[cell_id]);
    });
  synchronize(ratio);

  size_t number_of_cycles = max(ratio);

  if (number_of_cycles > 1) {
    const double cycle_ratio = 1. / number_of_cycles;

    parallel_for(
      fluxing_volumes.numberOfItems(), PUGS_LAMBDA(const FaceId face_id) { fluxing_volumes[face_id] *= cycle_ratio; });
  }

  m_number_of_cycles     = number_of_cycles;
  m_cycle_fluxing_volume = fluxing_volumes;
}

template <MeshConcept MeshType>
void
FluxingAdvectionSolver<MeshType>::_computeGeometricalData()
{
  auto fluxing_volumes = this->_computeAlgebraicFluxingVolume();
  this->_computeDonorCells(fluxing_volumes);
  fluxing_volumes = this->_computeFluxingVolume(fluxing_volumes);
  this->_computeCycleNumber(fluxing_volumes);
}

template <MeshConcept MeshType>
template <typename CellDataType>
void
FluxingAdvectionSolver<MeshType>::_remapOne(const CellValue<const double>& step_Vj,
                                            const std::vector<BoundaryConditionVariant>& q_bc_list,
                                            CellDataType& old_q)
{
  using DataType = std::decay_t<typename CellDataType::data_type>;

  static_assert(is_item_value_v<CellDataType> or is_item_array_v<CellDataType>, "invalid data type");

  const auto cell_to_face_matrix = m_new_mesh->connectivity().cellToFaceMatrix();
  const auto face_to_cell_matrix = m_new_mesh->connectivity().faceToCellMatrix();

  auto new_q = copy(old_q);

  if constexpr (is_item_value_v<CellDataType>) {
    parallel_for(
      m_new_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { new_q[cell_id] *= step_Vj[cell_id]; });
  } else if constexpr (is_item_array_v<CellDataType>) {
    parallel_for(
      m_new_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
        auto new_array      = new_q[cell_id];
        const double volume = step_Vj[cell_id];

        for (size_t i = 0; i < new_array.size(); ++i) {
          new_array[i] *= volume;
        }
      });
  }

  // First we deal with inner faces
  parallel_for(
    m_new_mesh->numberOfCells(), PUGS_CLASS_LAMBDA(CellId cell_id) {
      const auto& cell_to_face = cell_to_face_matrix[cell_id];
      for (size_t i_face = 0; i_face < cell_to_face.size(); ++i_face) {
        const FaceId face_id        = cell_to_face[i_face];
        const double fluxing_volume = m_cycle_fluxing_volume[face_id];

        const auto& face_to_cell = face_to_cell_matrix[face_id];

        if (face_to_cell.size() == 1) {
          continue;
        }

        CellId donnor_id = m_donnor_cell[face_id];

        if constexpr (is_item_value_v<CellDataType>) {
          auto fluxed_q = old_q[donnor_id];
          fluxed_q *= ((donnor_id == cell_id) ? -1 : 1) * fluxing_volume;

          new_q[cell_id] += fluxed_q;
        } else if constexpr (is_item_array_v<CellDataType>) {
          const double sign   = ((donnor_id == cell_id) ? -1 : 1);
          auto old_cell_array = old_q[donnor_id];
          auto new_cell_array = new_q[cell_id];
          for (size_t i = 0; i < new_cell_array.size(); ++i) {
            new_cell_array[i] += (sign * fluxing_volume) * old_cell_array[i];
          }
        }
      }
    });

  // Now we deal with boundary faces
  for (const auto& bc_descriptor : q_bc_list) {
    std::visit(
      [&](auto&& bc) {
        using TypeOfBC = std::decay_t<decltype(bc)>;

        if constexpr (std::is_same_v<TypeOfBC, SymmetryBoundaryCondition>) {
          auto face_list = bc.faceList();
          for (size_t i_face = 0; i_face < face_list.size(); ++i_face) {
            const FaceId face_id        = face_list[i_face];
            const double fluxing_volume = m_cycle_fluxing_volume[face_id];
            const CellId face_cell_id   = face_to_cell_matrix[face_id][0];
            if (fluxing_volume > 1E-12 * step_Vj[face_cell_id]) {
              std::ostringstream error_msg;
              error_msg << "invalid symmetry for face " << face_id
                        << " (number=" << m_old_mesh->connectivity().faceNumber()[face_id] << ")\n"
                        << "face has non zero fluxing volume " << fluxing_volume
                        << " (cell volume = " << step_Vj[face_cell_id] << ")\n";
              throw NormalError(error_msg.str());
            }
          }
        } else if constexpr (std::is_same_v<TypeOfBC, OutflowBoundaryCondition>) {
          auto face_list = bc.faceList();
          for (size_t i_face = 0; i_face < face_list.size(); ++i_face) {
            const FaceId face_id        = face_list[i_face];
            const CellId cell_id        = m_donnor_cell[face_id];
            const double fluxing_volume = m_cycle_fluxing_volume[face_id];
            if (cell_id != std::numeric_limits<CellId::base_type>::max()) {
              if constexpr (is_item_array_v<CellDataType>) {
                for (size_t i = 0; i < new_q[cell_id].size(); ++i) {
                  new_q[cell_id][i] -= fluxing_volume * old_q[cell_id][i];
                }
              } else {
                new_q[cell_id] -= fluxing_volume * old_q[cell_id];
              }
            } else {
              const CellId face_cell_id = face_to_cell_matrix[face_id][0];
              if (fluxing_volume > 1E-12 * step_Vj[face_cell_id]) {
                std::ostringstream error_msg;
                error_msg << "invalid outflow for face " << face_id
                          << " (number=" << m_old_mesh->connectivity().faceNumber()[face_id] << ")"
                          << "face has inflow fluxing volume " << fluxing_volume
                          << " (cell volume = " << step_Vj[face_cell_id] << ")\n";
                throw NormalError(error_msg.str());
              }
            }
          }
        } else if constexpr (std::is_same_v<TypeOfBC, InflowValueBoundaryCondition<DataType>>) {
          if constexpr (is_item_value_v<CellDataType>) {
            auto face_list  = bc.faceList();
            auto value_list = bc.valueList();
            for (size_t i_face = 0; i_face < face_list.size(); ++i_face) {
              const FaceId face_id        = face_list[i_face];
              const CellId cell_id        = m_donnor_cell[face_id];
              const double fluxing_volume = m_cycle_fluxing_volume[face_id];
              if (cell_id == std::numeric_limits<CellId::base_type>::max()) {
                Assert(face_to_cell_matrix[face_id].size() == 1, "boundary face must be connected to a single cell");
                const CellId face_cell_id = face_to_cell_matrix[face_id][0];
                new_q[face_cell_id] += fluxing_volume * value_list[i_face];
              } else {
                const CellId face_cell_id = face_to_cell_matrix[face_id][0];
                if (fluxing_volume > 1E-12 * step_Vj[face_cell_id]) {
                  std::ostringstream error_msg;
                  error_msg << "invalid inflow for face " << face_id
                            << " (number=" << m_old_mesh->connectivity().faceNumber()[face_id] << ")"
                            << "face has outflow fluxing volume " << fluxing_volume
                            << " (cell volume = " << step_Vj[face_cell_id] << ")\n";
                  throw NormalError(error_msg.str());
                }
              }
            }
          } else {
            throw UnexpectedError("invalid boundary condition for fluxing advection");
          }
        } else if constexpr (std::is_same_v<TypeOfBC, InflowArrayBoundaryCondition>) {
          if constexpr (is_item_array_v<CellDataType>) {
            auto face_list  = bc.faceList();
            auto array_list = bc.arrayList();
            for (size_t i_face = 0; i_face < face_list.size(); ++i_face) {
              const FaceId face_id        = face_list[i_face];
              const CellId cell_id        = m_donnor_cell[face_id];
              const double fluxing_volume = m_cycle_fluxing_volume[face_id];
              if (cell_id == std::numeric_limits<CellId::base_type>::max()) {
                Assert(face_to_cell_matrix[face_id].size() == 1, "boundary face must be connected to a single cell");
                const CellId face_cell_id = face_to_cell_matrix[face_id][0];
                auto new_array            = new_q[face_cell_id];
                const auto& bc_array      = array_list[i_face];

                for (size_t i = 0; i < new_array.size(); ++i) {
                  new_array[i] += fluxing_volume * bc_array[i];
                }
              } else {
                const CellId face_cell_id = face_to_cell_matrix[face_id][0];
                if (fluxing_volume > 1E-12 * step_Vj[face_cell_id]) {
                  std::ostringstream error_msg;
                  error_msg << "invalid inflow for face " << face_id
                            << " (number=" << m_old_mesh->connectivity().faceNumber()[face_id] << ")"
                            << "face has outflow fluxing volume " << fluxing_volume
                            << " (cell volume = " << step_Vj[face_cell_id] << ")\n";
                  throw NormalError(error_msg.str());
                }
              }
            }
          } else {
            throw UnexpectedError("invalid boundary condition for fluxing advection");
          }
        } else {
          throw UnexpectedError("invalid boundary condition for fluxing advection");
        }
      },
      bc_descriptor);
  }

  synchronize(new_q);
  old_q = new_q;
}

template <MeshConcept MeshType>
void
FluxingAdvectionSolver<MeshType>::_remapAllQuantities()
{
  const auto cell_to_face_matrix              = m_new_mesh->connectivity().cellToFaceMatrix();
  const auto face_local_number_in_their_cells = m_new_mesh->connectivity().faceLocalNumbersInTheirCells();

  MeshData<MeshType>& old_mesh_data = MeshDataManager::instance().getMeshData(*m_old_mesh);

  const CellValue<const double> old_Vj = old_mesh_data.Vj();
  const CellValue<double> step_Vj      = copy(old_Vj);
  for (size_t jstep = 0; jstep < m_number_of_cycles; ++jstep) {
    for (size_t i = 0; i < m_remapped_list.size(); ++i) {
      Assert(m_remapped_list.size() == m_boundary_condition_list.size());
      auto& remapped_q = m_remapped_list[i];
      auto& q_bc_list  = m_boundary_condition_list[i];
      std::visit([&](auto&& old_q) { this->_remapOne(step_Vj, q_bc_list, old_q); }, remapped_q);
    }

    parallel_for(
      m_new_mesh->numberOfCells(), PUGS_CLASS_LAMBDA(const CellId cell_id) {
        const auto& cell_to_face = cell_to_face_matrix[cell_id];
        for (size_t i_face = 0; i_face < cell_to_face.size(); ++i_face) {
          const FaceId face_id = cell_to_face[i_face];
          CellId donnor_id     = m_donnor_cell[face_id];

          double flux = ((donnor_id == cell_id) ? -1 : 1) * m_cycle_fluxing_volume[face_id];
          step_Vj[cell_id] += flux;
        }
      });

    synchronize(step_Vj);

    CellValue<double> inv_Vj(m_old_mesh->connectivity());
    parallel_for(
      m_new_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { inv_Vj[cell_id] = 1 / step_Vj[cell_id]; });

    for (auto& remapped_q : m_remapped_list) {
      std::visit(
        [&](auto&& new_q) {
          using CellDataType = std::decay_t<decltype(new_q)>;
          static_assert(is_item_value_v<CellDataType> or is_item_array_v<CellDataType>, "invalid data type");

          if constexpr (is_item_value_v<CellDataType>) {
            parallel_for(
              m_new_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { new_q[cell_id] *= inv_Vj[cell_id]; });
          } else if constexpr (is_item_array_v<CellDataType>) {
            parallel_for(
              m_new_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
                auto array              = new_q[cell_id];
                const double inv_volume = inv_Vj[cell_id];

                for (size_t i = 0; i < array.size(); ++i) {
                  array[i] *= inv_volume;
                }
              });
          }
        },
        remapped_q);
    }
  }
}

template <MeshConcept MeshType>
std::vector<std::shared_ptr<const DiscreteFunctionVariant>>
FluxingAdvectionSolver<MeshType>::remap(
  const std::vector<std::shared_ptr<const VariableBCDescriptor>>& quantity_list_with_bc)
{
  m_boundary_condition_list.resize(quantity_list_with_bc.size());
  for (size_t i = 0; i < quantity_list_with_bc.size(); ++i) {
    const auto& quantity_v_with_bc = quantity_list_with_bc[i];
    const auto& quantity_v         = quantity_v_with_bc->discreteFunctionVariant();
    const auto& bc_list            = quantity_v_with_bc->bcDescriptorList();
    std::visit(
      [&](auto&& variable) {
        using DiscreteFunctionT = std::decay_t<decltype(variable)>;
        this->_storeValues(variable);
        if constexpr (is_discrete_function_P0_v<DiscreteFunctionT>) {
          using DataType = std::decay_t<typename DiscreteFunctionT::data_type>;
          this->_storeValueBCList<DataType>(i, bc_list);
        } else if constexpr (is_discrete_function_P0_vector_v<DiscreteFunctionT>) {
          this->_storeArrayBCList(i, bc_list);
        } else {
          throw UnexpectedError("invalid discrete function type");
        }
      },
      quantity_v->discreteFunction());
  }

  this->_remapAllQuantities();

  std::vector<std::shared_ptr<const DiscreteFunctionVariant>> new_variables;

  for (size_t i = 0; i < quantity_list_with_bc.size(); ++i) {
    std::visit(
      [&](auto&& variable) {
        using DiscreteFunctionT = std::decay_t<decltype(variable)>;
        using DataType          = std::decay_t<typename DiscreteFunctionT::data_type>;

        if constexpr (is_discrete_function_P0_v<DiscreteFunctionT>) {
          new_variables.push_back(std::make_shared<DiscreteFunctionVariant>(
            DiscreteFunctionT(m_new_mesh, std::get<CellValue<DataType>>(m_remapped_list[i]))));
        } else if constexpr (is_discrete_function_P0_vector_v<DiscreteFunctionT>) {
          new_variables.push_back(std::make_shared<DiscreteFunctionVariant>(
            DiscreteFunctionT(m_new_mesh, std::get<CellArray<DataType>>(m_remapped_list[i]))));
        } else {
          throw UnexpectedError("invalid discrete function type");
        }
      },
      quantity_list_with_bc[i]->discreteFunctionVariant()->discreteFunction());
  }

  return new_variables;
}

template <MeshConcept MeshType>
template <typename DataType>
class FluxingAdvectionSolver<MeshType>::InflowValueBoundaryCondition
{
 private:
  const MeshFaceBoundary m_mesh_face_boundary;

  Array<const DataType> m_value_list;

 public:
  const Array<const FaceId>&
  faceList() const
  {
    return m_mesh_face_boundary.faceList();
  }

  const Array<const DataType>&
  valueList() const
  {
    return m_value_list;
  }

  InflowValueBoundaryCondition(const MeshFaceBoundary& mesh_face_boundary, Array<const DataType> value_list)
    : m_mesh_face_boundary(mesh_face_boundary), m_value_list(value_list)
  {
    ;
  }

  ~InflowValueBoundaryCondition() = default;
};

template <MeshConcept MeshType>
class FluxingAdvectionSolver<MeshType>::InflowArrayBoundaryCondition
{
 private:
  const MeshFaceBoundary m_mesh_face_boundary;

  Table<const double> m_array_list;

 public:
  const Array<const FaceId>&
  faceList() const
  {
    return m_mesh_face_boundary.faceList();
  }

  const Table<const double>&
  arrayList() const
  {
    return m_array_list;
  }

  InflowArrayBoundaryCondition(const MeshFaceBoundary& mesh_face_boundary, Table<const double> array_list)
    : m_mesh_face_boundary(mesh_face_boundary), m_array_list(array_list)
  {
    ;
  }

  ~InflowArrayBoundaryCondition() = default;
};

template <MeshConcept MeshType>
class FluxingAdvectionSolver<MeshType>::OutflowBoundaryCondition
{
 private:
  const MeshFaceBoundary m_mesh_face_boundary;

 public:
  const Array<const FaceId>&
  faceList() const
  {
    return m_mesh_face_boundary.faceList();
  }

  OutflowBoundaryCondition(const MeshFaceBoundary& mesh_face_boundary) : m_mesh_face_boundary(mesh_face_boundary)
  {
    ;
  }

  ~OutflowBoundaryCondition() = default;
};

template <MeshConcept MeshType>
class FluxingAdvectionSolver<MeshType>::SymmetryBoundaryCondition
{
 private:
  const MeshFlatFaceBoundary<MeshType> m_mesh_flat_face_boundary;

 public:
  const Array<const FaceId>&
  faceList() const
  {
    return m_mesh_flat_face_boundary.faceList();
  }

  SymmetryBoundaryCondition(const MeshFlatFaceBoundary<MeshType>& mesh_flat_face_boundary)
    : m_mesh_flat_face_boundary(mesh_flat_face_boundary)
  {
    ;
  }

  ~SymmetryBoundaryCondition() = default;
};

std::vector<std::shared_ptr<const DiscreteFunctionVariant>>
advectByFluxing(const std::shared_ptr<const MeshVariant> new_mesh_v,
                const std::vector<std::shared_ptr<const VariableBCDescriptor>>& remapped_variables_with_bc)
{
  std::vector<std::shared_ptr<const DiscreteFunctionVariant>> remapped_variables;
  for (auto i_remapped_var_with_bc : remapped_variables_with_bc) {
    remapped_variables.push_back(i_remapped_var_with_bc->discreteFunctionVariant());
  }

  if (not hasSameMesh(remapped_variables)) {
    throw NormalError("remapped quantities are not defined on the same mesh");
  }

  const std::shared_ptr<const MeshVariant> old_mesh_v = getCommonMesh(remapped_variables);

  return std::visit(
    [&](auto&& old_mesh, auto&& new_mesh) -> std::vector<std::shared_ptr<const DiscreteFunctionVariant>> {
      using OldMeshType = mesh_type_t<decltype(old_mesh)>;
      using NewMeshType = mesh_type_t<decltype(new_mesh)>;

      if constexpr (std::is_same_v<OldMeshType, NewMeshType>) {
        return FluxingAdvectionSolver<OldMeshType>{old_mesh, new_mesh}.remap(remapped_variables_with_bc);
      } else {
        throw NormalError("incompatible mesh types");
      }
    },
    old_mesh_v->variant(), new_mesh_v->variant());
}
