#ifndef DISCRETE_FUNCTION_VECTOR_INTEGRATOR_HPP
#define DISCRETE_FUNCTION_VECTOR_INTEGRATOR_HPP

#include <analysis/IQuadratureDescriptor.hpp>
#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IZoneDescriptor.hpp>
#include <mesh/MeshTraits.hpp>
#include <scheme/IDiscreteFunctionDescriptor.hpp>

#include <memory>
#include <vector>

class DiscreteFunctionVariant;
class MeshVariant;

class DiscreteFunctionVectorIntegrator
{
 private:
  std::shared_ptr<const MeshVariant> m_mesh;
  const std::vector<std::shared_ptr<const IZoneDescriptor>> m_zone_list;
  std::shared_ptr<const IQuadratureDescriptor> m_quadrature_descriptor;
  std::shared_ptr<const IDiscreteFunctionDescriptor> m_discrete_function_descriptor;
  const std::vector<FunctionSymbolId> m_function_id_list;

  template <MeshConcept MeshType, typename DataType>
  DiscreteFunctionVariant _integrateOnZoneList() const;

  template <MeshConcept MeshType, typename DataType>
  DiscreteFunctionVariant _integrateGlobally() const;

  template <MeshConcept MeshType, typename DataType>
  DiscreteFunctionVariant _integrate() const;

  template <MeshConcept MeshType>
  DiscreteFunctionVariant _integrate() const;

 public:
  DiscreteFunctionVariant integrate() const;

  DiscreteFunctionVectorIntegrator(
    const std::shared_ptr<const MeshVariant>& mesh,
    const std::shared_ptr<const IQuadratureDescriptor>& quadrature_descriptor,
    const std::shared_ptr<const IDiscreteFunctionDescriptor>& discrete_function_descriptor,
    const std::vector<FunctionSymbolId>& function_id_list)
    : m_mesh{mesh},
      m_quadrature_descriptor(quadrature_descriptor),
      m_discrete_function_descriptor{discrete_function_descriptor},
      m_function_id_list{function_id_list}
  {}

  DiscreteFunctionVectorIntegrator(
    const std::shared_ptr<const MeshVariant>& mesh,
    const std::vector<std::shared_ptr<const IZoneDescriptor>>& zone_list,
    const std::shared_ptr<const IQuadratureDescriptor>& quadrature_descriptor,
    const std::shared_ptr<const IDiscreteFunctionDescriptor>& discrete_function_descriptor,
    const std::vector<FunctionSymbolId>& function_id_list)
    : m_mesh{mesh},
      m_zone_list{zone_list},
      m_quadrature_descriptor(quadrature_descriptor),
      m_discrete_function_descriptor{discrete_function_descriptor},
      m_function_id_list{function_id_list}
  {}

  DiscreteFunctionVectorIntegrator(const DiscreteFunctionVectorIntegrator&) = delete;
  DiscreteFunctionVectorIntegrator(DiscreteFunctionVectorIntegrator&&)      = delete;

  ~DiscreteFunctionVectorIntegrator() = default;
};

#endif   // DISCRETE_FUNCTION_VECTOR_INTEGRATOR_HPP
