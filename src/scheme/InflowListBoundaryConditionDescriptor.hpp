#ifndef INFLOW_LIST_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define INFLOW_LIST_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>

class InflowListBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "inflow_list(" << ',' << *m_boundary_descriptor << ")";
    return os;
  }

  const std::vector<FunctionSymbolId> m_function_symbol_id_list;

 public:
  const std::vector<FunctionSymbolId>&
  functionSymbolIdList() const
  {
    return m_function_symbol_id_list;
  }

  Type
  type() const final
  {
    return Type::inflow_list;
  }

  InflowListBoundaryConditionDescriptor(std::shared_ptr<const IBoundaryDescriptor> boundary_descriptor,
                                        const std::vector<FunctionSymbolId>& function_symbol_id_list)
    : BoundaryConditionDescriptorBase{boundary_descriptor}, m_function_symbol_id_list{function_symbol_id_list}
  {
    ;
  }

  InflowListBoundaryConditionDescriptor(const InflowListBoundaryConditionDescriptor&) = delete;
  InflowListBoundaryConditionDescriptor(InflowListBoundaryConditionDescriptor&&)      = delete;

  ~InflowListBoundaryConditionDescriptor() = default;
};

#endif   // INFLOW_LIST_BOUNDARY_CONDITION_DESCRIPTOR_HPP
