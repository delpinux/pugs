#ifndef DISCRETE_FUNCTION_DESCRIPTOR_P0_HPP
#define DISCRETE_FUNCTION_DESCRIPTOR_P0_HPP

#include <scheme/IDiscreteFunctionDescriptor.hpp>

class DiscreteFunctionDescriptorP0 : public IDiscreteFunctionDescriptor
{
 public:
  DiscreteFunctionType
  type() const final
  {
    return DiscreteFunctionType::P0;
  }

  DiscreteFunctionDescriptorP0() noexcept = default;

  DiscreteFunctionDescriptorP0(const DiscreteFunctionDescriptorP0&) = default;

  DiscreteFunctionDescriptorP0(DiscreteFunctionDescriptorP0&&) noexcept = default;

  ~DiscreteFunctionDescriptorP0() noexcept = default;
};

#endif   // DISCRETE_FUNCTION_DESCRIPTOR_P0_HPP
