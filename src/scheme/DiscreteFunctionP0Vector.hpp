#ifndef DISCRETE_FUNCTION_P0_VECTOR_HPP
#define DISCRETE_FUNCTION_P0_VECTOR_HPP

#include <algebra/Vector.hpp>
#include <mesh/ItemArray.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <scheme/DiscreteFunctionDescriptorP0Vector.hpp>
#include <scheme/DiscreteFunctionP0.hpp>
#include <utils/Exceptions.hpp>

template <typename DataType>
class DiscreteFunctionP0Vector
{
 public:
  using data_type = DataType;

  friend class DiscreteFunctionP0Vector<std::add_const_t<DataType>>;
  friend class DiscreteFunctionP0Vector<std::remove_const_t<DataType>>;

  static_assert(std::is_arithmetic_v<DataType>, "DiscreteFunctionP0Vector are only defined for arithmetic data type");

 private:
  std::shared_ptr<const MeshVariant> m_mesh;
  CellArray<DataType> m_cell_arrays;

  DiscreteFunctionDescriptorP0Vector m_discrete_function_descriptor;

 public:
  PUGS_INLINE
  ASTNodeDataType
  dataType() const
  {
    return ast_node_data_type_from<std::remove_const_t<data_type>>;
  }

  PUGS_INLINE
  size_t
  size() const
  {
    return m_cell_arrays.sizeOfArrays();
  }

  PUGS_INLINE
  const CellArray<DataType>&
  cellArrays() const
  {
    return m_cell_arrays;
  }

  PUGS_INLINE std::shared_ptr<const MeshVariant>
  meshVariant() const
  {
    return m_mesh;
  }

  PUGS_INLINE
  const IDiscreteFunctionDescriptor&
  descriptor() const
  {
    return m_discrete_function_descriptor;
  }

  PUGS_FORCEINLINE
  operator DiscreteFunctionP0Vector<const DataType>() const
  {
    return DiscreteFunctionP0Vector<const DataType>(m_mesh, m_cell_arrays);
  }

  PUGS_INLINE
  void
  fill(const DataType& data) const noexcept
  {
    static_assert(not std::is_const_v<DataType>, "Cannot modify ItemValue of const");
    m_cell_arrays.fill(data);
  }

  PUGS_INLINE DiscreteFunctionP0Vector
  operator=(const DiscreteFunctionP0Vector& f)
  {
    Assert(m_mesh->id() == f.m_mesh->id());
    Assert(this->size() == f.size());
    m_cell_arrays = f.m_cell_arrays;
    return *this;
  }

  friend PUGS_INLINE DiscreteFunctionP0Vector<std::remove_const_t<DataType>>
  copy(const DiscreteFunctionP0Vector& source)
  {
    return DiscreteFunctionP0Vector<std::remove_const_t<DataType>>{source.m_mesh, copy(source.cellArrays())};
  }

  friend PUGS_INLINE void
  copy_to(const DiscreteFunctionP0Vector<DataType>& source,
          DiscreteFunctionP0Vector<std::remove_const_t<DataType>>& destination)
  {
    Assert(source.m_mesh->id() == destination.m_mesh->id());
    copy_to(source.m_cell_arrays, destination.m_cell_arrays);
  }

  PUGS_FORCEINLINE
  typename Table<DataType>::UnsafeRowView
  operator[](const CellId cell_id) const noexcept(NO_ASSERT)
  {
    return m_cell_arrays[cell_id];
  }

  PUGS_INLINE DiscreteFunctionP0Vector<std::remove_const_t<DataType>>
  operator-() const
  {
    Assert(m_cell_arrays.isBuilt());
    DiscreteFunctionP0Vector<std::remove_const_t<DataType>> opposite = copy(*this);

    const size_t size_of_arrays = this->size();
    parallel_for(
      m_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
        auto array = opposite[cell_id];
        for (size_t i = 0; i < size_of_arrays; ++i) {
          array[i] = -array[i];
        }
      });

    return opposite;
  }

  friend DiscreteFunctionP0Vector<std::remove_const_t<DataType>>
  operator+(const DiscreteFunctionP0Vector& f, const DiscreteFunctionP0Vector& g)
  {
    Assert(f.m_mesh->id() == g.m_mesh->id(), "functions are nor defined on the same mesh");
    Assert(f.size() == g.size(), "P0 vectors have different sizes");
    DiscreteFunctionP0Vector<std::remove_const_t<DataType>> sum(f.m_mesh, f.size());
    parallel_for(
      f.m_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
        for (size_t i = 0; i < f.size(); ++i) {
          sum[cell_id][i] = f[cell_id][i] + g[cell_id][i];
        }
      });
    return sum;
  }

  friend DiscreteFunctionP0Vector<std::remove_const_t<DataType>>
  operator-(const DiscreteFunctionP0Vector& f, const DiscreteFunctionP0Vector& g)
  {
    Assert(f.meshVariant()->id() == g.meshVariant()->id(), "functions are nor defined on the same mesh");
    Assert(f.size() == g.size(), "P0 vectors have different sizes");
    DiscreteFunctionP0Vector<std::remove_const_t<DataType>> difference(f.m_mesh, f.size());
    parallel_for(
      f.m_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
        for (size_t i = 0; i < f.size(); ++i) {
          difference[cell_id][i] = f[cell_id][i] - g[cell_id][i];
        }
      });
    return difference;
  }

  template <typename DataType2>
  friend DiscreteFunctionP0Vector<std::remove_const_t<DataType>>
  operator*(const DiscreteFunctionP0<DataType2>& f, const DiscreteFunctionP0Vector& g)
  {
    static_assert(std::is_arithmetic_v<DataType2>, "left operand must be arithmetic");

    Assert(f.meshVariant()->id() == g.meshVariant()->id(), "functions are nor defined on the same mesh");
    DiscreteFunctionP0Vector<std::remove_const_t<DataType>> product(g.m_mesh, g.size());
    const size_t size_of_arrays = g.size();
    parallel_for(
      g.m_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
        const DataType2& f_value = f[cell_id];
        for (size_t i = 0; i < size_of_arrays; ++i) {
          product[cell_id][i] = f_value * g[cell_id][i];
        }
      });
    return product;
  }

  template <typename DataType2>
  friend DiscreteFunctionP0Vector<std::remove_const_t<DataType>>
  operator*(const DataType2& a, const DiscreteFunctionP0Vector& f)
  {
    DiscreteFunctionP0Vector<std::remove_const_t<DataType>> product(f.m_mesh, f.size());
    const size_t size_of_arrays = f.size();
    parallel_for(
      f.m_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
        for (size_t i = 0; i < size_of_arrays; ++i) {
          product[cell_id][i] = a * f[cell_id][i];
        }
      });
    return product;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  sumOfComponents(const DiscreteFunctionP0Vector& f)
  {
    DiscreteFunctionP0<double> result{f.m_mesh};

    parallel_for(
      f.m_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
        const auto& f_cell_id = f[cell_id];

        double sum = 0;
        for (size_t i = 0; i < f.size(); ++i) {
          sum += f_cell_id[i];
        }

        result[cell_id] = sum;
      });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  dot(const DiscreteFunctionP0Vector& f, const DiscreteFunctionP0Vector& g)
  {
    Assert(f.meshVariant()->id() == g.meshVariant()->id(), "functions are nor defined on the same mesh");
    Assert(f.size() == g.size());
    DiscreteFunctionP0<double> result{f.m_mesh};
    parallel_for(
      f.m_mesh->numberOfCells(), PUGS_LAMBDA(CellId cell_id) {
        const auto& f_cell_id = f[cell_id];
        const auto& g_cell_id = g[cell_id];

        double sum = 0;
        for (size_t i = 0; i < f.size(); ++i) {
          sum += f_cell_id[i] * g_cell_id[i];
        }

        result[cell_id] = sum;
      });

    return result;
  }

  DiscreteFunctionP0Vector(const std::shared_ptr<const MeshVariant>& mesh, size_t size)
    : m_mesh{mesh}, m_cell_arrays{mesh->connectivity(), size}
  {}

  DiscreteFunctionP0Vector(const std::shared_ptr<const MeshVariant>& mesh, const CellArray<DataType>& cell_arrays)
    : m_mesh{mesh}, m_cell_arrays{cell_arrays}
  {
    Assert(m_mesh->connectivity().id() == cell_arrays.connectivity_ptr()->id());
  }

  template <MeshConcept MeshType>
  DiscreteFunctionP0Vector(const std::shared_ptr<const MeshType>& p_mesh, size_t size)
    : m_mesh{p_mesh->meshVariant()}, m_cell_arrays{m_mesh->connectivity(), size}
  {}

  template <MeshConcept MeshType>
  DiscreteFunctionP0Vector(const std::shared_ptr<const MeshType>& p_mesh, const CellArray<DataType>& cell_arrays)
    : m_mesh{p_mesh->meshVariant()}, m_cell_arrays{cell_arrays}
  {
    Assert(m_mesh->connectivity().id() == cell_arrays.connectivity_ptr()->id());
  }

  DiscreteFunctionP0Vector(const DiscreteFunctionP0Vector&) noexcept = default;
  DiscreteFunctionP0Vector(DiscreteFunctionP0Vector&&) noexcept      = default;

  ~DiscreteFunctionP0Vector() = default;
};

#endif   // DISCRETE_FUNCTION_P0_VECTOR_HPP
