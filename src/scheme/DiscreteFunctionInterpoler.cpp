#include <scheme/DiscreteFunctionInterpoler.hpp>

#include <language/utils/InterpolateItemValue.hpp>
#include <mesh/MeshCellZone.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>
#include <scheme/DiscreteFunctionP0.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>
#include <utils/Exceptions.hpp>

template <MeshConcept MeshType, typename DataType, typename ValueType>
DiscreteFunctionVariant
DiscreteFunctionInterpoler::_interpolateOnZoneList() const
{
  static_assert(std::is_convertible_v<DataType, ValueType>);
  Assert(m_zone_list.size() > 0, "no zone list provided");

  constexpr size_t Dimension = MeshType::Dimension;

  std::shared_ptr p_mesh        = m_mesh->get<MeshType>();
  MeshData<MeshType>& mesh_data = MeshDataManager::instance().getMeshData(*p_mesh);

  CellValue<bool> is_in_zone{p_mesh->connectivity()};
  is_in_zone.fill(false);

  size_t number_of_cells = 0;
  for (const auto& zone : m_zone_list) {
    auto mesh_cell_zone   = getMeshCellZone(*p_mesh, *zone);
    const auto& cell_list = mesh_cell_zone.cellList();
    for (size_t i_cell = 0; i_cell < cell_list.size(); ++i_cell) {
      const CellId cell_id = cell_list[i_cell];
      if (is_in_zone[cell_id]) {
        std::ostringstream os;
        os << "cell " << cell_id << " (number " << p_mesh->connectivity().cellNumber()[cell_id]
           << ") is present multiple times in zone list";
        throw NormalError(os.str());
      }
      ++number_of_cells;
      is_in_zone[cell_id] = true;
    }
  }

  Array<CellId> zone_cell_list{number_of_cells};
  {
    size_t i_cell = 0;
    for (CellId cell_id = 0; cell_id < p_mesh->numberOfCells(); ++cell_id) {
      if (is_in_zone[cell_id]) {
        zone_cell_list[i_cell++] = cell_id;
      }
    }
  }

  Array<const DataType> array =
    InterpolateItemValue<DataType(TinyVector<Dimension>)>::template interpolate<ItemType::cell>(m_function_id,
                                                                                                mesh_data.xj(),
                                                                                                zone_cell_list);

  CellValue<ValueType> cell_value{p_mesh->connectivity()};
  if constexpr (is_tiny_vector_v<ValueType> or is_tiny_matrix_v<ValueType>) {
    cell_value.fill(zero);
  } else {
    cell_value.fill(0);
  }

  parallel_for(
    zone_cell_list.size(), PUGS_LAMBDA(const size_t i_cell) { cell_value[zone_cell_list[i_cell]] = array[i_cell]; });

  return DiscreteFunctionP0<ValueType>{m_mesh, cell_value};
}

template <MeshConcept MeshType, typename DataType, typename ValueType>
DiscreteFunctionVariant
DiscreteFunctionInterpoler::_interpolateGlobally() const
{
  Assert(m_zone_list.size() == 0, "invalid call when zones are defined");
  constexpr size_t Dimension = MeshType::Dimension;

  std::shared_ptr p_mesh        = m_mesh->get<MeshType>();
  MeshData<MeshType>& mesh_data = MeshDataManager::instance().getMeshData(*p_mesh);

  if constexpr (std::is_same_v<DataType, ValueType>) {
    return DiscreteFunctionP0<ValueType>(m_mesh,
                                         InterpolateItemValue<DataType(TinyVector<Dimension>)>::template interpolate<
                                           ItemType::cell>(m_function_id, mesh_data.xj()));
  } else {
    static_assert(std::is_convertible_v<DataType, ValueType>);

    CellValue<DataType> cell_data =
      InterpolateItemValue<DataType(TinyVector<Dimension>)>::template interpolate<ItemType::cell>(m_function_id,
                                                                                                  mesh_data.xj());

    CellValue<ValueType> cell_value{p_mesh->connectivity()};

    parallel_for(
      p_mesh->numberOfCells(), PUGS_LAMBDA(const CellId cell_id) { cell_value[cell_id] = cell_data[cell_id]; });

    return DiscreteFunctionP0<ValueType>(m_mesh, cell_value);
  }
}

template <MeshConcept MeshType, typename DataType, typename ValueType>
DiscreteFunctionVariant
DiscreteFunctionInterpoler::_interpolate() const
{
  if (m_zone_list.size() == 0) {
    return this->_interpolateGlobally<MeshType, DataType, ValueType>();
  } else {
    return this->_interpolateOnZoneList<MeshType, DataType, ValueType>();
  }
}

template <MeshConcept MeshType>
DiscreteFunctionVariant
DiscreteFunctionInterpoler::_interpolate() const
{
  const auto& function_descriptor = m_function_id.descriptor();
  Assert(function_descriptor.domainMappingNode().children[1]->m_data_type == ASTNodeDataType::typename_t);

  const ASTNodeDataType& data_type = function_descriptor.domainMappingNode().children[1]->m_data_type.contentType();

  switch (data_type) {
  case ASTNodeDataType::bool_t: {
    return this->_interpolate<MeshType, bool, double>();
  }
  case ASTNodeDataType::unsigned_int_t: {
    return this->_interpolate<MeshType, uint64_t, double>();
  }
  case ASTNodeDataType::int_t: {
    return this->_interpolate<MeshType, int64_t, double>();
  }
  case ASTNodeDataType::double_t: {
    return this->_interpolate<MeshType, double>();
  }
  case ASTNodeDataType::vector_t: {
    switch (data_type.dimension()) {
    case 1: {
      return this->_interpolate<MeshType, TinyVector<1>>();
    }
    case 2: {
      return this->_interpolate<MeshType, TinyVector<2>>();
    }
    case 3: {
      return this->_interpolate<MeshType, TinyVector<3>>();
    }
      // LCOV_EXCL_START
    default: {
      std::ostringstream os;
      os << "invalid vector dimension " << rang::fgB::red << data_type.dimension() << rang::style::reset;

      throw UnexpectedError(os.str());
    }
      // LCOV_EXCL_STOP
    }
  }
  case ASTNodeDataType::matrix_t: {
    Assert(data_type.numberOfColumns() == data_type.numberOfRows(), "undefined matrix type");
    switch (data_type.numberOfColumns()) {
    case 1: {
      return this->_interpolate<MeshType, TinyMatrix<1>>();
    }
    case 2: {
      return this->_interpolate<MeshType, TinyMatrix<2>>();
    }
    case 3: {
      return this->_interpolate<MeshType, TinyMatrix<3>>();
    }
      // LCOV_EXCL_START
    default: {
      std::ostringstream os;
      os << "invalid vector dimension " << rang::fgB::red << data_type.dimension() << rang::style::reset;

      throw UnexpectedError(os.str());
    }
      // LCOV_EXCL_STOP
    }
  }
    // LCOV_EXCL_START
  default: {
    std::ostringstream os;
    os << "invalid interpolation value type: " << rang::fgB::red << dataTypeName(data_type) << rang::style::reset;

    throw UnexpectedError(os.str());
  }
    // LCOV_EXCL_STOP
  }
}

DiscreteFunctionVariant
DiscreteFunctionInterpoler::interpolate() const
{
  return std::visit(
    [&](auto&& mesh) {
      using MeshType = mesh_type_t<decltype(mesh)>;

      if constexpr (is_polygonal_mesh_v<MeshType>) {
        return this->_interpolate<MeshType>();
      } else {
        throw NormalError("unexpected mesh type");
      }
    },
    m_mesh->variant());
}
