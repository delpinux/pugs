#ifndef I_DISCRETE_FUNCTION_DESCRIPTOR_HPP
#define I_DISCRETE_FUNCTION_DESCRIPTOR_HPP

#include <scheme/DiscreteFunctionType.hpp>

class IDiscreteFunctionDescriptor
{
 public:
  virtual DiscreteFunctionType type() const = 0;

  IDiscreteFunctionDescriptor() noexcept = default;

  IDiscreteFunctionDescriptor(const IDiscreteFunctionDescriptor&) = default;

  IDiscreteFunctionDescriptor(IDiscreteFunctionDescriptor&&) noexcept = default;

  virtual ~IDiscreteFunctionDescriptor() noexcept = default;
};

#endif   // I_DISCRETE_FUNCTION_DESCRIPTOR_HPP
