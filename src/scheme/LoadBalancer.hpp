#ifndef LOAD_BALANCER_HPP
#define LOAD_BALANCER_HPP

#include <memory>
#include <tuple>
#include <vector>

class MeshVariant;
class DiscreteFunctionVariant;

class LoadBalancer
{
 public:
  std::vector<std::shared_ptr<const DiscreteFunctionVariant>> balance(
    const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& discrete_function_list);

  LoadBalancer()  = default;
  ~LoadBalancer() = default;
};

#endif   // LOAD_BALANCER_HPP
