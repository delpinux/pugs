#ifndef EXTERNAL_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define EXTERNAL_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>
#include <utils/Socket.hpp>

#include <memory>

class ExternalBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "external(" << m_name << ',' << *m_boundary_descriptor << ")";
    return os;
  }

  const std::string m_name;

  const std::shared_ptr<const Socket> m_socket;

 public:
  const std::string&
  name() const
  {
    return m_name;
  }

  const std::shared_ptr<const Socket>&
  socket() const
  {
    return m_socket;
  }

  Type
  type() const final
  {
    return Type::external;
  }

  ExternalBoundaryConditionDescriptor(const std::string& name,
                                      const std::shared_ptr<const IBoundaryDescriptor>& boundary_descriptor,
                                      const std::shared_ptr<const Socket>& socket)
    : BoundaryConditionDescriptorBase{boundary_descriptor}, m_name{name}, m_socket{socket}
  {}

  ExternalBoundaryConditionDescriptor(const ExternalBoundaryConditionDescriptor&) = delete;
  ExternalBoundaryConditionDescriptor(ExternalBoundaryConditionDescriptor&&)      = delete;

  ~ExternalBoundaryConditionDescriptor() = default;
};

#endif   // EXTERNAL_BOUNDARY_CONDITION_DESCRIPTOR_HPP
