#ifndef DISCRETE_FUNCTION_VARIANT_HPP
#define DISCRETE_FUNCTION_VARIANT_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <scheme/DiscreteFunctionP0.hpp>
#include <scheme/DiscreteFunctionP0Vector.hpp>
#include <utils/Demangle.hpp>
#include <utils/Exceptions.hpp>

class DiscreteFunctionVariant
{
 private:
  using Variant = std::variant<DiscreteFunctionP0<const double>,
                               DiscreteFunctionP0<const TinyVector<1>>,
                               DiscreteFunctionP0<const TinyVector<2>>,
                               DiscreteFunctionP0<const TinyVector<3>>,
                               DiscreteFunctionP0<const TinyMatrix<1>>,
                               DiscreteFunctionP0<const TinyMatrix<2>>,
                               DiscreteFunctionP0<const TinyMatrix<3>>,

                               DiscreteFunctionP0Vector<const double>>;

  Variant m_discrete_function;

 public:
  PUGS_INLINE
  const Variant&
  discreteFunction() const
  {
    return m_discrete_function;
  }

  template <typename DiscreteFunctionT>
  PUGS_INLINE auto
  get() const
  {
    static_assert(is_discrete_function_v<DiscreteFunctionT>, "invalid template argument");
    using DataType = typename DiscreteFunctionT::data_type;
    static_assert(std::is_const_v<DataType>, "data type of extracted discrete function must be const");

    if (not std::holds_alternative<DiscreteFunctionT>(this->m_discrete_function)) {
      std::ostringstream error_msg;
      error_msg << "invalid discrete function type\n";
      error_msg << "- required " << rang::fgB::red << demangle<DiscreteFunctionT>() << rang::fg::reset << '\n';
      error_msg << "- contains " << rang::fgB::yellow
                << std::visit([](auto&& f) -> std::string { return demangle<decltype(f)>(); },
                              this->m_discrete_function)
                << rang::fg::reset;
      throw NormalError(error_msg.str());
    }
    return std::get<DiscreteFunctionT>(this->discreteFunction());
  }

  template <typename DataType>
  DiscreteFunctionVariant(const DiscreteFunctionP0<DataType>& discrete_function)
    : m_discrete_function{DiscreteFunctionP0<const DataType>{discrete_function}}
  {
    static_assert(std::is_same_v<std::remove_const_t<DataType>, double> or                       //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<1, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<2, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyVector<3, double>> or      //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<1, 1, double>> or   //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<2, 2, double>> or   //
                    std::is_same_v<std::remove_const_t<DataType>, TinyMatrix<3, 3, double>>,
                  "DiscreteFunctionP0 with this DataType is not allowed in variant");
  }

  template <typename DataType>
  DiscreteFunctionVariant(const DiscreteFunctionP0Vector<DataType>& discrete_function)
    : m_discrete_function{DiscreteFunctionP0Vector<const DataType>{discrete_function}}
  {
    static_assert(std::is_same_v<std::remove_const_t<DataType>, double>,
                  "DiscreteFunctionP0Vector with this DataType is not allowed in variant");
  }

  DiscreteFunctionVariant& operator=(DiscreteFunctionVariant&&)      = default;
  DiscreteFunctionVariant& operator=(const DiscreteFunctionVariant&) = default;

  DiscreteFunctionVariant(const DiscreteFunctionVariant&) = default;
  DiscreteFunctionVariant(DiscreteFunctionVariant&&)      = default;

  DiscreteFunctionVariant()  = delete;
  ~DiscreteFunctionVariant() = default;
};

#endif   // DISCRETE_FUNCTION_VARIANT_HPP
