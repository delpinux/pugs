#ifndef DISCRETE_FUNCTION_P0_HPP
#define DISCRETE_FUNCTION_P0_HPP

#include <language/utils/ASTNodeDataTypeTraits.hpp>

#include <mesh/ItemValueUtils.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/MeshVariant.hpp>
#include <scheme/DiscreteFunctionDescriptorP0.hpp>

template <typename DataType>
class DiscreteFunctionP0
{
 public:
  using data_type = DataType;

  friend class DiscreteFunctionP0<std::add_const_t<DataType>>;
  friend class DiscreteFunctionP0<std::remove_const_t<DataType>>;

 private:
  std::shared_ptr<const MeshVariant> m_mesh_v;
  CellValue<DataType> m_cell_values;

  DiscreteFunctionDescriptorP0 m_discrete_function_descriptor;

 public:
  PUGS_INLINE
  ASTNodeDataType
  dataType() const
  {
    return ast_node_data_type_from<std::remove_const_t<DataType>>;
  }

  PUGS_INLINE
  const CellValue<DataType>&
  cellValues() const
  {
    return m_cell_values;
  }

  PUGS_INLINE std::shared_ptr<const MeshVariant>
  meshVariant() const
  {
    return m_mesh_v;
  }

  PUGS_INLINE
  const IDiscreteFunctionDescriptor&
  descriptor() const
  {
    return m_discrete_function_descriptor;
  }

  PUGS_FORCEINLINE
  operator DiscreteFunctionP0<const DataType>() const
  {
    return DiscreteFunctionP0<const DataType>(m_mesh_v, m_cell_values);
  }

  PUGS_INLINE
  void
  fill(const DataType& data) const noexcept
  {
    static_assert(not std::is_const_v<DataType>, "Cannot modify ItemValue of const");
    m_cell_values.fill(data);
  }

  PUGS_INLINE DiscreteFunctionP0
  operator=(const DiscreteFunctionP0& f)
  {
    Assert(m_mesh_v->id() == f.m_mesh_v->id());
    m_cell_values = f.m_cell_values;
    return *this;
  }

  friend PUGS_INLINE DiscreteFunctionP0<std::remove_const_t<DataType>>
  copy(const DiscreteFunctionP0& source)
  {
    return DiscreteFunctionP0<std::remove_const_t<DataType>>{source.m_mesh_v, copy(source.cellValues())};
  }

  friend PUGS_INLINE void
  copy_to(const DiscreteFunctionP0<DataType>& source, DiscreteFunctionP0<std::remove_const_t<DataType>>& destination)
  {
    Assert(source.m_mesh_v == destination.m_mesh_v);
    copy_to(source.m_cell_values, destination.m_cell_values);
  }

  PUGS_FORCEINLINE DataType&
  operator[](const CellId cell_id) const noexcept(NO_ASSERT)
  {
    return m_cell_values[cell_id];
  }

  PUGS_INLINE DiscreteFunctionP0<std::remove_const_t<DataType>>
  operator-() const
  {
    Assert(m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> opposite = copy(*this);
    parallel_for(
      m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { opposite[cell_id] = -opposite[cell_id]; });

    return opposite;
  }

  template <typename DataType2T>
  PUGS_INLINE DiscreteFunctionP0<decltype(DataType{} + DataType2T{})>
  operator+(const DiscreteFunctionP0<DataType2T>& g) const
  {
    const DiscreteFunctionP0& f = *this;
    Assert(f.m_cell_values.isBuilt() and g.m_cell_values.isBuilt());
    Assert(f.meshVariant()->id() == g.meshVariant()->id(), "functions are not defined on the same mesh");
    DiscreteFunctionP0<decltype(DataType{} + DataType2T{})> sum(f.m_mesh_v);
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum[cell_id] = f[cell_id] + g[cell_id]; });
    return sum;
  }

  template <typename LHSDataType>
  PUGS_INLINE friend DiscreteFunctionP0<decltype(LHSDataType{} + DataType{})>
  operator+(const LHSDataType& a, const DiscreteFunctionP0& g)
  {
    using SumDataType = decltype(LHSDataType{} + DataType{});
    Assert(g.m_cell_values.isBuilt());
    DiscreteFunctionP0<SumDataType> sum(g.m_mesh_v);
    parallel_for(
      g.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum[cell_id] = a + g[cell_id]; });
    return sum;
  }

  template <typename RHSDataType>
  PUGS_INLINE friend DiscreteFunctionP0<decltype(DataType{} + RHSDataType{})>
  operator+(const DiscreteFunctionP0& f, const RHSDataType& b)
  {
    using SumDataType = decltype(DataType{} + RHSDataType{});
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<SumDataType> sum(f.m_mesh_v);
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { sum[cell_id] = f[cell_id] + b; });
    return sum;
  }

  template <typename DataType2T>
  PUGS_INLINE DiscreteFunctionP0<decltype(DataType{} - DataType2T{})>
  operator-(const DiscreteFunctionP0<DataType2T>& g) const
  {
    const DiscreteFunctionP0& f = *this;
    Assert(f.m_cell_values.isBuilt() and g.m_cell_values.isBuilt());
    Assert(f.meshVariant()->id() == g.meshVariant()->id(), "functions are not defined on the same mesh");
    DiscreteFunctionP0<decltype(DataType{} - DataType2T{})> difference(f.m_mesh_v);
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { difference[cell_id] = f[cell_id] - g[cell_id]; });
    return difference;
  }

  template <typename LHSDataType>
  PUGS_INLINE friend DiscreteFunctionP0<decltype(LHSDataType{} - DataType{})>
  operator-(const LHSDataType& a, const DiscreteFunctionP0& g)
  {
    using DifferenceDataType = decltype(LHSDataType{} - DataType{});
    Assert(g.m_cell_values.isBuilt());
    DiscreteFunctionP0<DifferenceDataType> difference(g.m_mesh_v);
    parallel_for(
      g.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { difference[cell_id] = a - g[cell_id]; });
    return difference;
  }

  template <typename RHSDataType>
  PUGS_INLINE friend DiscreteFunctionP0<decltype(DataType{} - RHSDataType{})>
  operator-(const DiscreteFunctionP0& f, const RHSDataType& b)
  {
    using DifferenceDataType = decltype(DataType{} - RHSDataType{});
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<DifferenceDataType> difference(f.m_mesh_v);
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { difference[cell_id] = f[cell_id] - b; });
    return difference;
  }

  template <typename DataType2T>
  PUGS_INLINE DiscreteFunctionP0<decltype(DataType{} * DataType2T{})>
  operator*(const DiscreteFunctionP0<DataType2T>& g) const
  {
    const DiscreteFunctionP0& f = *this;
    Assert(f.m_cell_values.isBuilt());
    Assert(f.meshVariant()->id() == g.meshVariant()->id(), "functions are not defined on the same mesh");
    DiscreteFunctionP0<decltype(DataType{} * DataType2T{})> product(f.m_mesh_v);
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product[cell_id] = f[cell_id] * g[cell_id]; });
    return product;
  }

  template <typename LHSDataType>
  PUGS_INLINE friend DiscreteFunctionP0<decltype(LHSDataType{} * DataType{})>
  operator*(const LHSDataType& a, const DiscreteFunctionP0& f)
  {
    using ProductDataType = decltype(LHSDataType{} * DataType{});
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<ProductDataType> product(f.m_mesh_v);
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product[cell_id] = a * f[cell_id]; });
    return product;
  }

  template <typename RHSDataType>
  PUGS_INLINE friend DiscreteFunctionP0<decltype(DataType{} * RHSDataType{})>
  operator*(const DiscreteFunctionP0& f, const RHSDataType& b)
  {
    using ProductDataType = decltype(DataType{} * RHSDataType{});
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<ProductDataType> product(f.m_mesh_v);
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { product[cell_id] = f[cell_id] * b; });
    return product;
  }

  template <typename DataType2T>
  PUGS_INLINE DiscreteFunctionP0<decltype(DataType{} / DataType2T{})>
  operator/(const DiscreteFunctionP0<DataType2T>& g) const
  {
    const DiscreteFunctionP0& f = *this;
    Assert(f.m_cell_values.isBuilt() and g.m_cell_values.isBuilt());
    Assert(f.meshVariant()->id() == g.meshVariant()->id(), "functions are not defined on the same mesh");
    DiscreteFunctionP0<decltype(DataType{} / DataType2T{})> ratio(f.m_mesh_v);
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { ratio[cell_id] = f[cell_id] / g[cell_id]; });
    return ratio;
  }

  template <typename LHSDataType>
  PUGS_INLINE friend DiscreteFunctionP0<decltype(LHSDataType{} / DataType{})>
  operator/(const LHSDataType& a, const DiscreteFunctionP0& f)
  {
    using RatioDataType = decltype(LHSDataType{} / DataType{});
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<RatioDataType> ratio(f.m_mesh_v);
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { ratio[cell_id] = a / f[cell_id]; });
    return ratio;
  }

  template <typename RHSDataType>
  PUGS_INLINE friend DiscreteFunctionP0<decltype(DataType{} / RHSDataType{})>
  operator/(const DiscreteFunctionP0& f, const RHSDataType& b)
  {
    using RatioDataType = decltype(DataType{} / RHSDataType{});
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<RatioDataType> ratio(f.m_mesh_v);
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { ratio[cell_id] = f[cell_id] / b; });
    return ratio;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  sqrt(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::sqrt(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  abs(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::abs(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  sin(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::sin(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  cos(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::cos(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  tan(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::tan(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  asin(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::asin(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  acos(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::acos(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  atan(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::atan(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  sinh(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::sinh(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  cosh(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::cosh(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  tanh(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::tanh(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  asinh(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::asinh(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  acosh(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::acosh(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  atanh(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::atanh(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  exp(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::exp(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  log(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::log(f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  atan2(const DiscreteFunctionP0& f, const DiscreteFunctionP0& g)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt() and g.m_cell_values.isBuilt());
    Assert(f.m_mesh_v->id() == g.m_mesh_v->id());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(),
      PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::atan2(f[cell_id], g[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  atan2(const double a, const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::atan2(a, f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  atan2(const DiscreteFunctionP0& f, const double a)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::atan2(f[cell_id], a); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  pow(const DiscreteFunctionP0& f, const DiscreteFunctionP0& g)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt() and g.m_cell_values.isBuilt());
    Assert(f.m_mesh_v->id() == g.m_mesh_v->id());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::pow(f[cell_id], g[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  pow(const double a, const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::pow(a, f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  pow(const DiscreteFunctionP0& f, const double a)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::pow(f[cell_id], a); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  det(const DiscreteFunctionP0& A)
  {
    static_assert(is_tiny_matrix_v<std::decay_t<DataType>>);
    Assert(A.m_cell_values.isBuilt());
    DiscreteFunctionP0<double> result{A.m_mesh_v};
    parallel_for(
      A.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = det(A[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  trace(const DiscreteFunctionP0& A)
  {
    static_assert(is_tiny_matrix_v<std::decay_t<DataType>>);
    Assert(A.m_cell_values.isBuilt());
    DiscreteFunctionP0<double> result{A.m_mesh_v};
    parallel_for(
      A.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = trace(A[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<std::remove_const_t<DataType>>
  inverse(const DiscreteFunctionP0& A)
  {
    static_assert(is_tiny_matrix_v<std::decay_t<DataType>>);
    static_assert(DataType::NumberOfRows == DataType::NumberOfColumns, "cannot compute inverse of non square matrices");
    Assert(A.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{A.m_mesh_v};
    parallel_for(
      A.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = inverse(A[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<std::remove_const_t<DataType>>
  transpose(const DiscreteFunctionP0& A)
  {
    static_assert(is_tiny_matrix_v<std::decay_t<DataType>>);
    static_assert(DataType::NumberOfRows == DataType::NumberOfColumns, "cannot compute inverse of non square matrices");
    Assert(A.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{A.m_mesh_v};
    parallel_for(
      A.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = transpose(A[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  dot(const DiscreteFunctionP0& f, const DiscreteFunctionP0& g)
  {
    static_assert(is_tiny_vector_v<std::decay_t<DataType>>);
    Assert(f.m_cell_values.isBuilt() and g.m_cell_values.isBuilt());
    Assert(f.m_mesh_v->id() == g.m_mesh_v->id());
    DiscreteFunctionP0<double> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = dot(f[cell_id], g[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  dot(const DiscreteFunctionP0& f, const DataType& a)
  {
    static_assert(is_tiny_vector_v<std::decay_t<DataType>>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<double> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = dot(f[cell_id], a); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  dot(const DataType& a, const DiscreteFunctionP0& f)
  {
    static_assert(is_tiny_vector_v<std::decay_t<DataType>>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<double> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = dot(a, f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  doubleDot(const DiscreteFunctionP0& f, const DiscreteFunctionP0& g)
  {
    static_assert(is_tiny_matrix_v<std::decay_t<DataType>>);
    Assert(f.m_cell_values.isBuilt() and g.m_cell_values.isBuilt());
    Assert(f.m_mesh_v->id() == g.m_mesh_v->id());
    DiscreteFunctionP0<double> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(),
      PUGS_LAMBDA(CellId cell_id) { result[cell_id] = doubleDot(f[cell_id], g[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  doubleDot(const DiscreteFunctionP0& f, const DataType& a)
  {
    static_assert(is_tiny_matrix_v<std::decay_t<DataType>>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<double> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = doubleDot(f[cell_id], a); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<double>
  doubleDot(const DataType& a, const DiscreteFunctionP0& f)
  {
    static_assert(is_tiny_matrix_v<std::decay_t<DataType>>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<double> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = doubleDot(a, f[cell_id]); });

    return result;
  }

  template <typename LHSDataType>
  PUGS_INLINE friend DiscreteFunctionP0<decltype(tensorProduct(LHSDataType{}, DataType{}))>
  tensorProduct(const LHSDataType& u, const DiscreteFunctionP0& v)
  {
    static_assert(is_tiny_vector_v<std::decay_t<DataType>>);
    static_assert(is_tiny_vector_v<std::decay_t<LHSDataType>>);
    Assert(v.m_cell_values.isBuilt());
    DiscreteFunctionP0<decltype(tensorProduct(LHSDataType{}, DataType{}))> result{v.m_mesh_v};
    parallel_for(
      v.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = tensorProduct(u, v[cell_id]); });

    return result;
  }

  template <typename RHSDataType>
  PUGS_INLINE friend DiscreteFunctionP0<decltype(tensorProduct(DataType{}, RHSDataType{}))>
  tensorProduct(const DiscreteFunctionP0& u, const RHSDataType& v)
  {
    static_assert(is_tiny_vector_v<std::decay_t<DataType>>);
    static_assert(is_tiny_vector_v<std::decay_t<RHSDataType>>);
    Assert(u.m_cell_values.isBuilt());
    DiscreteFunctionP0<decltype(tensorProduct(DataType{}, RHSDataType{}))> result{u.m_mesh_v};
    parallel_for(
      u.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = tensorProduct(u[cell_id], v); });

    return result;
  }

  template <typename DataType2>
  PUGS_INLINE friend DiscreteFunctionP0<decltype(tensorProduct(DataType{}, DataType2{}))>
  tensorProduct(const DiscreteFunctionP0& u, const DiscreteFunctionP0<DataType2>& v)
  {
    static_assert(is_tiny_vector_v<std::decay_t<DataType>>);
    static_assert(is_tiny_vector_v<std::decay_t<DataType2>>);
    Assert(u.m_cell_values.isBuilt());
    Assert(v.m_cell_values.isBuilt());
    Assert(u.m_mesh_v->id() == v.m_mesh_v->id());
    DiscreteFunctionP0<decltype(tensorProduct(DataType{}, DataType2{}))> result{u.m_mesh_v};
    parallel_for(
      u.m_mesh_v->numberOfCells(),
      PUGS_LAMBDA(CellId cell_id) { result[cell_id] = tensorProduct(u[cell_id], v[cell_id]); });

    return result;
  }

  PUGS_INLINE friend double
  min(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());

    return min(f.m_cell_values);
  }

  PUGS_INLINE friend DiscreteFunctionP0<std::remove_const_t<DataType>>
  min(const DiscreteFunctionP0& f, const DiscreteFunctionP0& g)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt() and g.m_cell_values.isBuilt());
    Assert(f.m_mesh_v->id() == g.m_mesh_v->id());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::min(f[cell_id], g[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<std::remove_const_t<DataType>>
  min(const double a, const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::min(a, f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<std::remove_const_t<DataType>>
  min(const DiscreteFunctionP0& f, const double a)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::min(f[cell_id], a); });

    return result;
  }

  PUGS_INLINE friend double
  max(const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());

    return max(f.m_cell_values);
  }

  PUGS_INLINE friend DiscreteFunctionP0<std::remove_const_t<DataType>>
  max(const DiscreteFunctionP0& f, const DiscreteFunctionP0& g)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt() and g.m_cell_values.isBuilt());
    Assert(f.m_mesh_v->id() == g.m_mesh_v->id());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::max(f[cell_id], g[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<std::remove_const_t<DataType>>
  max(const double a, const DiscreteFunctionP0& f)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::max(a, f[cell_id]); });

    return result;
  }

  PUGS_INLINE friend DiscreteFunctionP0<std::remove_const_t<DataType>>
  max(const DiscreteFunctionP0& f, const double a)
  {
    static_assert(std::is_arithmetic_v<DataType>);
    Assert(f.m_cell_values.isBuilt());
    DiscreteFunctionP0<std::remove_const_t<DataType>> result{f.m_mesh_v};
    parallel_for(
      f.m_mesh_v->numberOfCells(), PUGS_LAMBDA(CellId cell_id) { result[cell_id] = std::max(f[cell_id], a); });

    return result;
  }

  PUGS_INLINE friend DataType
  sum(const DiscreteFunctionP0& f)
  {
    Assert(f.m_cell_values.isBuilt());
    return sum(f.m_cell_values);
  }

  PUGS_INLINE friend DataType
  integrate(const DiscreteFunctionP0& f)
  {
    Assert(f.m_cell_values.isBuilt());

    return std::visit(
      [&](auto&& p_mesh) {
        const auto& mesh = *p_mesh;

        CellValue<const double> cell_volume = MeshDataManager::instance().getMeshData(mesh).Vj();

        CellValue<std::remove_const_t<DataType>> f_v{mesh.connectivity()};
        parallel_for(
          mesh.numberOfCells(), PUGS_LAMBDA(CellId cell_id) { f_v[cell_id] = cell_volume[cell_id] * f[cell_id]; });

        return sum(f_v);
      },
      f.m_mesh_v->variant());
  }

  DiscreteFunctionP0(const std::shared_ptr<const MeshVariant>& mesh_v)
    : m_mesh_v{mesh_v}, m_cell_values{mesh_v->connectivity()}
  {}

  DiscreteFunctionP0(const std::shared_ptr<const MeshVariant>& mesh_v, const CellValue<DataType>& cell_value)
    : m_mesh_v{mesh_v}, m_cell_values{cell_value}
  {
    Assert(mesh_v->connectivity().id() == cell_value.connectivity_ptr()->id());
  }

  template <MeshConcept MeshType>
  DiscreteFunctionP0(const std::shared_ptr<const MeshType>& p_mesh)
    : m_mesh_v{p_mesh->meshVariant()}, m_cell_values{m_mesh_v->connectivity()}
  {}

  template <MeshConcept MeshType>
  DiscreteFunctionP0(const std::shared_ptr<const MeshType>& p_mesh, const CellValue<DataType>& cell_value)
    : m_mesh_v{p_mesh->meshVariant()}, m_cell_values{cell_value}
  {
    Assert(m_mesh_v->connectivity().id() == cell_value.connectivity_ptr()->id());
  }

  DiscreteFunctionP0() noexcept = delete;

  DiscreteFunctionP0(const DiscreteFunctionP0&) noexcept = default;
  DiscreteFunctionP0(DiscreteFunctionP0&&) noexcept      = default;

  ~DiscreteFunctionP0() = default;
};

#endif   // DISCRETE_FUNCTION_P0_HPP
