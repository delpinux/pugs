#ifndef SYMMETRY_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define SYMMETRY_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>

class SymmetryBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "symmetry(" << *m_boundary_descriptor << ")";
    return os;
  }

 public:
  Type
  type() const final
  {
    return Type::symmetry;
  }

  SymmetryBoundaryConditionDescriptor(const std::shared_ptr<const IBoundaryDescriptor>& boundary_descriptor)
    : BoundaryConditionDescriptorBase{boundary_descriptor}
  {
    ;
  }

  SymmetryBoundaryConditionDescriptor(const SymmetryBoundaryConditionDescriptor&) = delete;
  SymmetryBoundaryConditionDescriptor(SymmetryBoundaryConditionDescriptor&&)      = delete;

  ~SymmetryBoundaryConditionDescriptor() = default;
};

#endif   // SYMMETRY_BOUNDARY_CONDITION_DESCRIPTOR_HPP
