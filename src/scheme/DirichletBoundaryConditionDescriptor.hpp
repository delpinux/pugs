#ifndef DIRICHLET_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define DIRICHLET_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>

class DirichletBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "dirichlet(" << m_name << ',' << *m_boundary_descriptor << ")";
    return os;
  }

  const std::string m_name;

  const FunctionSymbolId m_rhs_symbol_id;

 public:
  const std::string&
  name() const
  {
    return m_name;
  }

  FunctionSymbolId
  rhsSymbolId() const
  {
    return m_rhs_symbol_id;
  }

  Type
  type() const final
  {
    return Type::dirichlet;
  }

  DirichletBoundaryConditionDescriptor(const std::string& name,
                                       const std::shared_ptr<const IBoundaryDescriptor>& boundary_descriptor,
                                       const FunctionSymbolId& rhs_symbol_id)
    : BoundaryConditionDescriptorBase{boundary_descriptor}, m_name{name}, m_rhs_symbol_id{rhs_symbol_id}
  {
    ;
  }

  DirichletBoundaryConditionDescriptor(const DirichletBoundaryConditionDescriptor&) = delete;
  DirichletBoundaryConditionDescriptor(DirichletBoundaryConditionDescriptor&&)      = delete;

  ~DirichletBoundaryConditionDescriptor() = default;
};

#endif   // DIRICHLET_BOUNDARY_CONDITION_DESCRIPTOR_HPP
