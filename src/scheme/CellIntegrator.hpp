#ifndef CELL_INTEGRATOR_HPP
#define CELL_INTEGRATOR_HPP

#include <analysis/IQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/CubeTransformation.hpp>
#include <geometry/LineTransformation.hpp>
#include <geometry/PrismTransformation.hpp>
#include <geometry/PyramidTransformation.hpp>
#include <geometry/SquareTransformation.hpp>
#include <geometry/TetrahedronTransformation.hpp>
#include <geometry/TriangleTransformation.hpp>
#include <mesh/CellType.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemId.hpp>
#include <utils/Array.hpp>
#include <utils/Stringify.hpp>

#include <functional>

class CellIntegrator
{
 private:
  template <size_t Dimension>
  class AllCells
  {
   private:
    const Connectivity<Dimension>& m_connectivity;

   public:
    using index_type = CellId;

    PUGS_INLINE
    CellId
    cellId(const CellId cell_id) const
    {
      return cell_id;
    }

    PUGS_INLINE
    size_t
    size() const
    {
      return m_connectivity.numberOfCells();
    }

    PUGS_INLINE
    AllCells(const Connectivity<Dimension>& connectivity) : m_connectivity{connectivity} {}
  };

  template <template <typename T> typename ArrayT>
  class CellList
  {
   private:
    const ArrayT<CellId>& m_cell_list;

   public:
    using index_type = typename ArrayT<CellId>::index_type;

    PUGS_INLINE
    CellId
    cellId(const index_type index) const
    {
      return m_cell_list[index];
    }

    PUGS_INLINE
    size_t
    size() const
    {
      return m_cell_list.size();
    }

    PUGS_INLINE
    CellList(const ArrayT<CellId>& cell_list) : m_cell_list{cell_list} {}
  };

  template <MeshConcept MeshType, typename OutputArrayT, typename ListTypeT, typename OutputType, typename InputType>
  static PUGS_INLINE void
  _tensorialIntegrateTo(std::function<OutputType(InputType)> function,
                        const IQuadratureDescriptor& quadrature_descriptor,
                        const MeshType& mesh,
                        const ListTypeT& cell_list,
                        OutputArrayT& value)
  {
    constexpr size_t Dimension = MeshType::Dimension;

    static_assert(std::is_same_v<TinyVector<Dimension>, std::decay_t<InputType>>, "invalid input data type");
    static_assert(std::is_same_v<std::remove_const_t<typename OutputArrayT::data_type>, OutputType>,
                  "invalid output data type");

    using execution_space = typename Kokkos::DefaultExecutionSpace::execution_space;
    Kokkos::Experimental::UniqueToken<execution_space, Kokkos::Experimental::UniqueTokenScope::Global> tokens;

    if constexpr (std::is_arithmetic_v<OutputType>) {
      value.fill(0);
    } else if constexpr (is_tiny_vector_v<OutputType> or is_tiny_matrix_v<OutputType>) {
      value.fill(zero);
    } else {
      static_assert(std::is_same_v<OutputType, double>, "unexpected output type");
    }

    const auto& connectivity = mesh.connectivity();

    const auto cell_to_node_matrix = connectivity.cellToNodeMatrix();
    const auto cell_type           = connectivity.cellType();
    const auto xr                  = mesh.xr();

    auto invalid_cell = std::make_pair(false, CellId{});

    const auto qf = [&] {
      if constexpr (Dimension == 1) {
        return QuadratureManager::instance().getLineFormula(quadrature_descriptor);
      } else if constexpr (Dimension == 2) {
        return QuadratureManager::instance().getSquareFormula(quadrature_descriptor);
      } else {
        static_assert(Dimension == 3);
        return QuadratureManager::instance().getCubeFormula(quadrature_descriptor);
      }
    }();

    parallel_for(cell_list.size(), [=, &invalid_cell, &qf, &cell_list](typename ListTypeT::index_type index) {
      const CellId cell_id = cell_list.cellId(index);

      const auto cell_to_node = cell_to_node_matrix[cell_id];

      if constexpr (Dimension == 1) {
        switch (cell_type[cell_id]) {
        case CellType::Line: {
          const LineTransformation<1> T(xr[cell_to_node[0]], xr[cell_to_node[1]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant() * function(T(xi));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      } else if constexpr (Dimension == 2) {
        switch (cell_type[cell_id]) {
        case CellType::Triangle: {
          const SquareTransformation<2> T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],
                                          xr[cell_to_node[2]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * function(T(xi));
          }
          break;
        }
        case CellType::Quadrangle: {
          const SquareTransformation<2> T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],
                                          xr[cell_to_node[3]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * function(T(xi));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      } else {
        static_assert(Dimension == 3);

        switch (cell_type[cell_id]) {
        case CellType::Tetrahedron: {
          const CubeTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],
                                     xr[cell_to_node[2]],   //
                                     xr[cell_to_node[3]], xr[cell_to_node[3]], xr[cell_to_node[3]],
                                     xr[cell_to_node[3]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * function(T(xi));
          }
          break;
        }
        case CellType::Pyramid: {
          if (cell_to_node.size() == 5) {
            const CubeTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],   //
                                       xr[cell_to_node[3]],                                             //
                                       xr[cell_to_node[4]], xr[cell_to_node[4]], xr[cell_to_node[4]],
                                       xr[cell_to_node[4]]);

            for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
              const auto xi = qf.point(i_point);
              value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * function(T(xi));
            }
          } else {
            // LCOV_EXCL_START
            throw NotImplementedError("integration on pyramid with non-quadrangular base (number of nodes " +
                                      stringify(cell_to_node.size()) + ")");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case CellType::Diamond: {
          if (cell_to_node.size() == 5) {
            {   // top tetrahedron
              const CubeTransformation T0(xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],
                                          xr[cell_to_node[3]],   //
                                          xr[cell_to_node[4]], xr[cell_to_node[4]], xr[cell_to_node[4]],
                                          xr[cell_to_node[4]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                value[index] += qf.weight(i_point) * T0.jacobianDeterminant(xi) * function(T0(xi));
              }
            }
            {   // bottom tetrahedron
              const CubeTransformation T1(xr[cell_to_node[3]], xr[cell_to_node[3]], xr[cell_to_node[2]],
                                          xr[cell_to_node[1]],   //
                                          xr[cell_to_node[0]], xr[cell_to_node[0]], xr[cell_to_node[0]],
                                          xr[cell_to_node[0]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                value[index] += qf.weight(i_point) * T1.jacobianDeterminant(xi) * function(T1(xi));
              }
            }
          } else if (cell_to_node.size() == 6) {
            {   // top pyramid
              const CubeTransformation T0(xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],
                                          xr[cell_to_node[4]],   //
                                          xr[cell_to_node[5]], xr[cell_to_node[5]], xr[cell_to_node[5]],
                                          xr[cell_to_node[5]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                value[index] += qf.weight(i_point) * T0.jacobianDeterminant(xi) * function(T0(xi));
              }
            }
            {   // bottom pyramid
              const CubeTransformation T1(xr[cell_to_node[4]], xr[cell_to_node[3]], xr[cell_to_node[2]],
                                          xr[cell_to_node[1]],   //
                                          xr[cell_to_node[0]], xr[cell_to_node[0]], xr[cell_to_node[0]],
                                          xr[cell_to_node[0]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                value[index] += qf.weight(i_point) * T1.jacobianDeterminant(xi) * function(T1(xi));
              }
            }
          } else {
            // LCOV_EXCL_START
            throw NotImplementedError("integration on diamond with non-quadrangular base (" +
                                      stringify(cell_to_node.size()) + ")");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case CellType::Prism: {
          const CubeTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]],   //
                                     xr[cell_to_node[2]], xr[cell_to_node[2]],   //
                                     xr[cell_to_node[3]], xr[cell_to_node[4]],   //
                                     xr[cell_to_node[5]], xr[cell_to_node[5]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * function(T(xi));
          }
          break;
        }
        case CellType::Hexahedron: {
          const CubeTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],
                                     xr[cell_to_node[4]], xr[cell_to_node[5]], xr[cell_to_node[6]],
                                     xr[cell_to_node[7]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * function(T(xi));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      }
    });

    // LCOV_EXCL_START
    if (invalid_cell.first) {
      std::ostringstream os;
      os << "invalid cell type for integration: " << name(cell_type[invalid_cell.second]);
      throw UnexpectedError(os.str());
    }
    // LCOV_EXCL_STOP
  }

  template <MeshConcept MeshType, typename OutputArrayT, typename ListTypeT, typename OutputType, typename InputType>
  static PUGS_INLINE void
  _directIntegrateTo(std::function<OutputType(InputType)> function,
                     const IQuadratureDescriptor& quadrature_descriptor,
                     const MeshType& mesh,
                     const ListTypeT& cell_list,
                     OutputArrayT& value)
  {
    Assert(not quadrature_descriptor.isTensorial());

    constexpr size_t Dimension = MeshType::Dimension;

    static_assert(std::is_same_v<TinyVector<Dimension>, std::decay_t<InputType>>, "invalid input data type");
    static_assert(std::is_same_v<std::remove_const_t<typename OutputArrayT::data_type>, OutputType>,
                  "invalid output data type");

    if constexpr (std::is_arithmetic_v<OutputType>) {
      value.fill(0);
    } else if constexpr (is_tiny_vector_v<OutputType> or is_tiny_matrix_v<OutputType>) {
      value.fill(zero);
    } else {
      static_assert(std::is_same_v<OutputType, double>, "unexpected output type");
    }

    const auto& connectivity = mesh.connectivity();

    const auto cell_to_node_matrix = connectivity.cellToNodeMatrix();
    const auto cell_type           = connectivity.cellType();
    const auto xr                  = mesh.xr();

    auto invalid_cell = std::make_pair(false, CellId{});

    parallel_for(cell_list.size(), [=, &invalid_cell, &quadrature_descriptor,
                                    &cell_list](const typename ListTypeT::index_type& index) {
      const CellId cell_id = cell_list.cellId(index);

      const auto cell_to_node = cell_to_node_matrix[cell_id];

      if constexpr (Dimension == 1) {
        switch (cell_type[cell_id]) {
        case CellType::Line: {
          const LineTransformation<1> T(xr[cell_to_node[0]], xr[cell_to_node[1]]);
          const auto qf = QuadratureManager::instance().getLineFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant() * function(T(xi));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      } else if constexpr (Dimension == 2) {
        switch (cell_type[cell_id]) {
        case CellType::Triangle: {
          const TriangleTransformation<2> T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]]);
          const auto qf = QuadratureManager::instance().getTriangleFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant() * function(T(xi));
          }
          break;
        }
        case CellType::Quadrangle: {
          const SquareTransformation<2> T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],
                                          xr[cell_to_node[3]]);
          const auto qf = QuadratureManager::instance().getSquareFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * function(T(xi));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      } else {
        static_assert(Dimension == 3);

        switch (cell_type[cell_id]) {
        case CellType::Tetrahedron: {
          const TetrahedronTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]],   //
                                            xr[cell_to_node[2]], xr[cell_to_node[3]]);
          const auto qf = QuadratureManager::instance().getTetrahedronFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant() * function(T(xi));
          }
          break;
        }
        case CellType::Pyramid: {
          if (cell_to_node.size() == 5) {
            const PyramidTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],   //
                                          xr[cell_to_node[3]], xr[cell_to_node[4]]);
            const auto qf = QuadratureManager::instance().getPyramidFormula(quadrature_descriptor);

            for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
              const auto xi = qf.point(i_point);
              value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * function(T(xi));
            }
          } else {
            // LCOV_EXCL_START
            throw NotImplementedError("integration on pyramid with non-quadrangular base");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case CellType::Diamond: {
          if (cell_to_node.size() == 5) {
            const auto qf = QuadratureManager::instance().getTetrahedronFormula(quadrature_descriptor);
            {   // top tetrahedron
              const TetrahedronTransformation T0(xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],   //
                                                 xr[cell_to_node[4]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                value[index] += qf.weight(i_point) * T0.jacobianDeterminant() * function(T0(xi));
              }
            }
            {   // bottom tetrahedron
              const TetrahedronTransformation T1(xr[cell_to_node[3]], xr[cell_to_node[2]], xr[cell_to_node[1]],   //
                                                 xr[cell_to_node[0]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                value[index] += qf.weight(i_point) * T1.jacobianDeterminant() * function(T1(xi));
              }
            }
          } else if (cell_to_node.size() == 6) {
            const auto qf = QuadratureManager::instance().getPyramidFormula(quadrature_descriptor);
            {   // top pyramid
              const PyramidTransformation T0(xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],
                                             xr[cell_to_node[4]], xr[cell_to_node[5]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                value[index] += qf.weight(i_point) * T0.jacobianDeterminant(xi) * function(T0(xi));
              }
            }
            {   // bottom pyramid
              const PyramidTransformation T1(xr[cell_to_node[4]], xr[cell_to_node[3]], xr[cell_to_node[2]],
                                             xr[cell_to_node[1]], xr[cell_to_node[0]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                value[index] += qf.weight(i_point) * T1.jacobianDeterminant(xi) * function(T1(xi));
              }
            }
          } else {
            // LCOV_EXCL_START
            throw NotImplementedError("integration on diamond with non-quadrangular base (" +
                                      stringify(cell_to_node.size()) + ")");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case CellType::Prism: {
          const PrismTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],
                                      xr[cell_to_node[3]], xr[cell_to_node[4]], xr[cell_to_node[5]]);
          const auto qf = QuadratureManager::instance().getPrismFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * function(T(xi));
          }
          break;
        }
        case CellType::Hexahedron: {
          const CubeTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],
                                     xr[cell_to_node[4]], xr[cell_to_node[5]], xr[cell_to_node[6]],
                                     xr[cell_to_node[7]]);
          const auto qf = QuadratureManager::instance().getCubeFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * function(T(xi));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      }
    });

    // LCOV_EXCL_START
    if (invalid_cell.first) {
      std::ostringstream os;
      os << "invalid cell type for integration: " << name(cell_type[invalid_cell.second]);
      throw UnexpectedError(os.str());
    }
    // LCOV_EXCL_STOP
  }

 public:
  template <MeshConcept MeshType, typename OutputArrayT, typename OutputType, typename InputType>
  static PUGS_INLINE void
  integrateTo(const std::function<OutputType(InputType)>& function,
              const IQuadratureDescriptor& quadrature_descriptor,
              const MeshType& mesh,
              OutputArrayT& value)
  {
    constexpr size_t Dimension = MeshType::Dimension;

    if (quadrature_descriptor.isTensorial()) {
      _tensorialIntegrateTo<MeshType, OutputArrayT>(function, quadrature_descriptor, mesh,
                                                    AllCells<Dimension>{mesh.connectivity()}, value);
    } else {
      _directIntegrateTo<MeshType, OutputArrayT>(function, quadrature_descriptor, mesh,
                                                 AllCells<Dimension>{mesh.connectivity()}, value);
    }
  }

  template <MeshConcept MeshType, typename OutputArrayT, typename FunctionType>
  static PUGS_INLINE void
  integrateTo(const FunctionType& function,
              const IQuadratureDescriptor& quadrature_descriptor,
              const MeshType& mesh,
              OutputArrayT& value)
  {
    integrateTo(std::function(function), quadrature_descriptor, mesh, value);
  }

  template <MeshConcept MeshType, typename OutputType, typename InputType, template <typename DataType> typename ArrayT>
  static PUGS_INLINE ArrayT<OutputType>
  integrate(const std::function<OutputType(InputType)>& function,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh,
            const ArrayT<CellId>& cell_list)
  {
    ArrayT<OutputType> value(size(cell_list));
    if (quadrature_descriptor.isTensorial()) {
      _tensorialIntegrateTo<MeshType, ArrayT<OutputType>>(function, quadrature_descriptor, mesh, CellList{cell_list},
                                                          value);
    } else {
      _directIntegrateTo<MeshType, ArrayT<OutputType>>(function, quadrature_descriptor, mesh, CellList{cell_list},
                                                       value);
    }

    return value;
  }

  template <MeshConcept MeshType, typename FunctionType, template <typename DataType> typename ArrayT>
  static PUGS_INLINE auto
  integrate(const FunctionType& function,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh,
            const ArrayT<CellId>& cell_list)
  {
    return integrate(std::function(function), quadrature_descriptor, mesh, cell_list);
  }
};

#endif   // CELL_INTEGRATOR_HPP
