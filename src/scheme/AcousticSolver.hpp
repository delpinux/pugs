#ifndef ACOUSTIC_SOLVER_HPP
#define ACOUSTIC_SOLVER_HPP

#include <mesh/MeshTraits.hpp>

#include <memory>
#include <tuple>
#include <vector>

class DiscreteFunctionVariant;
class IBoundaryConditionDescriptor;
class MeshVariant;
class ItemValueVariant;
class SubItemValuePerItemVariant;

double acoustic_dt(const std::shared_ptr<const DiscreteFunctionVariant>& c);

class AcousticSolverHandler
{
 public:
  enum class SolverType
  {
    Glace,
    Eucclhyd
  };

 private:
  struct IAcousticSolver
  {
    virtual std::tuple<const std::shared_ptr<const ItemValueVariant>,
                       const std::shared_ptr<const SubItemValuePerItemVariant>>
    compute_fluxes(
      const SolverType& solver_type,
      const std::shared_ptr<const DiscreteFunctionVariant>& rho,
      const std::shared_ptr<const DiscreteFunctionVariant>& c,
      const std::shared_ptr<const DiscreteFunctionVariant>& u,
      const std::shared_ptr<const DiscreteFunctionVariant>& p,
      const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>& bc_descriptor_list) const = 0;

    virtual std::tuple<std::shared_ptr<const MeshVariant>,
                       std::shared_ptr<const DiscreteFunctionVariant>,
                       std::shared_ptr<const DiscreteFunctionVariant>,
                       std::shared_ptr<const DiscreteFunctionVariant>>
    apply_fluxes(const double& dt,
                 const std::shared_ptr<const DiscreteFunctionVariant>& rho,
                 const std::shared_ptr<const DiscreteFunctionVariant>& u,
                 const std::shared_ptr<const DiscreteFunctionVariant>& E,
                 const std::shared_ptr<const ItemValueVariant>& ur,
                 const std::shared_ptr<const SubItemValuePerItemVariant>& Fjr) const = 0;

    virtual std::tuple<std::shared_ptr<const MeshVariant>,
                       std::shared_ptr<const DiscreteFunctionVariant>,
                       std::shared_ptr<const DiscreteFunctionVariant>,
                       std::shared_ptr<const DiscreteFunctionVariant>>
    apply(const SolverType& solver_type,
          const double& dt,
          const std::shared_ptr<const DiscreteFunctionVariant>& rho,
          const std::shared_ptr<const DiscreteFunctionVariant>& u,
          const std::shared_ptr<const DiscreteFunctionVariant>& E,
          const std::shared_ptr<const DiscreteFunctionVariant>& c,
          const std::shared_ptr<const DiscreteFunctionVariant>& p,
          const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>& bc_descriptor_list) const = 0;

    IAcousticSolver()                  = default;
    IAcousticSolver(IAcousticSolver&&) = default;
    IAcousticSolver& operator=(IAcousticSolver&&) = default;

    virtual ~IAcousticSolver() = default;
  };

  template <MeshConcept MeshType>
  class AcousticSolver;

  std::unique_ptr<IAcousticSolver> m_acoustic_solver;

 public:
  const IAcousticSolver&
  solver() const
  {
    return *m_acoustic_solver;
  }

  AcousticSolverHandler(const std::shared_ptr<const MeshVariant>& mesh_v);
};

#endif   // ACOUSTIC_SOLVER_HPP
