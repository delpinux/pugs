#include <scheme/LoadBalancer.hpp>

#include <mesh/ConnectivityDispatcher.hpp>
#include <mesh/ConnectivityDispatcherVariant.hpp>
#include <mesh/MeshBalancer.hpp>
#include <mesh/MeshVariant.hpp>
#include <scheme/DiscreteFunctionUtils.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>
#include <utils/Messenger.hpp>

std::vector<std::shared_ptr<const DiscreteFunctionVariant>>
LoadBalancer::balance(const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& discrete_function_list)
{
  if (not hasSameMesh(discrete_function_list)) {
    throw NormalError("discrete functions are not defined on the same mesh");
  }
  auto mesh_v = getCommonMesh(discrete_function_list);
  Assert(mesh_v.use_count() > 0);

  std::vector<std::shared_ptr<const DiscreteFunctionVariant>> balanced_discrete_function_list;

  MeshBalancer mesh_balancer(mesh_v);

  auto p_balanced_mesh_v = mesh_balancer.mesh();

  std::visit(
    [&mesh_balancer, &balanced_discrete_function_list, &discrete_function_list](auto&& p_balanced_mesh) {
      using MeshType             = mesh_type_t<decltype(p_balanced_mesh)>;
      constexpr size_t Dimension = MeshType::Dimension;
      const auto& dispatcher     = mesh_balancer.connectivityDispatcher()->get<Dimension>();

      for (size_t i_discrete_function = 0; i_discrete_function < discrete_function_list.size(); ++i_discrete_function) {
        std::visit(
          [&balanced_discrete_function_list, &dispatcher, &p_balanced_mesh](auto&& discrete_function) {
            using DiscreteFunctionT = std::decay_t<decltype(discrete_function)>;
            if constexpr (is_discrete_function_P0_v<DiscreteFunctionT>) {
              const auto& dispatched_cell_value = dispatcher->dispatch(discrete_function.cellValues());
              balanced_discrete_function_list.push_back(
                std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionT{p_balanced_mesh, dispatched_cell_value}));
            } else {
              const auto& dispatched_cell_array = dispatcher->dispatch(discrete_function.cellArrays());
              balanced_discrete_function_list.push_back(
                std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionT{p_balanced_mesh, dispatched_cell_array}));
            }
          },
          discrete_function_list[i_discrete_function]->discreteFunction());
      }
    },
    p_balanced_mesh_v->variant());

  return balanced_discrete_function_list;
}
