#ifndef FLUXING_ADVECION_SOLVER_HPP
#define FLUXING_ADVECION_SOLVER_HPP

#include <scheme/DiscreteFunctionVariant.hpp>
#include <scheme/VariableBCDescriptor.hpp>

#include <vector>

class MeshVariant;

std::vector<std::shared_ptr<const DiscreteFunctionVariant>> advectByFluxing(
  const std::shared_ptr<const MeshVariant> new_mesh,
  const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& remapped_variables);

std::vector<std::shared_ptr<const DiscreteFunctionVariant>> advectByFluxing(
  const std::shared_ptr<const MeshVariant> new_mesh,
  const std::vector<std::shared_ptr<const VariableBCDescriptor>>& remapped_variables_with_bc);

#endif   // FLUXING_ADVECION_SOLVER_HPP
