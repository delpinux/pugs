#ifndef AXIS_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define AXIS_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>

class AxisBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "axis(" << *m_boundary_descriptor << ")";
    return os;
  }

 public:
  Type
  type() const final
  {
    return Type::axis;
  }

  AxisBoundaryConditionDescriptor(const std::shared_ptr<const IBoundaryDescriptor>& boundary_descriptor)
    : BoundaryConditionDescriptorBase{boundary_descriptor}
  {
    ;
  }

  AxisBoundaryConditionDescriptor(const AxisBoundaryConditionDescriptor&) = delete;
  AxisBoundaryConditionDescriptor(AxisBoundaryConditionDescriptor&&)      = delete;

  ~AxisBoundaryConditionDescriptor() = default;
};

#endif   // AXIS_BOUNDARY_CONDITION_DESCRIPTOR_HPP
