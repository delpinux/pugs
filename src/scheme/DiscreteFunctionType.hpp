#ifndef DISCRETE_FUNCTION_TYPE_HPP
#define DISCRETE_FUNCTION_TYPE_HPP

#include <utils/PugsMacros.hpp>

#include <string>

enum class DiscreteFunctionType
{
  P0,
  P0Vector
};

PUGS_INLINE
std::string
name(DiscreteFunctionType type)
{
  switch (type) {
  case DiscreteFunctionType::P0:
    return "P0";
  case DiscreteFunctionType::P0Vector:
    return "P0Vector";
  }
  // LCOV_EXCL_START
  return {};
  // LCOV_EXCL_STOP
}

#endif   // DISCRETE_FUNCTION_TYPE_HPP
