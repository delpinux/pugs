#ifndef I_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define I_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <iostream>
#include <memory>

class IBoundaryDescriptor;

class IBoundaryConditionDescriptor
{
 public:
  enum class Type
  {
    axis,
    dirichlet,
    dirichlet_vector,
    external,
    fourier,
    fixed,
    free,
    inflow,
    inflow_list,
    neumann,
    outflow,
    symmetry,
    wall
  };

 protected:
  virtual std::ostream& _write(std::ostream& os) const = 0;

 public:
  friend std::ostream&
  operator<<(std::ostream& os, const IBoundaryConditionDescriptor& bcd)
  {
    return bcd._write(os);
  }

  virtual const std::shared_ptr<const IBoundaryDescriptor>& boundaryDescriptor_shared() const = 0;
  virtual const IBoundaryDescriptor& boundaryDescriptor() const                               = 0;

  virtual Type type() const = 0;

  IBoundaryConditionDescriptor()                                    = default;
  IBoundaryConditionDescriptor(const IBoundaryConditionDescriptor&) = delete;
  IBoundaryConditionDescriptor(IBoundaryConditionDescriptor&&)      = delete;

  virtual ~IBoundaryConditionDescriptor() = default;
};

#endif   // I_BOUNDARY_CONDITION_DESCRIPTOR_HPP
