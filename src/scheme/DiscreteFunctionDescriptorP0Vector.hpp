#ifndef DISCRETE_FUNCTION_DESCRIPTOR_P0_VECTOR_HPP
#define DISCRETE_FUNCTION_DESCRIPTOR_P0_VECTOR_HPP

#include <scheme/IDiscreteFunctionDescriptor.hpp>

class DiscreteFunctionDescriptorP0Vector : public IDiscreteFunctionDescriptor
{
 public:
  DiscreteFunctionType
  type() const final
  {
    return DiscreteFunctionType::P0Vector;
  }

  DiscreteFunctionDescriptorP0Vector() noexcept = default;

  DiscreteFunctionDescriptorP0Vector(const DiscreteFunctionDescriptorP0Vector&) = default;

  DiscreteFunctionDescriptorP0Vector(DiscreteFunctionDescriptorP0Vector&&) noexcept = default;

  ~DiscreteFunctionDescriptorP0Vector() noexcept = default;
};

#endif   // DISCRETE_FUNCTION_DESCRIPTOR_P0_VECTOR_HPP
