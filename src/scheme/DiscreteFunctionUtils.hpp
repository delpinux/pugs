#ifndef DISCRETE_FUNCTION_UTILS_HPP
#define DISCRETE_FUNCTION_UTILS_HPP

#include <scheme/DiscreteFunctionType.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>
#include <scheme/IDiscreteFunctionDescriptor.hpp>

#include <vector>

PUGS_INLINE
bool
checkDiscretizationType(const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& discrete_function_list,
                        const DiscreteFunctionType& discrete_function_type)
{
  for (const auto& discrete_function_variant : discrete_function_list) {
    if (not std::visit([&](auto&& f) { return f.descriptor().type() == discrete_function_type; },
                       discrete_function_variant->discreteFunction())) {
      return false;
    }
  }
  return true;
}

std::shared_ptr<const MeshVariant> getCommonMesh(
  const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& discrete_function_variant_list);

bool hasSameMesh(const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& discrete_function_variant_list);

bool hasSameMesh(const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& discrete_function_variant_list,
                 const std::shared_ptr<const MeshVariant>& mesh_v);

std::shared_ptr<const DiscreteFunctionVariant> shallowCopy(
  const std::shared_ptr<const MeshVariant>& mesh_v,
  const std::shared_ptr<const DiscreteFunctionVariant>& discrete_function);

#endif   // DISCRETE_FUNCTION_UTILS_HPP
