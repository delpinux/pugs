#include <scheme/DiscreteFunctionUtils.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshVariant.hpp>
#include <scheme/DiscreteFunctionP0.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>
#include <utils/Stringify.hpp>

std::shared_ptr<const MeshVariant>
getCommonMesh(const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& discrete_function_variant_list)
{
  std::optional<size_t> mesh_id;
  std::shared_ptr<const MeshVariant> mesh_v;
  bool is_same_mesh = true;
  for (const auto& discrete_function_variant : discrete_function_variant_list) {
    std::visit(
      [&](auto&& discrete_function) {
        if (not mesh_id.has_value()) {
          mesh_v  = discrete_function.meshVariant();
          mesh_id = discrete_function.meshVariant()->id();
        } else {
          if (mesh_id != discrete_function.meshVariant()->id()) {
            is_same_mesh = false;
            mesh_v.reset();
          }
        }
      },
      discrete_function_variant->discreteFunction());
  }

  return mesh_v;
}

bool
hasSameMesh(const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& discrete_function_variant_list)
{
  std::optional<size_t> mesh_id;

  bool same_mesh = true;
  for (const auto& discrete_function_variant : discrete_function_variant_list) {
    std::visit(
      [&](auto&& discrete_function) {
        if (not mesh_id.has_value()) {
          mesh_id = discrete_function.meshVariant()->id();
        } else {
          if (mesh_id != discrete_function.meshVariant()->id()) {
            same_mesh = false;
          }
        }
      },
      discrete_function_variant->discreteFunction());
  }

  return same_mesh;
}

bool
hasSameMesh(const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& discrete_function_variant_list,
            const std::shared_ptr<const MeshVariant>& mesh_v)
{
  const size_t mesh_id = mesh_v->id();

  bool same_mesh = true;
  for (const auto& discrete_function_variant : discrete_function_variant_list) {
    std::visit(
      [&](auto&& discrete_function) {
        if (mesh_id != discrete_function.meshVariant()->id()) {
          same_mesh = false;
        }
      },
      discrete_function_variant->discreteFunction());
  }

  return same_mesh;
}

template <typename DiscreteFunctionT>
std::shared_ptr<const DiscreteFunctionVariant>
shallowCopy(const std::shared_ptr<const MeshVariant>& mesh_v, const DiscreteFunctionT& f)
{
  const size_t function_connectivity_id = f.meshVariant()->shared_connectivity()->id();

  if (mesh_v->shared_connectivity()->id() != function_connectivity_id) {
    throw NormalError("cannot shallow copy when connectivity changes");
  }

  if constexpr (is_discrete_function_P0_v<DiscreteFunctionT>) {
    return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionT(mesh_v, f.cellValues()));
  } else if constexpr (is_discrete_function_P0_vector_v<DiscreteFunctionT>) {
    return std::make_shared<DiscreteFunctionVariant>(DiscreteFunctionT(mesh_v, f.cellArrays()));
  } else {
    throw UnexpectedError("invalid discrete function type");
  }
}

std::shared_ptr<const DiscreteFunctionVariant>
shallowCopy(const std::shared_ptr<const MeshVariant>& mesh_v,
            const std::shared_ptr<const DiscreteFunctionVariant>& discrete_function_variant)
{
  return std::visit(
    [&](auto&& f) {
      const size_t mesh_dimension = std::visit([](auto&& mesh) { return mesh->dimension(); }, mesh_v->variant());
      const size_t f_mesh_dimension =
        std::visit([](auto&& mesh) { return mesh->dimension(); }, f.meshVariant()->variant());
      if (mesh_v->id() == f.meshVariant()->id()) {
        return discrete_function_variant;
      } else if (mesh_dimension != f_mesh_dimension) {
        throw NormalError("incompatible mesh dimensions");
      }

      return shallowCopy(mesh_v, f);
    },
    discrete_function_variant->discreteFunction());
}
