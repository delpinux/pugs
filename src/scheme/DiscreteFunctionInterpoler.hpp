#ifndef DISCRETE_FUNCTION_INTERPOLER_HPP
#define DISCRETE_FUNCTION_INTERPOLER_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IZoneDescriptor.hpp>
#include <mesh/MeshTraits.hpp>
#include <scheme/IDiscreteFunctionDescriptor.hpp>

class DiscreteFunctionVariant;
class MeshVariant;

#include <memory>

class DiscreteFunctionInterpoler
{
 private:
  std::shared_ptr<const MeshVariant> m_mesh;
  const std::vector<std::shared_ptr<const IZoneDescriptor>> m_zone_list;
  std::shared_ptr<const IDiscreteFunctionDescriptor> m_discrete_function_descriptor;
  const FunctionSymbolId m_function_id;

  template <MeshConcept MeshType, typename DataType, typename ValueType = DataType>
  DiscreteFunctionVariant _interpolateOnZoneList() const;

  template <MeshConcept MeshType, typename DataType, typename ValueType = DataType>
  DiscreteFunctionVariant _interpolateGlobally() const;

  template <MeshConcept MeshType, typename DataType, typename ValueType = DataType>
  DiscreteFunctionVariant _interpolate() const;

  template <MeshConcept MeshType>
  DiscreteFunctionVariant _interpolate() const;

 public:
  DiscreteFunctionVariant interpolate() const;

  DiscreteFunctionInterpoler(const std::shared_ptr<const MeshVariant>& mesh,
                             const std::shared_ptr<const IDiscreteFunctionDescriptor>& discrete_function_descriptor,
                             const FunctionSymbolId& function_id)
    : m_mesh{mesh}, m_discrete_function_descriptor{discrete_function_descriptor}, m_function_id{function_id}
  {}

  DiscreteFunctionInterpoler(const std::shared_ptr<const MeshVariant>& mesh,
                             const std::vector<std::shared_ptr<const IZoneDescriptor>>& zone_list,
                             const std::shared_ptr<const IDiscreteFunctionDescriptor>& discrete_function_descriptor,
                             const FunctionSymbolId& function_id)
    : m_mesh{mesh},
      m_zone_list{zone_list},
      m_discrete_function_descriptor{discrete_function_descriptor},
      m_function_id{function_id}
  {}

  DiscreteFunctionInterpoler(const DiscreteFunctionInterpoler&) = delete;
  DiscreteFunctionInterpoler(DiscreteFunctionInterpoler&&)      = delete;

  ~DiscreteFunctionInterpoler() = default;
};

#endif   // DISCRETE_FUNCTION_INTERPOLER_HPP
