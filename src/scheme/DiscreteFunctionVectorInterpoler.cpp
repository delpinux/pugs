#include <scheme/DiscreteFunctionVectorInterpoler.hpp>

#include <language/utils/InterpolateItemArray.hpp>
#include <mesh/MeshCellZone.hpp>
#include <mesh/MeshTraits.hpp>
#include <scheme/DiscreteFunctionP0Vector.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>
#include <utils/Exceptions.hpp>

template <MeshConcept MeshType, typename DataType>
DiscreteFunctionVariant
DiscreteFunctionVectorInterpoler::_interpolateOnZoneList() const
{
  Assert(m_zone_list.size() > 0, "no zone list provided");
  constexpr size_t Dimension = MeshType::Dimension;

  std::shared_ptr p_mesh        = m_mesh->get<MeshType>();
  MeshData<MeshType>& mesh_data = MeshDataManager::instance().getMeshData(*p_mesh);

  CellValue<bool> is_in_zone{p_mesh->connectivity()};
  is_in_zone.fill(false);

  size_t number_of_cells = 0;
  for (const auto& zone : m_zone_list) {
    auto mesh_cell_zone   = getMeshCellZone(*p_mesh, *zone);
    const auto& cell_list = mesh_cell_zone.cellList();
    for (size_t i_cell = 0; i_cell < cell_list.size(); ++i_cell) {
      const CellId cell_id = cell_list[i_cell];
      if (is_in_zone[cell_id]) {
        std::ostringstream os;
        os << "cell " << cell_id << " (number " << p_mesh->connectivity().cellNumber()[cell_id]
           << ") is present multiple times in zone list";
        throw NormalError(os.str());
      }
      ++number_of_cells;
      is_in_zone[cell_id] = true;
    }
  }

  Array<CellId> zone_cell_list{number_of_cells};
  {
    size_t i_cell = 0;
    for (CellId cell_id = 0; cell_id < p_mesh->numberOfCells(); ++cell_id) {
      if (is_in_zone[cell_id]) {
        zone_cell_list[i_cell++] = cell_id;
      }
    }
  }

  Table<const DataType> table =
    InterpolateItemArray<DataType(TinyVector<Dimension>)>::template interpolate<ItemType::cell>(m_function_id_list,
                                                                                                mesh_data.xj(),
                                                                                                zone_cell_list);

  CellArray<DataType> cell_array{p_mesh->connectivity(), m_function_id_list.size()};
  cell_array.fill(0);

  parallel_for(
    zone_cell_list.size(), PUGS_LAMBDA(const size_t i_cell) {
      for (size_t i = 0; i < table.numberOfColumns(); ++i) {
        cell_array[zone_cell_list[i_cell]][i] = table(i_cell, i);
      }
    });

  return DiscreteFunctionP0Vector<DataType>(p_mesh, cell_array);
}

template <MeshConcept MeshType, typename DataType>
DiscreteFunctionVariant
DiscreteFunctionVectorInterpoler::_interpolateGlobally() const
{
  Assert(m_zone_list.size() == 0, "invalid call when zones are defined");
  constexpr size_t Dimension = MeshType::Dimension;

  std::shared_ptr p_mesh = m_mesh->get<MeshType>();

  MeshData<MeshType>& mesh_data = MeshDataManager::instance().getMeshData(*p_mesh);

  return DiscreteFunctionP0Vector<DataType>(m_mesh,
                                            InterpolateItemArray<DataType(TinyVector<Dimension>)>::template interpolate<
                                              ItemType::cell>(m_function_id_list, mesh_data.xj()));
}

template <MeshConcept MeshType, typename DataType>
DiscreteFunctionVariant
DiscreteFunctionVectorInterpoler::_interpolate() const
{
  if (m_zone_list.size() == 0) {
    return this->_interpolateGlobally<MeshType, DataType>();
  } else {
    return this->_interpolateOnZoneList<MeshType, DataType>();
  }
}

template <MeshConcept MeshType>
DiscreteFunctionVariant
DiscreteFunctionVectorInterpoler::_interpolate() const
{
  auto check_data_type = [](const ASTNodeDataType& data_type) {
    switch (data_type) {
    case ASTNodeDataType::bool_t:
    case ASTNodeDataType::unsigned_int_t:
    case ASTNodeDataType::int_t:
    case ASTNodeDataType::double_t: {
      break;
    }
    default: {
      std::ostringstream os;
      os << "vector functions require scalar value type.\n"
         << "Invalid interpolation value type: " << rang::fgB::red << dataTypeName(data_type) << rang::style::reset;
      throw NormalError(os.str());
    }
    }
  };

  if (m_function_id_list.size() == 1) {
    const auto& function_descriptor = m_function_id_list[0].descriptor();
    if ((function_descriptor.domainMappingNode().children[1]->m_data_type == ASTNodeDataType::typename_t) or
        (function_descriptor.domainMappingNode().children[1]->m_data_type == ASTNodeDataType::tuple_t)) {
      const ASTNodeDataType& data_type = function_descriptor.domainMappingNode().children[1]->m_data_type.contentType();

      check_data_type(data_type);
    } else {
      throw UnexpectedError("incorrect function");
    }
  } else {
    for (const auto& function_id : m_function_id_list) {
      const auto& function_descriptor = function_id.descriptor();
      if (function_descriptor.domainMappingNode().children[1]->m_data_type == ASTNodeDataType::typename_t) {
        const ASTNodeDataType& data_type =
          function_descriptor.domainMappingNode().children[1]->m_data_type.contentType();

        check_data_type(data_type);
      } else {
        throw UnexpectedError("incorrect function");
      }
    }
  }

  return this->_interpolate<MeshType, double>();
}

DiscreteFunctionVariant
DiscreteFunctionVectorInterpoler::interpolate() const
{
  if (m_discrete_function_descriptor->type() != DiscreteFunctionType::P0Vector) {
    throw NormalError("invalid discrete function type for vector interpolation");
  }

  return std::visit(
    [&](auto&& mesh) {
      using MeshType = mesh_type_t<decltype(mesh)>;

      if constexpr (is_polygonal_mesh_v<MeshType>) {
        return this->_interpolate<MeshType>();
      } else {
        throw NormalError("unexpected mesh type");
      }
    },
    m_mesh->variant());
}
