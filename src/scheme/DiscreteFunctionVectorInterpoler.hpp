#ifndef DISCRETE_FUNCTION_VECTOR_INTERPOLER_HPP
#define DISCRETE_FUNCTION_VECTOR_INTERPOLER_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IZoneDescriptor.hpp>
#include <mesh/MeshTraits.hpp>
#include <scheme/IDiscreteFunctionDescriptor.hpp>

class DiscreteFunctionVariant;
class MeshVariant;

#include <memory>
#include <vector>

class DiscreteFunctionVectorInterpoler
{
 private:
  std::shared_ptr<const MeshVariant> m_mesh;
  const std::vector<std::shared_ptr<const IZoneDescriptor>> m_zone_list;
  std::shared_ptr<const IDiscreteFunctionDescriptor> m_discrete_function_descriptor;
  const std::vector<FunctionSymbolId> m_function_id_list;

  template <MeshConcept MeshType, typename DataType>
  DiscreteFunctionVariant _interpolateOnZoneList() const;

  template <MeshConcept MeshType, typename DataType>
  DiscreteFunctionVariant _interpolateGlobally() const;

  template <MeshConcept MeshType, typename DataType>
  DiscreteFunctionVariant _interpolate() const;

  template <MeshConcept MeshType>
  DiscreteFunctionVariant _interpolate() const;

 public:
  DiscreteFunctionVariant interpolate() const;

  DiscreteFunctionVectorInterpoler(
    const std::shared_ptr<const MeshVariant>& mesh,
    const std::shared_ptr<const IDiscreteFunctionDescriptor>& discrete_function_descriptor,
    const std::vector<FunctionSymbolId>& function_id_list)
    : m_mesh{mesh}, m_discrete_function_descriptor{discrete_function_descriptor}, m_function_id_list{function_id_list}
  {}

  DiscreteFunctionVectorInterpoler(
    const std::shared_ptr<const MeshVariant>& mesh,
    const std::vector<std::shared_ptr<const IZoneDescriptor>>& zone_list,
    const std::shared_ptr<const IDiscreteFunctionDescriptor>& discrete_function_descriptor,
    const std::vector<FunctionSymbolId>& function_id_list)
    : m_mesh{mesh},
      m_zone_list{zone_list},
      m_discrete_function_descriptor{discrete_function_descriptor},
      m_function_id_list{function_id_list}
  {}

  DiscreteFunctionVectorInterpoler(const DiscreteFunctionVectorInterpoler&) = delete;
  DiscreteFunctionVectorInterpoler(DiscreteFunctionVectorInterpoler&&)      = delete;

  ~DiscreteFunctionVectorInterpoler() = default;
};

#endif   // DISCRETE_FUNCTION_VECTOR_INTERPOLER_HPP
