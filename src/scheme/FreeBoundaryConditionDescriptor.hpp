#ifndef FREE_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define FREE_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>

class FreeBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "free(" << *m_boundary_descriptor << ")";
    return os;
  }

 public:
  Type
  type() const final
  {
    return Type::free;
  }

  FreeBoundaryConditionDescriptor(std::shared_ptr<const IBoundaryDescriptor> boundary_descriptor)
    : BoundaryConditionDescriptorBase{boundary_descriptor}
  {
    ;
  }

  FreeBoundaryConditionDescriptor(const FreeBoundaryConditionDescriptor&) = delete;
  FreeBoundaryConditionDescriptor(FreeBoundaryConditionDescriptor&&)      = delete;

  ~FreeBoundaryConditionDescriptor() = default;
};

#endif   // FREE_BOUNDARY_CONDITION_DESCRIPTOR_HPP
