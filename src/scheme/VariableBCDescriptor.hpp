#ifndef VARIABLE_BC_DESCRIPTOR_HPP
#define VARIABLE_BC_DESCRIPTOR_HPP

class DiscreteFunctionVariant;
class IBoundaryConditionDescriptor;

#include <memory>
#include <vector>

class VariableBCDescriptor
{
 private:
  std::shared_ptr<const DiscreteFunctionVariant> m_discrete_function_variant;
  std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>> m_bc_descriptor_list;

 public:
  const std::shared_ptr<const DiscreteFunctionVariant>&
  discreteFunctionVariant() const
  {
    return m_discrete_function_variant;
  }

  const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>&
  bcDescriptorList() const
  {
    return m_bc_descriptor_list;
  }

  VariableBCDescriptor(const std::shared_ptr<const DiscreteFunctionVariant>& discrete_function_variant,
                       const std::vector<std::shared_ptr<const IBoundaryConditionDescriptor>>& bc_descriptor_list)
    : m_discrete_function_variant{discrete_function_variant}, m_bc_descriptor_list{bc_descriptor_list}
  {}

  VariableBCDescriptor(const VariableBCDescriptor&) = default;
  VariableBCDescriptor(VariableBCDescriptor&&)      = default;

  VariableBCDescriptor()  = default;
  ~VariableBCDescriptor() = default;
};

#endif   // VARIABLE_BC_DESCRIPTOR_HPP
