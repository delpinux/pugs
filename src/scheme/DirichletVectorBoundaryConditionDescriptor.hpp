#ifndef DIRICHLET_VECTOR_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define DIRICHLET_VECTOR_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>
#include <vector>

class DirichletVectorBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "dirichlet_vector(" << m_name << ',' << *m_boundary_descriptor << ")";
    return os;
  }

  const std::string m_name;

  const std::vector<FunctionSymbolId> m_rhs_symbol_id_list;

 public:
  const std::string&
  name() const
  {
    return m_name;
  }

  const std::vector<FunctionSymbolId>&
  rhsSymbolIdList() const
  {
    return m_rhs_symbol_id_list;
  }

  Type
  type() const final
  {
    return Type::dirichlet_vector;
  }

  DirichletVectorBoundaryConditionDescriptor(const std::string_view name,
                                             std::shared_ptr<const IBoundaryDescriptor> boundary_descriptor,
                                             const std::vector<FunctionSymbolId>& rhs_symbol_id_list)
    : BoundaryConditionDescriptorBase{boundary_descriptor}, m_name{name}, m_rhs_symbol_id_list{rhs_symbol_id_list}
  {
    ;
  }

  DirichletVectorBoundaryConditionDescriptor(const DirichletVectorBoundaryConditionDescriptor&) = delete;
  DirichletVectorBoundaryConditionDescriptor(DirichletVectorBoundaryConditionDescriptor&&)      = delete;

  ~DirichletVectorBoundaryConditionDescriptor() = default;
};

#endif   // DIRICHLET_VECTOR_BOUNDARY_CONDITION_DESCRIPTOR_HPP
