#ifndef NEUMANN_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define NEUMANN_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>

class NeumannBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "neumann(" << m_name << ',' << *m_boundary_descriptor << ")";
    return os;
  }

  const std::string m_name;

  const FunctionSymbolId m_rhs_symbol_id;

 public:
  const std::string&
  name() const
  {
    return m_name;
  }

  FunctionSymbolId
  rhsSymbolId() const
  {
    return m_rhs_symbol_id;
  }

  Type
  type() const final
  {
    return Type::neumann;
  }

  NeumannBoundaryConditionDescriptor(const std::string& name,
                                     const std::shared_ptr<const IBoundaryDescriptor>& boundary_descriptor,
                                     const FunctionSymbolId& rhs_symbol_id)
    : BoundaryConditionDescriptorBase{boundary_descriptor}, m_name{name}, m_rhs_symbol_id{rhs_symbol_id}
  {
    ;
  }

  NeumannBoundaryConditionDescriptor(const NeumannBoundaryConditionDescriptor&) = delete;
  NeumannBoundaryConditionDescriptor(NeumannBoundaryConditionDescriptor&&)      = delete;

  ~NeumannBoundaryConditionDescriptor() = default;
};

#endif   // NEUMANN_BOUNDARY_CONDITION_DESCRIPTOR_HPP
