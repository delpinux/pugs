#ifndef FOURIER_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define FOURIER_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>

class FourierBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "fourier(" << m_name << ',' << *m_boundary_descriptor << ")";
    return os;
  }

  const std::string m_name;

  const FunctionSymbolId m_mass_symbol_id;
  const FunctionSymbolId m_rhs_symbol_id;

 public:
  const std::string&
  name() const
  {
    return m_name;
  }

  FunctionSymbolId
  massSymbolId() const
  {
    return m_mass_symbol_id;
  }

  FunctionSymbolId
  rhsSymbolId() const
  {
    return m_rhs_symbol_id;
  }

  Type
  type() const final
  {
    return Type::fourier;
  }

  FourierBoundaryConditionDescriptor(const std::string& name,
                                     const std::shared_ptr<const IBoundaryDescriptor>& boundary_descriptor,
                                     const FunctionSymbolId& mass_symbol_id,
                                     const FunctionSymbolId& rhs_symbol_id)
    : BoundaryConditionDescriptorBase{boundary_descriptor},
      m_name{name},
      m_mass_symbol_id{mass_symbol_id},
      m_rhs_symbol_id{rhs_symbol_id}
  {
    ;
  }

  FourierBoundaryConditionDescriptor(const FourierBoundaryConditionDescriptor&) = delete;
  FourierBoundaryConditionDescriptor(FourierBoundaryConditionDescriptor&&)      = delete;

  ~FourierBoundaryConditionDescriptor() = default;
};

#endif   // FOURIER_BOUNDARY_CONDITION_DESCRIPTOR_HPP
