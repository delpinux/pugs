#ifndef OUTFLOW_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define OUTFLOW_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>

class OutflowBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "outflow(" << *m_boundary_descriptor << ")";
    return os;
  }

 public:
  Type
  type() const final
  {
    return Type::outflow;
  }

  OutflowBoundaryConditionDescriptor(const std::shared_ptr<const IBoundaryDescriptor>& boundary_descriptor)
    : BoundaryConditionDescriptorBase{boundary_descriptor}
  {
    ;
  }

  OutflowBoundaryConditionDescriptor(const OutflowBoundaryConditionDescriptor&) = delete;
  OutflowBoundaryConditionDescriptor(OutflowBoundaryConditionDescriptor&&)      = delete;

  ~OutflowBoundaryConditionDescriptor() = default;
};

#endif   // OUTFLOW_BOUNDARY_CONDITION_DESCRIPTOR_HPP
