#ifndef FACE_INTEGRATOR_HPP
#define FACE_INTEGRATOR_HPP

#include <analysis/IQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/LineTransformation.hpp>
#include <geometry/SquareTransformation.hpp>
#include <geometry/TriangleTransformation.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemId.hpp>
#include <utils/Array.hpp>

#include <functional>

class FaceIntegrator
{
 private:
  template <size_t Dimension>
  class AllFaces
  {
   private:
    const Connectivity<Dimension>& m_connectivity;

   public:
    using index_type = FaceId;

    PUGS_INLINE
    FaceId
    faceId(const FaceId face_id) const
    {
      return face_id;
    }

    PUGS_INLINE
    size_t
    size() const
    {
      return m_connectivity.numberOfFaces();
    }

    PUGS_INLINE
    AllFaces(const Connectivity<Dimension>& connectivity) : m_connectivity{connectivity} {}
  };

  template <template <typename T> typename ArrayT>
  class FaceList
  {
   private:
    const ArrayT<FaceId>& m_face_list;

   public:
    using index_type = typename ArrayT<FaceId>::index_type;

    PUGS_INLINE
    FaceId
    faceId(const index_type index) const
    {
      return m_face_list[index];
    }

    PUGS_INLINE
    size_t
    size() const
    {
      return m_face_list.size();
    }

    PUGS_INLINE
    FaceList(const ArrayT<FaceId>& face_list) : m_face_list{face_list} {}
  };

  template <MeshConcept MeshType, typename OutputArrayT, typename ListTypeT, typename OutputType, typename InputType>
  static PUGS_INLINE void
  _tensorialIntegrateTo(std::function<OutputType(InputType)> function,
                        const IQuadratureDescriptor& quadrature_descriptor,
                        const MeshType& mesh,
                        const ListTypeT& face_list,
                        OutputArrayT& value)
  {
    constexpr size_t Dimension = MeshType::Dimension;
    static_assert(Dimension > 1);

    static_assert(std::is_same_v<TinyVector<Dimension>, std::decay_t<InputType>>, "invalid input data type");
    static_assert(std::is_same_v<std::remove_const_t<typename OutputArrayT::data_type>, OutputType>,
                  "invalid output data type");

    using execution_space = typename Kokkos::DefaultExecutionSpace::execution_space;
    Kokkos::Experimental::UniqueToken<execution_space, Kokkos::Experimental::UniqueTokenScope::Global> tokens;

    if constexpr (std::is_arithmetic_v<OutputType>) {
      value.fill(0);
    } else if constexpr (is_tiny_vector_v<OutputType> or is_tiny_matrix_v<OutputType>) {
      value.fill(zero);
    } else {
      static_assert(std::is_same_v<OutputType, double>, "unexpected output type");
    }

    const auto& connectivity = mesh.connectivity();

    const auto face_to_node_matrix = connectivity.faceToNodeMatrix();
    const auto xr                  = mesh.xr();

    if constexpr (Dimension == 2) {
      const auto qf = QuadratureManager::instance().getLineFormula(quadrature_descriptor);

      parallel_for(face_list.size(), [=, &qf, &face_list](typename ListTypeT::index_type index) {
        const FaceId face_id = face_list.faceId(index);

        const auto face_to_node = face_to_node_matrix[face_id];

        Assert(face_to_node.size() == 2);
        const LineTransformation<2> T(xr[face_to_node[0]], xr[face_to_node[1]]);

        for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
          const auto xi = qf.point(i_point);
          value[index] += qf.weight(i_point) * T.velocityNorm() * function(T(xi));
        }
      });

    } else if constexpr (Dimension == 3) {
      const auto qf = QuadratureManager::instance().getSquareFormula(quadrature_descriptor);

      auto invalid_face = std::make_pair(false, FaceId{});

      parallel_for(face_list.size(), [=, &invalid_face, &qf, &face_list](typename ListTypeT::index_type index) {
        const FaceId face_id = face_list.faceId(index);

        const auto face_to_node = face_to_node_matrix[face_id];

        switch (face_to_node.size()) {
        case 3: {
          SquareTransformation<3> T(xr[face_to_node[0]], xr[face_to_node[1]], xr[face_to_node[2]], xr[face_to_node[2]]);
          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.areaVariationNorm(xi) * function(T(xi));
          }
          break;
        }
        case 4: {
          SquareTransformation<3> T(xr[face_to_node[0]], xr[face_to_node[1]], xr[face_to_node[2]], xr[face_to_node[3]]);
          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            value[index] += qf.weight(i_point) * T.areaVariationNorm(xi) * function(T(xi));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_face = std::make_pair(true, face_id);
        }
          // LCOV_EXCL_STOP
        }
      });

      // LCOV_EXCL_START
      if (invalid_face.first) {
        std::ostringstream os;
        os << "invalid face type for integration: " << face_to_node_matrix[invalid_face.second].size() << " points";
        throw UnexpectedError(os.str());
      }
      // LCOV_EXCL_STOP

    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("invalid dimension for face integration");
      // LCOV_EXCL_STOP
    }
  }

  template <MeshConcept MeshType, typename OutputArrayT, typename ListTypeT, typename OutputType, typename InputType>
  static PUGS_INLINE void
  _directIntegrateTo(std::function<OutputType(InputType)> function,
                     const IQuadratureDescriptor& quadrature_descriptor,
                     const MeshType& mesh,
                     const ListTypeT& face_list,
                     OutputArrayT& value)
  {
    Assert(not quadrature_descriptor.isTensorial());

    constexpr size_t Dimension = MeshType::Dimension;
    static_assert(Dimension > 1);

    static_assert(std::is_same_v<TinyVector<Dimension>, std::decay_t<InputType>>, "invalid input data type");
    static_assert(std::is_same_v<std::remove_const_t<typename OutputArrayT::data_type>, OutputType>,
                  "invalid output data type");

    if constexpr (std::is_arithmetic_v<OutputType>) {
      value.fill(0);
    } else if constexpr (is_tiny_vector_v<OutputType> or is_tiny_matrix_v<OutputType>) {
      value.fill(zero);
    } else {
      static_assert(std::is_same_v<OutputType, double>, "unexpected output type");
    }

    const auto& connectivity = mesh.connectivity();

    const auto face_to_node_matrix = connectivity.faceToNodeMatrix();
    const auto xr                  = mesh.xr();

    if constexpr (Dimension == 2) {
      parallel_for(face_list.size(),
                   [=, &quadrature_descriptor, &face_list](const typename ListTypeT::index_type& index) {
                     const FaceId face_id = face_list.faceId(index);

                     const auto face_to_node = face_to_node_matrix[face_id];

                     Assert(face_to_node.size() == 2);
                     const LineTransformation<2> T(xr[face_to_node[0]], xr[face_to_node[1]]);
                     const auto qf = QuadratureManager::instance().getLineFormula(quadrature_descriptor);

                     for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                       const auto xi = qf.point(i_point);
                       value[index] += qf.weight(i_point) * T.velocityNorm() * function(T(xi));
                     }
                   });

    } else if constexpr (Dimension == 3) {
      auto invalid_face = std::make_pair(false, FaceId{});

      parallel_for(face_list.size(),
                   [=, &invalid_face, &quadrature_descriptor, &face_list](const typename ListTypeT::index_type& index) {
                     const FaceId face_id = face_list.faceId(index);

                     const auto face_to_node = face_to_node_matrix[face_id];

                     switch (face_to_node.size()) {
                     case 3: {
                       const TriangleTransformation<3> T(xr[face_to_node[0]], xr[face_to_node[1]], xr[face_to_node[2]]);
                       const auto qf = QuadratureManager::instance().getTriangleFormula(quadrature_descriptor);

                       for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                         const auto xi = qf.point(i_point);
                         value[index] += qf.weight(i_point) * T.areaVariationNorm() * function(T(xi));
                       }
                       break;
                     }
                     case 4: {
                       const SquareTransformation<3> T(xr[face_to_node[0]], xr[face_to_node[1]], xr[face_to_node[2]],
                                                       xr[face_to_node[3]]);
                       const auto qf = QuadratureManager::instance().getSquareFormula(quadrature_descriptor);

                       for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                         const auto xi = qf.point(i_point);
                         value[index] += qf.weight(i_point) * T.areaVariationNorm(xi) * function(T(xi));
                       }
                       break;
                     }
                       // LCOV_EXCL_START
                     default: {
                       invalid_face = std::make_pair(true, face_id);
                       break;
                     }
                       // LCOV_EXCL_STOP
                     }
                   });

      // LCOV_EXCL_START
      if (invalid_face.first) {
        std::ostringstream os;
        os << "invalid face type for integration: " << face_to_node_matrix[invalid_face.second].size() << " points";
        throw UnexpectedError(os.str());
      }
      // LCOV_EXCL_STOP
    }
  }

 public:
  template <MeshConcept MeshType, typename OutputArrayT, typename OutputType, typename InputType>
  static PUGS_INLINE void
  integrateTo(const std::function<OutputType(InputType)>& function,
              const IQuadratureDescriptor& quadrature_descriptor,
              const MeshType& mesh,
              OutputArrayT& value)
  {
    constexpr size_t Dimension = MeshType::Dimension;

    if (quadrature_descriptor.isTensorial()) {
      _tensorialIntegrateTo<MeshType, OutputArrayT>(function, quadrature_descriptor, mesh,
                                                    AllFaces<Dimension>{mesh.connectivity()}, value);
    } else {
      _directIntegrateTo<MeshType, OutputArrayT>(function, quadrature_descriptor, mesh,
                                                 AllFaces<Dimension>{mesh.connectivity()}, value);
    }
  }

  template <MeshConcept MeshType, typename OutputArrayT, typename FunctionType>
  static PUGS_INLINE void
  integrateTo(const FunctionType& function,
              const IQuadratureDescriptor& quadrature_descriptor,
              const MeshType& mesh,
              OutputArrayT& value)
  {
    integrateTo(std::function(function), quadrature_descriptor, mesh, value);
  }

  template <MeshConcept MeshType, typename OutputType, typename InputType, template <typename DataType> typename ArrayT>
  static PUGS_INLINE ArrayT<OutputType>
  integrate(const std::function<OutputType(InputType)>& function,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh,
            const ArrayT<FaceId>& face_list)
  {
    ArrayT<OutputType> value(size(face_list));
    if (quadrature_descriptor.isTensorial()) {
      _tensorialIntegrateTo<MeshType, ArrayT<OutputType>>(function, quadrature_descriptor, mesh, FaceList{face_list},
                                                          value);
    } else {
      _directIntegrateTo<MeshType, ArrayT<OutputType>>(function, quadrature_descriptor, mesh, FaceList{face_list},
                                                       value);
    }

    return value;
  }

  template <MeshConcept MeshType, typename FunctionType, template <typename DataType> typename ArrayT>
  static PUGS_INLINE auto
  integrate(const FunctionType& function,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh,
            const ArrayT<FaceId>& face_list)
  {
    return integrate(std::function(function), quadrature_descriptor, mesh, face_list);
  }
};

#endif   // FACE_INTEGRATOR_HPP
