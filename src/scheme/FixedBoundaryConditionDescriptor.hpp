#ifndef FIXED_BOUNDARY_CONDITION_DESCRIPTOR_HPP
#define FIXED_BOUNDARY_CONDITION_DESCRIPTOR_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IBoundaryDescriptor.hpp>
#include <scheme/BoundaryConditionDescriptorBase.hpp>

#include <memory>

class FixedBoundaryConditionDescriptor : public BoundaryConditionDescriptorBase
{
 private:
  std::ostream&
  _write(std::ostream& os) const final
  {
    os << "fixed(" << *m_boundary_descriptor << ")";
    return os;
  }

  std::shared_ptr<const IBoundaryDescriptor> m_boundary_descriptor;

 public:
  Type
  type() const final
  {
    return Type::fixed;
  }

  FixedBoundaryConditionDescriptor(const std::shared_ptr<const IBoundaryDescriptor>& boundary_descriptor)
    : BoundaryConditionDescriptorBase{boundary_descriptor}
  {}

  FixedBoundaryConditionDescriptor(const FixedBoundaryConditionDescriptor&) = delete;
  FixedBoundaryConditionDescriptor(FixedBoundaryConditionDescriptor&&)      = delete;

  ~FixedBoundaryConditionDescriptor() = default;
};

#endif   // FIXED_BOUNDARY_CONDITION_DESCRIPTOR_HPP
