#ifndef ARRAY_SUBSCRIPT_PROCESSOR_HPP
#define ARRAY_SUBSCRIPT_PROCESSOR_HPP

#include <language/ast/ASTNode.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <language/utils/ParseError.hpp>

template <typename ArrayTypeT>
class ArraySubscriptProcessor : public INodeProcessor
{
 private:
  ASTNode& m_array_subscript_expression;

 public:
  Type
  type() const
  {
    return Type::array_subscript_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    auto get_index_value = [&](DataVariant&& value_variant) -> int64_t {
      int64_t index_value = 0;
      std::visit(
        [&](auto&& value) {
          using ValueT = std::decay_t<decltype(value)>;
          if constexpr (std::is_integral_v<ValueT>) {
            index_value = value;
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("invalid index type");
            // LCOV_EXCL_STOP
          }
        },
        value_variant);
      return index_value;
    };

    if constexpr (is_tiny_vector_v<ArrayTypeT>) {
      auto& index_expression = *m_array_subscript_expression.children[1];

      const int64_t index_value = get_index_value(index_expression.execute(exec_policy));

      auto& array_expression = *m_array_subscript_expression.children[0];

      auto&& array_value = array_expression.execute(exec_policy);
      ArrayTypeT& array  = std::get<ArrayTypeT>(array_value);

      return array[index_value];
    } else if constexpr (is_tiny_matrix_v<ArrayTypeT>) {
      auto& index0_expression = *m_array_subscript_expression.children[1];
      auto& index1_expression = *m_array_subscript_expression.children[2];

      const int64_t index0_value = get_index_value(index0_expression.execute(exec_policy));
      const int64_t index1_value = get_index_value(index1_expression.execute(exec_policy));

      auto& array_expression = *m_array_subscript_expression.children[0];

      auto&& array_value = array_expression.execute(exec_policy);
      ArrayTypeT& array  = std::get<ArrayTypeT>(array_value);

      return array(index0_value, index1_value);
    }
  }

  ArraySubscriptProcessor(ASTNode& array_subscript_expression)
    : m_array_subscript_expression{array_subscript_expression}
  {}

  virtual ~ArraySubscriptProcessor() = default;
};

#endif   // ARRAY_SUBSCRIPT_PROCESSOR_HPP
