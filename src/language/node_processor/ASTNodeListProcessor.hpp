#ifndef AST_NODE_LIST_PROCESSOR_HPP
#define AST_NODE_LIST_PROCESSOR_HPP

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <language/utils/ASTCheckpointsInfo.hpp>
#include <language/utils/SymbolTable.hpp>
#include <utils/checkpointing/Checkpoint.hpp>
#include <utils/checkpointing/ResumingManager.hpp>
#include <utils/pugs_config.hpp>

class ASTNodeListProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_node;

 public:
  Type
  type() const
  {
    return Type::list_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    ResumingManager& resuming_manager = ResumingManager::getInstance();
    if (resuming_manager.isResuming()) {
#ifdef PUGS_HAS_HDF5
      const size_t checkpoint_id = resuming_manager.checkpointId();

      const ASTCheckpointsInfo& ast_checkpoint_info = ASTCheckpointsInfo::getInstance();
      const ASTCheckpoint& ast_checkpoint           = ast_checkpoint_info.getASTCheckpoint(checkpoint_id);

      for (size_t i_child = ast_checkpoint.getASTLocation()[resuming_manager.currentASTLevel()++];
           i_child < m_node.children.size(); ++i_child) {
        m_node.children[i_child]->execute(exec_policy);
      }
#endif   // PUGS_HAS_HDF5
    } else {
      for (auto&& child : m_node.children) {
        child->execute(exec_policy);
      }
    }

    if (not(m_node.is_root() or m_node.is_type<language::for_statement_block>()))
      m_node.m_symbol_table->clearValues();

    return {};
  }

  ASTNodeListProcessor(ASTNode& node) : m_node{node} {}
};

#endif   // AST_NODE_LIST_PROCESSOR_HPP
