#ifndef NODE_PROCESSOR_HPP
#define NODE_PROCESSOR_HPP

#include <language/ast/ASTNode.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <language/utils/SymbolTable.hpp>

class NameProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_node;
  DataVariant* p_value{nullptr};

 public:
  Type
  type() const
  {
    return Type::name_processor;
  }

  DataVariant
  execute(ExecutionPolicy&)
  {
    if (std::holds_alternative<std::monostate>(*p_value)) {
      std::ostringstream os;
      os << "trying to use uninitialized symbol '" << rang::fgB::red << m_node.string() << rang::fg::reset << '\'';
      throw ParseError(os.str(), std::vector{m_node.begin()});
    }
    return *p_value;
  }

  NameProcessor(ASTNode& node) : m_node{node}
  {
    const std::string& symbol = m_node.string();
    auto [i_symbol, found]    = m_node.m_symbol_table->find(symbol, m_node.begin());
    Assert(found);
    p_value = &(i_symbol->attributes().value());
  }
};

#endif   // NODE_PROCESSOR_HPP
