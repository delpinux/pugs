#ifndef INNER_LIST_TO_VECTOR_PROCESSOR_HPP
#define INNER_LIST_TO_VECTOR_PROCESSOR_HPP

#include <language/ast/ASTNode.hpp>
#include <language/node_processor/ASTNodeExpressionListProcessor.hpp>
#include <language/node_processor/INodeProcessor.hpp>

class InnerListToVectorProcessor final : public INodeProcessor
{
 private:
  std::unique_ptr<ASTNodeExpressionListProcessor> m_expression_list_processor;

 public:
  Type
  type() const
  {
    return Type::inner_list_to_vector_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    AggregateDataVariant v = std::get<AggregateDataVariant>(m_expression_list_processor->execute(exec_policy));

    v.setIsFlattenable(false);
    return v;
  }

  InnerListToVectorProcessor(ASTNode& node)
    : m_expression_list_processor{std::make_unique<ASTNodeExpressionListProcessor>(node)}
  {}

  ~InnerListToVectorProcessor() = default;
};

#endif   // INNER_LIST_TO_VECTOR_PROCESSOR_HPP
