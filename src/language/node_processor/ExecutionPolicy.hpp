#ifndef EXECUTION_POLICY_HPP
#define EXECUTION_POLICY_HPP

#include <language/utils/DataVariant.hpp>
#include <utils/PugsMacros.hpp>

#include <memory>
#include <vector>

class ExecutionPolicy
{
 public:
  enum class JumpType
  {
    no_jump,
    break_jump,
    continue_jump
  };

  class Context
  {
   public:
    using Values       = std::vector<DataVariant>;
    using SharedValues = std::shared_ptr<Values>;

   private:
    int32_t m_id;
    SharedValues m_shared_values;

   public:
    auto
    size() const
    {
      return m_shared_values->size();
    }

    DataVariant&
    operator[](size_t i)
    {
      return (*m_shared_values)[i];
    }

    const DataVariant&
    operator[](size_t i) const
    {
      return (*m_shared_values)[i];
    }

    const Values&
    values() const
    {
      return *m_shared_values;
    }

    int32_t
    id() const
    {
      return m_id;
    }

    Context(int32_t id, const SharedValues& shared_values) : m_id{id}, m_shared_values{shared_values} {}

    Context(const Context&) = default;
  };

 private:
  JumpType m_jump_type;
  bool m_exec;

  std::vector<Context> m_context_list;

 public:
  PUGS_INLINE
  bool
  exec() const
  {
    return m_exec;
  }

  PUGS_INLINE
  JumpType
  jumpType() const
  {
    return m_jump_type;
  }

  Context&
  contextOfId(int32_t context_id)
  {
    for (auto i_context = m_context_list.rbegin(); i_context != m_context_list.rend(); ++i_context) {
      if (i_context->id() == context_id) {
        return *i_context;
      }
    }
    throw std::invalid_argument{"unable to find context"};
  }

  Context&
  currentContext()
  {
    return m_context_list.back();
  }

  ExecutionPolicy& operator=(const ExecutionPolicy&) = delete;
  ExecutionPolicy& operator=(ExecutionPolicy&&) = default;

  explicit ExecutionPolicy(const ExecutionPolicy&) = default;
  ExecutionPolicy(ExecutionPolicy&&)               = default;

  ExecutionPolicy() : m_jump_type{JumpType::no_jump}, m_exec{true} {}

  ExecutionPolicy(const ExecutionPolicy& parent_policy, const Context& context) : ExecutionPolicy{parent_policy}
  {
    m_context_list.push_back(context);
  }

  ExecutionPolicy(const ExecutionPolicy& parent_policy, JumpType jump_type)
    : m_jump_type{jump_type}, m_exec{jump_type == JumpType::no_jump}, m_context_list{parent_policy.m_context_list}
  {
    ;
  }
};

#endif   // EXECUTION_POLICY_HPP
