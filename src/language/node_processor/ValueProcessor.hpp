#ifndef VALUE_PROCESSOR_HPP
#define VALUE_PROCESSOR_HPP

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <utils/EscapedString.hpp>

class ValueProcessor final : public INodeProcessor
{
 private:
  DataVariant m_value;

 public:
  Type
  type() const
  {
    return Type::value_processor;
  }

  PUGS_INLINE
  DataVariant
  execute(ExecutionPolicy&)
  {
    return m_value;
  }

  ValueProcessor(ASTNode& node)
  {
    if (node.is_type<language::real>()) {
      std::stringstream ss(node.string());
      double v;
      ss >> v;
      m_value = v;
    } else if (node.is_type<language::integer>()) {
      std::stringstream ss(node.string());
      int64_t v;
      ss >> v;
      m_value = v;
    } else if (node.is_type<language::literal>()) {
      m_value = unescapeString(node.string());
    } else if (node.is_type<language::for_test>()) {
      // if AST contains a for_test statement, it means that no test were
      // given to the for-loop, so its value is always true
      m_value = true;
    } else if (node.is_type<language::true_kw>()) {
      m_value = true;
    } else if (node.is_type<language::false_kw>()) {
      m_value = false;
    }
  }
};

#endif   // VALUE_PROCESSOR_HPP
