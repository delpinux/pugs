#ifndef TINY_VECTOR_EXPRESSION_PROCESSOR_HPP
#define TINY_VECTOR_EXPRESSION_PROCESSOR_HPP

#include <algebra/TinyVector.hpp>
#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>
#include <language/node_processor/INodeProcessor.hpp>

template <size_t Dimension>
class TinyVectorExpressionProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_node;

 public:
  Type
  type() const
  {
    return Type::tiny_vector_expression_processor;
  }

  PUGS_INLINE
  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    TinyVector<Dimension> v{};
    Assert(m_node.children.size() == Dimension);

    for (size_t i = 0; i < Dimension; ++i) {
      std::visit(
        [&](auto&& x) {
          using ValueT = std::decay_t<decltype(x)>;
          if constexpr (std::is_arithmetic_v<ValueT>) {
            v[i] = x;
          } else {
            // LCOV_EXCL_START
            Assert(false, "unexpected value type");
            // LCOV_EXCL_STOP
          }
        },
        m_node.children[i]->execute(exec_policy));
    }

    return v;
  }

  TinyVectorExpressionProcessor(ASTNode& node) : m_node{node} {}
};

#endif   // TINY_VECTOR_EXPRESSION_PROCESSOR_HPP
