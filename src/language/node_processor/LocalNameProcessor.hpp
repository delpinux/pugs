#ifndef LOCAL_NODE_PROCESSOR_HPP
#define LOCAL_NODE_PROCESSOR_HPP

#include <language/ast/ASTNode.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <language/utils/SymbolTable.hpp>

class LocalNameProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_node;
  uint64_t m_value_id;
  int32_t m_context_id;

 public:
  Type
  type() const
  {
    return Type::local_name_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    return exec_policy.contextOfId(m_context_id)[m_value_id];
  }

  LocalNameProcessor(ASTNode& node) : m_node{node}
  {
    const std::string& symbol = m_node.string();
    auto [i_symbol, found]    = m_node.m_symbol_table->find(symbol, m_node.begin());
    Assert(found);
    m_value_id   = std::get<uint64_t>(i_symbol->attributes().value());
    m_context_id = i_symbol->attributes().contextId();
  }
};

#endif   // LOCAL_NODE_PROCESSOR_HPP
