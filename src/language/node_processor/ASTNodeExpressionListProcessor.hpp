#ifndef AST_NODE_EXPRESSION_LIST_PROCESSOR_HPP
#define AST_NODE_EXPRESSION_LIST_PROCESSOR_HPP

#include <language/PEGGrammar.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <language/utils/ParseError.hpp>
#include <language/utils/SymbolTable.hpp>

#include <vector>

class ASTNodeExpressionListProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_node;
  const size_t m_number_of_values;

  void
  _flattenResults(DataVariant&& value, std::vector<DataVariant>& list_values)
  {
    std::visit(
      [&](auto&& v) {
        using ValueT = std::decay_t<decltype(v)>;
        if constexpr (std::is_same_v<ValueT, AggregateDataVariant>) {
          if (v.isFlattenable()) {
            for (size_t i = 0; i < v.size(); ++i) {
              _flattenResults(std::move(v[i]), list_values);
            }
          } else {
            list_values.emplace_back(v);
          }
        } else {
          list_values.emplace_back(v);
        }
      },
      value);
  }

  size_t
  _getNumberOfValues() const
  {
    size_t number_of_values = 0;

    for (auto& child : m_node.children) {
      if (child->is_type<language::function_evaluation>()) {
        if (child->m_data_type != ASTNodeDataType::list_t) {
          ++number_of_values;
        } else {
          ASTNode& function_name_node = *child->children[0];

          auto [i_function_symbol, found] = m_node.m_symbol_table->find(function_name_node.string(), m_node.begin());
          Assert(found);

          switch (i_function_symbol->attributes().dataType()) {
          case ASTNodeDataType::function_t: {
            uint64_t function_id = std::get<uint64_t>(i_function_symbol->attributes().value());

            FunctionDescriptor& function_descriptor = m_node.m_symbol_table->functionTable()[function_id];

            ASTNode& function_image_domain = *function_descriptor.domainMappingNode().children[1];
            number_of_values += function_image_domain.children.size();
            break;
          }
            //    LCOV_EXCL_START
          default: {
            throw ParseError("unexpected function type", m_node.begin());
          }
            //    LCOV_EXCL_STOP
          }
        }
      } else {
        ++number_of_values;
      }
    }

    return number_of_values;
  }

 public:
  Type
  type() const
  {
    return Type::expression_list_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    std::vector<DataVariant> list_values;
    list_values.reserve(m_number_of_values);

    for (auto& child : m_node.children) {
      _flattenResults(child->execute(exec_policy), list_values);
    }

    Assert(list_values.size() == m_number_of_values);
    return AggregateDataVariant{std::move(list_values)};
  }

  ASTNodeExpressionListProcessor(ASTNode& node) : m_node{node}, m_number_of_values{this->_getNumberOfValues()} {}
};

#endif   // AST_NODE_EXPRESSION_LIST_PROCESSOR_HPP
