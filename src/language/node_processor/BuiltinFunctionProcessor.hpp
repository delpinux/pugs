#ifndef BUILTIN_FUNCTION_PROCESSOR_HPP
#define BUILTIN_FUNCTION_PROCESSOR_HPP

#include <language/PEGGrammar.hpp>
#include <language/node_processor/FunctionArgumentConverter.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/ParseError.hpp>

class BuiltinFunctionExpressionProcessor final : public INodeProcessor
{
 private:
  std::shared_ptr<IBuiltinFunctionEmbedder> m_embedded;

 public:
  Type
  type() const
  {
    return Type::builtin_function_expression_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    return m_embedded->apply(exec_policy.currentContext().values());
  }

  BuiltinFunctionExpressionProcessor(std::shared_ptr<IBuiltinFunctionEmbedder> embedded) : m_embedded(embedded) {}
};

class BuiltinFunctionProcessor : public INodeProcessor
{
 private:
  ASTNode& m_argument_node;

  std::unique_ptr<INodeProcessor> m_function_expression_processor;

  std::vector<std::unique_ptr<IFunctionArgumentConverter>> m_argument_converters;

 public:
  void
  addArgumentConverter(std::unique_ptr<IFunctionArgumentConverter>&& argument_converter)
  {
    m_argument_converters.emplace_back(std::move(argument_converter));
  }

  void
  setFunctionExpressionProcessor(std::unique_ptr<INodeProcessor>&& function_processor)
  {
    m_function_expression_processor = std::move(function_processor);
  }

  Type
  type() const
  {
    return Type::builtin_function_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    ExecutionPolicy context_exec_policy{exec_policy,
                                        ExecutionPolicy::Context{-1, std::make_shared<ExecutionPolicy::Context::Values>(
                                                                       m_argument_converters.size())}};

    if (m_argument_converters.size() == 1) {
      try {
        m_argument_converters[0]->convert(context_exec_policy, m_argument_node.execute(context_exec_policy));
      }
      catch (std::domain_error& e) {
        throw ParseError(e.what(), m_argument_node.begin());
      }
    } else {
      AggregateDataVariant argument_values{
        std::get<AggregateDataVariant>(m_argument_node.execute(context_exec_policy))};

      for (size_t i = 0; i < m_argument_converters.size(); ++i) {
        try {
          m_argument_converters[i]->convert(context_exec_policy, std::move(argument_values[i]));
        }
        catch (std::domain_error& e) {
          throw ParseError(e.what(), m_argument_node.children[i]->begin());
        }
      }
    }

    return m_function_expression_processor->execute(context_exec_policy);
  }

  BuiltinFunctionProcessor(ASTNode& argument_node) : m_argument_node{argument_node} {}
};

#endif   // BUILTIN_FUNCTION_PROCESSOR_HPP
