#ifndef FAKE_PROCESSOR_HPP
#define FAKE_PROCESSOR_HPP

#include <language/node_processor/INodeProcessor.hpp>

class FakeProcessor final : public INodeProcessor
{
 public:
  Type
  type() const
  {
    return Type::fake_processor;
  }

  DataVariant
  execute(ExecutionPolicy&)
  {
    return {};
  }

  FakeProcessor() = default;
};

#endif   // FAKE_PROCESSOR_HPP
