#ifndef INC_DEC_EXPRESSION_PROCESSOR_HPP
#define INC_DEC_EXPRESSION_PROCESSOR_HPP

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <language/utils/SymbolTable.hpp>

template <typename Op>
struct IncDecOp;

template <>
struct IncDecOp<language::unary_minusminus>
{
  template <typename A>
  PUGS_INLINE A
  eval(A& a)
  {
    if constexpr (std::is_same_v<A, uint64_t>) {
      if (a == 0) {
        throw std::domain_error("decrement would produce negative value");
      }
    }
    return --a;
  }
};

template <>
struct IncDecOp<language::unary_plusplus>
{
  template <typename A>
  PUGS_INLINE A
  eval(A& a)
  {
    return ++a;
  }
};

template <>
struct IncDecOp<language::post_minusminus>
{
  template <typename A>
  PUGS_INLINE A
  eval(A& a)
  {
    if constexpr (std::is_same_v<A, uint64_t>) {
      if (a == 0) {
        throw std::domain_error("decrement would produce negative value");
      }
    }
    return a--;
  }
};

template <>
struct IncDecOp<language::post_plusplus>
{
  template <typename A>
  PUGS_INLINE A
  eval(A& a)
  {
    return a++;
  }
};

template <typename IncDecOpT, typename DataT>
class IncDecExpressionProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_node;
  DataVariant* p_value{nullptr};

 public:
  Type
  type() const
  {
    return Type::inc_dec_expression_processor;
  }

  DataVariant
  execute(ExecutionPolicy&)
  {
    try {
      return IncDecOp<IncDecOpT>().eval(std::get<DataT>(*p_value));
    }
    catch (std::domain_error& e) {
      throw ParseError(e.what(), m_node.children[0]->begin());
    }
  }

  IncDecExpressionProcessor(ASTNode& node) : m_node{node}
  {
    Assert(m_node.children[0]->is_type<language::name>());
    // It is sure at this point that children 0 is a variable name
    const std::string& symbol = m_node.children[0]->string();
    auto [i_symbol, found]    = m_node.m_symbol_table->find(symbol, m_node.children[0]->begin());
    Assert(found);
    p_value = &i_symbol->attributes().value();
  }
};

#endif   // INC_DEC_EXPRESSION_PROCESSOR_HPP
