#ifndef CONCAT_EXPRESSION_PROCESSOR_HPP
#define CONCAT_EXPRESSION_PROCESSOR_HPP

#include <language/ast/ASTNode.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <utils/Stringify.hpp>

template <typename A_DataT, typename B_DataT>
class ConcatExpressionProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_node;

  PUGS_INLINE
  DataVariant
  _eval(const std::string& a, const DataVariant& b)
  {
    if constexpr (std::is_same_v<B_DataT, std::string>) {
      return a + std::get<B_DataT>(b);
    } else if constexpr ((std::is_arithmetic_v<B_DataT>)and(not std::is_same_v<B_DataT, bool>)) {
      return a + stringify(std::get<B_DataT>(b));
    } else {
      std::ostringstream os;
      os << a << std::boolalpha << b;
      return os.str();
    }
  }

  PUGS_INLINE
  DataVariant
  _eval(const DataVariant& a, const std::string& b)
  {
    static_assert(not std::is_same_v<A_DataT, std::string>, "this case is treated by the other eval function");
    if constexpr ((std::is_arithmetic_v<A_DataT>)and(not std::is_same_v<A_DataT, bool>)) {
      return stringify(std::get<A_DataT>(a)) + b;
    } else {
      std::ostringstream os;
      os << std::boolalpha << a << b;
      return os.str();
    }
  }

 public:
  Type
  type() const
  {
    return Type::concat_expression_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    if constexpr (std::is_same_v<std::string, A_DataT>) {
      return this->_eval(std::get<std::string>(m_node.children[0]->execute(exec_policy)),
                         m_node.children[1]->execute(exec_policy));
    } else {
      return this->_eval(m_node.children[0]->execute(exec_policy),
                         std::get<std::string>(m_node.children[1]->execute(exec_policy)));
    }
  }

  ConcatExpressionProcessor(ASTNode& node) : m_node{node} {}
};

#endif   // CONCAT_EXPRESSION_PROCESSOR_HPP
