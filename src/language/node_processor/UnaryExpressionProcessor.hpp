#ifndef UNARY_EXPRESSION_PROCESSOR_HPP
#define UNARY_EXPRESSION_PROCESSOR_HPP

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>
#include <language/node_processor/INodeProcessor.hpp>

template <typename DataType>
class DataHandler;

template <typename Op>
struct UnaryOp;

template <>
struct UnaryOp<language::unary_minus>
{
  template <typename A>
  PUGS_INLINE A
  eval(const A& a)
  {
    return -a;
  }
};

template <>
struct UnaryOp<language::unary_not>
{
  template <typename A>
  PUGS_INLINE bool
  eval(const A& a)
  {
    return not a;
  }
};

template <typename UnaryOpT, typename ValueT, typename DataT>
class UnaryExpressionProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_node;

  PUGS_INLINE ValueT
  _eval(const DataVariant& a)
  {
    return UnaryOp<UnaryOpT>().eval(static_cast<ValueT>(std::get<DataT>(a)));
  }

 public:
  Type
  type() const
  {
    return Type::unary_expression_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    return this->_eval(m_node.children[0]->execute(exec_policy));
  }

  UnaryExpressionProcessor(ASTNode& node) : m_node{node} {}
};

template <typename UnaryOpT, typename ValueT, typename DataT>
class UnaryExpressionProcessor<UnaryOpT, std::shared_ptr<ValueT>, std::shared_ptr<DataT>> final : public INodeProcessor
{
 private:
  ASTNode& m_node;

  PUGS_INLINE DataVariant
  _eval(const DataVariant& a)
  {
    const auto& embedded_a = std::get<EmbeddedData>(a);

    std::shared_ptr a_ptr = dynamic_cast<const DataHandler<DataT>&>(embedded_a.get()).data_ptr();

    return EmbeddedData(std::make_shared<DataHandler<ValueT>>(UnaryOp<UnaryOpT>().eval(a_ptr)));
  }

 public:
  Type
  type() const
  {
    return Type::unary_expression_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    return this->_eval(m_node.children[0]->execute(exec_policy));
  }

  UnaryExpressionProcessor(ASTNode& node) : m_node{node} {}
};

#endif   // UNARY_EXPRESSION_PROCESSOR_HPP
