#ifndef FUNCTION_PROCESSOR_HPP
#define FUNCTION_PROCESSOR_HPP

#include <language/PEGGrammar.hpp>
#include <language/node_processor/ASTNodeExpressionListProcessor.hpp>
#include <language/node_processor/FunctionArgumentConverter.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <language/utils/FunctionTable.hpp>
#include <language/utils/SymbolTable.hpp>
#include <utils/Stringify.hpp>

template <typename ReturnType, typename ExpressionValueType>
class FunctionExpressionProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_function_expression;

 public:
  Type
  type() const
  {
    return Type::function_expression_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    if constexpr (std::is_same_v<ReturnType, ExpressionValueType>) {
      return m_function_expression.execute(exec_policy);
    } else if constexpr (std::is_same_v<ReturnType, std::string>) {
      return stringify(std::get<ExpressionValueType>(m_function_expression.execute(exec_policy)));
    } else if constexpr (std::is_same_v<ExpressionValueType, ZeroType>) {
      return ReturnType{ZeroType::zero};
    } else if constexpr (std::is_convertible_v<ExpressionValueType, ReturnType>) {
      auto expression_value        = m_function_expression.execute(exec_policy);
      const ExpressionValueType& v = std::get<ExpressionValueType>(expression_value);
      if constexpr (std::is_same_v<ReturnType, uint64_t> and std::is_same_v<ExpressionValueType, int64_t>) {
        if (v < 0) {
          throw std::domain_error("trying to convert negative value (" + stringify(v) + ")");
        }
      }
      return static_cast<ReturnType>(v);
    } else if constexpr (std::is_arithmetic_v<ExpressionValueType> and
                         (is_tiny_vector_v<ReturnType> or is_tiny_matrix_v<ReturnType>)) {
      static_assert(ReturnType::Dimension == 1, "invalid conversion");
      return ReturnType(std::get<ExpressionValueType>(m_function_expression.execute(exec_policy)));
    } else if constexpr (is_std_vector_v<ReturnType>) {
      auto expression_value = m_function_expression.execute(exec_policy);

      const ExpressionValueType& v = std::get<ExpressionValueType>(expression_value);

      using TupleContentType = std::decay_t<typename ReturnType::value_type>;

      if constexpr (std::is_convertible_v<ExpressionValueType, TupleContentType>) {
        if constexpr (std::is_same_v<TupleContentType, uint64_t> and std::is_same_v<ExpressionValueType, int64_t>) {
          if (v < 0) {
            throw std::domain_error("trying to convert negative value (" + stringify(v) + ")");
          }
        }
        return ReturnType{static_cast<TupleContentType>(v)};
      } else if constexpr (is_tiny_matrix_v<TupleContentType>) {
        static_assert(ReturnType::value_type::NumberOfRows == ReturnType::value_type::NumberOfColumns);
        if constexpr (ReturnType::value_type::NumberOfRows == 1) {
          if constexpr (std::is_arithmetic_v<ExpressionValueType>) {
            return ReturnType{TupleContentType(v)};
          } else if constexpr (std::is_same_v<ExpressionValueType, AggregateDataVariant>) {
            ReturnType result(v.size());
            for (size_t i = 0; i < result.size(); ++i) {
              std::visit(
                [&](auto&& v_i) {
                  using ListElementType = std::decay_t<decltype(v_i)>;
                  if constexpr ((std::is_same_v<ListElementType, TupleContentType>) or
                                ((std::is_arithmetic_v<ListElementType>))) {
                    result[i] = static_cast<TupleContentType>(v_i);
                  } else {
                    // LCOV_EXCL_START
                    throw UnexpectedError("invalid list element type");
                    // LCOV_EXCL_STOP
                  }
                },
                v[i]);
            }
            return result;
          } else {
            static_assert(is_std_vector_v<ExpressionValueType>);
            ReturnType result(v.size());
            for (size_t i = 0; i < v.size(); ++i) {
              result[i] = static_cast<TupleContentType>(v[i]);
            }
            return result;
          }
        } else {
          if constexpr (std::is_same_v<ExpressionValueType, int64_t>) {
            return ReturnType{TupleContentType(ZeroType::zero)};
          } else if constexpr (std::is_same_v<ExpressionValueType, AggregateDataVariant>) {
            ReturnType result(v.size());
            for (size_t i = 0; i < result.size(); ++i) {
              std::visit(
                [&](auto&& v_i) {
                  using ListElementType = std::decay_t<decltype(v_i)>;
                  if constexpr (std::is_same_v<ListElementType, TupleContentType>) {
                    result[i] = v_i;
                  } else if constexpr (std::is_same_v<ListElementType, int64_t>) {
                    result[i] = ZeroType::zero;
                  } else {
                    // LCOV_EXCL_START
                    throw UnexpectedError("invalid list element type");
                    // LCOV_EXCL_STOP
                  }
                },
                v[i]);
            }
            return result;
          } else if constexpr (is_std_vector_v<ExpressionValueType>) {
            ReturnType result(v.size());
            for (size_t i = 0; i < v.size(); ++i) {
              if constexpr (std::is_same_v<typename ExpressionValueType::value_type, TupleContentType>) {
                result[i] = v[i];
              } else if constexpr (std::is_same_v<typename ExpressionValueType::value_type, int64_t>) {
                result[i] = ZeroType::zero;
              } else {
                // LCOV_EXCL_START
                throw UnexpectedError("invalid list element type");
                // LCOV_EXCL_STOP
              }
            }
            return result;
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("invalid expression value");
            // LCOV_EXCL_STOP
          }
        }
      } else if constexpr (is_tiny_vector_v<TupleContentType>) {
        if constexpr (ReturnType::value_type::Dimension == 1) {
          if constexpr (std::is_arithmetic_v<ExpressionValueType>) {
            return ReturnType{TupleContentType(v)};
          } else if constexpr (std::is_same_v<ExpressionValueType, AggregateDataVariant>) {
            ReturnType result(v.size());
            for (size_t i = 0; i < result.size(); ++i) {
              std::visit(
                [&](auto&& v_i) {
                  using ListElementType = std::decay_t<decltype(v_i)>;
                  if constexpr ((std::is_same_v<ListElementType, TupleContentType>) or
                                ((std::is_arithmetic_v<ListElementType>))) {
                    result[i] = static_cast<TupleContentType>(v_i);
                  } else {
                    // LCOV_EXCL_START
                    throw UnexpectedError("invalid list element type");
                    // LCOV_EXCL_STOP
                  }
                },
                v[i]);
            }
            return result;
          } else {
            static_assert(is_std_vector_v<ExpressionValueType>);
            ReturnType result(v.size());
            for (size_t i = 0; i < v.size(); ++i) {
              result[i] = static_cast<TupleContentType>(v[i]);
            }
            return result;
          }
        } else {
          if constexpr (std::is_same_v<ExpressionValueType, int64_t>) {
            return ReturnType{TupleContentType(ZeroType::zero)};
          } else if constexpr (std::is_same_v<ExpressionValueType, AggregateDataVariant>) {
            ReturnType result(v.size());
            for (size_t i = 0; i < result.size(); ++i) {
              std::visit(
                [&](auto&& v_i) {
                  using ListElementType = std::decay_t<decltype(v_i)>;
                  if constexpr (std::is_same_v<ListElementType, TupleContentType>) {
                    result[i] = v_i;
                  } else if constexpr (std::is_same_v<ListElementType, int64_t>) {
                    result[i] = ZeroType::zero;
                  } else {
                    // LCOV_EXCL_START
                    throw UnexpectedError("invalid list element type");
                    // LCOV_EXCL_STOP
                  }
                },
                v[i]);
            }
            return result;
          } else if constexpr (is_std_vector_v<ExpressionValueType>) {
            ReturnType result(v.size());
            for (size_t i = 0; i < v.size(); ++i) {
              if constexpr (std::is_same_v<typename ExpressionValueType::value_type, TupleContentType>) {
                result[i] = v[i];
              } else if constexpr (std::is_same_v<typename ExpressionValueType::value_type, int64_t>) {
                result[i] = ZeroType::zero;
              } else {
                // LCOV_EXCL_START
                throw UnexpectedError("invalid tuple element type");
                // LCOV_EXCL_STOP
              }
            }
            return result;
          }
        }
        // LCOV_EXCL_START
        throw UnexpectedError("invalid expression type");
        // LCOV_EXCL_STOP
      } else if constexpr (std::is_same_v<TupleContentType, std::string>) {
        return ReturnType{stringify(v)};
      } else if constexpr (is_std_vector_v<ExpressionValueType>) {
        using ExpressionContentType = typename ExpressionValueType::value_type;
        if constexpr (std::is_convertible_v<ExpressionContentType, TupleContentType>) {
          ReturnType result(v.size());
          for (size_t i = 0; i < v.size(); ++i) {
            if constexpr (std::is_same_v<TupleContentType, uint64_t> and
                          std::is_same_v<ExpressionContentType, int64_t>) {
              if (v[i] < 0) {
                throw std::domain_error("trying to convert negative value (" + stringify(v[i]) + ")");
              }
            }
            result[i] = v[i];
          }
          return result;
        } else if constexpr (std::is_same_v<TupleContentType, std::string>) {
          ReturnType result(v.size());
          for (size_t i = 0; i < v.size(); ++i) {
            result[i] = stringify(v[i]);
          }
          return result;
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("invalid tuple type");
          // LCOV_EXCL_STOP
        }
      } else if constexpr (std::is_same_v<AggregateDataVariant, ExpressionValueType>) {
        ReturnType result(v.size());
        for (size_t i = 0; i < result.size(); ++i) {
          std::visit(
            [&](auto&& v_i) {
              using ListElementType = std::decay_t<decltype(v_i)>;
              if constexpr (std::is_convertible_v<ListElementType, TupleContentType>) {
                if constexpr (std::is_same_v<TupleContentType, uint64_t> and std::is_same_v<ListElementType, int64_t>) {
                  if (v_i < 0) {
                    throw std::domain_error("trying to convert negative value (" + stringify(v_i) + ")");
                  }
                }
                result[i] = static_cast<TupleContentType>(v_i);
              } else if constexpr (std::is_same_v<TupleContentType, std::string>) {
                if constexpr (std::is_same_v<ListElementType, std::string>) {
                  result[i] = std::move(v_i);
                } else {
                  result[i] = stringify(v_i);
                }
              } else {
                // LCOV_EXCL_START
                throw UnexpectedError("invalid list element type");
                // LCOV_EXCL_STOP
              }
            },
            v[i]);
        }
        return result;
      } else {
        // LCOV_EXCL_START
        throw UnexpectedError("invalid tuple type");
        // LCOV_EXCL_STOP
      }
    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("invalid conversion");
      // LCOV_EXCL_STOP
    }
  }

  FunctionExpressionProcessor(ASTNode& function_component_expression)
    : m_function_expression{function_component_expression}
  {}
};

class FunctionProcessor : public INodeProcessor
{
 private:
  ASTNode& m_argument_node;

  const size_t m_context_size;
  const int32_t m_context_id;

  std::vector<std::unique_ptr<IFunctionArgumentConverter>> m_argument_converters;
  std::vector<std::unique_ptr<INodeProcessor>> m_function_expression_processors;

 public:
  void
  addArgumentConverter(std::unique_ptr<IFunctionArgumentConverter>&& argument_converter)
  {
    m_argument_converters.emplace_back(std::move(argument_converter));
  }

  void
  addFunctionExpressionProcessor(std::unique_ptr<INodeProcessor>&& function_processor)
  {
    m_function_expression_processors.emplace_back(std::move(function_processor));
  }

  Type
  type() const
  {
    return Type::function_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    // Context is built in each execution for thread safety: multiple thread can call a function at once
    ExecutionPolicy::Context context{m_context_id, std::make_shared<ExecutionPolicy::Context::Values>(m_context_size)};

    ExecutionPolicy context_exec_policy{exec_policy, context};

    if (m_argument_converters.size() == 1) {
      try {
        m_argument_converters[0]->convert(context_exec_policy, m_argument_node.execute(context_exec_policy));
      }
      catch (std::domain_error& e) {
        throw ParseError(e.what(), m_argument_node.begin());
      }
    } else {
      AggregateDataVariant argument_values{
        std::get<AggregateDataVariant>(m_argument_node.execute(context_exec_policy))};

      for (size_t i = 0; i < m_argument_converters.size(); ++i) {
        try {
          m_argument_converters[i]->convert(context_exec_policy, std::move(argument_values[i]));
        }
        catch (std::domain_error& e) {
          throw ParseError(e.what(), m_argument_node.children[i]->begin());
        }
      }
    }

    try {
      if (m_function_expression_processors.size() == 1) {
        return m_function_expression_processors[0]->execute(context_exec_policy);
      } else {
        std::vector<DataVariant> list_values;
        list_values.reserve(m_function_expression_processors.size());

        for (auto& function_expression_processor : m_function_expression_processors) {
          list_values.emplace_back(function_expression_processor->execute(context_exec_policy));
        }
        return AggregateDataVariant{std::move(list_values)};
      }
    }
    catch (std::domain_error& e) {
      throw ParseError(e.what(), m_argument_node.begin());
    }
  }

  FunctionProcessor(ASTNode& argument_node, SymbolTable::Context context)
    : m_argument_node{argument_node}, m_context_size{context.size()}, m_context_id{context.id()}
  {}
};

#endif   // FUNCTION_PROCESSOR_HPP
