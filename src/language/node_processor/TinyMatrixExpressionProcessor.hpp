#ifndef TINY_MATRIX_EXPRESSION_PROCESSOR_HPP
#define TINY_MATRIX_EXPRESSION_PROCESSOR_HPP

#include <algebra/TinyMatrix.hpp>
#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>
#include <language/node_processor/INodeProcessor.hpp>

template <size_t M, size_t N>
class TinyMatrixExpressionProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_node;

 public:
  Type
  type() const
  {
    return Type::tiny_matrix_expression_processor;
  }

  PUGS_INLINE
  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    TinyMatrix<M, N> A{};
    Assert(m_node.children.size() == M);

    for (size_t i = 0; i < M; ++i) {
      for (size_t j = 0; j < N; ++j) {
        std::visit(
          [&](auto&& x) {
            using ValueT = std::decay_t<decltype(x)>;
            if constexpr (std::is_arithmetic_v<ValueT>) {
              A(i, j) = x;
            } else {
              // LCOV_EXCL_START
              Assert(false, "unexpected value type");
              // LCOV_EXCL_STOP
            }
          },
          m_node.children[i]->children[j]->execute(exec_policy));
      }
    }

    return A;
  }

  TinyMatrixExpressionProcessor(ASTNode& node) : m_node{node} {}
};

#endif   // TINY_MATRIX_EXPRESSION_PROCESSOR_HPP
