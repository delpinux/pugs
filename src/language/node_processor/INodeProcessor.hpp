#ifndef I_NODE_PROCESSOR_HPP
#define I_NODE_PROCESSOR_HPP

#include <language/node_processor/ExecutionPolicy.hpp>
#include <language/utils/DataVariant.hpp>
#include <utils/Demangle.hpp>

#include <string>
#include <typeinfo>

class INodeProcessor
{
 public:
  enum class Type
  {
    affectation_processor,
    array_subscript_processor,
    binary_expression_processor,
    break_processor,
    builtin_function_expression_processor,
    builtin_function_processor,
    concat_expression_processor,
    continue_processor,
    do_while_processor,
    expression_list_processor,
    fake_processor,
    for_processor,
    function_expression_processor,
    function_processor,
    if_processor,
    inc_dec_expression_processor,
    inner_list_to_vector_processor,
    list_processor,
    local_name_processor,
    name_processor,
    tiny_matrix_expression_processor,
    tiny_vector_expression_processor,
    unary_expression_processor,
    value_processor,
    while_processor
  };

  virtual Type type() const = 0;

  virtual DataVariant execute(ExecutionPolicy& exec_policy) = 0;

  std::string
  typeIdName() const
  {
    return demangle(typeid(*this).name());
  }

  INodeProcessor(const INodeProcessor&) = delete;
  INodeProcessor(INodeProcessor&&)      = delete;

  INodeProcessor() = default;

  virtual ~INodeProcessor() = default;
};

#endif   // I_NODE_PROCESSOR_HPP
