#ifndef BREAK_PROCESSOR_HPP
#define BREAK_PROCESSOR_HPP

#include <language/node_processor/INodeProcessor.hpp>

class BreakProcessor final : public INodeProcessor
{
 public:
  Type
  type() const
  {
    return Type::break_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    exec_policy = ExecutionPolicy(exec_policy, ExecutionPolicy::JumpType::break_jump);
    return {};
  }

  BreakProcessor() = default;
};

#endif   // BREAK_PROCESSOR_HPP
