#ifndef AFFECTATION_PROCESSOR_HPP
#define AFFECTATION_PROCESSOR_HPP

#include <language/PEGGrammar.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <language/utils/ParseError.hpp>
#include <language/utils/SymbolTable.hpp>
#include <utils/Exceptions.hpp>
#include <utils/PugsTraits.hpp>
#include <utils/Stringify.hpp>

#include <exception>

template <typename Op>
struct AffOp;

template <>
struct AffOp<language::multiplyeq_op>
{
  template <typename A, typename B>
  PUGS_INLINE void
  eval(A& a, const B& b)
  {
    if constexpr (std::is_same_v<uint64_t, A> and std::is_same_v<int64_t, B>) {
      if (b < 0) {
        throw std::domain_error("trying to affect negative value (" + stringify(b) + ")");
      }
    }
    a *= b;
  }
};

template <>
struct AffOp<language::divideeq_op>
{
  template <typename A, typename B>
  PUGS_INLINE void
  eval(A& a, const B& b)
  {
    if constexpr (std::is_same_v<uint64_t, A> and std::is_same_v<int64_t, B>) {
      if (b < 0) {
        throw std::domain_error("trying to affect negative value (" + stringify(b) + ")");
      }
    }
    a /= b;
  }
};

template <>
struct AffOp<language::pluseq_op>
{
  template <typename A, typename B>
  PUGS_INLINE void
  eval(A& a, const B& b)
  {
    if constexpr (std::is_same_v<uint64_t, A> and std::is_same_v<int64_t, B>) {
      if (static_cast<int64_t>(a + b) < 0) {
        throw std::domain_error("trying to affect negative value (lhs: " + stringify(a) + " rhs: " + stringify(b) +
                                ")");
      }
    }
    a += b;
  }
};

template <>
struct AffOp<language::minuseq_op>
{
  template <typename A, typename B>
  PUGS_INLINE void
  eval(A& a, const B& b)
  {
    if constexpr (std::is_same_v<uint64_t, A> and std::is_same_v<int64_t, B>) {
      if (static_cast<int64_t>(a - b) < 0) {
        throw std::domain_error("trying to affect negative value (lhs: " + stringify(a) + " rhs: " + stringify(b) +
                                ")");
      }
    }
    a -= b;
  }
};

struct IAffectationExecutor
{
  virtual void affect(DataVariant&& rhs) = 0;

  IAffectationExecutor(const IAffectationExecutor&) = delete;
  IAffectationExecutor(IAffectationExecutor&&)      = delete;

  IAffectationExecutor() = default;

  virtual ~IAffectationExecutor() = default;
};

template <typename OperatorT, typename ValueT, typename DataT>
class AffectationExecutor final : public IAffectationExecutor
{
 private:
  DataVariant& m_lhs;
  ASTNode& m_node;

  static inline const bool m_is_defined{[] {
    if constexpr (std::is_same_v<std::decay_t<ValueT>, bool>) {
      return std::is_same_v<OperatorT, language::eq_op>;
    }
    return true;
  }()};

 public:
  AffectationExecutor(ASTNode& node, DataVariant& lhs) : m_lhs(lhs), m_node{node}
  {
    // LCOV_EXCL_START
    if constexpr (not m_is_defined) {
      throw UnexpectedError("invalid operands to affectation expression");
    }
    // LCOV_EXCL_STOP
  }

  PUGS_INLINE void
  affect(DataVariant&& rhs)
  {
    if constexpr (m_is_defined) {
      if constexpr (not std::is_same_v<DataT, ZeroType>) {
        if constexpr (std::is_same_v<ValueT, std::string>) {
          if constexpr (std::is_same_v<OperatorT, language::eq_op>) {
            if constexpr (std::is_same_v<std::string, DataT>) {
              m_lhs = std::get<DataT>(rhs);
            } else {
              m_lhs = std::move(stringify(std::get<DataT>(rhs)));
            }
          } else {
            ValueT& lhs = std::get<ValueT>(m_lhs);
            if constexpr (std::is_same_v<std::string, DataT>) {
              lhs += std::get<std::string>(rhs);
            } else {
              lhs += std::move(stringify(std::get<DataT>(rhs)));
            }
          }
        } else {
          if constexpr (std::is_same_v<OperatorT, language::eq_op>) {
            if constexpr (std::is_convertible_v<DataT, ValueT>) {
              const DataT& value = std::get<DataT>(rhs);
              if constexpr (std::is_same_v<uint64_t, ValueT> and std::is_same_v<int64_t, DataT>) {
                if (value < 0) {
                  throw std::domain_error("trying to affect negative value (" + stringify(value) + ")");
                }
              }
              m_lhs = static_cast<ValueT>(value);
            } else if constexpr (std::is_same_v<TinyVector<1>, ValueT>) {
              std::visit(
                [&](auto&& v) {
                  using Vi_T = std::decay_t<decltype(v)>;
                  if constexpr (std::is_convertible_v<Vi_T, double>) {
                    m_lhs = TinyVector<1>(v);
                  } else {
                    // LCOV_EXCL_START
                    throw UnexpectedError("unexpected rhs type in affectation");
                    // LCOV_EXCL_STOP
                  }
                },
                rhs);
            } else if constexpr (std::is_same_v<TinyMatrix<1>, ValueT>) {
              std::visit(
                [&](auto&& v) {
                  using Vi_T = std::decay_t<decltype(v)>;
                  if constexpr (std::is_convertible_v<Vi_T, double>) {
                    m_lhs = TinyMatrix<1>(v);
                  } else {
                    // LCOV_EXCL_START
                    throw UnexpectedError("unexpected rhs type in affectation");
                    // LCOV_EXCL_STOP
                  }
                },
                rhs);
            } else if constexpr (is_std_vector_v<ValueT> and is_std_vector_v<DataT>) {
              using ValueContentT = typename ValueT::value_type;
              using DataContentT  = typename DataT::value_type;

              static_assert(not std::is_same_v<ValueContentT, DataContentT>,
                            "the case should have been treated previously");
              if constexpr (std::is_convertible_v<DataContentT, ValueContentT>) {
                ValueT lhs(std::get<DataT>(rhs).size());
                std::visit(
                  [&](auto&& v) {
                    using Vi_T = std::decay_t<decltype(v)>;
                    if constexpr (is_std_vector_v<Vi_T>) {
                      if constexpr (std::is_arithmetic_v<typename Vi_T::value_type>) {
                        for (size_t i = 0; i < v.size(); ++i) {
                          const auto& vi = v[i];
                          if constexpr (std::is_same_v<ValueContentT, uint64_t> and
                                        std::is_same_v<DataContentT, int64_t>) {
                            if (vi < 0) {
                              throw std::domain_error("trying to affect negative value (" + stringify(v) + ")");
                            }
                          }
                          lhs[i] = vi;
                        }
                      } else {
                        // LCOV_EXCL_START
                        throw UnexpectedError("unexpected rhs type in affectation");
                        // LCOV_EXCL_STOP
                      }
                    } else {
                      // LCOV_EXCL_START
                      throw UnexpectedError("unexpected rhs type in affectation");
                      // LCOV_EXCL_STOP
                    }
                  },
                  rhs);
                m_lhs = lhs;

              } else if constexpr (std::is_same_v<ValueContentT, std::string>) {
                ValueT lhs(std::get<DataT>(rhs).size());

                std::visit(
                  [&](auto&& v) {
                    using V_T = std::decay_t<decltype(v)>;
                    if constexpr (is_std_vector_v<V_T>) {
                      for (size_t i = 0; i < v.size(); ++i) {
                        if constexpr (std::is_same_v<typename V_T::value_type, bool>) {
                          // Ugly workaround to allow compilation with libstdc++-9
                          bool v_i = v[i];
                          lhs[i]   = std::move(stringify(v_i));
                        } else {
                          lhs[i] = std::move(stringify(v[i]));
                        }
                      }
                    } else {
                      // LCOV_EXCL_START
                      throw UnexpectedError("unexpected rhs type in affectation");
                      // LCOV_EXCL_STOP
                    }
                  },
                  rhs);

                m_lhs = lhs;
              } else if constexpr (is_tiny_vector_v<ValueContentT>) {
                static_assert(not std::is_same_v<DataContentT, ValueContentT>, "should have been treated before");
                static_assert(ValueContentT::Dimension == 1,
                              "conversions are only allowed for dimension 1 TinyVector's");

                ValueT lhs(std::get<DataT>(rhs).size());

                std::visit(
                  [&](auto&& v) {
                    if constexpr (is_std_vector_v<std::decay_t<decltype(v)>>) {
                      using Vi_T = typename std::decay_t<decltype(v)>::value_type;
                      for (size_t i = 0; i < v.size(); ++i) {
                        if constexpr (std::is_arithmetic_v<Vi_T>) {
                          lhs[i][0] = v[i];
                        } else {
                          // LCOV_EXCL_START
                          throw UnexpectedError("unexpected rhs type in affectation");
                          // LCOV_EXCL_STOP
                        }
                      }
                    } else {
                      // LCOV_EXCL_START
                      throw UnexpectedError("unexpected rhs type in affectation");
                      // LCOV_EXCL_STOP
                    }
                  },
                  rhs);
                m_lhs = lhs;
              } else if constexpr (is_tiny_matrix_v<ValueContentT>) {
                static_assert(not std::is_same_v<DataContentT, ValueContentT>, "should have been treated before");
                static_assert(ValueContentT::Dimension == 1, "conversions are only allowed for 1x1 TinyMatrix's");

                ValueT lhs(std::get<DataT>(rhs).size());

                std::visit(
                  [&](auto&& v) {
                    if constexpr (is_std_vector_v<std::decay_t<decltype(v)>>) {
                      using Vi_T = typename std::decay_t<decltype(v)>::value_type;
                      for (size_t i = 0; i < v.size(); ++i) {
                        if constexpr (std::is_arithmetic_v<Vi_T>) {
                          lhs[i](0, 0) = v[i];
                        } else {
                          // LCOV_EXCL_START
                          throw UnexpectedError("unexpected rhs type in affectation");
                          // LCOV_EXCL_STOP
                        }
                      }
                    } else {
                      // LCOV_EXCL_START
                      throw UnexpectedError("unexpected rhs type in affectation");
                      // LCOV_EXCL_STOP
                    }
                  },
                  rhs);

                m_lhs = lhs;
              } else {
                // LCOV_EXCL_START
                throw UnexpectedError("invalid value type");
                // LCOV_EXCL_STOP
              }
            } else if constexpr (is_std_vector_v<ValueT> and std::is_same_v<DataT, AggregateDataVariant>) {
              using ValueContentT                         = typename ValueT::value_type;
              const AggregateDataVariant& children_values = std::get<AggregateDataVariant>(rhs);
              ValueT lhs(children_values.size());
              for (size_t i = 0; i < children_values.size(); ++i) {
                std::visit(
                  [&](auto&& child_value) {
                    using T = std::decay_t<decltype(child_value)>;
                    if constexpr (std::is_same_v<T, ValueContentT>) {
                      lhs[i] = child_value;
                    } else if constexpr (std::is_arithmetic_v<ValueContentT> and
                                         std::is_convertible_v<T, ValueContentT>) {
                      if constexpr (std::is_same_v<ValueContentT, uint64_t> and std::is_same_v<T, int64_t>) {
                        if (child_value < 0) {
                          throw std::domain_error("trying to affect negative value (" + stringify(child_value) + ")");
                        }
                      }
                      lhs[i] = static_cast<ValueContentT>(child_value);
                    } else if constexpr (std::is_same_v<std::string, ValueContentT>) {
                      lhs[i] = std::move(stringify(child_value));
                    } else if constexpr (is_tiny_vector_v<ValueContentT>) {
                      if constexpr (std::is_arithmetic_v<T>) {
                        if constexpr (std::is_same_v<ValueContentT, TinyVector<1>>) {
                          lhs[i][0] = child_value;
                        } else {
                          // in this case a 0 is given
                          Assert(child_value == 0);
                          lhs[i] = ZeroType{};
                        }
                      } else {
                        // LCOV_EXCL_START
                        throw UnexpectedError("unexpected error: unexpected right hand side type in affectation");
                        // LCOV_EXCL_STOP
                      }
                    } else if constexpr (is_tiny_matrix_v<ValueContentT>) {
                      if constexpr (std::is_arithmetic_v<T>) {
                        if constexpr (std::is_same_v<ValueContentT, TinyMatrix<1>>) {
                          lhs[i](0, 0) = child_value;
                        } else {
                          // in this case a 0 is given
                          Assert(child_value == 0);
                          lhs[i] = ZeroType{};
                        }
                      } else {
                        // LCOV_EXCL_START
                        throw UnexpectedError("unexpected error: unexpected right hand side type in affectation");
                        // LCOV_EXCL_STOP
                      }
                    } else {
                      // LCOV_EXCL_START
                      throw UnexpectedError("unexpected error: unexpected right hand side type in affectation");
                      // LCOV_EXCL_STOP
                    }
                  },
                  children_values[i]);

                m_lhs = lhs;
              }
            } else if constexpr (is_std_vector_v<ValueT>) {
              using ValueContentT = typename ValueT::value_type;
              ValueT lhs(1);
              std::visit(
                [&](auto&& child_value) {
                  using T = std::decay_t<decltype(child_value)>;
                  if constexpr (std::is_same_v<T, ValueContentT>) {
                    lhs[0] = child_value;
                  } else if constexpr (std::is_arithmetic_v<ValueContentT> and
                                       std::is_convertible_v<T, ValueContentT>) {
                    if constexpr (std::is_same_v<ValueContentT, uint64_t> and std::is_same_v<T, int64_t>) {
                      if (child_value < 0) {
                        throw std::domain_error("trying to affect negative value (" + stringify(child_value) + ")");
                      }
                    }
                    lhs[0] = static_cast<ValueContentT>(child_value);
                  } else if constexpr (std::is_same_v<std::string, ValueContentT>) {
                    lhs[0] = std::move(stringify(child_value));
                  } else if constexpr (is_tiny_vector_v<ValueContentT>) {
                    if constexpr (std::is_arithmetic_v<T>) {
                      if constexpr (std::is_same_v<ValueContentT, TinyVector<1>>) {
                        lhs[0][0] = child_value;
                      } else {
                        // in this case a 0 is given
                        Assert(child_value == 0);
                        lhs[0] = ZeroType{};
                      }
                    } else {
                      // LCOV_EXCL_START
                      throw UnexpectedError("unexpected error: unexpected right hand side type in affectation");
                      // LCOV_EXCL_STOP
                    }
                  } else if constexpr (is_tiny_matrix_v<ValueContentT>) {
                    if constexpr (std::is_arithmetic_v<T>) {
                      if constexpr (std::is_same_v<ValueContentT, TinyMatrix<1>>) {
                        lhs[0](0, 0) = child_value;
                      } else {
                        // in this case a 0 is given
                        Assert(child_value == 0);
                        lhs[0] = ZeroType{};
                      }
                    } else {
                      // LCOV_EXCL_START
                      throw UnexpectedError("unexpected error: unexpected right hand side type in affectation");
                      // LCOV_EXCL_STOP
                    }
                  } else {
                    // LCOV_EXCL_START
                    throw UnexpectedError("unexpected error: unexpected right hand side type in affectation");
                    // LCOV_EXCL_STOP
                  }
                },
                rhs);

              m_lhs = lhs;
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("invalid value type");
              // LCOV_EXCL_STOP
            }
          } else {
            AffOp<OperatorT>().eval(std::get<ValueT>(m_lhs), std::get<DataT>(rhs));
          }
        }
      } else if (std::is_same_v<OperatorT, language::eq_op>) {
        m_lhs = ValueT{zero};
      } else {
        static_assert(std::is_same_v<OperatorT, language::eq_op>, "unexpected operator type");
      }
    }
  }
};

template <typename OperatorT, typename ValueT, typename DataT>
class AffectationProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_rhs_node;

  std::unique_ptr<IAffectationExecutor> m_affectation_executor;

  std::unique_ptr<IAffectationExecutor>
  _buildAffectationExecutor(ASTNode& lhs_node)
  {
    if (lhs_node.is_type<language::name>()) {
      const std::string& symbol = lhs_node.string();
      auto [i_symbol, found]    = lhs_node.m_symbol_table->find(symbol, lhs_node.begin());
      Assert(found);
      DataVariant& value = i_symbol->attributes().value();

      using AffectationExecutorT = AffectationExecutor<OperatorT, ValueT, DataT>;
      return std::make_unique<AffectationExecutorT>(lhs_node, value);
    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("invalid lhs");
      // LCOV_EXCL_STOP
    }
  }

 public:
  Type
  type() const
  {
    return Type::affectation_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    try {
      m_affectation_executor->affect(m_rhs_node.execute(exec_policy));
    }
    catch (std::domain_error& e) {
      throw ParseError(e.what(), m_rhs_node.begin());
    }
    return {};
  }

  AffectationProcessor(ASTNode& lhs_node, ASTNode& rhs_node)
    : m_rhs_node{rhs_node}, m_affectation_executor{this->_buildAffectationExecutor(lhs_node)}
  {}
};

class AffectationToDataVariantProcessorBase : public INodeProcessor
{
 protected:
  DataVariant* m_lhs;

 public:
  AffectationToDataVariantProcessorBase(ASTNode& lhs_node)
  {
    const std::string& symbol = lhs_node.string();
    auto [i_symbol, found]    = lhs_node.m_symbol_table->find(symbol, lhs_node.begin());
    Assert(found);

    m_lhs = &i_symbol->attributes().value();
  }

  virtual ~AffectationToDataVariantProcessorBase() = default;
};

template <typename ValueT>
class AffectationToTupleProcessor final : public AffectationToDataVariantProcessorBase
{
 private:
  ASTNode& m_rhs_node;

 public:
  Type
  type() const
  {
    return Type::affectation_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    DataVariant value = m_rhs_node.execute(exec_policy);

    try {
      std::visit(
        [&](auto&& v) {
          using T = std::decay_t<decltype(v)>;
          if constexpr (std::is_same_v<T, ValueT>) {
            *m_lhs = std::vector{std::move(v)};
          } else if constexpr (std::is_arithmetic_v<ValueT> and std::is_convertible_v<T, ValueT>) {
            if constexpr (std::is_same_v<uint64_t, ValueT> and std::is_same_v<int64_t, T>) {
              if (v < 0) {
                throw std::domain_error("trying to affect negative value (" + stringify(v) + ")");
              }
            }
            *m_lhs = std::vector{std::move(static_cast<ValueT>(v))};
          } else if constexpr (std::is_same_v<std::string, ValueT>) {
            *m_lhs = std::vector{std::move(stringify(v))};
          } else if constexpr (is_tiny_vector_v<ValueT> or is_tiny_matrix_v<ValueT>) {
            if constexpr (std::is_same_v<ValueT, TinyVector<1>> and std::is_arithmetic_v<T>) {
              *m_lhs = std::vector<TinyVector<1>>{TinyVector<1>{static_cast<double>(v)}};
            } else if constexpr (std::is_same_v<ValueT, TinyMatrix<1>> and std::is_arithmetic_v<T>) {
              *m_lhs = std::vector<TinyMatrix<1>>{TinyMatrix<1>{static_cast<double>(v)}};
            } else if constexpr (std::is_same_v<T, int64_t>) {
              Assert(v == 0);
              *m_lhs = std::vector<ValueT>{ValueT{zero}};
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("unexpected right hand side type in affectation");
              // LCOV_EXCL_STOP
            }
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("unexpected right hand side type in affectation");
            // LCOV_EXCL_STOP
          }
        },
        value);
    }
    catch (std::domain_error& e) {
      throw ParseError(e.what(), m_rhs_node.begin());
    }
    return {};
  }

  AffectationToTupleProcessor(ASTNode& lhs_node, ASTNode& rhs_node)
    : AffectationToDataVariantProcessorBase(lhs_node), m_rhs_node{rhs_node}
  {}
};

template <typename ValueT>
class AffectationFromTupleProcessor final : public AffectationToDataVariantProcessorBase
{
 private:
  ASTNode& m_lhs_node;
  ASTNode& m_rhs_node;

 public:
  Type
  type() const
  {
    return Type::affectation_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    DataVariant value = m_rhs_node.execute(exec_policy);

    try {
      std::visit(
        [&](auto&& v) {
          using T = std::decay_t<decltype(v)>;
          if constexpr (is_std_vector_v<T>) {
            using TupleContentType = std::decay_t<typename T::value_type>;
            if (v.size() == 1) {
              if constexpr (std::is_same_v<TupleContentType, ValueT>) {
                *m_lhs = std::move(static_cast<ValueT>(v[0]));
              } else if constexpr (std::is_arithmetic_v<ValueT> and std::is_convertible_v<TupleContentType, ValueT>) {
                if constexpr (std::is_same_v<uint64_t, ValueT> and std::is_same_v<int64_t, TupleContentType>) {
                  if (v[0] < 0) {
                    throw std::domain_error("trying to affect negative value (" + stringify(v[0]) + ")");
                  }
                }
                *m_lhs = std::move(static_cast<ValueT>(v[0]));
              } else if constexpr ((std::is_same_v<ValueT, TinyVector<1>> or
                                    std::is_same_v<ValueT, TinyMatrix<1>>)and(std::is_arithmetic_v<TupleContentType>)) {
                *m_lhs = std::move(ValueT(v[0]));
              } else if constexpr (std::is_same_v<std::string, ValueT>) {
                *m_lhs = std::move(stringify(static_cast<TupleContentType>(v[0])));
              } else {
                // LCOV_EXCL_START
                throw UnexpectedError("unexpected rhs tuple content");
                // LCOV_EXCL_STOP
              }
            } else {
              std::ostringstream error_msg;
              error_msg << "cannot affect a " << rang::fgB::yellow << dataTypeName(m_rhs_node.m_data_type)
                        << rang::fg::reset << " of size " << v.size() << " to a " << rang::fgB::yellow
                        << dataTypeName(m_lhs_node.m_data_type) << rang::fg::reset;
              throw ParseError(error_msg.str(), m_rhs_node.begin());
            }
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("right hand side must be a tuple");
            // LCOV_EXCL_STOP
          }
        },
        value);
    }
    catch (std::domain_error& e) {
      throw ParseError(e.what(), m_rhs_node.begin());
    }
    return {};
  }

  AffectationFromTupleProcessor(ASTNode& lhs_node, ASTNode& rhs_node)
    : AffectationToDataVariantProcessorBase(lhs_node), m_lhs_node(lhs_node), m_rhs_node{rhs_node}
  {}
};

template <typename ValueT>
class AffectationToTupleFromListProcessor final : public AffectationToDataVariantProcessorBase
{
 private:
  ASTNode& m_rhs_node;

  void
  _copyAggregateDataVariant(const AggregateDataVariant& children_values)
  {
    std::vector<ValueT> tuple_value(children_values.size());
    for (size_t i = 0; i < children_values.size(); ++i) {
      try {
        std::visit(
          [&](auto&& child_value) {
            using T = std::decay_t<decltype(child_value)>;
            if constexpr (std::is_same_v<T, ValueT>) {
              tuple_value[i] = child_value;
            } else if constexpr (std::is_arithmetic_v<ValueT> and std::is_convertible_v<T, ValueT>) {
              if constexpr (std::is_same_v<uint64_t, ValueT> and std::is_same_v<int64_t, T>) {
                if (child_value < 0) {
                  throw std::domain_error("trying to affect negative value (" + stringify(child_value) + ")");
                }
              }
              tuple_value[i] = static_cast<ValueT>(child_value);
            } else if constexpr (std::is_same_v<std::string, ValueT>) {
              tuple_value[i] = std::move(stringify(child_value));
            } else if constexpr (is_tiny_vector_v<ValueT>) {
              if constexpr (std::is_arithmetic_v<T>) {
                if constexpr (std::is_same_v<ValueT, TinyVector<1>>) {
                  tuple_value[i][0] = child_value;
                } else {
                  // in this case a 0 is given
                  Assert(child_value == 0);
                  tuple_value[i] = ZeroType{};
                }
              } else {
                // LCOV_EXCL_START
                throw UnexpectedError("invalid right hand side type in affectation");
                // LCOV_EXCL_STOP
              }
            } else if constexpr (is_tiny_matrix_v<ValueT>) {
              if constexpr (std::is_arithmetic_v<T>) {
                if constexpr (std::is_same_v<ValueT, TinyMatrix<1>>) {
                  tuple_value[i](0, 0) = child_value;
                } else {
                  // in this case a 0 is given
                  Assert(child_value == 0);
                  tuple_value[i] = ZeroType{};
                }
              } else {
                // LCOV_EXCL_START
                throw UnexpectedError("invalid right hand side type in affectation");
                // LCOV_EXCL_STOP
              }
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("invalid right hand side type in affectation");
              // LCOV_EXCL_STOP
            }
          },
          children_values[i]);
      }
      catch (std::domain_error& e) {
        throw ParseError(e.what(), m_rhs_node.children[i]->begin());
      }
    }
    *m_lhs = std::move(tuple_value);
  }

 public:
  Type
  type() const
  {
    return Type::affectation_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    try {
      std::visit(
        [&](auto&& value_list) {
          using ValueListT = std::decay_t<decltype(value_list)>;
          if constexpr (std::is_same_v<AggregateDataVariant, ValueListT>) {
            this->_copyAggregateDataVariant(value_list);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("invalid rhs (expecting list)");
            // LCOV_EXCL_STOP
          }
        },
        m_rhs_node.execute(exec_policy));
    }
    catch (std::domain_error& e) {
      throw ParseError(e.what(), m_rhs_node.begin());   // LCOV_EXCL_LINE
    }
    return {};
  }

  AffectationToTupleFromListProcessor(ASTNode& lhs_node, ASTNode& rhs_node)
    : AffectationToDataVariantProcessorBase(lhs_node), m_rhs_node{rhs_node}
  {}
};

template <typename ValueT>
class AffectationToTupleFromTupleProcessor final : public AffectationToDataVariantProcessorBase
{
 private:
  ASTNode& m_rhs_node;

  template <typename DataType>
  void
  _copyVector(const std::vector<DataType>& values)
  {
    std::vector<ValueT> v(values.size());
    if constexpr (std::is_same_v<ValueT, DataType>) {
      v = std::move(values);
    } else if constexpr (std::is_arithmetic_v<ValueT> and std::is_convertible_v<DataType, ValueT>) {
      for (size_t i = 0; i < values.size(); ++i) {
        const DataType& vi = values[i];
        if constexpr (std::is_same_v<uint64_t, ValueT> and std::is_same_v<int64_t, DataType>) {
          if (vi < 0) {
            throw std::domain_error("trying to affect negative value (" + stringify(vi) + ")");
          }
        }
        v[i] = static_cast<DataType>(vi);
      }
    } else if constexpr (std::is_same_v<TinyVector<1>, ValueT> and std::is_arithmetic_v<DataType>) {
      for (size_t i = 0; i < values.size(); ++i) {
        v[i][0] = values[i];
      }
    } else if constexpr (std::is_same_v<TinyMatrix<1>, ValueT> and std::is_arithmetic_v<DataType>) {
      for (size_t i = 0; i < values.size(); ++i) {
        v[i](0, 0) = values[i];
      }
    } else if constexpr (std::is_same_v<ValueT, std::string>) {
      for (size_t i = 0; i < values.size(); ++i) {
        v[i] = std::move(stringify(values[i]));
      }
    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("invalid right hand side type in tuple affectation");
      // LCOV_EXCL_STOP
    }

    *m_lhs = std::move(v);
  }

 public:
  Type
  type() const
  {
    return Type::affectation_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    try {
      std::visit(
        [&](auto&& value_list) {
          using ValueListT = std::decay_t<decltype(value_list)>;
          if constexpr (is_std_vector_v<ValueListT>) {
            this->_copyVector(value_list);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("invalid rhs (expecting tuple)");
            // LCOV_EXCL_STOP
          }
        },
        m_rhs_node.execute(exec_policy));
    }
    catch (std::domain_error& e) {
      throw ParseError(e.what(), m_rhs_node.begin());
    }
    return {};
  }

  AffectationToTupleFromTupleProcessor(ASTNode& lhs_node, ASTNode& rhs_node)
    : AffectationToDataVariantProcessorBase(lhs_node), m_rhs_node{rhs_node}
  {}
};

template <typename ValueT>
class AffectationFromZeroProcessor final : public AffectationToDataVariantProcessorBase
{
 public:
  Type
  type() const
  {
    return Type::affectation_processor;
  }

  DataVariant
  execute(ExecutionPolicy&)
  {
    *m_lhs = ValueT{zero};
    return {};
  }

  AffectationFromZeroProcessor(ASTNode& lhs_node) : AffectationToDataVariantProcessorBase(lhs_node) {}
};

template <typename OperatorT>
class ListAffectationProcessor final : public INodeProcessor
{
 private:
  ASTNode& m_node;

  std::vector<std::unique_ptr<IAffectationExecutor>> m_affectation_executor_list;

 public:
  template <typename ValueT, typename DataT>
  void
  add(ASTNode& lhs_node)
  {
    using AffectationExecutorT = AffectationExecutor<OperatorT, ValueT, DataT>;

    if (lhs_node.is_type<language::name>()) {
      const std::string& symbol = lhs_node.string();
      auto [i_symbol, found]    = m_node.m_symbol_table->find(symbol, m_node.children[0]->end());
      Assert(found);
      DataVariant& value = i_symbol->attributes().value();

      if (not std::holds_alternative<ValueT>(value)) {
        value = ValueT{};
      }

      m_affectation_executor_list.emplace_back(std::make_unique<AffectationExecutorT>(m_node, value));
    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("invalid left hand side");
      // LCOV_EXCL_STOP
    }
  }

  Type
  type() const
  {
    return Type::affectation_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    AggregateDataVariant children_values = std::get<AggregateDataVariant>(m_node.children[1]->execute(exec_policy));
    Assert(m_affectation_executor_list.size() == children_values.size());
    for (size_t i = 0; i < m_affectation_executor_list.size(); ++i) {
      try {
        m_affectation_executor_list[i]->affect(std::move(children_values[i]));
      }
      catch (std::domain_error& e) {
        throw ParseError(e.what(), m_node.children[1]->children[i]->begin());
      }
    }
    return {};
  }

  ListAffectationProcessor(ASTNode& node) : m_node{node} {}
};

struct IAffectationFromTupleExecutor
{
  virtual void affect(ExecutionPolicy& exec_policy, DataVariant&& rhs) = 0;

  IAffectationFromTupleExecutor(const IAffectationFromTupleExecutor&) = delete;
  IAffectationFromTupleExecutor(IAffectationFromTupleExecutor&&)      = delete;

  IAffectationFromTupleExecutor() = default;

  virtual ~IAffectationFromTupleExecutor() = default;
};

template <typename OperatorT, typename ValueT, typename DataT>
class AffectationFromTupleExecutor final : public IAffectationFromTupleExecutor
{
 private:
  DataVariant& m_lhs;
  ASTNode& m_node;

  static_assert(std::is_same_v<OperatorT, language::eq_op>);

 public:
  AffectationFromTupleExecutor(ASTNode& node, DataVariant& lhs) : m_lhs{lhs}, m_node{node} {}

  PUGS_INLINE void
  affect(ExecutionPolicy&, DataVariant&& rhs) final
  {
    if constexpr (std::is_same_v<DataT, ValueT>) {
      m_lhs = rhs;
    } else if constexpr (std::is_convertible_v<DataT, ValueT>) {
      const DataT& v = std::get<DataT>(rhs);
      if constexpr (std::is_same_v<int64_t, DataT> and std::is_same_v<uint64_t, ValueT>) {
        if (v < 0) {
          throw std::domain_error("trying to affect negative value (" + stringify(v) + ")");
        }
      }
      m_lhs = static_cast<ValueT>(v);
    } else if constexpr ((std::is_same_v<ValueT, TinyVector<1>>)and(std::is_arithmetic_v<DataT>)) {
      m_lhs = std::move(ValueT(std::get<DataT>(rhs)));
    } else if constexpr ((std::is_same_v<ValueT, TinyMatrix<1>>)and(std::is_arithmetic_v<DataT>)) {
      m_lhs = std::move(ValueT(std::get<DataT>(rhs)));
    } else if constexpr (std::is_same_v<ValueT, std::string>) {
      m_lhs = std::move(stringify(std::get<DataT>(rhs)));
    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("incompatible types in affectation from tuple");
      // LCOV_EXCL_STOP
    }
  }
};

template <typename OperatorT, typename TupleContentType>
class ListAffectationFromTupleProcessor final : public INodeProcessor
{
 public:
  static_assert(not is_std_vector_v<TupleContentType>);

 private:
  ASTNode& m_node;

  std::vector<std::unique_ptr<IAffectationFromTupleExecutor>> m_affectation_executor_list;

 public:
  template <typename ValueT>
  void
  add(ASTNode& lhs_node)
  {
    using DataT = std::decay_t<TupleContentType>;

    using AffectationExecutorT = AffectationFromTupleExecutor<OperatorT, ValueT, DataT>;

    if (lhs_node.is_type<language::name>()) {
      const std::string& symbol = lhs_node.string();
      auto [i_symbol, found]    = m_node.m_symbol_table->find(symbol, m_node.children[0]->end());
      Assert(found);
      DataVariant& value = i_symbol->attributes().value();

      if (not std::holds_alternative<ValueT>(value)) {
        value = ValueT{};
      }

      m_affectation_executor_list.emplace_back(std::make_unique<AffectationExecutorT>(m_node, value));
    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("invalid left hand side");
      // LCOV_EXCL_STOP
    }
  }

  Type
  type() const
  {
    return Type::affectation_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    using TupleType = std::vector<TupleContentType>;

    TupleType tuple_values = std::move(std::get<TupleType>(m_node.children[1]->execute(exec_policy)));

    if (m_affectation_executor_list.size() == tuple_values.size()) {
      for (size_t i = 0; i < m_affectation_executor_list.size(); ++i) {
        try {
          m_affectation_executor_list[i]->affect(exec_policy,
                                                 DataVariant(static_cast<TupleContentType>(tuple_values[i])));
        }
        catch (std::domain_error& e) {
          throw ParseError(e.what(), m_node.children[1]->begin());
        }
      }
    } else {
      std::ostringstream error_msg;
      error_msg << "cannot affect a " << rang::fgB::yellow << dataTypeName(m_node.children[1]->m_data_type)
                << rang::fg::reset << " of size " << tuple_values.size() << " to a " << rang::fgB::yellow
                << dataTypeName(m_node.children[0]->m_data_type) << rang::fg::reset;
      throw ParseError(error_msg.str(), m_node.children[1]->begin());
    }
    return {};
  }

  ListAffectationFromTupleProcessor(ASTNode& node) : m_node{node} {}
};

#endif   // AFFECTATION_PROCESSOR_HPP
