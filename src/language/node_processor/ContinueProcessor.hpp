#ifndef CONTINUE_PROCESSOR_HPP
#define CONTINUE_PROCESSOR_HPP

#include <language/node_processor/INodeProcessor.hpp>

class ContinueProcessor final : public INodeProcessor
{
 public:
  Type
  type() const
  {
    return Type::continue_processor;
  }

  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    exec_policy = ExecutionPolicy(exec_policy, ExecutionPolicy::JumpType::continue_jump);
    return {};
  }

  ContinueProcessor() = default;
};

#endif   // CONTINUE_PROCESSOR_HPP
