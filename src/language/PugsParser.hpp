#ifndef PUGS_PARSER_HPP
#define PUGS_PARSER_HPP

#include <string>

void parser(const std::string& filename);

#endif   // PUGS_PARSER_HPP
