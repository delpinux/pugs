#include <language/ast/ASTNodeFunctionEvaluationExpressionBuilder.hpp>

#include <language/ast/ASTNodeBuiltinFunctionExpressionBuilder.hpp>
#include <language/ast/ASTNodeFunctionExpressionBuilder.hpp>
#include <language/utils/ParseError.hpp>
#include <language/utils/SymbolTable.hpp>

ASTNodeFunctionEvaluationExpressionBuilder::ASTNodeFunctionEvaluationExpressionBuilder(ASTNode& node)
{
  switch (node.children[0]->m_data_type) {
  case ASTNodeDataType::function_t: {
    ASTNodeFunctionExpressionBuilder{node};
    break;
  }
  case ASTNodeDataType::builtin_function_t: {
    ASTNodeBuiltinFunctionExpressionBuilder{node};
    break;
  }
    //    LCOV_EXCL_START
  default: {
    throw ParseError("unexpected function type", node.begin());
  }
    //    LCOV_EXCL_STOP
  }
}
