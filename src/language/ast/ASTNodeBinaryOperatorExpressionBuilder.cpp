#include <language/ast/ASTNodeBinaryOperatorExpressionBuilder.hpp>

#include <language/PEGGrammar.hpp>
#include <language/node_processor/BinaryExpressionProcessor.hpp>
#include <language/node_processor/ConcatExpressionProcessor.hpp>
#include <language/utils/BinaryOperatorMangler.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <language/utils/ParseError.hpp>

ASTNodeBinaryOperatorExpressionBuilder::ASTNodeBinaryOperatorExpressionBuilder(ASTNode& n)
{
  const ASTNodeDataType& lhs_data_type = n.children[0]->m_data_type;
  const ASTNodeDataType& rhs_data_type = n.children[1]->m_data_type;

  const std::string binary_operator_name = [&]() -> std::string {
    if (n.is_type<language::multiply_op>()) {
      return binaryOperatorMangler<language::multiply_op>(lhs_data_type, rhs_data_type);
    } else if (n.is_type<language::divide_op>()) {
      return binaryOperatorMangler<language::divide_op>(lhs_data_type, rhs_data_type);
    } else if (n.is_type<language::plus_op>()) {
      return binaryOperatorMangler<language::plus_op>(lhs_data_type, rhs_data_type);
    } else if (n.is_type<language::minus_op>()) {
      return binaryOperatorMangler<language::minus_op>(lhs_data_type, rhs_data_type);

    } else if (n.is_type<language::shift_left_op>()) {
      return binaryOperatorMangler<language::shift_left_op>(lhs_data_type, rhs_data_type);
    } else if (n.is_type<language::shift_right_op>()) {
      return binaryOperatorMangler<language::shift_right_op>(lhs_data_type, rhs_data_type);

    } else if (n.is_type<language::or_op>()) {
      return binaryOperatorMangler<language::or_op>(lhs_data_type, rhs_data_type);
    } else if (n.is_type<language::and_op>()) {
      return binaryOperatorMangler<language::and_op>(lhs_data_type, rhs_data_type);
    } else if (n.is_type<language::xor_op>()) {
      return binaryOperatorMangler<language::xor_op>(lhs_data_type, rhs_data_type);

    } else if (n.is_type<language::greater_op>()) {
      return binaryOperatorMangler<language::greater_op>(lhs_data_type, rhs_data_type);
    } else if (n.is_type<language::greater_or_eq_op>()) {
      return binaryOperatorMangler<language::greater_or_eq_op>(lhs_data_type, rhs_data_type);
    } else if (n.is_type<language::lesser_op>()) {
      return binaryOperatorMangler<language::lesser_op>(lhs_data_type, rhs_data_type);
    } else if (n.is_type<language::lesser_or_eq_op>()) {
      return binaryOperatorMangler<language::lesser_or_eq_op>(lhs_data_type, rhs_data_type);
    } else if (n.is_type<language::eqeq_op>()) {
      return binaryOperatorMangler<language::eqeq_op>(lhs_data_type, rhs_data_type);
    } else if (n.is_type<language::not_eq_op>()) {
      return binaryOperatorMangler<language::not_eq_op>(lhs_data_type, rhs_data_type);
    } else {
      throw ParseError("unexpected error: undefined binary operator", std::vector{n.begin()});
    }
  }();

  const auto& optional_processor_builder =
    OperatorRepository::instance().getBinaryProcessorBuilder(binary_operator_name);

  if (optional_processor_builder.has_value()) {
    n.m_node_processor = optional_processor_builder.value()->getNodeProcessor(n);
  } else {
    std::ostringstream error_message;
    error_message << "undefined binary operator type: ";
    error_message << rang::fgB::red << binary_operator_name << rang::fg::reset;

    throw ParseError(error_message.str(), std::vector{n.children[0]->begin()});
  }
}
