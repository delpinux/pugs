#ifndef AST_NODE_EMPTY_BLOCK_CLEANER_HPP
#define AST_NODE_EMPTY_BLOCK_CLEANER_HPP

#include <language/ast/ASTNode.hpp>

class ASTNodeEmptyBlockCleaner
{
 private:
  void _removeEmptyBlockNode(ASTNode& node);

 public:
  ASTNodeEmptyBlockCleaner(ASTNode& root_node);
};

#endif   // AST_NODE_EMPTY_BLOCK_CLEANER_HPP
