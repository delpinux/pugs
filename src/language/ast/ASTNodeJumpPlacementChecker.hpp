#ifndef AST_NODE_JUMP_PLACEMENT_CHECKER_HPP
#define AST_NODE_JUMP_PLACEMENT_CHECKER_HPP

#include <language/ast/ASTNode.hpp>

class ASTNodeJumpPlacementChecker
{
 private:
  void _checkJumpPlacement(ASTNode& node, bool is_inside_loop);

 public:
  ASTNodeJumpPlacementChecker(ASTNode& root_node);
};

#endif   // AST_NODE_JUMP_PLACEMENT_CHECKER_HPP
