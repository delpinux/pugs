#include <language/ast/ASTSymbolTableBuilder.hpp>

#include <language/PEGGrammar.hpp>
#include <language/utils/ParseError.hpp>
#include <language/utils/SymbolTable.hpp>
#include <utils/ConsoleManager.hpp>

void
ASTSymbolTableBuilder::buildSymbolTable(ASTNode& n, std::shared_ptr<SymbolTable>& symbol_table)
{
  if (n.is_type<language::block>() or (n.is_type<language::for_statement>()) or
      (n.is_type<language::while_statement>()) or (n.is_type<language::do_while_statement>())) {
    if (!n.children.empty()) {
      std::shared_ptr block_symbol_table = std::make_shared<SymbolTable>(symbol_table);
      n.m_symbol_table                   = block_symbol_table;

      for (auto& child : n.children) {
        this->buildSymbolTable(*child, block_symbol_table);
      }
    }
  } else if (n.is_type<language::fct_declaration>()) {
    std::shared_ptr local_symbol_table =
      std::make_shared<SymbolTable>(symbol_table, std::make_shared<SymbolTable::Context>());

    n.m_symbol_table          = local_symbol_table;
    const std::string& symbol = n.children[0]->string();

    if (symbol_table->getBuiltinFunctionSymbolList(symbol, n.children[0]->begin()).size() > 0) {
      std::ostringstream error_message;
      error_message << "symbol '" << rang::fg::red << symbol << rang::fg::reset
                    << "' already denotes a builtin function!";
      throw ParseError(error_message.str(), std::vector{n.children[0]->begin()});
    }

    if (auto [i_symbol, found] = symbol_table->find(symbol, n.children[0]->begin()); found) {
      std::ostringstream error_message;
      error_message << "symbol '" << rang::fg::red << symbol << rang::fg::reset << "' was already defined at line "
                    << i_symbol->attributes().position().line;
      throw ParseError(error_message.str(), std::vector{n.children[0]->begin()});
    }
    auto [i_symbol, success] = symbol_table->add(symbol, n.children[0]->begin());
    Assert(success);

    for (auto& child : n.children) {
      this->buildSymbolTable(*child, local_symbol_table);
    }

    size_t function_id =
      symbol_table->functionTable().add(FunctionDescriptor{symbol, std::move(n.children[1]), std::move(n.children[2])});
    i_symbol->attributes().value() = function_id;
    n.children.resize(1);
  } else {
    n.m_symbol_table = symbol_table;
    if (n.has_content()) {
      if (n.is_type<language::var_declaration>()) {
        auto register_symbol = [&](const ASTNode& argument_node) {
          if (symbol_table->getBuiltinFunctionSymbolList(argument_node.string(), argument_node.begin()).size() > 0) {
            std::ostringstream error_message;
            error_message << "symbol '" << rang::fg::red << argument_node.string() << rang::fg::reset
                          << "' already denotes a builtin function!";
            throw ParseError(error_message.str(), std::vector{argument_node.begin()});
          }

          if (auto [i_symbol, found] = symbol_table->find(argument_node.string(), argument_node.begin()); found) {
            std::ostringstream error_message;
            error_message << "symbol '" << rang::fg::red << argument_node.string() << rang::fg::reset
                          << "' was already defined at line " << i_symbol->attributes().position().line;
            throw ParseError(error_message.str(), std::vector{argument_node.begin()});
          }
          symbol_table->add(argument_node.string(), argument_node.begin());
        };

        if (n.children[0]->is_type<language::name>()) {
          register_symbol(*n.children[0]);
        } else {   // treats the case of list of parameters
          Assert(n.children[0]->is_type<language::name_list>());
          for (auto& child : n.children[0]->children) {
            register_symbol(*child);
          }
        }
      } else if (n.is_type<language::function_definition>()) {
        auto register_and_initialize_symbol = [&](const ASTNode& argument_node) {
          if (symbol_table->getBuiltinFunctionSymbolList(argument_node.string(), argument_node.begin()).size() > 0) {
            // LCOV_EXCL_START
            std::ostringstream error_message;
            error_message << "symbol '" << rang::fg::red << argument_node.string() << rang::fg::reset
                          << "' already denotes a builtin function!";
            throw ParseError(error_message.str(), std::vector{argument_node.begin()});
            // LCOV_EXCL_STOP
          }

          auto [i_symbol, success] = symbol_table->add(argument_node.string(), argument_node.begin());
          if (not success) {
            std::ostringstream error_message;
            error_message << "symbol '" << rang::fg::red << argument_node.string() << rang::fg::reset
                          << "' was already defined at line " << i_symbol->attributes().position().line;
            throw ParseError(error_message.str(), std::vector{argument_node.begin()});
          }
          // Symbols will be initialized at call
          i_symbol->attributes().setIsInitialized();
        };

        if (n.children[0]->is_type<language::empty_set>()) {
          // nothing to do
        } else if (n.children[0]->is_type<language::name>()) {
          register_and_initialize_symbol(*n.children[0]);
        } else {   // treats the case of list of parameters
          Assert(n.children[0]->is_type<language::name_list>());
          for (auto& child : n.children[0]->children) {
            register_and_initialize_symbol(*child);
          }
        }
      } else if (n.is_type<language::name>()) {
        if (not symbol_table->has(n.string(), n.begin())) {
          std::ostringstream error_message;
          error_message << "undefined symbol '" << rang::fg::red << n.string() << rang::fg::reset << '\'';
          throw ParseError(error_message.str(), std::vector{n.begin()});
        }
      }
    }

    for (auto& child : n.children) {
      this->buildSymbolTable(*child, symbol_table);
    }
  }
}

ASTSymbolTableBuilder::ASTSymbolTableBuilder(ASTNode& node)
{
  Assert(node.is_root());

  this->buildSymbolTable(node, node.m_symbol_table);

  if (ConsoleManager::showPreamble()) {
    std::cout << " - checked symbols declaration\n";
  }
}
