#include <language/ast/ASTNodeDataTypeBuilder.hpp>

#include <language/PEGGrammar.hpp>
#include <language/utils/ASTNodeNaturalConversionChecker.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/BuiltinFunctionEmbedderUtils.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <language/utils/ParseError.hpp>
#include <language/utils/SymbolTable.hpp>
#include <utils/ConsoleManager.hpp>
#include <utils/PugsAssert.hpp>

void
ASTNodeDataTypeBuilder::_buildDeclarationNodeDataTypes(ASTNode& type_node, ASTNode& name_node) const
{
  ASTNodeDataType data_type;
  if (type_node.is_type<language::type_expression>()) {
    if (type_node.children.size() != name_node.children.size()) {
      std::ostringstream message;
      message << "number of product spaces (" << type_node.children.size() << ") " << rang::fgB::yellow
              << type_node.string() << rang::style::reset << rang::style::bold << " differs from number of variables ("
              << name_node.children.size() << ") " << rang::fgB::yellow << name_node.string() << rang::style::reset;
      throw ParseError(message.str(), name_node.begin());
    }

    std::vector<std::shared_ptr<const ASTNodeDataType>> sub_data_type_list;
    sub_data_type_list.reserve(type_node.children.size());
    for (size_t i = 0; i < type_node.children.size(); ++i) {
      auto& sub_type_node = *type_node.children[i];
      auto& sub_name_node = *name_node.children[i];
      _buildDeclarationNodeDataTypes(sub_type_node, sub_name_node);
      sub_data_type_list.push_back(std::make_shared<const ASTNodeDataType>(sub_type_node.m_data_type));
    }
    data_type = ASTNodeDataType::build<ASTNodeDataType::list_t>(sub_data_type_list);
  } else {
    if (type_node.is_type<language::B_set>()) {
      data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
    } else if (type_node.is_type<language::Z_set>()) {
      data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
    } else if (type_node.is_type<language::N_set>()) {
      data_type = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
    } else if (type_node.is_type<language::R_set>()) {
      data_type = ASTNodeDataType::build<ASTNodeDataType::double_t>();
    } else if (type_node.is_type<language::empty_set>()) {
      throw ParseError("'void' keyword does not define a type", std::vector{type_node.begin()});
    } else if (type_node.is_type<language::vector_type>()) {
      data_type = getVectorDataType(type_node);
    } else if (type_node.is_type<language::matrix_type>()) {
      data_type = getMatrixDataType(type_node);
    } else if (type_node.is_type<language::tuple_type_specifier>()) {
      data_type = getTupleDataType(type_node);
    } else if (type_node.is_type<language::string_type>()) {
      data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
    } else if (type_node.is_type<language::type_name_id>()) {
      const std::string& type_name_id = type_node.string();

      auto& symbol_table = *type_node.m_symbol_table;

      auto [i_type_symbol, found] = symbol_table.find(type_name_id, type_node.begin());
      if (not found) {
        throw ParseError("undefined type identifier", std::vector{type_node.begin()});
      } else if (i_type_symbol->attributes().dataType() != ASTNodeDataType::type_name_id_t) {
        std::ostringstream os;
        os << "invalid type identifier, '" << type_name_id << "' was previously defined as a '"
           << dataTypeName(i_type_symbol->attributes().dataType()) << '\'';
        throw ParseError(os.str(), std::vector{type_node.begin()});
      }

      data_type = ASTNodeDataType::build<ASTNodeDataType::type_id_t>(type_name_id);
    }

    if (name_node.is_type<language::name_list>()) {
      throw ParseError("unexpected variable list for single space", std::vector{name_node.begin()});
    }

    Assert(name_node.is_type<language::name>());
    name_node.m_data_type = data_type;

    const std::string& symbol = name_node.string();

    std::shared_ptr<SymbolTable>& symbol_table = name_node.m_symbol_table;

    auto [i_symbol, found] = symbol_table->find(symbol, name_node.begin());
    Assert(found);
    i_symbol->attributes().setDataType(name_node.m_data_type);
  }

  Assert(data_type != ASTNodeDataType::undefined_t);
  type_node.m_data_type = ASTNodeDataType::build<ASTNodeDataType::typename_t>(data_type);
}

void
ASTNodeDataTypeBuilder::_buildNodeDataTypes(ASTNode& n) const
{
  if (n.is_type<language::block>() or n.is_type<language::for_statement>()) {
    for (auto& child : n.children) {
      this->_buildNodeDataTypes(*child);
    }

    if (n.is_type<language::for_statement>()) {
      const ASTNode& test_node = *n.children[1];

      if (not n.children[1]->is_type<language::for_test>()) {
        ASTNodeNaturalConversionChecker{test_node, ASTNodeDataType::build<ASTNodeDataType::bool_t>()};
      }   // in the case of empty for_test (not simplified node), nothing to check!
    }

    n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
  } else {
    if (n.has_content()) {
      if (n.is_type<language::import_instruction>()) {
        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
      } else if (n.is_type<language::module_name>()) {
        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();

      } else if (n.is_type<language::true_kw>() or n.is_type<language::false_kw>()) {
        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
      } else if (n.is_type<language::real>()) {
        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::double_t>();
      } else if (n.is_type<language::integer>()) {
        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
      } else if (n.is_type<language::row_expression>()) {
        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
      } else if (n.is_type<language::vector_type>()) {
        n.m_data_type = getVectorDataType(n);
      } else if (n.is_type<language::matrix_type>()) {
        n.m_data_type = getMatrixDataType(n);
      } else if (n.is_type<language::tuple_type_specifier>()) {
        n.m_data_type = getTupleDataType(n);

      } else if (n.is_type<language::literal>()) {
        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
      } else if (n.is_type<language::var_declaration>()) {
        auto& name_node = *(n.children[0]);
        auto& type_node = *(n.children[1]);

        _buildDeclarationNodeDataTypes(type_node, name_node);
        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
      } else if (n.is_type<language::fct_declaration>()) {
        n.children[0]->m_data_type = ASTNodeDataType::build<ASTNodeDataType::function_t>();

        const std::string& symbol = n.children[0]->string();

        auto [i_symbol, success] = n.m_symbol_table->find(symbol, n.children[0]->begin());

        auto& function_table = n.m_symbol_table->functionTable();

        uint64_t function_id                    = std::get<uint64_t>(i_symbol->attributes().value());
        FunctionDescriptor& function_descriptor = function_table[function_id];

        this->_buildNodeDataTypes(function_descriptor.domainMappingNode());

        ASTNode& parameters_domain_node = *function_descriptor.domainMappingNode().children[0];
        ASTNode& parameters_name_node   = *function_descriptor.definitionNode().children[0];

        {   // Function data type
          const std::string& function_symbol = n.children[0]->string();

          std::shared_ptr<SymbolTable>& symbol_table = n.m_symbol_table;

          auto [i_function_symbol, found] = symbol_table->find(function_symbol, n.children[0]->begin());
          Assert(found);
          i_function_symbol->attributes().setDataType(n.children[0]->m_data_type);
        }

        const size_t nb_parameter_domains =
          (parameters_domain_node.is_type<language::type_expression>()) ? parameters_domain_node.children.size() : 1;
        const size_t nb_parameter_names =
          (parameters_name_node.children.size() > 0) ? parameters_name_node.children.size() : 1;

        if (nb_parameter_domains != nb_parameter_names) {
          std::ostringstream message;
          message << "number of product spaces (" << nb_parameter_domains << ") " << rang::fgB::yellow
                  << parameters_domain_node.string() << rang::style::reset << rang::style::bold
                  << " differs from number of variables (" << nb_parameter_names << ") " << rang::fgB::yellow
                  << parameters_name_node.string() << rang::style::reset;
          throw ParseError(message.str(), parameters_domain_node.begin());
        }

        {
          if (parameters_domain_node.is_type<language::type_expression>()) {
            for (size_t i_domain = 0; i_domain < parameters_domain_node.children.size(); ++i_domain) {
              if (parameters_domain_node.children[i_domain]->is_type<language::tuple_type_specifier>()) {
                std::ostringstream message;
                message << "cannot use tuple " << rang::fgB::yellow
                        << dataTypeName(parameters_domain_node.children[i_domain]->m_data_type) << rang::fg::reset
                        << " as a domain for user functions" << rang::style::reset;
                throw ParseError(message.str(), parameters_domain_node.children[i_domain]->begin());
              }
            }
          } else {
            if (parameters_domain_node.is_type<language::tuple_type_specifier>()) {
              std::ostringstream message;
              message << "cannot use tuple " << rang::fgB::yellow << dataTypeName(parameters_domain_node.m_data_type)
                      << rang::fg::reset << " as a domain for user functions" << rang::style::reset;
              throw ParseError(message.str(), parameters_domain_node.begin());
            }
          }
        }

        auto simple_type_allocator = [&](const ASTNode& type_node, ASTNode& symbol_node) {
          Assert(symbol_node.is_type<language::name>());

          const ASTNodeDataType data_type = [&] {
            if (type_node.m_data_type == ASTNodeDataType::type_id_t) {
              return ASTNodeDataType::build<ASTNodeDataType::type_id_t>(type_node.m_data_type.nameOfTypeId());
            } else {
              return type_node.m_data_type.contentType();
            }
          }();

          symbol_node.m_data_type             = data_type;
          const std::string& symbol_node_name = symbol_node.string();

          std::shared_ptr<SymbolTable>& symbol_table = n.m_symbol_table;

          auto [i_symbol_node_name, found] = symbol_table->find(symbol_node_name, symbol_node.begin());
          Assert(found);
          i_symbol_node_name->attributes().setDataType(data_type);
        };

        if (parameters_domain_node.is_type<language::empty_set>() or
            parameters_name_node.is_type<language::empty_set>()) {
          if (not parameters_domain_node.is_type<language::empty_set>()) {
            std::ostringstream error_msg;
            throw ParseError("unexpected 'void' keyword", std::vector{parameters_name_node.begin()});
          } else if (not parameters_name_node.is_type<language::empty_set>()) {
            throw ParseError("expecting 'void' keyword", std::vector{parameters_name_node.begin()});
          }
        } else {
          if (nb_parameter_domains == 1) {
            simple_type_allocator(parameters_domain_node, parameters_name_node);
          } else {
            std::vector<std::shared_ptr<const ASTNodeDataType>> sub_data_type_list;
            sub_data_type_list.reserve(nb_parameter_domains);

            for (size_t i = 0; i < nb_parameter_domains; ++i) {
              simple_type_allocator(*parameters_domain_node.children[i], *parameters_name_node.children[i]);
              sub_data_type_list.push_back(
                std::make_shared<const ASTNodeDataType>(parameters_name_node.children[i]->m_data_type));
            }
            parameters_name_node.m_data_type = ASTNodeDataType::build<ASTNodeDataType::list_t>(sub_data_type_list);
          }
        }

        this->_buildNodeDataTypes(function_descriptor.definitionNode());

        ASTNode& image_domain_node     = *function_descriptor.domainMappingNode().children[1];
        ASTNode& image_expression_node = *function_descriptor.definitionNode().children[1];

        const size_t nb_image_domains =
          (image_domain_node.is_type<language::type_expression>()) ? image_domain_node.children.size() : 1;
        const size_t nb_image_expressions =
          (image_expression_node.is_type<language::expression_list>()) ? image_expression_node.children.size() : 1;

        if ((not image_domain_node.is_type<language::tuple_type_specifier>()) and
            (nb_image_domains != nb_image_expressions)) {
          std::ostringstream message;
          message << "number of image spaces (" << nb_image_domains << ") " << rang::fgB::yellow
                  << image_domain_node.string() << rang::style::reset << rang::style::bold
                  << " differs from number of expressions (" << nb_image_expressions << ") " << rang::fgB::yellow
                  << image_expression_node.string() << rang::style::reset;
          throw ParseError(message.str(), image_domain_node.begin());
        }

        this->_buildNodeDataTypes(image_expression_node);

        ASTNodeNaturalConversionChecker<AllowRToR1Conversion>(image_expression_node, image_domain_node.m_data_type);

        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
        return;
      } else if (n.is_type<language::name>()) {
        std::shared_ptr<SymbolTable>& symbol_table = n.m_symbol_table;

        auto [i_symbol, found] = symbol_table->find(n.string(), n.begin());
        if (found) {
          n.m_data_type = i_symbol->attributes().dataType();
        } else if (symbol_table->has(n.string(), n.begin())) {
          n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::builtin_function_t>();
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("could not find symbol " + n.string());
          // LCOV_EXCL_STOP
        }
      } else if (n.is_type<language::type_name_id>()) {
        const std::string& type_name_id = n.string();

        auto& symbol_table = *n.m_symbol_table;

        const auto [i_type_symbol, found] = symbol_table.find(type_name_id, n.begin());
        if (not found) {
          throw ParseError("undefined type identifier", std::vector{n.begin()});
        } else if (i_type_symbol->attributes().dataType() != ASTNodeDataType::type_name_id_t) {
          std::ostringstream os;
          os << "invalid type identifier, '" << type_name_id << "' was previously defined as a '"
             << dataTypeName(i_type_symbol->attributes().dataType()) << '\'';
          throw ParseError(os.str(), std::vector{n.begin()});
        }

        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::type_id_t>(type_name_id);
      }
    }
    for (auto& child : n.children) {
      this->_buildNodeDataTypes(*child);
    }

    if (n.is_type<language::vector_expression>()) {
      n.m_data_type = getVectorExpressionType(n);
    } else if (n.is_type<language::matrix_expression>()) {
      n.m_data_type = getMatrixExpressionType(n);
    }
    if (n.is_type<language::break_kw>() or n.is_type<language::continue_kw>()) {
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
    } else if (n.is_type<language::eq_op>() or n.is_type<language::multiplyeq_op>() or
               n.is_type<language::divideeq_op>() or n.is_type<language::pluseq_op>() or
               n.is_type<language::minuseq_op>()) {
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();

    } else if (n.is_type<language::inner_expression_list>()) {
      std::vector<std::shared_ptr<const ASTNodeDataType>> sub_data_type_list;
      sub_data_type_list.reserve(n.children.size());

      for (size_t i = 0; i < n.children.size(); ++i) {
        sub_data_type_list.push_back(std::make_shared<const ASTNodeDataType>(n.children[i]->m_data_type));
      }
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::list_t>(sub_data_type_list);

    } else if (n.is_type<language::type_mapping>() or n.is_type<language::function_definition>()) {
      for (auto& child : n.children) {
        this->_buildNodeDataTypes(*child);
      }
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
    } else if (n.is_type<language::type_expression>()) {
      std::vector<std::shared_ptr<const ASTNodeDataType>> sub_data_type_list;
      sub_data_type_list.reserve(n.children.size());

      auto check_sub_type = [&](const ASTNode& image_node) {
        ASTNodeDataType value_type;
        if (image_node.is_type<language::B_set>()) {
          value_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
        } else if (image_node.is_type<language::Z_set>()) {
          value_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
        } else if (image_node.is_type<language::N_set>()) {
          value_type = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
        } else if (image_node.is_type<language::R_set>()) {
          value_type = ASTNodeDataType::build<ASTNodeDataType::double_t>();
        } else if (image_node.is_type<language::vector_type>()) {
          value_type = getVectorDataType(image_node);
        } else if (image_node.is_type<language::matrix_type>()) {
          value_type = getMatrixDataType(image_node);
        } else if (image_node.is_type<language::string_type>()) {
          value_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
        } else if (image_node.is_type<language::type_name_id>()) {
          value_type = ASTNodeDataType::build<ASTNodeDataType::type_id_t>(image_node.m_data_type.nameOfTypeId());
        } else if (image_node.is_type<language::tuple_type_specifier>()) {
          value_type = getTupleDataType(image_node);
        }

        // LCOV_EXCL_START
        if (value_type == ASTNodeDataType::undefined_t) {
          throw ParseError("invalid value type", image_node.begin());
        }
        // LCOV_EXCL_STOP
      };

      for (size_t i = 0; i < n.children.size(); ++i) {
        check_sub_type(*n.children[i]);
        sub_data_type_list.push_back(std::make_shared<const ASTNodeDataType>(n.children[i]->m_data_type));
      }
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::typename_t>(
        ASTNodeDataType::build<ASTNodeDataType::list_t>(sub_data_type_list));
    } else if (n.is_type<language::for_post>() or n.is_type<language::for_init>() or
               n.is_type<language::for_statement_block>()) {
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
    } else if (n.is_type<language::for_test>()) {
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
    } else if (n.is_type<language::statement_block>()) {
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();
    } else if (n.is_type<language::if_statement>() or n.is_type<language::while_statement>()) {
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();

      const ASTNode& test_node = *n.children[0];
      ASTNodeNaturalConversionChecker{test_node, ASTNodeDataType::build<ASTNodeDataType::bool_t>()};
    } else if (n.is_type<language::do_while_statement>()) {
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();

      const ASTNode& test_node = *n.children[1];
      ASTNodeNaturalConversionChecker{test_node, ASTNodeDataType::build<ASTNodeDataType::bool_t>()};
    } else if (n.is_type<language::unary_not>() or n.is_type<language::unary_minus>()) {
      auto& operator_repository = OperatorRepository::instance();

      auto optional_value_type = [&] {
        if (n.is_type<language::unary_not>()) {
          return operator_repository.getUnaryOperatorValueType(
            unaryOperatorMangler<language::unary_not>(n.children[0]->m_data_type));
        } else if (n.is_type<language::unary_minus>()) {
          return operator_repository.getUnaryOperatorValueType(
            unaryOperatorMangler<language::unary_minus>(n.children[0]->m_data_type));
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("invalid unary operator type");
          // LCOV_EXCL_STOP
        }
      }();

      if (optional_value_type.has_value()) {
        n.m_data_type = optional_value_type.value();
      } else {
        std::ostringstream message;
        message << "undefined unary operator\n"
                << "note: unexpected operand type " << rang::fgB::red << dataTypeName(n.children[0]->m_data_type)
                << rang::style::reset;
        throw ParseError(message.str(), n.begin());
      }
    } else if (n.is_type<language::unary_plusplus>() or n.is_type<language::unary_minusminus>() or
               n.is_type<language::post_plusplus>() or n.is_type<language::post_minusminus>()) {
      auto& operator_repository = OperatorRepository::instance();

      auto optional_value_type = [&] {
        if (n.is_type<language::unary_plusplus>()) {
          return operator_repository.getIncDecOperatorValueType(
            incDecOperatorMangler<language::unary_plusplus>(n.children[0]->m_data_type));
        } else if (n.is_type<language::unary_minusminus>()) {
          return operator_repository.getIncDecOperatorValueType(
            incDecOperatorMangler<language::unary_minusminus>(n.children[0]->m_data_type));
        } else if (n.is_type<language::post_minusminus>()) {
          return operator_repository.getIncDecOperatorValueType(
            incDecOperatorMangler<language::post_minusminus>(n.children[0]->m_data_type));
        } else if (n.is_type<language::post_plusplus>()) {
          return operator_repository.getIncDecOperatorValueType(
            incDecOperatorMangler<language::post_plusplus>(n.children[0]->m_data_type));
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("unexpected operator type");
          // LCOV_EXCL_STOP
        }
      }();

      if (optional_value_type.has_value()) {
        n.m_data_type = optional_value_type.value();
      } else {
        std::ostringstream message;
        message << "undefined increment/decrement operator\n"
                << "note: unexpected operand type " << rang::fgB::red << dataTypeName(n.children[0]->m_data_type)
                << rang::style::reset;
        throw ParseError(message.str(), n.begin());
      }
    } else if (n.is_type<language::plus_op>() or n.is_type<language::minus_op>() or
               n.is_type<language::multiply_op>() or n.is_type<language::divide_op>() or
               n.is_type<language::shift_left_op>() or n.is_type<language::shift_right_op>() or
               n.is_type<language::lesser_op>() or n.is_type<language::lesser_or_eq_op>() or
               n.is_type<language::greater_op>() or n.is_type<language::greater_or_eq_op>() or
               n.is_type<language::eqeq_op>() or n.is_type<language::not_eq_op>() or n.is_type<language::and_op>() or
               n.is_type<language::or_op>() or n.is_type<language::xor_op>()) {
      const ASTNodeDataType type_0 = n.children[0]->m_data_type;
      const ASTNodeDataType type_1 = n.children[1]->m_data_type;
      auto& operator_repository    = OperatorRepository::instance();

      auto optional_value_type = [&] {
        if (n.is_type<language::plus_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::plus_op>(type_0, type_1));
        } else if (n.is_type<language::minus_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::minus_op>(type_0, type_1));
        } else if (n.is_type<language::multiply_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::multiply_op>(type_0, type_1));
        } else if (n.is_type<language::divide_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::divide_op>(type_0, type_1));

        } else if (n.is_type<language::shift_left_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::shift_left_op>(type_0, type_1));
        } else if (n.is_type<language::shift_right_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::shift_right_op>(type_0, type_1));
        } else if (n.is_type<language::lesser_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::lesser_op>(type_0, type_1));
        } else if (n.is_type<language::lesser_or_eq_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::lesser_or_eq_op>(type_0, type_1));
        } else if (n.is_type<language::greater_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::greater_op>(type_0, type_1));
        } else if (n.is_type<language::greater_or_eq_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::greater_or_eq_op>(type_0, type_1));
        } else if (n.is_type<language::eqeq_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::eqeq_op>(type_0, type_1));
        } else if (n.is_type<language::not_eq_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::not_eq_op>(type_0, type_1));

        } else if (n.is_type<language::and_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::and_op>(type_0, type_1));
        } else if (n.is_type<language::or_op>()) {
          return operator_repository.getBinaryOperatorValueType(binaryOperatorMangler<language::or_op>(type_0, type_1));
        } else if (n.is_type<language::xor_op>()) {
          return operator_repository.getBinaryOperatorValueType(
            binaryOperatorMangler<language::xor_op>(type_0, type_1));

        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("unexpected operator type");
          // LCOV_EXCL_STOP
        }
      }();

      if (optional_value_type.has_value()) {
        n.m_data_type = optional_value_type.value();
      } else {
        std::ostringstream message;
        message << "undefined binary operator\n"
                << "note: incompatible operand types " << dataTypeName(type_0) << " and " << dataTypeName(type_1);
        throw ParseError(message.str(), n.begin());
      }
    } else if (n.is_type<language::function_evaluation>()) {
      if (n.children[0]->m_data_type == ASTNodeDataType::function_t) {
        const std::string& function_name = n.children[0]->string();

        auto [i_function_symbol, success] = n.m_symbol_table->find(function_name, n.children[0]->begin());

        auto& function_table = n.m_symbol_table->functionTable();

        uint64_t function_id                    = std::get<uint64_t>(i_function_symbol->attributes().value());
        FunctionDescriptor& function_descriptor = function_table[function_id];

        ASTNode& image_domain_node = *function_descriptor.domainMappingNode().children[1];

        n.m_data_type = [&] {
          if (image_domain_node.m_data_type == ASTNodeDataType::type_id_t) {
            return ASTNodeDataType::build<ASTNodeDataType::type_id_t>(image_domain_node.m_data_type.nameOfTypeId());
          } else if (image_domain_node.m_data_type == ASTNodeDataType::tuple_t) {
            return ASTNodeDataType::build<ASTNodeDataType::tuple_t>(image_domain_node.m_data_type.contentType());
          } else {
            return image_domain_node.m_data_type.contentType();
          }
        }();

      } else if (n.children[0]->m_data_type == ASTNodeDataType::builtin_function_t) {
        auto builtin_function_embedder = getBuiltinFunctionEmbedder(n);

        n.m_data_type = builtin_function_embedder->getReturnDataType();
      } else {
        std::ostringstream message;
        message << "invalid function call\n"
                << "note: '" << n.children[0]->string() << "' (type: " << dataTypeName(n.children[0]->m_data_type)
                << ") is not a function!";
        throw ParseError(message.str(), n.begin());
      }
    } else if (n.is_type<language::subscript_expression>()) {
      auto& array_expression = *n.children[0];

      if (array_expression.m_data_type == ASTNodeDataType::vector_t) {
        auto& index_expression = *n.children[1];
        ASTNodeNaturalConversionChecker{index_expression, ASTNodeDataType::build<ASTNodeDataType::int_t>()};
        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::double_t>();
        if (n.children.size() != 2) {
          std::ostringstream message;
          message << "invalid index type: " << rang::fgB::yellow << dataTypeName(array_expression.m_data_type)
                  << rang::style::reset << " requires a single integer";
          throw ParseError(message.str(), index_expression.begin());
        }
      } else if (array_expression.m_data_type == ASTNodeDataType::matrix_t) {
        for (size_t i = 1; i < n.children.size(); ++i) {
          ASTNodeNaturalConversionChecker{*n.children[i], ASTNodeDataType::build<ASTNodeDataType::int_t>()};
        }
        n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::double_t>();

        if (n.children.size() != 3) {
          std::ostringstream message;
          message << "invalid index type: " << rang::fgB::yellow << dataTypeName(n.children[0]->m_data_type)
                  << rang::style::reset << " requires two integers";
          throw ParseError(message.str(), n.children[1]->begin());
        }

      } else {
        std::ostringstream message;
        message << "invalid subscript expression: " << rang::fgB::yellow << dataTypeName(array_expression.m_data_type)
                << rang::style::reset << " cannot be indexed";

        throw ParseError(message.str(), n.begin());
      }
    } else if (n.is_type<language::B_set>()) {
      n.m_data_type =
        ASTNodeDataType::build<ASTNodeDataType::typename_t>(ASTNodeDataType::build<ASTNodeDataType::bool_t>());
    } else if (n.is_type<language::Z_set>()) {
      n.m_data_type =
        ASTNodeDataType::build<ASTNodeDataType::typename_t>(ASTNodeDataType::build<ASTNodeDataType::int_t>());
    } else if (n.is_type<language::N_set>()) {
      n.m_data_type =
        ASTNodeDataType::build<ASTNodeDataType::typename_t>(ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>());
    } else if (n.is_type<language::string_type>()) {
      n.m_data_type =
        ASTNodeDataType::build<ASTNodeDataType::typename_t>(ASTNodeDataType::build<ASTNodeDataType::string_t>());
    } else if (n.is_type<language::R_set>()) {
      n.m_data_type =
        ASTNodeDataType::build<ASTNodeDataType::typename_t>(ASTNodeDataType::build<ASTNodeDataType::double_t>());
    } else if (n.is_type<language::empty_set>()) {
      n.m_data_type =
        ASTNodeDataType::build<ASTNodeDataType::typename_t>(ASTNodeDataType::build<ASTNodeDataType::void_t>());
    } else if (n.is_type<language::vector_type>()) {
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::typename_t>(getVectorDataType(n));
    } else if (n.is_type<language::matrix_type>()) {
      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::typename_t>(getMatrixDataType(n));
    } else if (n.is_type<language::name_list>() or n.is_type<language::lvalue_list>() or
               n.is_type<language::function_argument_list>() or n.is_type<language::expression_list>()) {
      std::vector<std::shared_ptr<const ASTNodeDataType>> sub_data_type_list;
      sub_data_type_list.reserve(n.children.size());

      for (size_t i = 0; i < n.children.size(); ++i) {
        sub_data_type_list.push_back(std::make_shared<const ASTNodeDataType>(n.children[i]->m_data_type));
      }

      n.m_data_type = ASTNodeDataType::build<ASTNodeDataType::list_t>(sub_data_type_list);
    }
  }
}

ASTNodeDataTypeBuilder::ASTNodeDataTypeBuilder(ASTNode& node)
{
  Assert(node.is_root());
  node.m_data_type = ASTNodeDataType::build<ASTNodeDataType::void_t>();

  this->_buildNodeDataTypes(node);

  if (ConsoleManager::showPreamble()) {
    std::cout << " - built node data types\n";
  }
}
