#ifndef AST_NODE_FUNCTION_EXPRESSION_BUILDER_HPP
#define AST_NODE_FUNCTION_EXPRESSION_BUILDER_HPP

#include <language/ast/ASTNode.hpp>
#include <language/ast/ASTNodeSubDataType.hpp>
#include <language/node_processor/INodeProcessor.hpp>

class FunctionProcessor;
class FunctionDescriptor;
class IFunctionArgumentConverter;

class ASTNodeFunctionExpressionBuilder
{
 private:
  ASTNode& m_node;

  template <typename SymbolType>
  std::unique_ptr<IFunctionArgumentConverter> _getArgumentConverter(SymbolType& parameter_symbol,
                                                                    const ASTNodeSubDataType& node_sub_data_type);

  void _storeArgumentConverter(ASTNode& parameter_variable,
                               ASTNodeSubDataType& node_sub_data_type,
                               FunctionProcessor& function_processor);

  std::unique_ptr<FunctionProcessor> _buildArgumentConverter(FunctionDescriptor& function_descriptor, ASTNode& node);

  std::unique_ptr<INodeProcessor> _getFunctionProcessor(const ASTNodeDataType& return_value_type,
                                                        ASTNode& node,
                                                        ASTNode& function_component_expression);

 public:
  ASTNodeFunctionExpressionBuilder(ASTNode& node);
};

#endif   // AST_NODE_FUNCTION_EXPRESSION_BUILDER_HPP
