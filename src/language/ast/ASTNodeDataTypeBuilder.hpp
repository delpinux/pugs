#ifndef AST_NODE_DATA_TYPE_BUILDER_HPP
#define AST_NODE_DATA_TYPE_BUILDER_HPP

#include <language/ast/ASTNode.hpp>

class ASTNodeDataTypeBuilder
{
 private:
  void _buildDeclarationNodeDataTypes(ASTNode& type_node, ASTNode& name_node) const;

  void _buildNodeDataTypes(ASTNode& node) const;

 public:
  ASTNodeDataTypeBuilder(ASTNode& root_node);
};

#endif   // AST_NODE_DATA_TYPE_BUILDER_HPP
