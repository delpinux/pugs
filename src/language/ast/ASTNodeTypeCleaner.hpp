#ifndef AST_NODE_DECLARATION_CLEANER_HPP
#define AST_NODE_DECLARATION_CLEANER_HPP

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>
#include <utils/PugsAssert.hpp>

#include <stack>

template <typename NodeType>
class ASTNodeTypeCleaner
{
 private:
  void
  _removeDeclarationNode(ASTNode& node)
  {
    std::stack<size_t> declaration_ids;
    for (size_t i_child = 0; i_child < node.children.size(); ++i_child) {
      if (node.children[i_child]->is_type<NodeType>()) {
        declaration_ids.push(i_child);
      }
    }

    while (declaration_ids.size() > 0) {
      size_t i_removed = declaration_ids.top();
      declaration_ids.pop();
      for (size_t i = i_removed; i + 1 < node.children.size(); ++i) {
        node.children[i] = std::move(node.children[i + 1]);
      }
      node.children.pop_back();
    }

    for (auto& child : node.children) {
      this->_removeDeclarationNode(*child);
    }
  }

 public:
  ASTNodeTypeCleaner(ASTNode& root_node)
  {
    Assert(root_node.is_root());
    this->_removeDeclarationNode(root_node);
  }
};

#endif   // AST_NODE_DECLARATION_CLEANER_HPP
