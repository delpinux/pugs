#ifndef AST_NODE_ARRAY_SUBSCRIPT_EXPRESSION_BUILDER_HPP
#define AST_NODE_ARRAY_SUBSCRIPT_EXPRESSION_BUILDER_HPP

#include <language/ast/ASTNode.hpp>

class ASTNodeArraySubscriptExpressionBuilder
{
 public:
  ASTNodeArraySubscriptExpressionBuilder(ASTNode& node);
};

#endif   // AST_NODE_ARRAY_SUBSCRIPT_EXPRESSION_BUILDER_HPP
