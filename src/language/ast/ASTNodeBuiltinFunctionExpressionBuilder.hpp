#ifndef AST_NODE_C_FUNCTION_EXPRESSION_BUILDER_HPP
#define AST_NODE_C_FUNCTION_EXPRESSION_BUILDER_HPP

#include <language/ast/ASTNode.hpp>
#include <language/ast/ASTNodeDataTypeFlattener.hpp>
#include <language/node_processor/INodeProcessor.hpp>

class BuiltinFunctionProcessor;
class IFunctionArgumentConverter;

class ASTNodeBuiltinFunctionExpressionBuilder
{
 private:
  PUGS_INLINE std::unique_ptr<IFunctionArgumentConverter> _getArgumentConverter(
    const ASTNodeDataType& parameter_type,
    const ASTNodeSubDataType& argument_node_sub_data_type,
    const size_t argument_number);

  PUGS_INLINE
  void _storeArgumentProcessor(const std::vector<ASTNodeDataType>& parameter_type_list,
                               const ASTNodeDataTypeFlattener::FlattenedDataTypeList& flattened_datatype_list,
                               const size_t argument_number,
                               BuiltinFunctionProcessor& _processor);

  PUGS_INLINE
  void _buildArgumentProcessors(const std::vector<ASTNodeDataType>& parameter_type_list,
                                ASTNode& node,
                                BuiltinFunctionProcessor& _processor);

 public:
  ASTNodeBuiltinFunctionExpressionBuilder(ASTNode& node);
};

#endif   // AST_NODE_C_FUNCTION_EXPRESSION_BUILDER_HPP
