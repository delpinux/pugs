#include <language/ast/ASTNodeDeclarationToAffectationConverter.hpp>

#include <language/PEGGrammar.hpp>
#include <utils/PugsAssert.hpp>

void
ASTNodeDeclarationToAffectationConverter::_convertNodeDeclaration(ASTNode& n)
{
  if (n.is_type<language::var_declaration>()) {
    if (n.children.size() == 4) {
      n.children[0] = std::move(n.children[2]);
      n.children[1] = std::move(n.children[3]);
      n.children.resize(2);
      n.set_type<language::eq_op>();
    }
  } else {
    for (auto& child : n.children) {
      this->_convertNodeDeclaration(*child);
    }
  }
}

ASTNodeDeclarationToAffectationConverter::ASTNodeDeclarationToAffectationConverter(ASTNode& n)
{
  Assert(n.is_root());
  this->_convertNodeDeclaration(n);
}
