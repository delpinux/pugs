#ifndef AST_SYMBOL_TABLE_BUILDER_HPP
#define AST_SYMBOL_TABLE_BUILDER_HPP

#include <language/ast/ASTNode.hpp>
#include <language/utils/SymbolTable.hpp>

class ASTSymbolTableBuilder
{
 private:
  void buildSymbolTable(ASTNode& node, std::shared_ptr<SymbolTable>& symbol_table);

 public:
  ASTSymbolTableBuilder(ASTNode& root_node);

  ASTSymbolTableBuilder(const ASTSymbolTableBuilder&) = delete;

  ~ASTSymbolTableBuilder() = default;
};

#endif   // AST_SYMBOL_TABLE_BUILDER_HPP
