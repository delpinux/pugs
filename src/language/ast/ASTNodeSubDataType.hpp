#ifndef AST_NODE_SUB_DATA_TYPE_HPP
#define AST_NODE_SUB_DATA_TYPE_HPP

#include <language/ast/ASTNode.hpp>
#include <language/utils/ASTNodeDataType.hpp>

struct ASTNodeSubDataType
{
  ASTNodeDataType m_data_type;
  ASTNode& m_parent_node;
};

#endif   // AST_NODE_SUB_DATA_TYPE_HPP
