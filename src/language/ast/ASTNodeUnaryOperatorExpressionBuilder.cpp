#include <language/ast/ASTNodeUnaryOperatorExpressionBuilder.hpp>

#include <language/PEGGrammar.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <language/utils/ParseError.hpp>
#include <language/utils/UnaryOperatorMangler.hpp>

ASTNodeUnaryOperatorExpressionBuilder::ASTNodeUnaryOperatorExpressionBuilder(ASTNode& n)
{
  const ASTNodeDataType& data_type = n.children[0]->m_data_type;

  const std::string unary_operator_name = [&] {
    if (n.is_type<language::unary_minus>()) {
      return unaryOperatorMangler<language::unary_minus>(data_type);
    } else if (n.is_type<language::unary_not>()) {
      return unaryOperatorMangler<language::unary_not>(data_type);
    } else {
      // LCOV_EXCL_START
      throw ParseError("unexpected error: undefined unary operator", std::vector{n.begin()});
      // LCOV_EXCL_STOP
    }
  }();

  const auto& optional_processor_builder = OperatorRepository::instance().getUnaryProcessorBuilder(unary_operator_name);

  if (optional_processor_builder.has_value()) {
    n.m_node_processor = optional_processor_builder.value()->getNodeProcessor(n);
  } else {
    std::ostringstream error_message;
    error_message << "undefined unary operator type: ";
    error_message << rang::fgB::red << unary_operator_name << rang::fg::reset;

    throw ParseError(error_message.str(), std::vector{n.children[0]->begin()});
  }
}
