#ifndef AST_EXECUTION_STACK_HPP
#define AST_EXECUTION_STACK_HPP

#include <utils/PugsAssert.hpp>
#include <utils/SourceLocation.hpp>

#include <pegtl/string_input.hpp>
#include <string>
#include <vector>

class ASTNode;
class ASTExecutionStack
{
 private:
  std::vector<const ASTNode*> m_stack;

  std::shared_ptr<TAO_PEGTL_NAMESPACE::string_input<>> m_file_input;
  std::string m_file_content;

  static ASTExecutionStack* m_instance;

  ASTExecutionStack() = default;
  ASTExecutionStack(const std::shared_ptr<TAO_PEGTL_NAMESPACE::string_input<>>& file_input,
                    const std::string& file_content);
  ~ASTExecutionStack() = default;

 public:
  std::string errorMessageAt(const std::string& error_msg) const;

  const std::string&
  fileContent() const
  {
    return m_file_content;
  }

  SourceLocation sourceLocation() const;

  const ASTNode& currentNode() const;

  void
  push(const ASTNode* node)
  {
    m_stack.push_back(node);
  }

  void
  pop()
  {
    m_stack.pop_back();
  }

  static void create();   // for unit tests only
  static void create(const std::shared_ptr<TAO_PEGTL_NAMESPACE::string_input<>>& file_input,
                     const std::string& file_content);
  static void destroy();

  static ASTExecutionStack&
  getInstance()
  {
    Assert(m_instance != nullptr);
    return *m_instance;
  }

  ASTExecutionStack(const ASTExecutionStack&) = delete;
  ASTExecutionStack(ASTExecutionStack&&)      = delete;
};

#endif   // AST_EXECUTION_STACK_HPP
