#ifndef AST_BUILDER_HPP
#define AST_BUILDER_HPP

#include <language/ast/ASTNode.hpp>

#include <memory>

struct ASTBuilder
{
 private:
  struct rearrange_binary_op;
  struct rearrange_post_unary_op;
  struct rearrange_pre_unary_op;
  struct simplify_node_list;
  struct simplify_for_statement_block;
  struct simplify_for_init;
  struct simplify_for_test;
  struct simplify_for_post;
  struct simplify_statement_block;
  struct simplify_stream_statement;
  struct simplify_type_expression;

 public:
  template <typename InputT>
  static std::unique_ptr<ASTNode> build(InputT& input);
};

#endif   // AST_BUILDER_HPP
