#ifndef AST_NODE_LIST_AFFECTATION_EXPRESSION_BUILDER_HPP
#define AST_NODE_LIST_AFFECTATION_EXPRESSION_BUILDER_HPP

#include <language/ast/ASTNode.hpp>
#include <language/ast/ASTNodeSubDataType.hpp>

template <typename OperatorT, typename TupleType>
class ListAffectationFromTupleProcessor;

template <typename OperatorT>
class ListAffectationProcessor;

class ASTNodeListAffectationExpressionBuilder
{
 private:
  ASTNode& m_node;

  template <typename OperatorT, typename TupleContentType>
  void _buildListAffectationFromTupleProcessor(ListAffectationFromTupleProcessor<OperatorT, TupleContentType>&);

  template <typename OperatorT>
  void _buildListAffectationFromTupleProcessor();

  template <typename OperatorT>
  void _buildAffectationProcessor(const ASTNodeSubDataType& rhs_node_sub_data_type,
                                  ASTNode& value_node,
                                  std::unique_ptr<ListAffectationProcessor<OperatorT>>& list_affectation_processor);

  template <typename OperatorT>
  void _buildListAffectationProcessor();

 public:
  ASTNodeListAffectationExpressionBuilder(ASTNode& node);
};

#endif   // AST_NODE_LIST_AFFECTATION_EXPRESSION_BUILDER_HPP
