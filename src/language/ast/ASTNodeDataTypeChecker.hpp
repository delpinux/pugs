#ifndef AST_NODE_DATA_TYPE_CHECKER_HPP
#define AST_NODE_DATA_TYPE_CHECKER_HPP

#include <language/ast/ASTNode.hpp>

class ASTNodeDataTypeChecker
{
 private:
  void _checkNodeDataTypes(const ASTNode& node);

 public:
  ASTNodeDataTypeChecker(const ASTNode& root_node);
};

#endif   // AST_NODE_DATA_TYPE_CHECKER_HPP
