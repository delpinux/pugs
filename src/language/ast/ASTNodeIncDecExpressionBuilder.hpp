#ifndef AST_NODE_INC_DEC_EXPRESSION_BUILDER_HPP
#define AST_NODE_INC_DEC_EXPRESSION_BUILDER_HPP

#include <language/ast/ASTNode.hpp>

struct ASTNodeIncDecExpressionBuilder
{
  ASTNodeIncDecExpressionBuilder(ASTNode& node);
};

#endif   // AST_NODE_INC_DEC_EXPRESSION_BUILDER_HPP
