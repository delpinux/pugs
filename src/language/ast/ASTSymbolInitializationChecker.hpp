#ifndef AST_SYMBOL_INITIALIZATION_CHECKER_HPP
#define AST_SYMBOL_INITIALIZATION_CHECKER_HPP

#include <language/ast/ASTNode.hpp>

class ASTSymbolInitializationChecker
{
 private:
  void _checkSymbolInitialization(ASTNode& node);

 public:
  ASTSymbolInitializationChecker(ASTNode& root_node);
};

#endif   // AST_SYMBOL_INITIALIZATION_CHECKER_HPP
