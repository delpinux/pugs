#ifndef AST_NODE_DECLARATION_TO_AFFECTATION_CONVERTER_HPP
#define AST_NODE_DECLARATION_TO_AFFECTATION_CONVERTER_HPP

#include <language/ast/ASTNode.hpp>

class ASTNodeDeclarationToAffectationConverter
{
 private:
  void _convertNodeDeclaration(ASTNode& node);

 public:
  ASTNodeDeclarationToAffectationConverter(ASTNode& root_node);
};

#endif   // AST_NODE_DECLARATION_TO_AFFECTATION_CONVERTER_HPP
