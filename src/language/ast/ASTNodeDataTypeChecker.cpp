#include <language/ast/ASTNodeDataTypeChecker.hpp>

#include <language/utils/ParseError.hpp>
#include <utils/ConsoleManager.hpp>

void
ASTNodeDataTypeChecker::_checkNodeDataTypes(const ASTNode& n)
{
  if (n.m_data_type == ASTNodeDataType::undefined_t) {
    throw ParseError("unexpected error: undefined datatype for AST node for " + n.name(), n.begin());
  }

  for (const auto& child : n.children) {
    this->_checkNodeDataTypes(*child);
  }
}

ASTNodeDataTypeChecker::ASTNodeDataTypeChecker(const ASTNode& node)
{
  Assert(node.is_root());
  this->_checkNodeDataTypes(node);

  if (ConsoleManager::showPreamble()) {
    std::cout << " - checked node data types\n";
  }
}
