#ifndef AST_NODE_AFFECTATION_EXPRESSION_BUILDER_HPP
#define AST_NODE_AFFECTATION_EXPRESSION_BUILDER_HPP

#include <language/ast/ASTNode.hpp>

struct ASTNodeAffectationExpressionBuilder
{
  ASTNodeAffectationExpressionBuilder(ASTNode& node);
};

#endif   // AST_NODE_AFFECTATION_EXPRESSION_BUILDER_HPP
