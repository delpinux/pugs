#include <language/ast/ASTNodeListAffectationExpressionBuilder.hpp>

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNodeDataTypeFlattener.hpp>
#include <language/node_processor/AffectationProcessor.hpp>
#include <language/utils/ASTNodeNaturalConversionChecker.hpp>
#include <language/utils/ParseError.hpp>

#include <language/utils/AffectationMangler.hpp>
#include <language/utils/OperatorRepository.hpp>

template <typename OperatorT, typename TupleContentType>
void
ASTNodeListAffectationExpressionBuilder::_buildListAffectationFromTupleProcessor(
  ListAffectationFromTupleProcessor<OperatorT, TupleContentType>& list_affectation_processor)
{
  ASTNode& name_list_node = *m_node.children[0];
  for (size_t i = 0; i < name_list_node.children.size(); ++i) {
    ASTNode& value_node = *name_list_node.children[i];
    if constexpr (std::is_same_v<TupleContentType, bool>) {
      switch (value_node.m_data_type) {
      case ASTNodeDataType::bool_t: {
        list_affectation_processor.template add<bool>(value_node);
        break;
      }
      case ASTNodeDataType::unsigned_int_t: {
        list_affectation_processor.template add<uint64_t>(value_node);
        break;
      }
      case ASTNodeDataType::int_t: {
        list_affectation_processor.template add<int64_t>(value_node);
        break;
      }
      case ASTNodeDataType::double_t: {
        list_affectation_processor.template add<double>(value_node);
        break;
      }
      case ASTNodeDataType::vector_t: {
        if (value_node.m_data_type.dimension() == 1) {
          list_affectation_processor.template add<TinyVector<1>>(value_node);
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("invalid vector dimension");
          // LCOV_EXCL_STOP
        }
        break;
      }
      case ASTNodeDataType::matrix_t: {
        if ((value_node.m_data_type.numberOfRows() == 1) and (value_node.m_data_type.numberOfColumns() == 1)) {
          list_affectation_processor.template add<TinyMatrix<1>>(value_node);
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("invalid vector dimension");
          // LCOV_EXCL_STOP
        }
        break;
      }
      case ASTNodeDataType::string_t: {
        list_affectation_processor.template add<std::string>(value_node);
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("incompatible tuple types in affectation");
      }
        // LCOV_EXCL_STOP
      }
    } else if constexpr ((std::is_same_v<TupleContentType, uint64_t>) or (std::is_same_v<TupleContentType, int64_t>)) {
      switch (value_node.m_data_type) {
      case ASTNodeDataType::unsigned_int_t: {
        list_affectation_processor.template add<uint64_t>(value_node);
        break;
      }
      case ASTNodeDataType::int_t: {
        list_affectation_processor.template add<int64_t>(value_node);
        break;
      }
      case ASTNodeDataType::double_t: {
        list_affectation_processor.template add<double>(value_node);
        break;
      }
      case ASTNodeDataType::vector_t: {
        if (value_node.m_data_type.dimension() == 1) {
          list_affectation_processor.template add<TinyVector<1>>(value_node);
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("invalid vector dimension");
          // LCOV_EXCL_STOP
        }
        break;
      }
      case ASTNodeDataType::matrix_t: {
        if ((value_node.m_data_type.numberOfRows() == 1) and (value_node.m_data_type.numberOfColumns() == 1)) {
          list_affectation_processor.template add<TinyMatrix<1>>(value_node);
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("invalid vector dimension");
          // LCOV_EXCL_STOP
        }
        break;
      }
      case ASTNodeDataType::string_t: {
        list_affectation_processor.template add<std::string>(value_node);
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("incompatible tuple types in affectation");
      }
        // LCOV_EXCL_STOP
      }
    } else if constexpr (std::is_same_v<TupleContentType, double>) {
      switch (value_node.m_data_type) {
      case ASTNodeDataType::double_t: {
        list_affectation_processor.template add<double>(value_node);
        break;
      }
      case ASTNodeDataType::vector_t: {
        if (value_node.m_data_type.dimension() == 1) {
          list_affectation_processor.template add<TinyVector<1>>(value_node);
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("invalid vector dimension");
          // LCOV_EXCL_STOP
        }
        break;
      }
      case ASTNodeDataType::matrix_t: {
        if ((value_node.m_data_type.numberOfRows() == 1) and (value_node.m_data_type.numberOfColumns() == 1)) {
          list_affectation_processor.template add<TinyMatrix<1>>(value_node);
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("invalid vector dimension");
          // LCOV_EXCL_STOP
        }
        break;
      }
      case ASTNodeDataType::string_t: {
        list_affectation_processor.template add<std::string>(value_node);
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("incompatible tuple types in affectation");
      }
        // LCOV_EXCL_STOP
      }
    } else if constexpr (is_tiny_vector_v<TupleContentType>) {
      switch (value_node.m_data_type) {
      case ASTNodeDataType::vector_t: {
        if (value_node.m_data_type.dimension() == TupleContentType::Dimension) {
          list_affectation_processor.template add<TupleContentType>(value_node);
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("invalid vector dimension");
          // LCOV_EXCL_STOP
        }
        break;
      }
      case ASTNodeDataType::string_t: {
        list_affectation_processor.template add<std::string>(value_node);
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("incompatible tuple types in affectation");
      }
        // LCOV_EXCL_STOP
      }
    } else if constexpr (is_tiny_matrix_v<TupleContentType>) {
      switch (value_node.m_data_type) {
      case ASTNodeDataType::matrix_t: {
        if ((value_node.m_data_type.numberOfRows() == TupleContentType::NumberOfRows) and
            (value_node.m_data_type.numberOfColumns() == TupleContentType::NumberOfColumns)) {
          list_affectation_processor.template add<TupleContentType>(value_node);
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("invalid vector dimension");
          // LCOV_EXCL_STOP
        }
        break;
      }
      case ASTNodeDataType::string_t: {
        list_affectation_processor.template add<std::string>(value_node);
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("incompatible tuple types in affectation");
      }
        // LCOV_EXCL_STOP
      }
    } else if constexpr (std::is_same_v<std::string, TupleContentType>) {
      if (value_node.m_data_type == ASTNodeDataType::string_t) {
        list_affectation_processor.template add<std::string>(value_node);
      } else {
        // LCOV_EXCL_START
        throw UnexpectedError("incompatible tuple types in affectation");
        // LCOV_EXCL_STOP
      }
    } else if constexpr (std::is_same_v<EmbeddedData, TupleContentType>) {
      if (value_node.m_data_type == ASTNodeDataType::type_id_t) {
        list_affectation_processor.template add<EmbeddedData>(value_node);
      } else {
        // LCOV_EXCL_START
        throw UnexpectedError("incompatible tuple types in affectation");
        // LCOV_EXCL_STOP
      }
    } else {
      // LCOV_EXCL_START
      throw UnexpectedError("incompatible tuple types in affectation");
      // LCOV_EXCL_STOP
    }
  }
}

template <typename OperatorT>
void
ASTNodeListAffectationExpressionBuilder::_buildListAffectationFromTupleProcessor()
{
  auto& rhs = *m_node.children[1];

  Assert(rhs.m_data_type == ASTNodeDataType::tuple_t);

  const ASTNodeSubDataType tuple_content_type{rhs.m_data_type.contentType(), rhs};

  for (auto&& child : m_node.children[0]->children) {
    if (child->m_data_type == ASTNodeDataType::tuple_t) {
      throw ParseError("cannot affect a tuple to a compound type made of tuples", std::vector{child->begin()});
    }

    ASTNodeNaturalConversionChecker<AllowRToR1Conversion>(tuple_content_type, child->m_data_type);
  }

  switch (rhs.m_data_type.contentType()) {
  case ASTNodeDataType::bool_t: {
    std::unique_ptr list_affectation_processor =
      std::make_unique<ListAffectationFromTupleProcessor<OperatorT, bool>>(m_node);

    this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

    m_node.m_node_processor = std::move(list_affectation_processor);
    break;
  }
  case ASTNodeDataType::unsigned_int_t: {
    std::unique_ptr list_affectation_processor =
      std::make_unique<ListAffectationFromTupleProcessor<OperatorT, uint64_t>>(m_node);

    this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

    m_node.m_node_processor = std::move(list_affectation_processor);
    break;
  }
  case ASTNodeDataType::int_t: {
    std::unique_ptr list_affectation_processor =
      std::make_unique<ListAffectationFromTupleProcessor<OperatorT, int64_t>>(m_node);

    this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

    m_node.m_node_processor = std::move(list_affectation_processor);
    break;
  }
  case ASTNodeDataType::double_t: {
    std::unique_ptr list_affectation_processor =
      std::make_unique<ListAffectationFromTupleProcessor<OperatorT, double>>(m_node);

    this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

    m_node.m_node_processor = std::move(list_affectation_processor);
    break;
  }
  case ASTNodeDataType::vector_t: {
    switch (rhs.m_data_type.contentType().dimension()) {
    case 1: {
      std::unique_ptr list_affectation_processor =
        std::make_unique<ListAffectationFromTupleProcessor<OperatorT, TinyVector<1>>>(m_node);

      this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

      m_node.m_node_processor = std::move(list_affectation_processor);
      break;
    }
    case 2: {
      std::unique_ptr list_affectation_processor =
        std::make_unique<ListAffectationFromTupleProcessor<OperatorT, TinyVector<2>>>(m_node);

      this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

      m_node.m_node_processor = std::move(list_affectation_processor);
      break;
    }
    case 3: {
      std::unique_ptr list_affectation_processor =
        std::make_unique<ListAffectationFromTupleProcessor<OperatorT, TinyVector<3>>>(m_node);

      this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

      m_node.m_node_processor = std::move(list_affectation_processor);
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("invalid vector dimension");
    }
      // LCOV_EXCL_STOP
    }
    break;
  }
  case ASTNodeDataType::matrix_t: {
    if (rhs.m_data_type.contentType().numberOfColumns() != rhs.m_data_type.contentType().numberOfRows()) {
      // LCOV_EXCL_START
      throw UnexpectedError("invalid matrix dimensions");
      // LCOV_EXCL_STOP
    }
    switch (rhs.m_data_type.contentType().numberOfColumns()) {
    case 1: {
      std::unique_ptr list_affectation_processor =
        std::make_unique<ListAffectationFromTupleProcessor<OperatorT, TinyMatrix<1>>>(m_node);

      this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

      m_node.m_node_processor = std::move(list_affectation_processor);
      break;
    }
    case 2: {
      std::unique_ptr list_affectation_processor =
        std::make_unique<ListAffectationFromTupleProcessor<OperatorT, TinyMatrix<2>>>(m_node);

      this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

      m_node.m_node_processor = std::move(list_affectation_processor);
      break;
    }
    case 3: {
      std::unique_ptr list_affectation_processor =
        std::make_unique<ListAffectationFromTupleProcessor<OperatorT, TinyMatrix<3>>>(m_node);

      this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

      m_node.m_node_processor = std::move(list_affectation_processor);
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("invalid matrix dimensions");
    }
      // LCOV_EXCL_STOP
    }
    break;
  }
  case ASTNodeDataType::string_t: {
    std::unique_ptr list_affectation_processor =
      std::make_unique<ListAffectationFromTupleProcessor<OperatorT, std::string>>(m_node);

    this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

    m_node.m_node_processor = std::move(list_affectation_processor);
    break;
  }
  case ASTNodeDataType::type_id_t: {
    std::unique_ptr list_affectation_processor =
      std::make_unique<ListAffectationFromTupleProcessor<OperatorT, EmbeddedData>>(m_node);

    this->_buildListAffectationFromTupleProcessor(*list_affectation_processor);

    m_node.m_node_processor = std::move(list_affectation_processor);
    break;
  }
    // LCOV_EXCL_START
  default: {
    throw UnexpectedError("undefined affectation type");
  }
    // LCOV_EXCL_STOP
  }
}

template <typename OperatorT>
void
ASTNodeListAffectationExpressionBuilder::_buildAffectationProcessor(
  const ASTNodeSubDataType& rhs_node_sub_data_type,
  ASTNode& value_node,
  std::unique_ptr<ListAffectationProcessor<OperatorT>>& list_affectation_processor)
{
  auto add_affectation_processor_for_data = [&]<typename ValueT>(const ASTNodeSubDataType& node_sub_data_type) {
    switch (node_sub_data_type.m_data_type) {
    case ASTNodeDataType::bool_t: {
      list_affectation_processor->template add<ValueT, bool>(value_node);
      break;
    }
    case ASTNodeDataType::unsigned_int_t: {
      list_affectation_processor->template add<ValueT, uint64_t>(value_node);
      break;
    }
    case ASTNodeDataType::int_t: {
      list_affectation_processor->template add<ValueT, int64_t>(value_node);
      break;
    }
    case ASTNodeDataType::double_t: {
      list_affectation_processor->template add<ValueT, double>(value_node);
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("invalid operand type for affectation");
    }
      // LCOV_EXCL_STOP
    }
  };

  auto add_affectation_processor_for_vector_data = [&]<typename ValueT>(const ASTNodeSubDataType& node_sub_data_type) {
    if constexpr (std::is_same_v<ValueT, TinyVector<1>>) {
      if ((node_sub_data_type.m_data_type == ASTNodeDataType::vector_t) and
          (node_sub_data_type.m_data_type.dimension() == ValueT::Dimension)) {
        list_affectation_processor->template add<ValueT, ValueT>(value_node);
      } else {
        add_affectation_processor_for_data.template operator()<TinyVector<1>>(node_sub_data_type);
      }
    } else if constexpr (std::is_same_v<ValueT, TinyVector<2>> or std::is_same_v<ValueT, TinyVector<3>>) {
      if ((node_sub_data_type.m_data_type == ASTNodeDataType::vector_t) and
          (node_sub_data_type.m_data_type.dimension() == ValueT::Dimension)) {
        list_affectation_processor->template add<ValueT, ValueT>(value_node);
      } else if (node_sub_data_type.m_parent_node.is_type<language::integer>()) {
        if (std::stoi(node_sub_data_type.m_parent_node.string()) == 0) {
          list_affectation_processor->template add<ValueT, ZeroType>(value_node);
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("unexpected error: invalid operand value");
          // LCOV_EXCL_STOP
        }
      } else {
        // LCOV_EXCL_START
        throw UnexpectedError("invalid dimension");
        // LCOV_EXCL_STOP
      }
    } else {
      static_assert(std::is_same_v<ValueT, TinyVector<1>>, "unexpected value type");
    }
  };

  auto add_affectation_processor_for_matrix_data = [&]<typename ValueT>(const ASTNodeSubDataType& node_sub_data_type) {
    if constexpr (std::is_same_v<ValueT, TinyMatrix<1>>) {
      if ((node_sub_data_type.m_data_type == ASTNodeDataType::matrix_t) and
          (node_sub_data_type.m_data_type.numberOfRows() == ValueT::NumberOfRows) and
          (node_sub_data_type.m_data_type.numberOfColumns() == ValueT::NumberOfColumns)) {
        list_affectation_processor->template add<ValueT, ValueT>(value_node);
      } else {
        add_affectation_processor_for_data.template operator()<TinyMatrix<1>>(node_sub_data_type);
      }
    } else if constexpr (std::is_same_v<ValueT, TinyMatrix<2>> or std::is_same_v<ValueT, TinyMatrix<3>>) {
      if ((node_sub_data_type.m_data_type == ASTNodeDataType::matrix_t) and
          (node_sub_data_type.m_data_type.numberOfRows() == ValueT::NumberOfRows) and
          (node_sub_data_type.m_data_type.numberOfColumns() == ValueT::NumberOfColumns)) {
        list_affectation_processor->template add<ValueT, ValueT>(value_node);
      } else if (node_sub_data_type.m_parent_node.is_type<language::integer>()) {
        if (std::stoi(node_sub_data_type.m_parent_node.string()) == 0) {
          list_affectation_processor->template add<ValueT, ZeroType>(value_node);
        } else {
          // LCOV_EXCL_START
          throw UnexpectedError("invalid operand value");
          // LCOV_EXCL_STOP
        }
      } else {
        // LCOV_EXCL_START
        throw UnexpectedError("invalid dimension");
        // LCOV_EXCL_STOP
      }
    } else {
      static_assert(std::is_same_v<ValueT, TinyMatrix<1>>, "unexpected value type");
    }
  };

  auto add_affectation_processor_for_tuple_data = [&](const ASTNodeDataType& tuple_type,
                                                      const ASTNodeSubDataType& node_sub_data_type) {
    if constexpr (std::is_same_v<OperatorT, language::eq_op>) {
      if (node_sub_data_type.m_data_type == ASTNodeDataType::tuple_t) {
        const ASTNodeDataType& rhs_tuple_content = node_sub_data_type.m_data_type.contentType();
        switch (tuple_type.contentType()) {
        case ASTNodeDataType::bool_t: {
          if (rhs_tuple_content == ASTNodeDataType::bool_t) {
            list_affectation_processor->template add<std::vector<bool>, std::vector<bool>>(value_node);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible tuple types in affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::unsigned_int_t: {
          if (rhs_tuple_content == ASTNodeDataType::bool_t) {
            list_affectation_processor->template add<std::vector<uint64_t>, std::vector<bool>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::unsigned_int_t) {
            list_affectation_processor->template add<std::vector<uint64_t>, std::vector<uint64_t>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::int_t) {
            list_affectation_processor->template add<std::vector<uint64_t>, std::vector<int64_t>>(value_node);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible tuple types in affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::int_t: {
          if (rhs_tuple_content == ASTNodeDataType::bool_t) {
            list_affectation_processor->template add<std::vector<int64_t>, std::vector<bool>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::unsigned_int_t) {
            list_affectation_processor->template add<std::vector<int64_t>, std::vector<uint64_t>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::int_t) {
            list_affectation_processor->template add<std::vector<int64_t>, std::vector<int64_t>>(value_node);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible tuple types in affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::double_t: {
          if (rhs_tuple_content == ASTNodeDataType::bool_t) {
            list_affectation_processor->template add<std::vector<double>, std::vector<bool>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::unsigned_int_t) {
            list_affectation_processor->template add<std::vector<double>, std::vector<uint64_t>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::int_t) {
            list_affectation_processor->template add<std::vector<double>, std::vector<int64_t>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::double_t) {
            list_affectation_processor->template add<std::vector<double>, std::vector<double>>(value_node);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible tuple types in affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::string_t: {
          if (rhs_tuple_content == ASTNodeDataType::bool_t) {
            list_affectation_processor->template add<std::vector<std::string>, std::vector<bool>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::unsigned_int_t) {
            list_affectation_processor->template add<std::vector<std::string>, std::vector<uint64_t>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::int_t) {
            list_affectation_processor->template add<std::vector<std::string>, std::vector<int64_t>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::double_t) {
            list_affectation_processor->template add<std::vector<std::string>, std::vector<double>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::string_t) {
            list_affectation_processor->template add<std::vector<std::string>, std::vector<std::string>>(value_node);
          } else if (rhs_tuple_content == ASTNodeDataType::vector_t) {
            switch (rhs_tuple_content.dimension()) {
            case 1: {
              list_affectation_processor->template add<std::vector<std::string>, std::vector<TinyVector<1>>>(
                value_node);
              break;
            }
            case 2: {
              list_affectation_processor->template add<std::vector<std::string>, std::vector<TinyVector<2>>>(
                value_node);
              break;
            }
            case 3: {
              list_affectation_processor->template add<std::vector<std::string>, std::vector<TinyVector<3>>>(
                value_node);
              break;
            }
            }
          } else if (rhs_tuple_content == ASTNodeDataType::matrix_t) {
            Assert(rhs_tuple_content.numberOfRows() == rhs_tuple_content.numberOfColumns());
            switch (rhs_tuple_content.numberOfRows()) {
            case 1: {
              list_affectation_processor->template add<std::vector<std::string>, std::vector<TinyMatrix<1>>>(
                value_node);
              break;
            }
            case 2: {
              list_affectation_processor->template add<std::vector<std::string>, std::vector<TinyMatrix<2>>>(
                value_node);
              break;
            }
            case 3: {
              list_affectation_processor->template add<std::vector<std::string>, std::vector<TinyMatrix<3>>>(
                value_node);
              break;
            }
            }
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible tuple types in affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::vector_t: {
          switch (tuple_type.contentType().dimension()) {
          case 1: {
            if (rhs_tuple_content == ASTNodeDataType::bool_t) {
              list_affectation_processor->template add<std::vector<TinyVector<1>>, std::vector<bool>>(value_node);
            } else if (rhs_tuple_content == ASTNodeDataType::unsigned_int_t) {
              list_affectation_processor->template add<std::vector<TinyVector<1>>, std::vector<uint64_t>>(value_node);
            } else if (rhs_tuple_content == ASTNodeDataType::int_t) {
              list_affectation_processor->template add<std::vector<TinyVector<1>>, std::vector<int64_t>>(value_node);
            } else if (rhs_tuple_content == ASTNodeDataType::double_t) {
              list_affectation_processor->template add<std::vector<TinyVector<1>>, std::vector<double>>(value_node);
            } else if (rhs_tuple_content == ASTNodeDataType::vector_t) {
              Assert(rhs_tuple_content.dimension() == 1);
              list_affectation_processor->template add<std::vector<TinyVector<1>>, std::vector<TinyVector<1>>>(
                value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
          case 2: {
            if (rhs_tuple_content == ASTNodeDataType::vector_t) {
              Assert(rhs_tuple_content.dimension() == 2);
              list_affectation_processor->template add<std::vector<TinyVector<2>>, std::vector<TinyVector<2>>>(
                value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
          case 3: {
            if (rhs_tuple_content == ASTNodeDataType::vector_t) {
              Assert(rhs_tuple_content.dimension() == 3);
              list_affectation_processor->template add<std::vector<TinyVector<3>>, std::vector<TinyVector<3>>>(
                value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("invalid dimension");
          }
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::matrix_t: {
          Assert(tuple_type.contentType().numberOfRows() == tuple_type.contentType().numberOfColumns());
          switch (tuple_type.contentType().numberOfRows()) {
          case 1: {
            if (rhs_tuple_content == ASTNodeDataType::bool_t) {
              list_affectation_processor->template add<std::vector<TinyMatrix<1>>, std::vector<bool>>(value_node);
            } else if (rhs_tuple_content == ASTNodeDataType::unsigned_int_t) {
              list_affectation_processor->template add<std::vector<TinyMatrix<1>>, std::vector<uint64_t>>(value_node);
            } else if (rhs_tuple_content == ASTNodeDataType::int_t) {
              list_affectation_processor->template add<std::vector<TinyMatrix<1>>, std::vector<int64_t>>(value_node);
            } else if (rhs_tuple_content == ASTNodeDataType::double_t) {
              list_affectation_processor->template add<std::vector<TinyMatrix<1>>, std::vector<double>>(value_node);
            } else if (rhs_tuple_content == ASTNodeDataType::matrix_t) {
              Assert(rhs_tuple_content.numberOfRows() == 1);
              Assert(rhs_tuple_content.numberOfColumns() == 1);
              list_affectation_processor->template add<std::vector<TinyMatrix<1>>, std::vector<TinyMatrix<1>>>(
                value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
          case 2: {
            if (rhs_tuple_content == ASTNodeDataType::matrix_t) {
              Assert(rhs_tuple_content.numberOfRows() == 2);
              Assert(rhs_tuple_content.numberOfColumns() == 2);
              list_affectation_processor->template add<std::vector<TinyMatrix<2>>, std::vector<TinyMatrix<2>>>(
                value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
          case 3: {
            if (rhs_tuple_content == ASTNodeDataType::matrix_t) {
              Assert(rhs_tuple_content.numberOfRows() == 3);
              Assert(rhs_tuple_content.numberOfColumns() == 3);
              list_affectation_processor->template add<std::vector<TinyMatrix<3>>, std::vector<TinyMatrix<3>>>(
                value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("invalid dimension");
          }
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::type_id_t: {
          if (rhs_tuple_content == ASTNodeDataType::type_id_t) {
            Assert(rhs_tuple_content.nameOfTypeId() == tuple_type.contentType().nameOfTypeId());
            list_affectation_processor->template add<std::vector<EmbeddedData>, std::vector<EmbeddedData>>(value_node);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible tuple types in affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          throw UnexpectedError("invalid operand type for tuple affectation");
        }
          // LCOV_EXCL_STOP
        }
      } else if (node_sub_data_type.m_data_type == ASTNodeDataType::list_t) {
        switch (tuple_type.contentType()) {
        case ASTNodeDataType::bool_t: {
          list_affectation_processor->template add<std::vector<bool>, AggregateDataVariant>(value_node);
          break;
        }
        case ASTNodeDataType::unsigned_int_t: {
          list_affectation_processor->template add<std::vector<uint64_t>, AggregateDataVariant>(value_node);
          break;
        }
        case ASTNodeDataType::int_t: {
          list_affectation_processor->template add<std::vector<int64_t>, AggregateDataVariant>(value_node);
          break;
        }
        case ASTNodeDataType::double_t: {
          list_affectation_processor->template add<std::vector<double_t>, AggregateDataVariant>(value_node);
          break;
        }
        case ASTNodeDataType::string_t: {
          list_affectation_processor->template add<std::vector<std::string>, AggregateDataVariant>(value_node);
          break;
        }
        case ASTNodeDataType::vector_t: {
          switch (tuple_type.contentType().dimension()) {
          case 1: {
            list_affectation_processor->template add<std::vector<TinyVector<1>>, AggregateDataVariant>(value_node);
            break;
          }
          case 2: {
            list_affectation_processor->template add<std::vector<TinyVector<2>>, AggregateDataVariant>(value_node);
            break;
          }
          case 3: {
            list_affectation_processor->template add<std::vector<TinyVector<3>>, AggregateDataVariant>(value_node);
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("invalid vector dimension");
          }
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::matrix_t: {
          Assert(tuple_type.contentType().numberOfRows() == tuple_type.contentType().numberOfColumns());
          switch (tuple_type.contentType().numberOfRows()) {
          case 1: {
            list_affectation_processor->template add<std::vector<TinyMatrix<1>>, AggregateDataVariant>(value_node);
            break;
          }
          case 2: {
            list_affectation_processor->template add<std::vector<TinyMatrix<2>>, AggregateDataVariant>(value_node);
            break;
          }
          case 3: {
            list_affectation_processor->template add<std::vector<TinyMatrix<3>>, AggregateDataVariant>(value_node);
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("invalid matrix dimension");
          }
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::type_id_t: {
          list_affectation_processor->template add<std::vector<EmbeddedData>, AggregateDataVariant>(value_node);
          break;
        }
          // LCOV_EXCL_START
        default: {
          throw UnexpectedError("invalid operand type for tuple affectation");
        }
          // LCOV_EXCL_STOP
        }
      } else {
        const ASTNodeDataType& rhs_type = node_sub_data_type.m_data_type;
        switch (tuple_type.contentType()) {
        case ASTNodeDataType::bool_t: {
          if (rhs_type == ASTNodeDataType::bool_t) {
            list_affectation_processor->template add<std::vector<bool>, bool>(value_node);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible tuple types in affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::unsigned_int_t: {
          if (rhs_type == ASTNodeDataType::bool_t) {
            list_affectation_processor->template add<std::vector<uint64_t>, bool>(value_node);
          } else if (rhs_type == ASTNodeDataType::unsigned_int_t) {
            list_affectation_processor->template add<std::vector<uint64_t>, uint64_t>(value_node);
          } else if (rhs_type == ASTNodeDataType::int_t) {
            list_affectation_processor->template add<std::vector<uint64_t>, int64_t>(value_node);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible tuple types in affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::int_t: {
          if (rhs_type == ASTNodeDataType::bool_t) {
            list_affectation_processor->template add<std::vector<int64_t>, bool>(value_node);
          } else if (rhs_type == ASTNodeDataType::unsigned_int_t) {
            list_affectation_processor->template add<std::vector<int64_t>, uint64_t>(value_node);
          } else if (rhs_type == ASTNodeDataType::int_t) {
            list_affectation_processor->template add<std::vector<int64_t>, int64_t>(value_node);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible tuple types in affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::double_t: {
          if (rhs_type == ASTNodeDataType::bool_t) {
            list_affectation_processor->template add<std::vector<double>, bool>(value_node);
          } else if (rhs_type == ASTNodeDataType::unsigned_int_t) {
            list_affectation_processor->template add<std::vector<double>, uint64_t>(value_node);
          } else if (rhs_type == ASTNodeDataType::int_t) {
            list_affectation_processor->template add<std::vector<double>, int64_t>(value_node);
          } else if (rhs_type == ASTNodeDataType::double_t) {
            list_affectation_processor->template add<std::vector<double>, double>(value_node);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible tuple types in affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::string_t: {
          if (rhs_type == ASTNodeDataType::bool_t) {
            list_affectation_processor->template add<std::vector<std::string>, bool>(value_node);
          } else if (rhs_type == ASTNodeDataType::unsigned_int_t) {
            list_affectation_processor->template add<std::vector<std::string>, uint64_t>(value_node);
          } else if (rhs_type == ASTNodeDataType::int_t) {
            list_affectation_processor->template add<std::vector<std::string>, int64_t>(value_node);
          } else if (rhs_type == ASTNodeDataType::double_t) {
            list_affectation_processor->template add<std::vector<std::string>, double>(value_node);
          } else if (rhs_type == ASTNodeDataType::string_t) {
            list_affectation_processor->template add<std::vector<std::string>, std::string>(value_node);
          } else if (rhs_type == ASTNodeDataType::vector_t) {
            switch (rhs_type.dimension()) {
            case 1: {
              list_affectation_processor->template add<std::vector<std::string>, TinyVector<1>>(value_node);
              break;
            }
            case 2: {
              list_affectation_processor->template add<std::vector<std::string>, TinyVector<2>>(value_node);
              break;
            }
            case 3: {
              list_affectation_processor->template add<std::vector<std::string>, TinyVector<3>>(value_node);
              break;
            }
            }
          } else if (rhs_type == ASTNodeDataType::matrix_t) {
            Assert(rhs_type.numberOfRows() == rhs_type.numberOfColumns());
            switch (rhs_type.numberOfRows()) {
            case 1: {
              list_affectation_processor->template add<std::vector<std::string>, TinyMatrix<1>>(value_node);
              break;
            }
            case 2: {
              list_affectation_processor->template add<std::vector<std::string>, TinyMatrix<2>>(value_node);
              break;
            }
            case 3: {
              list_affectation_processor->template add<std::vector<std::string>, TinyMatrix<3>>(value_node);
              break;
            }
            }
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible rhs type in tuple affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::vector_t: {
          switch (tuple_type.contentType().dimension()) {
          case 1: {
            if (rhs_type == ASTNodeDataType::bool_t) {
              list_affectation_processor->template add<std::vector<TinyVector<1>>, bool>(value_node);
            } else if (rhs_type == ASTNodeDataType::unsigned_int_t) {
              list_affectation_processor->template add<std::vector<TinyVector<1>>, uint64_t>(value_node);
            } else if (rhs_type == ASTNodeDataType::int_t) {
              list_affectation_processor->template add<std::vector<TinyVector<1>>, int64_t>(value_node);
            } else if (rhs_type == ASTNodeDataType::double_t) {
              list_affectation_processor->template add<std::vector<TinyVector<1>>, double>(value_node);
            } else if (rhs_type == ASTNodeDataType::vector_t) {
              Assert(rhs_type.dimension() == 1);
              list_affectation_processor->template add<std::vector<TinyVector<1>>, TinyVector<1>>(value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
          case 2: {
            if (rhs_type == ASTNodeDataType::vector_t) {
              Assert(rhs_type.dimension() == 2);
              list_affectation_processor->template add<std::vector<TinyVector<2>>, TinyVector<2>>(value_node);
            } else if (rhs_type == ASTNodeDataType::int_t) {
              list_affectation_processor->template add<std::vector<TinyVector<2>>, int64_t>(value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
          case 3: {
            if (rhs_type == ASTNodeDataType::vector_t) {
              Assert(rhs_type.dimension() == 3);
              list_affectation_processor->template add<std::vector<TinyVector<3>>, TinyVector<3>>(value_node);
            } else if (rhs_type == ASTNodeDataType::int_t) {
              list_affectation_processor->template add<std::vector<TinyVector<3>>, int64_t>(value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("invalid vector dimension for tuple affectation");
          }
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::matrix_t: {
          Assert(tuple_type.contentType().numberOfRows() == tuple_type.contentType().numberOfColumns());
          switch (tuple_type.contentType().numberOfRows()) {
          case 1: {
            if (rhs_type == ASTNodeDataType::bool_t) {
              list_affectation_processor->template add<std::vector<TinyMatrix<1>>, bool>(value_node);
            } else if (rhs_type == ASTNodeDataType::unsigned_int_t) {
              list_affectation_processor->template add<std::vector<TinyMatrix<1>>, uint64_t>(value_node);
            } else if (rhs_type == ASTNodeDataType::int_t) {
              list_affectation_processor->template add<std::vector<TinyMatrix<1>>, int64_t>(value_node);
            } else if (rhs_type == ASTNodeDataType::double_t) {
              list_affectation_processor->template add<std::vector<TinyMatrix<1>>, double>(value_node);
            } else if (rhs_type == ASTNodeDataType::matrix_t) {
              Assert(rhs_type.numberOfRows() == 1);
              Assert(rhs_type.numberOfColumns() == 1);
              list_affectation_processor->template add<std::vector<TinyMatrix<1>>, TinyMatrix<1>>(value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
          case 2: {
            if (rhs_type == ASTNodeDataType::matrix_t) {
              Assert(rhs_type.numberOfRows() == 2);
              Assert(rhs_type.numberOfColumns() == 2);
              list_affectation_processor->template add<std::vector<TinyMatrix<2>>, TinyMatrix<2>>(value_node);
            } else if (rhs_type == ASTNodeDataType::int_t) {
              list_affectation_processor->template add<std::vector<TinyMatrix<2>>, int64_t>(value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
          case 3: {
            if (rhs_type == ASTNodeDataType::matrix_t) {
              Assert(rhs_type.numberOfRows() == 3);
              Assert(rhs_type.numberOfColumns() == 3);
              list_affectation_processor->template add<std::vector<TinyMatrix<3>>, TinyMatrix<3>>(value_node);
            } else if (rhs_type == ASTNodeDataType::int_t) {
              list_affectation_processor->template add<std::vector<TinyMatrix<3>>, int64_t>(value_node);
            } else {
              // LCOV_EXCL_START
              throw UnexpectedError("incompatible tuple types in affectation");
              // LCOV_EXCL_STOP
            }
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("invalid vector dimension for tuple affectation");
          }
            // LCOV_EXCL_STOP
          }
          break;
        }
        case ASTNodeDataType::type_id_t: {
          if (rhs_type == ASTNodeDataType::type_id_t) {
            Assert(rhs_type.nameOfTypeId() == tuple_type.contentType().nameOfTypeId());
            list_affectation_processor->template add<std::vector<EmbeddedData>, EmbeddedData>(value_node);
          } else {
            // LCOV_EXCL_START
            throw UnexpectedError("incompatible tuple types in affectation");
            // LCOV_EXCL_STOP
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          throw UnexpectedError("invalid operand type for tuple affectation");
        }
          // LCOV_EXCL_STOP
        }
      }
    } else {
      static_assert(std::is_same_v<OperatorT, language::eq_op>, "unexpected operator type");
    }
  };

  auto add_affectation_processor_for_embedded_data = [&](const ASTNodeSubDataType& node_sub_data_type) {
    if constexpr (std::is_same_v<OperatorT, language::eq_op>) {
      switch (node_sub_data_type.m_data_type) {
      case ASTNodeDataType::type_id_t: {
        list_affectation_processor->template add<EmbeddedData, EmbeddedData>(value_node);
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("invalid operand type for embedded data affectation");
      }
        // LCOV_EXCL_STOP
      }
    } else {
      static_assert(std::is_same_v<OperatorT, language::eq_op>, "unexpected operator type");
    }
  };

  auto add_affectation_processor_for_string_data = [&](const ASTNodeSubDataType& node_sub_data_type) {
    if constexpr (std::is_same_v<OperatorT, language::eq_op>) {
      switch (node_sub_data_type.m_data_type) {
      case ASTNodeDataType::bool_t: {
        list_affectation_processor->template add<std::string, bool>(value_node);
        break;
      }
      case ASTNodeDataType::unsigned_int_t: {
        list_affectation_processor->template add<std::string, uint64_t>(value_node);
        break;
      }
      case ASTNodeDataType::int_t: {
        list_affectation_processor->template add<std::string, int64_t>(value_node);
        break;
      }
      case ASTNodeDataType::double_t: {
        list_affectation_processor->template add<std::string, double>(value_node);
        break;
      }
      case ASTNodeDataType::string_t: {
        list_affectation_processor->template add<std::string, std::string>(value_node);
        break;
      }
      case ASTNodeDataType::vector_t: {
        switch (node_sub_data_type.m_data_type.dimension()) {
        case 1: {
          list_affectation_processor->template add<std::string, TinyVector<1>>(value_node);
          break;
        }
        case 2: {
          list_affectation_processor->template add<std::string, TinyVector<2>>(value_node);
          break;
        }
        case 3: {
          list_affectation_processor->template add<std::string, TinyVector<3>>(value_node);
          break;
        }
          // LCOV_EXCL_START
        default: {
          throw UnexpectedError("invalid vector dimension");
        }
          // LCOV_EXCL_STOP
        }
        break;
      }
      case ASTNodeDataType::matrix_t: {
        Assert(node_sub_data_type.m_data_type.numberOfRows() == node_sub_data_type.m_data_type.numberOfColumns());
        switch (node_sub_data_type.m_data_type.numberOfRows()) {
        case 1: {
          list_affectation_processor->template add<std::string, TinyMatrix<1>>(value_node);
          break;
        }
        case 2: {
          list_affectation_processor->template add<std::string, TinyMatrix<2>>(value_node);
          break;
        }
        case 3: {
          list_affectation_processor->template add<std::string, TinyMatrix<3>>(value_node);
          break;
        }
          // LCOV_EXCL_START
        default: {
          throw UnexpectedError("invalid vector dimension");
        }
          // LCOV_EXCL_STOP
        }
        break;
      }

        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("invalid operand type for string affectation");
      }
        // LCOV_EXCL_STOP
      }
    } else {
      static_assert(std::is_same_v<OperatorT, language::eq_op>, "unexpected operator type");
    }
  };

  auto add_affectation_processor_for_value = [&](const ASTNodeDataType& value_type,
                                                 const ASTNodeSubDataType& node_sub_data_type) {
    switch (value_type) {
    case ASTNodeDataType::bool_t: {
      add_affectation_processor_for_data.template operator()<bool>(node_sub_data_type);
      break;
    }
    case ASTNodeDataType::unsigned_int_t: {
      add_affectation_processor_for_data.template operator()<uint64_t>(node_sub_data_type);
      break;
    }
    case ASTNodeDataType::int_t: {
      add_affectation_processor_for_data.template operator()<int64_t>(node_sub_data_type);
      break;
    }
    case ASTNodeDataType::double_t: {
      add_affectation_processor_for_data.template operator()<double>(node_sub_data_type);
      break;
    }
    case ASTNodeDataType::type_id_t: {
      add_affectation_processor_for_embedded_data(node_sub_data_type);
      break;
    }
    case ASTNodeDataType::vector_t: {
      switch (value_type.dimension()) {
      case 1: {
        add_affectation_processor_for_vector_data.template operator()<TinyVector<1>>(node_sub_data_type);
        break;
      }
      case 2: {
        add_affectation_processor_for_vector_data.template operator()<TinyVector<2>>(node_sub_data_type);
        break;
      }
      case 3: {
        add_affectation_processor_for_vector_data.template operator()<TinyVector<3>>(node_sub_data_type);
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("invalid dimension");
      }
        // LCOV_EXCL_STOP
      }
      break;
    }
    case ASTNodeDataType::matrix_t: {
      Assert(value_type.numberOfRows() == value_type.numberOfColumns());
      switch (value_type.numberOfRows()) {
      case 1: {
        add_affectation_processor_for_matrix_data.template operator()<TinyMatrix<1>>(node_sub_data_type);
        break;
      }
      case 2: {
        add_affectation_processor_for_matrix_data.template operator()<TinyMatrix<2>>(node_sub_data_type);
        break;
      }
      case 3: {
        add_affectation_processor_for_matrix_data.template operator()<TinyMatrix<3>>(node_sub_data_type);
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("invalid dimension");
      }
        // LCOV_EXCL_STOP
      }
      break;
    }
    case ASTNodeDataType::string_t: {
      add_affectation_processor_for_string_data(node_sub_data_type);
      break;
    }
    case ASTNodeDataType::tuple_t: {
      add_affectation_processor_for_tuple_data(value_type, node_sub_data_type);
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("unexpected error: undefined value type for tuple affectation");
    }
      // LCOV_EXCL_STOP
    }
  };

  ASTNodeNaturalConversionChecker<AllowRToR1Conversion>(rhs_node_sub_data_type, value_node.m_data_type);

  const std::string affectation_name =
    affectationMangler<language::eq_op>(value_node.m_data_type, rhs_node_sub_data_type.m_data_type);

  const auto& optional_processor_builder =
    OperatorRepository::instance().getAffectationProcessorBuilder(affectation_name);

  if (optional_processor_builder.has_value()) {
    add_affectation_processor_for_value(value_node.m_data_type, rhs_node_sub_data_type);
  } else {
    // LCOV_EXCL_START
    std::ostringstream error_message;
    error_message << "undefined affectation type: " << rang::fgB::red << affectation_name << rang::fg::reset;
    throw UnexpectedError(error_message.str());
    // LCOV_EXCL_STOP
  }
}

template <typename OperatorT>
void
ASTNodeListAffectationExpressionBuilder::_buildListAffectationProcessor()
{
  Assert(m_node.children[1]->is_type<language::expression_list>() or
         m_node.children[1]->is_type<language::function_evaluation>());

  ASTNodeDataTypeFlattener::FlattenedDataTypeList flattened_rhs_data_type_list;
  ASTNodeDataTypeFlattener{*m_node.children[1], flattened_rhs_data_type_list};

  ASTNode& name_list_node = *m_node.children[0];

  if (name_list_node.children.size() != flattened_rhs_data_type_list.size()) {
    throw ParseError("incompatible list sizes in affectation", std::vector{m_node.children[0]->begin()});
  }

  using ListAffectationProcessorT = ListAffectationProcessor<OperatorT>;

  std::unique_ptr list_affectation_processor = std::make_unique<ListAffectationProcessorT>(m_node);

  for (size_t i = 0; i < name_list_node.children.size(); ++i) {
    ASTNode& name_node = *name_list_node.children[i];
    this->_buildAffectationProcessor(flattened_rhs_data_type_list[i], name_node, list_affectation_processor);
  }

  m_node.m_node_processor = std::move(list_affectation_processor);
}

ASTNodeListAffectationExpressionBuilder::ASTNodeListAffectationExpressionBuilder(ASTNode& node) : m_node(node)
{
  if (node.children[1]->is_type<language::expression_list>() or
      node.children[1]->is_type<language::function_evaluation>() or
      (node.children[1]->m_data_type == ASTNodeDataType::tuple_t)) {
    if (node.is_type<language::eq_op>()) {
      if (node.children[1]->m_data_type == ASTNodeDataType::tuple_t) {
        this->_buildListAffectationFromTupleProcessor<language::eq_op>();
      } else {
        this->_buildListAffectationProcessor<language::eq_op>();
      }
    } else {
      throw ParseError("undefined affectation operator for lists", std::vector{node.begin()});
    }
  } else {
    throw ParseError("invalid right hand side in list affectation", std::vector{node.children[1]->begin()});
  }
}
