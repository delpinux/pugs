#ifndef AST_NODE_FUNCTION_EVALUATION_EXPRESSION_BUILDER_HPP
#define AST_NODE_FUNCTION_EVALUATION_EXPRESSION_BUILDER_HPP

#include <language/ast/ASTNode.hpp>

struct ASTNodeFunctionEvaluationExpressionBuilder
{
  ASTNodeFunctionEvaluationExpressionBuilder(ASTNode& node);
};

#endif   // AST_NODE_FUNCTION_EVALUATION_EXPRESSION_BUILDER_HPP
