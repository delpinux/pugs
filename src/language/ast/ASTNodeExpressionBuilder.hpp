#ifndef AST_NODE_EXPRESSION_BUILDER_HPP
#define AST_NODE_EXPRESSION_BUILDER_HPP

#include <language/ast/ASTNode.hpp>

class ASTNodeExpressionBuilder
{
 private:
  void _buildExpression(ASTNode& n);
  void _checkIsPureFunction(const ASTNode& n) const;

 public:
  ASTNodeExpressionBuilder(ASTNode& n);
};

#endif   // AST_NODE_EXPRESSION_BUILDER_HPP
