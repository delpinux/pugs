#include <language/ast/ASTBuilder.hpp>

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>
#include <language/utils/SymbolTable.hpp>
#include <utils/PugsAssert.hpp>

#include <pegtl/contrib/parse_tree.hpp>

struct ASTBuilder::rearrange_binary_op : TAO_PEGTL_NAMESPACE::parse_tree::apply<ASTBuilder::rearrange_binary_op>
{
  template <typename... States>
  static void
  transform(std::unique_ptr<ASTNode>& n, States&&... st)
  {
    if (n->children.size() == 1) {
      n = std::move(n->children.back());
    } else {
      // First we rearrange tree
      {
        auto& children = n->children;
        auto rhs       = std::move(children.back());
        children.pop_back();
        auto op = std::move(children.back());
        children.pop_back();
        op->children.emplace_back(std::move(n));
        op->children.emplace_back(std::move(rhs));
        n = std::move(op);
        transform(n->children.front(), st...);
      }
      // Then we eventually simplify operations
      {
        if (n->is_type<language::minus_op>()) {
          Assert(n->children.size() == 2);
          auto& rhs = n->children[1];
          if (rhs->is_type<language::unary_minus>()) {
            n->set_type<language::plus_op>();
            rhs = std::move(rhs->children[0]);
          }
        } else if (n->is_type<language::plus_op>()) {
          Assert(n->children.size() == 2);
          auto& rhs = n->children[1];
          if (rhs->is_type<language::unary_minus>()) {
            n->set_type<language::minus_op>();
            rhs = std::move(rhs->children[0]);
          }
        }
      }
    }
  }
};

struct ASTBuilder::rearrange_pre_unary_op : TAO_PEGTL_NAMESPACE::parse_tree::apply<ASTBuilder::rearrange_pre_unary_op>
{
  template <typename... States>
  static void
  transform(std::unique_ptr<ASTNode>& n, States&&...)
  {
    if (n->children.size() == 1) {
      n = std::move(n->children.back());
    } else {
      Assert(n->children.size() == 2);
      auto op         = std::move(n->children[0]);
      auto expression = std::move(n->children[1]);
      op->children.emplace_back(std::move(expression));
      n = std::move(op);
    }

    // Eventually simplify expressions
    if (n->is_type<language::unary_minus>() or n->is_type<language::unary_not>()) {
      if ((n->children[0]->children.size() == 1) and (n->children[0]->type == n->type)) {
        n = std::move(n->children[0]->children[0]);
      }
    }
  }
};

struct ASTBuilder::rearrange_post_unary_op : TAO_PEGTL_NAMESPACE::parse_tree::apply<ASTBuilder::rearrange_post_unary_op>
{
  template <typename... States>
  static void
  transform(std::unique_ptr<ASTNode>& n, States&&... st)
  {
    if (n->children.size() == 1) {
      n = std::move(n->children.back());
    } else {
      if (n->children.back()->is_type<language::subscript_expression>()) {
        auto op = std::move(n->children.back());
        n->children.pop_back();
        op->children.emplace_back(std::move(n));

        const size_t child_nb = op->children.size();
        for (size_t i = 1; i < op->children.size(); ++i) {
          std::swap(op->children[child_nb - i], op->children[child_nb - i - 1]);
        }

        n = std::move(op);

        transform(n->children[0], st...);
      } else {
        auto op = std::move(n->children.back());
        n->children.pop_back();

        auto expression = std::move(n);
        op->children.emplace_back(std::move(expression));
        n = std::move(op);
        transform(n->children[0], st...);
      }
    }
  }
};

struct ASTBuilder::simplify_type_expression
  : TAO_PEGTL_NAMESPACE::parse_tree::apply<ASTBuilder::simplify_type_expression>
{
  template <typename... States>
  static void
  transform(std::unique_ptr<ASTNode>& n, States&&... st)
  {
    if ((n->children.size() == 1) and (n->is_type<language::type_expression>())) {
      n = std::move(n->children.back());
      transform(n, st...);
    }
  }
};

struct ASTBuilder::simplify_node_list : TAO_PEGTL_NAMESPACE::parse_tree::apply<ASTBuilder::simplify_node_list>
{
  template <typename... States>
  static void
  transform(std::unique_ptr<ASTNode>& n, States&&... st)
  {
    if (n->is_type<language::name_list>() or n->is_type<language::lvalue_list>() or
        n->is_type<language::function_argument_list>()) {
      if (n->children.size() == 1) {
        n = std::move(n->children.back());
        transform(n, st...);
      }
    }
  }
};

struct ASTBuilder::simplify_statement_block
  : TAO_PEGTL_NAMESPACE::parse_tree::apply<ASTBuilder::simplify_statement_block>
{
  template <typename... States>
  static void
  transform(std::unique_ptr<ASTNode>& n, States&&... st)
  {
    if ((n->is_type<language::statement_block>() or n->is_type<language::block>()) and (n->children.size() == 1)) {
      if (not n->children[0]->is_type<language::var_declaration>()) {
        n = std::move(n->children.back());
        transform(n, st...);
      } else {
        n->set_type<language::block>();
      }
    }
  }
};

struct ASTBuilder::simplify_for_statement_block
  : TAO_PEGTL_NAMESPACE::parse_tree::apply<ASTBuilder::simplify_for_statement_block>
{
  template <typename... States>
  static void
  transform(std::unique_ptr<ASTNode>& n, States&&... st)
  {
    if ((n->is_type<language::for_statement_block>() or n->is_type<language::block>()) and (n->children.size() == 1)) {
      n = std::move(n->children.back());
      transform(n, st...);
    }
  }
};

struct ASTBuilder::simplify_for_init : TAO_PEGTL_NAMESPACE::parse_tree::apply<ASTBuilder::simplify_for_init>
{
  template <typename... States>
  static void
  transform(std::unique_ptr<ASTNode>& n, States&&...)
  {
    Assert(n->children.size() <= 1);
    if (n->children.size() == 1) {
      n = std::move(n->children.back());
    }
  }
};

struct ASTBuilder::simplify_for_test : TAO_PEGTL_NAMESPACE::parse_tree::apply<ASTBuilder::simplify_for_test>
{
  template <typename... States>
  static void
  transform(std::unique_ptr<ASTNode>& n, States&&...)
  {
    Assert(n->children.size() <= 1);
    if (n->children.size() == 1) {
      n = std::move(n->children.back());
    }
  }
};

struct ASTBuilder::simplify_for_post : TAO_PEGTL_NAMESPACE::parse_tree::apply<ASTBuilder::simplify_for_post>
{
  template <typename... States>
  static void
  transform(std::unique_ptr<ASTNode>& n, States&&...)
  {
    Assert(n->children.size() <= 1);
    if (n->children.size() == 1) {
      n = std::move(n->children.back());
    }
  }
};

template <typename Rule>
using selector = TAO_PEGTL_NAMESPACE::parse_tree::selector<
  Rule,
  TAO_PEGTL_NAMESPACE::parse_tree::store_content::on<language::import_instruction,
                                                     language::module_name,
                                                     language::true_kw,
                                                     language::false_kw,
                                                     language::integer,
                                                     language::real,
                                                     language::literal,
                                                     language::name,
                                                     language::B_set,
                                                     language::N_set,
                                                     language::Z_set,
                                                     language::R_set,
                                                     language::empty_set,
                                                     language::type_name_id,
                                                     language::tuple_type_specifier,
                                                     language::inner_expression_list,
                                                     language::vector_expression,
                                                     language::vector_type,
                                                     language::matrix_expression,
                                                     language::row_expression,
                                                     language::matrix_type,
                                                     language::string_type,
                                                     language::var_declaration,
                                                     language::fct_declaration,
                                                     language::type_mapping,
                                                     language::function_definition,
                                                     language::expression_list,
                                                     language::if_statement,
                                                     language::do_while_statement,
                                                     language::while_statement,
                                                     language::for_statement,
                                                     language::function_evaluation,
                                                     language::break_kw,
                                                     language::continue_kw,
                                                     language::unary_minus,
                                                     language::unary_not,
                                                     language::unary_plusplus,
                                                     language::unary_minusminus,
                                                     language::post_minusminus,
                                                     language::post_plusplus,
                                                     language::subscript_expression>,
  ASTBuilder::rearrange_binary_op::on<language::logical_or,
                                      language::logical_and,
                                      language::bitwise_xor,
                                      language::equality,
                                      language::compare,
                                      language::sum,
                                      language::shift,
                                      language::product,
                                      language::affectation,
                                      language::expression>,
  ASTBuilder::simplify_type_expression::on<language::type_expression>,
  ASTBuilder::rearrange_pre_unary_op::on<language::prefix_expression>,
  ASTBuilder::rearrange_post_unary_op::on<language::postfix_expression>,
  TAO_PEGTL_NAMESPACE::parse_tree::remove_content::on<language::plus_op,
                                                      language::minus_op,
                                                      language::shift_left_op,
                                                      language::shift_right_op,
                                                      language::multiply_op,
                                                      language::divide_op,
                                                      language::lesser_op,
                                                      language::lesser_or_eq_op,
                                                      language::greater_op,
                                                      language::greater_or_eq_op,
                                                      language::eqeq_op,
                                                      language::not_eq_op,
                                                      language::and_op,
                                                      language::or_op,
                                                      language::xor_op,
                                                      language::eq_op,
                                                      language::multiplyeq_op,
                                                      language::divideeq_op,
                                                      language::pluseq_op,
                                                      language::minuseq_op>,
  ASTBuilder::simplify_for_statement_block::on<language::for_statement_block>,
  TAO_PEGTL_NAMESPACE::parse_tree::discard_empty::on<language::ignored, language::semicol, language::block>,
  ASTBuilder::simplify_node_list::on<language::name_list, language::lvalue_list, language::function_argument_list>,
  ASTBuilder::simplify_statement_block::on<language::statement_block>,
  ASTBuilder::simplify_for_init::on<language::for_init>,
  ASTBuilder::simplify_for_test::on<language::for_test>,
  ASTBuilder::simplify_for_post::on<language::for_post>>;

template <typename InputT>
std::unique_ptr<ASTNode>
ASTBuilder::build(InputT& input)
{
  std::unique_ptr root_node =
    TAO_PEGTL_NAMESPACE::parse_tree::parse<language::grammar, ASTNode, selector, TAO_PEGTL_NAMESPACE::nothing,
                                           language::errors>(input);

  // build initial symbol tables
  std::shared_ptr symbol_table = std::make_shared<SymbolTable>();

  root_node->m_symbol_table = symbol_table;

  return root_node;
}

template std::unique_ptr<ASTNode> ASTBuilder::build(TAO_PEGTL_NAMESPACE::string_input<>& input);
