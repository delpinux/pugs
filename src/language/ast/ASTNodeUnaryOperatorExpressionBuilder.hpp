#ifndef AST_NODE_UNARY_OPERATOR_EXPRESSION_BUILDER_HPP
#define AST_NODE_UNARY_OPERATOR_EXPRESSION_BUILDER_HPP

#include <language/ast/ASTNode.hpp>

struct ASTNodeUnaryOperatorExpressionBuilder
{
  ASTNodeUnaryOperatorExpressionBuilder(ASTNode& node);
};

#endif   // AST_NODE_UNARY_OPERATOR_EXPRESSION_BUILDER_HPP
