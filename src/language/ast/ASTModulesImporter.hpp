#ifndef AST_MODULES_IMPORTER_HPP
#define AST_MODULES_IMPORTER_HPP

#include <language/ast/ASTNode.hpp>
#include <language/modules/ModuleRepository.hpp>

#include <set>
#include <string>

class SymbolTable;

class ASTModulesImporter
{
  std::set<std::string> m_imported_modules;
  SymbolTable& m_symbol_table;

  void _importModule(ASTNode& import_node);
  void _importAllModules(ASTNode& node);

 public:
  const ModuleRepository&
  moduleRepository() const
  {
    return ModuleRepository::getInstance();
  }

  ASTModulesImporter(ASTNode& root_node);

  ASTModulesImporter(const ASTModulesImporter&) = delete;

  ~ASTModulesImporter() = default;
};

#endif   // AST_MODULES_IMPORTER_HPP
