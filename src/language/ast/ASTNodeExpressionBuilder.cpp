#include <language/ast/ASTNodeExpressionBuilder.hpp>

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNodeAffectationExpressionBuilder.hpp>
#include <language/ast/ASTNodeArraySubscriptExpressionBuilder.hpp>
#include <language/ast/ASTNodeBinaryOperatorExpressionBuilder.hpp>
#include <language/ast/ASTNodeFunctionEvaluationExpressionBuilder.hpp>
#include <language/ast/ASTNodeIncDecExpressionBuilder.hpp>
#include <language/ast/ASTNodeListAffectationExpressionBuilder.hpp>
#include <language/ast/ASTNodeUnaryOperatorExpressionBuilder.hpp>
#include <language/node_processor/ASTNodeExpressionListProcessor.hpp>
#include <language/node_processor/ASTNodeListProcessor.hpp>
#include <language/node_processor/BreakProcessor.hpp>
#include <language/node_processor/ContinueProcessor.hpp>
#include <language/node_processor/DoWhileProcessor.hpp>
#include <language/node_processor/FakeProcessor.hpp>
#include <language/node_processor/ForProcessor.hpp>
#include <language/node_processor/IfProcessor.hpp>
#include <language/node_processor/InnerListToVectorProcessor.hpp>
#include <language/node_processor/LocalNameProcessor.hpp>
#include <language/node_processor/NameProcessor.hpp>
#include <language/node_processor/TinyMatrixExpressionProcessor.hpp>
#include <language/node_processor/TinyVectorExpressionProcessor.hpp>
#include <language/node_processor/ValueProcessor.hpp>
#include <language/node_processor/WhileProcessor.hpp>
#include <language/utils/ParseError.hpp>

void
ASTNodeExpressionBuilder::_checkIsPureFunction(const ASTNode& node) const
{
  if (node.is_type<language::unary_plusplus>() or node.is_type<language::unary_minusminus>() or
      node.is_type<language::post_plusplus>() or node.is_type<language::post_minusminus>()) {
    throw ParseError("invalid function definition. Function data must be constant!", node.begin());
  }

  for (auto&& child : node.children) {
    this->_checkIsPureFunction(*child);
  }
}

void
ASTNodeExpressionBuilder::_buildExpression(ASTNode& n)
{
  if (n.is_type<language::block>()) {
    n.m_node_processor = std::make_unique<ASTNodeListProcessor>(n);
  } else if ((n.is_type<language::eq_op>() or n.is_type<language::multiplyeq_op>() or
              n.is_type<language::divideeq_op>() or n.is_type<language::pluseq_op>() or
              n.is_type<language::minuseq_op>())) {
    if (n.children[0]->is_type<language::name_list>() or n.children[0]->is_type<language::lvalue_list>()) {
      ASTNodeListAffectationExpressionBuilder{n};
    } else {
      ASTNodeAffectationExpressionBuilder{n};
    }

  } else if (n.is_type<language::inner_expression_list>()) {
    n.m_node_processor = std::make_unique<InnerListToVectorProcessor>(n);

  } else if (n.is_type<language::function_definition>()) {
    this->_checkIsPureFunction(n);

    n.m_node_processor = std::make_unique<FakeProcessor>();

  } else if (n.is_type<language::function_evaluation>()) {
    ASTNodeFunctionEvaluationExpressionBuilder{n};

  } else if (n.is_type<language::subscript_expression>()) {
    ASTNodeArraySubscriptExpressionBuilder{n};

  } else if (n.is_type<language::real>()) {
    n.m_node_processor = std::make_unique<ValueProcessor>(n);
  } else if (n.is_type<language::integer>()) {
    n.m_node_processor = std::make_unique<ValueProcessor>(n);
  } else if (n.is_type<language::literal>()) {
    n.m_node_processor = std::make_unique<ValueProcessor>(n);
  } else if (n.is_type<language::true_kw>()) {
    n.m_node_processor = std::make_unique<ValueProcessor>(n);
  } else if (n.is_type<language::false_kw>()) {
    n.m_node_processor = std::make_unique<ValueProcessor>(n);

  } else if (n.is_type<language::vector_expression>()) {
    Assert(n.m_data_type == ASTNodeDataType::vector_t);
    switch (n.m_data_type.dimension()) {
    case 1: {
      n.m_node_processor = std::make_unique<TinyVectorExpressionProcessor<1>>(n);
      break;
    }
    case 2: {
      n.m_node_processor = std::make_unique<TinyVectorExpressionProcessor<2>>(n);
      break;
    }
    case 3: {
      n.m_node_processor = std::make_unique<TinyVectorExpressionProcessor<3>>(n);
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("invalid vector dimension");
    }
      // LCOV_EXCL_STOP
    }
  } else if (n.is_type<language::matrix_expression>()) {
    Assert(n.m_data_type == ASTNodeDataType::matrix_t);
    Assert(n.m_data_type.numberOfRows() == n.m_data_type.numberOfColumns());

    switch (n.m_data_type.numberOfRows()) {
    case 1: {
      n.m_node_processor = std::make_unique<TinyMatrixExpressionProcessor<1, 1>>(n);
      break;
    }
    case 2: {
      n.m_node_processor = std::make_unique<TinyMatrixExpressionProcessor<2, 2>>(n);
      break;
    }
    case 3: {
      n.m_node_processor = std::make_unique<TinyMatrixExpressionProcessor<3, 3>>(n);
      break;
    }
      // LCOV_EXCL_START
    default: {
      throw UnexpectedError("invalid matrix dimension");
    }
      // LCOV_EXCL_STOP
    }

  } else if ((n.is_type<language::row_expression>())) {
    n.m_node_processor = std::make_unique<FakeProcessor>();

  } else if ((n.is_type<language::function_argument_list>()) or (n.is_type<language::expression_list>())) {
    n.m_node_processor = std::make_unique<ASTNodeExpressionListProcessor>(n);

  } else if (n.is_type<language::name_list>() or n.is_type<language::lvalue_list>()) {
    n.m_node_processor = std::make_unique<FakeProcessor>();

  } else if (n.is_type<language::name>()) {
    if (n.m_data_type == ASTNodeDataType::builtin_function_t) {
      n.m_node_processor = std::make_unique<FakeProcessor>();
    } else {
      // Dealing with contexts
      auto [i_symbol, success] = n.m_symbol_table->find(n.string(), n.begin());
      Assert(success, "could not find symbol");
      if (i_symbol->attributes().hasLocalContext()) {
        n.m_node_processor = std::make_unique<LocalNameProcessor>(n);
      } else {
        n.m_node_processor = std::make_unique<NameProcessor>(n);
      }
    }
  } else if (n.is_type<language::unary_minus>() or n.is_type<language::unary_not>()) {
    ASTNodeUnaryOperatorExpressionBuilder{n};

  } else if (n.is_type<language::unary_minusminus>() or n.is_type<language::unary_plusplus>() or
             n.is_type<language::post_minusminus>() or n.is_type<language::post_plusplus>()) {
    ASTNodeIncDecExpressionBuilder{n};

  } else if (n.is_type<language::multiply_op>() or n.is_type<language::divide_op>() or n.is_type<language::plus_op>() or
             n.is_type<language::minus_op>() or n.is_type<language::or_op>() or n.is_type<language::and_op>() or
             n.is_type<language::xor_op>() or n.is_type<language::greater_op>() or
             n.is_type<language::shift_left_op>() or n.is_type<language::shift_right_op>() or
             n.is_type<language::greater_or_eq_op>() or n.is_type<language::lesser_op>() or
             n.is_type<language::lesser_or_eq_op>() or n.is_type<language::eqeq_op>() or
             n.is_type<language::not_eq_op>()) {
    ASTNodeBinaryOperatorExpressionBuilder{n};

  } else if (n.is_type<language::if_statement>()) {
    n.m_node_processor = std::make_unique<IfProcessor>(n);
  } else if (n.is_type<language::statement_block>()) {
    n.m_node_processor = std::make_unique<ASTNodeListProcessor>(n);
  } else if (n.is_type<language::do_while_statement>()) {
    n.m_node_processor = std::make_unique<DoWhileProcessor>(n);
  } else if (n.is_type<language::while_statement>()) {
    n.m_node_processor = std::make_unique<WhileProcessor>(n);
  } else if (n.is_type<language::for_statement>()) {
    n.m_node_processor = std::make_unique<ForProcessor>(n);
  } else if (n.is_type<language::for_statement_block>()) {
    n.m_node_processor = std::make_unique<ASTNodeListProcessor>(n);
  } else if (n.is_type<language::for_init>()) {
    n.m_node_processor = std::make_unique<FakeProcessor>();
  } else if (n.is_type<language::for_post>()) {
    n.m_node_processor = std::make_unique<FakeProcessor>();
  } else if (n.is_type<language::for_test>()) {
    n.m_node_processor = std::make_unique<ValueProcessor>(n);
  } else if (n.is_type<language::break_kw>()) {
    n.m_node_processor = std::make_unique<BreakProcessor>();
  } else if (n.is_type<language::continue_kw>()) {
    n.m_node_processor = std::make_unique<ContinueProcessor>();
  } else if (n.is_type<language::empty_set>()) {
    n.m_node_processor = std::make_unique<FakeProcessor>();
  } else {
    std::ostringstream error_message;
    error_message << "undefined node processor type '" << rang::fgB::red << n.name() << rang::fg::reset << "'";
    throw ParseError{error_message.str(), std::vector{n.begin()}};
  }

  for (auto& child : n.children) {
    this->_buildExpression(*child);
  }
}

ASTNodeExpressionBuilder::ASTNodeExpressionBuilder(ASTNode& node)
{
  Assert(node.is_root());
  node.m_node_processor = std::make_unique<ASTNodeListProcessor>(node);
  for (auto& child : node.children) {
    this->_buildExpression(*child);
  }

  // Build expressions of functions
  FunctionTable& function_table = node.m_symbol_table->functionTable();
  for (size_t function_id = 0; function_id < function_table.size(); ++function_id) {
    FunctionDescriptor& function_descriptor = function_table[function_id];
    ASTNode& function_expression            = function_descriptor.definitionNode();
    this->_buildExpression(function_expression);
  }
}
