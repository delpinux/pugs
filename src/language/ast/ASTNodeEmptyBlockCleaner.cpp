#include <language/ast/ASTNodeEmptyBlockCleaner.hpp>

#include <language/PEGGrammar.hpp>
#include <utils/PugsAssert.hpp>

#include <stack>

void
ASTNodeEmptyBlockCleaner::_removeEmptyBlockNode(ASTNode& n)
{
  for (auto& child : n.children) {
    this->_removeEmptyBlockNode(*child);
  }

  std::stack<size_t> empty_block_ids;
  for (size_t i_child = 0; i_child < n.children.size(); ++i_child) {
    if (n.children[i_child]->is_type<language::block>()) {
      if (n.children[i_child]->children.size() == 0)
        empty_block_ids.push(i_child);
    }
  }

  while (empty_block_ids.size() > 0) {
    size_t i_removed = empty_block_ids.top();
    empty_block_ids.pop();
    for (size_t i = i_removed; i + 1 < n.children.size(); ++i) {
      n.children[i] = std::move(n.children[i + 1]);
    }
    n.children.pop_back();
  }
}

ASTNodeEmptyBlockCleaner::ASTNodeEmptyBlockCleaner(ASTNode& n)
{
  Assert(n.is_root());
  this->_removeEmptyBlockNode(n);
}
