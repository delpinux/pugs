#ifndef AST_NODE_BINARY_OPERATOR_EXPRESSION_BUILDER_HPP
#define AST_NODE_BINARY_OPERATOR_EXPRESSION_BUILDER_HPP

#include <language/ast/ASTNode.hpp>

struct ASTNodeBinaryOperatorExpressionBuilder
{
  ASTNodeBinaryOperatorExpressionBuilder(ASTNode& node);
};

#endif   // AST_NODE_BINARY_OPERATOR_EXPRESSION_BUILDER_HPP
