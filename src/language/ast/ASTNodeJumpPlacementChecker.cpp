#include <language/ast/ASTNodeJumpPlacementChecker.hpp>

#include <language/PEGGrammar.hpp>
#include <language/utils/ParseError.hpp>

void
ASTNodeJumpPlacementChecker::_checkJumpPlacement(ASTNode& n, bool is_inside_loop)
{
  if (n.is_type<language::for_statement>() or n.is_type<language::do_while_statement>() or
      n.is_type<language::while_statement>()) {
    for (auto& child : n.children) {
      this->_checkJumpPlacement(*child, true);
    }
  } else if (n.is_type<language::break_kw>() or n.is_type<language::continue_kw>()) {
    if (not is_inside_loop) {
      std::ostringstream error_message;
      error_message << "unexpected '" << rang::fgB::red << n.string() << rang::fg::reset
                    << "' outside of loop or switch statement";
      throw ParseError(error_message.str(), std::vector{n.begin()});
    }
  } else {
    for (auto& child : n.children) {
      this->_checkJumpPlacement(*child, is_inside_loop);
    }
  }
}

ASTNodeJumpPlacementChecker::ASTNodeJumpPlacementChecker(ASTNode& n)
{
  Assert(n.is_root());
  this->_checkJumpPlacement(n, false);
}
