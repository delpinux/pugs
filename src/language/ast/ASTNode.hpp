#ifndef AST_NODE_HPP
#define AST_NODE_HPP

#include <language/ast/ASTExecutionStack.hpp>
#include <language/node_processor/ExecutionPolicy.hpp>
#include <language/node_processor/INodeProcessor.hpp>
#include <language/utils/ASTNodeDataType.hpp>
#include <language/utils/DataVariant.hpp>
#include <language/utils/ParseError.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>

#include <pegtl.hpp>
#include <pegtl/contrib/parse_tree.hpp>

inline TAO_PEGTL_NAMESPACE::file_input<>* p_file_input = nullptr;

class SymbolTable;

class ASTNode : public TAO_PEGTL_NAMESPACE::parse_tree::basic_node<ASTNode>
{
 private:
  static bool m_stack_details;

  PUGS_INLINE
  decltype(m_end)
  _getEnd() const
  {
    if (not this->has_content()) {
      if (this->children.size() > 0) {
        return this->children[children.size() - 1]->_getEnd();
      }
    }
    return m_end;
  }

  PUGS_INLINE
  decltype(m_begin)
  _getBegin() const
  {
    if (not this->has_content()) {
      if (this->children.size() > 0) {
        return this->children[0]->_getBegin();
      }
    }
    return m_begin;
  }

 public:
  static bool
  getStackDetails()
  {
    return m_stack_details;
  }

  static void
  setStackDetails(const bool stack_details)
  {
    m_stack_details = stack_details;
  }

  std::shared_ptr<SymbolTable> m_symbol_table;
  std::unique_ptr<INodeProcessor> m_node_processor;

  ASTNodeDataType m_data_type;

  [[nodiscard]] PUGS_INLINE std::string
  string() const
  {
    if (this->has_content()) {
      return this->TAO_PEGTL_NAMESPACE::parse_tree::basic_node<ASTNode>::string();
    } else {
      auto end   = this->_getEnd();
      auto begin = this->_getBegin();
      if (end.data != nullptr) {
        return std::string{begin.data, end.data};
      }
      return {"<optimized out>"};
    }
  }

  [[nodiscard]] PUGS_INLINE std::string_view
  string_view() const
  {
    if (this->has_content()) {
      return this->TAO_PEGTL_NAMESPACE::parse_tree::basic_node<ASTNode>::string_view();
    } else {
      auto end   = this->_getEnd();
      auto begin = this->_getBegin();
      if (end.data != nullptr) {
        return std::string_view(begin.data, end.data - begin.data);
      }
      return {"<optimized out>"};
    }
  }

  PUGS_INLINE
  std::string
  name() const noexcept
  {
    return demangle(std::string{this->type});
  }

  PUGS_INLINE
  DataVariant
  execute(ExecutionPolicy& exec_policy)
  {
    Assert(m_node_processor, "undefined node processor");

    if (exec_policy.exec()) {
      if (m_stack_details) {
        if (m_node_processor->type() == INodeProcessor::Type::builtin_function_processor) {
          ASTExecutionStack::getInstance().push(this);

          m_stack_details = false;
          auto result     = m_node_processor->execute(exec_policy);
          m_stack_details = true;

          ASTExecutionStack::getInstance().pop();

          return result;
        } else {
          ASTExecutionStack::getInstance().push(this);
          auto result = m_node_processor->execute(exec_policy);
          ASTExecutionStack::getInstance().pop();

          return result;
        }
      } else {
        return m_node_processor->execute(exec_policy);
      }
    } else {
      return {};
    }
  }
};

#endif   // AST_NODE_HPP
