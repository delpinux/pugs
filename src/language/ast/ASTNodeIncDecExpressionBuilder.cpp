#include <language/ast/ASTNodeIncDecExpressionBuilder.hpp>

#include <language/PEGGrammar.hpp>
#include <language/utils/IncDecOperatorMangler.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <language/utils/ParseError.hpp>

ASTNodeIncDecExpressionBuilder::ASTNodeIncDecExpressionBuilder(ASTNode& n)
{
  const ASTNodeDataType& data_type = n.children[0]->m_data_type;

  if (not n.children[0]->is_type<language::name>()) {
    std::ostringstream error_message;
    error_message << "invalid operand type, expecting a variable";

    throw ParseError(error_message.str(), std::vector{n.children[0]->begin()});
  }

  const std::string inc_dec_operator_name = [&] {
    if (n.is_type<language::unary_minusminus>()) {
      return incDecOperatorMangler<language::unary_minusminus>(data_type);
    } else if (n.is_type<language::unary_plusplus>()) {
      return incDecOperatorMangler<language::unary_plusplus>(data_type);
    } else if (n.is_type<language::post_minusminus>()) {
      return incDecOperatorMangler<language::post_minusminus>(data_type);
    } else if (n.is_type<language::post_plusplus>()) {
      return incDecOperatorMangler<language::post_plusplus>(data_type);
    } else {
      // LCOV_EXCL_START
      throw ParseError("unexpected error: undefined inc/dec operator", std::vector{n.begin()});
      // LCOV_EXCL_STOP
    }
  }();

  const auto& optional_processor_builder =
    OperatorRepository::instance().getIncDecProcessorBuilder(inc_dec_operator_name);

  if (optional_processor_builder.has_value()) {
    n.m_node_processor = optional_processor_builder.value()->getNodeProcessor(n);
  } else {
    std::ostringstream error_message;
    error_message << "undefined affectation type: ";
    error_message << rang::fgB::red << inc_dec_operator_name << rang::fg::reset;

    throw ParseError(error_message.str(), std::vector{n.children[0]->begin()});
  }
}
