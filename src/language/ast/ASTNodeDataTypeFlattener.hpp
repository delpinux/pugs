#ifndef AST_NODE_DATA_TYPE_FLATTENER_HPP
#define AST_NODE_DATA_TYPE_FLATTENER_HPP

#include <language/ast/ASTNode.hpp>
#include <language/ast/ASTNodeSubDataType.hpp>

#include <vector>

struct ASTNodeDataTypeFlattener
{
  using FlattenedDataTypeList = std::vector<ASTNodeSubDataType>;

  ASTNodeDataTypeFlattener(ASTNode& node, FlattenedDataTypeList& flattened_datatype);
};

#endif   // AST_NODE_DATA_TYPE_FLATTENER_HPP
