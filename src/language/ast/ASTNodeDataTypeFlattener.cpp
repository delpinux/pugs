#include <language/ast/ASTNodeDataTypeFlattener.hpp>

#include <language/PEGGrammar.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/BuiltinFunctionEmbedderUtils.hpp>
#include <language/utils/FunctionTable.hpp>
#include <language/utils/SymbolTable.hpp>

ASTNodeDataTypeFlattener::ASTNodeDataTypeFlattener(ASTNode& node, FlattenedDataTypeList& flattened_datatype_list)
{
  if (node.is_type<language::expression_list>() or node.is_type<language::function_argument_list>()) {
    for (auto& child_node : node.children) {
      ASTNodeDataTypeFlattener{*child_node, flattened_datatype_list};
    }
  } else if (node.is_type<language::function_evaluation>()) {
    if (node.m_data_type != ASTNodeDataType::list_t) {
      flattened_datatype_list.push_back({node.m_data_type, node});
    } else {
      ASTNode& function_name_node = *node.children[0];
      Assert(node.m_symbol_table->has(function_name_node.string(), node.begin()));

      switch (function_name_node.m_data_type) {
      case ASTNodeDataType::function_t: {
        auto [i_function_symbol, found] = node.m_symbol_table->find(function_name_node.string(), node.begin());

        uint64_t function_id = std::get<uint64_t>(i_function_symbol->attributes().value());

        FunctionDescriptor& function_descriptor = node.m_symbol_table->functionTable()[function_id];

        ASTNode& function_image_domain = *function_descriptor.domainMappingNode().children[1];

        for (auto& image_sub_domain : function_image_domain.children) {
          switch (image_sub_domain->m_data_type) {
          case ASTNodeDataType::typename_t: {
            flattened_datatype_list.push_back({image_sub_domain->m_data_type.contentType(), node});
            break;
          }
          case ASTNodeDataType::type_id_t: {
            flattened_datatype_list.push_back(
              {ASTNodeDataType::build<ASTNodeDataType::type_id_t>(image_sub_domain->m_data_type.nameOfTypeId()), node});
            break;
          }
          case ASTNodeDataType::tuple_t: {
            flattened_datatype_list.push_back(
              {ASTNodeDataType::build<ASTNodeDataType::tuple_t>(image_sub_domain->m_data_type.contentType()), node});
            break;
          }
            // LCOV_EXCL_START
          default: {
            throw UnexpectedError("invalid data type");
          }
            // LCOV_EXCL_STOP
          }
        }
        break;
      }
      case ASTNodeDataType::builtin_function_t: {
        const auto& compound_data_type = getBuiltinFunctionEmbedder(node)->getReturnDataType();

        for (const auto& data_type : compound_data_type.contentTypeList()) {
          flattened_datatype_list.push_back({*data_type, node});
        }

        break;
      }
        //    LCOV_EXCL_START
      default: {
        throw ParseError{"unexpected function type", node.begin()};
      }
        //    LCOV_EXCL_STOP
      }
    }
  } else {
    flattened_datatype_list.push_back({node.m_data_type, node});
  }
}
