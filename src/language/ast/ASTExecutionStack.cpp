#include <language/ast/ASTExecutionStack.hpp>

#include <language/ast/ASTNode.hpp>

ASTExecutionStack* ASTExecutionStack::m_instance = nullptr;

ASTExecutionStack::ASTExecutionStack(const std::shared_ptr<TAO_PEGTL_NAMESPACE::string_input<>>& file_input,
                                     const std::string& file_content)
  : m_file_input(file_input), m_file_content{file_content}
{}

std::string
ASTExecutionStack::errorMessageAt(const std::string& message) const
{
  auto& stack = ASTExecutionStack::getInstance().m_stack;
  std::ostringstream error_msg;

  if (stack.size() > 0) {
    auto p = stack[stack.size() - 1]->begin();
    error_msg << rang::style::bold << p.source << ':' << p.line << ':' << p.column << ": " << rang::style::reset
              << message << rang::fg::reset << '\n';

    if (m_file_input.use_count() > 0) {
      error_msg << m_file_input->line_at(p) << '\n'
                << std::string(p.column - 1, ' ') << rang::fgB::yellow << '^' << rang::fg::reset << '\n';
    }
  } else {
    error_msg << message;
  }

  return error_msg.str();
}

const ASTNode&
ASTExecutionStack::currentNode() const
{
  auto& stack = ASTExecutionStack::getInstance().m_stack;
  Assert(stack.size() > 0);

  return *stack[stack.size() - 1];
}

SourceLocation
ASTExecutionStack::sourceLocation() const
{
  auto& stack = ASTExecutionStack::getInstance().m_stack;
  Assert(stack.size() > 0);

  auto p = stack[stack.size() - 1]->begin();
  return SourceLocation(p.source, p.line, p.column);
}

void
ASTExecutionStack::create()
{
  if (m_instance == nullptr) {
    m_instance = new ASTExecutionStack();
  } else {
    throw UnexpectedError("ASTExecutionStack was already created!");
  }
}

void
ASTExecutionStack::create(const std::shared_ptr<TAO_PEGTL_NAMESPACE::string_input<>>& file_input,
                          const std::string& file_content)
{
  if (m_instance == nullptr) {
    m_instance = new ASTExecutionStack(file_input, file_content);
  } else {
    throw UnexpectedError("ASTExecutionStack was already created!");
  }
}

void
ASTExecutionStack::destroy()
{
  if (m_instance == nullptr) {
    throw UnexpectedError("ASTExecutionStack was not created!");
  }
  delete m_instance;
  m_instance = nullptr;
}
