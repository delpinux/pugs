#include <language/ast/ASTNodeArraySubscriptExpressionBuilder.hpp>

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <language/node_processor/ArraySubscriptProcessor.hpp>
#include <language/utils/ParseError.hpp>

ASTNodeArraySubscriptExpressionBuilder::ASTNodeArraySubscriptExpressionBuilder(ASTNode& node)
{
  auto& array_expression = *node.children[0];

  if (array_expression.m_data_type == ASTNodeDataType::vector_t) {
    switch (array_expression.m_data_type.dimension()) {
    case 1: {
      node.m_node_processor = std::make_unique<ArraySubscriptProcessor<TinyVector<1>>>(node);
      break;
    }
    case 2: {
      node.m_node_processor = std::make_unique<ArraySubscriptProcessor<TinyVector<2>>>(node);
      break;
    }
    case 3: {
      node.m_node_processor = std::make_unique<ArraySubscriptProcessor<TinyVector<3>>>(node);
      break;
    }
    default: {
      throw ParseError("unexpected error: invalid array dimension", array_expression.begin());
      break;
    }
    }
  } else if (array_expression.m_data_type == ASTNodeDataType::matrix_t) {
    Assert(array_expression.m_data_type.numberOfRows() == array_expression.m_data_type.numberOfColumns());

    switch (array_expression.m_data_type.numberOfRows()) {
    case 1: {
      node.m_node_processor = std::make_unique<ArraySubscriptProcessor<TinyMatrix<1>>>(node);
      break;
    }
    case 2: {
      node.m_node_processor = std::make_unique<ArraySubscriptProcessor<TinyMatrix<2>>>(node);
      break;
    }
    case 3: {
      node.m_node_processor = std::make_unique<ArraySubscriptProcessor<TinyMatrix<3>>>(node);
      break;
    }
    default: {
      throw ParseError("unexpected error: invalid array dimension", array_expression.begin());
      break;
    }
    }
  } else {
    throw ParseError("unexpected error: invalid array type", array_expression.begin());
  }
}
