#include <language/modules/LinearSolverModule.hpp>

#include <language/modules/ModuleRepository.hpp>

#include <algebra/EigenvalueSolverOptions.hpp>
#include <algebra/LinearSolverOptions.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/TypeDescriptor.hpp>

LinearSolverModule::LinearSolverModule()
{
  this->_addBuiltinFunction("setLSVerbosity", std::function(

                                                [](const bool& verbose) -> void {
                                                  LinearSolverOptions::default_options.verbose() = verbose;
                                                }

                                                ));

  this->_addBuiltinFunction("setLSEpsilon", std::function(

                                              [](const double& epsilon) -> void {
                                                LinearSolverOptions::default_options.epsilon() = epsilon;
                                              }

                                              ));

  this->_addBuiltinFunction("setLSMaxIter", std::function(

                                              [](const uint64_t& max_iter) -> void {
                                                LinearSolverOptions::default_options.maximumIteration() = max_iter;
                                              }

                                              ));

  this->_addBuiltinFunction("setLSLibrary", std::function(

                                              [](const std::string& library_name) -> void {
                                                LinearSolverOptions::default_options.library() =
                                                  getLSEnumFromName<LSLibrary>(library_name);
                                              }

                                              ));

  this->_addBuiltinFunction("setLSMethod", std::function(

                                             [](const std::string& method_name) -> void {
                                               LinearSolverOptions::default_options.method() =
                                                 getLSEnumFromName<LSMethod>(method_name);
                                             }

                                             ));

  this->_addBuiltinFunction("setLSPrecond", std::function(

                                              [](const std::string& precond_name) -> void {
                                                LinearSolverOptions::default_options.precond() =
                                                  getLSEnumFromName<LSPrecond>(precond_name);
                                              }

                                              ));

  this->_addBuiltinFunction("getLSOptions", std::function(

                                              []() -> std::string {
                                                std::ostringstream os;
                                                os << rang::fgB::yellow << "Linear solver options" << rang::style::reset
                                                   << '\n';
                                                os << LinearSolverOptions::default_options;
                                                return os.str();
                                              }

                                              ));

  this->_addBuiltinFunction("getLSAvailable", std::function(

                                                []() -> std::string {
                                                  std::ostringstream os;

                                                  os << rang::fgB::yellow << "Available linear solver options"
                                                     << rang::style::reset << '\n';
                                                  os << rang::fgB::blue << " libraries" << rang::style::reset << '\n';

                                                  printLSEnumListNames<LSLibrary>(os);
                                                  os << rang::fgB::blue << " methods" << rang::style::reset << '\n';
                                                  printLSEnumListNames<LSMethod>(os);
                                                  os << rang::fgB::blue << " preconditioners" << rang::style::reset
                                                     << '\n';
                                                  printLSEnumListNames<LSPrecond>(os);

                                                  return os.str();
                                                }

                                                ));

  this->_addBuiltinFunction("setESLibrary", std::function(

                                              [](const std::string& library_name) -> void {
                                                EigenvalueSolverOptions::default_options.library() =
                                                  getESEnumFromName<ESLibrary>(library_name);
                                              }

                                              ));

  this->_addBuiltinFunction("getESOptions", std::function(

                                              []() -> std::string {
                                                std::ostringstream os;
                                                os << rang::fgB::yellow << "Eigenvalue solver options"
                                                   << rang::style::reset << '\n';
                                                os << EigenvalueSolverOptions::default_options;
                                                return os.str();
                                              }

                                              ));

  this->_addBuiltinFunction("getESAvailable", std::function(

                                                []() -> std::string {
                                                  std::ostringstream os;

                                                  os << rang::fgB::yellow << "Available eigenvalue solver options"
                                                     << rang::style::reset << '\n';
                                                  os << rang::fgB::blue << " libraries" << rang::style::reset << '\n';

                                                  printESEnumListNames<ESLibrary>(os);

                                                  return os.str();
                                                }

                                                ));
}

void
LinearSolverModule::registerOperators() const
{}

void
LinearSolverModule::registerCheckpointResume() const
{}

ModuleRepository::Subscribe<LinearSolverModule> linear_solver_module;
