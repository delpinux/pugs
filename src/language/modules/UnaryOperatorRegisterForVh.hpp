#ifndef UNARY_OPERATOR_REGISTER_FOR_VH_HPP
#define UNARY_OPERATOR_REGISTER_FOR_VH_HPP

class UnaryOperatorRegisterForVh
{
 private:
  void _register_unary_minus();

 public:
  UnaryOperatorRegisterForVh();
};

#endif   // UNARY_OPERATOR_REGISTER_FOR_VH_HPP
