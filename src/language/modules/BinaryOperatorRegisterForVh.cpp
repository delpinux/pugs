#include <language/modules/BinaryOperatorRegisterForVh.hpp>

#include <language/modules/SchemeModuleTypes.hpp>
#include <language/utils/BinaryOperatorProcessorBuilder.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/DataVariant.hpp>
#include <language/utils/EmbeddedDiscreteFunctionOperators.hpp>
#include <language/utils/OperatorRepository.hpp>

void
BinaryOperatorRegisterForVh::_register_plus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    bool, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    int64_t, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    uint64_t, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    double, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, bool>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, int64_t>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, uint64_t>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, double>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyVector<1>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyVector<2>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyVector<3>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyMatrix<1>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyMatrix<2>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyMatrix<3>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyVector<1>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyVector<2>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyVector<3>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyMatrix<1>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyMatrix<2>>>());

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyMatrix<3>>>());
}

void
BinaryOperatorRegisterForVh::_register_minus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    bool, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    int64_t, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    uint64_t, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    double, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, bool>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, int64_t>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, uint64_t>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, double>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyVector<1>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyVector<2>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyVector<3>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyMatrix<1>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyMatrix<2>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    TinyMatrix<3>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyVector<1>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyVector<2>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyVector<3>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyMatrix<1>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyMatrix<2>>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>, TinyMatrix<3>>>());
}

void
BinaryOperatorRegisterForVh::_register_multiply()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<
      language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
      std::shared_ptr<const DiscreteFunctionVariant>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>, bool,
                                     std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>, int64_t,
                                     std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>, uint64_t,
                                     std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>, double,
                                     std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     std::shared_ptr<const DiscreteFunctionVariant>, bool>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     std::shared_ptr<const DiscreteFunctionVariant>, int64_t>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     std::shared_ptr<const DiscreteFunctionVariant>, uint64_t>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     std::shared_ptr<const DiscreteFunctionVariant>, double>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     TinyMatrix<1>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     TinyMatrix<2>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     TinyMatrix<3>, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     std::shared_ptr<const DiscreteFunctionVariant>, TinyVector<1>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     std::shared_ptr<const DiscreteFunctionVariant>, TinyVector<2>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     std::shared_ptr<const DiscreteFunctionVariant>, TinyVector<3>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     std::shared_ptr<const DiscreteFunctionVariant>, TinyMatrix<1>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     std::shared_ptr<const DiscreteFunctionVariant>, TinyMatrix<2>>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<
      BinaryOperatorProcessorBuilder<language::multiply_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                     std::shared_ptr<const DiscreteFunctionVariant>, TinyMatrix<3>>>());
}

void
BinaryOperatorRegisterForVh::_register_divide()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>,
                                                    std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    int64_t, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    uint64_t, std::shared_ptr<const DiscreteFunctionVariant>>>());

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, std::shared_ptr<const DiscreteFunctionVariant>,
                                                    double, std::shared_ptr<const DiscreteFunctionVariant>>>());
}

BinaryOperatorRegisterForVh::BinaryOperatorRegisterForVh()
{
  this->_register_plus();
  this->_register_minus();
  this->_register_multiply();
  this->_register_divide();
}
