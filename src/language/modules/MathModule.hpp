#ifndef MATH_MODULE_HPP
#define MATH_MODULE_HPP

#include <language/modules/BuiltinModule.hpp>

class MathModule : public BuiltinModule
{
 public:
  std::string_view
  name() const final
  {
    return "math";
  }

  void registerOperators() const final;
  void registerCheckpointResume() const final;

  MathModule();

  ~MathModule() = default;
};

#endif   // MATH_MODULE_HPP
