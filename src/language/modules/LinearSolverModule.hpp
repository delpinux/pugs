#ifndef LINEAR_SOLVER_MODULE_HPP
#define LINEAR_SOLVER_MODULE_HPP

#include <language/modules/BuiltinModule.hpp>

class LinearSolverModule : public BuiltinModule
{
 public:
  std::string_view
  name() const final
  {
    return "linear_solver";
  }

  void registerOperators() const final;
  void registerCheckpointResume() const final;

  LinearSolverModule();
  ~LinearSolverModule() = default;
};

#endif   // LINEAR_SOLVER_MODULE_HPP
