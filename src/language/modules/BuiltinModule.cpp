#include <language/modules/BuiltinModule.hpp>

#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/TypeDescriptor.hpp>
#include <language/utils/ValueDescriptor.hpp>
#include <utils/Exceptions.hpp>

#include <memory>

BuiltinModule::BuiltinModule(bool is_mandatory) : m_is_mandatory{is_mandatory} {}

void
BuiltinModule::_addBuiltinFunction(const std::string& name,
                                   std::shared_ptr<IBuiltinFunctionEmbedder> builtin_function_embedder)
{
  auto is_keyword = [](const std::string& s) -> bool {
    if (s.size() == 0) {
      return false;
    } else {
      if (not(std::isalpha(s[0]) or s[0] == '_')) {
        return false;
      }
      for (size_t i = 1; i < s.size(); ++i) {
        if (not(std::isalnum(s[0]) or s[0] == '_')) {
          return false;
        }
      }
    }

    return true;
  };

  if (not is_keyword(name)) {
    std::ostringstream os;
    os << "while defining module " << this->name() << " invalid builtin function name: '" << name << "'\n";
    throw UnexpectedError(os.str());
  }

  auto parameter_data_type_list = builtin_function_embedder->getParameterDataTypes();

  auto [i_builtin_function, success] = m_name_builtin_function_map.insert(
    std::make_pair(name + ':' + dataTypeName(parameter_data_type_list), builtin_function_embedder));
  if (not success) {
    throw NormalError("builtin-function '" + name + "' cannot be added!\n");
  }
}

void
BuiltinModule::_addNameValue(const std::string& name, const ASTNodeDataType& type, const DataVariant& data)
{
  std::shared_ptr value_descriptor = std::make_shared<ValueDescriptor>(type, data);

  auto [i_type, success] = m_name_value_map.insert(std::make_pair(name, value_descriptor));
  if (not success) {
    throw NormalError("variable '" + name + "' cannot be added!\n");
  }
}

void
BuiltinModule::_addTypeDescriptor(const ASTNodeDataType& ast_node_data_type)
{
  Assert(ast_node_data_type == ASTNodeDataType::type_id_t);
  std::shared_ptr type_descriptor = std::make_shared<TypeDescriptor>(ast_node_data_type.nameOfTypeId());
  auto [i_type, success]          = m_name_type_map.insert(std::make_pair(type_descriptor->name(), type_descriptor));
  if (not success) {
    throw NormalError("type '" + type_descriptor->name() + "' cannot be added!\n");
  }
}
