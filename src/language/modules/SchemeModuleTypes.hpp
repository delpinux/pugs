#ifndef SCHEME_MODULE_TYPES_HPP
#define SCHEME_MODULE_TYPES_HPP

#include <language/utils/ASTNodeDataTypeTraits.hpp>

class IBoundaryConditionDescriptor;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const IBoundaryConditionDescriptor>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("boundary_condition");

class VariableBCDescriptor;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const VariableBCDescriptor>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("variable_boundary_condition");

class DiscreteFunctionVariant;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const DiscreteFunctionVariant>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("Vh");

class IDiscreteFunctionDescriptor;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const IDiscreteFunctionDescriptor>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("Vh_type");

class IQuadratureDescriptor;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const IQuadratureDescriptor>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("quadrature");

#endif   // SCHEME_MODULE_TYPES_HPP
