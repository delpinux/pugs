#ifndef MATH_FUNCTION_REGISTER_FOR_VH_HPP
#define MATH_FUNCTION_REGISTER_FOR_VH_HPP

class SchemeModule;

class MathFunctionRegisterForVh
{
 public:
  MathFunctionRegisterForVh(SchemeModule& module);
};

#endif   // MATH_FUNCTION_REGISTER_FOR_VH_HPP
