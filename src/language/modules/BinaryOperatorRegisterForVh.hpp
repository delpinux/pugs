#ifndef BINARY_OPERATOR_REGISTER_FOR_VH_HPP
#define BINARY_OPERATOR_REGISTER_FOR_VH_HPP

class BinaryOperatorRegisterForVh
{
 private:
  void _register_plus();
  void _register_minus();
  void _register_multiply();
  void _register_divide();

 public:
  BinaryOperatorRegisterForVh();
};

#endif   // BINARY_OPERATOR_REGISTER_FOR_VH_HPP
