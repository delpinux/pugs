#include <language/modules/WriterModule.hpp>

#include <language/modules/ModuleRepository.hpp>

#include <language/modules/MeshModuleTypes.hpp>
#include <language/modules/SchemeModuleTypes.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/CheckpointResumeRepository.hpp>
#include <language/utils/TypeDescriptor.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/GmshReader.hpp>
#include <mesh/ItemArrayVariant.hpp>
#include <mesh/ItemValueVariant.hpp>
#include <mesh/Mesh.hpp>
#include <output/GnuplotWriter.hpp>
#include <output/GnuplotWriter1D.hpp>
#include <output/GnuplotWriterRaw.hpp>
#include <output/IWriter.hpp>
#include <output/NamedDiscreteFunction.hpp>
#include <output/NamedItemArrayVariant.hpp>
#include <output/NamedItemValueVariant.hpp>
#include <output/VTKWriter.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>

#include <utils/checkpointing/ReadINamedDiscreteData.hpp>
#include <utils/checkpointing/ReadIWriter.hpp>
#include <utils/checkpointing/WriteINamedDiscreteData.hpp>
#include <utils/checkpointing/WriteIWriter.hpp>

WriterModule::WriterModule()
{
  this->_addTypeDescriptor(ast_node_data_type_from<std::shared_ptr<const INamedDiscreteData>>);

  this->_addTypeDescriptor(ast_node_data_type_from<std::shared_ptr<const IWriter>>);

  this->_addBuiltinFunction("vtk_writer", std::function(

                                            [](const std::string& filename) -> std::shared_ptr<const IWriter> {
                                              return std::make_shared<VTKWriter>(filename);
                                            }

                                            ));

  this->_addBuiltinFunction("vtk_writer",
                            std::function(

                              [](const std::string& filename, const double& period) -> std::shared_ptr<const IWriter> {
                                return std::make_shared<VTKWriter>(filename, period);
                              }

                              ));

  this->_addBuiltinFunction("gnuplot_writer", std::function(

                                                [](const std::string& filename) -> std::shared_ptr<const IWriter> {
                                                  return std::make_shared<GnuplotWriter>(filename);
                                                }

                                                ));

  this->_addBuiltinFunction("gnuplot_writer",
                            std::function(

                              [](const std::string& filename, const double& period) -> std::shared_ptr<const IWriter> {
                                return std::make_shared<GnuplotWriter>(filename, period);
                              }

                              ));

  this->_addBuiltinFunction("gnuplot_1d_writer", std::function(

                                                   [](const std::string& filename) -> std::shared_ptr<const IWriter> {
                                                     return std::make_shared<GnuplotWriter1D>(filename);
                                                   }

                                                   ));

  this->_addBuiltinFunction("gnuplot_1d_writer",
                            std::function(

                              [](const std::string& filename, const double& period) -> std::shared_ptr<const IWriter> {
                                return std::make_shared<GnuplotWriter1D>(filename, period);
                              }

                              ));

  this->_addBuiltinFunction("gnuplot_raw_writer", std::function(

                                                    [](const std::string& filename) -> std::shared_ptr<const IWriter> {
                                                      return std::make_shared<GnuplotWriterRaw>(filename);
                                                    }

                                                    ));

  this->_addBuiltinFunction("gnuplot_raw_writer",
                            std::function(

                              [](const std::string& filename, const double& period) -> std::shared_ptr<const IWriter> {
                                return std::make_shared<GnuplotWriterRaw>(filename, period);
                              }

                              ));

  this->_addBuiltinFunction("name_output", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> discrete_function,
                                                const std::string& name) -> std::shared_ptr<const INamedDiscreteData> {
                                               return std::make_shared<const NamedDiscreteFunction>(discrete_function,
                                                                                                    name);
                                             }

                                             ));

  this->_addBuiltinFunction("name_output", std::function(

                                             [](std::shared_ptr<const ItemValueVariant> item_value_variant,
                                                const std::string& name) -> std::shared_ptr<const INamedDiscreteData> {
                                               return std::make_shared<const NamedItemValueVariant>(item_value_variant,
                                                                                                    name);
                                             }

                                             ));

  this->_addBuiltinFunction("name_output", std::function(

                                             [](std::shared_ptr<const ItemArrayVariant> item_array_variant,
                                                const std::string& name) -> std::shared_ptr<const INamedDiscreteData> {
                                               return std::make_shared<const NamedItemArrayVariant>(item_array_variant,
                                                                                                    name);
                                             }

                                             ));

  this->_addBuiltinFunction("write_mesh",
                            std::function(

                              [](std::shared_ptr<const IWriter> writer,
                                 std::shared_ptr<const MeshVariant> mesh_v) -> void { writer->writeMesh(mesh_v); }

                              ));

  this->_addBuiltinFunction("write", std::function(

                                       [](std::shared_ptr<const IWriter> writer,
                                          const std::vector<std::shared_ptr<const INamedDiscreteData>>&
                                            named_discrete_function_list) -> void {
                                         writer->write(named_discrete_function_list);
                                       }

                                       ));

  this->_addBuiltinFunction("write", std::function(

                                       [](std::shared_ptr<const IWriter> writer,
                                          const std::vector<std::shared_ptr<const INamedDiscreteData>>&
                                            named_discrete_function_list,
                                          const double& time) -> void {
                                         writer->writeIfNeeded(named_discrete_function_list, time);
                                       }

                                       ));

  this->_addBuiltinFunction("force_write", std::function(

                                             [](std::shared_ptr<const IWriter> writer,
                                                const std::vector<std::shared_ptr<const INamedDiscreteData>>&
                                                  named_discrete_function_list,
                                                const double& time) -> void {
                                               writer->writeForced(named_discrete_function_list, time);
                                             }

                                             ));

  this->_addBuiltinFunction("write",
                            std::function(

                              [](std::shared_ptr<const IWriter> writer, std::shared_ptr<const MeshVariant> mesh_v,
                                 const std::vector<std::shared_ptr<const INamedDiscreteData>>&
                                   named_discrete_function_list) -> void {
                                writer->writeOnMesh(mesh_v, named_discrete_function_list);
                              }

                              ));

  this->_addBuiltinFunction("write",
                            std::function(

                              [](std::shared_ptr<const IWriter> writer, std::shared_ptr<const MeshVariant> mesh_v,
                                 const std::vector<std::shared_ptr<const INamedDiscreteData>>&
                                   named_discrete_function_list,
                                 const double& time) -> void {
                                writer->writeOnMeshIfNeeded(mesh_v, named_discrete_function_list, time);
                              }

                              ));

  this->_addBuiltinFunction("force_write",
                            std::function(

                              [](std::shared_ptr<const IWriter> writer, std::shared_ptr<const MeshVariant> mesh_v,
                                 const std::vector<std::shared_ptr<const INamedDiscreteData>>&
                                   named_discrete_function_list,
                                 const double& time) -> void {
                                writer->writeOnMeshForced(mesh_v, named_discrete_function_list, time);
                              }

                              ));
}

void
WriterModule::registerOperators() const
{}

void
WriterModule::registerCheckpointResume() const
{
#ifdef PUGS_HAS_HDF5

  CheckpointResumeRepository::instance()
    .addCheckpointResume(ast_node_data_type_from<std::shared_ptr<const INamedDiscreteData>>,
                         std::function([](const std::string& symbol_name, const EmbeddedData& embedded_data,
                                          HighFive::File& file, HighFive::Group& checkpoint_group,
                                          HighFive::Group& symbol_table_group) {
                           checkpointing::writeINamedDiscreteData(symbol_name, embedded_data, file, checkpoint_group,
                                                                  symbol_table_group);
                         }),
                         std::function([](const std::string& symbol_name,
                                          const HighFive::Group& symbol_table_group) -> EmbeddedData {
                           return checkpointing::readINamedDiscreteData(symbol_name, symbol_table_group);
                         }));

  CheckpointResumeRepository::instance()
    .addCheckpointResume(ast_node_data_type_from<std::shared_ptr<const IWriter>>,
                         std::function([](const std::string& symbol_name, const EmbeddedData& embedded_data,
                                          HighFive::File& file, HighFive::Group& checkpoint_group,
                                          HighFive::Group& symbol_table_group) {
                           checkpointing::writeIWriter(symbol_name, embedded_data, file, checkpoint_group,
                                                       symbol_table_group);
                         }),
                         std::function([](const std::string& symbol_name,
                                          const HighFive::Group& symbol_table_group) -> EmbeddedData {
                           return checkpointing::readIWriter(symbol_name, symbol_table_group);
                         }));

#endif   // PUGS_HAS_HDF5
}

ModuleRepository::Subscribe<WriterModule> writer_module{};
