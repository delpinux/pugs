#ifndef MESH_MODULE_TYPES_HPP
#define MESH_MODULE_TYPES_HPP

#include <language/utils/ASTNodeDataTypeTraits.hpp>

class MeshVariant;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const MeshVariant>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("mesh");

class IBoundaryDescriptor;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const IBoundaryDescriptor>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("boundary");

class IInterfaceDescriptor;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const IInterfaceDescriptor>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("interface");

class IZoneDescriptor;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const IZoneDescriptor>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("zone");

template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const ItemType>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("item_type");

class ItemValueVariant;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const ItemValueVariant>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("item_value");

class ItemArrayVariant;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const ItemArrayVariant>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("item_array");

class SubItemValuePerItemVariant;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const SubItemValuePerItemVariant>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("sub_item_value");

class SubItemArrayPerItemVariant;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const SubItemArrayPerItemVariant>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("sub_item_array");

#endif   // MESH_MODULE_TYPES_HPP
