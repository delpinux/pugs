#include <language/modules/UnaryOperatorRegisterForVh.hpp>

#include <language/modules/SchemeModuleTypes.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/DataVariant.hpp>
#include <language/utils/EmbeddedDiscreteFunctionOperators.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <language/utils/UnaryOperatorProcessorBuilder.hpp>

void
UnaryOperatorRegisterForVh::_register_unary_minus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addUnaryOperator<language::unary_minus>(
    std::make_shared<
      UnaryOperatorProcessorBuilder<language::unary_minus, std::shared_ptr<const DiscreteFunctionVariant>,
                                    std::shared_ptr<const DiscreteFunctionVariant>>>());
}

UnaryOperatorRegisterForVh::UnaryOperatorRegisterForVh()
{
  this->_register_unary_minus();
}
