#include <language/modules/MathModule.hpp>

#include <language/modules/ModuleRepository.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>

#include <cmath>
#include <cstdlib>

MathModule::MathModule()
{
  this->_addBuiltinFunction("sqrt", std::function([](double x) -> double { return std::sqrt(x); }));

  this->_addBuiltinFunction("abs", std::function([](double x) -> double { return std::abs(x); }));

  this->_addBuiltinFunction("abs", std::function([](int64_t x) -> uint64_t { return std::abs(x); }));

  this->_addBuiltinFunction("sin", std::function([](double x) -> double { return std::sin(x); }));

  this->_addBuiltinFunction("cos", std::function([](double x) -> double { return std::cos(x); }));

  this->_addBuiltinFunction("tan", std::function([](double x) -> double { return std::tan(x); }));

  this->_addBuiltinFunction("asin", std::function([](double x) -> double { return std::asin(x); }));

  this->_addBuiltinFunction("acos", std::function([](double x) -> double { return std::acos(x); }));

  this->_addBuiltinFunction("atan", std::function([](double x) -> double { return std::atan(x); }));

  this->_addBuiltinFunction("atan2", std::function([](double x, double y) -> double { return std::atan2(x, y); }));

  this->_addBuiltinFunction("sinh", std::function([](double x) -> double { return std::sinh(x); }));

  this->_addBuiltinFunction("cosh", std::function([](double x) -> double { return std::cosh(x); }));

  this->_addBuiltinFunction("tanh", std::function([](double x) -> double { return std::tanh(x); }));

  this->_addBuiltinFunction("asinh", std::function([](double x) -> double { return std::asinh(x); }));

  this->_addBuiltinFunction("acosh", std::function([](double x) -> double { return std::acosh(x); }));

  this->_addBuiltinFunction("atanh", std::function([](double x) -> double { return std::atanh(x); }));

  this->_addBuiltinFunction("exp", std::function([](double x) -> double { return std::exp(x); }));

  this->_addBuiltinFunction("log", std::function([](double x) -> double { return std::log(x); }));

  this->_addBuiltinFunction("pow", std::function([](double x, double y) -> double { return std::pow(x, y); }));

  this->_addBuiltinFunction("ceil", std::function([](double x) -> int64_t { return std::ceil(x); }));

  this->_addBuiltinFunction("floor", std::function([](double x) -> int64_t { return std::floor(x); }));

  this->_addBuiltinFunction("trunc", std::function([](double x) -> int64_t { return std::trunc(x); }));

  this->_addBuiltinFunction("round", std::function([](double x) -> int64_t { return std::lround(x); }));

  this->_addBuiltinFunction("min", std::function([](double x, double y) -> double { return std::min(x, y); }));

  this->_addBuiltinFunction("min", std::function([](int64_t x, int64_t y) -> int64_t { return std::min(x, y); }));

  this->_addBuiltinFunction("max", std::function([](double x, double y) -> double { return std::max(x, y); }));

  this->_addBuiltinFunction("max", std::function([](int64_t x, int64_t y) -> int64_t { return std::max(x, y); }));

  this->_addBuiltinFunction("dot", std::function(
                                     [](const TinyVector<1> x, const TinyVector<1> y) -> double { return dot(x, y); }));

  this->_addBuiltinFunction("dot", std::function(
                                     [](const TinyVector<2> x, const TinyVector<2> y) -> double { return dot(x, y); }));

  this->_addBuiltinFunction("dot", std::function([](const TinyVector<3>& x, const TinyVector<3>& y) -> double {
                              return dot(x, y);
                            }));

  this->_addBuiltinFunction("doubleDot", std::function([](const TinyMatrix<1> A, const TinyMatrix<1> B) -> double {
                              return doubleDot(A, B);
                            }));

  this->_addBuiltinFunction("doubleDot", std::function([](const TinyMatrix<2> A, const TinyMatrix<2> B) -> double {
                              return doubleDot(A, B);
                            }));

  this->_addBuiltinFunction("doubleDot", std::function([](const TinyMatrix<3>& A, const TinyMatrix<3>& B) -> double {
                              return doubleDot(A, B);
                            }));

  this->_addBuiltinFunction("tensorProduct",
                            std::function([](const TinyVector<1> x, const TinyVector<1> y) -> TinyMatrix<1> {
                              return tensorProduct(x, y);
                            }));

  this->_addBuiltinFunction("tensorProduct",
                            std::function([](const TinyVector<2> x, const TinyVector<2> y) -> TinyMatrix<2> {
                              return tensorProduct(x, y);
                            }));

  this->_addBuiltinFunction("tensorProduct",
                            std::function([](const TinyVector<3>& x, const TinyVector<3>& y) -> TinyMatrix<3> {
                              return tensorProduct(x, y);
                            }));

  this->_addBuiltinFunction("det", std::function([](const TinyMatrix<1> A) -> double { return det(A); }));

  this->_addBuiltinFunction("det", std::function([](const TinyMatrix<2>& A) -> double { return det(A); }));

  this->_addBuiltinFunction("det", std::function([](const TinyMatrix<3>& A) -> double { return det(A); }));

  this->_addBuiltinFunction("trace", std::function([](const TinyMatrix<1> A) -> double { return trace(A); }));

  this->_addBuiltinFunction("trace", std::function([](const TinyMatrix<2>& A) -> double { return trace(A); }));

  this->_addBuiltinFunction("trace", std::function([](const TinyMatrix<3>& A) -> double { return trace(A); }));

  this->_addBuiltinFunction("inverse",
                            std::function([](const TinyMatrix<1> A) -> TinyMatrix<1> { return inverse(A); }));

  this->_addBuiltinFunction("inverse",
                            std::function([](const TinyMatrix<2>& A) -> TinyMatrix<2> { return inverse(A); }));

  this->_addBuiltinFunction("inverse",
                            std::function([](const TinyMatrix<3>& A) -> TinyMatrix<3> { return inverse(A); }));

  this->_addBuiltinFunction("transpose",
                            std::function([](const TinyMatrix<1> A) -> TinyMatrix<1> { return transpose(A); }));

  this->_addBuiltinFunction("transpose",
                            std::function([](const TinyMatrix<2>& A) -> TinyMatrix<2> { return transpose(A); }));

  this->_addBuiltinFunction("transpose",
                            std::function([](const TinyMatrix<3>& A) -> TinyMatrix<3> { return transpose(A); }));
}

void
MathModule::registerOperators() const
{}

void
MathModule::registerCheckpointResume() const
{}

ModuleRepository::Subscribe<MathModule> math_module;
