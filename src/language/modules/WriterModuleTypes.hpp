#ifndef WRITER_MODULE_TYPES_HPP
#define WRITER_MODULE_TYPES_HPP

#include <language/utils/ASTNodeDataTypeTraits.hpp>

class OutputNamedItemValueSet;
class INamedDiscreteData;

template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const INamedDiscreteData>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("output");

class IWriter;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const IWriter>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("writer");

#endif   // WRITER_MODULE_TYPES_HPP
