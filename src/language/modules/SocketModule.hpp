#ifndef SOCKET_MODULE_HPP
#define SOCKET_MODULE_HPP

#include <language/modules/BuiltinModule.hpp>
#include <language/modules/SocketModuleTypes.hpp>

class SocketModule : public BuiltinModule
{
 public:
  std::string_view
  name() const final
  {
    return "socket";
  }

  void registerOperators() const final;
  void registerCheckpointResume() const final;

  SocketModule();
  ~SocketModule() = default;
};

#endif   // SOCKET_MODULE_HPP
