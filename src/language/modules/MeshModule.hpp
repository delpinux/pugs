#ifndef MESH_MODULE_HPP
#define MESH_MODULE_HPP

#include <language/modules/BuiltinModule.hpp>
#include <language/modules/MeshModuleTypes.hpp>

class MeshModule : public BuiltinModule
{
 public:
  std::string_view
  name() const final
  {
    return "mesh";
  }

  void registerOperators() const final;
  void registerCheckpointResume() const final;

  MeshModule();

  ~MeshModule() = default;
};

#endif   // MESH_MODULE_HPP
