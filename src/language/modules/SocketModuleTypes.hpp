#ifndef SOCKET_MODULE_TYPES_HPP
#define SOCKET_MODULE_TYPES_HPP

#include <language/utils/ASTNodeDataTypeTraits.hpp>

class Socket;
template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const Socket>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("socket");

#endif   // SOCKET_MODULE_TYPES_HPP
