#include <language/modules/DevUtilsModule.hpp>

#include <language/modules/ModuleRepository.hpp>

#include <dev/ParallelChecker.hpp>
#include <language/modules/MeshModuleTypes.hpp>
#include <language/modules/SchemeModuleTypes.hpp>
#include <language/utils/ASTDotPrinter.hpp>
#include <language/utils/ASTExecutionInfo.hpp>
#include <language/utils/ASTPrinter.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/SymbolTable.hpp>

#include <fstream>

DevUtilsModule::DevUtilsModule()
{
  this->_addBuiltinFunction("getAST", std::function(

                                        []() -> std::string {
                                          const auto& root_node = ASTExecutionInfo::getInstance().rootNode();

                                          std::ostringstream os;
                                          os << ASTPrinter{root_node};

                                          return os.str();
                                        }

                                        ));

  this->_addBuiltinFunction("saveASTDot", std::function(

                                            [](const std::string& dot_filename) -> void {
                                              const auto& root_node = ASTExecutionInfo::getInstance().rootNode();

                                              std::ofstream fout(dot_filename);

                                              if (not fout) {
                                                std::ostringstream os;
                                                os << "could not create file '" << dot_filename << "'\n";
                                                throw NormalError(os.str());
                                              }

                                              ASTDotPrinter dot_printer{root_node};
                                              fout << dot_printer;

                                              if (not fout) {
                                                std::ostringstream os;
                                                os << "could not write AST to '" << dot_filename << "'\n";
                                                throw NormalError(os.str());
                                              }
                                            }

                                            ));

  this->_addBuiltinFunction("getFunctionAST", std::function(

                                                [](const FunctionSymbolId& function_symbol_id) -> std::string {
                                                  const auto& function_descriptor = function_symbol_id.descriptor();

                                                  std::ostringstream os;
                                                  os << function_descriptor.name() << ": domain mapping\n";
                                                  os << ASTPrinter(function_descriptor.domainMappingNode());
                                                  os << function_descriptor.name() << ": definition\n";
                                                  os << ASTPrinter(function_descriptor.definitionNode());

                                                  return os.str();
                                                }

                                                ));

  this->_addBuiltinFunction("parallel_check",
                            std::function(

                              [](const std::shared_ptr<const DiscreteFunctionVariant>& discrete_function,
                                 const std::string& name) {
                                parallel_check(*discrete_function, name,
                                               ASTExecutionStack::getInstance().sourceLocation());
                              }

                              ));

  this->_addBuiltinFunction("parallel_check",
                            std::function(

                              [](const std::shared_ptr<const ItemValueVariant>& item_value, const std::string& name) {
                                parallel_check(*item_value, name, ASTExecutionStack::getInstance().sourceLocation());
                              }

                              ));

  this->_addBuiltinFunction("parallel_check",
                            std::function(

                              [](const std::shared_ptr<const ItemArrayVariant>& item_array, const std::string& name) {
                                parallel_check(*item_array, name, ASTExecutionStack::getInstance().sourceLocation());
                              }

                              ));

  this->_addBuiltinFunction("parallel_check",
                            std::function(

                              [](const std::shared_ptr<const SubItemValuePerItemVariant>& subitem_value_per_item,
                                 const std::string& name) {
                                parallel_check(*subitem_value_per_item, name,
                                               ASTExecutionStack::getInstance().sourceLocation());
                              }

                              ));

  this->_addBuiltinFunction("parallel_check",
                            std::function(

                              [](const std::shared_ptr<const SubItemArrayPerItemVariant>& subitem_array_per_item,
                                 const std::string& name) {
                                parallel_check(*subitem_array_per_item, name,
                                               ASTExecutionStack::getInstance().sourceLocation());
                              }

                              ));
}

void
DevUtilsModule::registerOperators() const
{}

void
DevUtilsModule::registerCheckpointResume() const
{}

ModuleRepository::Subscribe<DevUtilsModule> dev_utils_module;
