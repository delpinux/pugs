#include <language/modules/SocketModule.hpp>

#include <language/modules/ModuleRepository.hpp>

#include <language/utils/BinaryOperatorProcessorBuilder.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/CheckpointResumeRepository.hpp>
#include <language/utils/OStream.hpp>
#include <language/utils/OperatorRepository.hpp>
#include <utils/Socket.hpp>

SocketModule::SocketModule()
{
  this->_addTypeDescriptor(ast_node_data_type_from<std::shared_ptr<const Socket>>);

  this->_addBuiltinFunction("createSocketServer",
                            std::function(

                              [](const uint64_t& port_number) -> std::shared_ptr<const Socket> {
                                return std::make_shared<const Socket>(createServerSocket(port_number));
                              }

                              ));

  this->_addBuiltinFunction("acceptSocketClient",
                            std::function(

                              [](std::shared_ptr<const Socket> server_socket) -> std::shared_ptr<const Socket> {
                                return std::make_shared<const Socket>(acceptClientSocket(*server_socket));
                              }

                              ));

  this->_addBuiltinFunction("connectSocketServer",
                            std::function(

                              [](const std::string& hostname,
                                 const uint64_t& port_number) -> std::shared_ptr<const Socket> {
                                return std::make_shared<const Socket>(connectServerSocket(hostname, port_number));
                              }

                              ));

  this->_addBuiltinFunction("write", std::function(

                                       [](const std::shared_ptr<const Socket>& socket, const bool& value) -> void {
                                         write(*socket, value);
                                       }

                                       ));

  this->_addBuiltinFunction("write", std::function(

                                       [](const std::shared_ptr<const Socket>& socket, const uint64_t& value) -> void {
                                         write(*socket, value);
                                       }

                                       ));

  this->_addBuiltinFunction("write", std::function(

                                       [](const std::shared_ptr<const Socket>& socket, const int64_t& value) -> void {
                                         write(*socket, value);
                                       }

                                       ));

  this->_addBuiltinFunction("write", std::function(

                                       [](const std::shared_ptr<const Socket>& socket, const double& value) -> void {
                                         write(*socket, value);
                                       }

                                       ));

  this->_addBuiltinFunction("write", std::function(

                                       [](const std::shared_ptr<const Socket>& socket,
                                          const TinyVector<1>& value) -> void { write(*socket, value); }

                                       ));

  this->_addBuiltinFunction("write", std::function(

                                       [](const std::shared_ptr<const Socket>& socket,
                                          const TinyVector<2>& value) -> void { write(*socket, value); }

                                       ));

  this->_addBuiltinFunction("write", std::function(

                                       [](const std::shared_ptr<const Socket>& socket,
                                          const TinyVector<3>& value) -> void { write(*socket, value); }

                                       ));

  this->_addBuiltinFunction("write", std::function(

                                       [](const std::shared_ptr<const Socket>& socket,
                                          const TinyMatrix<1>& value) -> void { write(*socket, value); }

                                       ));

  this->_addBuiltinFunction("write", std::function(

                                       [](const std::shared_ptr<const Socket>& socket,
                                          const TinyMatrix<2>& value) -> void { write(*socket, value); }

                                       ));

  this->_addBuiltinFunction("write", std::function(

                                       [](const std::shared_ptr<const Socket>& socket,
                                          const TinyMatrix<3>& value) -> void { write(*socket, value); }

                                       ));

  this->_addBuiltinFunction("write",
                            std::function(

                              [](const std::shared_ptr<const Socket>& socket, const std::string& value) -> void {
                                write(*socket, value.size());
                                write(*socket, value);
                              }

                              ));

  this->_addBuiltinFunction("read_B", std::function(

                                        [](const std::shared_ptr<const Socket>& socket) -> bool {
                                          bool value;
                                          read(*socket, value);

                                          return value;
                                        }

                                        ));

  this->_addBuiltinFunction("read_N", std::function(

                                        [](const std::shared_ptr<const Socket>& socket) -> uint64_t {
                                          uint64_t value;
                                          read(*socket, value);

                                          return value;
                                        }

                                        ));

  this->_addBuiltinFunction("read_Z", std::function(

                                        [](const std::shared_ptr<const Socket>& socket) -> int64_t {
                                          int64_t value;
                                          read(*socket, value);

                                          return value;
                                        }

                                        ));

  this->_addBuiltinFunction("read_R", std::function(

                                        [](const std::shared_ptr<const Socket>& socket) -> double {
                                          double value;
                                          read(*socket, value);

                                          return value;
                                        }

                                        ));

  this->_addBuiltinFunction("read_R1", std::function(

                                         [](const std::shared_ptr<const Socket>& socket) -> TinyVector<1> {
                                           TinyVector<1> value;
                                           read(*socket, value);

                                           return value;
                                         }

                                         ));

  this->_addBuiltinFunction("read_R2", std::function(

                                         [](const std::shared_ptr<const Socket>& socket) -> TinyVector<2> {
                                           TinyVector<2> value;
                                           read(*socket, value);

                                           return value;
                                         }

                                         ));

  this->_addBuiltinFunction("read_R3", std::function(

                                         [](const std::shared_ptr<const Socket>& socket) -> TinyVector<3> {
                                           TinyVector<3> value;
                                           read(*socket, value);

                                           return value;
                                         }

                                         ));

  this->_addBuiltinFunction("read_R1x1", std::function(

                                           [](const std::shared_ptr<const Socket>& socket) -> TinyMatrix<1> {
                                             TinyMatrix<1> value;
                                             read(*socket, value);

                                             return value;
                                           }

                                           ));

  this->_addBuiltinFunction("read_R2x2", std::function(

                                           [](const std::shared_ptr<const Socket>& socket) -> TinyMatrix<2> {
                                             TinyMatrix<2> value;
                                             read(*socket, value);

                                             return value;
                                           }

                                           ));

  this->_addBuiltinFunction("read_R3x3", std::function(

                                           [](const std::shared_ptr<const Socket>& socket) -> TinyMatrix<3> {
                                             TinyMatrix<3> value;
                                             read(*socket, value);

                                             return value;
                                           }

                                           ));

  this->_addBuiltinFunction("read_string", std::function(

                                             [](const std::shared_ptr<const Socket>& socket) -> std::string {
                                               size_t size;
                                               read(*socket, size);
                                               std::string value;
                                               if (size > 0) {
                                                 value.resize(size);
                                                 read(*socket, value);
                                               }
                                               return value;
                                             }

                                             ));
}

void
SocketModule::registerOperators() const
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, std::shared_ptr<const Socket>>>());
}

void
SocketModule::registerCheckpointResume() const
{
  CheckpointResumeRepository::instance()
    .addCheckpointResume(ast_node_data_type_from<std::shared_ptr<const Socket>>,
                         std::function(
                           [](const std::string&, const EmbeddedData&, HighFive::File&, HighFive::Group&,
                              HighFive::Group&) { throw NotImplementedError("checkpoint/resume with sockets"); }),
                         std::function([](const std::string&, const HighFive::Group&) -> EmbeddedData {
                           throw NotImplementedError("checkpoint/resume with sockets");
                         }));
}

ModuleRepository::Subscribe<SocketModule> socket_module;
