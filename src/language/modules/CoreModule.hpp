#ifndef CORE_MODULE_HPP
#define CORE_MODULE_HPP

#include <language/modules/BuiltinModule.hpp>

class CoreModule : public BuiltinModule
{
 public:
  std::string_view
  name() const final
  {
    return "core";
  }

  void registerOperators() const final;
  void registerCheckpointResume() const final;

  CoreModule();
  ~CoreModule() = default;
};

#endif   // CORE_MODULE_HPP
