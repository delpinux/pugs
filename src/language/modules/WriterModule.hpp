#ifndef WRITER_MODULE_HPP
#define WRITER_MODULE_HPP

#include <language/modules/BuiltinModule.hpp>
#include <language/modules/WriterModuleTypes.hpp>

class WriterModule : public BuiltinModule
{
 public:
  std::string_view
  name() const final
  {
    return "writer";
  }

  void registerOperators() const final;
  void registerCheckpointResume() const final;

  WriterModule();

  ~WriterModule() = default;
};

#endif   // WRITER_MODULE_HPP
