#ifndef IMODULE_HPP
#define IMODULE_HPP

#include <language/utils/DataVariant.hpp>

#include <memory>
#include <string>
#include <string_view>
#include <unordered_map>

class IBuiltinFunctionEmbedder;
class TypeDescriptor;
class ValueDescriptor;

class IModule
{
 public:
  using NameBuiltinFunctionMap = std::unordered_map<std::string, std::shared_ptr<IBuiltinFunctionEmbedder>>;
  using NameTypeMap            = std::unordered_map<std::string, std::shared_ptr<TypeDescriptor>>;
  using NameValueMap           = std::unordered_map<std::string, std::shared_ptr<ValueDescriptor>>;

  IModule()                     = default;
  IModule(IModule&&)            = default;
  IModule& operator=(IModule&&) = default;

  virtual bool isMandatory() const = 0;

  virtual const NameBuiltinFunctionMap& getNameBuiltinFunctionMap() const = 0;

  virtual const NameTypeMap& getNameTypeMap() const = 0;

  virtual const NameValueMap& getNameValueMap() const = 0;

  virtual void registerOperators() const        = 0;
  virtual void registerCheckpointResume() const = 0;

  virtual std::string_view name() const = 0;

  virtual ~IModule() = default;
};

#endif   // IMODULE_HPP
