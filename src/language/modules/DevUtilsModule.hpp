#ifndef DEV_UTILS_MODULE_HPP
#define DEV_UTILS_MODULE_HPP

#include <language/modules/BuiltinModule.hpp>

class DevUtilsModule : public BuiltinModule
{
 public:
  std::string_view
  name() const final
  {
    return "dev_utils";
  }

  void registerOperators() const final;
  void registerCheckpointResume() const final;

  DevUtilsModule();
  ~DevUtilsModule() = default;
};

#endif   // DEV_UTILS_MODULE_HPP
