#include <language/modules/MathFunctionRegisterForVh.hpp>

#include <language/modules/SchemeModule.hpp>
#include <language/modules/SchemeModuleTypes.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/EmbeddedDiscreteFunctionMathFunctions.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>

MathFunctionRegisterForVh::MathFunctionRegisterForVh(SchemeModule& scheme_module)
{
  scheme_module._addBuiltinFunction("sqrt", std::function(

                                              [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                                -> std::shared_ptr<const DiscreteFunctionVariant> { return sqrt(a); }

                                              ));

  scheme_module._addBuiltinFunction("abs", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return abs(a); }

                                             ));

  scheme_module._addBuiltinFunction("sin", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return sin(a); }

                                             ));

  scheme_module._addBuiltinFunction("cos", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return cos(a); }

                                             ));

  scheme_module._addBuiltinFunction("tan", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return tan(a); }

                                             ));

  scheme_module._addBuiltinFunction("asin", std::function(

                                              [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                                -> std::shared_ptr<const DiscreteFunctionVariant> { return asin(a); }

                                              ));

  scheme_module._addBuiltinFunction("acos", std::function(

                                              [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                                -> std::shared_ptr<const DiscreteFunctionVariant> { return acos(a); }

                                              ));

  scheme_module._addBuiltinFunction("atan", std::function(

                                              [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                                -> std::shared_ptr<const DiscreteFunctionVariant> { return atan(a); }

                                              ));

  scheme_module._addBuiltinFunction("atan2",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a,
                                         std::shared_ptr<const DiscreteFunctionVariant> b)
                                        -> std::shared_ptr<const DiscreteFunctionVariant> { return atan2(a, b); }

                                      ));

  scheme_module._addBuiltinFunction("atan2",
                                    std::function(

                                      [](double a, std::shared_ptr<const DiscreteFunctionVariant> b)
                                        -> std::shared_ptr<const DiscreteFunctionVariant> { return atan2(a, b); }

                                      ));

  scheme_module._addBuiltinFunction("atan2", std::function(

                                               [](std::shared_ptr<const DiscreteFunctionVariant> a,
                                                  double b) -> std::shared_ptr<const DiscreteFunctionVariant> {
                                                 return atan2(a, b);
                                               }

                                               ));

  scheme_module._addBuiltinFunction("sinh", std::function(

                                              [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                                -> std::shared_ptr<const DiscreteFunctionVariant> { return sinh(a); }

                                              ));

  scheme_module._addBuiltinFunction("tanh", std::function(

                                              [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                                -> std::shared_ptr<const DiscreteFunctionVariant> { return tanh(a); }

                                              ));

  scheme_module._addBuiltinFunction("asinh", std::function(

                                               [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                                 -> std::shared_ptr<const DiscreteFunctionVariant> { return asinh(a); }

                                               ));

  scheme_module._addBuiltinFunction("acosh", std::function(

                                               [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                                 -> std::shared_ptr<const DiscreteFunctionVariant> { return acosh(a); }

                                               ));

  scheme_module._addBuiltinFunction("atanh", std::function(

                                               [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                                 -> std::shared_ptr<const DiscreteFunctionVariant> { return atanh(a); }

                                               ));

  scheme_module._addBuiltinFunction("exp", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return exp(a); }

                                             ));

  scheme_module._addBuiltinFunction("log", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return log(a); }

                                             ));

  scheme_module._addBuiltinFunction("pow", std::function(

                                             [](double a, std::shared_ptr<const DiscreteFunctionVariant> b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return pow(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("pow", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a, double b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return pow(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("pow", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a,
                                                std::shared_ptr<const DiscreteFunctionVariant> b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return pow(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("dot", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a,
                                                std::shared_ptr<const DiscreteFunctionVariant> b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return dot(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("dot", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a, const TinyVector<1> b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return dot(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("dot", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a, const TinyVector<2> b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return dot(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("dot",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a, const TinyVector<3>& b)
                                        -> std::shared_ptr<const DiscreteFunctionVariant> { return dot(a, b); }

                                      ));

  scheme_module._addBuiltinFunction("dot", std::function(

                                             [](const TinyVector<1> a, std::shared_ptr<const DiscreteFunctionVariant> b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return dot(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("dot", std::function(

                                             [](const TinyVector<2> a, std::shared_ptr<const DiscreteFunctionVariant> b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return dot(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("dot",
                                    std::function(

                                      [](const TinyVector<3>& a, std::shared_ptr<const DiscreteFunctionVariant> b)
                                        -> std::shared_ptr<const DiscreteFunctionVariant> { return dot(a, b); }

                                      ));

  scheme_module._addBuiltinFunction("tensorProduct", std::function(

                                                       [](std::shared_ptr<const DiscreteFunctionVariant> a,
                                                          std::shared_ptr<const DiscreteFunctionVariant> b)
                                                         -> std::shared_ptr<const DiscreteFunctionVariant> {
                                                         return tensorProduct(a, b);
                                                       }

                                                       ));

  scheme_module._addBuiltinFunction("tensorProduct",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a,
                                         const TinyVector<1> b) -> std::shared_ptr<const DiscreteFunctionVariant> {
                                        return tensorProduct(a, b);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("tensorProduct",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a,
                                         const TinyVector<2> b) -> std::shared_ptr<const DiscreteFunctionVariant> {
                                        return tensorProduct(a, b);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("tensorProduct",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a,
                                         const TinyVector<3>& b) -> std::shared_ptr<const DiscreteFunctionVariant> {
                                        return tensorProduct(a, b);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("tensorProduct",
                                    std::function(

                                      [](const TinyVector<1> a, std::shared_ptr<const DiscreteFunctionVariant> b)
                                        -> std::shared_ptr<const DiscreteFunctionVariant> {
                                        return tensorProduct(a, b);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("tensorProduct",
                                    std::function(

                                      [](const TinyVector<2> a, std::shared_ptr<const DiscreteFunctionVariant> b)
                                        -> std::shared_ptr<const DiscreteFunctionVariant> {
                                        return tensorProduct(a, b);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("tensorProduct",
                                    std::function(

                                      [](const TinyVector<3>& a, std::shared_ptr<const DiscreteFunctionVariant> b)
                                        -> std::shared_ptr<const DiscreteFunctionVariant> {
                                        return tensorProduct(a, b);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("det", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> A)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return det(A); }

                                             ));

  scheme_module._addBuiltinFunction("inverse",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> A)
                                        -> std::shared_ptr<const DiscreteFunctionVariant> { return inverse(A); }

                                      ));

  scheme_module._addBuiltinFunction("trace", std::function(

                                               [](std::shared_ptr<const DiscreteFunctionVariant> A)
                                                 -> std::shared_ptr<const DiscreteFunctionVariant> { return trace(A); }

                                               ));
  scheme_module._addBuiltinFunction("min",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> double { return min(a); }

                                      ));

  scheme_module._addBuiltinFunction("transpose",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> A)
                                        -> std::shared_ptr<const DiscreteFunctionVariant> { return transpose(A); }

                                      ));

  scheme_module._addBuiltinFunction("min", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a,
                                                std::shared_ptr<const DiscreteFunctionVariant> b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return min(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("min", std::function(

                                             [](double a, std::shared_ptr<const DiscreteFunctionVariant> b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return min(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("min", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a, double b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return min(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("max",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> double { return max(a); }

                                      ));

  scheme_module._addBuiltinFunction("max", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a,
                                                std::shared_ptr<const DiscreteFunctionVariant> b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return max(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("max", std::function(

                                             [](double a, std::shared_ptr<const DiscreteFunctionVariant> b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return max(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("max", std::function(

                                             [](std::shared_ptr<const DiscreteFunctionVariant> a, double b)
                                               -> std::shared_ptr<const DiscreteFunctionVariant> { return max(a, b); }

                                             ));

  scheme_module._addBuiltinFunction("sum_of_R", std::function(

                                                  [](std::shared_ptr<const DiscreteFunctionVariant> a) -> double {
                                                    return sum_of<double>(a);
                                                  }

                                                  ));

  scheme_module._addBuiltinFunction("sum_of_R1",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyVector<1> {
                                        return sum_of<TinyVector<1>>(a);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("sum_of_R2",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyVector<2> {
                                        return sum_of<TinyVector<2>>(a);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("sum_of_R3",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyVector<3> {
                                        return sum_of<TinyVector<3>>(a);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("sum_of_R1x1",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyMatrix<1> {
                                        return sum_of<TinyMatrix<1>>(a);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("sum_of_R2x2",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyMatrix<2> {
                                        return sum_of<TinyMatrix<2>>(a);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("sum_of_R3x3",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyMatrix<3> {
                                        return sum_of<TinyMatrix<3>>(a);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("sum_of_Vh", std::function(

                                                   [](std::shared_ptr<const DiscreteFunctionVariant> a)
                                                     -> std::shared_ptr<const DiscreteFunctionVariant> {
                                                     return sum_of_Vh_components(a);
                                                   }

                                                   ));

  scheme_module._addBuiltinFunction("vectorize",
                                    std::function(

                                      [](const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>&
                                           discrete_function_list) -> std::shared_ptr<const DiscreteFunctionVariant> {
                                        return vectorize(discrete_function_list);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("integral_of_R", std::function(

                                                       [](std::shared_ptr<const DiscreteFunctionVariant> a) -> double {
                                                         return integral_of<double>(a);
                                                       }

                                                       ));

  scheme_module._addBuiltinFunction("integral_of_R1",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyVector<1> {
                                        return integral_of<TinyVector<1>>(a);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("integral_of_R2",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyVector<2> {
                                        return integral_of<TinyVector<2>>(a);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("integral_of_R3",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyVector<3> {
                                        return integral_of<TinyVector<3>>(a);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("integral_of_R1x1",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyMatrix<1> {
                                        return integral_of<TinyMatrix<1>>(a);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("integral_of_R2x2",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyMatrix<2> {
                                        return integral_of<TinyMatrix<2>>(a);
                                      }

                                      ));

  scheme_module._addBuiltinFunction("integral_of_R3x3",
                                    std::function(

                                      [](std::shared_ptr<const DiscreteFunctionVariant> a) -> TinyMatrix<3> {
                                        return integral_of<TinyMatrix<3>>(a);
                                      }

                                      ));
}
