#ifndef SCHEME_MODULE_HPP
#define SCHEME_MODULE_HPP

#include <language/modules/BuiltinModule.hpp>

class SchemeModule : public BuiltinModule
{
  friend class MathFunctionRegisterForVh;

 public:
  std::string_view
  name() const final
  {
    return "scheme";
  }

  void registerOperators() const final;
  void registerCheckpointResume() const final;

  SchemeModule();

  ~SchemeModule() = default;
};

#endif   // SCHEME_MODULE_HPP
