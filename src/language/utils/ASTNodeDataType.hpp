#ifndef AST_NODE_DATA_TYPE_HPP
#define AST_NODE_DATA_TYPE_HPP

#include <utils/PugsAssert.hpp>

#include <array>
#include <limits>
#include <memory>
#include <string>
#include <variant>
#include <vector>

class ASTNode;
class ASTNodeDataType;

ASTNodeDataType getVectorDataType(const ASTNode& type_node);
ASTNodeDataType getVectorExpressionType(const ASTNode& vector_expression_node);

ASTNodeDataType getMatrixDataType(const ASTNode& type_node);
ASTNodeDataType getMatrixExpressionType(const ASTNode& matrix_expression_node);

ASTNodeDataType getTupleDataType(const ASTNode& type_node);

std::string dataTypeName(const std::vector<ASTNodeDataType>& data_type_vector);

std::string dataTypeName(const ASTNodeDataType& data_type);

ASTNodeDataType dataTypePromotion(const ASTNodeDataType& data_type_1, const ASTNodeDataType& data_type_2);

bool isNaturalConversion(const ASTNodeDataType& data_type, const ASTNodeDataType& target_data_type);

class ASTNodeDataType
{
 public:
  enum DataType : int32_t
  {
    undefined_t        = -1,
    bool_t             = 0,
    int_t              = 1,
    unsigned_int_t     = 2,
    double_t           = 3,
    vector_t           = 4,
    matrix_t           = 5,
    tuple_t            = 6,
    list_t             = 7,
    string_t           = 8,
    typename_t         = 10,
    type_name_id_t     = 11,
    type_id_t          = 21,
    function_t         = 22,
    builtin_function_t = 23,
    void_t             = std::numeric_limits<int32_t>::max()
  };

 private:
  DataType m_data_type;

  using DataTypeDetails = std::variant<std::monostate,
                                       size_t,
                                       std::array<size_t, 2>,
                                       std::string,
                                       std::shared_ptr<const ASTNodeDataType>,
                                       std::vector<std::shared_ptr<const ASTNodeDataType>>>;

  DataTypeDetails m_details;

 public:
  PUGS_INLINE
  size_t
  dimension() const
  {
    Assert(std::holds_alternative<size_t>(m_details));
    return std::get<size_t>(m_details);
  }

  PUGS_INLINE
  size_t
  numberOfRows() const
  {
    Assert(std::holds_alternative<std::array<size_t, 2>>(m_details));
    return std::get<std::array<size_t, 2>>(m_details)[0];
  }

  PUGS_INLINE
  size_t
  numberOfColumns() const
  {
    Assert(std::holds_alternative<std::array<size_t, 2>>(m_details));
    return std::get<std::array<size_t, 2>>(m_details)[1];
  }

  PUGS_INLINE
  const std::string&
  nameOfTypeId() const
  {
    Assert(std::holds_alternative<std::string>(m_details));
    return std::get<std::string>(m_details);
  }

  PUGS_INLINE
  const ASTNodeDataType&
  contentType() const
  {
    Assert(std::holds_alternative<std::shared_ptr<const ASTNodeDataType>>(m_details));
    return *std::get<std::shared_ptr<const ASTNodeDataType>>(m_details);
  }

  PUGS_INLINE
  const std::vector<std::shared_ptr<const ASTNodeDataType>>&
  contentTypeList() const
  {
    Assert(std::holds_alternative<std::vector<std::shared_ptr<const ASTNodeDataType>>>(m_details));
    return std::get<std::vector<std::shared_ptr<const ASTNodeDataType>>>(m_details);
  }

  PUGS_INLINE
  operator const DataType&() const
  {
    return m_data_type;
  }

  ASTNodeDataType& operator=(const ASTNodeDataType&) = default;
  ASTNodeDataType& operator=(ASTNodeDataType&&)      = default;

  template <DataType data_type>
  [[nodiscard]] static ASTNodeDataType
  build()
  {
    static_assert(data_type != tuple_t, "tuple_t requires sub_type");
    static_assert(data_type != typename_t, "typename_t requires sub_type");
    static_assert(data_type != vector_t, "vector_t requires dimension");
    static_assert(data_type != type_id_t, "type_id_t requires name");
    static_assert(data_type != list_t, "list_t requires list of types");

    return ASTNodeDataType{data_type};
  }

  template <DataType data_type>
  [[nodiscard]] static ASTNodeDataType
  build(const ASTNodeDataType& content_type)
  {
    static_assert((data_type == tuple_t) or (data_type == typename_t),
                  "incorrect data_type construction: cannot have content");
    Assert(content_type != ASTNodeDataType::undefined_t);

    return ASTNodeDataType{data_type, content_type};
  }

  template <DataType data_type>
  [[nodiscard]] static ASTNodeDataType
  build(const size_t dimension)
  {
    static_assert((data_type == vector_t), "incorrect data_type construction: cannot have dimension");
    return ASTNodeDataType{data_type, dimension};
  }

  template <DataType data_type>
  [[nodiscard]] static ASTNodeDataType
  build(const size_t nb_rows, const size_t nb_columns)
  {
    static_assert((data_type == matrix_t), "incorrect data_type construction: cannot have dimension");
    return ASTNodeDataType{data_type, nb_rows, nb_columns};
  }

  template <DataType data_type>
  [[nodiscard]] static ASTNodeDataType
  build(const std::string& type_name)
  {
    static_assert((data_type == type_id_t), "incorrect data_type construction: cannot provide name of type");
    return ASTNodeDataType{data_type, type_name};
  }

  template <DataType data_type>
  [[nodiscard]] static ASTNodeDataType
  build(const std::vector<std::shared_ptr<const ASTNodeDataType>>& list_of_types)
  {
    static_assert((data_type == list_t), "incorrect data_type construction: cannot provide a list of data types");

    for (const auto& i : list_of_types) {
      Assert(i->m_data_type != ASTNodeDataType::undefined_t, "cannot build a type list containing undefined types");
    }

    return ASTNodeDataType{data_type, list_of_types};
  }

  ASTNodeDataType() : m_data_type{undefined_t} {}

  ASTNodeDataType(const ASTNodeDataType&) = default;

  ASTNodeDataType(ASTNodeDataType&&) = default;

  ~ASTNodeDataType() = default;

 private:
  explicit ASTNodeDataType(DataType data_type) : m_data_type{data_type} {}

  explicit ASTNodeDataType(DataType data_type, const ASTNodeDataType& content_type)
    : m_data_type{data_type}, m_details{std::make_shared<const ASTNodeDataType>(content_type)}
  {}

  explicit ASTNodeDataType(DataType data_type, const std::vector<std::shared_ptr<const ASTNodeDataType>>& list_of_types)
    : m_data_type{data_type}, m_details{list_of_types}
  {}

  explicit ASTNodeDataType(DataType data_type, const size_t dimension) : m_data_type{data_type}, m_details{dimension} {}

  explicit ASTNodeDataType(DataType data_type, const size_t nb_rows, const size_t nb_columns)
    : m_data_type{data_type}, m_details{std::array{nb_rows, nb_columns}}
  {}

  explicit ASTNodeDataType(DataType data_type, const std::string& type_name)
    : m_data_type{data_type}, m_details{type_name}
  {}
};

#endif   // AST_NODE_DATA_TYPE_HPP
