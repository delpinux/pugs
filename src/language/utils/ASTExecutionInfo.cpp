#include <language/utils/ASTExecutionInfo.hpp>

#include <language/ast/ASTNode.hpp>

const ASTExecutionInfo* ASTExecutionInfo::m_execution_info_instance = nullptr;

ASTExecutionInfo::ASTExecutionInfo(const ASTNode& root_node, const ModuleRepository& module_repository)
  : m_root_node{root_node}, m_module_repository{module_repository}
{
  Assert(m_execution_info_instance == nullptr, "Can only define one ASTExecutionInfo");

  m_execution_info_instance = this;
}

const ASTExecutionInfo&
ASTExecutionInfo::getInstance()
{
  Assert(m_execution_info_instance != nullptr, "ASTExecutionInfo is not defined!");
  return *m_execution_info_instance;
}

ASTExecutionInfo::~ASTExecutionInfo()
{
  m_execution_info_instance = nullptr;
}
