#include <language/utils/DataVariant.hpp>

#include <utils/PugsAssert.hpp>
#include <utils/PugsTraits.hpp>

std::ostream&
operator<<(std::ostream& os, const DataVariant& data_variant)
{
  std::visit(
    [&](auto&& v) {
      using ValueT = std::decay_t<decltype(v)>;
      if constexpr (std::is_same_v<ValueT, std::monostate>) {
        os << "--";
      } else if constexpr (is_std_vector_v<ValueT>) {
        os << '(';
        if constexpr (std::is_same_v<bool, typename ValueT::value_type>) {
          os << std::boolalpha;
        }
        if (v.size() > 0) {
          os << v[0];
        }
        for (size_t i = 1; i < v.size(); ++i) {
          os << ", " << v[i];
        }
        os << ')';
      } else if constexpr (std::is_same_v<bool, ValueT>) {
        os << std::boolalpha << v;
      } else {
        os << v;
      }
    },
    data_variant);

  return os;
}

std::ostream&
operator<<(std::ostream& os, const AggregateDataVariant& compound)
{
  Assert(compound.m_data_vector.size() > 0, "unexpected compound data size");
  os << '(';
  os << compound.m_data_vector[0];
  for (size_t i = 1; i < compound.m_data_vector.size(); ++i) {
    os << ", " << compound.m_data_vector[i];
  }
  os << ')';
  return os;
}
