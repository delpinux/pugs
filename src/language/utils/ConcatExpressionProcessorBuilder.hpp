#ifndef CONCAT_EXPRESSION_PROCESSOR_BUILDER_HPP
#define CONCAT_EXPRESSION_PROCESSOR_BUILDER_HPP

#include <language/PEGGrammar.hpp>
#include <language/node_processor/ConcatExpressionProcessor.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/IBinaryOperatorProcessorBuilder.hpp>

#include <type_traits>

template <typename A_DataT, typename B_DataT>
class ConcatExpressionProcessorBuilder final : public IBinaryOperatorProcessorBuilder
{
  static_assert(std::is_same_v<A_DataT, std::string> or std::is_same_v<B_DataT, std::string>,
                "one of the operand types must be an std::string");

 public:
  ConcatExpressionProcessorBuilder() = default;

  ASTNodeDataType
  getDataTypeOfA() const
  {
    return ast_node_data_type_from<A_DataT>;
  }

  ASTNodeDataType
  getDataTypeOfB() const
  {
    return ast_node_data_type_from<B_DataT>;
  }

  ASTNodeDataType
  getReturnValueType() const
  {
    return ast_node_data_type_from<std::string>;
  }

  std::unique_ptr<INodeProcessor>
  getNodeProcessor(ASTNode& node) const
  {
    return std::make_unique<ConcatExpressionProcessor<A_DataT, B_DataT>>(node);
  }
};

#endif   // CONCAT_EXPRESSION_PROCESSOR_BUILDER_HPP
