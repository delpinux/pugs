#ifndef AFFECTATION_REGISTER_FOR_RNXN_HPP
#define AFFECTATION_REGISTER_FOR_RNXN_HPP

#include <cstdlib>

template <size_t Dimension>
class AffectationRegisterForRnxn
{
 private:
  void _register_eq_op();
  void _register_pluseq_op();
  void _register_minuseq_op();
  void _register_multiplyeq_op();

 public:
  AffectationRegisterForRnxn();
};

#endif   // AFFECTATION_REGISTER_FOR_RNXN_HPP
