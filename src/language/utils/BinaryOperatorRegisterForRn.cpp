#include <language/utils/BinaryOperatorRegisterForRn.hpp>

#include <language/utils/BinaryOperatorProcessorBuilder.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/OStream.hpp>
#include <language/utils/OperatorRepository.hpp>

template <size_t Dimension>
void
BinaryOperatorRegisterForRn<Dimension>::_register_ostream()
{
  OperatorRepository& repository = OperatorRepository::instance();

  using Rn = TinyVector<Dimension>;

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, Rn>>());

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, std::vector<Rn>>>());
}

template <size_t Dimension>
void
BinaryOperatorRegisterForRn<Dimension>::_register_comparisons()
{
  OperatorRepository& repository = OperatorRepository::instance();

  using Rn = TinyVector<Dimension>;

  repository.addBinaryOperator<language::eqeq_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::eqeq_op, bool, Rn, Rn>>());

  repository.addBinaryOperator<language::not_eq_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::not_eq_op, bool, Rn, Rn>>());
}

template <size_t Dimension>
void
BinaryOperatorRegisterForRn<Dimension>::_register_product_by_a_scalar()
{
  OperatorRepository& repository = OperatorRepository::instance();

  using Rn = TinyVector<Dimension>;

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::multiply_op, Rn, bool, Rn>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::multiply_op, Rn, uint64_t, Rn>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::multiply_op, Rn, int64_t, Rn>>());

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::multiply_op, Rn, double, Rn>>());
}

template <size_t Dimension>
template <typename OperatorT>
void
BinaryOperatorRegisterForRn<Dimension>::_register_arithmetic()
{
  OperatorRepository& repository = OperatorRepository::instance();

  using Rn = TinyVector<Dimension>;

  repository.addBinaryOperator<OperatorT>(std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, Rn, Rn, Rn>>());
}

template <size_t Dimension>
BinaryOperatorRegisterForRn<Dimension>::BinaryOperatorRegisterForRn()
{
  this->_register_ostream();

  this->_register_comparisons();

  this->_register_product_by_a_scalar();

  this->_register_arithmetic<language::plus_op>();
  this->_register_arithmetic<language::minus_op>();
}

template class BinaryOperatorRegisterForRn<1>;
template class BinaryOperatorRegisterForRn<2>;
template class BinaryOperatorRegisterForRn<3>;
