#ifndef UNARY_OPERATOR_REGISTER_FOR_R_HPP
#define UNARY_OPERATOR_REGISTER_FOR_R_HPP

class UnaryOperatorRegisterForR
{
 private:
  void _register_unary_minus();

 public:
  UnaryOperatorRegisterForR();
};

#endif   // UNARY_OPERATOR_REGISTER_FOR_R_HPP
