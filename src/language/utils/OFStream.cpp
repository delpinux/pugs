#include <language/utils/OFStream.hpp>

#include <utils/Filesystem.hpp>
#include <utils/Messenger.hpp>

OFStream::OFStream(const std::string& filename, bool append)
  : OStream(OStream::Type::std_ofstream), m_filename{filename}
{
  if (parallel::rank() == 0) {
    createDirectoryIfNeeded(filename);
    if (append) {
      m_fstream.open(filename, std::ios_base::app);
    } else {
      m_fstream.open(filename);
    }
    if (m_fstream.is_open()) {
      m_ostream = &m_fstream;
    } else {
      std::stringstream error_msg;
      error_msg << "cannot create file " << rang::fgB::yellow << filename << rang::style::reset;
      throw NormalError(error_msg.str());
    }
  }
}
