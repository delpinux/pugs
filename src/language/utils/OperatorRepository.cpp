#include <language/utils/OperatorRepository.hpp>

#include <utils/PugsAssert.hpp>

OperatorRepository* OperatorRepository::m_instance = nullptr;

void
OperatorRepository::reset()
{
  m_affectation_builder_list.clear();
  m_binary_operator_builder_list.clear();
  m_inc_dec_operator_builder_list.clear();
  m_unary_operator_builder_list.clear();
}

void
OperatorRepository::create()
{
  Assert(m_instance == nullptr, "AffectationRepository was already created");
  m_instance = new OperatorRepository;
}

void
OperatorRepository::destroy()
{
  Assert(m_instance != nullptr, "AffectationRepository was not created");
  delete m_instance;
  m_instance = nullptr;
}
