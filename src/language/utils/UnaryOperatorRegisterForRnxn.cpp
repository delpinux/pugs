#include <language/utils/UnaryOperatorRegisterForRnxn.hpp>

#include <language/utils/OperatorRepository.hpp>
#include <language/utils/UnaryOperatorProcessorBuilder.hpp>

template <size_t Dimension>
void
UnaryOperatorRegisterForRnxn<Dimension>::_register_unary_minus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addUnaryOperator<language::unary_minus>(
    std::make_shared<
      UnaryOperatorProcessorBuilder<language::unary_minus, TinyMatrix<Dimension>, TinyMatrix<Dimension>>>());
}

template <size_t Dimension>
UnaryOperatorRegisterForRnxn<Dimension>::UnaryOperatorRegisterForRnxn()
{
  this->_register_unary_minus();
}

template class UnaryOperatorRegisterForRnxn<1>;
template class UnaryOperatorRegisterForRnxn<2>;
template class UnaryOperatorRegisterForRnxn<3>;
