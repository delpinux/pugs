#ifndef EMBEDDER_TABLE_HPP
#define EMBEDDER_TABLE_HPP

#include <utils/PugsAssert.hpp>

#include <memory>
#include <vector>

template <typename EmbedderT>
class EmbedderTable
{
 private:
  std::vector<std::shared_ptr<EmbedderT>> m_embedder_list;

 public:
  PUGS_INLINE
  size_t
  size() const
  {
    return m_embedder_list.size();
  }

  PUGS_INLINE
  const std::shared_ptr<EmbedderT>&
  operator[](size_t embedder_id) const
  {
    Assert(embedder_id < m_embedder_list.size());
    return m_embedder_list[embedder_id];
  }

  void
  add(std::shared_ptr<EmbedderT> embedder)
  {
    m_embedder_list.push_back(embedder);
  }

  EmbedderTable()  = default;
  ~EmbedderTable() = default;
};

#endif   // EMBEDDER_TABLE_HPP
