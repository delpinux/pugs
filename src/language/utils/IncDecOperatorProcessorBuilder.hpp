#ifndef INC_DEC_OPERATOR_PROCESSOR_BUILDER_HPP
#define INC_DEC_OPERATOR_PROCESSOR_BUILDER_HPP

#include <algebra/TinyVector.hpp>
#include <language/PEGGrammar.hpp>
#include <language/node_processor/IncDecExpressionProcessor.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/IIncDecOperatorProcessorBuilder.hpp>

#include <type_traits>

template <typename OperatorT, typename DataT>
class IncDecOperatorProcessorBuilder final : public IIncDecOperatorProcessorBuilder
{
 public:
  IncDecOperatorProcessorBuilder() = default;

  ASTNodeDataType
  getReturnValueType() const
  {
    return ast_node_data_type_from<DataT>;
  }

  std::unique_ptr<INodeProcessor>
  getNodeProcessor(ASTNode& node) const
  {
    return std::make_unique<IncDecExpressionProcessor<OperatorT, DataT>>(node);
  }
};

#endif   // INC_DEC_OPERATOR_PROCESSOR_BUILDER_HPP
