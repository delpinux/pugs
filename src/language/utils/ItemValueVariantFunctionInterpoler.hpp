#ifndef ITEM_VALUE_VARIANT_FUNCTION_INTERPOLER_HPP
#define ITEM_VALUE_VARIANT_FUNCTION_INTERPOLER_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IZoneDescriptor.hpp>
#include <mesh/ItemType.hpp>
#include <mesh/ItemValueVariant.hpp>
#include <mesh/MeshTraits.hpp>

class MeshVariant;

class ItemValueVariantFunctionInterpoler
{
 private:
  std::shared_ptr<const MeshVariant> m_mesh_v;
  const ItemType m_item_type;
  const FunctionSymbolId m_function_id;

  template <MeshConcept MeshType, typename DataType>
  std::shared_ptr<ItemValueVariant> _interpolate() const;

  template <MeshConcept MeshType>
  std::shared_ptr<ItemValueVariant> _interpolate() const;

 public:
  std::shared_ptr<ItemValueVariant> interpolate() const;

  ItemValueVariantFunctionInterpoler(const std::shared_ptr<const MeshVariant>& mesh_v,
                                     const ItemType& item_type,
                                     const FunctionSymbolId& function_id)
    : m_mesh_v{mesh_v}, m_item_type{item_type}, m_function_id{function_id}
  {}

  ItemValueVariantFunctionInterpoler(const ItemValueVariantFunctionInterpoler&) = delete;
  ItemValueVariantFunctionInterpoler(ItemValueVariantFunctionInterpoler&&)      = delete;

  ~ItemValueVariantFunctionInterpoler() = default;
};

#endif   // ITEM_VALUE_VARIANT_FUNCTION_INTERPOLER_HPP
