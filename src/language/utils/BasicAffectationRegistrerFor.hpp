#ifndef BASIC_AFFECTATION_REGISTRER_FOR_HPP
#define BASIC_AFFECTATION_REGISTRER_FOR_HPP

#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/AffectationProcessorBuilder.hpp>
#include <language/utils/OperatorRepository.hpp>

template <typename T>
class BasicAffectationRegisterFor
{
 public:
  BasicAffectationRegisterFor() : BasicAffectationRegisterFor(ast_node_data_type_from<T>) {}

  BasicAffectationRegisterFor(const ASTNodeDataType& ast_node_data_type)
  {
    OperatorRepository& repository = OperatorRepository::instance();

    repository.addAffectation<language::eq_op>(ast_node_data_type, ast_node_data_type,
                                               std::make_shared<AffectationProcessorBuilder<language::eq_op, T, T>>());

    repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(ast_node_data_type),
                                               ast_node_data_type,
                                               std::make_shared<AffectationToTupleProcessorBuilder<T>>());

    repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(ast_node_data_type),
                                               ASTNodeDataType::build<ASTNodeDataType::list_t>(
                                                 std::vector<std::shared_ptr<const ASTNodeDataType>>{}),
                                               std::make_shared<AffectationToTupleFromListProcessorBuilder<T>>());

    repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(ast_node_data_type),
                                               ASTNodeDataType::build<ASTNodeDataType::tuple_t>(ast_node_data_type),
                                               std::make_shared<AffectationToTupleFromTupleProcessorBuilder<T>>());

    repository.addAffectation<language::eq_op>(ast_node_data_type,
                                               ASTNodeDataType::build<ASTNodeDataType::tuple_t>(ast_node_data_type),
                                               std::make_shared<AffectationFromTupleProcessorBuilder<T>>());
  }
};

#endif   // BASIC_AFFECTATION_REGISTRER_FOR_HPP
