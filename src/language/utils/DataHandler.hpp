#ifndef DATA_HANDLER_HPP
#define DATA_HANDLER_HPP

#include <utils/PugsAssert.hpp>

#include <memory>

class IDataHandler
{
 public:
  IDataHandler(const IDataHandler&) = delete;
  IDataHandler(IDataHandler&&)      = delete;

  IDataHandler() = default;

  virtual ~IDataHandler() = default;
};

template <typename DataT>
class DataHandler : public IDataHandler
{
 private:
  std::shared_ptr<DataT> m_data;

 public:
  std::shared_ptr<DataT>
  data_ptr() const
  {
    return m_data;
  }

  DataHandler(std::shared_ptr<DataT> data) : m_data(data) {}
  ~DataHandler() = default;
};

#endif   // DATA_HANDLER_HPP
