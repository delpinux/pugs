#ifndef AFFECTATION_REGISTER_FOR_R_HPP
#define AFFECTATION_REGISTER_FOR_R_HPP

class AffectationRegisterForR
{
 private:
  void _register_eq_op();
  void _register_pluseq_op();
  void _register_minuseq_op();
  void _register_multiplyeq_op();
  void _register_divideeq_op();

 public:
  AffectationRegisterForR();
};

#endif   // AFFECTATION_REGISTER_FOR_R_HPP
