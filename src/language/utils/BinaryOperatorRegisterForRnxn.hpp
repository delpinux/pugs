#ifndef BINARY_OPERATOR_REGISTER_FOR_RNXN_HPP
#define BINARY_OPERATOR_REGISTER_FOR_RNXN_HPP

#include <cstdlib>

template <size_t Dimension>
class BinaryOperatorRegisterForRnxn
{
 private:
  void _register_ostream();
  void _register_comparisons();
  void _register_product_by_a_scalar();
  void _register_product_by_a_vector();
  template <typename OperatorT>
  void _register_arithmetic();

 public:
  BinaryOperatorRegisterForRnxn();
};

#endif   // BINARY_OPERATOR_REGISTER_FOR_RNXN_HPP
