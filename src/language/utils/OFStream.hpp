#ifndef OFSTREAM_HPP
#define OFSTREAM_HPP

#include <language/utils/OStream.hpp>

#include <fstream>

class OFStream final : public OStream
{
 private:
  std::string m_filename;
  std::ofstream m_fstream;

 public:
  const std::string&
  filename() const
  {
    return m_filename;
  }

  OFStream(const std::string& filename, bool append = false);

  OFStream()  = delete;
  ~OFStream() = default;
};

#endif   // OFSTREAM_HPP
