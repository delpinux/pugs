#include <language/utils/AffectationRegisterForN.hpp>

#include <language/utils/AffectationProcessorBuilder.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/OperatorRepository.hpp>

void
AffectationRegisterForN::_register_eq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto B = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository
    .addAffectation<language::eq_op>(N, B,
                                     std::make_shared<AffectationProcessorBuilder<language::eq_op, uint64_t, bool>>());

  repository.addAffectation<
    language::eq_op>(N, Z, std::make_shared<AffectationProcessorBuilder<language::eq_op, uint64_t, int64_t>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N), B,
                                             std::make_shared<AffectationToTupleProcessorBuilder<uint64_t>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N), Z,
                                             std::make_shared<AffectationToTupleProcessorBuilder<uint64_t>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                             ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                             std::make_shared<AffectationToTupleFromTupleProcessorBuilder<uint64_t>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                             ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                             std::make_shared<AffectationToTupleFromTupleProcessorBuilder<uint64_t>>());

  repository.addAffectation<language::eq_op>(N, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<uint64_t>>());

  repository.addAffectation<language::eq_op>(N, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<uint64_t>>());
}

void
AffectationRegisterForN::_register_pluseq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto B = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addAffectation<
    language::pluseq_op>(N, B, std::make_shared<AffectationProcessorBuilder<language::pluseq_op, uint64_t, bool>>());

  repository.addAffectation<
    language::pluseq_op>(N, N,
                         std::make_shared<AffectationProcessorBuilder<language::pluseq_op, uint64_t, uint64_t>>());

  repository.addAffectation<
    language::pluseq_op>(N, Z, std::make_shared<AffectationProcessorBuilder<language::pluseq_op, uint64_t, int64_t>>());
}

void
AffectationRegisterForN::_register_minuseq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto B = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addAffectation<
    language::minuseq_op>(N, B, std::make_shared<AffectationProcessorBuilder<language::minuseq_op, uint64_t, bool>>());

  repository.addAffectation<
    language::minuseq_op>(N, N,
                          std::make_shared<AffectationProcessorBuilder<language::minuseq_op, uint64_t, uint64_t>>());

  repository.addAffectation<
    language::minuseq_op>(N, Z,
                          std::make_shared<AffectationProcessorBuilder<language::minuseq_op, uint64_t, int64_t>>());
}

void
AffectationRegisterForN::_register_multiplyeq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto B = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addAffectation<
    language::multiplyeq_op>(N, B,
                             std::make_shared<AffectationProcessorBuilder<language::multiplyeq_op, uint64_t, bool>>());

  repository.addAffectation<language::multiplyeq_op>(N, N,
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, uint64_t, uint64_t>>());

  repository.addAffectation<language::multiplyeq_op>(N, Z,
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, uint64_t, int64_t>>());
}

void
AffectationRegisterForN::_register_divideeq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addAffectation<
    language::divideeq_op>(N, N,
                           std::make_shared<AffectationProcessorBuilder<language::divideeq_op, uint64_t, uint64_t>>());

  repository.addAffectation<
    language::divideeq_op>(N, Z,
                           std::make_shared<AffectationProcessorBuilder<language::divideeq_op, uint64_t, int64_t>>());
}

AffectationRegisterForN::AffectationRegisterForN()
{
  BasicAffectationRegisterFor<uint64_t>{};

  this->_register_eq_op();
  this->_register_pluseq_op();
  this->_register_minuseq_op();
  this->_register_multiplyeq_op();
  this->_register_divideeq_op();
}
