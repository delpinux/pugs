#ifndef EMBEDDED_DISCRETE_FUNCTION_MATH_FUNCTIONS_HPP
#define EMBEDDED_DISCRETE_FUNCTION_MATH_FUNCTIONS_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>

#include <memory>

class DiscreteFunctionVariant;

std::shared_ptr<const DiscreteFunctionVariant> sqrt(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> sqrt(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> abs(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> sin(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> cos(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> tan(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> asin(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> acos(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> atan(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> atan2(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                     const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> atan2(const double,
                                                     const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> atan2(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                     const double);

std::shared_ptr<const DiscreteFunctionVariant> sinh(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> cosh(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> tanh(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> asinh(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> acosh(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> atanh(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> exp(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> log(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> pow(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                   const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> pow(const double, const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> pow(const std::shared_ptr<const DiscreteFunctionVariant>&, const double);

std::shared_ptr<const DiscreteFunctionVariant> dot(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                   const std::shared_ptr<const DiscreteFunctionVariant>&);

template <size_t VectorDimension>
std::shared_ptr<const DiscreteFunctionVariant> dot(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                   const TinyVector<VectorDimension>&);

template <size_t VectorDimension>
std::shared_ptr<const DiscreteFunctionVariant> dot(const TinyVector<VectorDimension>&,
                                                   const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> doubleDot(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

template <size_t SquareMatrixNbRow>
std::shared_ptr<const DiscreteFunctionVariant> doubleDot(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyMatrix<SquareMatrixNbRow>&);

template <size_t SquareMatrixNbRow>
std::shared_ptr<const DiscreteFunctionVariant> doubleDot(const TinyMatrix<SquareMatrixNbRow>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> tensorProduct(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                             const std::shared_ptr<const DiscreteFunctionVariant>&);

template <size_t VectorDimension>
std::shared_ptr<const DiscreteFunctionVariant> tensorProduct(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                             const TinyVector<VectorDimension>&);

template <size_t VectorDimension>
std::shared_ptr<const DiscreteFunctionVariant> tensorProduct(const TinyVector<VectorDimension>&,
                                                             const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> det(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> trace(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> inverse(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> transpose(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> sum_of_Vh_components(
  const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> vectorize(
  const std::vector<std::shared_ptr<const DiscreteFunctionVariant>>& discrete_function_list);

double min(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> min(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                   const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> min(const double, const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> min(const std::shared_ptr<const DiscreteFunctionVariant>&, const double);

double max(const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> max(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                   const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> max(const double, const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> max(const std::shared_ptr<const DiscreteFunctionVariant>&, const double);

template <typename ValueT>
ValueT sum_of(const std::shared_ptr<const DiscreteFunctionVariant>&);

template <typename ValueT>
ValueT integral_of(const std::shared_ptr<const DiscreteFunctionVariant>&);

#endif   // EMBEDDED_DISCRETE_FUNCTION_MATH_FUNCTIONS_HPP
