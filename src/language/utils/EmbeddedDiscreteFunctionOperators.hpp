#ifndef EMBEDDED_DISCRETE_FUNCTION_OPERATORS_HPP
#define EMBEDDED_DISCRETE_FUNCTION_OPERATORS_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>

#include <memory>

class DiscreteFunctionVariant;

// unary minus
std::shared_ptr<const DiscreteFunctionVariant> operator-(const std::shared_ptr<const DiscreteFunctionVariant>&);

// sum
std::shared_ptr<const DiscreteFunctionVariant> operator+(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const double&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const double&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const TinyVector<1>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const TinyVector<2>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const TinyVector<3>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const TinyMatrix<1>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const TinyMatrix<2>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const TinyMatrix<3>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyVector<1>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyVector<2>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyVector<3>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyMatrix<1>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyMatrix<2>&);

std::shared_ptr<const DiscreteFunctionVariant> operator+(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyMatrix<3>&);

// difference
std::shared_ptr<const DiscreteFunctionVariant> operator-(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const double&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const double&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const TinyVector<1>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const TinyVector<2>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const TinyVector<3>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const TinyMatrix<1>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const TinyMatrix<2>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const TinyMatrix<3>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyVector<1>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyVector<2>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyVector<3>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyMatrix<1>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyMatrix<2>&);

std::shared_ptr<const DiscreteFunctionVariant> operator-(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyMatrix<3>&);

// product
std::shared_ptr<const DiscreteFunctionVariant> operator*(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator*(const double&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator*(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const double&);

std::shared_ptr<const DiscreteFunctionVariant> operator*(const TinyMatrix<1>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator*(const TinyMatrix<2>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator*(const TinyMatrix<3>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator*(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyVector<1>&);

std::shared_ptr<const DiscreteFunctionVariant> operator*(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyVector<2>&);

std::shared_ptr<const DiscreteFunctionVariant> operator*(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyVector<3>&);

std::shared_ptr<const DiscreteFunctionVariant> operator*(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyMatrix<1>&);

std::shared_ptr<const DiscreteFunctionVariant> operator*(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyMatrix<2>&);

std::shared_ptr<const DiscreteFunctionVariant> operator*(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const TinyMatrix<3>&);

// ratio
std::shared_ptr<const DiscreteFunctionVariant> operator/(const double&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

std::shared_ptr<const DiscreteFunctionVariant> operator/(const std::shared_ptr<const DiscreteFunctionVariant>&,
                                                         const std::shared_ptr<const DiscreteFunctionVariant>&);

#endif   // EMBEDDED_DISCRETE_FUNCTION_OPERATORS_HPP
