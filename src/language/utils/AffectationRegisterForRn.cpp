#include <language/utils/AffectationRegisterForRn.hpp>

#include <language/utils/AffectationProcessorBuilder.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/OperatorRepository.hpp>

template <size_t Dimension>
void
AffectationRegisterForRn<Dimension>::_register_eq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Rn = ASTNodeDataType::build<ASTNodeDataType::vector_t>(Dimension);
  auto Z  = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addAffectation<
    language::eq_op>(Rn, Z,
                     std::make_shared<AffectationFromZeroProcessorBuilder<language::eq_op, TinyVector<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Rn), Z,
                                     std::make_shared<AffectationToTupleProcessorBuilder<TinyVector<Dimension>>>());
}

template <>
void
AffectationRegisterForRn<1>::_register_eq_op()
{
  constexpr size_t Dimension = 1;

  OperatorRepository& repository = OperatorRepository::instance();

  auto B = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();
  auto R = ASTNodeDataType::build<ASTNodeDataType::double_t>();

  auto R1 = ASTNodeDataType::build<ASTNodeDataType::vector_t>(Dimension);

  repository.addAffectation<
    language::eq_op>(R1, B,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, TinyVector<Dimension>, bool>>());

  repository.addAffectation<
    language::eq_op>(R1, N,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, TinyVector<Dimension>, uint64_t>>());

  repository.addAffectation<
    language::eq_op>(R1, Z,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, TinyVector<Dimension>, int64_t>>());

  repository.addAffectation<
    language::eq_op>(R1, R,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, TinyVector<Dimension>, double>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1), B,
                                     std::make_shared<AffectationToTupleProcessorBuilder<TinyVector<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1), N,
                                     std::make_shared<AffectationToTupleProcessorBuilder<TinyVector<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1), Z,
                                     std::make_shared<AffectationToTupleProcessorBuilder<TinyVector<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1), R,
                                     std::make_shared<AffectationToTupleProcessorBuilder<TinyVector<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<TinyVector<1>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<TinyVector<1>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<TinyVector<1>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<TinyVector<1>>>());

  repository
    .addAffectation<language::eq_op>(R1, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                     std::make_shared<AffectationFromTupleProcessorBuilder<TinyVector<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(R1, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                     std::make_shared<AffectationFromTupleProcessorBuilder<TinyVector<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(R1, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                     std::make_shared<AffectationFromTupleProcessorBuilder<TinyVector<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(R1, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R),
                                     std::make_shared<AffectationFromTupleProcessorBuilder<TinyVector<Dimension>>>());
}

template <size_t Dimension>
void
AffectationRegisterForRn<Dimension>::_register_pluseq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Rn = ASTNodeDataType::build<ASTNodeDataType::vector_t>(Dimension);

  repository
    .addAffectation<language::pluseq_op>(Rn, Rn,
                                         std::make_shared<AffectationProcessorBuilder<
                                           language::pluseq_op, TinyVector<Dimension>, TinyVector<Dimension>>>());
}

template <size_t Dimension>
void
AffectationRegisterForRn<Dimension>::_register_minuseq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Rn = ASTNodeDataType::build<ASTNodeDataType::vector_t>(Dimension);

  repository
    .addAffectation<language::minuseq_op>(Rn, Rn,
                                          std::make_shared<AffectationProcessorBuilder<
                                            language::minuseq_op, TinyVector<Dimension>, TinyVector<Dimension>>>());
}

template <size_t Dimension>
void
AffectationRegisterForRn<Dimension>::_register_multiplyeq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Rn = ASTNodeDataType::build<ASTNodeDataType::vector_t>(Dimension);

  auto B = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();
  auto R = ASTNodeDataType::build<ASTNodeDataType::double_t>();

  repository.addAffectation<language::multiplyeq_op>(Rn, B,
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, TinyVector<Dimension>, bool>>());

  repository.addAffectation<language::multiplyeq_op>(Rn, N,
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, TinyVector<Dimension>, uint64_t>>());

  repository.addAffectation<language::multiplyeq_op>(Rn, Z,
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, TinyVector<Dimension>, int64_t>>());

  repository.addAffectation<language::multiplyeq_op>(Rn, R,
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, TinyVector<Dimension>, double>>());
}

template <size_t Dimension>
AffectationRegisterForRn<Dimension>::AffectationRegisterForRn()
{
  BasicAffectationRegisterFor<TinyVector<Dimension>>{};
  this->_register_eq_op();
  this->_register_pluseq_op();
  this->_register_minuseq_op();
  this->_register_multiplyeq_op();
}

template class AffectationRegisterForRn<1>;
template class AffectationRegisterForRn<2>;
template class AffectationRegisterForRn<3>;
