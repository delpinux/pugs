#ifndef INTEGRATE_CELL_VALUE_HPP
#define INTEGRATE_CELL_VALUE_HPP

#include <analysis/IQuadratureDescriptor.hpp>
#include <language/utils/IntegrateOnCells.hpp>
#include <mesh/ItemType.hpp>
#include <mesh/ItemValue.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>

template <typename T>
class IntegrateCellValue;
template <typename OutputType, typename InputType>
class IntegrateCellValue<OutputType(InputType)>
{
 public:
  template <MeshConcept MeshType>
  PUGS_INLINE static CellValue<OutputType>
  integrate(const FunctionSymbolId& function_symbol_id,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh)
  {
    static_assert(is_polygonal_mesh_v<MeshType>);
    CellValue<OutputType> value(mesh.connectivity());
    IntegrateOnCells<OutputType(const InputType)>::template integrateTo<MeshType>(function_symbol_id,
                                                                                  quadrature_descriptor, mesh, value);

    return value;
  }

  PUGS_INLINE static CellValue<OutputType>
  integrate(const FunctionSymbolId& function_symbol_id,
            const IQuadratureDescriptor& quadrature_descriptor,
            const std::shared_ptr<const MeshVariant>& mesh_v)
  {
    return std::visit(
      [&](auto&& p_mesh) -> CellValue<OutputType> {
        using MeshType = mesh_type_t<decltype(p_mesh)>;
        if constexpr ((is_polygonal_mesh_v<MeshType>)and(MeshType::Dimension == InputType::Dimension)) {
          return integrate(function_symbol_id, quadrature_descriptor, *p_mesh);
        } else {
          // LCOV_EXCL_START
          throw NormalError("invalid mesh type");
          // LCOV_EXCL_STOP
        }
      },
      mesh_v->variant());
  }

  template <MeshConcept MeshType>
  PUGS_INLINE static Array<OutputType>
  integrate(const FunctionSymbolId& function_symbol_id,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh,
            const Array<const CellId>& list_of_cells)
  {
    static_assert(is_polygonal_mesh_v<MeshType>);
    return IntegrateOnCells<OutputType(const InputType)>::integrate(function_symbol_id, quadrature_descriptor, mesh,
                                                                    Array<const CellId>{list_of_cells});
  }

  PUGS_INLINE static Array<OutputType>
  integrate(const FunctionSymbolId& function_symbol_id,
            const IQuadratureDescriptor& quadrature_descriptor,
            const std::shared_ptr<const MeshVariant>& mesh_v,
            const Array<const CellId>& list_of_cells)
  {
    return std::visit(
      [&](auto&& p_mesh) -> Array<OutputType> {
        using MeshType = mesh_type_t<decltype(p_mesh)>;
        if constexpr ((is_polygonal_mesh_v<MeshType>)and(MeshType::Dimension == InputType::Dimension)) {
          return integrate(function_symbol_id, quadrature_descriptor, *p_mesh, list_of_cells);
        } else {
          // LCOV_EXCL_START
          throw NormalError("invalid mesh type");
          // LCOV_EXCL_STOP
        }
      },
      mesh_v->variant());
  }

  template <MeshConcept MeshType>
  PUGS_INLINE static Array<OutputType>
  integrate(const FunctionSymbolId& function_symbol_id,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh,
            const Array<CellId>& list_of_cells)
  {
    static_assert(is_polygonal_mesh_v<MeshType>);
    return integrate(function_symbol_id, quadrature_descriptor, mesh, Array<const CellId>{list_of_cells});
  }

  PUGS_INLINE static Array<OutputType>
  integrate(const FunctionSymbolId& function_symbol_id,
            const IQuadratureDescriptor& quadrature_descriptor,
            const std::shared_ptr<const MeshVariant>& mesh_v,
            const Array<CellId>& list_of_cells)
  {
    return std::visit(
      [&](auto&& p_mesh) -> Array<OutputType> {
        using MeshType = mesh_type_t<decltype(p_mesh)>;
        if constexpr ((is_polygonal_mesh_v<MeshType>)and(MeshType::Dimension == InputType::Dimension)) {
          return integrate(function_symbol_id, quadrature_descriptor, *p_mesh, list_of_cells);
        } else {
          // LCOV_EXCL_START
          throw NormalError("invalid mesh type");
          // LCOV_EXCL_STOP
        }
      },
      mesh_v->variant());
  }
};

#endif   // INTEGRATE_CELL_VALUE_HPP
