#include <language/utils/UnaryOperatorRegisterForR.hpp>

#include <language/utils/OperatorRepository.hpp>
#include <language/utils/UnaryOperatorProcessorBuilder.hpp>

void
UnaryOperatorRegisterForR::_register_unary_minus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addUnaryOperator<language::unary_minus>(
    std::make_shared<UnaryOperatorProcessorBuilder<language::unary_minus, double, double>>());
}

UnaryOperatorRegisterForR::UnaryOperatorRegisterForR()
{
  this->_register_unary_minus();
}
