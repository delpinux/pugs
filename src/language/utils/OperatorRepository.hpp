#ifndef OPERATOR_REPOSITORY_HPP
#define OPERATOR_REPOSITORY_HPP

#include <language/node_processor/INodeProcessor.hpp>
#include <language/utils/ASTNodeDataType.hpp>
#include <language/utils/AffectationMangler.hpp>
#include <language/utils/BinaryOperatorMangler.hpp>
#include <language/utils/IAffectationProcessorBuilder.hpp>
#include <language/utils/IBinaryOperatorProcessorBuilder.hpp>
#include <language/utils/IIncDecOperatorProcessorBuilder.hpp>
#include <language/utils/IUnaryOperatorProcessorBuilder.hpp>
#include <language/utils/IncDecOperatorMangler.hpp>
#include <language/utils/UnaryOperatorMangler.hpp>

#include <utils/Exceptions.hpp>

#include <optional>
#include <unordered_map>

class OperatorRepository
{
 private:
  template <typename ProcessorBuilderT>
  class Descriptor
  {
   private:
    ASTNodeDataType m_value_type;
    std::shared_ptr<const ProcessorBuilderT> m_processor_builder;

   public:
    const ASTNodeDataType&
    valueType() const
    {
      return m_value_type;
    }

    const std::shared_ptr<const ProcessorBuilderT>&
    processorBuilder() const
    {
      return m_processor_builder;
    }

    Descriptor(const ASTNodeDataType& value_type, const std::shared_ptr<const ProcessorBuilderT>& processor_builder)
      : m_value_type{value_type}, m_processor_builder{processor_builder}
    {}

    Descriptor(const Descriptor&) = default;
    Descriptor(Descriptor&&)      = default;
    Descriptor()                  = default;
    ~Descriptor()                 = default;
  };

  std::unordered_map<std::string, Descriptor<IAffectationProcessorBuilder>> m_affectation_builder_list;

  std::unordered_map<std::string, Descriptor<IIncDecOperatorProcessorBuilder>> m_inc_dec_operator_builder_list;

  std::unordered_map<std::string, Descriptor<IBinaryOperatorProcessorBuilder>> m_binary_operator_builder_list;

  std::unordered_map<std::string, Descriptor<IUnaryOperatorProcessorBuilder>> m_unary_operator_builder_list;

 public:
  void reset();

  template <typename BinaryOperatorTypeT>
  void
  addBinaryOperator(const std::shared_ptr<const IBinaryOperatorProcessorBuilder>& processor_builder)
  {
    const std::string binary_operator_type_name =
      binaryOperatorMangler<BinaryOperatorTypeT>(processor_builder->getDataTypeOfA(),
                                                 processor_builder->getDataTypeOfB());
    if (auto [i_descriptor, success] =
          m_binary_operator_builder_list.try_emplace(binary_operator_type_name,
                                                     Descriptor{processor_builder->getReturnValueType(),
                                                                processor_builder});
        not success) {
      // LCOV_EXCL_START
      throw UnexpectedError(binary_operator_type_name + " has already an entry");
      // LCOV_EXCL_STOP
    }
  }

  template <typename OperatorTypeT>
  void
  addAffectation(const ASTNodeDataType& lhs_type,
                 const ASTNodeDataType& rhs_type,
                 const std::shared_ptr<const IAffectationProcessorBuilder>& processor_builder)
  {
    const std::string affectation_type_name = affectationMangler<OperatorTypeT>(lhs_type, rhs_type);
    if (auto [i_descriptor, success] =
          m_affectation_builder_list.try_emplace(affectation_type_name,
                                                 Descriptor{ASTNodeDataType::build<ASTNodeDataType::void_t>(),
                                                            processor_builder});
        not success) {
      // LCOV_EXCL_START
      throw UnexpectedError(affectation_type_name + " has already an entry");
      // LCOV_EXCL_STOP
    }
  }

  template <typename OperatorTypeT>
  void
  addIncDecOperator(const ASTNodeDataType& operand_type,
                    const std::shared_ptr<const IIncDecOperatorProcessorBuilder>& processor_builder)
  {
    const std::string inc_dec_operator_type_name = incDecOperatorMangler<OperatorTypeT>(operand_type);
    if (auto [i_descriptor, success] =
          m_inc_dec_operator_builder_list.try_emplace(inc_dec_operator_type_name,
                                                      Descriptor{processor_builder->getReturnValueType(),
                                                                 processor_builder});
        not success) {
      // LCOV_EXCL_START
      throw UnexpectedError(inc_dec_operator_type_name + " has already an entry");
      // LCOV_EXCL_STOP
    }
  }

  template <typename OperatorTypeT>
  void
  addUnaryOperator(const std::shared_ptr<const IUnaryOperatorProcessorBuilder>& processor_builder)
  {
    const std::string unary_operator_type_name =
      unaryOperatorMangler<OperatorTypeT>(processor_builder->getOperandDataType());
    if (auto [i_descriptor, success] =
          m_unary_operator_builder_list.try_emplace(unary_operator_type_name,
                                                    Descriptor{processor_builder->getReturnValueType(),
                                                               processor_builder});
        not success) {
      // LCOV_EXCL_START
      throw UnexpectedError(unary_operator_type_name + " has already an entry");
      // LCOV_EXCL_STOP
    }
  }

  [[nodiscard]] std::optional<std::shared_ptr<const IAffectationProcessorBuilder>>
  getAffectationProcessorBuilder(const std::string& name) const
  {
    auto&& processor_builder = m_affectation_builder_list.find(name);
    if (processor_builder != m_affectation_builder_list.end()) {
      return processor_builder->second.processorBuilder();
    }
    return {};
  }

  [[nodiscard]] std::optional<std::shared_ptr<const IIncDecOperatorProcessorBuilder>>
  getIncDecProcessorBuilder(const std::string& name) const
  {
    auto&& processor_builder = m_inc_dec_operator_builder_list.find(name);
    if (processor_builder != m_inc_dec_operator_builder_list.end()) {
      return processor_builder->second.processorBuilder();
    }
    return {};
  }

  [[nodiscard]] std::optional<std::shared_ptr<const IBinaryOperatorProcessorBuilder>>
  getBinaryProcessorBuilder(const std::string& name) const
  {
    auto&& processor_builder = m_binary_operator_builder_list.find(name);
    if (processor_builder != m_binary_operator_builder_list.end()) {
      return processor_builder->second.processorBuilder();
    }
    return {};
  }

  [[nodiscard]] std::optional<std::shared_ptr<const IUnaryOperatorProcessorBuilder>>
  getUnaryProcessorBuilder(const std::string& name) const
  {
    auto&& processor_builder = m_unary_operator_builder_list.find(name);
    if (processor_builder != m_unary_operator_builder_list.end()) {
      return processor_builder->second.processorBuilder();
    }
    return {};
  }

  [[nodiscard]] std::optional<ASTNodeDataType>
  getIncDecOperatorValueType(const std::string& name) const
  {
    auto&& processor_builder = m_inc_dec_operator_builder_list.find(name);
    if (processor_builder != m_inc_dec_operator_builder_list.end()) {
      return processor_builder->second.valueType();
    }
    return {};
  }

  [[nodiscard]] std::optional<ASTNodeDataType>
  getBinaryOperatorValueType(const std::string& name) const
  {
    auto&& processor_builder = m_binary_operator_builder_list.find(name);
    if (processor_builder != m_binary_operator_builder_list.end()) {
      return processor_builder->second.valueType();
    }
    return {};
  }

  [[nodiscard]] std::optional<ASTNodeDataType>
  getUnaryOperatorValueType(const std::string& name) const
  {
    auto&& processor_builder = m_unary_operator_builder_list.find(name);
    if (processor_builder != m_unary_operator_builder_list.end()) {
      return processor_builder->second.valueType();
    }
    return {};
  }

  static void create();

  PUGS_INLINE
  static OperatorRepository&
  instance()
  {
    Assert(m_instance != nullptr);
    return *m_instance;
  }

  static void destroy();

 private:
  static OperatorRepository* m_instance;

  OperatorRepository() = default;

  ~OperatorRepository() = default;
};

#endif   // OPERATOR_REPOSITORY_HPP
