#include <language/utils/UnaryOperatorRegisterForN.hpp>

#include <language/utils/OperatorRepository.hpp>
#include <language/utils/UnaryOperatorProcessorBuilder.hpp>

void
UnaryOperatorRegisterForN::_register_unary_minus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addUnaryOperator<language::unary_minus>(
    std::make_shared<UnaryOperatorProcessorBuilder<language::unary_minus, int64_t, uint64_t>>());
}

UnaryOperatorRegisterForN::UnaryOperatorRegisterForN()
{
  this->_register_unary_minus();
}
