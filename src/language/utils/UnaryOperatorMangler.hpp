#ifndef UNARY_OPERATOR_MANGLER_HPP
#define UNARY_OPERATOR_MANGLER_HPP

#include <language/utils/ASTNodeDataType.hpp>
#include <utils/Exceptions.hpp>

#include <string>

namespace language
{
struct unary_minus;
struct unary_not;
}   // namespace language

template <typename UnaryOperatorT>
std::string
unaryOperatorMangler(const ASTNodeDataType& operand)
{
  const std::string operator_name = [] {
    if constexpr (std::is_same_v<language::unary_minus, UnaryOperatorT>) {
      return "-";
    } else if constexpr (std::is_same_v<language::unary_not, UnaryOperatorT>) {
      return "not";
    } else {
      static_assert(std::is_same_v<language::unary_minus, UnaryOperatorT>, "undefined unary operator");
    }
  }();

  return operator_name + " " + dataTypeName(operand);
}

#endif   // UNARY_OPERATOR_MANGLER_HPP
