#ifndef UNARY_OPERATOR_PROCESSOR_BUILDER_HPP
#define UNARY_OPERATOR_PROCESSOR_BUILDER_HPP

#include <algebra/TinyVector.hpp>
#include <language/PEGGrammar.hpp>
#include <language/node_processor/UnaryExpressionProcessor.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/IUnaryOperatorProcessorBuilder.hpp>

#include <type_traits>

template <typename OperatorT, typename ValueT, typename DataT>
class UnaryOperatorProcessorBuilder final : public IUnaryOperatorProcessorBuilder
{
 public:
  UnaryOperatorProcessorBuilder() = default;

  ASTNodeDataType
  getOperandDataType() const
  {
    return ast_node_data_type_from<DataT>;
  }

  ASTNodeDataType
  getReturnValueType() const
  {
    return ast_node_data_type_from<ValueT>;
  }

  std::unique_ptr<INodeProcessor>
  getNodeProcessor(ASTNode& node) const
  {
    return std::make_unique<UnaryExpressionProcessor<OperatorT, ValueT, DataT>>(node);
  }
};

#endif   // UNARY_OPERATOR_PROCESSOR_BUILDER_HPP
