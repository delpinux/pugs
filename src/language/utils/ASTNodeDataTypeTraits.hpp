#ifndef AST_NODE_DATA_TYPE_TRAITS_HPP
#define AST_NODE_DATA_TYPE_TRAITS_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <language/utils/ASTNodeDataType.hpp>
#include <language/utils/FunctionSymbolId.hpp>

#include <vector>

template <typename T>
inline ASTNodeDataType
_ast_node_data_type_undefined()
{
  constexpr bool type_is_undefined = not std::is_same_v<T, T>;
  static_assert(type_is_undefined, "Module header defining this data type must be included");
  return {};
}
template <typename T>
inline ASTNodeDataType ast_node_data_type_from = _ast_node_data_type_undefined<T>();

template <>
inline ASTNodeDataType ast_node_data_type_from<void> = ASTNodeDataType::build<ASTNodeDataType::void_t>();
template <>
inline ASTNodeDataType ast_node_data_type_from<bool> = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
template <>
inline ASTNodeDataType ast_node_data_type_from<int64_t> = ASTNodeDataType::build<ASTNodeDataType::int_t>();
template <>
inline ASTNodeDataType ast_node_data_type_from<uint64_t> = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
template <>
inline ASTNodeDataType ast_node_data_type_from<double> = ASTNodeDataType::build<ASTNodeDataType::double_t>();
template <>
inline ASTNodeDataType ast_node_data_type_from<std::string> = ASTNodeDataType::build<ASTNodeDataType::string_t>();
template <>
inline ASTNodeDataType ast_node_data_type_from<FunctionSymbolId> =
  ASTNodeDataType::build<ASTNodeDataType::function_t>();
template <size_t N>
inline ASTNodeDataType ast_node_data_type_from<TinyVector<N>> = ASTNodeDataType::build<ASTNodeDataType::vector_t>(N);
template <size_t N>
inline ASTNodeDataType ast_node_data_type_from<TinyMatrix<N>> = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(N, N);

template <typename T>
inline ASTNodeDataType ast_node_data_type_from<std::vector<T>> =
  ASTNodeDataType::build<ASTNodeDataType::tuple_t>(ast_node_data_type_from<T>);

#endif   // AST_NODE_DATA_TYPE_TRAITS_HPP
