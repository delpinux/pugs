#ifndef INC_DEC_OPERATOR_MANGLER_HPP
#define INC_DEC_OPERATOR_MANGLER_HPP

#include <language/utils/ASTNodeDataType.hpp>
#include <utils/Exceptions.hpp>

#include <string>

namespace language
{
struct unary_minusminus;
struct unary_plusplus;
struct post_minusminus;
struct post_plusplus;
}   // namespace language

template <typename IncDecOperatorT>
std::string
incDecOperatorMangler(const ASTNodeDataType& operand)
{
  if constexpr (std::is_same_v<language::unary_minusminus, IncDecOperatorT>) {
    return std::string{"-- "} + dataTypeName(operand);
  } else if constexpr (std::is_same_v<language::post_minusminus, IncDecOperatorT>) {
    return dataTypeName(operand) + " --";
  } else if constexpr (std::is_same_v<language::unary_plusplus, IncDecOperatorT>) {
    return std::string{"++ "} + dataTypeName(operand);
  } else if constexpr (std::is_same_v<language::post_plusplus, IncDecOperatorT>) {
    return dataTypeName(operand) + " ++";
  } else {
    static_assert(std::is_same_v<language::unary_minusminus, IncDecOperatorT>, "undefined inc/dec operator");
  }
}

#endif   // INC_DEC_OPERATOR_MANGLER_HPP
