#ifndef INC_DEC_OPERATOR_REGISTER_FOR_N_HPP
#define INC_DEC_OPERATOR_REGISTER_FOR_N_HPP

class IncDecOperatorRegisterForN
{
 private:
  void _register_unary_minusminus();
  void _register_unary_plusplus();
  void _register_post_minusminus();
  void _register_post_plusplus();

 public:
  IncDecOperatorRegisterForN();
};

#endif   // INC_DEC_OPERATOR_REGISTER_FOR_N_HPP
