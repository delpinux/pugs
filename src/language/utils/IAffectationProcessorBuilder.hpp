#ifndef I_AFFECTATION_PROCESSOR_BUILDER_HPP
#define I_AFFECTATION_PROCESSOR_BUILDER_HPP

class ASTNode;
class INodeProcessor;

#include <memory>

class IAffectationProcessorBuilder
{
 public:
  virtual std::unique_ptr<INodeProcessor> getNodeProcessor(ASTNode& lhs_node, ASTNode& rhs_node) const = 0;

  virtual ~IAffectationProcessorBuilder() = default;
};

#endif   // I_AFFECTATION_PROCESSOR_BUILDER_HPP
