#include <language/utils/UnaryOperatorRegisterForB.hpp>

#include <language/utils/OperatorRepository.hpp>
#include <language/utils/UnaryOperatorProcessorBuilder.hpp>

void
UnaryOperatorRegisterForB::_register_unary_minus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addUnaryOperator<language::unary_minus>(
    std::make_shared<UnaryOperatorProcessorBuilder<language::unary_minus, int64_t, bool>>());
}

void
UnaryOperatorRegisterForB::_register_unary_not()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addUnaryOperator<language::unary_not>(
    std::make_shared<UnaryOperatorProcessorBuilder<language::unary_not, bool, bool>>());
}

UnaryOperatorRegisterForB::UnaryOperatorRegisterForB()
{
  this->_register_unary_minus();
  this->_register_unary_not();
}
