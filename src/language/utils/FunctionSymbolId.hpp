#ifndef FUNCTION_SYMBOL_ID_HPP
#define FUNCTION_SYMBOL_ID_HPP

#include <utils/PugsAssert.hpp>
#include <utils/PugsMacros.hpp>

#include <cstddef>
#include <iostream>
#include <memory>

class FunctionDescriptor;
class SymbolTable;

class FunctionSymbolId
{
 private:
  uint64_t m_function_id;
  std::shared_ptr<SymbolTable> m_symbol_table = nullptr;

 public:
  [[nodiscard]] PUGS_INLINE uint64_t
  id() const noexcept
  {
    return m_function_id;
  }

  [[nodiscard]] PUGS_INLINE const SymbolTable&
  symbolTable() const
  {
    Assert(m_symbol_table, "FunctionSymbolId is not initialized properly");
    return *m_symbol_table;
  }

  [[nodiscard]] const FunctionDescriptor& descriptor() const;

  friend std::ostream&
  operator<<(std::ostream& os, const FunctionSymbolId& function_symbol_id)
  {
    os << function_symbol_id.m_function_id;
    return os;
  }

  FunctionSymbolId& operator=(const FunctionSymbolId&) = default;
  FunctionSymbolId& operator=(FunctionSymbolId&&) = default;

  FunctionSymbolId() = default;

  FunctionSymbolId(const FunctionSymbolId&) = default;
  FunctionSymbolId(FunctionSymbolId&&)      = default;

  FunctionSymbolId(uint64_t function_id, const std::shared_ptr<SymbolTable>& symbol_table)
    : m_function_id(function_id), m_symbol_table(symbol_table)
  {}

  ~FunctionSymbolId() = default;
};

#endif   // FUNCTION_SYMBOL_ID_HPP
