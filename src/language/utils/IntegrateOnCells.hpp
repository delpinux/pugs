#ifndef INTEGRATE_ON_CELLS_HPP
#define INTEGRATE_ON_CELLS_HPP

#include <analysis/IQuadratureDescriptor.hpp>
#include <analysis/QuadratureManager.hpp>
#include <geometry/CubeTransformation.hpp>
#include <geometry/LineTransformation.hpp>
#include <geometry/PrismTransformation.hpp>
#include <geometry/PyramidTransformation.hpp>
#include <geometry/SquareTransformation.hpp>
#include <geometry/TetrahedronTransformation.hpp>
#include <geometry/TriangleTransformation.hpp>
#include <language/utils/PugsFunctionAdapter.hpp>
#include <mesh/CellType.hpp>
#include <mesh/Connectivity.hpp>
#include <mesh/ItemId.hpp>
#include <mesh/MeshTraits.hpp>
#include <utils/Array.hpp>

class FunctionSymbolId;

template <typename T>
class IntegrateOnCells;
template <typename OutputType, typename InputType>
class IntegrateOnCells<OutputType(InputType)> : public PugsFunctionAdapter<OutputType(InputType)>
{
 private:
  using Adapter = PugsFunctionAdapter<OutputType(InputType)>;

  template <size_t Dimension>
  class AllCells
  {
   private:
    const Connectivity<Dimension>& m_connectivity;

   public:
    using index_type = CellId;

    PUGS_INLINE
    CellId
    cellId(const CellId cell_id) const
    {
      return cell_id;
    }

    PUGS_INLINE
    size_t
    size() const
    {
      return m_connectivity.numberOfCells();
    }

    PUGS_INLINE
    AllCells(const Connectivity<Dimension>& connectivity) : m_connectivity{connectivity} {}
  };

  class CellList
  {
   private:
    const Array<const CellId>& m_cell_list;

   public:
    using index_type = Array<const CellId>::index_type;

    PUGS_INLINE
    CellId
    cellId(const index_type index) const
    {
      return m_cell_list[index];
    }

    PUGS_INLINE
    size_t
    size() const
    {
      return m_cell_list.size();
    }

    PUGS_INLINE
    CellList(const Array<const CellId>& cell_list) : m_cell_list{cell_list} {}
  };

  template <MeshConcept MeshType, typename OutputArrayT, typename ListTypeT>
  static PUGS_INLINE void
  _tensorialIntegrateTo(const FunctionSymbolId& function_symbol_id,
                        const IQuadratureDescriptor& quadrature_descriptor,
                        const MeshType& mesh,
                        const ListTypeT& cell_list,
                        OutputArrayT& value)
  {
    constexpr size_t Dimension = MeshType::Dimension;

    static_assert(std::is_same_v<TinyVector<Dimension>, InputType>, "invalid input data type");
    static_assert(std::is_same_v<std::remove_const_t<typename OutputArrayT::data_type>, OutputType>,
                  "invalid output data type");

    auto& expression    = Adapter::getFunctionExpression(function_symbol_id);
    auto convert_result = Adapter::getResultConverter(expression.m_data_type);

    auto context_list = Adapter::getContextList(expression);

    using execution_space = typename Kokkos::DefaultExecutionSpace::execution_space;
    Kokkos::Experimental::UniqueToken<execution_space, Kokkos::Experimental::UniqueTokenScope::Global> tokens;

    if constexpr (std::is_arithmetic_v<OutputType>) {
      value.fill(0);
    } else if constexpr (is_tiny_vector_v<OutputType> or is_tiny_matrix_v<OutputType>) {
      value.fill(zero);
    } else {
      static_assert(std::is_same_v<OutputType, double>, "unexpected output type");
    }

    const auto& connectivity = mesh.connectivity();

    const auto cell_to_node_matrix = connectivity.cellToNodeMatrix();
    const auto cell_type           = connectivity.cellType();
    const auto xr                  = mesh.xr();

    auto invalid_cell = std::make_pair(false, CellId{});

    const auto qf = [&] {
      if constexpr (Dimension == 1) {
        return QuadratureManager::instance().getLineFormula(quadrature_descriptor);
      } else if constexpr (Dimension == 2) {
        return QuadratureManager::instance().getSquareFormula(quadrature_descriptor);
      } else {
        static_assert(Dimension == 3);
        return QuadratureManager::instance().getCubeFormula(quadrature_descriptor);
      }
    }();

    parallel_for(cell_list.size(), [=, &expression, &tokens, &invalid_cell, &qf,
                                    &cell_list](typename ListTypeT::index_type index) {
      const int32_t t        = tokens.acquire();
      auto& execution_policy = context_list[t];

      const CellId cell_id = cell_list.cellId(index);

      const auto cell_to_node = cell_to_node_matrix[cell_id];

      if constexpr (Dimension == 1) {
        switch (cell_type[cell_id]) {
        case CellType::Line: {
          const LineTransformation<1> T(xr[cell_to_node[0]], xr[cell_to_node[1]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant() * convert_result(std::move(result));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      } else if constexpr (Dimension == 2) {
        switch (cell_type[cell_id]) {
        case CellType::Triangle: {
          const SquareTransformation<2> T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],
                                          xr[cell_to_node[2]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * convert_result(std::move(result));
          }
          break;
        }
        case CellType::Quadrangle: {
          const SquareTransformation<2> T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],
                                          xr[cell_to_node[3]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * convert_result(std::move(result));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      } else {
        static_assert(Dimension == 3);

        switch (cell_type[cell_id]) {
        case CellType::Tetrahedron: {
          const CubeTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],
                                     xr[cell_to_node[2]],   //
                                     xr[cell_to_node[3]], xr[cell_to_node[3]], xr[cell_to_node[3]],
                                     xr[cell_to_node[3]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * convert_result(std::move(result));
          }
          break;
        }
        case CellType::Pyramid: {
          if (cell_to_node.size() == 5) {
            const CubeTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],   //
                                       xr[cell_to_node[3]],                                             //
                                       xr[cell_to_node[4]], xr[cell_to_node[4]], xr[cell_to_node[4]],
                                       xr[cell_to_node[4]]);

            for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
              const auto xi = qf.point(i_point);
              Adapter::convertArgs(execution_policy.currentContext(), T(xi));
              auto result = expression.execute(execution_policy);
              value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * convert_result(std::move(result));
            }
          } else {
            // LCOV_EXCL_START
            throw NotImplementedError("integration on pyramid with non-quadrangular base (number of nodes " +
                                      stringify(cell_to_node.size()) + ")");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case CellType::Diamond: {
          if (cell_to_node.size() == 5) {
            {   // top tetrahedron
              const CubeTransformation T0(xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],
                                          xr[cell_to_node[3]],   //
                                          xr[cell_to_node[4]], xr[cell_to_node[4]], xr[cell_to_node[4]],
                                          xr[cell_to_node[4]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                Adapter::convertArgs(execution_policy.currentContext(), T0(xi));
                auto result = expression.execute(execution_policy);
                value[index] += qf.weight(i_point) * T0.jacobianDeterminant(xi) * convert_result(std::move(result));
              }
            }
            {   // bottom tetrahedron
              const CubeTransformation T1(xr[cell_to_node[3]], xr[cell_to_node[3]], xr[cell_to_node[2]],
                                          xr[cell_to_node[1]],   //
                                          xr[cell_to_node[0]], xr[cell_to_node[0]], xr[cell_to_node[0]],
                                          xr[cell_to_node[0]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                Adapter::convertArgs(execution_policy.currentContext(), T1(xi));
                auto result = expression.execute(execution_policy);
                value[index] += qf.weight(i_point) * T1.jacobianDeterminant(xi) * convert_result(std::move(result));
              }
            }
          } else if (cell_to_node.size() == 6) {
            {   // top pyramid
              const CubeTransformation T0(xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],
                                          xr[cell_to_node[4]],   //
                                          xr[cell_to_node[5]], xr[cell_to_node[5]], xr[cell_to_node[5]],
                                          xr[cell_to_node[5]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                Adapter::convertArgs(execution_policy.currentContext(), T0(xi));
                auto result = expression.execute(execution_policy);
                value[index] += qf.weight(i_point) * T0.jacobianDeterminant(xi) * convert_result(std::move(result));
              }
            }
            {   // bottom pyramid
              const CubeTransformation T1(xr[cell_to_node[4]], xr[cell_to_node[3]], xr[cell_to_node[2]],
                                          xr[cell_to_node[1]],   //
                                          xr[cell_to_node[0]], xr[cell_to_node[0]], xr[cell_to_node[0]],
                                          xr[cell_to_node[0]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                Adapter::convertArgs(execution_policy.currentContext(), T1(xi));
                auto result = expression.execute(execution_policy);
                value[index] += qf.weight(i_point) * T1.jacobianDeterminant(xi) * convert_result(std::move(result));
              }
            }
          } else {
            // LCOV_EXCL_START
            throw NotImplementedError("integration on diamond with non-quadrangular base (" +
                                      stringify(cell_to_node.size()) + ")");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case CellType::Prism: {
          const CubeTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]],   //
                                     xr[cell_to_node[2]], xr[cell_to_node[2]],   //
                                     xr[cell_to_node[3]], xr[cell_to_node[4]],   //
                                     xr[cell_to_node[5]], xr[cell_to_node[5]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * convert_result(std::move(result));
          }
          break;
        }
        case CellType::Hexahedron: {
          const CubeTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],
                                     xr[cell_to_node[4]], xr[cell_to_node[5]], xr[cell_to_node[6]],
                                     xr[cell_to_node[7]]);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * convert_result(std::move(result));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      }

      tokens.release(t);
    });

    // LCOV_EXCL_START
    if (invalid_cell.first) {
      std::ostringstream os;
      os << "invalid cell type for integration: " << name(cell_type[invalid_cell.second]);
      throw UnexpectedError(os.str());
    }
    // LCOV_EXCL_STOP
  }

  template <MeshConcept MeshType, typename OutputArrayT, typename ListTypeT>
  static PUGS_INLINE void
  _directIntegrateTo(const FunctionSymbolId& function_symbol_id,
                     const IQuadratureDescriptor& quadrature_descriptor,
                     const MeshType& mesh,
                     const ListTypeT& cell_list,
                     OutputArrayT& value)
  {
    Assert(not quadrature_descriptor.isTensorial());

    constexpr size_t Dimension = MeshType::Dimension;

    static_assert(std::is_same_v<TinyVector<Dimension>, InputType>, "invalid input data type");
    static_assert(std::is_same_v<std::remove_const_t<typename OutputArrayT::data_type>, OutputType>,
                  "invalid output data type");

    auto& expression    = Adapter::getFunctionExpression(function_symbol_id);
    auto convert_result = Adapter::getResultConverter(expression.m_data_type);

    auto context_list = Adapter::getContextList(expression);

    using execution_space = typename Kokkos::DefaultExecutionSpace::execution_space;
    Kokkos::Experimental::UniqueToken<execution_space, Kokkos::Experimental::UniqueTokenScope::Global> tokens;

    if constexpr (std::is_arithmetic_v<OutputType>) {
      value.fill(0);
    } else if constexpr (is_tiny_vector_v<OutputType> or is_tiny_matrix_v<OutputType>) {
      value.fill(zero);
    } else {
      static_assert(std::is_same_v<OutputType, double>, "unexpected output type");
    }

    const auto& connectivity = mesh.connectivity();

    const auto cell_to_node_matrix = connectivity.cellToNodeMatrix();
    const auto cell_type           = connectivity.cellType();
    const auto xr                  = mesh.xr();

    auto invalid_cell = std::make_pair(false, CellId{});

    parallel_for(cell_list.size(), [=, &expression, &tokens, &invalid_cell, &quadrature_descriptor,
                                    &cell_list](const typename ListTypeT::index_type& index) {
      const int32_t t        = tokens.acquire();
      auto& execution_policy = context_list[t];

      const CellId cell_id = cell_list.cellId(index);

      const auto cell_to_node = cell_to_node_matrix[cell_id];

      if constexpr (Dimension == 1) {
        switch (cell_type[cell_id]) {
        case CellType::Line: {
          const LineTransformation<1> T(xr[cell_to_node[0]], xr[cell_to_node[1]]);
          const auto qf = QuadratureManager::instance().getLineFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant() * convert_result(std::move(result));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      } else if constexpr (Dimension == 2) {
        switch (cell_type[cell_id]) {
        case CellType::Triangle: {
          const TriangleTransformation<2> T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]]);
          const auto qf = QuadratureManager::instance().getTriangleFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant() * convert_result(std::move(result));
          }
          break;
        }
        case CellType::Quadrangle: {
          const SquareTransformation<2> T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],
                                          xr[cell_to_node[3]]);
          const auto qf = QuadratureManager::instance().getSquareFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * convert_result(std::move(result));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      } else {
        static_assert(Dimension == 3);

        switch (cell_type[cell_id]) {
        case CellType::Tetrahedron: {
          const TetrahedronTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]],   //
                                            xr[cell_to_node[2]], xr[cell_to_node[3]]);
          const auto qf = QuadratureManager::instance().getTetrahedronFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant() * convert_result(std::move(result));
          }
          break;
        }
        case CellType::Pyramid: {
          if (cell_to_node.size() == 5) {
            const PyramidTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],   //
                                          xr[cell_to_node[3]], xr[cell_to_node[4]]);
            const auto qf = QuadratureManager::instance().getPyramidFormula(quadrature_descriptor);

            for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
              const auto xi = qf.point(i_point);
              Adapter::convertArgs(execution_policy.currentContext(), T(xi));
              auto result = expression.execute(execution_policy);
              value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * convert_result(std::move(result));
            }
          } else {
            // LCOV_EXCL_START
            throw NotImplementedError("integration on pyramid with non-quadrangular base");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case CellType::Diamond: {
          if (cell_to_node.size() == 5) {
            const auto qf = QuadratureManager::instance().getTetrahedronFormula(quadrature_descriptor);
            {   // top tetrahedron
              const TetrahedronTransformation T0(xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],   //
                                                 xr[cell_to_node[4]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                Adapter::convertArgs(execution_policy.currentContext(), T0(xi));
                auto result = expression.execute(execution_policy);
                value[index] += qf.weight(i_point) * T0.jacobianDeterminant() * convert_result(std::move(result));
              }
            }
            {   // bottom tetrahedron
              const TetrahedronTransformation T1(xr[cell_to_node[3]], xr[cell_to_node[2]], xr[cell_to_node[1]],   //
                                                 xr[cell_to_node[0]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                Adapter::convertArgs(execution_policy.currentContext(), T1(xi));
                auto result = expression.execute(execution_policy);
                value[index] += qf.weight(i_point) * T1.jacobianDeterminant() * convert_result(std::move(result));
              }
            }
          } else if (cell_to_node.size() == 6) {
            const auto qf = QuadratureManager::instance().getPyramidFormula(quadrature_descriptor);
            {   // top pyramid
              const PyramidTransformation T0(xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],
                                             xr[cell_to_node[4]], xr[cell_to_node[5]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                Adapter::convertArgs(execution_policy.currentContext(), T0(xi));
                auto result = expression.execute(execution_policy);
                value[index] += qf.weight(i_point) * T0.jacobianDeterminant(xi) * convert_result(std::move(result));
              }
            }
            {   // bottom pyramid
              const PyramidTransformation T1(xr[cell_to_node[4]], xr[cell_to_node[3]], xr[cell_to_node[2]],
                                             xr[cell_to_node[1]], xr[cell_to_node[0]]);

              for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
                const auto xi = qf.point(i_point);
                Adapter::convertArgs(execution_policy.currentContext(), T1(xi));
                auto result = expression.execute(execution_policy);
                value[index] += qf.weight(i_point) * T1.jacobianDeterminant(xi) * convert_result(std::move(result));
              }
            }
          } else {
            // LCOV_EXCL_START
            throw NotImplementedError("integration on diamond with non-quadrangular base (" +
                                      stringify(cell_to_node.size()) + ")");
            // LCOV_EXCL_STOP
          }
          break;
        }
        case CellType::Prism: {
          const PrismTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]],
                                      xr[cell_to_node[3]], xr[cell_to_node[4]], xr[cell_to_node[5]]);
          const auto qf = QuadratureManager::instance().getPrismFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * convert_result(std::move(result));
          }
          break;
        }
        case CellType::Hexahedron: {
          const CubeTransformation T(xr[cell_to_node[0]], xr[cell_to_node[1]], xr[cell_to_node[2]], xr[cell_to_node[3]],
                                     xr[cell_to_node[4]], xr[cell_to_node[5]], xr[cell_to_node[6]],
                                     xr[cell_to_node[7]]);
          const auto qf = QuadratureManager::instance().getCubeFormula(quadrature_descriptor);

          for (size_t i_point = 0; i_point < qf.numberOfPoints(); ++i_point) {
            const auto xi = qf.point(i_point);
            Adapter::convertArgs(execution_policy.currentContext(), T(xi));
            auto result = expression.execute(execution_policy);
            value[index] += qf.weight(i_point) * T.jacobianDeterminant(xi) * convert_result(std::move(result));
          }
          break;
        }
          // LCOV_EXCL_START
        default: {
          invalid_cell = std::make_pair(true, cell_id);
          break;
        }
          // LCOV_EXCL_STOP
        }
      }

      tokens.release(t);
    });

    // LCOV_EXCL_START
    if (invalid_cell.first) {
      std::ostringstream os;
      os << "invalid cell type for integration: " << name(cell_type[invalid_cell.second]);
      throw UnexpectedError(os.str());
    }
    // LCOV_EXCL_STOP
  }

 public:
  template <MeshConcept MeshType, typename ArrayT>
  static PUGS_INLINE void
  integrateTo(const FunctionSymbolId& function_symbol_id,
              const IQuadratureDescriptor& quadrature_descriptor,
              const MeshType& mesh,
              ArrayT& value)
  {
    constexpr size_t Dimension = MeshType::Dimension;

    if (quadrature_descriptor.isTensorial()) {
      _tensorialIntegrateTo<MeshType, ArrayT>(function_symbol_id, quadrature_descriptor, mesh,
                                              AllCells<Dimension>{mesh.connectivity()}, value);
    } else {
      _directIntegrateTo<MeshType, ArrayT>(function_symbol_id, quadrature_descriptor, mesh,
                                           AllCells<Dimension>{mesh.connectivity()}, value);
    }
  }

  template <MeshConcept MeshType, template <typename DataType> typename ArrayT>
  static PUGS_INLINE ArrayT<OutputType>
  integrate(const FunctionSymbolId& function_symbol_id,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh,
            const ArrayT<const CellId>& cell_list)
  {
    ArrayT<OutputType> value(size(cell_list));
    if (quadrature_descriptor.isTensorial()) {
      _tensorialIntegrateTo<MeshType, ArrayT<OutputType>>(function_symbol_id, quadrature_descriptor, mesh,
                                                          CellList{cell_list}, value);
    } else {
      _directIntegrateTo<MeshType, ArrayT<OutputType>>(function_symbol_id, quadrature_descriptor, mesh,
                                                       CellList{cell_list}, value);
    }

    return value;
  }

  template <MeshConcept MeshType, template <typename DataType> typename ArrayT>
  static PUGS_INLINE ArrayT<OutputType>
  integrate(const FunctionSymbolId& function_symbol_id,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh,
            const ArrayT<CellId>& cell_list)
  {
    return integrate(function_symbol_id, quadrature_descriptor, mesh, ArrayT<const CellId>{cell_list});
  }
};

#endif   // INTEGRATE_ON_CELLS_HPP
