#ifndef AFFECTATION_REGISTER_FOR_RN_HPP
#define AFFECTATION_REGISTER_FOR_RN_HPP

#include <cstdlib>

template <size_t Dimension>
class AffectationRegisterForRn
{
 private:
  void _register_eq_op();
  void _register_pluseq_op();
  void _register_minuseq_op();
  void _register_multiplyeq_op();

 public:
  AffectationRegisterForRn();
};

#endif   // AFFECTATION_REGISTER_FOR_RN_HPP
