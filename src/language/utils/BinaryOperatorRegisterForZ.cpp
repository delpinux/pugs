#include <language/utils/BinaryOperatorRegisterForZ.hpp>

#include <language/utils/BasicBinaryOperatorRegisterComparisonOf.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/OStream.hpp>

void
BinaryOperatorRegisterForZ::_register_ostream()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, int64_t>>());

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, std::vector<int64_t>>>());
}

void
BinaryOperatorRegisterForZ::_register_comparisons()
{
  BasicBinaryOperatorRegisterComparisonOf<bool, int64_t>{};
  BasicBinaryOperatorRegisterComparisonOf<int64_t, bool>{};

  BasicBinaryOperatorRegisterComparisonOf<uint64_t, int64_t>{};
  BasicBinaryOperatorRegisterComparisonOf<int64_t, uint64_t>{};

  BasicBinaryOperatorRegisterComparisonOf<int64_t, int64_t>{};
}

template <typename OperatorT>
void
BinaryOperatorRegisterForZ::_register_arithmetic()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, int64_t, int64_t, bool>>());
  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, int64_t, bool, int64_t>>());

  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, int64_t, int64_t, uint64_t>>());
  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, int64_t, uint64_t, int64_t>>());

  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, int64_t, int64_t, int64_t>>());
}

void
BinaryOperatorRegisterForZ::_register_divide()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, int64_t, bool, int64_t>>());

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, int64_t, int64_t, uint64_t>>());
  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, int64_t, uint64_t, int64_t>>());

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, int64_t, int64_t, int64_t>>());
}

BinaryOperatorRegisterForZ::BinaryOperatorRegisterForZ()
{
  this->_register_ostream();
  this->_register_comparisons();
  this->_register_arithmetic<language::plus_op>();
  this->_register_arithmetic<language::minus_op>();
  this->_register_arithmetic<language::multiply_op>();
  this->_register_divide();
}
