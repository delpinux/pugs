#ifndef BASIC_BINARY_OPERATOR_REGISTER_COMPARISON_OF_HPP
#define BASIC_BINARY_OPERATOR_REGISTER_COMPARISON_OF_HPP

#include <language/utils/BinaryOperatorProcessorBuilder.hpp>
#include <language/utils/OperatorRepository.hpp>

template <typename A_DataT, typename B_DataT>
struct BasicBinaryOperatorRegisterComparisonOf
{
  BasicBinaryOperatorRegisterComparisonOf()
  {
    OperatorRepository& repository = OperatorRepository::instance();

    repository.addBinaryOperator<language::eqeq_op>(
      std::make_shared<BinaryOperatorProcessorBuilder<language::eqeq_op, bool, A_DataT, B_DataT>>());

    repository.addBinaryOperator<language::not_eq_op>(
      std::make_shared<BinaryOperatorProcessorBuilder<language::not_eq_op, bool, A_DataT, B_DataT>>());

    repository.addBinaryOperator<language::greater_op>(
      std::make_shared<BinaryOperatorProcessorBuilder<language::greater_op, bool, A_DataT, B_DataT>>());

    repository.addBinaryOperator<language::greater_or_eq_op>(
      std::make_shared<BinaryOperatorProcessorBuilder<language::greater_or_eq_op, bool, A_DataT, B_DataT>>());

    repository.addBinaryOperator<language::lesser_op>(
      std::make_shared<BinaryOperatorProcessorBuilder<language::lesser_op, bool, A_DataT, B_DataT>>());

    repository.addBinaryOperator<language::lesser_or_eq_op>(
      std::make_shared<BinaryOperatorProcessorBuilder<language::lesser_or_eq_op, bool, A_DataT, B_DataT>>());
  }
};

#endif   // BASIC_BINARY_OPERATOR_REGISTER_COMPARISON_OF_HPP
