#ifndef INTEGRATE_CELL_ARRAY_HPP
#define INTEGRATE_CELL_ARRAY_HPP

#include <language/utils/IntegrateCellValue.hpp>
#include <mesh/CellType.hpp>
#include <mesh/ItemArray.hpp>
#include <mesh/MeshTraits.hpp>

template <typename T>
class IntegrateCellArray;
template <typename OutputType, typename InputType>
class IntegrateCellArray<OutputType(InputType)>
{
  static constexpr size_t Dimension = OutputType::Dimension;

 public:
  template <MeshConcept MeshType>
  PUGS_INLINE static CellArray<OutputType>
  integrate(const std::vector<FunctionSymbolId>& function_symbol_id_list,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh)
  {
    CellArray<OutputType> cell_array{mesh.connectivity(), function_symbol_id_list.size()};

    for (size_t i_function_symbol = 0; i_function_symbol < function_symbol_id_list.size(); ++i_function_symbol) {
      const FunctionSymbolId& function_symbol_id = function_symbol_id_list[i_function_symbol];
      CellValue<OutputType> cell_value =
        IntegrateCellValue<OutputType(InputType)>::integrate(function_symbol_id, quadrature_descriptor, mesh);
      parallel_for(
        cell_value.numberOfItems(),
        PUGS_LAMBDA(CellId cell_id) { cell_array[cell_id][i_function_symbol] = cell_value[cell_id]; });
    }

    return cell_array;
  }

  template <MeshConcept MeshType>
  PUGS_INLINE static Table<OutputType>
  integrate(const std::vector<FunctionSymbolId>& function_symbol_id_list,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh,
            const Array<const CellId>& list_of_cells)
  {
    Table<OutputType> table{list_of_cells.size(), function_symbol_id_list.size()};

    for (size_t i_function_symbol = 0; i_function_symbol < function_symbol_id_list.size(); ++i_function_symbol) {
      const FunctionSymbolId& function_symbol_id = function_symbol_id_list[i_function_symbol];
      Array<OutputType> array =
        IntegrateCellValue<OutputType(InputType)>::integrate(function_symbol_id, quadrature_descriptor, mesh,
                                                             list_of_cells);

      parallel_for(
        array.size(), PUGS_LAMBDA(size_t i) { table[i][i_function_symbol] = array[i]; });
    }

    return table;
  }

  template <MeshConcept MeshType>
  PUGS_INLINE static Table<OutputType>
  integrate(const std::vector<FunctionSymbolId>& function_symbol_id_list,
            const IQuadratureDescriptor& quadrature_descriptor,
            const MeshType& mesh,
            const Array<CellId>& list_of_cells)
  {
    return integrate(function_symbol_id_list, quadrature_descriptor, mesh, Array<const CellId>{list_of_cells});
  }
};

#endif   // INTEGRATE_CELL_ARRAY_HPP
