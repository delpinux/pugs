#ifndef PARSE_ERROR_HPP
#define PARSE_ERROR_HPP

#include <utils/Exceptions.hpp>

#include <pegtl/position.hpp>

#include <vector>

class ParseError : public IExitError
{
 private:
  std::string m_error_msg;
  std::vector<TAO_PEGTL_NAMESPACE::position> m_positions;

 public:
  const auto&
  positions() const
  {
    return m_positions;
  }

  ParseError(const std::string_view msg, std::vector<TAO_PEGTL_NAMESPACE::position>&& positions)
    : IExitError(msg), m_positions{std::move(positions)}
  {}

  ParseError(const std::string_view msg, const std::vector<TAO_PEGTL_NAMESPACE::position>& positions)
    : IExitError(msg), m_positions{positions}
  {}

  ParseError(const std::string_view msg, const TAO_PEGTL_NAMESPACE::position& positions)
    : IExitError(msg), m_positions{positions}
  {}

  ~ParseError() = default;
};

#endif   // PARSE_ERROR_HPP
