#include <language/utils/AffectationRegisterForRnxn.hpp>

#include <language/utils/AffectationProcessorBuilder.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/OperatorRepository.hpp>

template <size_t Dimension>
void
AffectationRegisterForRnxn<Dimension>::_register_eq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  auto Rnxn = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(Dimension, Dimension);

  repository.addAffectation<
    language::eq_op>(Rnxn, Z,
                     std::make_shared<AffectationFromZeroProcessorBuilder<language::eq_op, TinyMatrix<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Rnxn), Z,
                                     std::make_shared<AffectationToTupleProcessorBuilder<TinyMatrix<Dimension>>>());
}

template <>
void
AffectationRegisterForRnxn<1>::_register_eq_op()
{
  constexpr size_t Dimension = 1;

  OperatorRepository& repository = OperatorRepository::instance();

  auto B = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();
  auto R = ASTNodeDataType::build<ASTNodeDataType::double_t>();

  auto R1x1 = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(Dimension, Dimension);

  repository.addAffectation<
    language::eq_op>(R1x1, B,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, TinyMatrix<Dimension>, bool>>());

  repository.addAffectation<
    language::eq_op>(R1x1, N,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, TinyMatrix<Dimension>, uint64_t>>());

  repository.addAffectation<
    language::eq_op>(R1x1, Z,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, TinyMatrix<Dimension>, int64_t>>());

  repository.addAffectation<
    language::eq_op>(R1x1, R,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, TinyMatrix<Dimension>, double>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1x1), B,
                                     std::make_shared<AffectationToTupleProcessorBuilder<TinyMatrix<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1x1), N,
                                     std::make_shared<AffectationToTupleProcessorBuilder<TinyMatrix<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1x1), Z,
                                     std::make_shared<AffectationToTupleProcessorBuilder<TinyMatrix<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1x1), R,
                                     std::make_shared<AffectationToTupleProcessorBuilder<TinyMatrix<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1x1),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<TinyMatrix<1>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1x1),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<TinyMatrix<1>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1x1),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<TinyMatrix<1>>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1x1),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<TinyMatrix<1>>>());

  repository
    .addAffectation<language::eq_op>(R1x1, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                     std::make_shared<AffectationFromTupleProcessorBuilder<TinyMatrix<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(R1x1, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                     std::make_shared<AffectationFromTupleProcessorBuilder<TinyMatrix<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(R1x1, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                     std::make_shared<AffectationFromTupleProcessorBuilder<TinyMatrix<Dimension>>>());

  repository
    .addAffectation<language::eq_op>(R1x1, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R),
                                     std::make_shared<AffectationFromTupleProcessorBuilder<TinyMatrix<Dimension>>>());
}

template <size_t Dimension>
void
AffectationRegisterForRnxn<Dimension>::_register_pluseq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Rnxn = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(Dimension, Dimension);

  repository
    .addAffectation<language::pluseq_op>(Rnxn, Rnxn,
                                         std::make_shared<AffectationProcessorBuilder<
                                           language::pluseq_op, TinyMatrix<Dimension>, TinyMatrix<Dimension>>>());
}

template <size_t Dimension>
void
AffectationRegisterForRnxn<Dimension>::_register_minuseq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Rnxn = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(Dimension, Dimension);

  repository
    .addAffectation<language::minuseq_op>(Rnxn, Rnxn,
                                          std::make_shared<AffectationProcessorBuilder<
                                            language::minuseq_op, TinyMatrix<Dimension>, TinyMatrix<Dimension>>>());
}

template <size_t Dimension>
void
AffectationRegisterForRnxn<Dimension>::_register_multiplyeq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto B = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();
  auto R = ASTNodeDataType::build<ASTNodeDataType::double_t>();

  auto Rnxn = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(Dimension, Dimension);

  repository.addAffectation<language::multiplyeq_op>(Rnxn, B,
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, TinyMatrix<Dimension>, bool>>());

  repository.addAffectation<language::multiplyeq_op>(Rnxn, N,
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, TinyMatrix<Dimension>, uint64_t>>());

  repository.addAffectation<language::multiplyeq_op>(Rnxn, Z,
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, TinyMatrix<Dimension>, int64_t>>());

  repository.addAffectation<language::multiplyeq_op>(Rnxn, R,
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, TinyMatrix<Dimension>, double>>());
}

template <size_t Dimension>
AffectationRegisterForRnxn<Dimension>::AffectationRegisterForRnxn()
{
  BasicAffectationRegisterFor<TinyMatrix<Dimension>>{};
  this->_register_eq_op();
  this->_register_pluseq_op();
  this->_register_minuseq_op();
  this->_register_multiplyeq_op();
}

template class AffectationRegisterForRnxn<1>;
template class AffectationRegisterForRnxn<2>;
template class AffectationRegisterForRnxn<3>;
