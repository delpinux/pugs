#include <language/utils/AffectationRegisterForZ.hpp>

#include <language/utils/AffectationProcessorBuilder.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/OperatorRepository.hpp>

void
AffectationRegisterForZ::_register_eq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto B = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository
    .addAffectation<language::eq_op>(Z, B,
                                     std::make_shared<AffectationProcessorBuilder<language::eq_op, int64_t, bool>>());

  repository.addAffectation<
    language::eq_op>(Z, N, std::make_shared<AffectationProcessorBuilder<language::eq_op, int64_t, uint64_t>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z), B,
                                             std::make_shared<AffectationToTupleProcessorBuilder<int64_t>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z), N,
                                             std::make_shared<AffectationToTupleProcessorBuilder<int64_t>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                             ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                             std::make_shared<AffectationToTupleFromTupleProcessorBuilder<int64_t>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                             ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                             std::make_shared<AffectationToTupleFromTupleProcessorBuilder<int64_t>>());

  repository.addAffectation<language::eq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<int64_t>>());

  repository.addAffectation<language::eq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<int64_t>>());
}

void
AffectationRegisterForZ::_register_pluseq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addAffectation<
    language::pluseq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::bool_t>(),
                         std::make_shared<AffectationProcessorBuilder<language::pluseq_op, int64_t, bool>>());

  repository.addAffectation<
    language::pluseq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>(),
                         std::make_shared<AffectationProcessorBuilder<language::pluseq_op, int64_t, uint64_t>>());

  repository.addAffectation<
    language::pluseq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::int_t>(),
                         std::make_shared<AffectationProcessorBuilder<language::pluseq_op, int64_t, int64_t>>());
}

void
AffectationRegisterForZ::_register_minuseq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addAffectation<
    language::minuseq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::bool_t>(),
                          std::make_shared<AffectationProcessorBuilder<language::minuseq_op, int64_t, bool>>());

  repository.addAffectation<
    language::minuseq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>(),
                          std::make_shared<AffectationProcessorBuilder<language::minuseq_op, int64_t, uint64_t>>());

  repository.addAffectation<
    language::minuseq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::int_t>(),
                          std::make_shared<AffectationProcessorBuilder<language::minuseq_op, int64_t, int64_t>>());
}

void
AffectationRegisterForZ::_register_multiplyeq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addAffectation<
    language::multiplyeq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::bool_t>(),
                             std::make_shared<AffectationProcessorBuilder<language::multiplyeq_op, int64_t, bool>>());

  repository.addAffectation<language::multiplyeq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>(),
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, int64_t, uint64_t>>());

  repository.addAffectation<language::multiplyeq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::int_t>(),
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, int64_t, int64_t>>());
}

void
AffectationRegisterForZ::_register_divideeq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addAffectation<
    language::divideeq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>(),
                           std::make_shared<AffectationProcessorBuilder<language::divideeq_op, int64_t, uint64_t>>());

  repository.addAffectation<
    language::divideeq_op>(Z, ASTNodeDataType::build<ASTNodeDataType::int_t>(),
                           std::make_shared<AffectationProcessorBuilder<language::divideeq_op, int64_t, int64_t>>());
}

AffectationRegisterForZ::AffectationRegisterForZ()
{
  BasicAffectationRegisterFor<int64_t>{};
  this->_register_eq_op();
  this->_register_pluseq_op();
  this->_register_minuseq_op();
  this->_register_multiplyeq_op();
  this->_register_divideeq_op();
}
