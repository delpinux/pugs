#ifndef EMBEDDED_DISCRETE_FUNCTION_UTILS_HPP
#define EMBEDDED_DISCRETE_FUNCTION_UTILS_HPP

#include <language/utils/ASTNodeDataType.hpp>
#include <scheme/DiscreteFunctionVariant.hpp>
#include <scheme/IDiscreteFunctionDescriptor.hpp>

#include <utils/Demangle.hpp>

#include <string>

struct EmbeddedDiscreteFunctionUtils
{
  template <typename T>
  static PUGS_INLINE std::string
  getOperandTypeName(const T& t)
  {
    if constexpr (is_shared_ptr_v<T>) {
      Assert(t.use_count() > 0, "dangling shared_ptr");
      return getOperandTypeName(*t);
    } else if constexpr (std::is_same_v<DiscreteFunctionVariant, std::decay_t<T>>) {
      return std::visit([](auto&& v) -> std::string { return getOperandTypeName(v); }, t.discreteFunction());
    } else if constexpr (is_discrete_function_v<std::decay_t<T>>) {
      std::string type_name = "Vh(" + name(t.descriptor().type()) + ':' + dataTypeName(t.dataType()) + ')';
      return type_name;
    } else {
      return dataTypeName(ast_node_data_type_from<T>);
    }
  }

  template <typename T1, typename T2>
  PUGS_INLINE static std::string
  incompatibleOperandTypes(const T1& t1, const T2& t2)
  {
    return "incompatible operand types " + getOperandTypeName(t1) + " and " + getOperandTypeName(t2);
  }

  template <typename T>
  PUGS_INLINE static std::string
  invalidOperandType(const T& t)
  {
    return "invalid operand type " + getOperandTypeName(t);
  }
};

#endif   // EMBEDDED_DISCRETE_FUNCTION_UTILS_HPP
