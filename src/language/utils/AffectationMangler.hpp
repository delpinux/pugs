#ifndef AFFECTATION_MANGLER_HPP
#define AFFECTATION_MANGLER_HPP

#include <language/utils/ASTNodeDataType.hpp>
#include <utils/Exceptions.hpp>

#include <string>

namespace language
{
struct eq_op;
struct multiplyeq_op;
struct divideeq_op;
struct pluseq_op;
struct minuseq_op;
}   // namespace language

template <typename AffectationOperatorT>
std::string
affectationMangler(const ASTNodeDataType& lhs, const ASTNodeDataType& rhs)
{
  const std::string lhs_name = dataTypeName(lhs);

  const std::string operator_name = [] {
    if constexpr (std::is_same_v<language::eq_op, AffectationOperatorT>) {
      return "=";
    } else if constexpr (std::is_same_v<language::multiplyeq_op, AffectationOperatorT>) {
      return "*=";
    } else if constexpr (std::is_same_v<language::divideeq_op, AffectationOperatorT>) {
      return "/=";
    } else if constexpr (std::is_same_v<language::pluseq_op, AffectationOperatorT>) {
      return "+=";
    } else if constexpr (std::is_same_v<language::minuseq_op, AffectationOperatorT>) {
      return "-=";
    } else {
      static_assert(std::is_same_v<language::eq_op, AffectationOperatorT>, "undefined affectation operator");
    }
  }();

  const std::string rhs_name = [&]() -> std::string {
    if (rhs == ASTNodeDataType::list_t) {
      return "list";
    } else {
      return dataTypeName(rhs);
    }
  }();
  return lhs_name + " " + operator_name + " " + rhs_name;
}

#endif   // AFFECTATION_MANGLER_HPP
