#ifndef UNARY_OPERATOR_REGISTER_FOR_Z_HPP
#define UNARY_OPERATOR_REGISTER_FOR_Z_HPP

class UnaryOperatorRegisterForZ
{
 private:
  void _register_unary_minus();

 public:
  UnaryOperatorRegisterForZ();
};

#endif   // UNARY_OPERATOR_REGISTER_FOR_Z_HPP
