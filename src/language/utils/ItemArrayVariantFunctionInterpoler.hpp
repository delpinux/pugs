#ifndef ITEM_ARRAY_VARIANT_FUNCTION_INTERPOLER_HPP
#define ITEM_ARRAY_VARIANT_FUNCTION_INTERPOLER_HPP

#include <language/utils/FunctionSymbolId.hpp>
#include <mesh/IZoneDescriptor.hpp>
#include <mesh/ItemArrayVariant.hpp>
#include <mesh/ItemType.hpp>
#include <mesh/MeshTraits.hpp>

class MeshVariant;

class ItemArrayVariantFunctionInterpoler
{
 private:
  std::shared_ptr<const MeshVariant> m_mesh_v;
  const ItemType m_item_type;
  const std::vector<FunctionSymbolId> m_function_id_list;

  template <MeshConcept MeshType, typename DataType>
  std::shared_ptr<ItemArrayVariant> _interpolate() const;

  template <MeshConcept MeshType>
  std::shared_ptr<ItemArrayVariant> _interpolate() const;

 public:
  std::shared_ptr<ItemArrayVariant> interpolate() const;

  ItemArrayVariantFunctionInterpoler(const std::shared_ptr<const MeshVariant>& mesh_v,
                                     const ItemType& item_type,
                                     const std::vector<FunctionSymbolId>& function_id_list)
    : m_mesh_v{mesh_v}, m_item_type{item_type}, m_function_id_list{function_id_list}
  {}

  ItemArrayVariantFunctionInterpoler(const ItemArrayVariantFunctionInterpoler&) = delete;
  ItemArrayVariantFunctionInterpoler(ItemArrayVariantFunctionInterpoler&&)      = delete;

  ~ItemArrayVariantFunctionInterpoler() = default;
};

#endif   // ITEM_ARRAY_VARIANT_FUNCTION_INTERPOLER_HPP
