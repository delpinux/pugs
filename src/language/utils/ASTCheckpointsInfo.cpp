#include <language/utils/ASTCheckpointsInfo.hpp>

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>

const ASTCheckpointsInfo* ASTCheckpointsInfo::m_checkpoints_info_instance = nullptr;

void
ASTCheckpointsInfo::_findASTCheckpoint(std::vector<size_t>& location, const ASTNode& node)
{
  if (node.is_type<language::function_evaluation>()) {
    const ASTNode& node_name = *node.children[0];
    if (node_name.is_type<language::name>() and (node_name.m_data_type == ASTNodeDataType::builtin_function_t) and
        ((node_name.string() == "checkpoint") or (node_name.string() == "checkpoint_and_exit"))) {
      m_ast_checkpoint_list.push_back(ASTCheckpoint{location, &node});
      return;
    }
  }

  if (node.children.size() > 0) {
    location.push_back(0);
    for (size_t i_node = 0; i_node < node.children.size(); ++i_node) {
      location[location.size() - 1] = i_node;
      this->_findASTCheckpoint(location, *node.children[i_node]);
    }
    location.pop_back();
  }
}

ASTCheckpointsInfo::ASTCheckpointsInfo(const ASTNode& root_node)
{
  Assert(m_checkpoints_info_instance == nullptr, "Can only define one ASTCheckpointsInfo");
  m_checkpoints_info_instance = this;

  Assert(root_node.is_root());

  std::vector<size_t> location;
  this->_findASTCheckpoint(location, root_node);

  Assert(location.size() == 0);
}

const ASTCheckpointsInfo&
ASTCheckpointsInfo::getInstance()
{
  Assert(m_checkpoints_info_instance != nullptr, "ASTCheckpointsInfo is not defined!");
  return *m_checkpoints_info_instance;
}

ASTCheckpointsInfo::~ASTCheckpointsInfo()
{
  m_checkpoints_info_instance = nullptr;
}
