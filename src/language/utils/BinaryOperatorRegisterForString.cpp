#include <language/utils/BinaryOperatorRegisterForString.hpp>

#include <language/utils/BinaryOperatorProcessorBuilder.hpp>
#include <language/utils/ConcatExpressionProcessorBuilder.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/OStream.hpp>
#include <language/utils/OperatorRepository.hpp>

void
BinaryOperatorRegisterForString::_register_ostream()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, std::string>>());

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, std::vector<std::string>>>());
}

void
BinaryOperatorRegisterForString::_register_comparisons()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::eqeq_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::eqeq_op, bool, std::string, std::string>>());

  repository.addBinaryOperator<language::not_eq_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::not_eq_op, bool, std::string, std::string>>());
}

template <typename T>
void
BinaryOperatorRegisterForString::_register_concat()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::plus_op>(std::make_shared<ConcatExpressionProcessorBuilder<std::string, T>>());
  if constexpr (not std::is_same_v<T, std::string>) {
    repository.addBinaryOperator<language::plus_op>(
      std::make_shared<ConcatExpressionProcessorBuilder<T, std::string>>());
  }
}

BinaryOperatorRegisterForString::BinaryOperatorRegisterForString()
{
  this->_register_ostream();

  this->_register_comparisons();

  this->_register_concat<bool>();
  this->_register_concat<unsigned long>();
  this->_register_concat<long>();
  this->_register_concat<double>();
  this->_register_concat<TinyVector<1>>();
  this->_register_concat<TinyVector<2>>();
  this->_register_concat<TinyVector<3>>();
  this->_register_concat<TinyMatrix<1>>();
  this->_register_concat<TinyMatrix<2>>();
  this->_register_concat<TinyMatrix<3>>();
  this->_register_concat<std::string>();
}
