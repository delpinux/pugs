#ifndef AFFECTATION_REGISTER_FOR_Z_HPP
#define AFFECTATION_REGISTER_FOR_Z_HPP

class AffectationRegisterForZ
{
 private:
  void _register_eq_op();
  void _register_pluseq_op();
  void _register_minuseq_op();
  void _register_multiplyeq_op();
  void _register_divideeq_op();

 public:
  AffectationRegisterForZ();
};

#endif   // AFFECTATION_REGISTER_FOR_Z_HPP
