#include <language/utils/AffectationRegisterForString.hpp>

#include <language/utils/AffectationProcessorBuilder.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/OperatorRepository.hpp>

void
AffectationRegisterForString::_register_eq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto B    = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N    = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z    = ASTNodeDataType::build<ASTNodeDataType::int_t>();
  auto R    = ASTNodeDataType::build<ASTNodeDataType::double_t>();
  auto R1   = ASTNodeDataType::build<ASTNodeDataType::vector_t>(1);
  auto R2   = ASTNodeDataType::build<ASTNodeDataType::vector_t>(2);
  auto R3   = ASTNodeDataType::build<ASTNodeDataType::vector_t>(3);
  auto R1x1 = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1);
  auto R2x2 = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2);
  auto R3x3 = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3);

  auto string_t = ASTNodeDataType::build<ASTNodeDataType::string_t>();

  repository.addAffectation<
    language::eq_op>(string_t, B, std::make_shared<AffectationProcessorBuilder<language::eq_op, std::string, bool>>());

  repository.addAffectation<
    language::eq_op>(string_t, N,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, std::string, uint64_t>>());

  repository.addAffectation<
    language::eq_op>(string_t, Z,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, std::string, int64_t>>());

  repository.addAffectation<
    language::eq_op>(string_t, R,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, std::string, double_t>>());

  repository.addAffectation<
    language::eq_op>(string_t, R1,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, std::string, TinyVector<1>>>());

  repository.addAffectation<
    language::eq_op>(string_t, R2,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, std::string, TinyVector<2>>>());

  repository.addAffectation<
    language::eq_op>(string_t, R3,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, std::string, TinyVector<3>>>());

  repository.addAffectation<
    language::eq_op>(string_t, R1x1,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, std::string, TinyMatrix<1>>>());

  repository.addAffectation<
    language::eq_op>(string_t, R2x2,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, std::string, TinyMatrix<2>>>());

  repository.addAffectation<
    language::eq_op>(string_t, R3x3,
                     std::make_shared<AffectationProcessorBuilder<language::eq_op, std::string, TinyMatrix<3>>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t), B,
                                             std::make_shared<AffectationToTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t), N,
                                             std::make_shared<AffectationToTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t), Z,
                                             std::make_shared<AffectationToTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t), R,
                                             std::make_shared<AffectationToTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t), R1,
                                             std::make_shared<AffectationToTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t), R2,
                                             std::make_shared<AffectationToTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t), R3,
                                             std::make_shared<AffectationToTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t), R1x1,
                                             std::make_shared<AffectationToTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t), R2x2,
                                             std::make_shared<AffectationToTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t), R3x3,
                                             std::make_shared<AffectationToTupleProcessorBuilder<std::string>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<std::string>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<std::string>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<std::string>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<std::string>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<std::string>>());
  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R2),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<std::string>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R3),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<std::string>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1x1),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<std::string>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R2x2),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<std::string>>());

  repository
    .addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(string_t),
                                     ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R3x3),
                                     std::make_shared<AffectationToTupleFromTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(string_t, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(string_t, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(string_t, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(string_t, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(string_t, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(string_t, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R2),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(string_t, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R3),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(string_t, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R1x1),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(string_t, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R2x2),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<std::string>>());

  repository.addAffectation<language::eq_op>(string_t, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R3x3),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<std::string>>());
}

void
AffectationRegisterForString::_register_pluseq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto B    = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N    = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z    = ASTNodeDataType::build<ASTNodeDataType::int_t>();
  auto R    = ASTNodeDataType::build<ASTNodeDataType::double_t>();
  auto R1   = ASTNodeDataType::build<ASTNodeDataType::vector_t>(1);
  auto R2   = ASTNodeDataType::build<ASTNodeDataType::vector_t>(2);
  auto R3   = ASTNodeDataType::build<ASTNodeDataType::vector_t>(3);
  auto R1x1 = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(1, 1);
  auto R2x2 = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(2, 2);
  auto R3x3 = ASTNodeDataType::build<ASTNodeDataType::matrix_t>(3, 3);

  auto string_t = ASTNodeDataType::build<ASTNodeDataType::string_t>();

  repository.addAffectation<language::pluseq_op>(string_t, string_t,
                                                 std::make_shared<AffectationProcessorBuilder<
                                                   language::pluseq_op, std::string, std::string>>());

  repository.addAffectation<
    language::pluseq_op>(string_t, B,
                         std::make_shared<AffectationProcessorBuilder<language::pluseq_op, std::string, bool>>());

  repository.addAffectation<
    language::pluseq_op>(string_t, N,
                         std::make_shared<AffectationProcessorBuilder<language::pluseq_op, std::string, uint64_t>>());

  repository.addAffectation<
    language::pluseq_op>(string_t, Z,
                         std::make_shared<AffectationProcessorBuilder<language::pluseq_op, std::string, int64_t>>());

  repository.addAffectation<
    language::pluseq_op>(string_t, R,
                         std::make_shared<AffectationProcessorBuilder<language::pluseq_op, std::string, double>>());

  repository.addAffectation<language::pluseq_op>(string_t, R1,
                                                 std::make_shared<AffectationProcessorBuilder<
                                                   language::pluseq_op, std::string, TinyVector<1>>>());

  repository.addAffectation<language::pluseq_op>(string_t, R2,
                                                 std::make_shared<AffectationProcessorBuilder<
                                                   language::pluseq_op, std::string, TinyVector<2>>>());

  repository.addAffectation<language::pluseq_op>(string_t, R3,
                                                 std::make_shared<AffectationProcessorBuilder<
                                                   language::pluseq_op, std::string, TinyVector<3>>>());

  repository.addAffectation<language::pluseq_op>(string_t, R1x1,
                                                 std::make_shared<AffectationProcessorBuilder<
                                                   language::pluseq_op, std::string, TinyMatrix<1>>>());

  repository.addAffectation<language::pluseq_op>(string_t, R2x2,
                                                 std::make_shared<AffectationProcessorBuilder<
                                                   language::pluseq_op, std::string, TinyMatrix<2>>>());

  repository.addAffectation<language::pluseq_op>(string_t, R3x3,
                                                 std::make_shared<AffectationProcessorBuilder<
                                                   language::pluseq_op, std::string, TinyMatrix<3>>>());
}

AffectationRegisterForString::AffectationRegisterForString()
{
  BasicAffectationRegisterFor<std::string>{};
  this->_register_eq_op();
  this->_register_pluseq_op();
}
