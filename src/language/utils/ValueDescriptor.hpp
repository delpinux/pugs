#ifndef VALUE_DESCRIPTOR_HPP
#define VALUE_DESCRIPTOR_HPP

#include <language/utils/ASTNodeDataType.hpp>
#include <language/utils/DataVariant.hpp>

#include <string>

class ValueDescriptor
{
 private:
  const ASTNodeDataType m_type;
  const DataVariant m_value;

 public:
  const ASTNodeDataType
  type() const
  {
    return m_type;
  }

  const DataVariant
  value() const
  {
    return m_value;
  }

  ValueDescriptor(const ASTNodeDataType& type, const DataVariant& value) : m_type{type}, m_value{value} {}

  ValueDescriptor(const ValueDescriptor&) = delete;
  ValueDescriptor(ValueDescriptor&&)      = delete;

  ~ValueDescriptor() = default;
};

#endif   // VALUE_DESCRIPTOR_HPP
