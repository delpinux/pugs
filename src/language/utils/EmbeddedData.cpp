#include <language/utils/EmbeddedData.hpp>

#include <language/utils/DataHandler.hpp>

std::ostream&
operator<<(std::ostream& os, const EmbeddedData&)
{
  os << "embedded_data";
  return os;
}

EmbeddedData::EmbeddedData() {}

EmbeddedData::~EmbeddedData() {}
