#ifndef INC_DEC_OPERATOR_REGISTER_FOR_Z_HPP
#define INC_DEC_OPERATOR_REGISTER_FOR_Z_HPP

class IncDecOperatorRegisterForZ
{
 private:
  void _register_unary_minusminus();
  void _register_unary_plusplus();
  void _register_post_minusminus();
  void _register_post_plusplus();

 public:
  IncDecOperatorRegisterForZ();
};

#endif   // INC_DEC_OPERATOR_REGISTER_FOR_Z_HPP
