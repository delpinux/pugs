#include <language/utils/ASTDotPrinter.hpp>

#include <language/PEGGrammar.hpp>
#include <utils/EscapedString.hpp>

void
ASTDotPrinter::_print(std::ostream& os, const ASTNode& node) const
{
  if (node.is_root()) {
    os << "  x" << m_node_number_map.at(&node) << " [ label=\"root \\n" << dataTypeName(node.m_data_type) << "\" ]\n";
  } else {
    if (node.has_content()) {
      os << "  x" << m_node_number_map.at(&node) << " [ label=\"" << node.name() << "\\n"
         << escapeString(node.string_view()) << "\\n"
         << dataTypeName(node.m_data_type) << "\" ]\n";
    } else {
      os << "  x" << m_node_number_map.at(&node) << " [ label=\"" << node.name() << "\\n"
         << dataTypeName(node.m_data_type) << "\" ]\n";
    }
  }
  if (!node.children.empty()) {
    os << "  x" << m_node_number_map.at(&node) << " -> { ";
    for (auto& child : node.children) {
      os << "x" << m_node_number_map.at(child.get()) << ((child == node.children.back()) ? " }\n" : ", ");
    }
    for (auto& child : node.children) {
      this->_print(os, *child);
    }
  }
}

std::ostream&
operator<<(std::ostream& os, const ASTDotPrinter& ast_printer)
{
  os << "digraph parse_tree\n{\n";
  ast_printer._print(os, ast_printer.m_node);
  os << "}\n";
  return os;
}

void
ASTDotPrinter::_buildNodeNumberMap(const ASTNode& node)
{
  m_node_number_map[&node] = m_node_number_map.size();
  for (auto& child : node.children) {
    this->_buildNodeNumberMap(*child);
  }
}

ASTDotPrinter::ASTDotPrinter(const ASTNode& node) : m_node{node}
{
  Assert(node.is_root());
  this->_buildNodeNumberMap(node);
}
