#include <language/utils/ASTNodeDataType.hpp>

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>
#include <language/utils/ASTNodeNaturalConversionChecker.hpp>
#include <language/utils/ParseError.hpp>
#include <language/utils/SymbolTable.hpp>
#include <utils/PugsAssert.hpp>
#include <utils/Stringify.hpp>

ASTNodeDataType
getVectorDataType(const ASTNode& type_node)
{
  if (not(type_node.is_type<language::vector_type>() and (type_node.children.size() == 2))) {
    throw ParseError("unexpected node type", type_node.begin());
  }
  ASTNode& dimension_node = *type_node.children[1];
  if (not dimension_node.is_type<language::integer>()) {
    throw ParseError("unexpected non integer constant dimension", dimension_node.begin());
  }
  const size_t dimension = std::stol(dimension_node.string());
  if (not(dimension > 0 and dimension <= 3)) {
    throw ParseError("invalid dimension (must be 1, 2 or 3)", dimension_node.begin());
  }

  return ASTNodeDataType::build<ASTNodeDataType::vector_t>(dimension);
}

ASTNodeDataType
getVectorExpressionType(const ASTNode& vector_expression_node)
{
  if (not(vector_expression_node.is_type<language::vector_expression>() and
          (vector_expression_node.children.size() > 0))) {
    throw ParseError("unexpected node type", vector_expression_node.begin());
  }

  const size_t dimension = vector_expression_node.children.size();
  if (not(dimension > 0 and dimension <= 3)) {
    throw ParseError("invalid dimension (must be 1, 2 or 3)", vector_expression_node.begin());
  }

  for (size_t i = 0; i < dimension; ++i) {
    ASTNodeNaturalConversionChecker(*vector_expression_node.children[i],
                                    ASTNodeDataType::build<ASTNodeDataType::double_t>());
  }

  return ASTNodeDataType::build<ASTNodeDataType::vector_t>(dimension);
}

ASTNodeDataType
getMatrixDataType(const ASTNode& type_node)
{
  if (not(type_node.is_type<language::matrix_type>() and (type_node.children.size() == 3))) {
    throw ParseError("unexpected node type", type_node.begin());
  }

  ASTNode& dimension0_node = *type_node.children[1];
  if (not dimension0_node.is_type<language::integer>()) {
    throw ParseError("unexpected non integer constant dimension", dimension0_node.begin());
  }
  const size_t dimension0 = std::stol(dimension0_node.string());

  ASTNode& dimension1_node = *type_node.children[2];
  if (not dimension1_node.is_type<language::integer>()) {
    throw ParseError("unexpected non integer constant dimension", dimension1_node.begin());
  }
  const size_t dimension1 = std::stol(dimension1_node.string());

  if (dimension0 != dimension1) {
    throw ParseError("only square matrices are supported", type_node.begin());
  }

  if (not(dimension0 > 0 and dimension0 <= 3)) {
    throw ParseError("invalid dimension (must be 1, 2 or 3)", dimension0_node.begin());
  }

  return ASTNodeDataType::build<ASTNodeDataType::matrix_t>(dimension0, dimension1);
}

ASTNodeDataType
getMatrixExpressionType(const ASTNode& matrix_expression_node)
{
  if (not(matrix_expression_node.is_type<language::matrix_expression>() and
          matrix_expression_node.children.size() > 0)) {
    throw ParseError("unexpected node type", matrix_expression_node.begin());
  }

  const size_t dimension0 = matrix_expression_node.children.size();
  if (not(dimension0 > 0 and dimension0 <= 3)) {
    throw ParseError("invalid dimension (must be 1, 2 or 3)", matrix_expression_node.begin());
  }
  for (size_t i = 0; i < dimension0; ++i) {
    if (not matrix_expression_node.children[i]->is_type<language::row_expression>()) {
      throw ParseError("expecting row expression", matrix_expression_node.children[i]->begin());
    }
  }

  const size_t dimension1 = matrix_expression_node.children[0]->children.size();
  if (dimension0 != dimension1) {
    throw ParseError("only square matrices are supported", matrix_expression_node.begin());
  }

  for (size_t i = 1; i < dimension0; ++i) {
    if (matrix_expression_node.children[i]->children.size() != dimension1) {
      throw ParseError("row must have same sizes", matrix_expression_node.begin());
    }
  }

  for (size_t i = 0; i < dimension0; ++i) {
    for (size_t j = 0; j < dimension1; ++j) {
      ASTNodeNaturalConversionChecker(*matrix_expression_node.children[i]->children[j],
                                      ASTNodeDataType::build<ASTNodeDataType::double_t>());
    }
  }

  return ASTNodeDataType::build<ASTNodeDataType::matrix_t>(dimension0, dimension1);
}

ASTNodeDataType
getTupleDataType(const ASTNode& type_node)
{
  const auto& content_node = type_node.children[0];

  if (content_node->is_type<language::type_name_id>()) {
    const std::string& type_name_id = content_node->string();

    auto& symbol_table = *type_node.m_symbol_table;

    const auto [i_type_symbol, found] = symbol_table.find(type_name_id, content_node->begin());
    if (not found) {
      throw ParseError("undefined type identifier", std::vector{content_node->begin()});
    } else if (i_type_symbol->attributes().dataType() != ASTNodeDataType::type_name_id_t) {
      std::ostringstream os;
      os << "invalid type identifier, '" << type_name_id << "' was previously defined as a '"
         << dataTypeName(i_type_symbol->attributes().dataType()) << '\'';
      throw ParseError(os.str(), std::vector{content_node->begin()});
    }

    content_node->m_data_type = ASTNodeDataType::build<ASTNodeDataType::type_id_t>(type_name_id);
  } else if (content_node->is_type<language::B_set>()) {
    content_node->m_data_type = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  } else if (content_node->is_type<language::Z_set>()) {
    content_node->m_data_type = ASTNodeDataType::build<ASTNodeDataType::int_t>();
  } else if (content_node->is_type<language::N_set>()) {
    content_node->m_data_type = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  } else if (content_node->is_type<language::R_set>()) {
    content_node->m_data_type = ASTNodeDataType::build<ASTNodeDataType::double_t>();
  } else if (content_node->is_type<language::vector_type>()) {
    content_node->m_data_type = getVectorDataType(*type_node.children[0]);
  } else if (content_node->is_type<language::matrix_type>()) {
    content_node->m_data_type = getMatrixDataType(*type_node.children[0]);
  } else if (content_node->is_type<language::string_type>()) {
    content_node->m_data_type = ASTNodeDataType::build<ASTNodeDataType::string_t>();
  } else {
    // LCOV_EXCL_START
    throw UnexpectedError("unexpected content type in tuple");
    // LCOV_EXCL_STOP
  }

  return ASTNodeDataType::build<ASTNodeDataType::tuple_t>(content_node->m_data_type);
}

std::string
dataTypeName(const ASTNodeDataType& data_type)
{
  switch (data_type) {
  case ASTNodeDataType::undefined_t: {
    return "undefined";
  }
  case ASTNodeDataType::bool_t: {
    return "B";
  }
  case ASTNodeDataType::unsigned_int_t: {
    return "N";
  }
  case ASTNodeDataType::int_t: {
    return "Z";
  }
  case ASTNodeDataType::double_t: {
    return "R";
  }
  case ASTNodeDataType::vector_t: {
    std::ostringstream data_type_name_vector;
    data_type_name_vector << "R^" << data_type.dimension();
    return data_type_name_vector.str();
  }
  case ASTNodeDataType::matrix_t: {
    std::ostringstream data_type_name_matrix;
    data_type_name_matrix << "R^" << data_type.numberOfRows() << "x" << data_type.numberOfColumns();
    return data_type_name_matrix.str();
  }
  case ASTNodeDataType::tuple_t: {
    std::ostringstream data_type_name_tuple;
    data_type_name_tuple << "(" << dataTypeName(data_type.contentType()) << ")";
    return data_type_name_tuple.str();
  }
  case ASTNodeDataType::list_t: {
    std::ostringstream data_type_name_list;
    const auto& data_type_list = data_type.contentTypeList();
    if (data_type_list.size() > 0) {
      if (*data_type_list[0] == ASTNodeDataType::list_t) {
        data_type_name_list << '(' << dataTypeName(*data_type_list[0]) << ')';
      } else {
        data_type_name_list << dataTypeName(*data_type_list[0]);
      }
      for (size_t i = 1; i < data_type_list.size(); ++i) {
        if (*data_type_list[i] == ASTNodeDataType::list_t) {
          data_type_name_list << "*(" << dataTypeName(*data_type_list[i]) << ')';
        } else {
          data_type_name_list << '*' << dataTypeName(*data_type_list[i]);
        }
      }
      return data_type_name_list.str();
    } else {
      return "void";
    }
  }
  case ASTNodeDataType::string_t: {
    return "string";
  }
  case ASTNodeDataType::typename_t: {
    return dataTypeName(data_type.contentType());
  }
  case ASTNodeDataType::type_name_id_t: {
    return "type_name_id";
  }
  case ASTNodeDataType::type_id_t: {
    return data_type.nameOfTypeId();
  }
  case ASTNodeDataType::function_t: {
    return "function";
  }
  case ASTNodeDataType::builtin_function_t: {
    return "builtin_function";
  }
  case ASTNodeDataType::void_t: {
    return "void";
  }
  // LCOV_EXCL_START
  default: {
    throw UnexpectedError("unknown data type");
  }
    // LCOV_EXCL_STOP
  }
}

std::string
dataTypeName(const std::vector<ASTNodeDataType>& data_type_vector)
{
  if (data_type_vector.size() == 0) {
    return dataTypeName(ASTNodeDataType::build<ASTNodeDataType::void_t>());
  } else if (data_type_vector.size() == 1) {
    return dataTypeName(data_type_vector[0]);
  } else {
    std::ostringstream os;
    os << dataTypeName(data_type_vector[0]);
    for (size_t i = 1; i < data_type_vector.size(); ++i) {
      os << '*' << dataTypeName(data_type_vector[i]);
    }
    return os.str();
  }
}

ASTNodeDataType
dataTypePromotion(const ASTNodeDataType& data_type_1, const ASTNodeDataType& data_type_2)
{
  if (data_type_1 == data_type_2) {
    return data_type_1;
  } else if ((std::max(data_type_1, data_type_2) <= ASTNodeDataType::double_t) and
             (std::min(data_type_1, data_type_2) >= ASTNodeDataType::bool_t)) {
    return std::max(data_type_1, data_type_2);
  } else if ((data_type_1 == ASTNodeDataType::string_t) and (data_type_2 < ASTNodeDataType::string_t) and
             (data_type_2 >= ASTNodeDataType::bool_t)) {
    return data_type_1;
  } else if ((data_type_1 >= ASTNodeDataType::bool_t) and (data_type_1 <= ASTNodeDataType::double_t) and
             (data_type_2 == ASTNodeDataType::vector_t)) {
    return data_type_2;
  } else {
    return ASTNodeDataType{};
  }
}

bool
isNaturalConversion(const ASTNodeDataType& data_type, const ASTNodeDataType& target_data_type)
{
  if (target_data_type == data_type) {
    if (data_type == ASTNodeDataType::type_id_t) {
      return (data_type.nameOfTypeId() == target_data_type.nameOfTypeId());
    } else if (data_type == ASTNodeDataType::vector_t) {
      return (data_type.dimension() == target_data_type.dimension());
    } else if (data_type == ASTNodeDataType::matrix_t) {
      return ((data_type.numberOfRows() == target_data_type.numberOfRows()) and
              (data_type.numberOfColumns() == target_data_type.numberOfColumns()));
    } else {
      return true;
    }
  } else if (target_data_type == ASTNodeDataType::bool_t) {
    return false;
  } else if (target_data_type == ASTNodeDataType::unsigned_int_t) {
    return ((data_type == ASTNodeDataType::int_t) or (data_type == ASTNodeDataType::bool_t));
  } else if (target_data_type == ASTNodeDataType::int_t) {
    return ((data_type == ASTNodeDataType::unsigned_int_t) or (data_type == ASTNodeDataType::bool_t));
  } else if (target_data_type == ASTNodeDataType::double_t) {
    return ((data_type == ASTNodeDataType::unsigned_int_t) or (data_type == ASTNodeDataType::int_t) or
            (data_type == ASTNodeDataType::bool_t));
  } else if (target_data_type == ASTNodeDataType::string_t) {
    return ((data_type >= ASTNodeDataType::bool_t) and (data_type < ASTNodeDataType::string_t));
  } else if (target_data_type == ASTNodeDataType::tuple_t) {
    return isNaturalConversion(data_type, target_data_type.contentType());
  }
  return false;
}
