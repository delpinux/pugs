#include <language/utils/UnaryOperatorRegisterForRn.hpp>

#include <language/utils/OperatorRepository.hpp>
#include <language/utils/UnaryOperatorProcessorBuilder.hpp>

template <size_t Dimension>
void
UnaryOperatorRegisterForRn<Dimension>::_register_unary_minus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addUnaryOperator<language::unary_minus>(
    std::make_shared<
      UnaryOperatorProcessorBuilder<language::unary_minus, TinyVector<Dimension>, TinyVector<Dimension>>>());
}

template <size_t Dimension>
UnaryOperatorRegisterForRn<Dimension>::UnaryOperatorRegisterForRn()
{
  this->_register_unary_minus();
}

template class UnaryOperatorRegisterForRn<1>;
template class UnaryOperatorRegisterForRn<2>;
template class UnaryOperatorRegisterForRn<3>;
