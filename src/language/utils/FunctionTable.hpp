#ifndef FUNCTION_TABLE_HPP
#define FUNCTION_TABLE_HPP

#include <language/ast/ASTNode.hpp>
#include <language/utils/ASTNodeDataType.hpp>
#include <language/utils/DataVariant.hpp>
#include <utils/PugsAssert.hpp>

#include <pegtl/position.hpp>

class FunctionDescriptor
{
  std::string m_name;

  std::unique_ptr<ASTNode> m_domain_mapping_node;
  std::unique_ptr<ASTNode> m_definition_node;

 public:
  const std::string&
  name() const
  {
    return m_name;
  }

  auto&
  domainMappingNode() const
  {
    Assert(m_domain_mapping_node, "undefined domain mapping node");
    return *m_domain_mapping_node;
  }

  auto&
  definitionNode() const
  {
    Assert(m_definition_node, "undefined expression node");
    return *m_definition_node;
  }

  FunctionDescriptor& operator=(FunctionDescriptor&&) = default;

  FunctionDescriptor(const std::string& name,
                     std::unique_ptr<ASTNode>&& domain_mapping_node,
                     std::unique_ptr<ASTNode>&& definition_node)
    : m_name{name}, m_domain_mapping_node(std::move(domain_mapping_node)), m_definition_node(std::move(definition_node))
  {}

  FunctionDescriptor(FunctionDescriptor&&) = default;
  FunctionDescriptor()                     = default;
  ~FunctionDescriptor()                    = default;
};

class FunctionTable
{
 private:
  std::vector<FunctionDescriptor> m_function_descriptor_list;

 public:
  PUGS_INLINE
  size_t
  size() const
  {
    return m_function_descriptor_list.size();
  }

  PUGS_INLINE
  FunctionDescriptor&
  operator[](size_t function_id)
  {
    Assert(function_id < m_function_descriptor_list.size());
    return m_function_descriptor_list[function_id];
  }

  PUGS_INLINE
  const FunctionDescriptor&
  operator[](size_t function_id) const
  {
    Assert(function_id < m_function_descriptor_list.size());
    return m_function_descriptor_list[function_id];
  }

  size_t
  add(FunctionDescriptor&& function_descriptor)
  {
    m_function_descriptor_list.emplace_back(std::move(function_descriptor));
    return m_function_descriptor_list.size() - 1;
  }

  FunctionTable()  = default;
  ~FunctionTable() = default;
};

#endif   // FUNCTION_TABLE_HPP
