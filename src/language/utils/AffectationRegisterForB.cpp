#include <language/utils/AffectationRegisterForB.hpp>

#include <language/utils/AffectationProcessorBuilder.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/OperatorRepository.hpp>

AffectationRegisterForB::AffectationRegisterForB()
{
  BasicAffectationRegisterFor<bool>{};
}
