#ifndef AFFECTATION_REGISTER_FOR_STRING_HPP
#define AFFECTATION_REGISTER_FOR_STRING_HPP

class AffectationRegisterForString
{
 private:
  void _register_eq_op();
  void _register_pluseq_op();

 public:
  AffectationRegisterForString();
};

#endif   // AFFECTATION_REGISTER_FOR_STRING_HPP
