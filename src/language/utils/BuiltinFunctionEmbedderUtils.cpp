#include <language/utils/BuiltinFunctionEmbedderUtils.hpp>

#include <language/PEGGrammar.hpp>
#include <language/ast/ASTNode.hpp>
#include <language/utils/BuiltinFunctionEmbedder.hpp>
#include <language/utils/ParseError.hpp>
#include <language/utils/SymbolTable.hpp>

std::shared_ptr<IBuiltinFunctionEmbedder>
getBuiltinFunctionEmbedder(ASTNode& n)
{
  const std::string builtin_function_name = n.children[0]->string();
  auto& symbol_table                      = *n.m_symbol_table;

  auto& args_node = *n.children[1];

  auto get_argument_value_type = [](ASTNode& argument_node, std::vector<ASTNodeDataType>& arg_type_list) -> void {
    if (argument_node.is_type<language::function_evaluation>()) {
      ASTNode& function_node = *argument_node.children[0];
      switch (function_node.m_data_type) {
      case ASTNodeDataType::function_t: {
        auto [i_function_symbol, found] =
          argument_node.m_symbol_table->find(function_node.string(), argument_node.begin());
        Assert(found);
        Assert(i_function_symbol->attributes().dataType() == ASTNodeDataType::function_t);

        uint64_t function_id = std::get<uint64_t>(i_function_symbol->attributes().value());

        FunctionDescriptor& function_descriptor = argument_node.m_symbol_table->functionTable()[function_id];
        ASTNode& function_image_domain          = *function_descriptor.domainMappingNode().children[1];
        if (not(function_image_domain.is_type<language::vector_type>() or
                function_image_domain.is_type<language::matrix_type>()) and
            function_image_domain.children.size() > 0) {
          for (size_t i = 0; i < function_image_domain.children.size(); ++i) {
            arg_type_list.push_back(function_image_domain.children[i]->m_data_type);
          }
        } else {
          arg_type_list.push_back(function_image_domain.m_data_type);
        }
        break;
      }
      case ASTNodeDataType::builtin_function_t: {
        std::shared_ptr builtin_function = getBuiltinFunctionEmbedder(argument_node);
        arg_type_list.push_back(builtin_function->getReturnDataType());
        break;
      }
        // LCOV_EXCL_START
      default: {
        throw UnexpectedError("unexpected function type");
      }
        // LCOV_EXCL_STOP
      }
    } else {
      arg_type_list.push_back(argument_node.m_data_type);
    }
  };

  std::vector<ASTNodeDataType> arg_type_list;
  if (args_node.is_type<language::function_argument_list>()) {
    for (auto& arg : args_node.children) {
      get_argument_value_type(*arg, arg_type_list);
    }
  } else {
    get_argument_value_type(args_node, arg_type_list);
  }

  // replace typenames by their values
  for (size_t i = 0; i < arg_type_list.size(); ++i) {
    if (arg_type_list[i] == ASTNodeDataType::typename_t) {
      arg_type_list[i] = arg_type_list[i].contentType();
    }
  }

  std::string mangled_name = builtin_function_name + ':' + dataTypeName(args_node.m_data_type);

  std::vector builtin_function_candidate_list =
    symbol_table.getBuiltinFunctionSymbolList(builtin_function_name, n.begin());

  auto is_castable_to_vector = [](const ASTNodeDataType& arg_type, const ASTNodeDataType& target_type) {
    if (isNaturalConversion(arg_type, target_type)) {
      return true;
    }
    bool is_castable = true;
    if (target_type.dimension() > 1) {
      return (arg_type == ASTNodeDataType::int_t);
    } else {
      is_castable &= isNaturalConversion(arg_type, ASTNodeDataType::build<ASTNodeDataType::double_t>());
    }
    return is_castable;
  };

  auto is_castable_to_matrix = [](const ASTNodeDataType& arg_type, const ASTNodeDataType& target_type) {
    if (isNaturalConversion(arg_type, target_type)) {
      return true;
    }

    bool is_castable = true;
    if (target_type.numberOfRows() > 1) {
      return (arg_type == ASTNodeDataType::int_t);
    } else {
      is_castable &= isNaturalConversion(arg_type, ASTNodeDataType::build<ASTNodeDataType::double_t>());
    }
    return is_castable;
  };

  std::vector<uint64_t> callable_id_list;
  for (auto candidate : builtin_function_candidate_list) {
    uint64_t builtin_function_id = std::get<uint64_t>(candidate.attributes().value());

    auto& builtin_function_embedder_table     = n.m_symbol_table->builtinFunctionEmbedderTable();
    std::shared_ptr builtin_function_embedder = builtin_function_embedder_table[builtin_function_id];

    bool is_castable = true;

    if (builtin_function_embedder->numberOfParameters() == arg_type_list.size()) {
      std::vector<ASTNodeDataType> builtin_function_parameter_type_list =
        builtin_function_embedder->getParameterDataTypes();
      for (size_t i_arg = 0; i_arg < arg_type_list.size(); ++i_arg) {
        const ASTNodeDataType& target_type = builtin_function_parameter_type_list[i_arg];
        const ASTNodeDataType& arg_type    = arg_type_list[i_arg];

        if (not isNaturalConversion(arg_type, target_type)) {
          switch (target_type) {
          case ASTNodeDataType::vector_t: {
            is_castable &= is_castable_to_vector(arg_type, target_type);
            break;
          }
          case ASTNodeDataType::matrix_t: {
            is_castable &= is_castable_to_matrix(arg_type, target_type);
            break;
          }
          case ASTNodeDataType::tuple_t: {
            ASTNodeDataType tuple_content_type = target_type.contentType();
            if (not isNaturalConversion(arg_type, tuple_content_type)) {
              switch (tuple_content_type) {
              case ASTNodeDataType::vector_t: {
                if (arg_type == ASTNodeDataType::list_t) {
                  for (const auto& element_type : arg_type.contentTypeList()) {
                    is_castable &= is_castable_to_vector(*element_type, tuple_content_type);
                  }
                } else {
                  is_castable &= is_castable_to_vector(arg_type, tuple_content_type);
                }
                break;
              }
              case ASTNodeDataType::matrix_t: {
                if (arg_type == ASTNodeDataType::list_t) {
                  for (const auto& element_type : arg_type.contentTypeList()) {
                    is_castable &= is_castable_to_matrix(*element_type, tuple_content_type);
                  }
                } else {
                  is_castable &= is_castable_to_matrix(arg_type, tuple_content_type);
                }
                break;
              }
              default:
                if (arg_type == ASTNodeDataType::list_t) {
                  for (const auto& element_type : arg_type.contentTypeList()) {
                    is_castable &= isNaturalConversion(*element_type, tuple_content_type);
                  }
                } else {
                  is_castable &= false;
                }
              }
            }
            break;
          }
          default:
            is_castable &= false;
          }
        }
      }

      if (is_castable) {
        callable_id_list.push_back(builtin_function_id);
      }
    }
  }

  const uint64_t builtin_function_id = [&] {
    switch (size(callable_id_list)) {
    case 0: {
      std::ostringstream error_msg;
      error_msg << "no matching function to call " << rang::fgB::red << builtin_function_name << rang::style::reset
                << rang::style::bold << ": " << rang::fgB::yellow << dataTypeName(args_node.m_data_type)
                << rang::style::reset << rang::style::bold << "\nnote: candidates are";

      for (auto candidate : builtin_function_candidate_list) {
        uint64_t candidate_builtin_function_id = std::get<uint64_t>(candidate.attributes().value());

        auto& builtin_function_embedder_table = n.m_symbol_table->builtinFunctionEmbedderTable();
        std::shared_ptr candidate_builtin_function_embedder =
          builtin_function_embedder_table[candidate_builtin_function_id];

        error_msg << "\n " << builtin_function_name << ": "
                  << dataTypeName(candidate_builtin_function_embedder->getParameterDataTypes()) << " -> "
                  << dataTypeName(candidate_builtin_function_embedder->getReturnDataType());
      }

      throw ParseError(error_msg.str(), n.begin());
    }
    case 1: {
      return callable_id_list[0];
    }
    default: {
      auto& builtin_function_embedder_table = n.m_symbol_table->builtinFunctionEmbedderTable();
      for (auto callable_id : callable_id_list) {
        std::shared_ptr builtin_function_embedder = builtin_function_embedder_table[callable_id];
        // If has exact match return it
        if (dataTypeName(args_node.m_data_type) == dataTypeName(builtin_function_embedder->getParameterDataTypes())) {
          return callable_id;
        }
      }
      // else this is an ambiguous call
      std::ostringstream error_msg;
      error_msg << "ambiguous function call " << rang::fgB::red << builtin_function_name << rang::style::reset
                << rang::style::bold << ": " << rang::fgB::yellow << dataTypeName(args_node.m_data_type)
                << rang::style::reset << rang::style::bold << "\nnote: candidates are";

      for (auto callable_id : callable_id_list) {
        std::shared_ptr builtin_function_embedder = builtin_function_embedder_table[callable_id];

        error_msg << "\n " << builtin_function_name << ": "
                  << dataTypeName(builtin_function_embedder->getParameterDataTypes()) << " -> "
                  << dataTypeName(builtin_function_embedder->getReturnDataType());
      }
      throw ParseError(error_msg.str(), n.begin());
    }
    }
  }();

  return n.m_symbol_table->builtinFunctionEmbedderTable()[builtin_function_id];
}
