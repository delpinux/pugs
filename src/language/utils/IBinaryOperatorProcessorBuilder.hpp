#ifndef I_BINARY_OPERATOR_PROCESSOR_BUILDER_HPP
#define I_BINARY_OPERATOR_PROCESSOR_BUILDER_HPP

class ASTNode;
class INodeProcessor;

#include <language/utils/ASTNodeDataType.hpp>

#include <memory>

class IBinaryOperatorProcessorBuilder
{
 public:
  [[nodiscard]] virtual std::unique_ptr<INodeProcessor> getNodeProcessor(ASTNode& node) const = 0;

  [[nodiscard]] virtual ASTNodeDataType getReturnValueType() const = 0;
  [[nodiscard]] virtual ASTNodeDataType getDataTypeOfA() const     = 0;
  [[nodiscard]] virtual ASTNodeDataType getDataTypeOfB() const     = 0;

  virtual ~IBinaryOperatorProcessorBuilder() = default;
};

#endif   // I_BINARY_OPERATOR_PROCESSOR_BUILDER_HPP
