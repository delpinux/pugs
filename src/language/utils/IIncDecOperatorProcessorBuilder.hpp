#ifndef I_INC_DEC_OPERATOR_PROCESSOR_BUILDER_HPP
#define I_INC_DEC_OPERATOR_PROCESSOR_BUILDER_HPP

class ASTNode;
class INodeProcessor;
#include <language/utils/ASTNodeDataType.hpp>

#include <memory>

class IIncDecOperatorProcessorBuilder
{
 public:
  [[nodiscard]] virtual std::unique_ptr<INodeProcessor> getNodeProcessor(ASTNode& node) const = 0;

  [[nodiscard]] virtual ASTNodeDataType getReturnValueType() const = 0;

  virtual ~IIncDecOperatorProcessorBuilder() = default;
};

#endif   // I_INC_DEC_OPERATOR_PROCESSOR_BUILDER_HPP
