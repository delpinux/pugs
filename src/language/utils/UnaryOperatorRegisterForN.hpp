#ifndef UNARY_OPERATOR_REGISTER_FOR_N_HPP
#define UNARY_OPERATOR_REGISTER_FOR_N_HPP

class UnaryOperatorRegisterForN
{
 private:
  void _register_unary_minus();

 public:
  UnaryOperatorRegisterForN();
};

#endif   // UNARY_OPERATOR_REGISTER_FOR_N_HPP
