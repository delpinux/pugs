#ifndef INTERPOLATE_ITEM_ARRAY_HPP
#define INTERPOLATE_ITEM_ARRAY_HPP

#include <language/utils/EvaluateArrayAtPoints.hpp>
#include <language/utils/InterpolateItemValue.hpp>
#include <mesh/ItemArray.hpp>
#include <mesh/ItemType.hpp>

template <typename T>
class InterpolateItemArray;
template <typename OutputType, typename InputType>
class InterpolateItemArray<OutputType(InputType)>
{
  static constexpr size_t Dimension = OutputType::Dimension;

 private:
  PUGS_INLINE static bool
  _isSingleTupleFunction(const std::vector<FunctionSymbolId>& function_symbol_id_list)
  {
    if (function_symbol_id_list.size() > 1) {
      return false;
    } else {
      Assert(function_symbol_id_list.size() == 1);
      const FunctionSymbolId& function_symbol_id = function_symbol_id_list[0];
      return (function_symbol_id.descriptor().domainMappingNode().children[1]->m_data_type == ASTNodeDataType::tuple_t);
    }
  }

 public:
  template <ItemType item_type>
  PUGS_INLINE static ItemArray<OutputType, item_type>
  interpolate(const std::vector<FunctionSymbolId>& function_symbol_id_list,
              const ItemValue<const InputType, item_type>& position)
  {
    if (_isSingleTupleFunction(function_symbol_id_list)) {
      const FunctionSymbolId& function_symbol_id = function_symbol_id_list[0];
      const size_t table_size = function_symbol_id.descriptor().definitionNode().children[1]->children.size();

      ItemArray<OutputType, item_type> item_array{*position.connectivity_ptr(), table_size};
      EvaluateArrayAtPoints<OutputType(const InputType)>::evaluateTo(function_symbol_id, position, item_array);

      return item_array;
    } else {
      ItemArray<OutputType, item_type> item_array{*position.connectivity_ptr(), function_symbol_id_list.size()};

      for (size_t i_function_symbol = 0; i_function_symbol < function_symbol_id_list.size(); ++i_function_symbol) {
        const FunctionSymbolId& function_symbol_id = function_symbol_id_list[i_function_symbol];
        ItemValue<OutputType, item_type> item_value =
          InterpolateItemValue<OutputType(InputType)>::interpolate(function_symbol_id, position);
        parallel_for(
          item_value.numberOfItems(),
          PUGS_LAMBDA(ItemIdT<item_type> item_id) { item_array[item_id][i_function_symbol] = item_value[item_id]; });
      }

      return item_array;
    }
  }

  template <ItemType item_type>
  PUGS_INLINE static Table<OutputType>
  interpolate(const std::vector<FunctionSymbolId>& function_symbol_id_list,
              const ItemValue<const InputType, item_type>& position,
              const Array<const ItemIdT<item_type>>& list_of_items)
  {
    if (_isSingleTupleFunction(function_symbol_id_list)) {
      Array<InputType> item_position{list_of_items.size()};
      using ItemId = ItemIdT<item_type>;
      parallel_for(
        list_of_items.size(), PUGS_LAMBDA(size_t i_item) {
          ItemId item_id        = list_of_items[i_item];
          item_position[i_item] = position[item_id];
        });

      const FunctionSymbolId& function_symbol_id = function_symbol_id_list[0];
      const size_t table_size = function_symbol_id.descriptor().definitionNode().children[1]->children.size();

      Table<OutputType> table{list_of_items.size(), table_size};
      EvaluateArrayAtPoints<OutputType(const InputType)>::evaluateTo(function_symbol_id, item_position, table);

      return table;
    } else {
      Table<OutputType> table{list_of_items.size(), function_symbol_id_list.size()};

      for (size_t i_function_symbol = 0; i_function_symbol < function_symbol_id_list.size(); ++i_function_symbol) {
        const FunctionSymbolId& function_symbol_id = function_symbol_id_list[i_function_symbol];
        Array<OutputType> array =
          InterpolateItemValue<OutputType(InputType)>::interpolate(function_symbol_id, position, list_of_items);

        parallel_for(
          array.size(), PUGS_LAMBDA(size_t i) { table[i][i_function_symbol] = array[i]; });
      }

      return table;
    }
  }

  template <ItemType item_type>
  PUGS_INLINE static Table<OutputType>
  interpolate(const std::vector<FunctionSymbolId>& function_symbol_id_list,
              const ItemValue<const InputType, item_type>& position,
              const Array<ItemIdT<item_type>>& list_of_items)
  {
    return interpolate(function_symbol_id_list, position, Array<const ItemIdT<item_type>>{list_of_items});
  }
};

#endif   // INTERPOLATE_ITEM_ARRAY_HPP
