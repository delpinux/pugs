#include <language/utils/IncDecOperatorRegisterForN.hpp>

#include <language/utils/IncDecOperatorProcessorBuilder.hpp>
#include <language/utils/OperatorRepository.hpp>

void
IncDecOperatorRegisterForN::_register_unary_minusminus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();

  repository.addIncDecOperator<language::unary_minusminus>(N, std::make_shared<IncDecOperatorProcessorBuilder<
                                                                language::unary_minusminus, uint64_t>>());
}

void
IncDecOperatorRegisterForN::_register_unary_plusplus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();

  repository.addIncDecOperator<
    language::unary_plusplus>(N,
                              std::make_shared<IncDecOperatorProcessorBuilder<language::unary_plusplus, uint64_t>>());
}

void
IncDecOperatorRegisterForN::_register_post_minusminus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();

  repository.addIncDecOperator<
    language::post_minusminus>(N,
                               std::make_shared<IncDecOperatorProcessorBuilder<language::post_minusminus, uint64_t>>());
}

void
IncDecOperatorRegisterForN::_register_post_plusplus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();

  repository.addIncDecOperator<
    language::post_plusplus>(N, std::make_shared<IncDecOperatorProcessorBuilder<language::post_plusplus, uint64_t>>());
}

IncDecOperatorRegisterForN::IncDecOperatorRegisterForN()
{
  this->_register_unary_minusminus();
  this->_register_unary_plusplus();
  this->_register_post_minusminus();
  this->_register_post_plusplus();
}
