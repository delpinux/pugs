#ifndef BINARY_OPERATOR_REGISTER_FOR_STRING_HPP
#define BINARY_OPERATOR_REGISTER_FOR_STRING_HPP

class BinaryOperatorRegisterForString
{
 private:
  void _register_ostream();

  void _register_comparisons();

  template <typename RHS_T>
  void _register_concat();

 public:
  BinaryOperatorRegisterForString();
};

#endif   // BINARY_OPERATOR_REGISTER_FOR_STRING_HPP
