#include <language/utils/BinaryOperatorRegisterForN.hpp>

#include <language/utils/BasicBinaryOperatorRegisterComparisonOf.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/OStream.hpp>

void
BinaryOperatorRegisterForN::_register_ostream()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, uint64_t>>());

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, std::vector<uint64_t>>>());
}

void
BinaryOperatorRegisterForN::_register_comparisons()
{
  BasicBinaryOperatorRegisterComparisonOf<bool, uint64_t>{};
  BasicBinaryOperatorRegisterComparisonOf<uint64_t, bool>{};

  BasicBinaryOperatorRegisterComparisonOf<uint64_t, uint64_t>{};
}

template <typename OperatorT>
void
BinaryOperatorRegisterForN::_register_arithmetic()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, uint64_t, uint64_t, bool>>());
  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, uint64_t, bool, uint64_t>>());

  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, uint64_t, uint64_t, uint64_t>>());
}

void
BinaryOperatorRegisterForN::_register_divide()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, uint64_t, bool, uint64_t>>());

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, uint64_t, uint64_t, uint64_t>>());
}

void
BinaryOperatorRegisterForN::_register_minus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, int64_t, uint64_t, bool>>());
  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, int64_t, bool, uint64_t>>());

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, int64_t, uint64_t, uint64_t>>());
}

BinaryOperatorRegisterForN::BinaryOperatorRegisterForN()
{
  this->_register_ostream();
  this->_register_comparisons();
  this->_register_arithmetic<language::plus_op>();
  this->_register_minus();
  this->_register_arithmetic<language::multiply_op>();
  this->_register_divide();
}
