#ifndef I_UNARY_OPERATOR_PROCESSOR_BUILDER_HPP
#define I_UNARY_OPERATOR_PROCESSOR_BUILDER_HPP

class ASTNode;
class INodeProcessor;

#include <language/utils/ASTNodeDataType.hpp>

#include <memory>

class IUnaryOperatorProcessorBuilder
{
 public:
  [[nodiscard]] virtual std::unique_ptr<INodeProcessor> getNodeProcessor(ASTNode& node) const = 0;

  [[nodiscard]] virtual ASTNodeDataType getOperandDataType() const = 0;
  [[nodiscard]] virtual ASTNodeDataType getReturnValueType() const = 0;

  virtual ~IUnaryOperatorProcessorBuilder() = default;
};

#endif   // I_UNARY_OPERATOR_PROCESSOR_BUILDER_HPP
