#ifndef EXIT_HPP
#define EXIT_HPP

namespace language
{

class Exit
{
 private:
  int m_code = 0;

 public:
  int
  code() const
  {
    return m_code;
  }

  Exit(int code) : m_code{code} {}

  Exit(const Exit&) = default;
  Exit(Exit&&)      = default;

  ~Exit() = default;
};

}   // namespace language

#endif   // EXIT_HPP
