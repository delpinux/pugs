#ifndef INTERPOLATE_ITEM_VALUE_HPP
#define INTERPOLATE_ITEM_VALUE_HPP

#include <language/utils/EvaluateAtPoints.hpp>
#include <mesh/ItemType.hpp>
#include <mesh/ItemValue.hpp>

template <typename T>
class InterpolateItemValue;
template <typename OutputType, typename InputType>
class InterpolateItemValue<OutputType(InputType)>
{
 public:
  template <ItemType item_type>
  PUGS_INLINE static ItemValue<OutputType, item_type>
  interpolate(const FunctionSymbolId& function_symbol_id, const ItemValue<const InputType, item_type>& position)
  {
    const IConnectivity& connectivity = *position.connectivity_ptr();
    ItemValue<OutputType, item_type> value(connectivity);
    EvaluateAtPoints<OutputType(const InputType)>::evaluateTo(function_symbol_id, position, value);

    return value;
  }

  template <ItemType item_type>
  PUGS_INLINE static Array<OutputType>
  interpolate(const FunctionSymbolId& function_symbol_id,
              const ItemValue<const InputType, item_type>& position,
              const Array<const ItemIdT<item_type>>& list_of_items)
  {
    Array<InputType> item_position{list_of_items.size()};
    using ItemId = ItemIdT<item_type>;
    parallel_for(
      list_of_items.size(), PUGS_LAMBDA(size_t i_item) {
        ItemId item_id        = list_of_items[i_item];
        item_position[i_item] = position[item_id];
      });

    return EvaluateAtPoints<OutputType(const InputType)>::evaluate(function_symbol_id, item_position);
  }

  template <ItemType item_type>
  PUGS_INLINE static Array<OutputType>
  interpolate(const FunctionSymbolId& function_symbol_id,
              const ItemValue<const InputType, item_type>& position,
              const Array<ItemIdT<item_type>>& list_of_items)
  {
    return interpolate(function_symbol_id, position, Array<const ItemIdT<item_type>>{list_of_items});
  }
};

#endif   // INTERPOLATE_ITEM_VALUE_HPP
