#ifndef UNARY_OPERATOR_REGISTER_FOR_RN_HPP
#define UNARY_OPERATOR_REGISTER_FOR_RN_HPP

#include <cstdlib>

template <size_t Dimension>
class UnaryOperatorRegisterForRn
{
 private:
  void _register_unary_minus();

 public:
  UnaryOperatorRegisterForRn();
};

#endif   // UNARY_OPERATOR_REGISTER_FOR_RN_HPP
