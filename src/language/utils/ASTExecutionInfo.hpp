#ifndef AST_EXECUTION_INFO_HPP
#define AST_EXECUTION_INFO_HPP

#include <string>

class ModuleRepository;
class ASTNode;
class ASTExecutionInfo
{
 private:
  static const ASTExecutionInfo* m_execution_info_instance;

  const ASTNode& m_root_node;

  const ModuleRepository& m_module_repository;

  // The only place where the ASTExecutionInfo can be built
  friend void parser(const std::string& filename);
  // also allowed for testing
  friend void test_ASTExecutionInfo(const ASTNode&, const ModuleRepository&);

  ASTExecutionInfo(const ASTNode& root_node, const ModuleRepository& module_repository);

 public:
  const ASTNode&
  rootNode() const
  {
    return m_root_node;
  }

  const ModuleRepository&
  moduleRepository() const
  {
    return m_module_repository;
  }

  static const ASTExecutionInfo& getInstance();

  ASTExecutionInfo() = delete;

  ~ASTExecutionInfo();
};

#endif   // AST_EXECUTION_INFO_HPP
