#include <language/utils/UnaryOperatorRegisterForZ.hpp>

#include <language/utils/OperatorRepository.hpp>
#include <language/utils/UnaryOperatorProcessorBuilder.hpp>

void
UnaryOperatorRegisterForZ::_register_unary_minus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addUnaryOperator<language::unary_minus>(
    std::make_shared<UnaryOperatorProcessorBuilder<language::unary_minus, int64_t, int64_t>>());
}

UnaryOperatorRegisterForZ::UnaryOperatorRegisterForZ()
{
  this->_register_unary_minus();
}
