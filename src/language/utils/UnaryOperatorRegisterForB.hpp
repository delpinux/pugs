#ifndef UNARY_OPERATOR_REGISTER_FOR_B_HPP
#define UNARY_OPERATOR_REGISTER_FOR_B_HPP

class UnaryOperatorRegisterForB
{
 private:
  void _register_unary_minus();
  void _register_unary_not();

 public:
  UnaryOperatorRegisterForB();
};

#endif   // UNARY_OPERATOR_REGISTER_FOR_B_HPP
