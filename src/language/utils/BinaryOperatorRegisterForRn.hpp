#ifndef BINARY_OPERATOR_REGISTER_FOR_RN_HPP
#define BINARY_OPERATOR_REGISTER_FOR_RN_HPP

#include <cstdlib>

template <size_t Dimension>
class BinaryOperatorRegisterForRn
{
 private:
  void _register_ostream();
  void _register_comparisons();
  void _register_product_by_a_scalar();
  template <typename OperatorT>
  void _register_arithmetic();

 public:
  BinaryOperatorRegisterForRn();
};

#endif   // BINARY_OPERATOR_REGISTER_FOR_RN_HPP
