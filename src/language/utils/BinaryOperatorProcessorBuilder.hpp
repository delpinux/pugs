#ifndef BINARY_OPERATOR_PROCESSOR_BUILDER_HPP
#define BINARY_OPERATOR_PROCESSOR_BUILDER_HPP

#include <algebra/TinyVector.hpp>
#include <language/PEGGrammar.hpp>
#include <language/node_processor/BinaryExpressionProcessor.hpp>
#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <language/utils/IBinaryOperatorProcessorBuilder.hpp>
#include <language/utils/ParseError.hpp>

template <typename OperatorT, typename ValueT, typename A_DataT, typename B_DataT>
class BinaryOperatorProcessorBuilder final : public IBinaryOperatorProcessorBuilder
{
 public:
  BinaryOperatorProcessorBuilder() = default;

  ASTNodeDataType
  getDataTypeOfA() const
  {
    return ast_node_data_type_from<A_DataT>;
  }

  ASTNodeDataType
  getDataTypeOfB() const
  {
    return ast_node_data_type_from<B_DataT>;
  }

  ASTNodeDataType
  getReturnValueType() const
  {
    return ast_node_data_type_from<ValueT>;
  }

  std::unique_ptr<INodeProcessor>
  getNodeProcessor(ASTNode& node) const
  {
    return std::make_unique<BinaryExpressionProcessor<OperatorT, ValueT, A_DataT, B_DataT>>(node);
  }
};

#endif   // BINARY_OPERATOR_PROCESSOR_BUILDER_HPP
