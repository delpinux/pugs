#ifndef AST_CHECKPOINT_HPP
#define AST_CHECKPOINT_HPP

#include <vector>

class ASTNode;
class ASTCheckpoint
{
 private:
  const ASTNode* const m_p_node;
  const std::vector<size_t> m_ast_location;

 public:
  const ASTNode&
  node() const
  {
    return *m_p_node;
  }

  const std::vector<size_t>&
  getASTLocation() const
  {
    return m_ast_location;
  }

  ASTCheckpoint(const std::vector<size_t>& ast_location, const ASTNode* const p_node)
    : m_p_node{p_node}, m_ast_location{ast_location}
  {}

  ASTCheckpoint(const ASTCheckpoint&) = default;
  ASTCheckpoint(ASTCheckpoint&&)      = default;

  ~ASTCheckpoint() = default;
};

#endif   // AST_CHECKPOINT_HPP
