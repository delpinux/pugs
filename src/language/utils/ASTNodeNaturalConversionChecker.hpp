#ifndef AST_NODE_NATURAL_CONVERSION_CHECKER_HPP
#define AST_NODE_NATURAL_CONVERSION_CHECKER_HPP

#include <language/ast/ASTNode.hpp>
#include <language/ast/ASTNodeSubDataType.hpp>
#include <language/utils/ASTNodeDataType.hpp>

struct AllowRToR1Conversion
{
};

struct DisallowRToR1Conversion
{
};

template <typename RToR1Conversion = DisallowRToR1Conversion>
class ASTNodeNaturalConversionChecker
{
 private:
  using RToR1ConversionStrategy = RToR1Conversion;

  void _checkIsNaturalTypeConversion(const ASTNode& ast_node,
                                     const ASTNodeDataType& data_type,
                                     const ASTNodeDataType& target_data_type) const;

  void _checkIsNaturalExpressionConversion(const ASTNode& ast_node,
                                           const ASTNodeDataType& data_type,
                                           const ASTNodeDataType& target_data_type) const;

 public:
  ASTNodeNaturalConversionChecker(const ASTNode& data_node, const ASTNodeDataType& target_data_type);

  ASTNodeNaturalConversionChecker(const ASTNodeSubDataType& data_node_sub_data_type,
                                  const ASTNodeDataType& target_data_type);
};

#endif   //  AST_NODE_NATURAL_CONVERSION_CHECKER_HPP
