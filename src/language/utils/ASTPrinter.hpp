#ifndef AST_PRINTER_HPP
#define AST_PRINTER_HPP

#include <language/ast/ASTNode.hpp>

class ASTPrinter
{
 public:
  enum class Format
  {
    raw,
    pretty
  };

  using InfoBaseType = uint32_t;

  enum class Info : InfoBaseType
  {
    none      = 0,
    data_type = 1 << 0,
    exec_type = 1 << 1,
    all       = std::numeric_limits<InfoBaseType>::max()
  };

 private:
  const ASTNode& m_node;

  InfoBaseType m_info;

  mutable std::string prefix;
  mutable std::vector<int> last_prefix_size;

  std::string T_junction;
  std::string L_junction;

  std::string pipe_space;
  std::string space_space;

  void _print(std::ostream& os, const ASTNode& node) const;

  template <typename NodeVector>
  void _print(std::ostream& os, const NodeVector& node_list) const;

 public:
  friend std::ostream& operator<<(std::ostream& os, const ASTPrinter& ast_printer);

  ASTPrinter(const ASTNode& node,
             Format format                                = Format::pretty,
             std::initializer_list<Info> initializer_list = {Info::all});

  ASTPrinter(const ASTPrinter&) = delete;

  ASTPrinter(ASTPrinter&&) = delete;

  ~ASTPrinter() = default;
};

#endif   // AST_PRINTER_HPP
