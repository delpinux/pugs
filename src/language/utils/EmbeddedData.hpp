#ifndef EMBEDDED_DATA_HPP
#define EMBEDDED_DATA_HPP

#include <utils/PugsAssert.hpp>

#include <iostream>
#include <memory>

class IDataHandler;

class EmbeddedData
{
 private:
  std::shared_ptr<IDataHandler> m_data_handler{nullptr};

 public:
  [[nodiscard]] const IDataHandler&
  get() const
  {
    Assert(m_data_handler);
    return *m_data_handler;
  }

  friend std::ostream& operator<<(std::ostream& os, const EmbeddedData&);

  EmbeddedData& operator=(const EmbeddedData&) = default;
  EmbeddedData& operator=(EmbeddedData&&) = default;

  EmbeddedData(const EmbeddedData&) = default;
  EmbeddedData(EmbeddedData&&)      = default;

  EmbeddedData(const std::shared_ptr<IDataHandler>& data_handler) : m_data_handler{data_handler} {}
  EmbeddedData(std::shared_ptr<IDataHandler>&& data_handler) : m_data_handler{std::move(data_handler)} {}

  EmbeddedData();
  ~EmbeddedData();
};

#endif   // EMBEDDED_DATA_HPP
