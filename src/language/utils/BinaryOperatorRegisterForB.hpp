#ifndef BINARY_OPERATOR_REGISTER_FOR_B_HPP
#define BINARY_OPERATOR_REGISTER_FOR_B_HPP

class BinaryOperatorRegisterForB
{
 private:
  void _register_ostream();
  void _register_comparisons();
  void _register_logical_operators();
  void _register_plus();
  void _register_minus();
  void _register_multiply();

 public:
  BinaryOperatorRegisterForB();
};

#endif   // BINARY_OPERATOR_REGISTER_FOR_B_HPP
