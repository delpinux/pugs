#ifndef UNARY_OPERATOR_REGISTER_FOR_RNXN_HPP
#define UNARY_OPERATOR_REGISTER_FOR_RNXN_HPP

#include <cstdlib>

template <size_t Dimension>
class UnaryOperatorRegisterForRnxn
{
 private:
  void _register_unary_minus();

 public:
  UnaryOperatorRegisterForRnxn();
};

#endif   // UNARY_OPERATOR_REGISTER_FOR_RNXN_HPP
