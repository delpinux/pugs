#ifndef AST_DOT_PRINTER_HPP
#define AST_DOT_PRINTER_HPP

#include <language/ast/ASTNode.hpp>

#include <map>

class ASTDotPrinter
{
 private:
  const ASTNode& m_node;

  std::map<const ASTNode*, uint32_t> m_node_number_map;

  void _print(std::ostream& os, const ASTNode& node) const;

  void _buildNodeNumberMap(const ASTNode& node);

 public:
  friend std::ostream& operator<<(std::ostream& os, const ASTDotPrinter& ast_printer);

  ASTDotPrinter(const ASTNode& node);

  ASTDotPrinter(const ASTDotPrinter&) = delete;

  ASTDotPrinter(ASTDotPrinter&&) = delete;

  ~ASTDotPrinter() = default;
};

#endif   // AST_DOT_PRINTER_HPP
