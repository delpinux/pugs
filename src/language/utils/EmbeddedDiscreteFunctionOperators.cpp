#include <language/utils/EmbeddedDiscreteFunctionOperators.hpp>

#include <language/node_processor/BinaryExpressionProcessor.hpp>
#include <language/node_processor/UnaryExpressionProcessor.hpp>
#include <language/utils/EmbeddedDiscreteFunctionUtils.hpp>
#include <scheme/DiscreteFunctionP0.hpp>
#include <scheme/DiscreteFunctionP0Vector.hpp>
#include <scheme/DiscreteFunctionUtils.hpp>
#include <utils/Exceptions.hpp>

// unary operators

template <typename UnaryOperatorT, typename DiscreteFunctionT>
std::shared_ptr<const DiscreteFunctionVariant>
applyUnaryOperation(const DiscreteFunctionT& f)
{
  return std::make_shared<DiscreteFunctionVariant>(UnaryOp<UnaryOperatorT>{}.eval(f));
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const std::shared_ptr<const DiscreteFunctionVariant>& f_v)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyUnaryOperation<language::unary_minus>(f);
    },
    f_v->discreteFunction());
}

// binary operators

template <typename DiscreteFunctionT, typename BinOperatorT>
std::shared_ptr<const DiscreteFunctionVariant>
innerCompositionLaw(const DiscreteFunctionT& f, const DiscreteFunctionT& g)
{
  using data_type = std::decay_t<typename DiscreteFunctionT::data_type>;
  if constexpr ((std::is_same_v<language::multiply_op, BinOperatorT> and is_tiny_vector_v<data_type>) or
                (std::is_same_v<language::divide_op, BinOperatorT> and not std::is_arithmetic_v<data_type>)) {
    throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f, g));
  } else {
    if constexpr (is_discrete_function_P0_vector_v<DiscreteFunctionT>) {
      if (f.size() != g.size()) {
        std::ostringstream error_msg;
        error_msg << EmbeddedDiscreteFunctionUtils::getOperandTypeName(f) << " spaces have different sizes";
        throw NormalError(error_msg.str());
      } else {
        return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(f, g));
      }
    } else {
      return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(f, g));
    }
  }
}

template <typename DiscreteFunctionT1, typename DiscreteFunctionT2, typename BinOperatorT>
std::shared_ptr<const DiscreteFunctionVariant>
applyBinaryOperation(const DiscreteFunctionT1& f, const DiscreteFunctionT2& g)
{
  using f_data_type = std::decay_t<typename DiscreteFunctionT1::data_type>;
  using g_data_type = std::decay_t<typename DiscreteFunctionT2::data_type>;
  if constexpr (std::is_same_v<language::multiply_op, BinOperatorT>) {
    if constexpr (is_discrete_function_P0_v<DiscreteFunctionT1> and
                  is_discrete_function_P0_vector_v<DiscreteFunctionT2> and std::is_same_v<double, f_data_type>) {
      return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(f, g));
    } else if constexpr (is_discrete_function_P0_v<DiscreteFunctionT1> and
                         is_discrete_function_P0_v<DiscreteFunctionT2>) {
      if constexpr (std::is_same_v<double, f_data_type> and
                    (is_tiny_vector_v<g_data_type> or is_tiny_matrix_v<g_data_type>)) {
        return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(f, g));
      } else if constexpr (is_tiny_matrix_v<f_data_type> and is_tiny_vector_v<g_data_type>) {
        if constexpr (f_data_type::NumberOfColumns == g_data_type::Dimension) {
          return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(f, g));
        } else {
          throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f, g));
        }
      } else {
        throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f, g));
      }
    } else {
      throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f, g));
    }
  } else {
    throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f, g));
  }
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const std::shared_ptr<const DiscreteFunctionVariant>& f_v,
          const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  if (not hasSameMesh({f_v, g_v})) {
    throw NormalError("operands are defined on different meshes");
  }

  return std::visit(
    [&](auto&& f, auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {
      using TypeOfF = std::decay_t<decltype(f)>;
      using TypeOfG = std::decay_t<decltype(g)>;
      if constexpr (std::is_same_v<TypeOfF, TypeOfG>) {
        return innerCompositionLaw<TypeOfF, language::plus_op>(f, g);
      } else {
        throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f_v, g_v));
      }
    },
    f_v->discreteFunction(), g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const std::shared_ptr<const DiscreteFunctionVariant>& f_v,
          const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  if (not hasSameMesh({f_v, g_v})) {
    throw NormalError("operands are defined on different meshes");
  }

  return std::visit(
    [&](auto&& f, auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {
      using TypeOfF = std::decay_t<decltype(f)>;
      using TypeOfG = std::decay_t<decltype(g)>;
      if constexpr (std::is_same_v<TypeOfF, TypeOfG>) {
        return innerCompositionLaw<TypeOfF, language::minus_op>(f, g);
      } else {
        throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f_v, g_v));
      }
    },
    f_v->discreteFunction(), g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const std::shared_ptr<const DiscreteFunctionVariant>& f_v,
          const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  if (not hasSameMesh({f_v, g_v})) {
    throw NormalError("operands are defined on different meshes");
  }

  return std::visit(
    [&](auto&& f, auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {
      using TypeOfF = std::decay_t<decltype(f)>;
      using TypeOfG = std::decay_t<decltype(g)>;
      if constexpr (std::is_same_v<TypeOfF, TypeOfG> and not is_discrete_function_P0_vector_v<TypeOfF>) {
        return innerCompositionLaw<TypeOfF, language::multiply_op>(f, g);
      } else {
        return applyBinaryOperation<TypeOfF, TypeOfG, language::multiply_op>(f, g);
      }
    },
    f_v->discreteFunction(), g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator/(const std::shared_ptr<const DiscreteFunctionVariant>& f_v,
          const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  if (not hasSameMesh({f_v, g_v})) {
    throw NormalError("operands are defined on different meshes");
  }

  return std::visit(
    [&](auto&& f, auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {
      using TypeOfF = std::decay_t<decltype(f)>;
      using TypeOfG = std::decay_t<decltype(g)>;
      if constexpr (std::is_same_v<TypeOfF, TypeOfG> and not is_discrete_function_P0_vector_v<TypeOfF>) {
        return innerCompositionLaw<TypeOfF, language::divide_op>(f, g);
      } else {
        throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f_v, g_v));
      }
    },
    f_v->discreteFunction(), g_v->discreteFunction());
}

template <typename BinOperatorT, typename DataType, typename DiscreteFunctionT>
std::shared_ptr<const DiscreteFunctionVariant>
applyBinaryOperationWithLeftConstant(const DataType& a, const DiscreteFunctionT& f)
{
  if constexpr (is_discrete_function_P0_v<DiscreteFunctionT>) {
    using lhs_data_type = std::decay_t<DataType>;
    using rhs_data_type = std::decay_t<typename DiscreteFunctionT::data_type>;

    if constexpr (std::is_same_v<language::multiply_op, BinOperatorT>) {
      if constexpr (std::is_same_v<lhs_data_type, double>) {
        return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(a, f));
      } else if constexpr (is_tiny_matrix_v<lhs_data_type> and std::is_same_v<lhs_data_type, rhs_data_type>) {
        return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(a, f));
      } else if constexpr (is_tiny_matrix_v<lhs_data_type> and is_tiny_vector_v<rhs_data_type>) {
        if constexpr (lhs_data_type::NumberOfColumns == rhs_data_type::Dimension) {
          return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(a, f));
        } else {
          throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(a, f));
        }
      } else {
        throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(a, f));
      }
    } else if constexpr (std::is_same_v<language::divide_op, BinOperatorT>) {
      if constexpr (std::is_same_v<lhs_data_type, double> and std::is_same_v<rhs_data_type, double>) {
        return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(a, f));
      } else {
        throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(a, f));
      }
    } else if constexpr (std::is_same_v<language::plus_op, BinOperatorT> or
                         std::is_same_v<language::minus_op, BinOperatorT>) {
      if constexpr ((std::is_same_v<rhs_data_type, lhs_data_type>) or
                    (std::is_arithmetic_v<rhs_data_type> and std::is_arithmetic_v<lhs_data_type>)) {
        return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(a, f));
      } else {
        throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(a, f));
      }
    } else {
      throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(a, f));
    }
  } else if constexpr (is_discrete_function_P0_vector_v<DiscreteFunctionT> and
                       std::is_same_v<language::multiply_op, BinOperatorT>) {
    using lhs_data_type = std::decay_t<DataType>;
    if constexpr (std::is_same_v<lhs_data_type, double>) {
      return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(a, f));
    } else {
      throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(a, f));
    }
  } else {
    throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(a, f));
  }
}

template <typename BinOperatorT, typename DataType, typename DiscreteFunctionT>
std::shared_ptr<const DiscreteFunctionVariant>
applyBinaryOperationWithRightConstant(const DiscreteFunctionT& f, const DataType& a)
{
  if constexpr (is_discrete_function_P0_v<DiscreteFunctionT>) {
    using lhs_data_type = std::decay_t<typename DiscreteFunctionT::data_type>;
    using rhs_data_type = std::decay_t<DataType>;

    if constexpr (std::is_same_v<language::multiply_op, BinOperatorT>) {
      if constexpr (is_tiny_matrix_v<lhs_data_type> and is_tiny_matrix_v<rhs_data_type>) {
        if constexpr (lhs_data_type::NumberOfColumns == rhs_data_type::NumberOfRows) {
          return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(f, a));
        } else {
          throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f, a));
        }
      } else if constexpr (is_tiny_matrix_v<lhs_data_type> and is_tiny_vector_v<rhs_data_type>) {
        if constexpr (lhs_data_type::NumberOfColumns == rhs_data_type::Dimension) {
          return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(f, a));
        } else {
          throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f, a));
        }
      } else if constexpr (std::is_same_v<lhs_data_type, double> and
                           (is_tiny_matrix_v<rhs_data_type> or is_tiny_vector_v<rhs_data_type> or
                            std::is_arithmetic_v<rhs_data_type>)) {
        return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(f, a));
      } else {
        throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f, a));
      }
    } else if constexpr (std::is_same_v<language::plus_op, BinOperatorT> or
                         std::is_same_v<language::minus_op, BinOperatorT>) {
      if constexpr ((std::is_same_v<lhs_data_type, rhs_data_type>) or
                    (std::is_arithmetic_v<lhs_data_type> and std::is_arithmetic_v<rhs_data_type>)) {
        return std::make_shared<DiscreteFunctionVariant>(BinOp<BinOperatorT>{}.eval(f, a));
      } else {
        throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f, a));
      }
    } else {
      throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f, a));
    }
  } else {
    throw NormalError(EmbeddedDiscreteFunctionUtils::incompatibleOperandTypes(f, a));
  }
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const double& f, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::plus_op>(f, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const TinyVector<1>& v, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::plus_op>(v, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const TinyVector<2>& v, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::plus_op>(v, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const TinyVector<3>& v, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::plus_op>(v, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const TinyMatrix<1>& A, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::plus_op>(A, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const TinyMatrix<2>& A, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::plus_op>(A, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const TinyMatrix<3>& A, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::plus_op>(A, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const double& g)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::plus_op>(f, g);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyVector<1>& v)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::plus_op>(f, v);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyVector<2>& v)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::plus_op>(f, v);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyVector<3>& v)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::plus_op>(f, v);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyMatrix<1>& A)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::plus_op>(f, A);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyMatrix<2>& A)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::plus_op>(f, A);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator+(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyMatrix<3>& A)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::plus_op>(f, A);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const double& f, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::minus_op>(f, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const TinyVector<1>& v, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::minus_op>(v, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const TinyVector<2>& v, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::minus_op>(v, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const TinyVector<3>& v, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::minus_op>(v, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const TinyMatrix<1>& A, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::minus_op>(A, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const TinyMatrix<2>& A, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::minus_op>(A, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const TinyMatrix<3>& A, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::minus_op>(A, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const double& g)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::minus_op>(f, g);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyVector<1>& v)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::minus_op>(f, v);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyVector<2>& v)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::minus_op>(f, v);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyVector<3>& v)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::minus_op>(f, v);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyMatrix<1>& A)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::minus_op>(f, A);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyMatrix<2>& A)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::minus_op>(f, A);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator-(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyMatrix<3>& A)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::minus_op>(f, A);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const double& f, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::multiply_op>(f, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const TinyMatrix<1>& A, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::multiply_op>(A, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const TinyMatrix<2>& A, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::multiply_op>(A, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const TinyMatrix<3>& A, const std::shared_ptr<const DiscreteFunctionVariant>& g_v)
{
  return std::visit(
    [&](auto&& g) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::multiply_op>(A, g);
    },
    g_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const double& g)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::multiply_op>(f, g);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyVector<1>& v)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::multiply_op>(f, v);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyVector<2>& v)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::multiply_op>(f, v);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyVector<3>& v)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::multiply_op>(f, v);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyMatrix<1>& A)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::multiply_op>(f, A);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyMatrix<2>& A)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::multiply_op>(f, A);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator*(const std::shared_ptr<const DiscreteFunctionVariant>& f_v, const TinyMatrix<3>& A)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithRightConstant<language::multiply_op>(f, A);
    },
    f_v->discreteFunction());
}

std::shared_ptr<const DiscreteFunctionVariant>
operator/(const double& a, const std::shared_ptr<const DiscreteFunctionVariant>& f_v)
{
  return std::visit(
    [&](auto&& f) -> std::shared_ptr<const DiscreteFunctionVariant> {   //
      return applyBinaryOperationWithLeftConstant<language::divide_op>(a, f);
    },
    f_v->discreteFunction());
}
