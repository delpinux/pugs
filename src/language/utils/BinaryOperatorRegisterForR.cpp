#include <language/utils/BinaryOperatorRegisterForR.hpp>

#include <language/utils/BasicBinaryOperatorRegisterComparisonOf.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/OStream.hpp>

void
BinaryOperatorRegisterForR::_register_ostream()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, double>>());

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, std::vector<double>>>());
}

void
BinaryOperatorRegisterForR::_register_comparisons()
{
  BasicBinaryOperatorRegisterComparisonOf<bool, double>{};
  BasicBinaryOperatorRegisterComparisonOf<double, bool>{};

  BasicBinaryOperatorRegisterComparisonOf<uint64_t, double>{};
  BasicBinaryOperatorRegisterComparisonOf<double, uint64_t>{};

  BasicBinaryOperatorRegisterComparisonOf<int64_t, double>{};
  BasicBinaryOperatorRegisterComparisonOf<double, int64_t>{};

  BasicBinaryOperatorRegisterComparisonOf<double, double>{};
}

template <typename OperatorT>
void
BinaryOperatorRegisterForR::_register_arithmetic()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, double, double, bool>>());
  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, double, bool, double>>());

  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, double, double, uint64_t>>());
  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, double, uint64_t, double_t>>());

  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, double, double, int64_t>>());
  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, double, int64_t, double_t>>());

  repository.addBinaryOperator<OperatorT>(
    std::make_shared<BinaryOperatorProcessorBuilder<OperatorT, double, double, double>>());
}

void
BinaryOperatorRegisterForR::_register_divide()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, double, bool, double>>());

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, double, double, uint64_t>>());
  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, double, uint64_t, double_t>>());

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, double, double, int64_t>>());
  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, double, int64_t, double_t>>());

  repository.addBinaryOperator<language::divide_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::divide_op, double, double, double>>());
}

BinaryOperatorRegisterForR::BinaryOperatorRegisterForR()
{
  this->_register_ostream();
  this->_register_comparisons();
  this->_register_arithmetic<language::plus_op>();
  this->_register_arithmetic<language::minus_op>();
  this->_register_arithmetic<language::multiply_op>();
  this->_register_divide();
}
