#ifndef BINARY_OPERATOR_MANGLER_HPP
#define BINARY_OPERATOR_MANGLER_HPP

#include <language/utils/ASTNodeDataType.hpp>
#include <utils/Exceptions.hpp>

#include <string>

namespace language
{
struct multiply_op;
struct divide_op;
struct plus_op;
struct minus_op;

struct shift_left_op;
struct shift_right_op;

struct or_op;
struct and_op;
struct xor_op;

struct greater_op;
struct greater_or_eq_op;
struct lesser_op;
struct lesser_or_eq_op;
struct eqeq_op;
struct not_eq_op;
}   // namespace language

template <typename BinaryOperatorT>
std::string
binaryOperatorMangler(const ASTNodeDataType& lhs_data_type, const ASTNodeDataType& rhs_data_type)
{
  const std::string operator_name = [] {
    if constexpr (std::is_same_v<BinaryOperatorT, language::multiply_op>) {
      return "*";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::divide_op>) {
      return "/";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::plus_op>) {
      return "+";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::minus_op>) {
      return "-";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::or_op>) {
      return "or";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::and_op>) {
      return "and";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::xor_op>) {
      return "xor";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::greater_op>) {
      return ">";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::greater_or_eq_op>) {
      return ">=";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::lesser_op>) {
      return "<";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::lesser_or_eq_op>) {
      return "<=";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::eqeq_op>) {
      return "==";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::not_eq_op>) {
      return "!=";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::shift_left_op>) {
      return "<<";
    } else if constexpr (std::is_same_v<BinaryOperatorT, language::shift_right_op>) {
      return ">>";
    } else {
      static_assert(std::is_same_v<language::multiply_op, BinaryOperatorT>, "undefined binary operator");
    }
  }();

  return dataTypeName(lhs_data_type) + " " + operator_name + " " + dataTypeName(rhs_data_type);
}

#endif   // BINARY_OPERATOR_MANGLER_HPP
