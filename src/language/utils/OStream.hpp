#ifndef OSTREAM_HPP
#define OSTREAM_HPP

#include <language/utils/ASTNodeDataTypeTraits.hpp>
#include <utils/PugsAssert.hpp>

#include <memory>

class OStream
{
 public:
  enum class Type
  {
    std_ostream,
    std_ofstream
  };

 protected:
  mutable std::ostream* m_ostream = nullptr;
  Type m_type;

 public:
  const Type&
  type() const
  {
    return m_type;
  }

  template <typename DataT>
  friend std::shared_ptr<const OStream>
  operator<<(const std::shared_ptr<const OStream>& os, const DataT& t)
  {
    Assert(os.use_count() > 0, "non allocated stream");
    if (os->m_ostream != nullptr) {
      *os->m_ostream << t;
    }
    return os;
  }

  template <typename DataT>
  friend std::shared_ptr<const OStream>
  operator<<(const std::shared_ptr<const OStream>& os, const std::shared_ptr<const DataT>& t)
  {
    Assert(os.use_count() > 0, "non allocated stream");
    if (os->m_ostream != nullptr) {
      *os->m_ostream << *t;
    }
    return os;
  }

  OStream(std::ostream& os, Type type = Type::std_ostream) : m_ostream(&os), m_type{type} {}
  OStream(Type type) : m_type{type} {}

  virtual ~OStream() = default;
};

template <>
inline ASTNodeDataType ast_node_data_type_from<std::shared_ptr<const OStream>> =
  ASTNodeDataType::build<ASTNodeDataType::type_id_t>("ostream");

#endif   // OSTREAM_HPP
