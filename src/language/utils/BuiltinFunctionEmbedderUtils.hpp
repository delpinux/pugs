#ifndef BUILTIN_FUNCTION_EMBEDDER_UTILS_HPP
#define BUILTIN_FUNCTION_EMBEDDER_UTILS_HPP

class IBuiltinFunctionEmbedder;
class ASTNode;

#include <memory>

std::shared_ptr<IBuiltinFunctionEmbedder> getBuiltinFunctionEmbedder(ASTNode& n);

#endif   // BUILTIN_FUNCTION_EMBEDDER_UTILS_HPP
