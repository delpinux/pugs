#include <language/utils/FunctionSymbolId.hpp>
#include <language/utils/SymbolTable.hpp>

const FunctionDescriptor&
FunctionSymbolId::descriptor() const
{
  return m_symbol_table->functionTable()[m_function_id];
}
