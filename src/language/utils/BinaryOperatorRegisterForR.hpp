#ifndef BINARY_OPERATOR_REGISTER_FOR_R_HPP
#define BINARY_OPERATOR_REGISTER_FOR_R_HPP

class BinaryOperatorRegisterForR
{
 private:
  void _register_ostream();
  template <typename OperatorT>
  void _register_arithmetic();
  void _register_divide();
  void _register_comparisons();

 public:
  BinaryOperatorRegisterForR();
};

#endif   // BINARY_OPERATOR_REGISTER_FOR_R_HPP
