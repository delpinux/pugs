#include <language/utils/IncDecOperatorRegisterForZ.hpp>

#include <language/utils/IncDecOperatorProcessorBuilder.hpp>
#include <language/utils/OperatorRepository.hpp>

void
IncDecOperatorRegisterForZ::_register_unary_minusminus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addIncDecOperator<language::unary_minusminus>(Z, std::make_shared<IncDecOperatorProcessorBuilder<
                                                                language::unary_minusminus, int64_t>>());
}

void
IncDecOperatorRegisterForZ::_register_unary_plusplus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addIncDecOperator<
    language::unary_plusplus>(Z, std::make_shared<IncDecOperatorProcessorBuilder<language::unary_plusplus, int64_t>>());
}

void
IncDecOperatorRegisterForZ::_register_post_minusminus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto N = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addIncDecOperator<
    language::post_minusminus>(N,
                               std::make_shared<IncDecOperatorProcessorBuilder<language::post_minusminus, int64_t>>());
}

void
IncDecOperatorRegisterForZ::_register_post_plusplus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();

  repository.addIncDecOperator<
    language::post_plusplus>(Z, std::make_shared<IncDecOperatorProcessorBuilder<language::post_plusplus, int64_t>>());
}

IncDecOperatorRegisterForZ::IncDecOperatorRegisterForZ()
{
  this->_register_unary_minusminus();
  this->_register_unary_plusplus();
  this->_register_post_minusminus();
  this->_register_post_plusplus();
}
