#ifndef TYPE_DESCRIPTOR_HPP
#define TYPE_DESCRIPTOR_HPP

#include <string>

class TypeDescriptor
{
 private:
  std::string m_name;

 public:
  const std::string&
  name() const
  {
    return m_name;
  }

  TypeDescriptor(std::string_view name) : m_name(name) {}
  ~TypeDescriptor() = default;
};

#endif   // TYPE_DESCRIPTOR_HPP
