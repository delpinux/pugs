#include <language/utils/BinaryOperatorRegisterForB.hpp>

#include <language/utils/BasicBinaryOperatorRegisterComparisonOf.hpp>
#include <language/utils/DataHandler.hpp>
#include <language/utils/OStream.hpp>

void
BinaryOperatorRegisterForB::_register_ostream()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, bool>>());

  repository.addBinaryOperator<language::shift_left_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::shift_left_op, std::shared_ptr<const OStream>,
                                                    std::shared_ptr<const OStream>, std::vector<bool>>>());
}

void
BinaryOperatorRegisterForB::_register_comparisons()
{
  BasicBinaryOperatorRegisterComparisonOf<bool, bool>{};
}

void
BinaryOperatorRegisterForB::_register_logical_operators()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::and_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::and_op, bool, bool, bool>>());

  repository.addBinaryOperator<language::or_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::or_op, bool, bool, bool>>());

  repository.addBinaryOperator<language::xor_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::xor_op, bool, bool, bool>>());
}

void
BinaryOperatorRegisterForB::_register_plus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::plus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::plus_op, uint64_t, bool, bool>>());
}

void
BinaryOperatorRegisterForB::_register_minus()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::minus_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::minus_op, int64_t, bool, bool>>());
}

void
BinaryOperatorRegisterForB::_register_multiply()
{
  OperatorRepository& repository = OperatorRepository::instance();

  repository.addBinaryOperator<language::multiply_op>(
    std::make_shared<BinaryOperatorProcessorBuilder<language::multiply_op, uint64_t, bool, bool>>());
}

BinaryOperatorRegisterForB::BinaryOperatorRegisterForB()
{
  this->_register_ostream();
  this->_register_comparisons();
  this->_register_logical_operators();
  this->_register_plus();
  this->_register_minus();
  this->_register_multiply();
}
