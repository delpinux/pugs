#include <language/utils/AffectationRegisterForR.hpp>

#include <language/utils/AffectationProcessorBuilder.hpp>
#include <language/utils/BasicAffectationRegistrerFor.hpp>
#include <language/utils/OperatorRepository.hpp>

void
AffectationRegisterForR::_register_eq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto B = ASTNodeDataType::build<ASTNodeDataType::bool_t>();
  auto N = ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>();
  auto Z = ASTNodeDataType::build<ASTNodeDataType::int_t>();
  auto R = ASTNodeDataType::build<ASTNodeDataType::double_t>();

  repository
    .addAffectation<language::eq_op>(R, B,
                                     std::make_shared<AffectationProcessorBuilder<language::eq_op, double, bool>>());

  repository.addAffectation<
    language::eq_op>(R, N, std::make_shared<AffectationProcessorBuilder<language::eq_op, double, uint64_t>>());

  repository
    .addAffectation<language::eq_op>(R, Z,
                                     std::make_shared<AffectationProcessorBuilder<language::eq_op, double, int64_t>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R), B,
                                             std::make_shared<AffectationToTupleProcessorBuilder<double>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R), N,
                                             std::make_shared<AffectationToTupleProcessorBuilder<double>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R), Z,
                                             std::make_shared<AffectationToTupleProcessorBuilder<double>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R),
                                             ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                             std::make_shared<AffectationToTupleFromTupleProcessorBuilder<double>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R),
                                             ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                             std::make_shared<AffectationToTupleFromTupleProcessorBuilder<double>>());

  repository.addAffectation<language::eq_op>(ASTNodeDataType::build<ASTNodeDataType::tuple_t>(R),
                                             ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                             std::make_shared<AffectationToTupleFromTupleProcessorBuilder<double>>());

  repository.addAffectation<language::eq_op>(R, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(B),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<double>>());

  repository.addAffectation<language::eq_op>(R, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(N),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<double>>());

  repository.addAffectation<language::eq_op>(R, ASTNodeDataType::build<ASTNodeDataType::tuple_t>(Z),
                                             std::make_shared<AffectationFromTupleProcessorBuilder<double>>());
}

void
AffectationRegisterForR::_register_pluseq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto R = ASTNodeDataType::build<ASTNodeDataType::double_t>();

  repository.addAffectation<
    language::pluseq_op>(R, ASTNodeDataType::build<ASTNodeDataType::bool_t>(),
                         std::make_shared<AffectationProcessorBuilder<language::pluseq_op, double, bool>>());

  repository.addAffectation<
    language::pluseq_op>(R, ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>(),
                         std::make_shared<AffectationProcessorBuilder<language::pluseq_op, double, uint64_t>>());

  repository.addAffectation<
    language::pluseq_op>(R, ASTNodeDataType::build<ASTNodeDataType::int_t>(),
                         std::make_shared<AffectationProcessorBuilder<language::pluseq_op, double, int64_t>>());

  repository.addAffectation<
    language::pluseq_op>(R, R, std::make_shared<AffectationProcessorBuilder<language::pluseq_op, double, double>>());
}

void
AffectationRegisterForR::_register_minuseq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto R = ASTNodeDataType::build<ASTNodeDataType::double_t>();

  repository.addAffectation<
    language::minuseq_op>(R, ASTNodeDataType::build<ASTNodeDataType::bool_t>(),
                          std::make_shared<AffectationProcessorBuilder<language::minuseq_op, double, bool>>());

  repository.addAffectation<
    language::minuseq_op>(R, ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>(),
                          std::make_shared<AffectationProcessorBuilder<language::minuseq_op, double, uint64_t>>());

  repository.addAffectation<
    language::minuseq_op>(R, ASTNodeDataType::build<ASTNodeDataType::int_t>(),
                          std::make_shared<AffectationProcessorBuilder<language::minuseq_op, double, int64_t>>());

  repository.addAffectation<
    language::minuseq_op>(R, R, std::make_shared<AffectationProcessorBuilder<language::minuseq_op, double, double>>());
}

void
AffectationRegisterForR::_register_multiplyeq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto R = ASTNodeDataType::build<ASTNodeDataType::double_t>();

  repository.addAffectation<
    language::multiplyeq_op>(R, ASTNodeDataType::build<ASTNodeDataType::bool_t>(),
                             std::make_shared<AffectationProcessorBuilder<language::multiplyeq_op, double, bool>>());

  repository.addAffectation<language::multiplyeq_op>(R, ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>(),
                                                     std::make_shared<AffectationProcessorBuilder<
                                                       language::multiplyeq_op, double, uint64_t>>());

  repository.addAffectation<
    language::multiplyeq_op>(R, ASTNodeDataType::build<ASTNodeDataType::int_t>(),
                             std::make_shared<AffectationProcessorBuilder<language::multiplyeq_op, double, int64_t>>());

  repository.addAffectation<
    language::multiplyeq_op>(R, R,
                             std::make_shared<AffectationProcessorBuilder<language::multiplyeq_op, double, double>>());
}

void
AffectationRegisterForR::_register_divideeq_op()
{
  OperatorRepository& repository = OperatorRepository::instance();

  auto R = ASTNodeDataType::build<ASTNodeDataType::double_t>();

  repository.addAffectation<
    language::divideeq_op>(R, ASTNodeDataType::build<ASTNodeDataType::unsigned_int_t>(),
                           std::make_shared<AffectationProcessorBuilder<language::divideeq_op, double, uint64_t>>());

  repository.addAffectation<
    language::divideeq_op>(R, ASTNodeDataType::build<ASTNodeDataType::int_t>(),
                           std::make_shared<AffectationProcessorBuilder<language::divideeq_op, double, int64_t>>());

  repository.addAffectation<
    language::divideeq_op>(R, R,
                           std::make_shared<AffectationProcessorBuilder<language::divideeq_op, double, double>>());
}

AffectationRegisterForR::AffectationRegisterForR()
{
  BasicAffectationRegisterFor<double>{};
  this->_register_eq_op();
  this->_register_pluseq_op();
  this->_register_minuseq_op();
  this->_register_multiplyeq_op();
  this->_register_divideeq_op();
}
