#ifndef EVALUATE_AT_POINTS_HPP
#define EVALUATE_AT_POINTS_HPP

#include <language/utils/PugsFunctionAdapter.hpp>
#include <utils/Array.hpp>

class FunctionSymbolId;

template <typename T>
class EvaluateAtPoints;
template <typename OutputType, typename InputType>
class EvaluateAtPoints<OutputType(InputType)> : public PugsFunctionAdapter<OutputType(InputType)>
{
  using Adapter = PugsFunctionAdapter<OutputType(InputType)>;

 public:
  template <typename InputArrayT, typename OutputArrayT>
  static PUGS_INLINE void
  evaluateTo(const FunctionSymbolId& function_symbol_id, const InputArrayT& position, OutputArrayT& value)
  {
    static_assert(std::is_same_v<std::remove_const_t<typename InputArrayT::data_type>, InputType>,
                  "invalid input data type");
    static_assert(std::is_same_v<std::remove_const_t<typename OutputArrayT::data_type>, OutputType>,
                  "invalid output data type");
    Assert(size(value) == size(position));

    auto& expression    = Adapter::getFunctionExpression(function_symbol_id);
    auto convert_result = Adapter::getResultConverter(expression.m_data_type);

    auto context_list = Adapter::getContextList(expression);

    using execution_space = typename Kokkos::DefaultExecutionSpace::execution_space;
    Kokkos::Experimental::UniqueToken<execution_space, Kokkos::Experimental::UniqueTokenScope::Global> tokens;

    if constexpr (std::is_arithmetic_v<OutputType>) {
      value.fill(0);
    } else if constexpr (is_tiny_vector_v<OutputType> or is_tiny_matrix_v<OutputType>) {
      value.fill(zero);
    } else {
      static_assert(std::is_same_v<OutputType, double>, "unexpected output type");
    }

    parallel_for(size(position), [=, &expression, &tokens](typename InputArrayT::index_type i) {
      const int32_t t = tokens.acquire();

      auto& execution_policy = context_list[t];

      Adapter::convertArgs(execution_policy.currentContext(), position[i]);
      auto result = expression.execute(execution_policy);
      value[i]    = convert_result(std::move(result));

      tokens.release(t);
    });
  }

  template <class InputArrayT>
  static PUGS_INLINE Array<OutputType>
  evaluate(const FunctionSymbolId& function_symbol_id, const InputArrayT& position)
  {
    static_assert(std::is_same_v<std::remove_const_t<typename InputArrayT::data_type>, InputType>,
                  "invalid input data type");
    Array<OutputType> value(size(position));
    evaluateTo(function_symbol_id, position, value);
    return value;
  }
};

#endif   // EVALUATE_AT_POINTS_HPP
