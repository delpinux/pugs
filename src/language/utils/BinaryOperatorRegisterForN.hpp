#ifndef BINARY_OPERATOR_REGISTER_FOR_N_HPP
#define BINARY_OPERATOR_REGISTER_FOR_N_HPP

class BinaryOperatorRegisterForN
{
 private:
  void _register_ostream();
  template <typename OperatorT>
  void _register_arithmetic();
  void _register_divide();
  void _register_comparisons();
  void _register_minus();

 public:
  BinaryOperatorRegisterForN();
};

#endif   // BINARY_OPERATOR_REGISTER_FOR_N_HPP
