#ifndef PYRAMID_TRANSFORMATION_HPP
#define PYRAMID_TRANSFORMATION_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>

class PyramidTransformation
{
 private:
  constexpr static size_t Dimension      = 3;
  constexpr static size_t NumberOfPoints = 5;

  TinyMatrix<Dimension, NumberOfPoints - 1> m_coefficients;
  TinyVector<Dimension> m_shift;

 public:
  PUGS_INLINE
  TinyVector<Dimension>
  operator()(const TinyVector<Dimension>& x) const
  {
    const TinyVector<NumberOfPoints - 1> X{x[0], x[1], x[2], x[0] * x[1]};
    return m_coefficients * X + m_shift;
  }

  double
  jacobianDeterminant(const TinyVector<Dimension>& X) const
  {
    const auto& T   = m_coefficients;
    const double& x = X[0];
    const double& y = X[1];

    const TinyMatrix<Dimension, Dimension> J{T(0, 0) + T(0, 3) * y,   //
                                             T(0, 1) + T(0, 3) * x,   //
                                             T(0, 2),
                                             //
                                             T(1, 0) + T(1, 3) * y,   //
                                             T(1, 1) + T(1, 3) * x,   //
                                             T(1, 2),
                                             //
                                             T(2, 0) + T(2, 3) * y,   //
                                             T(2, 1) + T(2, 3) * x,   //
                                             T(2, 2)};
    return det(J);
  }

  PUGS_INLINE
  PyramidTransformation(const TinyVector<Dimension>& a,
                        const TinyVector<Dimension>& b,
                        const TinyVector<Dimension>& c,
                        const TinyVector<Dimension>& d,
                        const TinyVector<Dimension>& e)
  {
    static_assert(Dimension == 3, "invalid dimension");

    m_shift = 0.25 * (a + b + c + d);

    for (size_t i = 0; i < Dimension; ++i) {
      m_coefficients(i, 0) = 0.5 * (b[i] + c[i]) - m_shift[i];
      m_coefficients(i, 1) = 0.5 * (c[i] + d[i]) - m_shift[i];
      m_coefficients(i, 2) = e[i] - m_shift[i];
      m_coefficients(i, 3) = 0.5 * (a[i] + c[i]) - m_shift[i];
    }
  }

  ~PyramidTransformation() = default;
};

#endif   // PYRAMID_TRANSFORMATION_HPP
