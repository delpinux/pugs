#ifndef SQUARE_TRANSFORMATION_HPP
#define SQUARE_TRANSFORMATION_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>

#include <array>

template <size_t GivenDimension>
class SquareTransformation;

template <>
class SquareTransformation<2>
{
 public:
  constexpr static size_t Dimension      = 2;
  constexpr static size_t NumberOfPoints = 4;

 private:
  TinyMatrix<Dimension, NumberOfPoints - 1> m_coefficients;
  TinyVector<Dimension> m_shift;

 public:
  PUGS_INLINE
  TinyVector<Dimension>
  operator()(const TinyVector<2>& x) const
  {
    const TinyVector<NumberOfPoints - 1> X{x[0], x[1], x[0] * x[1]};
    return m_coefficients * X + m_shift;
  }

  PUGS_INLINE double
  jacobianDeterminant(const TinyVector<Dimension>& X) const
  {
    const auto& T   = m_coefficients;
    const double& x = X[0];
    const double& y = X[1];

    const TinyMatrix<Dimension, Dimension> J{T(0, 0) + T(0, 2) * y,   //
                                             T(0, 1) + T(0, 2) * x,
                                             //
                                             T(1, 0) + T(1, 2) * y,   //
                                             T(1, 1) + T(1, 2) * x};
    return det(J);
  }

  PUGS_INLINE
  SquareTransformation(const TinyVector<Dimension>& a,
                       const TinyVector<Dimension>& b,
                       const TinyVector<Dimension>& c,
                       const TinyVector<Dimension>& d)
  {
    for (size_t i = 0; i < Dimension; ++i) {
      m_coefficients(i, 0) = 0.25 * (-a[i] + b[i] + c[i] - d[i]);
      m_coefficients(i, 1) = 0.25 * (-a[i] - b[i] + c[i] + d[i]);
      m_coefficients(i, 2) = 0.25 * (+a[i] - b[i] + c[i] - d[i]);

      m_shift[i] = 0.25 * (a[i] + b[i] + c[i] + d[i]);
    }
  }

  ~SquareTransformation() = default;
};

template <size_t GivenDimension>
class SquareTransformation
{
 public:
  constexpr static size_t Dimension = GivenDimension;
  static_assert(Dimension == 3, "Square transformation is defined in dimension 2 or 3");

  constexpr static size_t NumberOfPoints = 4;

 private:
  TinyMatrix<Dimension, NumberOfPoints - 1> m_coefficients;
  TinyVector<Dimension> m_shift;

 public:
  PUGS_INLINE
  TinyVector<Dimension>
  operator()(const TinyVector<2>& x) const
  {
    const TinyVector<NumberOfPoints - 1> X{x[0], x[1], x[0] * x[1]};
    return m_coefficients * X + m_shift;
  }

  PUGS_INLINE double
  areaVariationNorm(const TinyVector<2>& X) const
  {
    const auto& T   = m_coefficients;
    const double& x = X[0];
    const double& y = X[1];

    const TinyVector<Dimension> dxT{T(0, 0) + T(0, 2) * y,   //
                                    T(1, 0) + T(1, 2) * y,   //
                                    T(2, 0) + T(2, 2) * y};

    const TinyVector<Dimension> dyT{T(0, 1) + T(0, 2) * x,   //
                                    T(1, 1) + T(1, 2) * x,   //
                                    T(2, 1) + T(2, 2) * x};

    return l2Norm(crossProduct(dxT, dyT));
  }

  PUGS_INLINE
  SquareTransformation(const TinyVector<Dimension>& a,
                       const TinyVector<Dimension>& b,
                       const TinyVector<Dimension>& c,
                       const TinyVector<Dimension>& d)
  {
    for (size_t i = 0; i < Dimension; ++i) {
      m_coefficients(i, 0) = 0.25 * (-a[i] + b[i] + c[i] - d[i]);
      m_coefficients(i, 1) = 0.25 * (-a[i] - b[i] + c[i] + d[i]);
      m_coefficients(i, 2) = 0.25 * (+a[i] - b[i] + c[i] - d[i]);

      m_shift[i] = 0.25 * (a[i] + b[i] + c[i] + d[i]);
    }
  }

  ~SquareTransformation() = default;
};

#endif   // SQUARE_TRANSFORMATION_HPP
