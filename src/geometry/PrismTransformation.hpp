#ifndef PRISM_TRANSFORMATION_HPP
#define PRISM_TRANSFORMATION_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>

class PrismTransformation
{
 private:
  constexpr static size_t Dimension      = 3;
  constexpr static size_t NumberOfPoints = 6;

  TinyMatrix<Dimension, NumberOfPoints - 1> m_coefficients;
  TinyVector<Dimension> m_shift;

 public:
  PUGS_INLINE
  TinyVector<Dimension>
  operator()(const TinyVector<Dimension>& x) const
  {
    const TinyVector<NumberOfPoints - 1> X{x[0], x[1], x[2], x[0] * x[2], x[1] * x[2]};
    return m_coefficients * X + m_shift;
  }

  double
  jacobianDeterminant(const TinyVector<Dimension>& X) const
  {
    const auto& T   = m_coefficients;
    const double& x = X[0];
    const double& y = X[1];
    const double& z = X[2];

    const TinyMatrix<Dimension, Dimension> J{T(0, 0) + T(0, 3) * z,   //
                                             T(0, 1) + T(0, 4) * z,   //
                                             T(0, 2) + T(0, 3) * x + T(0, 4) * y,
                                             //
                                             T(1, 0) + T(1, 3) * z,   //
                                             T(1, 1) + T(1, 4) * z,   //
                                             T(1, 2) + T(1, 3) * x + T(1, 4) * y,
                                             //
                                             T(2, 0) + T(2, 3) * z,   //
                                             T(2, 1) + T(2, 4) * z,   //
                                             T(2, 2) + T(2, 3) * x + T(2, 4) * y};
    return det(J);
  }

  PUGS_INLINE
  PrismTransformation(const TinyVector<Dimension>& a,
                      const TinyVector<Dimension>& b,
                      const TinyVector<Dimension>& c,
                      const TinyVector<Dimension>& d,
                      const TinyVector<Dimension>& e,
                      const TinyVector<Dimension>& f)
  {
    static_assert(Dimension == 3, "invalid dimension");

    for (size_t i = 0; i < Dimension; ++i) {
      m_coefficients(i, 0) = 0.5 * (b[i] - a[i] + e[i] - d[i]);
      m_coefficients(i, 1) = 0.5 * (c[i] + f[i] - a[i] - d[i]);
      m_coefficients(i, 2) = 0.5 * (d[i] - a[i]);
      m_coefficients(i, 3) = 0.5 * (a[i] - b[i] + e[i] - d[i]);
      m_coefficients(i, 4) = 0.5 * (f[i] - c[i] + a[i] - d[i]);
    }

    m_shift = 0.5 * (a + d);
  }

  ~PrismTransformation() = default;
};

#endif   // PRISM_TRANSFORMATION_HPP
