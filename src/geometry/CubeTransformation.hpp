#ifndef CUBE_TRANSFORMATION_HPP
#define CUBE_TRANSFORMATION_HPP

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>

#include <array>

class CubeTransformation
{
 public:
  constexpr static size_t Dimension      = 3;
  constexpr static size_t NumberOfPoints = 8;

 private:
  TinyMatrix<Dimension, NumberOfPoints - 1> m_coefficients;
  TinyVector<Dimension> m_shift;

 public:
  PUGS_INLINE
  TinyVector<Dimension>
  operator()(const TinyVector<Dimension>& x) const
  {
    const TinyVector<NumberOfPoints - 1> X{x[0], x[1], x[2], x[0] * x[1], x[1] * x[2], x[0] * x[2], x[0] * x[1] * x[2]};
    return m_coefficients * X + m_shift;
  }

  PUGS_INLINE double
  jacobianDeterminant(const TinyVector<Dimension>& X) const
  {
    static_assert(Dimension == 3, "invalid dimension");
    const auto& T   = m_coefficients;
    const double& x = X[0];
    const double& y = X[1];
    const double& z = X[2];

    const TinyMatrix<Dimension, Dimension> J{T(0, 0) + T(0, 3) * y + T(0, 5) * z + T(0, 6) * y * z,
                                             T(0, 1) + T(0, 3) * x + T(0, 4) * z + T(0, 6) * x * z,
                                             T(0, 2) + T(0, 4) * y + T(0, 5) * x + T(0, 6) * x * y,
                                             //
                                             T(1, 0) + T(1, 3) * y + T(1, 5) * z + T(1, 6) * y * z,
                                             T(1, 1) + T(1, 3) * x + T(1, 4) * z + T(1, 6) * x * z,
                                             T(1, 2) + T(1, 4) * y + T(1, 5) * x + T(1, 6) * x * y,
                                             //
                                             T(2, 0) + T(2, 3) * y + T(2, 5) * z + T(2, 6) * y * z,
                                             T(2, 1) + T(2, 3) * x + T(2, 4) * z + T(2, 6) * x * z,
                                             T(2, 2) + T(2, 4) * y + T(2, 5) * x + T(2, 6) * x * y};

    return det(J);
  }

  PUGS_INLINE
  CubeTransformation(const TinyVector<Dimension>& a,
                     const TinyVector<Dimension>& b,
                     const TinyVector<Dimension>& c,
                     const TinyVector<Dimension>& d,
                     const TinyVector<Dimension>& e,
                     const TinyVector<Dimension>& f,
                     const TinyVector<Dimension>& g,
                     const TinyVector<Dimension>& h)
  {
    for (size_t i = 0; i < Dimension; ++i) {
      m_coefficients(i, 0) = 0.125 * (-a[i] + b[i] + c[i] - d[i] - e[i] + f[i] + g[i] - h[i]);
      m_coefficients(i, 1) = 0.125 * (-a[i] - b[i] + c[i] + d[i] - e[i] - f[i] + g[i] + h[i]);
      m_coefficients(i, 2) = 0.125 * (-a[i] - b[i] - c[i] - d[i] + e[i] + f[i] + g[i] + h[i]);
      m_coefficients(i, 3) = 0.125 * (+a[i] - b[i] + c[i] - d[i] + e[i] - f[i] + g[i] - h[i]);
      m_coefficients(i, 4) = 0.125 * (+a[i] + b[i] - c[i] - d[i] - e[i] - f[i] + g[i] + h[i]);
      m_coefficients(i, 5) = 0.125 * (+a[i] - b[i] - c[i] + d[i] - e[i] + f[i] + g[i] - h[i]);
      m_coefficients(i, 6) = 0.125 * (-a[i] + b[i] - c[i] + d[i] + e[i] - f[i] + g[i] - h[i]);

      m_shift[i] = 0.125 * (a[i] + b[i] + c[i] + d[i] + e[i] + f[i] + g[i] + h[i]);
    }
  }

  ~CubeTransformation() = default;
};

#endif   // CUBE_TRANSFORMATION_HPP
