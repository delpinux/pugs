#ifndef NAMED_DISCRETE_FUNCTION_HPP
#define NAMED_DISCRETE_FUNCTION_HPP

#include <output/INamedDiscreteData.hpp>

#include <memory>
#include <string>

class DiscreteFunctionVariant;

class NamedDiscreteFunction final : public INamedDiscreteData
{
 private:
  std::shared_ptr<const DiscreteFunctionVariant> m_discrete_function_variant;
  std::string m_name;

 public:
  Type
  type() const final
  {
    return INamedDiscreteData::Type::discrete_function;
  }

  const std::string&
  name() const final
  {
    return m_name;
  }

  const std::shared_ptr<const DiscreteFunctionVariant>
  discreteFunctionVariant() const
  {
    return m_discrete_function_variant;
  }

  NamedDiscreteFunction(const std::shared_ptr<const DiscreteFunctionVariant>& discrete_function,
                        const std::string& name)
    : m_discrete_function_variant{discrete_function}, m_name{name}
  {}

  NamedDiscreteFunction(const NamedDiscreteFunction&) = default;
  NamedDiscreteFunction(NamedDiscreteFunction&&)      = default;

  NamedDiscreteFunction()  = default;
  ~NamedDiscreteFunction() = default;
};

#endif   // NAMED_DISCRETE_FUNCTION_HPP
