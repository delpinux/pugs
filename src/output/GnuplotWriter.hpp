#ifndef GNUPLOT_WRITER_HPP
#define GNUPLOT_WRITER_HPP

#include <output/GnuplotWriterBase.hpp>

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <mesh/MeshTraits.hpp>
#include <output/OutputNamedItemValueSet.hpp>

class MeshVariant;

#include <optional>
#include <string>

class GnuplotWriter final : public GnuplotWriterBase
{
 private:
  template <typename DataType>
  void _writeCellData(const CellValue<DataType>& cell_value, CellId cell_id, std::ostream& fout) const;

  template <typename DataType>
  void _writeCellData(const CellArray<DataType>& cell_value, CellId cell_id, std::ostream& fout) const;

  template <typename DataType>
  void _writeNodeData(const NodeValue<DataType>& node_value, NodeId cell_id, std::ostream& fout) const;

  template <typename DataType>
  void _writeNodeData(const NodeArray<DataType>& node_value, NodeId cell_id, std::ostream& fout) const;

  template <typename ItemDataT>
  void _writeData(const ItemDataT& item_data,
                  [[maybe_unused]] CellId cell_id,
                  [[maybe_unused]] NodeId node_id,
                  std::ostream& fout) const;

  template <MeshConcept MeshType>
  void _writeDataAtNodes(const MeshType& mesh,
                         const OutputNamedItemDataSet& output_named_item_data_set,
                         std::ostream& fout) const;

  template <MeshConcept MeshType>
  void _write(const MeshType& mesh,
              const OutputNamedItemDataSet& output_named_item_data_set,
              std::optional<double> time) const;

  void _writeAtTime(const MeshVariant& mesh_v,
                    const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list,
                    double time) const final;

  void _write(const MeshVariant& mesh_v,
              const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list) const final;

  void _writeMesh(const MeshVariant& mesh_v) const final;

 public:
  Type
  type() const final
  {
    return Type::gnuplot;
  }

  GnuplotWriter(const std::string& base_filename) : GnuplotWriterBase(base_filename) {}

  GnuplotWriter(const std::string& base_filename, const double time_period)
    : GnuplotWriterBase(base_filename, time_period)
  {}

  ~GnuplotWriter() = default;
};

#endif   // GNUPLOT_WRITER_HPP
