#ifndef I_NAMED_DISCRETE_DATA_HPP
#define I_NAMED_DISCRETE_DATA_HPP

#include <string>

class INamedDiscreteData
{
 public:
  enum class Type
  {
    item_array,
    item_value,
    discrete_function
  };

  virtual Type type() const = 0;

  virtual const std::string& name() const = 0;

  INamedDiscreteData(const INamedDiscreteData&) = default;
  INamedDiscreteData(INamedDiscreteData&&)      = default;

  INamedDiscreteData() = default;

  virtual ~INamedDiscreteData() = default;
};

#endif   // I_NAMED_DISCRETE_DATA_HPP
