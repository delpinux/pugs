#ifndef NAMED_ITEM_VALUE_VARIANT_HPP
#define NAMED_ITEM_VALUE_VARIANT_HPP

#include <output/INamedDiscreteData.hpp>

#include <memory>
#include <string>

class ItemValueVariant;

class NamedItemValueVariant final : public INamedDiscreteData
{
 private:
  std::shared_ptr<const ItemValueVariant> m_item_value_variant;
  std::string m_name;

 public:
  Type
  type() const final
  {
    return INamedDiscreteData::Type::item_value;
  }

  const std::string&
  name() const final
  {
    return m_name;
  }

  const std::shared_ptr<const ItemValueVariant>
  itemValueVariant() const
  {
    return m_item_value_variant;
  }

  NamedItemValueVariant(const std::shared_ptr<const ItemValueVariant>& item_value_variant, const std::string& name)
    : m_item_value_variant{item_value_variant}, m_name{name}
  {}

  NamedItemValueVariant(const NamedItemValueVariant&) = default;
  NamedItemValueVariant(NamedItemValueVariant&&)      = default;

  NamedItemValueVariant()  = default;
  ~NamedItemValueVariant() = default;
};

#endif   // NAMED_ITEM_VALUE_VARIANT_HPP
