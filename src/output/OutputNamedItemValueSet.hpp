#ifndef OUTPUT_NAMED_ITEM_DATA_SET_HPP
#define OUTPUT_NAMED_ITEM_DATA_SET_HPP

#include <mesh/ItemArray.hpp>
#include <mesh/ItemValue.hpp>

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <utils/Exceptions.hpp>

#include <sstream>
#include <string>
#include <variant>

template <typename DataType,
          ItemType item_type,
          typename ConnectivityPtr,
          template <typename DataTypeT, ItemType item_type_t, typename ConnectivityPtrT>
          typename ItemDataT>
class NamedItemData
{
 private:
  std::string m_name;
  using ItemDataType = ItemDataT<const DataType, item_type, std::shared_ptr<const IConnectivity>>;
  ItemDataType m_item_data;

 public:
  constexpr const std::string&
  name() const
  {
    return m_name;
  }

  constexpr const ItemDataType&
  itemData() const
  {
    return m_item_data;
  }

  NamedItemData& operator=(const NamedItemData&) = default;
  NamedItemData& operator=(NamedItemData&&) = default;

  NamedItemData(const std::string& name, const ItemDataT<DataType, item_type, ConnectivityPtr>& item_data)
    : m_name(name), m_item_data(item_data)
  {
    ;
  }

  NamedItemData(const std::string& name, const ItemDataT<const DataType, item_type, ConnectivityPtr>& item_data)
    : m_name(name), m_item_data(item_data)
  {
    ;
  }

  NamedItemData(const NamedItemData&) = default;
  NamedItemData(NamedItemData&&)      = default;
  ~NamedItemData()                    = default;
};

class OutputNamedItemDataSet
{
 public:
  using ItemDataVariant = std::variant<NodeValue<const bool>,
                                       NodeValue<const int>,
                                       NodeValue<const long int>,
                                       NodeValue<const unsigned long int>,
                                       NodeValue<const double>,
                                       NodeValue<const TinyVector<1, double>>,
                                       NodeValue<const TinyVector<2, double>>,
                                       NodeValue<const TinyVector<3, double>>,
                                       NodeValue<const TinyMatrix<1, 1, double>>,
                                       NodeValue<const TinyMatrix<2, 2, double>>,
                                       NodeValue<const TinyMatrix<3, 3, double>>,

                                       EdgeValue<const bool>,
                                       EdgeValue<const int>,
                                       EdgeValue<const long int>,
                                       EdgeValue<const unsigned long int>,
                                       EdgeValue<const double>,
                                       EdgeValue<const TinyVector<1, double>>,
                                       EdgeValue<const TinyVector<2, double>>,
                                       EdgeValue<const TinyVector<3, double>>,
                                       EdgeValue<const TinyMatrix<1, 1, double>>,
                                       EdgeValue<const TinyMatrix<2, 2, double>>,
                                       EdgeValue<const TinyMatrix<3, 3, double>>,

                                       FaceValue<const bool>,
                                       FaceValue<const int>,
                                       FaceValue<const long int>,
                                       FaceValue<const unsigned long int>,
                                       FaceValue<const double>,
                                       FaceValue<const TinyVector<1, double>>,
                                       FaceValue<const TinyVector<2, double>>,
                                       FaceValue<const TinyVector<3, double>>,
                                       FaceValue<const TinyMatrix<1, 1, double>>,
                                       FaceValue<const TinyMatrix<2, 2, double>>,
                                       FaceValue<const TinyMatrix<3, 3, double>>,

                                       CellValue<const bool>,
                                       CellValue<const int>,
                                       CellValue<const long int>,
                                       CellValue<const unsigned long int>,
                                       CellValue<const double>,
                                       CellValue<const TinyVector<1, double>>,
                                       CellValue<const TinyVector<2, double>>,
                                       CellValue<const TinyVector<3, double>>,
                                       CellValue<const TinyMatrix<1, 1, double>>,
                                       CellValue<const TinyMatrix<2, 2, double>>,
                                       CellValue<const TinyMatrix<3, 3, double>>,

                                       NodeArray<const bool>,
                                       NodeArray<const int>,
                                       NodeArray<const long int>,
                                       NodeArray<const unsigned long int>,
                                       NodeArray<const double>,

                                       EdgeArray<const bool>,
                                       EdgeArray<const int>,
                                       EdgeArray<const long int>,
                                       EdgeArray<const unsigned long int>,
                                       EdgeArray<const double>,

                                       FaceArray<const bool>,
                                       FaceArray<const int>,
                                       FaceArray<const long int>,
                                       FaceArray<const unsigned long int>,
                                       FaceArray<const double>,

                                       CellArray<const bool>,
                                       CellArray<const int>,
                                       CellArray<const long int>,
                                       CellArray<const unsigned long int>,
                                       CellArray<const double>>;

 private:
  // We do not use a map, we want variables to be written in the
  // provided order
  std::vector<std::pair<std::string, ItemDataVariant>> m_name_itemvariant_list;

 public:
  auto
  begin() const
  {
    return m_name_itemvariant_list.begin();
  }

  auto
  end() const
  {
    return m_name_itemvariant_list.end();
  }

  template <typename DataType,
            ItemType item_type,
            typename ConnectivityPtr,
            template <typename DataTypeT, ItemType item_type_t, typename ConnectivityPtrT>
            typename ItemDataT>
  void
  add(const NamedItemData<DataType, item_type, ConnectivityPtr, ItemDataT>& named_item_data)
  {
    for (auto& [name, itemvariant] : m_name_itemvariant_list) {
      if (name == named_item_data.name()) {
        std::ostringstream error_msg;
        error_msg << "duplicate name '" << rang::fgB::yellow << name << rang::fg::reset << "' in name output list";
        throw NormalError(error_msg.str());
      }
    }
    m_name_itemvariant_list.push_back(std::make_pair(named_item_data.name(), named_item_data.itemData()));
  }

  OutputNamedItemDataSet(const OutputNamedItemDataSet&) = default;
  OutputNamedItemDataSet()                              = default;
  ~OutputNamedItemDataSet()                             = default;
};

#endif   // OUTPUT_NAMED_ITEM_DATA_SET_HPP
