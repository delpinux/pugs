#ifndef NAMED_ITEM_ARRAY_VARIANT_HPP
#define NAMED_ITEM_ARRAY_VARIANT_HPP

#include <output/INamedDiscreteData.hpp>

#include <memory>
#include <string>

class ItemArrayVariant;

class NamedItemArrayVariant final : public INamedDiscreteData
{
 private:
  std::shared_ptr<const ItemArrayVariant> m_item_array_variant;
  std::string m_name;

 public:
  Type
  type() const final
  {
    return INamedDiscreteData::Type::item_array;
  }

  const std::string&
  name() const final
  {
    return m_name;
  }

  const std::shared_ptr<const ItemArrayVariant>
  itemArrayVariant() const
  {
    return m_item_array_variant;
  }

  NamedItemArrayVariant(const std::shared_ptr<const ItemArrayVariant>& item_array_variant, const std::string& name)
    : m_item_array_variant{item_array_variant}, m_name{name}
  {}

  NamedItemArrayVariant(const NamedItemArrayVariant&) = default;
  NamedItemArrayVariant(NamedItemArrayVariant&&)      = default;

  NamedItemArrayVariant()  = default;
  ~NamedItemArrayVariant() = default;
};

#endif   // NAMED_ITEM_ARRAY_VARIANT_HPP
