#ifndef GNUPLOT_WRITER_RAW_HPP
#define GNUPLOT_WRITER_RAW_HPP

#include <output/GnuplotWriterBase.hpp>

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <mesh/MeshTraits.hpp>
#include <output/OutputNamedItemValueSet.hpp>

class MeshVariant;

#include <optional>
#include <string>

class GnuplotWriterRaw final : public GnuplotWriterBase
{
 private:
  template <typename DataType, ItemType item_type>
  size_t _itemDataNbRow(const ItemValue<DataType, item_type>&) const;

  template <typename DataType, ItemType item_type>
  size_t _itemDataNbRow(const ItemArray<DataType, item_type>&) const;

  template <MeshConcept MeshType, ItemType item_type>
  void _writeItemDatas(const MeshType& mesh,
                       const OutputNamedItemDataSet& output_named_item_data_set,
                       std::ostream& fout) const;

  template <MeshConcept MeshType>
  void _write(const MeshType& mesh,
              const OutputNamedItemDataSet& output_named_item_value_set,
              std::optional<double> time) const;

  void _writeAtTime(const MeshVariant& mesh_v,
                    const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list,
                    double time) const final;

  void _write(const MeshVariant& mesh_v,
              const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list) const final;

  void _writeMesh(const MeshVariant& mesh_v) const final;

 public:
  Type
  type() const final
  {
    return Type::gnuplot_raw;
  }

  GnuplotWriterRaw(const std::string& base_filename) : GnuplotWriterBase(base_filename) {}

  GnuplotWriterRaw(const std::string& base_filename, const double time_period)
    : GnuplotWriterBase(base_filename, time_period)
  {}

  ~GnuplotWriterRaw() = default;
};

#endif   // GNUPLOT_WRITER_RAW_HPP
