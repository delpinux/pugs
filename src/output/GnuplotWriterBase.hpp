#ifndef GNUPLOT_WRITER_BASE_HPP
#define GNUPLOT_WRITER_BASE_HPP

#include <output/WriterBase.hpp>

#include <utils/PugsMacros.hpp>

class GnuplotWriterBase : public WriterBase
{
 protected:
  std::string _getDateAndVersionComment() const;

  std::string _getFilename() const;

  void _writePreamble(const size_t& dimension,
                      const OutputNamedItemDataSet& output_named_item_data_set,
                      const bool& store_coordinates,
                      std::ostream& fout) const;

  template <typename ItemDataType>
  PUGS_INLINE bool
  _is_cell_data(const ItemDataType&) const
  {
    return ItemDataType::item_t == ItemType::cell;
  }

  template <typename ItemDataType>
  PUGS_INLINE bool
  _is_face_data(const ItemDataType&) const
  {
    return ItemDataType::item_t == ItemType::face;
  }

  template <typename ItemDataType>
  PUGS_INLINE bool
  _is_edge_data(const ItemDataType&) const
  {
    return ItemDataType::item_t == ItemType::edge;
  }

  template <typename ItemDataType>
  PUGS_INLINE bool
  _is_node_data(const ItemDataType&) const
  {
    return ItemDataType::item_t == ItemType::node;
  }

 public:
  GnuplotWriterBase(const std::string& base_filename, const double& time_period)
    : WriterBase(base_filename, time_period)
  {}

  GnuplotWriterBase(const std::string& base_filename) : WriterBase(base_filename) {}

  virtual ~GnuplotWriterBase() = default;
};

#endif   // GNUPLOT_WRITER_BASE_HPP
