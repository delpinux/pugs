#ifndef I_WRITER_HPP
#define I_WRITER_HPP

#include <output/INamedDiscreteData.hpp>

#include <memory>
#include <vector>

class MeshVariant;

class IWriter
{
 public:
  enum class Type
  {
    gnuplot,
    gnuplot_1d,
    gnuplot_raw,
    vtk
  };

  virtual Type type() const = 0;

  virtual void writeMesh(const std::shared_ptr<const MeshVariant>& mesh_v) const = 0;
  virtual void writeMesh(const MeshVariant& mesh_v) const                        = 0;

  virtual void write(const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list) const = 0;

  virtual void writeIfNeeded(const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list,
                             double time) const = 0;

  virtual void writeForced(const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list,
                           double time) const = 0;

  virtual void writeOnMesh(
    const std::shared_ptr<const MeshVariant>& mesh_v,
    const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list) const = 0;

  virtual void writeOnMeshIfNeeded(
    const std::shared_ptr<const MeshVariant>& mesh_v,
    const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list,
    double time) const = 0;

  virtual void writeOnMeshForced(const std::shared_ptr<const MeshVariant>& mesh_v,
                                 const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list,
                                 double time) const = 0;

  IWriter()          = default;
  virtual ~IWriter() = default;
};

#endif   // I_WRITER_HPP
