#ifndef VTK_WRITER_HPP
#define VTK_WRITER_HPP

#include <output/WriterBase.hpp>

#include <algebra/TinyMatrix.hpp>
#include <algebra/TinyVector.hpp>
#include <mesh/MeshTraits.hpp>
#include <output/OutputNamedItemValueSet.hpp>

#include <optional>
#include <string>

class VTKWriter final : public WriterBase
{
 private:
  class SerializedDataList;

  std::string _getDateAndVersionComment() const;

  std::string _getFilenamePVTU() const;

  std::string _getFilenameVTU(int rank_number) const;

  template <typename ItemDataT>
  void _write_item_pvtu(std::ofstream& os, const std::string& name, const ItemDataT&) const;

  template <typename ItemDataT>
  void _write_node_pvtu(std::ofstream& os, const std::string& name, const ItemDataT&) const;

  template <typename ItemDataT>
  void _write_cell_pvtu(std::ofstream& os, const std::string& name, const ItemDataT&) const;

  template <typename DataType>
  struct VTKType;

  template <typename DataType>
  void _write_array(std::ofstream& os,
                    const std::string& name,
                    const Array<DataType>& array,
                    SerializedDataList& serialized_data_list) const;

  template <size_t N, typename DataType>
  void _write_array(std::ofstream& os,
                    const std::string& name,
                    const Array<TinyVector<N, DataType>>& array,
                    SerializedDataList& serialized_data_list) const;

  template <typename DataType>
  void _write_node_value(std::ofstream& os,
                         const std::string& name,
                         const NodeValue<const DataType>& item_value,
                         SerializedDataList& serialized_data_list) const;

  template <size_t N, typename DataType>
  void _write_node_value(std::ofstream& os,
                         const std::string& name,
                         const NodeValue<const TinyVector<N, DataType>>& item_value,
                         SerializedDataList& serialized_data_list) const;

  template <size_t N, typename DataType>
  void _write_node_value(std::ofstream& os,
                         const std::string& name,
                         const NodeValue<const TinyMatrix<N, N, DataType>>& item_value,
                         SerializedDataList& serialized_data_list) const;

  template <typename DataType>
  void _write_node_value(std::ofstream&,
                         const std::string&,
                         const CellValue<const DataType>&,
                         SerializedDataList&) const;

  template <typename DataType>
  void _write_cell_value(std::ofstream& os,
                         const std::string& name,
                         const CellValue<const DataType>& item_value,
                         SerializedDataList& serialized_data_list) const;

  template <typename ItemDataT>
  void _write_item_data(std::ofstream& os,
                        const std::string& name,
                        const ItemDataT& item_data,
                        SerializedDataList& serialized_data_list) const;

  template <typename ItemDataT>
  void _write_cell_data(std::ofstream& os,
                        const std::string& name,
                        const ItemDataT& item_data,
                        SerializedDataList& serialized_data_list) const;

  template <typename ItemDataT>
  void _write_node_data(std::ofstream& os,
                        const std::string& name,
                        const ItemDataT& item_data,
                        SerializedDataList& serialized_data_list) const;

  template <MeshConcept MeshType>
  void _write(const MeshType& mesh,
              const OutputNamedItemDataSet& output_named_item_data_set,
              std::optional<double> time) const;

  void _writeAtTime(const MeshVariant& mesh_v,
                    const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list,
                    double time) const final;

  void _write(const MeshVariant& mesh_v,
              const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list) const final;

  void _writeMesh(const MeshVariant& mesh_v) const final;

 public:
  Type
  type() const final
  {
    return Type::vtk;
  }

  VTKWriter(const std::string& base_filename) : WriterBase(base_filename) {}

  VTKWriter(const std::string& base_filename, const double time_period) : WriterBase(base_filename, time_period) {}

  ~VTKWriter() = default;
};

#endif   // VTK_WRITER_HPP
