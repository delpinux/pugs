#include <output/GnuplotWriter.hpp>

#include <mesh/Connectivity.hpp>
#include <mesh/ItemValue.hpp>
#include <mesh/Mesh.hpp>
#include <mesh/MeshData.hpp>
#include <mesh/MeshDataManager.hpp>
#include <mesh/MeshTraits.hpp>
#include <mesh/MeshVariant.hpp>
#include <utils/Demangle.hpp>
#include <utils/Filesystem.hpp>
#include <utils/Messenger.hpp>
#include <utils/PugsTraits.hpp>
#include <utils/RevisionInfo.hpp>
#include <utils/Stringify.hpp>

#include <fstream>

template <typename DataType>
void
GnuplotWriter::_writeCellData(const CellValue<DataType>& cell_value, CellId cell_id, std::ostream& fout) const
{
  const auto& value = cell_value[cell_id];
  if constexpr (std::is_arithmetic_v<DataType>) {
    fout << ' ' << value;
  } else if constexpr (is_tiny_vector_v<std::decay_t<DataType>>) {
    for (size_t i = 0; i < value.dimension(); ++i) {
      fout << ' ' << value[i];
    }
  } else if constexpr (is_tiny_matrix_v<std::decay_t<DataType>>) {
    for (size_t i = 0; i < value.numberOfRows(); ++i) {
      for (size_t j = 0; j < value.numberOfColumns(); ++j) {
        fout << ' ' << value(i, j);
      }
    }
  } else {
    throw UnexpectedError("invalid data type for cell value output: " + demangle<DataType>());
  }
}

template <typename DataType>
void
GnuplotWriter::_writeCellData(const CellArray<DataType>& cell_array, CellId cell_id, std::ostream& fout) const
{
  const auto& array = cell_array[cell_id];
  if constexpr (std::is_arithmetic_v<DataType>) {
    for (size_t i = 0; i < array.size(); ++i) {
      fout << ' ' << array[i];
    }
  } else {
    throw UnexpectedError("invalid data type for cell value output: " + demangle<DataType>());
  }
}

template <typename ItemDataT>
void
GnuplotWriter::_writeData(const ItemDataT& item_data,
                          [[maybe_unused]] CellId cell_id,
                          [[maybe_unused]] NodeId node_id,
                          std::ostream& fout) const
{
  if constexpr (ItemDataT::item_t == ItemType::cell) {
    this->_writeCellData(item_data, cell_id, fout);
  } else if constexpr (ItemDataT::item_t == ItemType::node) {
    this->_writeNodeData(item_data, node_id, fout);
  } else {
    throw UnexpectedError{"invalid item type"};
  }
}

template <MeshConcept MeshType>
void
GnuplotWriter::_writeDataAtNodes(const MeshType& mesh,
                                 const OutputNamedItemDataSet& output_named_item_data_set,
                                 std::ostream& fout) const
{
  if constexpr (MeshType::Dimension == 1) {
    auto cell_to_node_matrix = mesh.connectivity().cellToNodeMatrix();
    auto cell_is_owned       = mesh.connectivity().cellIsOwned();

    for (CellId cell_id = 0; cell_id < mesh.numberOfCells(); ++cell_id) {
      if (cell_is_owned[cell_id]) {
        const auto& cell_nodes = cell_to_node_matrix[cell_id];
        for (size_t i_node = 0; i_node < cell_nodes.size(); ++i_node) {
          const NodeId& node_id                     = cell_nodes[i_node];
          const TinyVector<MeshType::Dimension>& xr = mesh.xr()[node_id];
          fout << xr[0];
          for (const auto& [name, item_data_variant] : output_named_item_data_set) {
            std::visit([&](auto&& item_data) { _writeData(item_data, cell_id, node_id, fout); }, item_data_variant);
          }
          fout << '\n';
        }

        fout << "\n\n";
      }
    }
  } else if constexpr (MeshType::Dimension == 2) {
    auto cell_to_node_matrix = mesh.connectivity().cellToNodeMatrix();
    auto cell_is_owned       = mesh.connectivity().cellIsOwned();

    for (CellId cell_id = 0; cell_id < mesh.numberOfCells(); ++cell_id) {
      if (cell_is_owned[cell_id]) {
        const auto& cell_nodes = cell_to_node_matrix[cell_id];
        for (size_t i_node = 0; i_node < cell_nodes.size(); ++i_node) {
          const NodeId& node_id                     = cell_nodes[i_node];
          const TinyVector<MeshType::Dimension>& xr = mesh.xr()[node_id];
          fout << xr[0] << ' ' << xr[1];
          for (const auto& [name, item_data_variant] : output_named_item_data_set) {
            std::visit([&](auto&& item_data) { _writeData(item_data, cell_id, node_id, fout); }, item_data_variant);
          }
          fout << '\n';
        }
        const NodeId& node_id                     = cell_nodes[0];
        const TinyVector<MeshType::Dimension>& xr = mesh.xr()[node_id];
        fout << xr[0] << ' ' << xr[1];
        for (const auto& [name, item_data_variant] : output_named_item_data_set) {
          std::visit([&](auto&& item_data) { _writeData(item_data, cell_id, node_id, fout); }, item_data_variant);
        }
        fout << "\n\n\n";
      }
    }
  } else {
    throw UnexpectedError("invalid mesh dimension");
  }
}

template <typename DataType>
void
GnuplotWriter::_writeNodeData(const NodeValue<DataType>& node_value, NodeId node_id, std::ostream& fout) const
{
  const auto& value = node_value[node_id];
  if constexpr (std::is_arithmetic_v<DataType>) {
    fout << ' ' << value;
  } else if constexpr (is_tiny_vector_v<std::decay_t<DataType>>) {
    for (size_t i = 0; i < value.dimension(); ++i) {
      fout << ' ' << value[i];
    }
  } else if constexpr (is_tiny_matrix_v<std::decay_t<DataType>>) {
    for (size_t i = 0; i < value.numberOfRows(); ++i) {
      for (size_t j = 0; j < value.numberOfColumns(); ++j) {
        fout << ' ' << value(i, j);
      }
    }
  } else {
    throw UnexpectedError("invalid data type for cell value output: " + demangle<DataType>());
  }
}

template <typename DataType>
void
GnuplotWriter::_writeNodeData(const NodeArray<DataType>& node_array, NodeId node_id, std::ostream& fout) const
{
  const auto& array = node_array[node_id];
  if constexpr (std::is_arithmetic_v<DataType>) {
    for (size_t i = 0; i < array.size(); ++i) {
      fout << ' ' << array[i];
    }
  } else {
    throw UnexpectedError("invalid data type for cell value output: " + demangle<DataType>());
  }
}

template <MeshConcept MeshType>
void
GnuplotWriter::_write(const MeshType& mesh,
                      const OutputNamedItemDataSet& output_named_item_data_set,
                      std::optional<double> time) const
{
  createDirectoryIfNeeded(_getFilename());

  if (parallel::rank() == 0) {
    std::ofstream fout{_getFilename()};
    if (not fout) {
      std::ostringstream error_msg;
      error_msg << "cannot create file \"" << rang::fgB::yellow << _getFilename() << rang::fg::reset << '"';
      throw NormalError(error_msg.str());
    }

    fout.precision(15);
    fout.setf(std::ios_base::scientific);

    fout << this->_getDateAndVersionComment();
    if (time.has_value()) {
      fout << "# time = " << *time << "\n\n";
    }
    this->_writePreamble(MeshType::Dimension, output_named_item_data_set, true /*store coordinates*/, fout);
  }

  for (const auto& [name, item_data_variant] : output_named_item_data_set) {
    std::visit(
      [&, var_name = name](auto&& item_data) {
        using ItemDataType = std::decay_t<decltype(item_data)>;
        if (ItemDataType::item_t == ItemType::face) {
          std::ostringstream error_msg;
          error_msg << "gnuplot_writer does not support face data, cannot save variable \"" << rang::fgB::yellow
                    << var_name << rang::fg::reset << '"';
          throw NormalError(error_msg.str());
        }
      },
      item_data_variant);
  }

  for (const auto& [name, item_data_variant] : output_named_item_data_set) {
    std::visit(
      [&, var_name = name](auto&& item_data) {
        using ItemDataType = std::decay_t<decltype(item_data)>;
        if (ItemDataType::item_t == ItemType::edge) {
          std::ostringstream error_msg;
          error_msg << "gnuplot_writer does not support edge data, cannot save variable \"" << rang::fgB::yellow
                    << var_name << rang::fg::reset << '"';
          throw NormalError(error_msg.str());
        }
      },
      item_data_variant);
  }

  createDirectoryIfNeeded(m_base_filename);

  for (size_t i_rank = 0; i_rank < parallel::size(); ++i_rank) {
    if (i_rank == parallel::rank()) {
      std::ofstream fout(_getFilename(), std::ios_base::app);
      if (not fout) {
        std::ostringstream error_msg;
        error_msg << "cannot open file \"" << rang::fgB::yellow << _getFilename() << rang::fg::reset << '"';
        throw NormalError(error_msg.str());
      }

      fout.precision(15);
      fout.setf(std::ios_base::scientific);

      this->_writeDataAtNodes(mesh, output_named_item_data_set, fout);
    }
    parallel::barrier();
  }
}

void
GnuplotWriter::_writeAtTime(const MeshVariant& mesh_v,
                            const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list,
                            double time) const
{
  OutputNamedItemDataSet output_named_item_data_set = this->_getOutputNamedItemDataSet(named_discrete_data_list);

  std::visit(
    [&](auto&& p_mesh) {
      using MeshType = mesh_type_t<decltype(p_mesh)>;
      if constexpr ((MeshType::Dimension == 1) or (MeshType::Dimension == 2)) {
        this->_write(*p_mesh, output_named_item_data_set, time);
      } else {
        throw NormalError("gnuplot format is not available in dimension " + stringify(MeshType::Dimension));
      }
    },
    mesh_v.variant());
}

void
GnuplotWriter::_writeMesh(const MeshVariant& mesh_v) const
{
  OutputNamedItemDataSet output_named_item_data_set{};

  std::visit(
    [&](auto&& p_mesh) {
      using MeshType = mesh_type_t<decltype(p_mesh)>;
      if constexpr ((MeshType::Dimension == 1) or (MeshType::Dimension == 2)) {
        this->_write(*p_mesh, output_named_item_data_set, {});
      } else {
        throw NormalError("gnuplot format is not available in dimension " + stringify(MeshType::Dimension));
      }
    },
    mesh_v.variant());
}

void
GnuplotWriter::_write(const MeshVariant& mesh_v,
                      const std::vector<std::shared_ptr<const INamedDiscreteData>>& named_discrete_data_list) const
{
  OutputNamedItemDataSet output_named_item_data_set = this->_getOutputNamedItemDataSet(named_discrete_data_list);

  std::visit(
    [&](auto&& p_mesh) {
      using MeshType = mesh_type_t<decltype(p_mesh)>;
      if constexpr ((MeshType::Dimension == 1) or (MeshType::Dimension == 2)) {
        this->_write(*p_mesh, output_named_item_data_set, {});
      } else {
        throw NormalError("gnuplot format is not available in dimension " + stringify(MeshType::Dimension));
      }
    },
    mesh_v.variant());
}
