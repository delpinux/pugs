#include <dev/ParallelChecker.hpp>

void
parallel_check(const ItemArrayVariant& item_array_variant,
               const std::string& name,
               const SourceLocation& source_location)
{
  std::visit([&](auto&& item_value) { parallel_check(item_value, name, source_location); },
             item_array_variant.itemArray());
}
