#include <dev/ParallelChecker.hpp>

void
parallel_check(const ItemValueVariant& item_value_variant,
               const std::string& name,
               const SourceLocation& source_location)
{
  std::visit([&](auto&& item_value) { parallel_check(item_value, name, source_location); },
             item_value_variant.itemValue());
}
