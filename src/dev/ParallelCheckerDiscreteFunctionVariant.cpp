#include <dev/ParallelChecker.hpp>

void
parallel_check(const DiscreteFunctionVariant& discrete_function_variant,
               const std::string& name,
               const SourceLocation& source_location)
{
  std::visit([&](auto&& discrete_function) { parallel_check(discrete_function, name, source_location); },
             discrete_function_variant.discreteFunction());
}
