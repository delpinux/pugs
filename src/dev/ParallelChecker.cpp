#include <dev/ParallelChecker.hpp>

ParallelChecker* ParallelChecker::m_instance = nullptr;

void
ParallelChecker::create()
{
  Assert(ParallelChecker::m_instance == nullptr, "ParallelChecker has already been created");
  ParallelChecker::m_instance = new ParallelChecker;
}

void
ParallelChecker::destroy()
{
  Assert(ParallelChecker::m_instance != nullptr, "ParallelChecker has already been destroyed");
  delete ParallelChecker::m_instance;
  ParallelChecker::m_instance = nullptr;
}
