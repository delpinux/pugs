#include <dev/ParallelChecker.hpp>

void
parallel_check(const SubItemValuePerItemVariant& subitem_value_per_item_variant,
               const std::string& name,
               const SourceLocation& source_location)
{
  std::visit([&](auto&& item_value) { parallel_check(item_value, name, source_location); },
             subitem_value_per_item_variant.subItemValuePerItem());
}
